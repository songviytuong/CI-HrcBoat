$('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});

$().ready(function(){
    setTimeout(function(){
        $('#GroupName').focus();
    },100);
});

$('.sGroup').click(function(){
    $.ajax({
        url: "/insertData",
        dataType: "html",
        type: "POST",
        data: "GroupName="+$('#GroupName').val(),
        context: $(this),
        success: function(result){
            if(result == "OK"){
                alert('Tạo nhóm thành công !!!');
                window.location = "/";
            }else{
                alert('Yêu cầu nhập tên nhóm !!!');
                $('#GroupName').focus();
            }
        }
    });
});

$('.uGroup').click(function(){
    $.ajax({
        url: "/updateData",
        dataType: "html",
        type: "POST",
        data: "GroupName="+$('#GroupName').val()+"&ID="+$('#idEdit').val(),
        context: $(this),
        success: function(result){
            console.log(result);
            if(result == "OK"){
                alert('Cập nhật thành công !!!');
                window.location = "../../";
            }
        }
    });
});

$('.group').change(function(){
    var id = $(this).val();
    if($(this).val() == -1){
        $('.showMember').html("");
    }else{
        $.ajax({
            url: "/listMember",
            dataType: "html",
            type: "POST",
            data: "group_id="+id,
            context: $(this),
            success: function(result){
                $('.showMember').html(result);
            }
        });
    }
});

$('.addMember').click(function(){
    $(this).addClass("disabled");
    var name = $('#Name').val();
    var gid = $('.group').val();
    if(name == ""){
        alert('Nhập vào tên Thành Viên !!!');
        $('#Name').focus();
        $(this).removeClass("disabled");
    }else if(gid == -1){
        alert('Chọn nhóm để thêm Thành Viên !!!');
        $('.group').focus();
        $(this).removeClass("disabled");
    }else{
        $.ajax({
            url: "/addMember",
            dataType: "html",
            type: "POST",
            data: $('#frmCreateMember').serialize(),
            context: $(this),
            success: function(result){
                if(result == "OK"){
                    $('.group').change();
                    $('#Name').val("");
                    $('#Phone').val("");
                }
                $(this).removeClass("disabled");
            }
        });
    }
});

$('.fa-play').click(function(){
    window.location = "/play";
});

$('#Number').keypress(function(e){
    if(e.which == 13) {
        $('.sCreate').click();
    }
});

$('#GroupName').keypress(function(e){
    if(e.which == 13) {
        $('.sGroup').click();
    }
});

$('.sCreate').click(function(){
    $.ajax({
        url: "/screate",
        dataType: "html",
        type: "POST",
        data: "Number="+$('#Number').val(),
        context: $(this),
        success: function(result){
            if(result == "False"){
                $('#Number').focus();
            }else{
                $('#showResult').html(result);
                $('#Number').attr("disabled",true);
                $(this).attr("disabled",true);
                $('.sPlay').removeClass("hidden");
                $('.sReset').removeClass("hidden");
            }
        }
    });
});

$('.sReset').click(function(){
    location.reload();
});

$('.sPlay').click(function(){
    $.ajax({
        url: "/splay",
        dataType: "html",
        type: "POST",
        data: $('#frmCreatePlay').serialize(),
        context: $(this),
        success: function(result){
            window.location = "/iplayer/"+result;
        }
    });
});
$('.sPrint').click(function(){
    $('.prt').remove();
    window.print();
});
$('.sPlays').click(function(){
    $.ajax({
        url: "/splays",
        dataType: "html",
        type: "POST",
        data: "Number="+$('#Number').val(),
        context: $(this),
        success: function(result){
            $(this).addClass("disabled",true);
            $('#showResult').html(result);
        }
    });
});


$(document).on("click", ".delGroup", function(){
    if (!confirm('Bạn muốn xóa Group?\nLưu ý: Các Thành Viên trong nhóm bị xóa :(')) {
        return false;
    }else{
        $.ajax({
            type: "GET",
            url: "/deleteGroup",
            data: "ID=" + $(this).attr("rel"),
            beforeSend: function () {
            },
            success: function (data) {
                location.reload();
            }
        });
    }
});

$(document).on("click", ".delMember", function(){
    if (!confirm('Bạn muốn xóa Thành Viên ??')) {
        return false;
    }else{
        $.ajax({
            type: "GET",
            url: "/deleteMember",
            data: "ID=" + $(this).attr("rel"),
            beforeSend: function () {
            },
            success: function () {
                $('.group').change();
                $('#Name').val("");
                $('#Phone').val("");
            }
        });
    }
});