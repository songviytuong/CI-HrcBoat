$(document).ready(function(){	

	//Set animate.css
		if ($('.content').hasClass('home') && $(window).width() >= 991) {
			// new WOW().init();			

			// $('.main-slider').addClass('wow animated fadeIn');


			// $('header .main-nav').addClass('wow animated fadeInDown').attr( "data-wow-delay", "0.5s" );;				

			// $('.slider-icon').addClass('wow animated pulse').attr( "data-wow-delay", "0.5s" );;

			// $('.about-box').addClass('wow animated fadeInDown').attr( "data-wow-delay", "1s" );

			// $('.service-box').addClass('wow animated fadeInDown').attr( "data-wow-delay", "0.5s" );	
			// $('.service-box .col-xs-12:nth-child(1)').addClass('wow animated bounceInLeft').attr( "data-wow-delay", "0.5s" );	
			// $('.service-box .col-xs-12:nth-child(2)').addClass('wow animated flipInX').attr( "data-wow-delay", "0.5s" );	
			// $('.service-box .col-xs-12:nth-child(3)').addClass('wow animated bounceInRight').attr( "data-wow-delay", "0.5s" );	

			// $('.partner-box').addClass('wow animated fadeInDown').attr( "data-wow-delay", "0.5s" );

			// $('.blog-box').addClass('wow animated fadeInDown').attr( "data-wow-delay", "1.5s" );

			// $('.recruitment-box').addClass('wow animated fadeInDown').attr( "data-wow-delay", "0.5s" );			

			// $('footer').addClass('wow animated fadeInDown').attr( "data-wow-delay", "0.5s" );	
		};

	$('#recruitment-modal').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget);
		  var recipient = button.data('title');
		  var postid = button.data('id');
		  var modal = $(this);
		  modal.find('.modal-title').text(recipient);
		  modal.find('#PostID').val(postid);
	});

	$(window).bind("load", function() {

		if ($('.content').hasClass('detail-gallery-page')) {
			/* copy loaded thumbnails into carousel */
			$('.gallery-content .img-thumbnail').on('load', function() {
			  
			}).each(function(i) {
			  if(this.complete) {
			  	var item = $('<div class="item"><img src="" /></div>');
			  	item.find("img").attr("src",$(this).attr("data-img"));
			  	item.appendTo('.carousel-inner'); 
			    if (i==0){ // set first item active
			     item.addClass('active');
			    }
			  }
			});

			/* activate the carousel */
			$('#modalCarousel').carousel({interval:false});
			/* when clicking a thumbnail */
			$('.gallery-content .img-thumbnail').click(function(e){
				e.preventDefault();
			    var idx = $(this).parents('div').index();
			  	var id = parseInt(idx);	
			  	$('#galleryModal').modal('show'); // show the modal			  			  	
			    $('#modalCarousel').carousel(id); // slide carousel to selected
			  	
			});
		};
		
		// scrollUp
		var scrollButton = $('#scrollUp');
		$(window).scroll(function(){
			$(this).scrollTop() >= 1000 ? scrollButton.show() : scrollButton.hide();			
			console.log($(this).scrollTop());
		});

		// Click on scrollUp button
		scrollButton.click(function(){
			console.log("click");
			$('html,body').animate({scrollTop:0},600);
		});

		//Set height to image	
		if ($('.content').hasClass('home') && $(window).width() <= 767) {	
			var $height = $('.feature-box').find('.col-xs-4 img').height();
			$('.feature-box .col-sm-6').css('max-height', $height - 1);		
		}

		//Display btn-normal < 1199
		if ($(window).width() <= 1199) {
			$('.btn-default').addClass('btn-sm');
		}

		//Display btn-normal < 480
		if ($('.content').hasClass('detail') && $(window).width() <= 480) {
			var $width = $('.carousel-inner img').width() / 4 - 2;
			$('.carousel-indicators li').css('max-width', $width).find('img').css('max-width', $width);
		}

		//Show about-page tooltip
		if ($('.content').hasClass('about-page') && $(window).width() >= 480) {
			$('.about-core-box').find('a').hover(function(){
			    $(this).next().css({'opacity': 1, 'z-index': 102, 'display': 'block'});
			    }, function(){
			    $(this).next().css({'opacity': 0, 'z-index': 98, 'display': 'none'});
			}); 
		};	
		if ($('.content').hasClass('about-page') && $(window).width() <= 480) {
			$('.about-core-box').find('a').click(function(event){
				event.preventDefault();
				var addHeight = $(this).next().height() + $(this).parent().height() + 85;
				$(this).parent().height(addHeight);
			}); 
		};	

				
	});
});