$(document).ready(function(){
	$(".delete_link").click(function(){
        var r = confirm("Bạn có chắc muốn xóa dữ liệu này ?");
        if (r == false) {
            return false;
        }
    });

    $(".checkfull").change(function(){
    	var classname = $(this).attr("data");
    	if(this.checked===true){
	    	$(".checkbox_"+classname).prop('checked', true);
	    }else{
	    	$(".checkbox_"+classname).prop('checked', false);
	    }
    });

    $(".tab_control a").click(function(){
        $(".tab_control a").removeClass("current");
        $(this).addClass("current");
        var data = $(this).attr("data");
        $(".tabcontent").hide();
        $("."+data).show();
    });

    $('button[type="submit"]').click(function(){
        $(this).addClass("saving");
    });
});
	