<?php
class conf_define extends CI_Model {

    var $tablename    = 'ttp_define';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_order_status($group,$type,$order = null,$by = null)
    {
        if($order == null){$order = "name";}
        if($by == null){$by = "asc";}
        
        $this->db->select('code, name');
        $this->db->where('group',$group); 
        $this->db->where('type',$type);
        $this->db->order_by($order, $by);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }
    
    function get_list_by_group($code = null){
        if($code){
            $this->db->select('*');
            $this->db->where('group',$code); 
            $this->db->order_by('name', 'asc'); 
            $result = $this->db->get($this->tablename)->result();
            if($result != null)
            return $result;
        }
        return false;
    }

}