<?php
class Conf_group extends CI_Model {

    const _GroupTable = 'ttp_group';
    public function __construct(){
        parent::__construct();
    }
    
    function getListGroup()
    {
        $this->db->select('*');
        $result = $this->db->get(self::_GroupTable)->result();
        return $result;
    }
    
    function getCountListGroup()
    {
        $this->db->select('count(*) as nav');
        $result = $this->db->get(self::_GroupTable)->row();
        return $result;
    }
    
    function getListConfig(){
        
    }
}