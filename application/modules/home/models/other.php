<?php
class Other extends CI_Model {

    const _tablename    = 'users';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->db = $this->load->database('otherdb',TRUE);
    }
    
    public function getUsers()
    {
        $this->db->select('*');
        $res = $this->db->get(self::_tablename)->result();
        return $res;
    }

}