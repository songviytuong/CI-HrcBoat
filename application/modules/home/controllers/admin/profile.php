<?php 
class Profile extends Admin_Controller { 
	public $user;
	public $classname="profile";
	public $limit = 10;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$id = $this->user->ID;
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_user where ID=$id")->row();
			if(!$result) show_error("Data is empty !");
			$this->template->add_title('Profile user');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/profile/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/profile_edit',$data);
			$this->template->render();
		}
	}
	

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
		$UserName = isset($_POST['UserName']) ? $this->lib->fill_data($_POST['UserName']) : '' ;
		$Password = isset($_POST['Password']) ? $this->lib->fill_data($_POST['Password']) : '' ;
		$Email = isset($_POST['Email']) ? $this->lib->fill_data($_POST['Email']) : '' ;
		$LastName = isset($_POST['LastName']) ? $this->lib->fill_data($_POST['LastName']) : '' ;
		$FirstName = isset($_POST['FirstName']) ? $this->lib->fill_data($_POST['FirstName']) : '' ;
		$Gender = isset($_POST['Gender']) ? $this->lib->fill_data($_POST['Gender']) : 0 ;
		$check = $this->db->query("select * from ttp_user where UserName='$UserName' and ID!=$ID")->row();
		if($check){
			$return = ADMINPATH ."/home/".$this->classname."/edit/$ID/?error=user_exists";
			redirect($return);
		}
		if($UserName!='' && $Password!='' && $Email!='' && $Gender!=''){
			$user = $this->db->query("select * from ttp_user where ID=$ID")->row();
			if($user){
				$newPassword = $Password==$user->Password ? $Password : sha1($Password) ;
				$data = array(
					'Gender'	=> $Gender,
					'UserName'	=> $UserName,
					'Password'	=> $newPassword,
					'Email'		=> $Email,
					'LastName'	=> $LastName,
					'FirstName'	=> $FirstName,
					'LastEdited'=> date('Y-m-d H:i:s')
				);
				if(isset($_FILES['Image_upload'])){
					if($_FILES['Image_upload']['tmp_name']!=''){
						$this->upload_to = "user_thumb/$ID";
						$data['Thumb'] = $this->upload_image_single(true,100,100);
						$this->delete_file_path($user->Thumb);
					}
				}
				$this->db->where("ID",$ID);
				$this->db->update("ttp_user",$data);
			}
		}
		redirect(ADMINPATH.'/home/system_user/');
	}
}
?>