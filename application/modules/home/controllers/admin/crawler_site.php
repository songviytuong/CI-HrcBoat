<?php 
class Crawler_site extends Admin_Controller { 
	public $user;
	public $classname="crawler_site";
	public $limit = 15;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_Site($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Site list | Config Crawler');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_crawler_site")->row();
        $nav = $nav ? $nav->nav : 0 ;
		$object = $this->db->query("select * from ttp_crawler_site $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/crawler_site/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/crawler_site/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/crawler_site_home',$data); 
		$this->template->render();
	}

	public function search(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		if(isset($_GET['keyword'])){
			$keyword = $this->lib->fill_data($_GET['keyword']);
			$str = "select * from ttp_crawler_site";
			$str = $keyword != '' ? $str." where Site like '%".urldecode($keyword)."%'" : $str ;
			$this->template->add_title('Tìm kiếm dữ liệu');
			$data=array(
				'data'	=> $this->db->query($str)->result(),
				'nav'	=> '',
				'start'	=> 0,
				'base_link' =>	base_url().ADMINPATH.'/home/crawler_site/',
			);
			$this->template->write_view('content','admin/crawler_site_home',$data);
			$this->template->render();
			return;
		}
		redirect(ADMINPATH.'/home/crawler_site/');
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$this->db->query("delete from ttp_crawler_site where ID=$id");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	

	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add Site | Config Crawler');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/crawler_site/'
		);
		$this->template->write_view('content','admin/crawler_site_add',$data);
		$this->template->render();
	}

	public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$Site = isset($_POST['Site']) ? $_POST['Site'] : '' ;
		$Published = isset($_POST['Published']) ? $_POST['Published'] : '' ;
		if($Site!=''){
			$data = array(
				'Published'	=> $Published,
				'Site'	=> $Site
			);
			$json = array();
			foreach($_POST as $key=>$value){
				if(!array_key_exists($key,$data)){
					$json["$key"] = $value;
				}
			}
			$data['Data'] = json_encode($json);
			$this->db->insert("ttp_crawler_site",$data);
		}
		redirect(ADMINPATH.'/home/crawler_site/');
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_crawler_site where ID=$id")->row();
			if(!$result) show_error();
			$this->template->add_title('Add Site | Config Crawler');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/crawler_site/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/crawler_site_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
		$Site = isset($_POST['Site']) ? $this->lib->fill_data($_POST['Site']) : '' ;
		$Published = isset($_POST['Published']) ? $_POST['Published'] : '' ;
		if($Site!=''){
			$data = array(
				'Published'	=> $Published,
				'Site'	=> $Site
			);
			$json = array();
			foreach($_POST as $key=>$value){
				if(!array_key_exists($key,$data)){
					if($key!='ID')
					$json["$key"] = $value;
				}
			}
			$data['Data'] = json_encode($json);
			$this->db->where("ID",$ID);
			$this->db->update("ttp_crawler_site",$data);
		}
		redirect(ADMINPATH.'/home/crawler_site/');
	}

}
?>