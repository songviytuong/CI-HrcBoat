<?php 
class Permission extends Admin_Controller { 
	public $user;
	public $classname="home";
	public $limit = 15;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->template->add_title('Error permission');
		$this->template->write_view('content','admin/error_permission');
		$this->template->render();
	}

	public function module(){
		$this->template->add_title('Error module');
		$this->template->write_view('content','admin/error_permission');
		$this->template->render();	
	}
}
?>