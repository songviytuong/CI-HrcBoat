<?php 
class Home extends Admin_Controller { 
 
 	public $user;
 	public $classname="home";

    public function __construct() { 
        parent::__construct();   
        $session = $this->session->userdata('ttp_usercp');
	$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
	public function index(){
		$this->template->add_title('Admin Page');
		$this->template->write_view('content','admin/home');
		$this->template->render();
	}
        
        public function rander($user){
            die("$user");
        }
}
?>