<?php 
class Site_categories extends Admin_Controller { 
	public $user;
	public $classname="site_categories";
	public $limit = 15;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Categories list | Config Site');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID")->row();
        $nav = $nav ? $nav->nav : 0 ;
		$object = $this->db->query("select a.*,b.Title as PageTitle from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID order by Created DESC $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/site_categories/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/site_categories/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/site_categories_home',$data); 
		$this->template->render();
	}

	public function search($link='search'){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
		$keyword = $this->session->userdata("categories_filter_Keywords");
		$PagelinksID = $this->session->userdata("categories_filter_PagelinksID");
		$Published = $this->session->userdata("categories_filter_Published");
		$str_nav = "select count(1) as nav from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID";
		$str = "select a.*,b.Title as PageTitle from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID";
		$str = $keyword != '' ? $str." and (a.Title like '%".urldecode($keyword)."%' or b.Title like '%".urldecode($keyword)."%')" : $str ;
		$str_nav = $keyword != '' ? $str_nav." and (a.Title like '%".urldecode($keyword)."%' or b.Title like '%".urldecode($keyword)."%')" : $str_nav ;
		if($PagelinksID>0){
			$str.=" and a.PagelinksID=$PagelinksID";
			$str_nav.=" and a.PagelinksID=$PagelinksID";
		}
		if($Published!=''){
			$str.=" and a.Published=$Published";
			$str_nav.=" and a.Published=$Published";
		}
		$nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
		$this->template->add_title('Tìm kiếm dữ liệu');
		$data=array(
			'data'	=> $this->db->query($str." order by ID DESC $limit_str")->result(),
			'nav'	=> $this->lib->nav(base_url().ADMINPATH.'/home/site_categories/'.$link,5,$nav,$this->limit),
			'start'	=> $start,
			'base_link' =>	base_url().ADMINPATH.'/home/site_categories/',
		);
		$this->template->write_view('content','admin/site_categories_home',$data);
		$this->template->render();
	}

	public function setsessionsearch(){
		if(isset($_POST['Keywords'])){
            $Keywords = mysql_real_escape_string($_POST['Keywords']);
            $this->session->set_userdata("categories_filter_Keywords",$Keywords);
        }
		if(isset($_POST['PagelinksID'])){
            $PagelinksID = mysql_real_escape_string($_POST['PagelinksID']);
            $this->session->set_userdata("categories_filter_PagelinksID",$PagelinksID);
        }
        if(isset($_POST['Published'])){
            $Published = mysql_real_escape_string($_POST['Published']);
            $this->session->set_userdata("categories_filter_Published",$Published);
        }
		$this->search('setsessionsearch');
	}

	public function clearfilter(){
		$this->session->unset_userdata("categories_filter_Keywords");
        $this->session->unset_userdata("categories_filter_PagelinksID");
        $this->session->unset_userdata("categories_filter_Published");
        $this->search('setsessionsearch');
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			if($id>1)
			$this->db->query("delete from ttp_categories where ID=$id");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	

	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add Categories | Config Site');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/site_categories/'
		);
		$this->template->write_view('content','admin/site_categories_add',$data);
		$this->template->render();
	}

	public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Title_en = isset($_POST['Title_en']) ? $this->lib->fill_data($_POST['Title_en']) : '' ;
		$Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '' ;
		$Description_en = isset($_POST['Description_en']) ? $this->security->xss_clean($_POST['Description_en']) : '' ;
		$Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title ;
		$Alias_en = isset($_POST['Alias_en']) ? $_POST['Alias_en'] : $Title_en ;
		$Link = $this->lib->alias($Alias);
		$Link_en = $this->lib->alias($Alias_en);
		$Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
		$PagelinksID = isset($_POST['PagelinksID']) ? $this->lib->fill_data($_POST['PagelinksID']) : 0 ;
		$check = $this->db->query("select * from ttp_categories where Alias='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}
		$check = $this->db->query("select * from ttp_post where Alias='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}
		$check = $this->db->query("select * from ttp_pagelinks where Alias='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}

		$check = $this->db->query("select * from ttp_categories where Alias_en='$Link_en'")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		$check = $this->db->query("select * from ttp_post where Alias_en='$Link_en'")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		$check = $this->db->query("select * from ttp_pagelinks where Alias_en='$Link_en'")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		if($Title!=''){
			$data = array(
				'PagelinksID'	=> $PagelinksID,
				'Published'		=> $Published,
				'Title'			=> $Title,
				'Description'	=> $Description,
				'Title_en'		=> $Title_en,
				'Description_en'=> $Description_en,
				'Alias'			=> $Link,
				'Alias_en'		=> $Link_en,
				'Created'		=> date("Y-m-d H:i:s"),
				'LastEdited'	=> date("Y-m-d H:i:s")
			);
			$arr = array();
			foreach($_POST as $key=>$value){
				if(!array_key_exists($key,$data)){
					$key = trim($key);
					$key = strtolower($key);
					$arr[$key] = $value;
				}
			}
			$arr = json_encode($arr);
			$data['Data'] = $arr;
			$this->db->insert("ttp_categories",$data);
		}
		redirect(ADMINPATH.'/home/site_categories/');
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_categories where ID=$id")->row();
			if(!$result) return;
			$this->template->add_title('Add Page | Config Site');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/site_categories/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/site_categories_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '' ;
		$Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title ;
		$Title_en = isset($_POST['Title_en']) ? $this->lib->fill_data($_POST['Title_en']) : '' ;
		$Description_en = isset($_POST['Description_en']) ? $this->security->xss_clean($_POST['Description_en']) : '' ;
		$Alias_en = isset($_POST['Alias_en']) ? $_POST['Alias_en'] : $Title_en ;
		$Link = $this->lib->alias($Alias);
		$Link_en = $this->lib->alias($Alias_en);
		$Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
		$PagelinksID = isset($_POST['PagelinksID']) ? $this->lib->fill_data($_POST['PagelinksID']) : 0 ;
		$check = $this->db->query("select * from ttp_categories where Alias='$Link' and ID!=$ID")->row();
		if($check){
			$Link = $Link."-".$ID;
		}
		$check = $this->db->query("select * from ttp_post where Alias='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}
		$check = $this->db->query("select * from ttp_pagelinks where Alias='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}

		$check = $this->db->query("select * from ttp_categories where Alias_en='$Link_en' and ID!=$ID")->row();
		if($check){
			$Link_en = $Link_en."-".$ID;
		}
		$check = $this->db->query("select * from ttp_post where Alias_en='$Link_en'")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		$check = $this->db->query("select * from ttp_pagelinks where Alias_en='$Link_en'")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		if($Title!=''){	
			$data = array(
				'PagelinksID'	=> $PagelinksID,
				'Published'		=> $Published,
				'Title'			=> $Title,
				'Description'	=> $Description,
				'Alias'			=> $Link,
				'Title_en'		=> $Title_en,
				'Description_en'=> $Description_en,
				'Alias_en'		=> $Link_en,
				'LastEdited'	=> date("Y-m-d H:i:s")
			);
			$arr = array();
			foreach($_POST as $key=>$value){
				if(!array_key_exists($key,$data) && $key!="ID"){
					$key = trim($key);
					$key = strtolower($key);
					$arr[$key] = $value;
				}
			}
			$arr = json_encode($arr);
			$data['Data'] = $arr;
			$this->db->where("ID",$ID);
			$this->db->update("ttp_categories",$data);
		
		}
		redirect(ADMINPATH.'/home/site_categories/');
	}

}
?>