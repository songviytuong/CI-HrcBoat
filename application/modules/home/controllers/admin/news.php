<?php 
class News extends Admin_Controller { 
 
    public function __construct() { 
        parent::__construct();   
        // Load library template 
        $this->load->library('template'); 
        // Set template 
        $this->template->set_template('admin');  
 
        // Add CSS and JS 
        $this->template->add_css('public/admin/css/style.css'); 
        $this->template->add_js('public/admin/js/jquery-1.7.2.min.js');
		$this->template->add_js('public/admin/js/script_admin.js');
        // Add DocType 
        $this->template->add_doctype(); 
	date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public $limit = 30;
	
	public function index(){
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
		$this->template->add_title('Quản lý bài viết tin tức');
		$nav = $this->db->query("select count(*) as nav from news")->row();
        $nav = $nav ? $nav->nav : 0 ;
		$data=array(
			'data'=>$this->db->query("select * from news order by ID ASC $limit_str")->result(),
			'limit'=>$this->limit,
            'nav'=>$this->lib->nav(base_url().'admin/home/news/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/news_home',$data); 
		$this->template->render();
	}
	
	public function news_add(){
		$this->template->add_title('Add bài tin tức'); 
		$this->template->write_view('content','admin/news_add_new');
		$this->template->render();
	}
	
	public function news_add_new(){
		$accept = array('Title','Title_en','Introtext','Introtext_en','MetaTitle','MetaKeywords','MetaDescription','MetaTitle_en','MetaKeywords_en','MetaDescription_en','Published');
		if(!isset($_POST)) return;
		if(count($_POST)!= count($accept)) return;
		$new_object = $this->db->query("select max(ID) as ID from news")->row();
		$new_id = $new_object ? $new_object->ID+1 : 1 ;
		$data = array(
			'ID'=>$new_id,
			'Introtext'=>$_POST['Introtext'],
			'Introtext_en'=>$_POST['Introtext_en'],
			'Title'=>mysql_real_escape_string($this->input->post('Title')),
			'MetaTitle'=>mysql_real_escape_string($this->input->post('MetaTitle')),
			'MetaKeywords'=>mysql_real_escape_string($this->input->post('MetaKeywords')),
			'MetaDescription'=>mysql_real_escape_string($this->input->post('MetaDescription')),
			'Title_en'=>mysql_real_escape_string($this->input->post('Title_en')),
			'MetaTitle_en'=>mysql_real_escape_string($this->input->post('MetaTitle_en')),
			'MetaKeywords_en'=>mysql_real_escape_string($this->input->post('MetaKeywords_en')),
			'MetaDescription_en'=>mysql_real_escape_string($this->input->post('MetaDescription_en')),
			'Published'=>mysql_real_escape_string($this->input->post('Published')),
			'Created'=>date('Y-m-d H:i:m',time()),
			'LastEdited'=>date('Y-m-d H:i:m',time())
		);
		$this->upload_to = "news/".$new_id;
		if(isset($_FILES['Image_upload'])){
			if($_FILES['Image_upload']['error']==0){
				$id_files = $this->upload_image();
				$data['ImageID'] = $id_files;
			}
		}
		$this->db->insert('news', $data);
		redirect('admin/home/news');
	}
	
	public function update(){
		$accept = array('ID','Title','Title_en','Introtext','Introtext_en','MetaTitle','MetaKeywords','MetaDescription','MetaTitle_en','MetaKeywords_en','MetaDescription_en','Published');
		if(!isset($_POST)) return;
		if(count($_POST)!= count($accept)) return;
		$ID = $this->security->xss_clean($this->input->post('ID'));
		if(!is_numeric($ID)) return ;
		$data = array(
			'Introtext'=>$this->input->post('Introtext'),
			'Introtext_en'=>$this->input->post('Introtext_en'),
			'Title'=>mysql_real_escape_string($this->input->post('Title')),
			'MetaTitle'=>mysql_real_escape_string($this->input->post('MetaTitle')),
			'MetaKeywords'=>mysql_real_escape_string($this->input->post('MetaKeywords')),
			'MetaDescription'=>mysql_real_escape_string($this->input->post('MetaDescription')),
			'Title_en'=>mysql_real_escape_string($this->input->post('Title_en')),
			'MetaTitle_en'=>mysql_real_escape_string($this->input->post('MetaTitle_en')),
			'MetaKeywords_en'=>mysql_real_escape_string($this->input->post('MetaKeywords_en')),
			'MetaDescription_en'=>mysql_real_escape_string($this->input->post('MetaDescription_en')),
			'Published'=>mysql_real_escape_string($this->input->post('Published')),
			'LastEdited'=>date('Y-m-d H:i:m',time())
		);
		$this->upload_to = "news/".$ID;
		if(isset($_FILES['Image_upload'])){
			if($_FILES['Image_upload']['error']==0){
				$id_files = $this->upload_image();
				$data['ImageID'] = $id_files;
				$this->delete_file_by_id_file($object->ImageID);
			}
		}
		$this->db->where("ID",$ID);
        $this->db->update("news",$data);
        redirect('admin/home/news');
	}
	
	public function delete($id){
		if(isset($id)){
			if(is_numeric($id)){
				$this->db->query("delete from news where ID=$id");
			}
		}
		redirect('admin/home/news');
	}
	
	public function edit($id){
		if(isset($id)){
			if(is_numeric($id)){
				$this->template->add_title('Edit bài viết tin tức'); 
				$object = $this->db->query("select * from news where ID = $id")->row();
				if(!$object) return;
				$file = $this->db->query("select * from files where ID=$object->ImageID")->row();
				$data = array(
					'data'=>$object,
					'Image'=>$file
				);
				$this->template->write_view('content','admin/news_edit',$data); 
				$this->template->render();
				return;
			}
		}
		redirect('admin');
	}
	
	
	public function search(){
		if(isset($_POST['keyword'])){
			$keyword = $_POST['keyword'];
			$str = "select * from news";
			$str = $keyword != '' ? $str." where Title like '%".urldecode($keyword)."%'" : $str ;
			$this->template->add_title('Tìm kiếm bài viết');
			$data=array(
				'data'=>$this->db->query($str)->result()
			);
			$this->template->write_view('content','admin/news_home',$data); 
			$this->template->render();
		}
	}
}
?>