<?php 
class Site_staticconfig extends Admin_Controller { 
	public $user;
	public $classname="site_staticconfig";
	public $limit = 10;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$this->template->add_title('Static config | Config Site');
		$object = $this->db->query("select * from ttp_static_config where ID=1")->row();
		$json = $object ? json_decode($object->Data,true) : array() ;
		$json_en = $object ? json_decode($object->Data_en,true) : array() ;
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/site_staticconfig/',
			'data'		=>	$json,
			'data_en'		=>	$json_en,
		);
		$this->template->write_view('content','admin/site_staticconfig_home',$data); 
		$this->template->render();
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Address = isset($_POST['Address']) ? $this->lib->fill_data($_POST['Address']) : '' ;
		$Certificate = isset($_POST['Certificate']) ? $this->lib->fill_data($_POST['Certificate']) : '' ;
		$Hotline = isset($_POST['Hotline']) ? $this->lib->fill_data($_POST['Hotline']) : '' ;
		$Copyright = isset($_POST['Copyright']) ? $this->lib->fill_data($_POST['Copyright']) : '' ;
		$Analytics = isset($_POST['Analytics']) ? $this->lib->fill_data($_POST['Analytics']) : '' ;
		$data = array(
			'Address'		=> $Address,
			'Title'			=> $Title,
			'Certificate'	=> $Certificate,
			'Hotline'		=> $Hotline,
			'Copyright'		=> $Copyright,
			'Analytics'		=> $Analytics
		);
		$this->db->where("ID",1);
		$this->db->update("ttp_static_config",array("Data"=>json_encode($data)));	
		redirect(ADMINPATH.'/home/site_staticconfig/');
	}

	public function update_en(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Address = isset($_POST['Address']) ? $this->lib->fill_data($_POST['Address']) : '' ;
		$Certificate = isset($_POST['Certificate']) ? $this->lib->fill_data($_POST['Certificate']) : '' ;
		$Hotline = isset($_POST['Hotline']) ? $this->lib->fill_data($_POST['Hotline']) : '' ;
		$Copyright = isset($_POST['Copyright']) ? $this->lib->fill_data($_POST['Copyright']) : '' ;
		$data = array(
			'Address'		=> $Address,
			'Title'			=> $Title,
			'Certificate'	=> $Certificate,
			'Hotline'		=> $Hotline,
			'Copyright'		=> $Copyright
		);
		$this->db->where("ID",1);
		$this->db->update("ttp_static_config",array("Data_en"=>json_encode($data)));	
		redirect(ADMINPATH.'/home/site_staticconfig/');
	}

}
?>