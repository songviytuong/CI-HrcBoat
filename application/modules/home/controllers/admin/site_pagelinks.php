<?php 
class Site_pagelinks extends Admin_Controller { 
	public $user;
	public $classname="site_pagelinks";
	public $limit = 10;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Page list | Config Site');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_pagelinks")->row();
        $nav = $nav ? $nav->nav : 0 ;
		$object = $this->db->query("select * from ttp_pagelinks order by STT ASC $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/site_pagelinks/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/site_pagelinks/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/site_pagelinks_home',$data); 
		$this->template->render();
	}

	public function search(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		if(isset($_GET['keyword'])){
			$keyword = $this->lib->fill_data($_GET['keyword']);
			$str = "select * from ttp_pagelinks";
			$str = $keyword != '' ? $str." where Title like '%".urldecode($keyword)."%' or Alias like '%".urldecode($keyword)."%'" : $str ;
			$this->template->add_title('Tìm kiếm dữ liệu');
			$data=array(
				'data'	=> $this->db->query($str)->result(),
				'nav'	=> '',
				'start'	=> 0,
				'base_link' =>	base_url().ADMINPATH.'/home/site_pagelinks/',
			);
			$this->template->write_view('content','admin/site_pagelinks_home',$data);
			$this->template->render();
			return;
		}
		redirect(ADMINPATH.'/home/site_pagelinks/');
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$page = $this->db->query("select * from ttp_pagelinks where ID=$id")->row();
			if($page){
				$this->db->query("update ttp_pagelinks set STT=STT-1 where STT>$page->STT");
			}
			$this->db->query("delete from ttp_pagelinks where ID=$id");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	

	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add Page | Config Site');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/site_pagelinks/'
		);
		$this->template->write_view('content','admin/site_pagelinks_add',$data);
		$this->template->render();
	}

	public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Title_en = isset($_POST['Title_en']) ? $this->lib->fill_data($_POST['Title_en']) : '' ;
		$Classname = isset($_POST['ClassName']) ? $this->lib->fill_data($_POST['ClassName']) : '' ;
		$Link = isset($_POST['Link']) ? $this->lib->fill_data($_POST['Link']) : '' ;
		$Link = $this->lib->alias($Link);
		$Link_en = isset($_POST['Link_en']) ? $this->lib->fill_data($_POST['Link_en']) : '' ;
		$Link_en = $this->lib->alias($Link_en);
		$MetaTitle = isset($_POST['MetaTitle']) ? $this->lib->fill_data($_POST['MetaTitle']) : '' ;
		$MetaTitle = $MetaTitle=="" ? $Title : $MetaTitle ;
		$MetaTitle_en = isset($_POST['MetaTitle_en']) ? $this->lib->fill_data($_POST['MetaTitle_en']) : '' ;
		$MetaKeywords = isset($_POST['MetaKeywords']) ? $this->lib->fill_data($_POST['MetaKeywords']) : '' ;
		$MetaKeywords_en = isset($_POST['MetaKeywords_en']) ? $this->lib->fill_data($_POST['MetaKeywords_en']) : '' ;
		$MetaDescription = isset($_POST['MetaDescription']) ? $this->lib->fill_data($_POST['MetaDescription']) : '' ;
		$MetaDescription_en = isset($_POST['MetaDescription_en']) ? $this->lib->fill_data($_POST['MetaDescription_en']) : '' ;
		$MetaExt = isset($_POST['MetaExt']) ? $this->lib->fill_data($_POST['MetaExt']) : '' ;
		$MetaExt_en = isset($_POST['MetaExt_en']) ? $this->lib->fill_data($_POST['MetaExt_en']) : '' ;
		$Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
		$STT = isset($_POST['STT']) ? $this->lib->fill_data($_POST['STT']) : 0 ;
		$check = $this->db->query("select * from ttp_pagelinks where Alias='$Link' or Alias_en='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}
		$check = $this->db->query("select * from ttp_post where Alias='$Link' or Alias_en='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}
		$check = $this->db->query("select * from ttp_categories where Alias='$Link' or Alias_en='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}

		$check = $this->db->query("select * from ttp_pagelinks where Alias='$Link_en' or Alias_en='$Link_en'")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		$check = $this->db->query("select * from ttp_post where Alias='$Link_en' or Alias_en='$Link_en'")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		$check = $this->db->query("select * from ttp_categories where Alias='$Link_en' or Alias_en='$Link_en'")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		if($Title!='' && $Classname!='' && $Published!=''){
			$data = array(
				'STT'			=> $STT+1,
				'Classname'		=> $Classname,
				'Published'		=> $Published,
				'Title'			=> $Title,
				'Title_en'		=> $Title_en,
				'Alias'			=> $Link,
				'Alias_en'		=> $Link_en,
				'MetaTitle'		=> $MetaTitle,
				'MetaKeywords'	=> $MetaKeywords,
				'MetaDescription'=> $MetaDescription,
				'MetaTitle_en'	=> $MetaTitle_en,
				'MetaKeywords_en'=> $MetaKeywords_en,
				'MetaDescription_en'=> $MetaDescription_en,
				'MetaExt'		=> $MetaExt,
				'MetaExt_en'	=> $MetaExt_en,
				'Created'		=> date("Y-m-d H:i:s"),
				'LastEdited'	=> date("Y-m-d H:i:s")
			);
			$this->db->query("update ttp_pagelinks set STT=STT+1 where STT > $STT");
			$this->db->insert("ttp_pagelinks",$data);
		}
		redirect(ADMINPATH.'/home/site_pagelinks/');
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_pagelinks where ID=$id")->row();
			if(!$result) return;
			$this->template->add_title('Add Page | Config Site');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/site_pagelinks/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/site_pagelinks_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Title_en = isset($_POST['Title_en']) ? $this->lib->fill_data($_POST['Title_en']) : '' ;
		$Classname = isset($_POST['ClassName']) ? $this->lib->fill_data($_POST['ClassName']) : '' ;
		$Link = isset($_POST['Link']) ? $this->lib->fill_data($_POST['Link']) : '' ;
		$Link_en = isset($_POST['Link_en']) ? $this->lib->fill_data($_POST['Link_en']) : '' ;
		$MetaTitle = isset($_POST['MetaTitle']) ? $this->lib->fill_data($_POST['MetaTitle']) : '' ;
		$MetaTitle = $MetaTitle=="" ? $Title : $MetaTitle ;
		$MetaTitle_en = isset($_POST['MetaTitle_en']) ? $this->lib->fill_data($_POST['MetaTitle_en']) : '' ;
		$MetaKeywords = isset($_POST['MetaKeywords']) ? $this->lib->fill_data($_POST['MetaKeywords']) : '' ;
		$MetaKeywords_en = isset($_POST['MetaKeywords_en']) ? $this->lib->fill_data($_POST['MetaKeywords_en']) : '' ;
		$MetaDescription = isset($_POST['MetaDescription']) ? $this->lib->fill_data($_POST['MetaDescription']) : '' ;
		$MetaDescription_en = isset($_POST['MetaDescription_en']) ? $this->lib->fill_data($_POST['MetaDescription_en']) : '' ;
		$MetaExt = isset($_POST['MetaExt']) ? $this->lib->fill_data($_POST['MetaExt']) : '' ;
		$MetaExt_en = isset($_POST['MetaExt_en']) ? $this->lib->fill_data($_POST['MetaExt_en']) : '' ;
		$Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
		$STT = isset($_POST['STT']) ? $this->lib->fill_data($_POST['STT']) : 0 ;
		$check = $this->db->query("select * from ttp_pagelinks where (Alias='$Link' or Alias_en='$Link') and ID!=$ID")->row();
		if($check){
			$Link = $Link."-".$ID;
		}
		$check = $this->db->query("select * from ttp_post where Alias='$Link' or Alias_en='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}
		$check = $this->db->query("select * from ttp_categories where Alias='$Link' or Alias_en='$Link'")->row();
		if($check){
			$Link = $Link."-".time();
		}

		$check = $this->db->query("select * from ttp_pagelinks where (Alias='$Link_en' or Alias_en='$Link_en') and ID!=$ID")->row();
		if($check){
			$Link_en = $Link_en."-".$ID;
		}
		$check = $this->db->query("select * from ttp_post where (Alias='$Link_en' or Alias_en='$Link_en')")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		$check = $this->db->query("select * from ttp_categories where (Alias='$Link_en' or Alias_en='$Link_en')")->row();
		if($check){
			$Link_en = $Link_en."-".time();
		}
		if($Title!='' && $Classname!='' && $Published!=''){
			$page = $this->db->query("select * from ttp_pagelinks where ID=$ID")->row();
			if($page){
				if($page->STT==$STT){
					$STT = $STT;
				}else{
					$STT=$STT+1;
					$this->db->query("update ttp_pagelinks set STT=STT+1 where STT >= $STT");
				}
				$data = array(
					'STT'			=> $STT,
					'Classname'		=> $Classname,
					'Published'		=> $Published,
					'Title'			=> $Title,
					'Title_en'		=> $Title_en,
					'Alias'			=> $Link,
					'Alias_en'		=> $Link_en,
					'MetaTitle'		=> $MetaTitle,
					'MetaKeywords'	=> $MetaKeywords,
					'MetaDescription'=> $MetaDescription,
					'MetaTitle_en'	=> $MetaTitle_en,
					'MetaKeywords_en'=> $MetaKeywords_en,
					'MetaDescription_en'=> $MetaDescription_en,
					'MetaExt'		=> $MetaExt,
					'MetaExt_en'	=> $MetaExt_en,
					'LastEdited'	=> date("Y-m-d H:i:s")
				);
				$this->db->where("ID",$ID);
				$this->db->update("ttp_pagelinks",$data);
			}
			
		}
		redirect(ADMINPATH.'/home/site_pagelinks/');
	}

}
?>