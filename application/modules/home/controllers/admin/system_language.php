<?php 
class System_language extends Admin_Controller { 
	public $user;
	public $classname="system_language";
	public $limit = 10;
	public $tablename = 'ttp_define';

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        if($this->user->IsAdmin!=1){
                $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
                if(!is_numeric($start)) $start=0;
                $this->template->add_title('Language Setting | Config System');
                $limit_str = "limit $start,$this->limit";
                $nav = $this->db->query("select count(1) as nav from $this->tablename where del_flg = 0 and `group`='language' and `type`='site' and `lang`='vi'")->row();
                $nav = $nav ? $nav->nav : 0 ;
		$object = $this->db->query("select id,code,name,del_flg from $this->tablename where del_flg = 0 and `group`='language' and `type`='site' and `lang`='vi' order by position ASC $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/system_language/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/system_language/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/system_language_home',$data); 
		$this->template->render();
	}
        
        public function add(){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Language Setting | Config System');
            $data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/system_language/'
		);
            $this->template->write_view('content','admin/system_language_add',$data); 
            $this->template->render();
        }
        
        public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$name = isset($_POST['name']) ? $this->lib->fill_data($_POST['name']) : '' ;
		$code = isset($_POST['code']) ? $this->lib->fill_data($_POST['code']) : '' ;
		$position = isset($_POST['position']) ? $this->lib->fill_data($_POST['position']) : '' ;
		$del_flg = isset($_POST['del_flg']) ? $this->lib->fill_data($_POST['del_flg']) : '' ;
                
		if($name!='' && $code!='' && $del_flg!=''){
			$data = array(
				'name'	=> $name,
				'code'	=> $code,
				'group'	=> 'language',
				'type'	=> 'site',
				'lang'	=> 'vi',
				'position'	=> $position,
				'del_flg'	=> $del_flg
			);
			$this->db->insert($this->tablename,$data);
		}
		redirect(ADMINPATH.'/home/system_language/');
	}

}
?>