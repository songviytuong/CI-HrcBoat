<?php 
class System_group extends Admin_Controller {

    public $user;
    public $classname = "group";
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Group Setting | Config System');
        $limit_str = "limit $start,$this->limit";
        
        $this->load->model('conf_group', 'cfg');
        $object = $this->cfg->getListGroup();
        $nav = $this->cfg->getCountListGroup();
        $nav = $nav ? $nav->nav : 0;
        $data = array(
                'base_link' =>	base_url().ADMINPATH.'/home/system_group/',
                'data'		=>	$object,
                'start'		=>	$start,
                'slist'		=>	"False",
                'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/system_group/index',5,$nav,$this->limit)
        );
        $this->template->write_view('content','admin/system_group',$data); 
        $this->template->render();
    }
    
    public function slist($code) {
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Group Setting | Config System');
        $limit_str = "limit $start,$this->limit";
        $nav = 0;
        $object = $this->conf_define->get_list_by_group($code);
        $data = array(
                'base_link' =>	base_url().ADMINPATH.'/home/system_group/',
                'data'		=>	$object,
                'start'		=>	$start,
                'slist'		=>	"True",
                'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/system_group/slist'.$code,5,$nav,$this->limit)
        );
        $this->template->write_view('content','admin/system_group',$data); 
        $this->template->render();
    }
}

?>