<?php 
class Contact extends CI_Controller { 
    public $limit = 20;
    public function __construct() { 
        parent::__construct();
        $this->load->library('template'); 
        $this->template->set_template('site');
        $staticconfig = $this->db->query("select * from ttp_static_config where ID=1")->row();
        $lang = $this->uri->segment(1);
        $lang = $lang=="en" ? "en" : "vi" ;
        $datafooter = array(
            "static"=>$staticconfig,
            'lang'  =>$lang
        );
        $this->template->write_view('header','header',array('lang'=>$lang));
        $this->template->write_view('footer','footer',$datafooter);
        $this->template->add_doctype();
	}

	
	public function index($lang='vi')
        {
            die($lang);
        }

    }