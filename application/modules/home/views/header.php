<header class="header">
    <nav class="navbar main-nav">
        <div class="container">
            <?php 
            $link = $lang=="en" ? "en" : "" ;
            ?>
            <div class="logo sm-logo visible-xs"><a href="<?php echo $link ?>"><span><?php echo $lang=="en" ? "Tran Toan Phat" : "Trần Toàn Phát" ; ?></span></a></div>
            <div class="navbar-header">
              <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" type="button" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <?php 
                if($lang=="vi"){
                    $menu = $this->db->query("select Title,Alias,Classname from ttp_pagelinks where Published=1 and Classname!='index' order by STT ASC limit 0,6")->result();     
                }else{
                    $menu = $this->db->query("select Title_en as Title,Alias_en as Alias,Classname from ttp_pagelinks where Published=1 and Classname!='index' order by STT ASC limit 0,6")->result();
                }
                if(count($menu)>0){
                    $i=1;
                    foreach($menu as $row){
                        $row->Alias = $lang=="en" ? "en/".$row->Alias : $row->Alias ;
                        if($i==4){
                            echo '<li class="logo hidden-xs"><a href="'.$link.'" title="Trần Toàn Phát">Trần Toàn Phát</a></li>';
                        }
                        echo "<li id='menu_$row->Classname'><a href='$row->Alias'>$row->Title</a></li>";
                        $i++;
                    }
                }
                ?>
              </ul>
            </div>
        </div>
    </nav>
    <!-- End main-nav-->
</header>