<script>
    var d = document.getElementById("menu_about");
    d.className = d.className + "current-menu-item";
</script>
<?php 
  if($lang=="en"){
      $pagelink = $this->uri->segment(2);
  }else{
      $pagelink = $this->uri->segment(1);
  }
?>
<div class="content about-page">
      <div class="top-slide hidden-xs">
      	<?php 
      	$headerimage = $this->db->query("select * from ttp_banner where PositionID=2 order by ID DESC limit 0,1")->row();
      	if($headerimage){
      		echo file_exists($headerimage->Thumb) ? "<img src='$headerimage->Thumb' alt='' class='img-responsive'>" : "";
      	}
      	?>
      	
      </div>
      <!-- top-slide-->
      <?php 
        if($lang=="en"){
            $about = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%' limit 0,1")->row();
        }else{
            $about = $this->db->query("select a.ID,a.Title,a.Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%' limit 0,1")->row();
        }
        
        if($about){
            echo '<div class="content-box">
            <div class="container">
              <h3 class="box-title"><span>'.$about->Title.'</span><i class="hidden-xs">'.$about->Description.'</i></h3>';
            if($lang=="en"){
                $result = $this->db->query("select Thumb,Introtext_en as Introtext,Title_en as Title from ttp_post where CategoriesID=$about->ID and Published=1")->result();
            }else{
                $result = $this->db->query("select Thumb,Introtext,Title from ttp_post where CategoriesID=$about->ID and Published=1")->result();
            }
            if(count($result)>0){
                foreach($result as $row){
                  $image = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" alt="'.$row->Title.'" class="img-responsive center-block">' : "" ;
                  echo '<div class="row">
                            <div class="video-embed col-xs-12 col-sm-5">'.$image.'</div>
                            <div class="col-xs-12 col-sm-7">
                              '.$row->Introtext.'
                            </div>
                      </div>';
                }
            }
            echo '</div></div>';
        }

        if($lang=="en"){
            $achievement = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block2\"%' limit 0,1")->row();
        }else{
            $achievement = $this->db->query("select a.ID,a.Title,a.Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block2\"%' limit 0,1")->row();
        }
        
        if($achievement){
            echo '<div class="content-box about-achievement-box">
                    <div class="container">
                      <h3 class="box-title"><span>
                          '.$achievement->Title.'</span><i class="hidden-xs">'.$achievement->Description.'</i></h3>
                      <div class="timeline"><dl>';
            if($lang=="en"){
                $result = $this->db->query("select Thumb,Description_en as Description,Title_en as Title,Data,Alias_en as Alias from ttp_post where CategoriesID=$achievement->ID and Published=1 order by ID DESC")->result();
            }else{
                $result = $this->db->query("select Thumb,Description,Title,Data,Alias from ttp_post where CategoriesID=$achievement->ID and Published=1 order by ID DESC")->result();
            }
            if(count($result)>0){
                $arr = array();
                $i=0;
                foreach ($result as $row) {
                    $json = json_decode($row->Data);
                    if($lang=="en"){
                        $link = isset($json->changelink_en) ? $json->changelink_en : $row->Alias ;  
                    }else{
                        $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
                    }
                    if(strrpos($link,"http://")!=0){
                        $link = $lang=="en" ? "en/".$link : $link ;
                    }
                    if(isset($json->year)){
                        if(isset($arr["$json->year"])){
                            if(isset($json->number) && isset($json->timeline)){
                                $class= $i%2==0 ? "right" : "left" ;
                                $img = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" class="events-object img-circle img-responsive">' : "" ;
                                $arr[$json->year][$json->number] = '
                                <dd class="pos-'.$class.' clearfix">
                                  <div class="circ"></div>
                                  <div class="time">'.$json->timeline.'</div>
                                  <div class="events">
                                    <div class="pull-left col-xs-4"> '.$img.' </div>
                                    <div class="events-body col-xs-8">
                                      <h4 class="events-heading"> <a href="'.$link.'">'.$row->Title.'</a></h4>
                                      <p>'.$row->Description.'</p>
                                    </div>
                                  </div>
                                </dd>';
                                $i++;
                            }
                        }else{
                            $arr[$json->year] = array();
                            if(isset($json->number) && isset($json->timeline)){
                                $class= $i%2==0 ? "right" : "left" ;
                                $img = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" class="events-object img-circle img-responsive">' : "" ;
                                $arr[$json->year][$json->number] = '
                                <dd class="pos-'.$class.' clearfix">
                                  <div class="circ"></div>
                                  <div class="time">'.$json->timeline.'</div>
                                  <div class="events">
                                    <div class="pull-left col-xs-4"> '.$img.' </div>
                                    <div class="events-body col-xs-8">
                                      <h4 class="events-heading"> <a href="'.$link.'">'.$row->Title.'</a></h4>
                                      <p>'.$row->Description.'</p>
                                    </div>
                                  </div>
                                </dd>';
                                $i++;
                            }
                        }
                    }
                }
            }
            $year = $lang=="en" ? "Year" : "Năm" ;
            foreach($arr as $key=>$value){
                echo "<dt>$year $key</dt>";
                ksort($value);
                echo implode("",$value);
            }
            $grow = $lang=="en" ? GROW_EN : GROW_VI ;
            echo "<dt>$grow</dt></dl></div></div></div>";
        }
      
        if($lang=="en"){
            $core = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block3\"%' limit 0,1")->row();
        }else{
            $core = $this->db->query("select a.ID,a.Title,a.Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block3\"%' limit 0,1")->row();
        }
        
        if($core){
            $corevalue = $lang=="en" ? "Core value" : "Giá trị cốt lõi" ;
            echo '<div class="content-box about-core-box"><div class="container"><h3 class="box-title"><span>'.$core->Title.'</span><i class="hidden-xs">'.$core->Description.'</i></h3><div class="row">';
            echo '<div class="col-xs-6 item hidden-xs hidden-sm"><a> <b>6</b><span>'.$corevalue.'</span></a></div>';
            if($lang=="en"){
                $result = $this->db->query("select Thumb,Introtext_en as Introtext,Title_en as Title,Data,Alias_en as Alias from ttp_post where CategoriesID=$core->ID and Published=1")->result();
            }else{
                $result = $this->db->query("select Thumb,Introtext,Title,Data,Alias from ttp_post where CategoriesID=$core->ID and Published=1")->result();
            }
            
            if(count($result)>0){
                $i=1;
                foreach($result as $row){
                    $json = json_decode($row->Data);
                    echo '<div class="col-xs-4 item"><a> <b>'.$i.' </b><span>'.$row->Title.'</span><i class="fa fa-plus-circle"></i></a>
                      <div class="tooltip">
                        '.$row->Introtext.'
                      </div>
                    </div>';
                    $i++;
                }
            }
            echo "</div></div></div>";
        } 

        if($lang=="en"){
            $vision = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block4\"%' limit 0,1")->row();
        }else{
            $vision = $this->db->query("select a.ID,a.Title,a.Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block4\"%' limit 0,1")->row();
        }
        if($vision){
            echo '<div class="content-box about-vision-box"><div class="container"><h3 class="box-title"><span>'.$vision->Title.'</span><i class="hidden-xs">'.$vision->Description.'</i></h3><div class="row">';
            if($lang=="en"){
                $result = $this->db->query("select Introtext_en as Introtext,Title_en as Title from ttp_post where CategoriesID=$vision->ID and Published=1")->result();
            }else{
                $result = $this->db->query("select Introtext,Title from ttp_post where CategoriesID=$vision->ID and Published=1")->result();
            }
            if(count($result)>0){
                foreach($result as $row){
                    echo '<blockquote class="col-xs-11 col-sm-7 center-block">
                      <h4>'.$row->Title.'</h4>
                      '.$row->Introtext.'
                    </blockquote>';
                }
            }
            echo "</div></div></div>";
        }

        if($lang=="en"){
            $block5 = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block5\"%' limit 0,1")->row();
        }else{
            $block5 = $this->db->query("select a.ID,a.Title,a.Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block5\"%' limit 0,1")->row();
        }
        
        if($block5){
            echo '<div class="content-box ttp-testimonial"><div class="container"><h3 class="box-title"><span>'.$block5->Title.'</span><i class="hidden-xs">'.$block5->Description.'</i></h3><div class="row">';
            if($lang=="en"){
                $result = $this->db->query("select Introtext_en as Introtext,Title_en as Title,Thumb,Description_en as Description from ttp_post where CategoriesID=$block5->ID and Published=1")->result();
            }else{
                $result = $this->db->query("select Introtext,Title,Thumb,Description from ttp_post where CategoriesID=$block5->ID and Published=1")->result();
            }
            if(count($result)>0){
                foreach($result as $row){
                    if(file_exists($row->Thumb)){
                        echo '<div class="col-xs-12 col-sm-4 item"><img src="'.$row->Thumb.'" alt="'.$row->Title.'" class="img-responsive">
                          <div class="meta"> 
                            <h4><strong>'.$row->Title.'</strong><span>'.$row->Description.'</span></h4>
                            '.$row->Introtext.'
                          </div>
                        </div>';
                    }
                }
            }
            echo "</div></div></div>";
        }

        if($lang=="en"){
            $footerimage = $this->db->query("select Data_en as Data,Thumb from ttp_banner where PositionID=6 order by ID DESC")->row();
        }else{
            $footerimage = $this->db->query("select * from ttp_banner where PositionID=6 order by ID DESC")->row();
        }
        
        if($footerimage){
            $image = file_exists($footerimage->Thumb) ? "url('$footerimage->Thumb') top center no-repeat" : "";
            $json = json_decode($footerimage->Data);
            $description = isset($json->Description) ? $json->Description : "" ;
            echo '<div class="bottom-quote" style="background:'.$image.'">
                      <div class="container">
                          <div class="meta col-xs-8 col-sm-6 col-md-4 center-block">
                          '.$description.'
                        </div>
                      </div>
                  </div>';
            
        }

      ?>
      
      
    </div>