<?php 
  $arr_ID = "$data->ID";
?>
<div class="content detail-page detail-gallery-page">
      <div class="container">
        <div class="row">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i></a></li>
            <?php 
            if($data->CategoriesID>0){
                if($lang=="en"){
                    $categories = $this->db->query("select Title_en as Title,Alias_en as Alias from ttp_categories where ID=$data->CategoriesID")->row();
                }else{
                    $categories = $this->db->query("select Title,Alias from ttp_categories where ID=$data->CategoriesID")->row();
                }
                if($categories){
                    $link = $lang=="en" ? 'en/'.$categories->Alias : $categories->Alias ;
                    echo "<li><a href='$link'>$categories->Title</a></li>";
                }
            }
            ?>
            <li class="active"><?php echo $data->Title ?></li>
          </ol>
          <div class="main-content col-xs-12 col-md-8">
            <div class="detail-content">
              <h3 class="post-title"><?php echo $data ? $data->Title : "" ; ?></h3>
              <div class="top-meta">
                <div class="meta-info">
                  <p><span class="post-date"><?php echo $data ? date('d/m/Y H:i:s',strtotime($data->Created)) : "" ; ?></span></p>
                </div>
                <div class="default-share"></div>
              </div>
              <div class="post-content-wrap" style="text-align: justify;">
                <p class="post-excerpt"><?php echo $data->Description ?></p>
                <?php
                $json = json_decode($data->Data);
                if(isset($json->video)){
                    $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9" style="margin-bottom:15px"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'" allowfullscreen></iframe></div>' : "" ;
                    echo $video;
                }
                ?>
                <div class="post-content">
                  <?php echo str_replace(' src=',' class="img-thumbnail img-responsive" src=',$data->Introtext); ?>
                  <?php 
                    if(isset($json->album)){
                        if(is_numeric($json->album)){
                          $result = $this->db->query("select * from ttp_album where ID=$json->album")->row();
                          if($result){
                              $IDfile = json_decode($result->Data,true);
                              $listid = count($IDfile)>0 ? implode(",",$IDfile) : "0" ;
                              $file = $this->db->query("select * from ttp_images where ID in($listid)")->result();
                              if(count($file)>0){
                                  echo '<div class="gallery-content">';
                                  foreach ($file as $row) {
                                      $key = array_search($row->ID,$IDfile);
                                      $thumb = $this->lib->getfilesize($row->Thumb,270,151);
                                      $IDfile[$key] = '<div class="col-lg-3 col-sm-4 col-xs-6">
                                              <a href=""><img alt="'.$data->Title.'" src="'.$thumb.'" data-img="'.$row->Thumb.'" class="img-thumbnail img-responsive"></a>
                                            </div>';
                                  }
                                  echo implode("",$IDfile);
                                  echo "</div>";
                              }
                          }
                        }
                    }
                   ?>
                </div>
              </div>
              <div class="post-source-tags">
                <!--<div class="post-small-box"><span>Nguồn bài viết</span><a href="#">VH Magazine</a></div>-->
                  <?php 
                  if($data->Tags!=''){
                      $data->Tags = str_replace("[","",$data->Tags);
                      $data->Tags = str_replace("]","",$data->Tags);
                      $tags = $data->Tags;
                      if($tags!=''){
                          $result = $this->db->query("select * from ttp_tags where ID in($tags)")->result();
                          if(count($result)>0){
                              echo '<ul class="post-tags post-small-box list-unstyled"><li><span>TAGS</span></li>';
                              foreach($result as $item){
                                  echo "<li><a href='$item->Alias'>$item->Title</a></li>";
                              }
                              echo '</ul>';
                          }
                      }
                  }
                  ?>
              </div>
            </div>
            <!-- End detail-content-->
            <div class="relate-post main-content-box">
              <h3 class="blog-box-title"><span><?php echo $lang=="en" ? RELATED_EN : RELATED_VI ; ?></span></h3>
              <div class="row">
                <?php 
                if($lang=="en"){
                    $relative = $this->db->query("select Title_en as Title,Alias_en as Alias,Thumb,ID,Data,Description_en as Description from ttp_post where Published=1 and CategoriesID=$data->CategoriesID and Thumb!='' and ID!=$data->ID order by ID DESC limit 0,3")->result();
                    if(count($relative)==0){
                        $relative = $this->db->query("select Title_en as Title,Alias_en as Alias,Thumb,ID,Data,Description_en as Description from ttp_post where Published=1 and Thumb!='' and ID!=$data->ID order by ID DESC limit 0,3")->result();  
                    }
                }else{
                    $relative = $this->db->query("select * from ttp_post where Published=1 and CategoriesID=$data->CategoriesID and Thumb!='' and ID!=$data->ID order by ID DESC limit 0,3")->result();
                    if(count($relative)==0){
                        $relative = $this->db->query("select * from ttp_post where Published=1 and Thumb!='' and ID!=$data->ID order by ID DESC limit 0,3")->result();  
                    }
                }
                if(count($relative)>0){
                    foreach ($relative as $row) {
                        $json = json_decode($row->Data);
                        $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
                        $link = $lang=="en" ? 'en/'.$link : $link ;
                        $image = $this->lib->getfilesize($row->Thumb,270,151);
                        echo '<div class="col-xs-4 post-item"><a href="'.$link.'" class="post-img"><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a>
                          <h4><a href="'.$link.'">'.$row->Title.'</a></h4>
                          <p class="hidden-xs">'.$this->lib->splitString($row->Description,100).'</p>
                        </div>';
                        $arr_ID.=",".$row->ID;
                    }
                }
                ?>
                
              </div>
            </div>
            <!-- End relate-post		-->
          </div>
          <div class="sidebar col-xs-12 col-md-4">
            <div class="search-form sidebar-box">
              <div class="form-group no-gutters">
                <form method="post" action="<?php echo $lang=="en" ? base_url()."en/" : base_url(); ?>search">
                <label for="" class="control-label"><?php echo $lang=="en" ? SEARCH_EN : SEARCH_VI ; ?>:								</label>
                <input type="text" placeholder="<?php echo $lang=="en" ? SEARCH_EN : SEARCH_VI ; ?>" class="search-input form-control" name="keywords" required>
                <input type="hidden" name="lang" value="<?php echo $lang ?>" />
                <input type="submit" value="" class="search-button fa">
                </form>
              </div>
              <!-- End search-form-->
            </div>
            <?php 
            if($lang=="en"){
                $hot = $this->db->query("select a.Title_en as Title,a.Alias_en as Alias,a.Thumb,a.Data,a.ID from ttp_post a where a.Published=1 and a.Data like '%\"hot\":\"true\"%' order by a.LastEdited DESC")->row();
            }else{
                $hot = $this->db->query("select a.Title,a.Alias,a.Thumb,a.Data,a.Alias,a.ID from ttp_post a where a.Published=1 and a.Data like '%\"hot\":\"true\"%' order by a.LastEdited DESC")->row();
            }
            if($hot){
                $json = json_decode($hot->Data);
                $link = isset($json->changelink) ? $json->changelink : $hot->Alias ;
                $link = $lang=="en" ? 'en/'.$link : $link ;
                $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'" allowfullscreen></iframe></div>' : "" ;
                if($video==''){
                    $video = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" alt="'.$row->Title.'" class="img-responsive">' : "" ;
                }
                $tieudiem = $lang=="en" ? FOCAL_EN : FOCAL_VI ;
                echo '<div class="sidebar-box">
                      <h3 class="sidebar-title">'.$tieudiem.'</h3>
                      '.$video.'
                      <h4><a href="'.$link.'">'.$hot->Title.'</a></h4>
                    </div>';
            }
            ?>
            
            <div class="sidebar-box">
              <h3 class="sidebar-title"><?php echo $lang=="en" ? FEATURE_EN : FEATURE_VI ; ?></h3>
              <div class="row">
              <?php 
              if($lang=="en"){
                  $featured = $this->db->query("select Title_en as Title,Thumb,Description_en as Description,ID,Alias_en as Alias,Data from ttp_post where Thumb!='' and Published=1 and ID not in($arr_ID) and Data like '%\"featured\":\"true\"%' order by ID DESC limit 0,5")->result();
                  if(count($featured)==0){
                      $featured = $this->db->query("select Title_en as Title,Thumb,Description_en as Description,ID,Alias_en as Alias,Data from ttp_post where Thumb!='' and Published=1 and ID not in($arr_ID) order by View DESC limit 0,5")->result();
                  }
              }else{
                  $featured = $this->db->query("select * from ttp_post where Thumb!='' and Published=1 and ID not in($arr_ID) and Data like '%\"featured\":\"true\"%' order by ID DESC limit 0,5")->result();
                  if(count($featured)==0){
                      $featured = $this->db->query("select * from ttp_post where Thumb!='' and Published=1 and ID not in($arr_ID) order by View DESC limit 0,5")->result();
                  }
              }

              if(count($featured)>0){
                  foreach ($featured as $row) {
                      $json = json_decode($row->Data);
                      $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
                      $link = $lang=="en" ? "en/".$link : $link ;
                      $image = $this->lib->getfilesize($row->Thumb,113,63);
                      echo '<div class="item col-xs-6 col-md-12 post-item"><a href="'.$link.'" class="post-img"><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a>
                            <h5><a href="'.$link.'">'.$row->Title.'</a></h5>
                          </div>';
                  }
              }
              ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="galleryModal" role="dialog" class="modal gallery-modal">
        <div class="modal-dialog">
          <div class="modal-content">
            <button type="button" data-dismiss="modal" class="close">×</button>
            <div class="modal-body">
              <div id="modalCarousel" class="carousel">
                <div class="carousel-inner"></div><a href="#modalCarousel" data-slide="prev" class="carousel-control left"><i class="fa fa-angle-left"></i></a><a href="#modalCarousel" data-slide="next" class="carousel-control right"><i class="fa fa-angle-right"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>