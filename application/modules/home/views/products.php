<script>
    var d = document.getElementById("menu_products");
    d.className = d.className + "current-menu-item";
</script>
<?php 
  if($lang=="en"){
      $pagelink = $this->uri->segment(2);
  }else{
      $pagelink = $this->uri->segment(1);
  }
?>
<div class="content service-page">
      <div class="top-slide hidden-xs">
          <?php 
          $headerimage = $this->db->query("select * from ttp_banner where PositionID=3 order by ID DESC limit 0,1")->row();
          if($headerimage){
            echo file_exists($headerimage->Thumb) ? "<img src='$headerimage->Thumb' alt='' class='img-responsive'>" : "";
          }
          ?>
      </div>
      <!-- top-slide-->
      <?php 
      $visitwebsite = $lang=="en" ? GOTO_EN : GOTO_VI ;
      if($lang=="en"){
          $ecom = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%' limit 0,1")->row();
      }else{
          $ecom = $this->db->query("select a.ID,a.Title,a.Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%' limit 0,1")->row();
      }
      if($ecom){
          echo '<div class="content-box service-function-box"><div class="container">
          <h3 class="box-title"><span>'.$ecom->Title.'</span><i class="hidden-xs">'.$ecom->Description.'</i></h3>
          <div class="row">';
          if($lang=="en"){
              $result = $this->db->query("select Title_en as Title,Data,Description_en as Description from ttp_post where CategoriesID=$ecom->ID and Published=1")->result();
          }else{
              $result = $this->db->query("select Title,Data,Description from ttp_post where CategoriesID=$ecom->ID and Published=1")->result();
          }
          if(count($result)>0){
              foreach($result as $row){
                  $json = json_decode($row->Data);
                  $icon = isset($json->icon) ? $json->icon : "" ;
                  echo '<div class="col-xs-12 col-sm-4 text-center">'.$icon.'
                          <h4>'.$row->Title.'</h4>
                          <p>'.$row->Description.'</p>
                        </div>';
              }
          }
          echo "</div></div>";
      }
      
      if($lang=="en"){
          $visit = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description,a.Data,a.Thumb,a.Alias_en as Alias from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%'")->result();
      }else{
          $visit = $this->db->query("select a.ID,a.Title,a.Description,a.Data,a.Thumb,a.Alias from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%'")->result();
      }
      if(count($visit)>0){
          echo '<div class="content-box service-visit-box"><div class="container">';
          foreach($visit as $row){
              $json = json_decode($row->Data);
              if($lang=="en"){
                  $link = isset($json->changelink_en) ? $json->changelink_en : $row->Alias ;  
              }else{
                  $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
              }
              if(strrpos($link,"http://")!=0){
                  $link = $lang=="en" ? "en/".$link : $link ;
              }
              $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'"></iframe></div>' : "" ;
              if($video==''){
                  $video = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" alt="'.$row->Title.'" class="img-responsive">' : "" ;
              }
              echo '<div class="row">
                    <div class="col-xs-12 col-sm-6">'.$video.'</div>
                    <div class="col-xs-12 col-sm-6">
                      <h4>'.$row->Title.'</h4>
                      '.$row->Description.'
                      <a href="'.$link.'" class="btn btn-lg btn-primary">'.$visitwebsite.'</a>
                    </div>
                  </div>';
          }
          echo "</div></div>";
      }

      if($lang=="en"){
          $adiva = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block2\"%' limit 0,1")->row();
      }else{
          $adiva = $this->db->query("select a.ID,a.Title,a.Description from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block2\"%' limit 0,1")->row();
      }
      if($adiva){
          echo '<div class="content-box service-adiva-box"><div class="container">
          <h3 class="box-title"><span>'.$adiva->Title.'</span><i class="hidden-xs">'.$adiva->Description.'</i></h3>
          <div class="row">';
          if($lang=="en"){
              $result = $this->db->query("select Thumb,Introtext_en as Introtext,Title_en as Title,Data,Alias_en as Alias from ttp_post where CategoriesID=$adiva->ID and Published=1")->result();
          }else{
              $result = $this->db->query("select Thumb,Introtext,Title,Data,Alias from ttp_post where CategoriesID=$adiva->ID and Published=1")->result();
          }
          if(count($result)>0){
              foreach($result as $row){
                  $json = json_decode($row->Data);
                  if($lang=="en"){
                      $link = isset($json->changelink_en) ? $json->changelink_en : $row->Alias ;  
                  }else{
                      $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
                  }
                  if(strrpos($link,"http://")!=0){
                      $link = $lang=="en" ? "en/".$link : $link ;
                  }
                  $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'"></iframe></div>' : "" ;
                  if($video==''){
                      $video = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" alt="'.$row->Title.'" class="img-responsive">' : "" ;
                  }
                  echo '<div class="col-xs-12 col-sm-5">'.$video.'</div>
                        <div class="col-xs-12 col-sm-7">
                            '.$row->Introtext.'
                        </div>';
              }
          }
          echo "</div></div></div>";
      }
      
      if($lang=="en"){
          $visit = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description,a.Data,a.Thumb,a.Alias_en as Alias from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block2\"%'")->result();
      }else{
          $visit = $this->db->query("select a.ID,a.Title,a.Description,a.Data,a.Thumb,a.Alias from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block2\"%'")->result();
      }
      if(count($visit)>0){
          echo '<div class="content-box service-visit-box"><div class="container">';
          foreach($visit as $row){
              $json = json_decode($row->Data);
              if($lang=="en"){
                  $link = isset($json->changelink_en) ? $json->changelink_en : $row->Alias ;  
              }else{
                  $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
              }
              if(strrpos($link,"http://")!=0){
                  $link = $lang=="en" ? "en/".$link : $link ;
              }
              $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'"></iframe></div>' : "" ;
              if($video==''){
                  $video = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" alt="'.$row->Title.'" class="img-responsive">' : "" ;
              }
              echo '<div class="row">
                    <div class="col-xs-12 col-sm-6">'.$video.'</div>
                    <div class="col-xs-12 col-sm-6">
                      <h4>'.$row->Title.'</h4>
                      '.$row->Description.'
                      <a href="'.$link.'" class="btn btn-lg btn-primary">'.$visitwebsite.'</a>
                    </div>
                  </div>';
          }
          echo "</div></div>";
      }

      if($lang=="en"){
          $footerimage = $this->db->query("select Data_en as Data,Thumb from ttp_banner where PositionID=6 order by ID DESC")->row();
      }else{
          $footerimage = $this->db->query("select * from ttp_banner where PositionID=6 order by ID DESC")->row();
      }
      
      if($footerimage){
          $image = file_exists($footerimage->Thumb) ? "url('$footerimage->Thumb') top center no-repeat" : "";
          $json = json_decode($footerimage->Data);
          $description = isset($json->Description) ? $json->Description : "" ;
          echo '<div class="bottom-quote" style="background:'.$image.'">
                    <div class="container">
                        <div class="meta col-xs-8 col-sm-6 col-md-4 center-block">
                        '.$description.'
                      </div>
                    </div>
                </div>';
          
      }
      ?>
    </div>