<div class="content 404-page">
      <div class="content-box">
        <div class="container">
          <div class="row">
            <div class="box404 text-center">
              <h3>Không tìm thấy nội dung mà bạn yêu cầu</h3>
              <p>Hãy sử dụng chức năng liên hệ phía trên. Hoặc yêu cầu hỗ trợ.</p>
              <p></p><a href="" class="btn btn-primary">Về trang chủ</a><a href="" class="btn btn-default">Hỗ trợ</a>
            </div>
          </div>
        </div>
      </div>
    </div>