<script>
    var d = document.getElementById("menu_family");
    d.className = d.className + "current-menu-item";
</script>
<?php 
  if($lang=="en"){
      $pagelink = $this->uri->segment(2);
  }else{
      $pagelink = $this->uri->segment(1);
  }
  $arr_hot = '0';
?>
<div class="content blog-page">
      <div class="content-box">
        <div class="container">
          <div class="row">
            <?php 
              if($lang=="en"){
                  $slide = $this->db->query("select a.ID,a.Title_en as Title,a.Alias_en as Alias,a.Thumb from ttp_post a,ttp_pagelinks b where b.Alias_en='$pagelink' and a.PagelinksID=b.ID and b.Published=1 and a.Published=1 and a.Data like '%\"useslide\":\"true\"%' order by a.ID DESC")->result();
              }else{
                  $slide = $this->db->query("select a.ID,a.Title,a.Alias,a.Thumb from ttp_post a,ttp_pagelinks b where b.Alias='$pagelink' and a.PagelinksID=b.ID and b.Published=1 and a.Published=1 and a.Data like '%\"useslide\":\"true\"%' order by a.ID DESC")->result();
              }
              if(count($slide)>0){
                  echo '<div id="carousel-slider" data-ride="carousel" class="carousel slide blog-slider col-xs-12 col-md-8">       
              <div role="listbox" class="carousel-inner">';
                  $i=1;
                  foreach ($slide as $row) {
                      if(file_exists($row->Thumb)){
                        $image = $this->lib->getfilesize($row->Thumb,770,430);
                        $active = $i==1 ? "active" : "";
                        $link = $lang=="en" ? "en/".$row->Alias : $row->Alias ;
                        echo '<div class="item '.$active.'"><a href="'.$link.'" class="slide-img img-responsive"><img src="'.$image.'" alt="'.$row->Title.'"></a>
                                <div class="carousel-caption">
                                  <h3><a href="'.$link.'">'.$row->Title.'</a></h3>
                                </div>
                              </div>';
                        $arr_hot.=",$row->ID";
                      }
                      $i++;
                  }
                  echo '</div>
                  <a href="#carousel-slider" role="button" data-slide="prev" class="left carousel-control"><span aria-hidden="true" class="fa fa-angle-left"></span><span class="sr-only">Previous</span></a>
                  <a href="#carousel-slider" role="button" data-slide="next" class="right carousel-control"><span aria-hidden="true" class="fa fa-angle-right"></span><span class="sr-only">Next</span></a>
                  </div>';
              }
            ?>
            
            <div class="sidebar col-xs-12 col-md-4">					
              <div class="search-form sidebar-box">
                <div class="form-group no-gutters">
                  <form method="post" action="<?php echo base_url()."search" ?>">
                    <label for="" class="control-label"><?php echo $lang=="en" ? SEARCH_EN : SEARCH_VI ; ?>:               </label>
                    <input type="text" placeholder="<?php echo $lang=="en" ? SEARCH_EN : SEARCH_VI ; ?>" class="search-input form-control" name="keywords" required>
                    <input type="hidden" name="lang" value="<?php echo $lang ?>" />
                    <input type="submit" value="" class="search-button fa">
                  </form>
                </div>
                <!-- End search-form-->
              </div>
              <div class="sidebar-box">
                <?php 
                if($lang=="en"){
                    $hot = $this->db->query("select a.Title_en as Title,a.Alias_en as Alias,a.Thumb,a.Data,a.ID from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"hot\":\"true\"%' order by a.LastEdited DESC")->row();
                }else{
                    $hot = $this->db->query("select a.Title,a.Alias,a.Thumb,a.Data,a.Alias,a.ID from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"hot\":\"true\"%' order by a.LastEdited DESC")->row();
                }
                if($hot){
                    $arr_hot .= ",$hot->ID";
                    $json = json_decode($hot->Data);
                    $link = isset($json->changelink) ? $json->changelink : $hot->Alias ;
                    $link = $lang=="en" ? "en/".$link : $link ;
                    $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'" allowfullscreen></iframe></div>' : "" ;
                    if($video==''){
                        $video = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" alt="'.$row->Title.'" class="img-responsive">' : "" ;
                    }
                    $tieudiem = $lang=="en" ? FOCAL_EN : FOCAL_VI ;
                    echo '<h3 class="sidebar-title">'.$tieudiem.'</h3>'.$video.'
                    <h4><a href="'.$link.'">'.$hot->Title.'</a></h4>';    
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>


      <?php 
      $xemtatca = $lang=="en" ? VIEWALL_EN : VIEWALL_VI ;
      if($lang=="en"){
          $videoalbum = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description,a.Alias_en as Alias from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block3\"%'")->row();
      }else{
          $videoalbum = $this->db->query("select a.ID,a.Title,a.Description,a.Alias from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block3\"%'")->row();
      }
      if($videoalbum){
          $videolink = $lang=="en" ? "en/".$videoalbum->Alias : $videoalbum->Alias ;
          echo '<div class="content-box blog-video-box"><div class="container">
          <h3 class="blog-box-title"><span>'.$videoalbum->Title.'</span><a href="'.$videolink.'" class="read-more">'.$xemtatca.'</a></h3><div class="row">';
          if($lang=="en"){
              $result = $this->db->query("select Title_en as Title,Data,Description_en Description,Alias_en as Alias,Thumb from ttp_post where CategoriesID=$videoalbum->ID and Published=1 order by ID DESC limit 0,6")->result();
          }else{
              $result = $this->db->query("select Title,Data,Description,Alias,Thumb from ttp_post where CategoriesID=$videoalbum->ID and Published=1 order by ID DESC limit 0,6")->result();
          }
          if(count($result)>0){
              $i=1;
              foreach($result as $row){
                  $json = json_decode($row->Data);
                  $link = isset($json->link) ? $json->link : $row->Alias ;
                  $link = $lang=="en" ? "en/".$link : $link ;
                  if($i==1){
                      $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'" allowfullscreen></iframe></div>' : "" ;
                      echo '<div class="col-md-8 feature-post">'.$video.'
                        <h4><a href="'.$link.'">'.$row->Title.'</a></h4>
                      </div>';
                  }else{
                    $image = $this->lib->getfilesize($row->Thumb,113,63);
                    if($i==2){
                      echo '<div class="col-md-4 post-content">';
                    }
                    echo '<div class="post-item"><a href="'.$link.'" class="post-img"><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a>
                            <h5><a href="'.$link.'">'.$row->Title.'</a></h5>
                          </div>';
                  }
                  $i++;
              }
              if($i>1) echo "</div>";
          }
          echo '</div></div></div>';
      }

      if($lang=="en"){
          $news = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description,a.Alias_en as Alias from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%'")->row();
      }else{
          $news = $this->db->query("select a.ID,a.Title,a.Description,a.Alias from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%'")->row();
      }
      if($news){
          $newslink = $lang=="en" ? "en/".$news->Alias : $news->Alias ;
          echo '<div class="content-box"><div class="container">
          <h3 class="blog-box-title"><span>'.$news->Title.'</span><a href="'.$newslink.'" class="read-more">'.$xemtatca.'</a></h3><div class="row">';
          if($lang=="en"){
              $result = $this->db->query("select Title_en as Title,Data,Description_en as Description,Alias_en as Alias,Thumb from ttp_post where CategoriesID=$news->ID and Published=1 and ID not in($arr_hot) order by ID DESC limit 0,8")->result();
          }else{
              $result = $this->db->query("select Title,Data,Description,Alias,Thumb from ttp_post where CategoriesID=$news->ID and Published=1 and ID not in($arr_hot) order by ID DESC limit 0,8")->result();
          }
          if(count($result)>0){
              $i=1;
              foreach($result as $row){
                  $json = json_decode($row->Data);
                  $link = isset($json->link) ? $json->link : $row->Alias ;
                  $link = $lang=="en" ? "en/".$link : $link ;
                  $image = $this->lib->getfilesize($row->Thumb,270,151);
                  echo '<div class="col-xs-6 col-sm-4 col-md-3 post-item"><a href="'.$link.'" class="post-img"><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a>
                          <h4><a href="'.$link.'">'.$this->lib->splitString($row->Title,55).'</a></h4>
                          <p class="hidden-xs">'.$this->lib->splitString($row->Description,100).'</p>
                        </div>';
                  if(($i%4+1)==1){
                    echo "</div><div class='row'>";
                  }
                  $i++;
              }
          }
          echo '<div class="clearfix"></div></div></div></div>';
      }

      if($lang=="en"){
          $album = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description,a.Alias_en as Alias from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block2\"%'")->row();
      }else{
          $album = $this->db->query("select a.ID,a.Title,a.Description,a.Alias from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block2\"%'")->row();
      }
      if($album){
          $albumlink = $lang=="en" ? "en/".$album->Alias : $album->Alias ;
          echo '<div class="content-box blog-gallery-box"><div class="container">
          <h3 class="blog-box-title"><span>'.$album->Title.'</span><a href="'.$albumlink.'" class="read-more">'.$xemtatca.'</a></h3><div class="row">';
          if($lang=="en"){
              $result = $this->db->query("select Title_en as Title,Data,Description_en as Description,Alias_en as Alias,Thumb from ttp_post where CategoriesID=$album->ID and Published=1 and ID not in($arr_hot) order by ID DESC limit 0,6")->result();
          }else{
              $result = $this->db->query("select Title,Data,Description,Alias,Thumb from ttp_post where CategoriesID=$album->ID and Published=1 and ID not in($arr_hot) order by ID DESC limit 0,6")->result();
          }
          if(count($result)>0){
              $i=1;
              foreach($result as $row){
                  $json = json_decode($row->Data);
                  $link = isset($json->link) ? $json->link : $row->Alias ;
                  $link = $lang=="en" ? "en/".$link : $link ;
                  if($i<=2){
                      $image = $this->lib->getfilesize($row->Thumb,770,430);
                      echo '<div class="col-xs-6 col-sm-4"><a href="'.$link.'"><span>'.$row->Title.'</span><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a></div>';
                  }else{
                    if($i==3){
                      echo '<div class="clearfix visible-xs"></div>';
                    }
                    $image = $this->lib->getfilesize($row->Thumb,270,151);
                    echo '<div class="col-xs-3 col-sm-2"><a href="'.$link.'"><span>'.$row->Title.'</span><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a></div>';
                  }
                  $i++;
              }
          }
          echo '</div></div></div>';
      }

      ?>
      </div>
    </div>