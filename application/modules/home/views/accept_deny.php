<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class="header"><i class="fa fa-exclamation-triangle"></i> MESSAGE FROM SYSTEM</div>
<title>Warning access !</title>
<div class="content">
	<div class="block1">
		<img src='<?php echo base_url().'public/admin/images/fire.png' ?>' />
	</div>
	<div class="block2">
		<h1>403 ACCESS DENY</h1>
		<p>You don't have permission to connect this app. Your ip not exists in access list ip from this app . </p>
		<small>Hotline support : <i>1900555552</i> | ext : 922</small>
	</div>
</div>
<div class="footer"> <p>Copyright 2016 All Rights Reserved. Privacy and Terms .</p></div>
<style>
	*{margin:0px;padding:0px;font-family: arial;color:#333;}
	.header{width:100%;padding:15px 0px;background: #AC1F2D;color:#FFF;text-align: center;font-size: 15px}
	.header i{color:#FFF;margin-right:5px;font-size: 17px}
	.content{width:100%;overflow:hidden;padding:50px 0px;}
	.content .block1{width:40%;float:left;text-align: center;padding-right:1%;}
	.content .block2{width:40%;float:left;padding-left:1%;border-left:1px solid #ccc;padding:90px 2%;}
	.content .block2 h1{margin-bottom:10px;}
	.content .block2 p{margin-bottom:10px;line-height: 24px;}
	.content .block2 h2{}
	.footer{position: fixed;left:0px;bottom:0px;right:0px;width:100%;padding:10px 0px;font-size: 11px;font-family: tahoma;background:#222;text-align:right}
	.footer p{color:#999;margin-right:15px;}
</style>