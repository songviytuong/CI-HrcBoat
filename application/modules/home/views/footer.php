<?php 
    if($lang=="en"){
        $json = $static ? json_decode($static->Data_en) : (object) array() ;
    }else{
        $json = $static ? json_decode($static->Data) : (object) array() ;  
    }
?>    
    <div class="bottom-function">
        <div id="scrollUp" class="scroll-up"><i class="fa fa-angle-up fa-2x"></i></div>
    </div>
    <footer>
      <div class="container">
        <div class="row">
          <div class="foot-group foot-info col-sm-9">
            <div class="row">
              <div class="col-xs-12 col-sm-4"><a href="" class="foot-logo">Trần Toàn Phát</a>
                <?php echo isset($json->Title) ? "<h4>".$json->Title."</h4>" : "<h4></h4>"; ?>
              </div>
              <div class="col-xs-12 col-sm-8">
                <div class="footer-social">
                  <p><span><?php echo $lang=="en" ? SHARE_EN : SHARE_VI ; ?>: <a href="https://www.facebook.com/tinhchatlamdep.adiva"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/Adiva_Collagen"><i class="fa fa-twitter"></i></a><a href="https://www.pinterest.com/collagenadiva/"><i class="fa fa-pinterest"></i></a><a href="https://www.youtube.com/channel/UCqbWT2K-mvBmqn1akWEOg7g"><i class="fa fa-youtube"></i></a><a href="https://plus.google.com/107873205962568374836/about"><i class="fa fa-google-plus"></i></a></span></p>
                </div>
                <?php 
                  echo "<p>";
                  echo $lang=="en" ? ADDRESS_EN : ADDRESS_VI ;
                  echo isset($json->Address) ? ": ".$json->Address : "";
                  echo "</p>";
                  echo isset($json->Certificate) ? "<p>".$json->Certificate."</p>" : "<p></p>";
                  echo isset($json->Hotline) ? "<h5>Hotline: <b>".$json->Hotline."</b></h5>" : "<p></p>";
                ?>
              </div>
            </div>
          </div>
          <div class="foot-group foot-subcript hidden-xs col-sm-3">
            <h3 class="foot-title"><?php echo $lang=="en" ? NEWS_EN : NEWS_VI ; ?></h3>
            <p class="hidden-xs"><?php echo $lang=="en" ? REGISDESCRIPTION_EN : REGISDESCRIPTION_VI ; ?></p>
            <div class="subcript-form">
              <form method="post" action="<?php echo base_url()."save-email" ?>">
                <p class="input-group">
                  <input type="text" placeholder="<?php echo $lang=="en" ? EMAILADDRESS_EN : EMAILADDRESS_VI ; ?>" class="form-control" name="Email" required><span class="input-group-btn">
                    <button type="submit" class="btn btn-primary">Đăng ký</button></span>
                </p>
              </form>
            </div>
          </div>
        </div>
        <div class="foot-bottom">    
          <p class="pull-left"><?php echo isset($json->Copyright) ? $json->Copyright : "" ; ?></p>
          <ul class="list-inline pull-right">
            <li><span class="visible-xxs">Ngôn ngữ:</span>
              <?php 
              if($lang=="en"){
                  echo '<div class="btn-group lang-select"><a class="btn btn-default en-flag">Tiếng Anh</a><a href="#" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="" class="vi-flag">Tiếng Việt</a></li>
                    </ul>
                  </div>';
              }else{
                  echo '<div class="btn-group lang-select"><a class="btn btn-default vi-flag">Tiếng Việt</a><a href="#" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="en/" class="en-flag">Tiếng Anh</a></li>
                    </ul>
                  </div>';
              }
              ?>
<!--                  <div class="btn-group lang-select"><a class="btn btn-default vi-flag">Tiếng Việt</a><a href="#" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a class="en-flag">Tiếng Anh</a></li>
                    </ul>
                  </div>-->
            </li>
            <li><a href=""><?php echo $lang=="en" ? POLICY_EN : POLICY_VI ; ?></a></li>
            <li><a href=""><?php echo $lang=="en" ? TERMS_EN : TERMS_VI ; ?></a></li>
          </ul>
        </div>
      </div>
    </footer>