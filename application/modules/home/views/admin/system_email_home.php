<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="">
    <div class="tab_control col-md-12 col-sm-12 col-xs-12">
        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>SENDER</small></a>
        <a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>RECIVER</small></a>
        <a class="col-md-1 col-sm-1 col-xs-4" data="tab3"><small>CONFIG</small></a>
        <a class="col-md-1 col-sm-1 col-xs-4" data="tab4"><small>TEMPLATE</small></a>
    </div>
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="tab2 tabcontent">
                    <div class="x_title">
                        <h2>RECIVER <small> / Send mail to your self</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><button type="submit" class="btn btn-success">Save All</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">Email Reciver (<span class="required">*</span>)</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" class="form-control col-md-12 col-xs-12" name="Email_reciver" value="<?php echo isset($data->Email_reciver) ? $data->Email_reciver : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">Title Mail (<span class="required">*</span>)</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" class="form-control col-md-12 col-xs-12" name="Reciver_title" value="<?php echo isset($data->Reciver_title) ? $data->Reciver_title : "" ; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end RECIVER -->
                <div class="tab1 tabcontent">
                    <div class="x_title">
                        <h2>SENDER <small> / Send mail to client</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><button type="submit" class="btn btn-success">Save All</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">Title Mail (<span class="required">*</span>)</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" required="required" class="form-control col-md-12 col-xs-12" name="Sender_title" value="<?php echo isset($data->Sender_title) ? $data->Sender_title : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">Content send mail (<span class="required">*</span>)</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea class="resizable_textarea form-control ckeditor" name="Sender_content"><?php echo isset($data->Sender_content) ? $data->Sender_content : "" ; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end SEND -->
                <div class="tab3 tabcontent">
                    <div class="x_title">
                        <h2>CONFIG <small> / Config email</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><button type="submit" class="btn btn-success">Save All</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">Protocol</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" class="form-control col-md-12 col-xs-12" name="Protocol" value="<?php echo isset($data->Protocol) ? $data->Protocol : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">SMTP_host</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" class="form-control col-md-12 col-xs-12" name="SMTP_host" value="<?php echo isset($data->SMTP_host) ? $data->SMTP_host : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">SMTP_port</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" class="form-control col-md-12 col-xs-12" name="SMTP_port" value="<?php echo isset($data->SMTP_port) ? $data->SMTP_port : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">SMTP_user</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" class="form-control col-md-12 col-xs-12" name="SMTP_user" value="<?php echo isset($data->SMTP_user) ? $data->SMTP_user : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12">SMTP_password</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="password" class="form-control col-md-12 col-xs-12" name="SMTP_password" value="<?php echo isset($data->SMTP_password) ? $data->SMTP_password : "" ; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end SEND -->
                <div class="tab4 tabcontent">
                    <div class="x_title">
                        <h2>TEMPLATE <small> / Template email</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><button type="submit" class="btn btn-success">Save All</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <a href="public/themes_email/themes_01.html" target="_blank"><img src="public/themes_email/thumb_themes_01.PNG" class="img-responsive" /></a>
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center">
                                <input type="radio" name="Themes" value="themes_01" <?php echo isset($data->Themes) && @$data->Themes=="themes_01" ? "checked='checked'" : "" ; ?>/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end TEMPLATE -->
            </div>
        </div>
        </form>
    </div>
</div>
<style>
    a{cursor: pointer}
</style>

