<div class="">
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel tab1 tabcontent">
                <div class="x_title">
                    <h2>TAGS <small> / Edit tags</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                        <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Title <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="CreateLink" required="required" class="form-control col-md-7 col-xs-12" name="Title" value="<?php echo $data->Title ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tags link (<span class="required">*</span>) </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="AliasLink" required="required" class="form-control col-md-12 col-xs-12" name="Alias" value="<?php echo $data->Alias ?>">
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-12" id='loadingalias' style="padding-top:7px"><i class="fa fa-cog fa-spin"></i> Loading ...</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Published</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Published" value="1" <?php echo $data->Published==1 ? "checked='checked'" : '' ; ?>> &nbsp; Enable &nbsp;
                                        <input type="radio" name="Published" value="0" <?php echo $data->Published==0 ? "checked='checked'" : '' ; ?>> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="x_title" id="title_ext">
                        <h2>OPTION EXTENTSION <small> / Add new option</small></h2>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content"  id="recive_ajax">
                    <?php 
                    $json = json_decode($data->Data,true);
                    if(count($json)>0){
                        foreach($json as $key=>$value){
                            echo '<div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x]</a> '.$key.'</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div>
                                            <textarea class="resizable_textarea form-control" name="'.$key.'" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;">'.$value.'</textarea>
                                    </div>
                                </div>
                            </div>';
                        }        
                    }
                    ?>
                    </div>
                    <div class="x_content">
                        <div class="form-group" id="control_addfield">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Add new field</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="optionname" class="form-control col-md-6 col-xs-12">
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <select class="form-control" id="optionvalue">
                                    <option value="inputtext">Text Field</option>
                                    <option value="textarea">Text Area Field </option>
                                </select>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
            </div>
            
        </div>
        </form>
    </div>
</div>
<style>
    <?php 
    if(count($json)>0){
        echo "#title_ext{display:block;}";
    }else{
        echo "#title_ext{display:none;}";
    }
    ?>
    #addfield{display:none;}
    #control_addfield{display:none;}
    a{cursor: pointer}
    #loadingalias{display:none;}
    #loadingalias_en{display:none;}
</style>
<script>
    $(document).ready(function(){
        $("#addoption").click(function(){
            $("#title_ext").show();
            $("#addfield").show();
            $("#control_addfield").show();
            $(this).hide();
        });

        $("#addfield").click(function(){
            var optionname = $("#optionname").val();
            var optionvalue = $("#optionvalue").val();
            if(optionname!='' && optionvalue!=''){
                $.ajax({
                    url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_option_input/' ?>"+optionname+"/"+optionvalue, 
                    success: function(result){
                        if(result!='fasle'){
                            $("#recive_ajax").append(result);
                            $("#optionname").val('');
                        }
                    }
                });
            }
        });

        $("#CreateLink").change(function(){
            var data = $(this).val();
            getalias(data);
        });

        $("#AliasLink").change(function(){
            var data = $(this).val();
            var title =  $("#CreateLink").val();
            data = data!='' ? data : title ;
            getalias(data);
        });

        $("#CreateLink_en").change(function(){
            var data = $(this).val();
            getalias_en(data);
        });

        $("#AliasLink_en").change(function(){
            var data = $(this).val();
            var title =  $("#CreateLink_en").val();
            data = data!='' ? data : title ;
            getalias_en(data);
        });
    });

    function getalias(path){
        if(path!=''){
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_alias/' ?>",
                dataType: "html",
                type: "POST",
                data: "ob=cat&data="+path,
                beforeSend: function(){
                    $("#loadingalias").show();
                },
                success: function(result){
                    if(result!=false){
                        $("#AliasLink").val(result);
                    }
                    $("#loadingalias").hide();
                }
            });
        }else{
            $("#AliasLink").val('');
        }
    }

    function getalias_en(path){
        if(path!=''){
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_alias/' ?>",
                dataType: "html",
                type: "POST",
                data: "ob=cat&data="+path,
                beforeSend: function(){
                    $("#loadingalias_en").show();
                },
                success: function(result){
                    if(result!=false){
                        $("#AliasLink_en").val(result);
                    }
                    $("#loadingalias_en").hide();
                }
            });
        }else{
            $("#AliasLink_en").val('');
        }
    }

    function removeoption(ob){
        var formgroup = $(ob).parent("label").parent("div");
        formgroup.remove();
    }
</script>
