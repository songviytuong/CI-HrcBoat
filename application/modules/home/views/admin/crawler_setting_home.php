<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>CRAWL <small> / Bot config</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">USERAGENT</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="USERAGENT" value="<?php echo isset($data['USERAGENT']) ? $data['USERAGENT'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">REFERER</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="REFERER" value="<?php echo isset($data['REFERER']) ? $data['REFERER'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">TIMEOUT</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="TIMEOUT" value="<?php echo isset($data['TIMEOUT']) ? $data['TIMEOUT'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Use Proxy</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <?php 
                                        $proxy = isset($data['UseProxy']) ? $data['UseProxy'] : 0 ;
                                        ?>
                                        <input type="radio" name="UseProxy" value="1" <?php echo $proxy==1 ? "checked='checked'" : '' ; ?>> &nbsp; Enable &nbsp;
                                        <input type="radio" name="UseProxy" value="0" <?php echo $proxy!=1 ? "checked='checked'" : '' ; ?>> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">PROXY IP</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="PROXYIP" value="<?php echo isset($data['PROXYIP']) ? $data['PROXYIP'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">PROXY PORT</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="PROXYPORT" value="<?php echo isset($data['PROXYPORT']) ? $data['PROXYPORT'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">PROXY USER</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="PROXYUSER" value="<?php echo isset($data['PROXYUSER']) ? $data['PROXYUSER'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">PROXY PASSWORD</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="PROXYPASSWORD" value="<?php echo isset($data['PROXYPASSWORD']) ? $data['PROXYPASSWORD'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
