<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>LANGUAGE SETTING</h3>
        </div>
        <div class="title_right" style="display:none;">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>MODULE <small> / Add</small></h2>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post" autocomplete="off">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Language Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Alias <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="code" required="required" class="form-control col-md-7 col-xs-12" name="code">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Position
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="position" class="form-control col-md-7 col-xs-12" name="position" style="width:100px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="del_flg" value="1" checked="checked"> &nbsp; Enable &nbsp;
                                        <input type="radio" name="del_flg" value="0"> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                                <button type="button" class="btn btn-danger" onclick="javascript:history.go(-1);">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
