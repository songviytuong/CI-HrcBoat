<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>CRAWL <small> / Add new data destination</small></h2>
                    <div class="clearfix"></div>
                </div>
                <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post" enctype="multipart/form-data">
                    <div class="x_content"> 
                        <div class="form-group">
                            <label class="col-md-6 col-sm-6 col-xs-12">Enter list destination link</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea class="resizable_textarea form-control" id="textarea_data" name="Destination" style="word-wrap: break-word; resize: horizontal; min-height: 300px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <button type="submit" class="btn btn-success">Next Step</button>
                                <button type="button" class="btn btn-primary" id="showbox">Crawl Link</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="crawl_preview_box">
    <div class="crawl_preview_containner">
        <div class="header_crawl_box">
            <h2>CRAWL LINKS</h2>
            <a id="close_crawl_preview"><i class="fa fa-times"></i></a>
        </div>
        <div class="content_crawl_box">
            <div class="reciver_data">
                
            </div>
            <p>Enter your link to crawl destination links</p>
            <input type="text" name="Destination_link" id="Destination_link" class="form-control col-md-12 col-xs-12" style="margin-bottom: 10px;" />
            <button type="button" class="btn btn-primary" id="Crawl_links">Crawl</button>
            <button type="button" class="btn btn-default" id="Clear_crawl_links">Clear</button>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<style>
    .row_links{}
    .row_links a{display:block;padding:3px 0px;border-bottom:1px solid #EEE;color:#999;}
    .row_links a:hover{border-bottom:1px solid #E1e1e1;text-decoration: none;color:#444;}
</style>
<script>

    $("#showbox").click(function(){
        $(".crawl_preview_box").show();
    });

    $("#close_crawl_preview").click(function(){
        $(".crawl_preview_box").hide();
    });

    $("#Clear_crawl_links").click(function(){
        $("#Destination_link").val("");
        $(".reciver_data").html("");
    });

    $("#Crawl_links").click(function(){
        var link = $("#Destination_link").val();
        if(link!=''){
            $.ajax({
                url: "<?php echo $base_link.'/crawl_links_from_string' ?>",
                dataType: "html",
                type: "POST",
                data: "str="+link,
                beforeSend: function(){
                    $(".reciver_data").html("<p>Loading ...</p>");
                },
                success: function(result){
                    if(result!='false'){
                        $(".reciver_data").html(result);
                    }else{
                        $(".reciver_data").html("<p>Not found data from this string .</p>");
                    }
                }
            });
        }
    });

    function uselinks(ob){
        var html = $(ob).html();
        var oldval = $("#textarea_data").val();
        $("#textarea_data").val(oldval+"\n"+html);
        $(ob).remove();
    }
</script>