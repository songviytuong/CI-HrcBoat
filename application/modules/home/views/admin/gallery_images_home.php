<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>GALLERY GROUP
                <small></small>
            </h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>IMAGES <small> / view all images</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a href="<?php echo $base_link . "add" ?>" class="btn btn-success" role="button">Upload image</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php 
                    if(count($data)>0){
                        foreach($data as $row){
                            $img = $this->lib->getfilesize($row->Thumb,270,151);
                            echo "<div class='col-md-2 col-sm-2 col-xs-4 custom'>
                                    <a href='{$base_link}edit/$row->ID'><img src='$img' class='img-responsive' /></a>
                                    <a class='delete_link' href='{$base_link}delete/$row->ID'><i class='fa fa-trash-o'></i></a>
                                </div>";
                        }
                    }
                    ?>
                </div>
                <div class="clearfix"></div>
                <?php 
                    echo $nav;
                ?>
            </div>
        </div>
    </div>
</div>
<style>
    .custom{margin-bottom: 15px; position: relative;overflow:hidden;}
    .custom img{box-sizing:border-box;border:1px solid #E1e1e1;}
    .custom .delete_link{position: absolute;top: -20px;right: 10px;background: #2A3F54;padding: 2px 5px;opacity: 0;transition:all 0.5s;-webkit-transition:all 0.5s;-moz-transition:all 0.5s;}
    .custom:hover .delete_link{opacity: 1;top:0px;}
    .custom .delete_link:hover{background: #eee}
    .custom .delete_link:hover i{color:#2A3F54;}
    .custom .delete_link i{color:#FFF;}
    nav{text-align:center;}
</style>