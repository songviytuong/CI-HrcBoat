<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>CRAWL GROUP
                <small></small>
            </h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'setsessionsearch'; ?>" method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" name="Keywords" placeholder="Search for..." value="<?php echo $this->session->userdata("crawler_filter_Keywords"); ?>" required>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Data <small> / view all data - <?php echo "total <b>(".$find.")</b> records" ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a href="<?php echo $base_link . "add" ?>" class="btn btn-success" role="button">Crawl data</a></li>
                        <li><a href="<?php echo $base_link . "public_data" ?>" class="btn btn-primary" role="button">Public data</a></li>
                    </ul>
                    <div class="col-md-12 col-sm-12 col-xs-12 filters">
                        <form action="<?php echo $base_link."setsessionsearch" ?>" method="POST">
                            <ul>
                                <li><span>Filter records</span></li>
                                <li> 
                                    <select class="form-control" name="Published">
                                        <?php 
                                        $currentpage = $this->uri->segment(4);
                                        $Published = $this->session->userdata("crawler_filter_Published");
                                        ?>
                                        <option value="" <?php echo $Published=='' ? "selected='selected'" : '' ; ?>>-- All status --</option>
                                        <option value="1" <?php echo $Published==1 && is_numeric($Published) ? "selected='selected'" : '' ; ?>> Published </option>
                                        <option value="0" <?php echo $Published==0 && is_numeric($Published) ? "selected='selected'" : '' ; ?>> Available </option>
                                    </select>
                                </li>
                                <li> 
                                    <select class="form-control" name="Site">
                                        <option value="">-- All site --</option>
                                        <?php 
                                        $currentsite = $this->session->userdata("crawler_filter_Site");
                                        $site = $this->db->query("select * from ttp_crawler_site")->result();
                                        if(count($site)>0){
                                            foreach($site as $row){
                                                $selected=$row->Site==$currentsite ? "selected='selected'" : '' ;    
                                                echo "<option value='$row->Site' $selected>$row->Site</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </li>
                                <li><input type="submit" class="btn btn-default" value="Filter" /></li>
                                <li><a class="btn btn-default" href="<?php echo $base_link.'clearfilter' ?>">Clear Filter</a>
                            </ul>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    
                </div>
                <div class="x_content">
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th>STT</th>
                                <th>Destination </th>
                                <th>Created </th>
                                <th>Status </th>
                                <th class=" no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(count($data)>0){
                                $i=$start;
                                foreach($data as $row){ 
                                    $json = json_decode($row->Data,true);
                                    $i++;
                            ?>
                                    <tr class="even pointer">
                                        <td class="a-center ">
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo "<a href='{$base_link}edit/$row->ID'>".$json['Title'].'</a>'; ?><br>
                                            <?php 
                                                $site = explode("/",$row->Destination);
                                                $type = isset($site[2]) ? $site[2] : '' ;
                                                echo isset($json['Categories']) ? "<small>$type » ".str_replace(" / "," » ",$json['Categories'])."</small>" : "<small>$type</small>";
                                            ?>
                                        </td>
                                        <td><?php echo $row->Created; ?></td>
                                        <?php 
                                        switch ($row->Published) {
                                            case 0:
                                                $published="Available";
                                                break;
                                            case 1:
                                                $published="Published";
                                                break;
                                            default:
                                                $published="Empty";
                                                break;
                                        }
                                        ?>
                                        <td class="a-right a-right "><?php echo $published; ?></td>
                                        <td class="last" style="min-width: 150px;">
                                            <a href='<?php echo $base_link . "edit/$row->ID" ?>'><i class="fa fa-pencil-square-o"></i> Edit </a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href='<?php echo $base_link."delete/$row->ID" ?>' class='delete_link'><i class='fa fa-trash-o'></i> Delete</a>
                                        </td>
                                    </tr>
                            <?php 
                                }
                            }else{
                                ?>
                                <tr class="even pointer">
                                    <td class="a-center" colspan="5" style="text-align:center">Data is empty .</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <?php if(count($data)>0) echo $nav; ?>
                </div>
            </div>
        </div>
