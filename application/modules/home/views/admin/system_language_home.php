<div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>LANGUAGE SETTING
                                <small></small>
                            </h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <form action="<?php echo $base_link . 'setsessionsearch'; ?>" method="post">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="Keywords" placeholder="Search for..." required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Go!</button>
                                    </span>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>LANGUAGE <small> / view all language</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="<?php echo $base_link . "add" ?>" class="btn btn-success" role="button"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>STT</th>
                                                <th>Language</th>
                                                <th>Published </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count($data)>0){
                                                $i=$start;
                                                foreach($data as $row){ 
                                                    $i++;
                                            ?>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td><?php echo "<a href='{$base_link}edit/$row->id'>".$row->name.'</a>'; ?></td>
                                                        <td class="a-right a-right "><?php echo $row->del_flg == 0 ? "<i class='fa fa-check'></i> Enable" : "<i class='fa fa-close'></i> Disable" ; ?></td>
                                                        <td class=" last">
                                                            <a href='<?php echo $base_link . "edit/$row->id" ?>'><i class="fa fa-pencil-square-o"></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <?php 
                                                            if($row->id>1){
                                                                echo "<a href='{$base_link}delete/$row->id' class='delete_link'><i class='fa fa-trash-o'></i> Delete</a>";
                                                            } 
                                                            ?>
                                                        </td>
                                                    </tr>
                                            <?php 
                                                }
                                            }else{
                                                echo "<tr><td colspan='7' style='text-align:center'>Data is empty !</td></tr>";
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php if(count($data)>0) echo $nav; ?>
                                </div>
                            </div>
                        </div>

