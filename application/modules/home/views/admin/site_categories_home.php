<div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>SITE GROUP
                                <small></small>
                            </h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <form action="<?php echo $base_link . 'setsessionsearch'; ?>" method="post">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="Keywords" placeholder="Search for..." value="<?php echo $this->session->userdata("categories_filter_Keywords"); ?>" required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Go!</button>
                                    </span>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>CATEGORIES <small> / view all categories</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="<?php echo $base_link . "add" ?>" class="btn btn-success" role="button">Add New</a></li>
                                    </ul>
                                    <div class="col-md-12 col-sm-12 col-xs-12 filters">
                                        <form action="<?php echo $base_link."setsessionsearch" ?>" method="POST">
                                            <ul>
                                                <li><span>Filter records</span></li>
                                                <li> 
                                                    <select class="form-control" name="Published">
                                                        <?php 
                                                        $currentpage = $this->uri->segment(4);
                                                        $Published = $this->session->userdata("categories_filter_Published");
                                                        ?>
                                                        <option value="" <?php echo $Published=='' ? "selected='selected'" : '' ; ?>>-- All Published --</option>
                                                        <option value="1" <?php echo $Published==1 && is_numeric($Published) ? "selected='selected'" : '' ; ?>> Enable </option>
                                                        <option value="0" <?php echo $Published==0 && is_numeric($Published) ? "selected='selected'" : '' ; ?>> Disable </option>
                                                    </select>
                                                </li>
                                                <li>
                                                    <select class="form-control" name="PagelinksID">
                                                        <option value="">-- All Page --</option>
                                                        <?php 
                                                        $PagelinksID = $this->session->userdata("categories_filter_PagelinksID");
                                                        $pagelinks = $this->db->query("select Title,ID from ttp_pagelinks")->result();
                                                        if(count($pagelinks)>0){
                                                            foreach($pagelinks as $row){
                                                                if($currentpage=='setsessionsearch'){
                                                                    $selected=$row->ID==$PagelinksID ? "selected='selected'" : '' ;    
                                                                }else{
                                                                    $selected='';
                                                                }
                                                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </li>
                                                <li><input type="submit" class="btn btn-default" value="Filter" /></li>
                                                <li><a class="btn btn-default" href="<?php echo $base_link.'clearfilter' ?>">Clear Filter</a>
                                            </ul>
                                        </form>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>STT</th>
                                                <th>Title </th>
                                                <th>Page </th>
                                                <th>Created </th>
                                                <th>Published </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count($data)>0){
                                                $i=$start;
                                                foreach($data as $row){ 
                                                    $i++;
                                            ?>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td><?php echo "<a href='{$base_link}edit/$row->ID'>".$row->Title.'</a>'; ?></td>
                                                        <td><?php echo $row->PageTitle; ?></td>
                                                        <td><?php echo $row->Created; ?></td>
                                                        <td class="a-right a-right "><?php echo $row->Published==1 ? "Enable" : "Disable" ; ?></td>
                                                        <td class=" last">
                                                            <a href='<?php echo $base_link . "edit/$row->ID" ?>'><i class="fa fa-pencil-square-o"></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <?php 
                                                            if($row->ID>1){
                                                                echo "<a href='{$base_link}delete/$row->ID' class='delete_link'><i class='fa fa-trash-o'></i> Delete</a>";
                                                            } 
                                                            ?>
                                                        </td>
                                                    </tr>
                                            <?php 
                                                }
                                            }else{
                                                ?>
                                                <tr class="even pointer">
                                                    <td class="a-center" colspan="6" style="text-align:center">Data is empty .</td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php if(count($data)>0) echo $nav; ?>
                                </div>
                            </div>
                        </div>
