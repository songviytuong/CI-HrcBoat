<div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>CLIENT INTERACTION GROUP
                                <small></small>
                            </h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Go!</button>
                                    </span>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>CUSTOMER EMAIL <small> / view all email</small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>STT</th>
                                                <th>Email </th>
                                                <th>Created </th>
                                                <th class=" no-link last"><span class="nobr">Action</span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(count($data)>0){
                                                $i=$start;
                                                foreach($data as $row){ 
                                                    $i++;
                                            ?>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td><?php echo $row->Email; ?></td>
                                                        <td><?php echo $row->Created; ?></td>
                                                        <td class=" last">
                                                            <a href='<?php echo "{$base_link}delete/$row->ID" ?>' class='delete_link'><i class='fa fa-trash-o'></i> Delete</a>
                                                        </td>
                                                    </tr>
                                            <?php 
                                                }
                                            }else{
                                                ?>
                                                <tr class="even pointer">
                                                    <td class="a-center" colspan="4" style="text-align:center">Data is empty .</td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php if(count($data)>0) echo $nav; ?>
                                </div>
                            </div>
                        </div>
