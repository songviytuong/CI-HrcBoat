<div class="">
    <div class="tab_control col-md-12 col-sm-12 col-xs-12">
        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>VIETNAM</small></a>
        <a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>ENGLISH</small></a>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel tab1 tabcontent">
                <div class="x_title">
                    <h2>STATIC <small> / Edit static config</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên công ty</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Title" value="<?php echo isset($data['Title']) ? $data['Title'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Hotline</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Hotline" value="<?php echo isset($data['Hotline']) ? $data['Hotline'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Địa chỉ</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="Address" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data['Address']) ? $data['Address'] : "" ; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Giấy chứng nhận</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="Certificate" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data['Certificate']) ? $data['Certificate'] : "" ; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Copyright</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="Copyright" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data['Copyright']) ? $data['Copyright'] : "" ; ?></textarea>
                            </div>
                        </div>
                        <div class="x_title">
                            <h2>Tracking <small> / Google Analytics</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">GA ID</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Analytics" value="<?php echo isset($data['Analytics']) ? $data['Analytics'] : "" ; ?>" placeholder='Example: UA-61245331-1'>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end VIETNAM --> 
            <div class="x_panel tab2 tabcontent">
                <div class="x_title">
                    <h2>STATIC <small> / Edit static config</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update_en" ?>" method="post">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên công ty</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Title" value="<?php echo isset($data_en['Title']) ? $data_en['Title'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Hotline</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Hotline" value="<?php echo isset($data_en['Hotline']) ? $data_en['Hotline'] : "" ; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Địa chỉ</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="Address" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data_en['Address']) ? $data_en['Address'] : "" ; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Giấy chứng nhận</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="Certificate" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data_en['Certificate']) ? $data_en['Certificate'] : "" ; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Copyright</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="Copyright" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo isset($data_en['Copyright']) ? $data_en['Copyright'] : "" ; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
