<?php 
class Authentication extends CI_Controller {     
	public function __construct() {
	    parent::__construct();
    }	

	public function index(){
		$this->is_login();
		redirect(ADMINPATH. '/report');
	}	

	public function is_login(){		
		$user = $this->session->userdata('ttp_usercp');		
		if($user==''){
			redirect(ADMINPATH. '/login');
		}
	}	

	public function logout(){		
		$this->session->unset_userdata('ttp_usercp');
		$this->session->sess_destroy();
		redirect(ADMINPATH);
	}	

	public function login(){
		$this->load->view('login');
	}	

	public function login_now(){
		if(isset($_POST['username']) && isset($_POST['password'])){			
			$username = $this->security->xss_clean($_POST['username']);			
			$password = $this->security->xss_clean($_POST['password']);
			$token = $this->security->xss_clean($_POST['ttp']);
			$temp = $password;
			$username = mysql_real_escape_string($username);
			$password = mysql_real_escape_string($password);
			$password = sha1($password);
			$result = $this->db->query("select a.ID,a.UserType from ttp_user a,ttp_role b where a.UserName = '$username' and a.Password='$password' and a.Published=1 and b.Published=1 and a.RoleID=b.ID")->row();
			if($result){
				//$this->accept_ip($result->IsAdmin,$token);
				$this->session->set_userdata('ttp_usercp',$username);
				if($temp=='123456'){
					//redirect(ADMINPATH."/home/profile?mustchange=true");
				}
				if($result->UserType==1 || $result->UserType==2 || $result->UserType==3 || $result->UserType==4 || $result->UserType==5 || $result->UserType==7 || $result->UserType==8 || $result->UserType==10){
					redirect(ADMINPATH."/report/import_order");
				}elseif($result->UserType==9){
					redirect(ADMINPATH."/report/warehouse_inventory_export/internal");
				}else{
					if($result->UserType==6){
						redirect(ADMINPATH."/report/warehouse_inventory_perchaseorder/index");
					}else{
						redirect(ADMINPATH);
					}
				}
			}
			redirect(ADMINPATH. '/login/error');
		}
	}

	public function accept_ip($admin=0,$token=''){
        $str = file_get_contents('ipaccept.txt');
        $str = explode('|',$str);
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if(!in_array($ip,$str)){
            if($admin==1 && $token=='false'){
        		return;
        	}
        	redirect('accept_deny');
        }
    }
}
?>