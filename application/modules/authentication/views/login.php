<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trần Toàn Phát | Login</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>public/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/css/animate.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url() ?>public/admin/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>public/admin/css/icheck/flat/green.css" rel="stylesheet">
    <script src="<?php echo base_url() ?>public/admin/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <style>
        .warning{text-align:center;padding:5px 10px;text-align:center;color:#FFF;background: #FD9191}
    </style>
</head>

<body style="background:#F7F7F7;">
    <?php 
      $error = $this->uri->segment(3);
      if($error=='error'){
          echo "<div class='warning'>User hoặc Password không chính xác vui lòng kiểm tra lại !.</div>";
      }
    ?>
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>
        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form method="post" action="<?php echo base_url().ADMINPATH."/check-login" ?>">
                        <h1>Login Form</h1>
                        <div>
                            <input type="text" class="form-control" placeholder="Username" name="username" required id="username" />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" required name="password" id="password" />
                        </div>
                        <div>
                            <input type="submit" name="commit" value="Login" class="btn btn-default submit" />
                            <a class="reset_pass" href="#">Lost your password?</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                          <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1>Tran Toan Phat</h1>

                                <p>©2015 All Rights Reserved. Privacy and Terms</p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>
</body>

</html>