<?php 
class Shop extends CI_Controller { 
    public function __construct() { 
        parent::__construct();
            $this->load->library('template'); 
            $this->template->set_template('shop');
	}
	public function index()
        {
            // get the id_product
            $id_product = $this->input->get('id_product', TRUE);
            // show result
            echo 'My product has the id: '.$id_product;
            $this->template->write_view('content','shop');
            $this->template->render();
        }
        
        public function detail($slug = null,$id = null)
        {
            echo "Slug: ".$slug;
            echo "<br/>";
            echo "Trang ID: ".$id;
        }
}
?>