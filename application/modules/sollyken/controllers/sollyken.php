<?php

class Sollyken extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->load->helper('override');
        $this->template->set_template('sollyken');
    }

    public function index()
    {
        $is_admin = $this->session->userdata("admin");
        if (isset($_GET['admin'])) {
            $is_admin = ($_GET['admin']) ? true : false;
            $this->session->set_userdata("admin", $is_admin);
        }
        $this->load->model("solly", "vou");
        $group = ($this->vou->listGroup() != false) ? $this->vou->listGroup() : array();
        $data = array(
            'data' => $group
        );
        $this->template->write_view('content', 'index', $data);
        $this->template->render();
    }

    public function add()
    {
        $this->load->model("solly", "vou");
        $group = $this->vou->listGroup();
        $data = array(
            'data' => $group
        );
        $this->template->write_view('content', 'add', $data);
        $this->template->render();
    }

    public function listMember($id = null)
    {
        $id = $_POST["group_id"];
        $listMember = $this->db->query("SELECT * FROM sollyken_details WHERE GroupID = $id ORDER BY ID ASC")->result();
        $data = array(
            'data' => $listMember
        );
        $this->load->view('listMember', $data);
    }
    public function resetActive()
    {
        $Class = isset($_POST["Class"]) ? $_POST["Class"] : "";
        if ($Class != "") {
            $arr = array(
                $Class => 1
            );
        } else {
            $arr = array(
                'Active' => 1
            );
        }
        if ($this->db->update("sollyken_details", $arr)) {
            echo "OK";
        }
    }

    public function listPlayer()
    {
        $this->template->write_view('content', 'listPlayer');
        $this->template->render();
    }

    public function listPlayerData()
    {
        $resultParent = $this->db->query("SELECT d.ID as ID, d.Name as Member, g.Name as GroupName, d.ClassA as ClassA, d.ClassE as ClassE, d.ClassF as ClassF, d.ClassG as ClassG, d.ClassH as ClassH, d.ClassJ as ClassJ FROM sollyken_group g inner join sollyken_details d on (g.ID = d.GroupID) ORDER BY d.GroupID")->result();
        $data = array(
            'data' => $resultParent
        );
        $this->load->view('onClass', $data);
    }

    public function onClass()
    {
        $ID = isset($_POST["ID"]) ? $_POST["ID"] : '';
        $Status = isset($_POST["Status"]) ? $_POST["Status"] : '';
        $Class = isset($_POST["Class"]) ? $_POST["Class"] : '';
        if ($Status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $arr = array(
            $Class => $status
        );
        $this->db->where("ID", $ID);
        if ($this->db->update("sollyken_details", $arr)) {
            echo "OK";
        }
    }

    public function activeMember()
    {
        $ID = isset($_POST["ID"]) ? $_POST["ID"] : '';
        $Active = isset($_POST["Active"]) ? $_POST["Active"] : '';
        if ($Active == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $arr = array(
            'Active' => $status
        );
        $this->db->where("ID", $ID);
        if ($this->db->update("sollyken_details", $arr)) {
            echo "OK";
        }
    }
    public function addMember()
    {
        $Name = isset($_REQUEST["Name"]) ? $_REQUEST["Name"] : '';
        $Phone = isset($_REQUEST["Phone"]) ? $_REQUEST["Phone"] : '';
        $GroupID = isset($_REQUEST["group"]) ? $_REQUEST["group"] : '';
        if ($Name != '') {
            $arr = array(
                'Name' => $Name,
                'Phone' => $Phone,
                'GroupID' => $GroupID
            );
            if ($this->db->insert("sollyken_details", $arr)) {
                echo "OK";
            }
        }
    }

    public function updateData()
    {
        $Name = isset($_POST["GroupName"]) ? $_POST["GroupName"] : '';
        $ID = isset($_POST["ID"]) ? $_POST["ID"] : '';
        $arrUpdate = array(
            'Name' => $Name
        );
        $this->db->where('ID', $ID);
        $this->db->update("sollyken_group", $arrUpdate);
        echo "OK";
    }

    public function edit()
    {
        $id = $this->uri->segment(2);
        $result = $this->db->query("SELECT * FROM sollyken_group WHERE ID=$id")->row();
        $data = array(
            'dataEdit' => $result->Name
        );
        $this->template->write_view('content', 'index', $data);
        $this->template->render();
    }

    public function insertData()
    {
        $GroupName = isset($_POST["GroupName"]) ? $_POST["GroupName"] : '';
        if ($GroupName != '') {
            $arr = array(
                'Name' => $_POST["GroupName"]
            );
            if ($this->db->insert("sollyken_group", $arr)) {
                echo "OK";
            }
        } else {
            echo "Empty";
        }
    }

    public function play()
    {
        $data = array(
            'data' => "",
        );
        $this->template->write_view('content', 'play', $data);
        $this->template->render();
    }

    public function random()
    {
        $data = array(
            'data' => ""
        );
        $this->template->write_view('content', 'random', $data);
        $this->template->render();
    }

    public function screate()
    {
        $num = $_POST["Number"];
        if ($num != null) {
            $html = "<form id='frmCreatePlay' class='login-form' method='post'>";
            for ($i = 1; $i <= $num; $i++) {
                $html .= "<input type='text' name='player[]' placeholder='Người chơi $i'/>";
            }
            $html .= "</form>";
            echo $html;
        } else {
            echo "False";
        }
    }

    public function splay()
    {
        $num = isset($_REQUEST["player"]) ? $_REQUEST["player"] : "[]";
        shuffle($num);
        $insertPlay = array(
            'Name' => json_encode($num),
            'Status' => 0
        );
        $this->db->insert("sollyken_player", $insertPlay);
        echo $this->db->insert_id();
    }

    public function getCount()
    {
        $class = $_POST["Class"];
        $count = $this->db->query("SELECT count(*) as count FROM sollyken_details Where Class$class = 1")->row()->count;
        echo $count;
    }

    public function splays()
    {
        $class = isset($_POST["Class"]) ? $_POST["Class"] : '';
        $num = isset($_POST["Number"]) ? $_POST["Number"] : 0;
        if ($class == '') {
            $where = "";
        } else {
            $where = "d.Class" . $class . "=";
        }
        $result = $this->db->query("SELECT d.GroupID as GroupID, d.ID as ID, g.Name as gName, d.Name as Member FROM sollyken_details d inner join sollyken_group g on (d.GroupID = g.ID) WHERE $where 1")->result_array();
        $d = $this->cache->get('_Random');
        if ($d == false) {
            $this->cache->write($result, '_Random', 120);
        }
        $arr = array();
        foreach ($result as $key => $aa) {
            $arr[$key]["gName"] = $aa["gName"];
            $arr[$key]["Member"] = $aa["Member"];
            $arr[$key]["class"] = $aa["GroupID"];
        }

        $dataR = new sortteam();
        $dataR->team = array();
        $dataR->errors = '';
        // if (count($arr) % 2 != 0) {
        //     array_push($arr, [
        //         'gName' => 'xxx',
        //         'Member' => 'xxx',
        //         'class' => 19
        //     ]);
        // }
        $dataR->get_a_b(0, count($arr), $arr);

        // header('Content-Type: application/json');
        // echo json_encode($dataR);
        // exit();

        $class = isset($_POST["Class"]) ? "Class " . $_POST["Class"] : '---';
        $data = array(
            'column' => $num,
            'data' => $dataR->team,
            'class' => $class,
            'error' => $dataR->errors,
        );

        // header('Content-Type: application/json');
        // echo json_encode($data);
        // exit();
        $this->load->view('playTable', $data);
    }

    public function iplayer()
    {
        $playid = $this->uri->segment(2);
        $result = $this->db->query("SELECT * FROM sollyken_player WHERE ID=$playid")->row();
        $abc = json_decode($result->Name);
        $data = array(
            'data' => $abc,
            'playid' => $playid
        );
        $this->template->write_view('content', 'iplayer', $data);
        $this->template->render();
    }

    public function deleteGroup()
    {
        $id = $_GET["ID"];
        $this->db->query("DELETE FROM sollyken_details WHERE GroupID=$id");
        $this->load->model("solly", "vou");
        $this->vou->delGroup($id);
    }

    public function deleteMember()
    {
        $id = $_GET["ID"];
        $this->load->model("solly", "vou");
        $this->vou->delMember($id);
    }
}

ini_set('memory_limit', '9999M');
// ini_set('display_errors', false);
// error_reporting(-1);
// set_error_handler(function ($code, $string, $file, $line) {
//     throw new ErrorException($string, null, $code, $file, $line);
// });
// register_shutdown_function(function () {
//     $error = error_get_last();
//     if (null !== $error) {
//         echo 'ErrorException';
//     }
// });

class sortteam
{
    public $team = array();
    public $errors = '';

    public function get_a_b($min, $max, $arr)
    {
        if (count($arr) < 2) {
            return $this->team;
        }
        $a = rand($min, $max);
        $b = rand($min, $max);
        if (($b - $a) == 1) {
            $this->get_a_b($min, $max, $arr);
        } else {
            if (isset($arr[$a]) && isset($arr[$b])) {
                $classa = $arr[$a]['class'];
                $classb = $arr[$b]['class'];
                $last = isset($this->team[count($this->team) - 1]) ? $this->team[count($this->team) - 1]['class'] : "";
                if ($classa != $classb && $last != $classa) {
                    $this->team[] = $arr[$a];
                    $this->team[] = $arr[$b];
                    unset($arr[$a]);
                    unset($arr[$b]);
                }
                $this->get_a_b($min, $max, $arr);
            } else {
                $this->get_a_b($min, $max, $arr);
            }
        }
        #add
        $this->team = array_unique(array_merge($this->team, $arr), SORT_REGULAR);
        // $this->get_a_b(0, count($this->team), $this->team);
        // $this->team = $this->team + $arr;
        // var_dump($this->team);
        // var_dump($arr);
        // exit();
    }
}
