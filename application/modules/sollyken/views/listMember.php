<table class="table table-striped table-bordered table-hover table-condensed">
    <tr style="background-color: #060; color:#fff;">
        <th class="text-center col-lg-2"></th>
        <th class="text-center col-lg-6">Tên thành viên</th>
        <th class="text-center col-xs-2"><i class="fa fa-undo" rel="" aria-hidden="true" style="cursor:pointer"></i></th>
        <th class="text-center col-xs-2"></th>
    </tr>
    <?php
    $i = 1;
    foreach ($data as $row) {
    ?>
    <tr>
        <td class="text-center"><?=$i;?></td>
        <td class="text-left"><?=$row->Name;?></td>
        <td class="text-center"><?php
        if($row->Active == 0){
            echo "<i class='fa fa-toggle-off activeMember fa-lg' style='cursor:pointer' data-source='".$row->Active."' rel='".$row->ID."'></i>";
        }else{
            echo "<i class='fa fa-toggle-on activeMember fa-lg' style='cursor:pointer' data-source='".$row->Active."' rel='".$row->ID."'></i>";
        }
        ?></td>
        <td class="text-center">
            <i class="fa fa-trash-o text-danger delMember" rel="<?= $row->ID ?>" style="cursor: pointer"></i>
        </td>
    </tr>
    <?php $i++; } ?>
</table>
