<div class="col-lg-8 col-lg-offset-2" style="background-color: #fff; padding-top: 10px;">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <tr style="background-color: #0184ff; color:#fff;">
            <th class="text-center"></th>
            <th class="text-center col-lg-3"><i class="fa fa-user"></i> Người chơi</th>
            <th class="text-center"><i class="fa fa-group"></i> Tên Nhóm</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassA" aria-hidden="true" style="cursor:pointer"></i> ClassA</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassE" aria-hidden="true" style="cursor:pointer"></i> ClassE</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassF" aria-hidden="true" style="cursor:pointer"></i> ClassF</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassG" aria-hidden="true" style="cursor:pointer"></i> ClassG</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassH" aria-hidden="true" style="cursor:pointer"></i> ClassH</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassJ" aria-hidden="true" style="cursor:pointer"></i> ClassJ</th>
        </tr>
        <?php
        $i = 1;
        foreach ($data as $row) {
        ?>
            <tr>
                <td class="text-center"><?= $i; ?></td>
                <td><?= $row->Member ?></td>
                <td><?= $row->GroupName ?></td>
                <td class="text-center text-success">
                    <i class='fa fa-toggle-<?= ($row->ClassA) ? 'on' : 'off' ?> onClass fa-lg' style='cursor:pointer' data-class='ClassA' data-source='<?= $row->ClassA ?>' rel='<?= $row->ID ?>'></i>
                </td>
                <td class="text-center">
                    <i class='fa fa-toggle-<?= ($row->ClassE) ? 'on' : 'off' ?> onClass fa-lg' style='cursor:pointer' data-class='ClassE' data-source='<?= $row->ClassE ?>' rel='<?= $row->ID ?>'></i>
                </td>
                <td class="text-center">
                    <i class='fa fa-toggle-<?= ($row->ClassF) ? 'on' : 'off' ?> onClass fa-lg' style='cursor:pointer' data-class='ClassF' data-source='<?= $row->ClassF ?>' rel='<?= $row->ID ?>'></i>
                </td>
                <td class="text-center">
                    <i class='fa fa-toggle-<?= ($row->ClassG) ? 'on' : 'off' ?> onClass fa-lg' style='cursor:pointer' data-class='ClassG' data-source='<?= $row->ClassG ?>' rel='<?= $row->ID ?>'></i>
                </td>
                <td class="text-center">
                    <i class='fa fa-toggle-<?= ($row->ClassH) ? 'on' : 'off' ?> onClass fa-lg' style='cursor:pointer' data-class='ClassH' data-source='<?= $row->ClassH ?>' rel='<?= $row->ID ?>'></i>
                </td>
                <td class="text-center">
                    <i class='fa fa-toggle-<?= ($row->ClassJ) ? 'on' : 'off' ?> onClass fa-lg' style='cursor:pointer' data-class='ClassJ' data-source='<?= $row->ClassJ ?>' rel='<?= $row->ID ?>'></i>
                </td>
            </tr>
        <?php $i++;
        } ?>
    </table>
    <br />
</div>