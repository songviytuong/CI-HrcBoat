<style>
    .row{
        margin-left:0px;
        margin-right:0px;
    }
    
</style>
<div class="row">
    <?php
    
        $i = 1;
        $j = 1;
        $count = count($data);
        $member = ceil($count / $column);
        $k = 1;
        foreach($data as $row){
            if ($j%$member == 1)
            { 
            echo "<div class='col-lg-4'><table class='table table-striped table-bordered table-hover table-condensed' style='background-color:#fff'>
            <tr>
                <th class='text-center col-lg-1' style='width:40px !important;'></th>
                <th class='text-center col-lg-4' style='background-color: red; color:#fff;'>Bảng ".$i."</th>
                <th class='text-center col-lg-3'></th>
            </tr>
            <tr style='background-color: #060; color:#fff;'>
                <th class='text-center col-lg-2'></th>
                <th class='text-center col-lg-4'>Tên thành viên</th>
                <th class='text-center col-lg-3'>Nhóm</th>
            </tr>";
            $i++;
            }
            echo "<tr><td class='text-center'>".$k."</td><td>".$row["Member"]."</td><td class='text-center' style='font-size:75%; font-weight:bold; padding-top:10px;'>".$row["gName"]."</td></tr>";
            if ($j%$member == 0)
            {
                echo "</table></div>";
                $k = 0;
            }
            $j++;
            $k++;
        }
        if ($j%$member != 1) echo "</div>";
    ?>
</div>
