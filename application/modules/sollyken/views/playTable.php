<style>
    .row {
        margin-left: 0px;
        margin-right: 0px;
    }
</style>
<div class="row">
    <?php
    function partition($list, $p)
    {
        $listlen = count($list);
        $partlen = floor($listlen / $p);
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice($list, $mark, $incr);
            $mark += $incr;
        }
        return $partition;
    }

    $k = 1;
    $newArr = partition($data, $column);
    foreach ($newArr as $key => $row) {
        echo "<div class='col-lg-4 col-md-6 col-xs-12" . $key . "' style='margin-bottom:20px;'><table class='table table-striped table-bordered table-hover table-condensed' style='background-color:#fff'>
        <tr>
            <th class='text-center' style='width:40px !important;'></th>
            <th class='text-center' style='width:50%; background-color: red; color:#fff;'>Bảng " . ($key + 1) . "</th>
            <th class='text-center'>" . $class . "</th>
        </tr>
        <tr style='background-color: #060; color:#fff;'>
            <th></th>
            <th class='text-center'>Tên thành viên</th>
            <th class='text-center'>Nhóm</th>
        </tr>";
        foreach ($row as $c => $item) {
            echo "<tr><td class='text-center'>" . $k . "</td><td>" . $item["Member"] . "</td><td class='text-center' style='font-size:75%; font-weight:bold; padding-top:10px;" . (($item["class"] == (isset($row[$c - 1]) ? $row[$c - 1]["class"] : 0)) ? 'background-color:red;' : '') . "'>" . $item["gName"]  . "</td></tr>";
            $k++;
        }
        echo "</table></div>";
        if (($key + 1) % 3 == 0) {
            echo "</div><div class='row'>";
        }
        $k = 1;
    }
    ?>
</div>