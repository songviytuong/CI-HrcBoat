<style>
    .row {
        margin-left: 0px;
        margin-right: 0px;
    }
</style>
<div class="row">
    <?php
    $count = count($data);
    $member = ceil($count / $column);
    for ($i = 0; $i < $column; $i++) {
    ?>
        <div class="col-lg-4">
            <table class="table table-striped table-bordered table-hover table-condensed">
                <tr>
                    <th class="text-center col-lg-2"></th>
                    <th class="text-center col-lg-6" style="background-color: red; color:#fff;">Bảng <?= $i + 1; ?></th>
                    <th class="text-center col-lg-2"></th>
                </tr>
                <tr style="background-color: #060; color:#fff;">
                    <th class="text-center col-lg-2"></th>
                    <th class="text-center col-lg-6">Tên thành viên</th>
                    <th class="text-center col-lg-2">Nhóm</th>
                </tr>
                <?php
                $j = 1;
                foreach ($data as $row) {
                    if ($count % $member == 1) {
                        echo "4banghi";
                    }
                ?>
                    <tr>
                        <td class="text-center"><?= $j; ?></td>
                        <td><?= $row->Name ?></td>
                        <td class="text-center"><?= $row->GroupID ?></td>
                    </tr>
                <?php $j++;
                } ?>
            </table>
        </div>
    <?php } ?>
</div>