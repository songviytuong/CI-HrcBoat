<div class="form">
    <input type="number" name="Number" id="Number" min="0" placeholder="Nhập số lượng người chơi..."/>
    <a class="btn btn-danger sCreate"><i class="fa fa-credit-card"></i> Tạo mới</a>
    <div class="clearfix"><br/></div>
    <div id="showResult"></div>
    <a class="btn btn-warning sReset hidden"><i class="fa fa-repeat"></i> Làm lại</a>
    <a class="btn btn-primary sPlay hidden"><i class="fa fa-random"></i> Bắt đầu</a>
</div>
<div id="listResult"></div>