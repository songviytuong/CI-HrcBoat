<div class="form">
    <a href="<?php echo base_url() ?>random" class="btn btn-warning"><i class="fa fa-repeat"></i></a> Lượt chơi <?= $playid; ?><br /><br />
    <table class="table table-striped table-bordered table-hover table-condensed">
        <tr style="background-color: #060; color:#fff;">
            <th class="text-center col-xs-2">Stt</th>
            <th class="text-center col-lg-6">Người chơi</th>
        </tr>
        <?php
        foreach ($data as $key => $row) {
        ?>
            <tr>
                <td class="text-center"><?= $key + 1; ?></td>
                <td class="text-center"><?= $row; ?></td>
            </tr>
        <?php } ?>
    </table>
</div>