<?php 
class Course extends Admin_Controller { 

    public $user;
    public $classname="training_course";
    private $_data;
    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->add_title('Training Tools');
        $this->template->write_view('sitebar','admin/training_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function list_course_user(){
        $this->load->model('user_model');
        $this->load->model('training_model');
        $this->load->model('training_group_model');
        $_group = $this->training_group_model->getList();

        $this->load->model('training_level_model');
        $_level = $this->training_level_model->getList();
        $name_group = array();
        $name_level = array();
        foreach ($_group as $key => $value) {
            $name_group[$value["id"]] = $value["title"];
        }
        $this->_data["name_group"] = $name_group;

        foreach ($_level as $key => $value) {
            $name_level[$value["id"]] = $value["title"];
        }
        $this->_data["name_level"] = $name_level;

        $this->_data["group"] = $_group;
        $groupid = $this->input->post('groupid');
        $this->_data["groupid"] = $groupid;

        $luser = $this->user_model->getList();
        $name_user = array();
        foreach ($luser as $key => $value) {
            $name_user[$value["ID"]] = $value["UserName"];
            # code...
        }

        $this->_data["name_user"] = $name_user;
        $this->load->model('training_user_model');

        $list = $this->training_user_model->getListAlluser($groupid);

        $this->_data["list"] = $list;
        $this->_data["luser"] = $luser;


        $this->template->write_view('content','admin/list_course_user',  $this->_data);
        $this->template->render();
    }
    public function slist(){
        $this->load->model('training_model');
        $this->load->model('training_group_model');
        $_group = $this->training_group_model->getList();

        $this->load->model('training_level_model');
        $_level = $this->training_level_model->getList();
        $name_group = array();
        $name_level = array();
        foreach ($_group as $key => $value) {
            $name_group[$value["id"]] = $value["title"];
        }
        $this->_data["name_group"] = $name_group;

        foreach ($_level as $key => $value) {
            $name_level[$value["id"]] = $value["title"];
        }
        $this->_data["name_level"] = $name_level;

        $this->_data["group"] = $_group;
        $groupid = $this->input->post('groupid');
        $this->_data["groupid"] = $groupid;
        $list = $this->training_model->getList($groupid);
        $this->_data["list"] = $list;
        $this->template->write_view('content','admin/list_course',  $this->_data);
        $this->template->render();
    }
    public function delete_course($id){
        $check = $this->db->query("select a.* from ttp_training a,ttp_training_user b where a.id=b.courseid and b.id=$id")->row();
        if($check){
            if($check->status==0){
                $this->db->query("delete from ttp_training_user where id=$id");
                $this->db->query("delete from ttp_training_lesson_history where courseid=$id");
                $this->db->query("delete from ttp_training_lesson_history where courseid=$id");
            }
        }
    }
    public function delete_lesson($id){
        $data["enabled"] = 0;
        $this->load->model('training_lesson_model');
        $this->training_lesson_model->update($id,$data);
    }
    public function delete_question($id){
        $this->db->query("update ttp_training_lesson_question set enabled = 0 where questionid = $id")->row();
        $data["enabled"] = 0;
        $this->load->model('training_question_model');
        $this->training_question_model->update($id,$data);
    }
     public function delete_answer($id){
        $data["enabled"] = 0;
        $this->load->model('training_answer_model');
        $this->training_answer_model->update($id,$data);
    }
    public function delete_course_user($id){
        $data["enabled"] = 0;
        $this->load->model('training_user_model');
        $this->training_user_model->update($id,$data);
    }
    public function delete_chapter($id){
        $data["enabled"] = 0;
        $this->load->model('training_chapter_model');
        $this->training_chapter_model->update($id,$data);
    }
    public function delete_part($id){
        $data["enabled"] = 0;
        $this->load->model('training_part_model');
        $this->training_part_model->update($id,$data);
    }

}
?>