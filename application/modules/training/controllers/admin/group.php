<?php 
class Group extends Admin_Controller { 
 
 	public $user;
 	public $classname="training_group";
    private $_data;
    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/training_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_title('Training Tools');
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
	public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $data = array(
            'data'=>$this->db->query("select * from ttp_training_group")->result()
        );
        $this->template->add_title('Group | Training Tools');
		$this->template->write_view('content','admin/group_home',$data);
		$this->template->render();
	}

    public function remove($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $this->db->query("delete from ttp_training_group where id=$id");
        echo "OK";
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $ID = isset($_POST['ID']) ? $_POST['ID'] : '' ;
        if($Title!='' && is_numeric($ID)){
            $this->db->query("update ttp_training_group set title='$Title' where id=$ID");
        }
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        if($Title!=''){
            $data = array(
                'title' =>$Title,
                'datetime'  =>date('Y-m-d H:i:s'),
                'enabled' =>1
            );
            $this->db->insert('ttp_training_group',$data);
        }
    }
}
?>