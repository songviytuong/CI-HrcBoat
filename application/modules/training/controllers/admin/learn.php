<?php 
class Learn extends Admin_Controller { 
 
 	public $user;
 	public $classname="learn";
    private $_data;
    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/training_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_title('Lean Training Tools');
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
	public function index(){
        $this->template->add_title('Training Tools');
		$this->template->write_view('content','admin/training_content');
		$this->template->render();
	}

    public function get_certificate($coursesid=0,$lessonid=0,$chapterid=0,$partid=0,$courseid=0){
        if($lessonid>0 && $chapterid>0 && $partid>0 && $courseid>0){
            $this->update_history($lessonid,$chapterid,$partid,$courseid,1);
        }
        $this->load->model('training_point_model');
        $total_lquestion = $this->training_point_model->getCountLessonByCourse($coursesid);
        $total1 = $total_lquestion["total"];
        
        $total_complete = $this->training_point_model->getCountCompleteLessonByCourse($this->user->ID,$coursesid);
        $total2 = $total_complete["total"];
        $status_pass = $total1 == $total2 ? 0 : 1 ;
        $courses = $this->db->query("select a.*,b.enddate from ttp_training a,ttp_training_user b where b.status=1 and a.id=b.courseid and a.id=$coursesid and b.userid=".$this->user->ID)->row();
        $data = array(
            'data'          =>$courses,
            'coursesid'     =>$coursesid,
            'status_pass'   =>$status_pass
        );
        $this->template->add_title('Certificate of courses');
        $this->template->write_view('content','admin/training_certificate',$data);
        $this->template->render();
    }
       
    public function add(){
        $this->template->write_view('content','admin/add',  $this->_data);
        $this->template->render();
    }
    public function slist(){
        $kq = $this->input->post("kq");
        if(!empty($kq)){
            $kq = str_replace("'", "", $kq);
        }
        $list = $this->db->query("select b.*,c.title as group_title,a.title,a.hour,d.title as level_title from ttp_training a,ttp_training_user b,ttp_training_group c,ttp_training_level d where a.id=b.courseid and b.userid=".$this->user->ID." and a.groupid=c.id and a.level=d.id")->result();
        $this->_data["kq"] = $kq;
        $this->_data["list"] = $list;
        $this->template->write_view('content','admin/slist',  $this->_data);
        $this->template->render();
    }
    public function  detail($id){
        $this->load->model('training_lesson_model');
        $this->load->model('training_level_model');
        $this->load->model('training_group_model');

        $this->load->model('training_user_model');
        $user = (array)$this->user;
        $groupid = $user["UserType"];
        $userid = $user["ID"];
        $detail = $this->training_user_model->getDetailByCourse($userid,$id);
        if(empty($detail)){
            redirect('/administrator/training/learn/slist');
        }
        $this->_data["detail"] = $detail;

        
        $detail_level = $this->training_level_model->getDetail($detail["level"]);

        $detail_group = $this->training_group_model->getDetail($detail["groupid"]);

        $this->_data["name_level"] = $detail_level["title"];
        $this->_data["name_group"] = $detail_group["title"];

        $count_course = $this->training_lesson_model->getCountLessonByCourse($detail["courseid"]);
        $count = array();
        foreach ($count_course as $key => $items) {
            $count[$items["type"]] = $items["total"];
        }
        
        $this->_data["counts"] = $count;

        $this->load->model('user_model');
        $detail_us = $this->user_model->getDetail($detail["courseid"]);
        $this->_data["detail_us"] = $detail_us;

        $this->load->model('training_part_model');
        $list =  $this->training_part_model->getListByScienceId($id);
        
        foreach ($list as $key => $value) {
            $_partid[] = $value["id"];
        }
        if(isset($_partid)){
            $partid = implode($_partid, ",");
            $this->load->model('training_chapter_model');
            $slist =  $this->training_chapter_model->getListByPartId($partid);
            $arr_chapter = array();
            $arr_idchapter = array();
            $arr = array();
            foreach ($slist as $key => $value) {
                $arr_chapter["title"] = $value["title"];
                $arr_chapter["id"] = $value["id"];
                $arr[$value["partid"]][] = $arr_chapter;
                $arr_idchapter[$value["partid"]][] = $value["id"];
                $_lesson[] = $value["id"];
            }
        }
        
        if(isset($_lesson)){
            $lesson = implode($_lesson, ",");
            
            $slesson =  $this->training_lesson_model->getListByChapterId($lesson);
            $arr_lesson = array();
            foreach ($slesson as $key => $value) {
                $arr_lesson[$value["chapterid"]][$value["partid"]][] = $value["title"];
            }
        }
        
        foreach ($list as $key => &$value) {
            $l = array();
            $ids = $value["id"];
            $value["arr_chapter"] = isset($arr[$ids]) ? $arr[$ids] : '' ;
            $value["arr_idchapter"] = isset($arr_idchapter[$ids]) ? $arr_idchapter[$ids] : '' ;
            if($value["arr_idchapter"] !=null){
                foreach ($value["arr_idchapter"] as $key2 => &$value2) {
                    $value["lesson"][$value2][$value["id"]] = isset($arr_lesson[$value2][$value["id"]]) ? $arr_lesson[$value2][$value["id"]] : '';
                    
                }
            }
            
        }

        $this->_data["list"] = $list;
        $total = $this->db->query("select sum(hour) as total from ttp_training_lesson where courseid=$id")->row();
        $this->_data["total_time"]  = $total ? $total->total : 0 ;
        $this->template->write_view('content','admin/detail',  $this->_data);
        $this->template->render();
    }
    public function study($scienceid,$testid=0){
        $this->_data["testid"] = $testid;
        $this->load->model('training_part_model');
        $list =  $this->training_part_model->getListByScienceId($scienceid);
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $_partid[] = $value["id"];
            }
            $partid = implode($_partid, ",");
            $this->load->model('training_chapter_model');
            $slist =  $this->training_chapter_model->getListByPartId($partid);


            $arr_chapter = array();
            $arr_idchapter = array();
            $arr = array();
            foreach ($slist as $key => $value) {
                $arr_chapter["title"] = $value["title"];
                $arr_chapter["id"] = $value["id"];
                $arr[$value["partid"]][] = $arr_chapter;
                $arr_idchapter[$value["partid"]][] = $value["id"];
                $_lesson[] = $value["id"];
            }
            if(isset($_lesson)){
                $lesson = implode($_lesson, ",");
                $this->load->model('training_lesson_model');
                $slesson =  $this->training_lesson_model->getListByChapterId($lesson);
                $arr_lesson = array();
                foreach ($slesson as $key => $value) {
                    $arr_lesson["title"] = $value["title"];
                    $arr_lesson["id"] = $value["id"];
                    $arr_lesson["type"] = $value["type"];
                    $arr_lesson["hour"] = $value["hour"];
                    $arr2[$value["chapterid"]][$value["partid"]][] = $arr_lesson;
                }
            }
            foreach ($list as $key => &$value) {
                $l = array();
                $ids = $value["id"];
                $value["arr_chapter"] = isset($arr[$ids]) ? $arr[$ids] : '' ;
                $value["arr_idchapter"] = isset($arr_idchapter[$ids]) ? $arr_idchapter[$ids] : '' ;
                if($value["arr_idchapter"] !=null){
                    foreach ($value["arr_idchapter"] as $key2 => &$value2) {
                        $value["lesson"][$value2][$value["id"]] = isset($arr2[$value2][$value["id"]]) ? $arr2[$value2][$value["id"]] : '';
                        
                    }
                }
                
            }
        }

        $user = (array)$this->user;
        $userid = $user["ID"];
        $this->load->model('training_history_model');
        $shistory =  $this->training_history_model->getList($userid);

        $history1 = array();
        foreach ($shistory as $key4 => $value4) {
            if($value4["status"]==1){
                $history1[] = $value4["lessonid"];
            }
        }
        $this->_data["history1"] = $history1;

        $this->_data["list"] = $list;
        $this->_data['scienceid'] = $scienceid;
        $this->template->write_view('content','admin/study',  $this->_data);
        $this->template->render();
    }
    public function getlesson($id){
        $this->load->model('training_lesson_model');
        $detail =  $this->training_lesson_model->getDetail($id);
        $this->_data["detail"] = $detail;
        if($detail["type"] ==3){

        }
        echo json_encode($detail);
    }
    public function get_question($lessonid,$limit=0){
        $this->load->model('training_lesson_question_model');
        $detail =  $this->training_lesson_question_model->getDetailByLessonId($lessonid,$limit);
        $arr = array();
        $res = array();
        $arr["cauhoi"] = $detail["title"];
        $arr["questionid"] = $detail["questionid"];
        $questionid = $detail["questionid"];

        $this->load->model('training_answer_model');
        $_answer = $this->training_answer_model->getListByQuestionId($questionid);

        foreach ($_answer as $key => $value) {
            $arr["title"] = $value["title"];
            $arr["id"] = $value["id"];
            $res[] = $arr;
        }
        echo json_encode($res);
    }
    public function send_question(){
        $arr = array();
        $ret = array();
        $answerid = (int)$this->input->post("answerid");
        $courseid = $this->input->post("courseid");
        $partid = $this->input->post("partid");
        $chapterid = $this->input->post("chapterid");
        $lessonid = $this->input->post("lessonid");
        $questionid = $this->input->post("questionid");
        $user = (array)$this->user;
        $userid = $user["ID"];
        if($answerid ==0){
            $arr["id"] = "answerid";
            $arr["msg"] = "Vui lòng chọn đáp án";
            $ret[] = $arr;
        }
        if(count($ret) >0){
            echo json_encode($ret);
            return;
        }else{
            $this->load->model('training_answer_model');
            $trues = 0;
            $detail = $this->training_answer_model->getDetail($answerid);
            if($detail["true"]==1){
                $trues = 1;
                $data["mark_true"] = 1;
            }
            if($detail["true"]==0){
                $trues = 2;
                $data["mark_false"] = 1;
            }
            $data["userid"] = $userid;
            $data["courseid"] = $courseid;
            $data["partid"] = $partid;
            $data["chapterid"] = $chapterid;
            $data["lessonid"] = $lessonid;
            $data["start_date"] = date('Y-m-d H:i:s');
            $this->load->model('training_point_model');

            $checkpoint = $this->training_point_model->getDetailByLessonUserId($userid,$lessonid);
            if($checkpoint ==null){
                $pointid = $this->training_point_model->insert($data);
            }else{
                $pointid = $checkpoint["id"];
            }
            
            $sdata["pointid"] = $pointid;
            $sdata["questionid"] = $questionid;
            $sdata["answerid"] = $answerid;
            $sdata["trues"] = $trues;
            $this->load->model('training_point_detail_model');

            $check_detail_point = $this->training_point_detail_model->getDetailByQuestionId($pointid,$questionid);
            if($check_detail_point ==null){
                $sdata["datetime"] = date('Y-m-d H:i:s');
                $sdata["userid"] = $userid;
                
                $this->training_point_detail_model->insert($sdata);
            }else{
                $sdata["end_datetime"] = date('Y-m-d H:i:s');
                $this->training_point_detail_model->update($check_detail_point["id"],$sdata);
            }

            if($checkpoint !=null){
                $ssum = $this->training_point_detail_model->getSum($checkpoint["id"]);
                $arr_true = array();
                for($i=0;$i<=2;$i++){
                    $arr_true[$i] = 0;
                }
                foreach ($ssum as $key => $value) {
                    $arr_true[$value["trues"]] = $value["total"];
                }
                $udata["mark_true"] = $arr_true[1];
                $udata["mark_false"] = $arr_true[2];
                $udata["mark_pass"] = $arr_true[0];
                $this->training_point_model->update($checkpoint["id"],$udata);
            }
            
            $arr["id"] = "ok";
            $arr["msg"] = "ok";
            $ret[] = $arr;
            echo json_encode($ret);
        }

        
    }
    public function next_question($lessonid,$limit){
        $user = (array)$this->user;
        $userid = $user["ID"];
        $this->load->model('training_lesson_question_model');
        $count_question = $this->training_lesson_question_model->getCountQuestionByLesson($lessonid);
        if($limit < $count_question["total"]){
            $detail_question =  $this->training_lesson_question_model->getDetailByLessonId($lessonid,$limit);
            $arr["cauhoi"] = $detail_question["title"];
            $arr["suggestid"] = $detail_question["suggestid"];
            $lesson = $this->db->query("select title from ttp_training_lesson where id=".$arr["suggestid"])->row();
            $arr["lesson"] = $lesson ? $lesson->title : '' ;
            $arr["image"] = file_exists($detail_question["Image"]) ? "<img class='img-thumbnail' src='".$detail_question["Image"]."' />" : '';
            $arr["questionid"] = $detail_question["questionid"];
            $questionid = $detail_question["questionid"];

            $this->load->model('training_answer_model');
            $_answer = $this->training_answer_model->getListByQuestionId($questionid);
            $res = array();
            foreach ($_answer as $key => $value) {
                $arr["title"] = $value["title"];
                $arr["id"] = $value["id"];
                $res[] = $arr;
            }
            echo json_encode($res);
            return;
        }else{
            $this->load->model('training_point_model');
            $detail_point = $this->training_point_model->getDetailByLessonUserId($userid,$lessonid);
            $point = $detail_point["mark_true"]/$count_question["total"]*100;
            $data["mark_abs"] = round($point) ;
            $data["status"] = 1;
            $this->training_point_model->update($detail_point["id"],$data);

            $total_lquestion = $this->training_point_model->getCountLessonByCourse($detail_point["courseid"]);
            $total1 = $total_lquestion["total"];
            
            $total_complete = $this->training_point_model->getCountCompleteLessonByCourse($userid,$detail_point["courseid"]);
            $total2 = $total_complete["total"];
            $sstatus = 0;
            $arr['pass'] = 'notpass';
            if($total1 == $total2){
                $check_point = $this->db->query("select count(id) as total from ttp_training_point where mark_abs<100 and lessonid=$lessonid and userid=".$this->user->ID)->row();
                if($check_point->total==0){
                    $arr['pass'] = 'passed';
                    $sstatus = 1;
                    $enddate = date('Y-m-d H:i:s');
                    $this->update_history($detail_point["lessonid"],$detail_point["chapterid"],$detail_point["partid"],$detail_point["courseid"],1);
                }else{
                    $sstatus = 2;
                    $enddate='';
                }
            }else{
                $check_point = $this->db->query("select mark_abs from ttp_training_point where lessonid=$lessonid and userid=".$this->user->ID)->row();
                if($check_point){
                    $arr['state'] = 3;
                    if($check_point->mark_abs==100){
                        $arr['pass'] = "passed_one";
                        $this->update_history($detail_point["lessonid"],$detail_point["chapterid"],$detail_point["partid"],$detail_point["courseid"],1);
                    }
                }
                $sstatus = 2;
                $enddate = '';
            }
            $this->load->model('training_user_model');
            $this->training_user_model->updateStatusCourseID($userid,$detail_point["courseid"],$sstatus,$enddate);

            $arr["id"] ="ok";
            $arr["msg"] = "ok";
            $res[] = $arr;
        }
        echo json_encode($res);
    }
    public function update_history($lessonid,$chapterid,$partid,$courseid,$status){
        $user = (array)$this->user;
        $userid = $user["ID"];
        $this->load->model('training_user_model');
        $_checkuser = $this->training_user_model->getDetailByCourseUser($userid,$courseid);
        if($_checkuser != null){
            if($_checkuser["startdate"] ==null){
                $sdata["startdate"] = date('Y-m-d H:i:s');
                $this->training_user_model->update($_checkuser["id"],$sdata);
            }
        }
        $this->load->model('training_history_model');
        $checkDetail = $this->training_history_model->checkDetail($userid,$lessonid,$chapterid,$partid,$courseid);
        if($checkDetail == null){
            $data["userid"] = $userid;
            $data["lessonid"] = $lessonid;
            $data["chapterid"] = $chapterid;
            $data["partid"] = $partid;
            $data["courseid"] = $courseid;
            $data["datetime"] = date('Y-m-d H:i:s');
            $data["status"] = $status;
            $this->training_history_model->insert($data);
        }else{
            $data["status"] = $status;
            if($status ==1){
                $this->training_history_model->updateStatus($status, $userid,$lessonid,$chapterid,$partid,$courseid);
            }
            
        }
    }

    public function get_result($lessonid){
        $user = (array)$this->user;
        $userid = $user["ID"];
        $this->load->model('training_history_model');
        $list = $this->training_history_model->getHistoryByLessonId($lessonid,$userid);
        foreach ($list as $key => $value) {
            $_questionid[] = $value["questionid"];
        }
        $questionid = implode($_questionid, ",");

        $this->load->model('training_point_detail_model');
        $point = $this->training_point_detail_model->countQuestionIdByUserId($userid,$questionid);
        $arr_true = array();
        $arr_false = array();
        foreach ($point as $key => $items) {
            if($items["trues"]==1){
                $arr_true[$items["questionid"]] = $items["total"];
            }
            if($items["trues"]==2){
                $arr_false[$items["questionid"]] = $items["total"];
            }
        }
        foreach ($list as $key => &$item) {
            $item["total_true"] = isset($arr_true[$item["questionid"]]) ? $arr_true[$item["questionid"]] : 0;
            $item["total_false"] = isset($arr_false[$item["questionid"]]) ? $arr_false[$item["questionid"]] : 0 ;
        }
        
        echo json_encode($list);
    }
    public function reset_lesson(){
        $user = (array)$this->user;
        $userid = $user["ID"];
        $courseid = $this->input->post("courseid");
        $partid = $this->input->post("partid");
        $chapterid = $this->input->post("chapterid");
        $lessonid = $this->input->post("lessonid");
        $this->load->model('training_point_model');
        
        $checkpoint = $this->training_point_model->updateStatus($userid,$lessonid);
        if($checkpoint ==null){
            $arr["id"] = "nok";
            $arr["msg"] = "nok";
            $ret[] = $arr;
        }else{
            $arr["id"] = "ok";
            $arr["msg"] = "ok";
            $ret[] = $arr;
        }
        echo json_encode($ret);

    }
    
    public function get_course_by_id($courseid){
        $this->load->model('training_part_model');
        $list = $this->training_part_model->getListByScienceId($courseid);
        echo json_encode($list);
    }
    public function get_part_by_id($partid){
        $this->load->model('training_chapter_model');
        $list = $this->training_chapter_model->getListByPartId($partid);
        echo json_encode($list);
    }
    public function get_part(){
        $this->load->model('training_part_model');
        $list = $this->training_part_model->getList();
        echo json_encode($list);
    }
    public function get_chapter(){
        $this->load->model('training_chapter_model');
        $list = $this->training_chapter_model->getList();
        echo json_encode($list);
    }
    public function get_chapter_by_course_by_part($courseid = 0,$partid = 0){
        $this->load->model('training_chapter_model');
        $list = $this->training_chapter_model->getListByCourseByPart($courseid,$partid);
        echo json_encode($list);
    }
    
    public function get_group(){
        $this->load->model('training_group_model');
        $list = $this->training_group_model->getList();
        echo json_encode($list);
    }
    public function get_level(){
        $this->load->model('training_level_model');
        $list = $this->training_level_model->getList();
        echo json_encode($list);
    }
    public function get_type($id=0){
        $ret = array(
            "1"=>"Lý thuyết",
            "2"=>"Video",
            "3"=>"Bài kiểm tra"
            );
        if($id>0){
            return $ret[$id];
        }else{
            return $ret;
        }
    }

}
?>