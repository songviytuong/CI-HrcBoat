<?php 
class Manager extends Admin_Controller { 
 
 	public $user;
 	public $classname="manager";
    private $_data;
    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/training_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_title('Training Tools');
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	public function add_course($id=0,$tab=0){
            $class0 = '';
            $class1 = '';
            if($id==0){
                $tab=0;
            }
            
            $none = 'none';
            $this->load->model('training_model');

            $this->load->model('training_part_model');
            $lpart = $this->training_part_model->getList();
            $part_name = array();

            foreach ($lpart as $key => $value) {
                $part_name[$value["id"]] = $value["title"];
            }

            $this->_data["part_name"] = $part_name;


            
            $this->load->model('training_group_model');
            $group = $this->training_group_model->getList();
            $this->_data["group"] = $group;

            $this->load->model('training_level_model');
            $level = $this->training_level_model->getList();
            $this->_data["level"] = $level;

            if($id>0){
                $none='';
                $this->load->model('training_model');
                $detail = $this->training_model->getDetail($id);
                $this->_data["data"] = $detail;
                if(!empty($detail)){
                    $this->load->model('training_lesson_model');
                    $list_lesson = $this->training_lesson_model->getListByCourseId($detail["id"]);
                    $this->_data["list_lesson"] = $list_lesson;
                }else{
                    $tab=0;
                }
                

            }
            $this->_data["none"] = $none;


            


            $this->load->model('training_chapter_model');
            $list_chapter = $this->training_chapter_model->getList();
            
            $chapter_name = array();
            foreach ($list_chapter as $key => $value) {
                $chapter_name[$value["id"]] = $value["title"];
                # code...
            }

            $this->_data["chapter_name"] = $chapter_name;

            $type = $this->get_type();
            $this->_data["type"] = $type;
            if($tab ==0){
                $class0 = 'active';
            }
            if($tab ==1){
                $class1 = 'active';
            }
            $this->_data["class0"] = $class0;
            $this->_data["class1"] = $class1;
            // $this->load->model('training_question_model');
            // $list_question = $this->training_question_model->getList();
            // $this->_data["list_question"] = $list_question;




            $this->template->write_view('content','admin/add_course',  $this->_data);
            $this->template->render();
        }
        public function save_course(){
            $this->load->model('training_model');
            $id = (int)$this->input->post("id");
            $title = $this->input->post("title");
            $groupid = $this->input->post("groupid");
            $level = $this->input->post("level");
            $des_full = $this->input->post("full");
            $des_soft = $this->input->post("soft");
            $study = $this->input->post("sstudy");
            $request = $this->input->post("srequest");
            $video = $this->input->post("video");
            $fast_test = (int)$this->input->post("fast_test");
            $point_test = (int)$this->input->post("point_test");
            // upload
            $config['upload_path'] = 'public/uploads/images_training/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '100';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            
            //end upload





            $ret = array();
            $arr = array();
            if($title ==null){
                $arr["id"] = "title";
                $arr["msg"] = "Thông báo.\nVui lòng nhập tiêu đề.";
                $ret[] = $arr;
            }
            if($groupid ==0){
                $arr["id"] = "groupid";
                $arr["msg"] = "Thông báo.\nVui lòng nhập phân nhóm.";
                $ret[] = $arr;
            }
            if($level ==0){
                $arr["id"] = "level";
                $arr["msg"] = "Thông báo.\nVui lòng nhập cấp độ khóa học.";
                $ret[] = $arr;
            }
            if($fast_test >0 && $point_test <1){
                $arr["id"] = "point_test";
                $arr["msg"] = "Thông báo.\nVị trí phải lớn hơn 0 :)).";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('img'))
                {
                    if($id>0){
                        $detail = $this->training_model->getDetail($id);
                        $img = $detail["img"];
                    }else{
                        $img = '';
                    }
                    $error = array($this->upload->display_errors());
                    // $sc = "<script>alert('Error! images');window.history.back();</script>";
                    // $sc = "<script>alert('".$error[0]."');window.history.back();</script>";
                    // echo $error;
                    // return;
                    
                }else{
                    $image_data = $this->upload->data();
                    $img = $image_data["file_name"];

                }

                
                $data["img"] = isset($img) ? $img :'';
                if($id==0){
                    $data["title"] = $title;
                    $data["groupid"] = $groupid;
                    $data["level"] = $level;
                    $data["des_full"] = $des_full;
                    $data["des_soft"] = $des_soft;
                    $data["study"] = $study;
                    $data["request"] = $request;
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $data["video"] = $video;
                    $data["fast_test"] = $fast_test;
                    $data["point_test"] = $point_test;
                    $this->training_model->insert($data);
                }else{
                    $data["title"] = $title;
                    $data["groupid"] = $groupid;
                    $data["level"] = $level;
                    $data["des_full"] = $des_full;
                    $data["des_soft"] = $des_soft;
                    $data["study"] = $study;
                    $data["request"] = $request;
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $data["video"] = $video;
                    $data["fast_test"] = $fast_test;
                    $data["point_test"] = $point_test;
                    $this->training_model->update($id,$data);

                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                $sc = "<script>alert('Luu thanh cong');window.history.back();</script>";
                echo $sc;
                // redirect('/administrator/training/training/add_course');
                // echo json_encode($ret);
            }
        }

        public function get_course_by_id($courseid){
            $this->load->model('training_part_model');
            $list = $this->training_part_model->getListByScienceId($courseid);
            echo json_encode($list);
        }
        public function get_part_by_id($partid){
            $this->load->model('training_chapter_model');
            $list = $this->training_chapter_model->getListByPartId($partid);
            echo json_encode($list);
        }
        public function get_part(){
            $this->load->model('training_part_model');
            $list = $this->training_part_model->getList();
            echo json_encode($list);
        }
        public function get_chapter(){
            $this->load->model('training_chapter_model');
            $list = $this->training_chapter_model->getList();
            echo json_encode($list);
        }
        public function get_chapter_by_course_by_part($courseid = 0,$partid = 0){
            $this->load->model('training_chapter_model');
            $list = $this->training_chapter_model->getListByCourseByPart($courseid,$partid);
            echo json_encode($list);
        }

        public function add_part($id=0){
            $this->load->model('training_part_model');
            $this->load->model('training_model');
            $list = $this->training_model->getList();
            $this->_data["list"] = $list;
            if($id>0){
                $detail = $this->training_part_model->getDetail($id);
                $this->_data["detail"] = $detail;
            }

            $lpart = $this->training_part_model->getList();
            $this->_data["data"] = $lpart;

            $name_course = array();

            foreach ($list as $key => $value) {
                $name_course[$value["id"]] = $value["title"];
            }
            $this->_data["name_course"] = $name_course;


            $this->template->write_view('content','admin/add_part',  $this->_data);
            $this->template->render();
        }
        public function save_part(){
            $id = (int)$this->input->post("id_part");
            $title = $this->input->post("title_part_new");
            $courseid = $this->input->post("courseid");
            $arr = array();
            $ret = array();
            if(empty($title)){
                $arr["id"] = "title_part_new";
                $arr["msg"] = "Thông báo\nVui lòng nhập tiêu đề phần khóa học.";
                $ret[] = $arr;
            }
            if(empty($courseid)){
                $arr["id"] = "courseid";
                $arr["msg"] = "Thông báo\nVui lòng chọn khóa học.";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return ;
            }else{
                $this->load->model('training_part_model');
                $data["title"] = $title;
                $data["scienceid"] =$courseid;
                if($id==0){
                    // insert data
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $this->training_part_model->insert($data);
                }else{
                    // update data
                    $this->training_part_model->update($id,$data);
                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }

        }
        public function add_chapter($id=0){
            $this->load->model('training_model');
            $course = $this->training_model->getList();
            $this->_data["course"] = $course;
            $this->load->model('training_chapter_model');

            $this->load->model('training_part_model');
            $list = $this->training_part_model->getList();
            $this->_data["list"] = $list;

            if($id>0){
                $detail = $this->training_chapter_model->getDetail($id);
                $this->_data["detail"] = $detail;
                $partid = $detail["partid"];
            }
            $name_course = array();
            $name_part = array();

            foreach ($course as $key => $value) {
                $name_course[$value["id"]] = $value["title"];
            }
            $this->_data["name_course"] = $name_course;

            foreach ($list as $key => $value) {
                $name_part[$value["id"]] = $value["title"];
            }
            $this->_data["name_part"] = $name_part;
            

            $lchapter = $this->training_chapter_model->getList();
            $this->_data["data"] = $lchapter;

            $this->template->write_view('content','admin/add_chapter',  $this->_data);
            $this->template->render();
        }
        public function save_chapter(){
            $id = (int)$this->input->post("id_chapter");
            $title = $this->input->post("title_chapter_new");
            $courseid = $this->input->post("courseid");
            $partid = $this->input->post("partid");
            $arr = array();
            $ret = array();
            if(empty($title)){
                $arr["id"] = "title_chapter_new";
                $arr["msg"] = "Thông báo\nVui lòng nhập tiêu đề chương khóa học.";
                $ret[] = $arr;
            }
            if(empty($courseid)){
                $arr["id"] = "courseid";
                $arr["msg"] = "Thông báo\nVui lòng chọn khóa học.";
                $ret[] = $arr;
            }
            if(empty($partid)){
                $arr["id"] = "partid";
                $arr["msg"] = "Thông báo\nVui lòng chọn Phần của chương.";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return ;
            }else{
                $this->load->model('training_chapter_model');
                $data["title"] = $title;
                $data["partid"] =$partid;
                $data["courseid"] =$courseid;
                if($id==0){
                    // insert data
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $this->training_chapter_model->insert($data);
                }else{
                    // update data
                    $this->training_chapter_model->update($id,$data);
                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }

        public function add_lesson($id=0,$courseid=0,$tip=1){
            $this->load->model('training_model');
            $course = $this->training_model->getList();
            $this->_data["course"] = $course;
            
            // var_dump($courseid);exit();

            $type = $this->get_type();
            $this->_data["type"] = $type;
            $this->load->model('training_question_model');
            $list_question = $this->training_question_model->getList();
            $this->_data["list_question"] = $list_question;

            if($id >0){
                $this->load->model('training_lesson_question_model');
                $lesson_question = $this->training_lesson_question_model->getListByLessonId($id);
                if(!empty($lesson_question)){
                    foreach ($lesson_question as $key => $value) {
                        $questionid_active[] = $value["questionid"];
                    }
                    $this->_data["questionid_active"] = $questionid_active;

                }
                
                $this->load->model('training_lesson_model');
                $data = $this->training_lesson_model->getDetail($id);
                $courseid = $data["courseid"];
                $this->_data["data"] = $data;
                $display1 = 'none';
                $display2 = 'none';
                $display3 = 'none';
                if($data["type"] ==1){
                    $display1 = '';
                }
                if($data["type"] ==2){
                    $display2 = '';
                }
                if($data["type"] ==3){
                    $display3 = '';
                }

                $this->load->model('training_chapter_model');
                $chapter = $this->training_chapter_model->getListByPartId($data["partid"]);
                $this->_data["chapter"] = $chapter;
                
            }else{
                $display1 = 'none';
                $display2 = 'none';
                $display3 = 'none';
                if($tip == 1){
                    $display1 = '';
                }
                if($tip == 2){
                    $display2 = '';
                }
                if($tip == 3){
                    $display3 = '';
                } 
            }
            

            // var_dump($display3);exit();
            $this->_data["tip"] = $tip;
            $this->_data["display1"] = $display1;
            $this->_data["display2"] = $display2;
            $this->_data["display3"] = $display3;


            $this->_data["cid"] = $courseid;
            $this->load->model('training_part_model');
            $part = $this->training_part_model->getListByScienceId($courseid);
            $this->_data["part"] = $part;
            



            if($id>0){
                $this->template->write_view('content','admin/edit_lesson',  $this->_data);
            }else{
                $this->template->write_view('content','admin/add_lesson',  $this->_data);
            }




            
            $this->template->render();
        }
        public function save_lesson(){
            $checks = $this->input->post("checks");
            $id = (int)$this->input->post("id");
            $title = $this->input->post("title");
            $type = $this->input->post("type");
            $video = $this->input->post("video");
            $hour = $this->input->post("hour");
            $courseid = $this->input->post("courseid");
            $partid = $this->input->post("partid");
            $chapterid = $this->input->post("chapterid");
            $des_soft = $this->input->post("des_soft");
            $des_full = $this->input->post("full");
            $ret =array();
            $arr = array();
            if(empty($title)){
                $arr["id"] = "title";
                $arr["msg"] = "Thông báo\nVui lòng nhập tiêu đề bài học";
                $ret[] = $arr;
            }
            if(empty($type)){
                $arr["id"] = "type";
                $arr["msg"] = "Thông báo\nVui lòng chọn loại bài học";
                $ret[] = $arr;
            }
            
            if(empty($courseid)){
                $arr["id"] = "courseid";
                $arr["msg"] = "Thông báo\nVui lòng chọn khóa học";
                $ret[] = $arr;
            }
            if(empty($partid)){
                $arr["id"] = "partid";
                $arr["msg"] = "Thông báo\nVui lòng chọn phần học";
                $ret[] = $arr;
            }
            if(empty($chapterid)){
                $arr["id"] = "chapterid";
                $arr["msg"] = "Thông báo\nVui lòng chọn chương";
                $ret[] = $arr;
            }
            if($type ==3 && empty($checks)){
                $arr["id"] = "checkAll";
                $arr["msg"] = "Thông báo\nVui lòng chọn câu hỏi";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $this->load->model('training_lesson_model');
                $this->load->model('training_lesson_question_model');
                $data["title"] = $title;
                $data["type"] = $type;
                $data["video"] = $video;
                $data["enabled"] = 1;
                $data["datetime"] = date('Y-m-d H:i:s');
                $data["hour"] = $hour;
                $data["courseid"] = $courseid;
                $data["partid"] = $partid;
                $data["chapterid"] = $chapterid;
                $data["des_soft"] = $des_soft;
                $data["des_full"] = $des_full;
                $now = date('Y-m-d H:i:s');
                $user = (array)$this->user;
                $username = $user["UserName"];
                if($id==0){
                    $lessonid = $this->training_lesson_model->insert($data);
                }else{
                    $lessonid = $id;
                    $this->training_lesson_model->update($id,$data);
                }
                if($type ==3){
                    for ($i=0; $i < count($checks); $i++) {
                        $_ischeck = $this->training_lesson_question_model->getDetailByLessonQuestion($lessonid,$checks[$i]);
                        if($_ischeck != null){
                            continue;
                        }else{
                            $_query[] = "(NULL, '$lessonid', $checks[$i], '1', '$now','$username', NULL, NULL)";
                        }

                        
                    }
                    if(!empty($_query)){
                        $query = count($_query)>0 ? "INSERT INTO `ttp_corp`.`ttp_training_lesson_question` (`id`, `lessonid`, `questionid`, `enabled`, `datetime`, `creator`, `end_datetime`, `end_creator`) VALUES ".implode(",",$_query) : '';
                        $this->training_lesson_model->excute($query);
                    }
                    

                }


                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }


        }

    public function add_answer($id=0,$cid=0){

            $this->_data["cid"] = $cid;

            if($id>0){
                $this->load->model('training_question_model');
                $data = $this->training_question_model->getDetail($id);
                $this->_data["data"] = $data;

                $this->load->model('training_answer_model');
                $list_answer = $this->training_answer_model->getListByQuestionId($id);

                $this->_data["list_answer"] = $list_answer;
                $this->template->write_view('content','admin/edit_answer',  $this->_data);


            }else{
                $this->template->write_view('content','admin/add_answer',  $this->_data);
            }
            
            $this->template->render();
        }
        public function save_answer(){
             $ids = $this->input->post("ids");
            $this->load->model('training_answer_model');
            $id = (int)$this->input->post("id");
            $title = $this->input->post("title");
            for($i=0;$i<count($title);$i++){
                $_true[] = $this->input->post("trues".$i);
            }
            $title_question = $this->input->post("title_question");

            if($id>0){
                $detail = $this->training_answer_model->getDetail($id);
                if(!empty($detail)){
                    $questionid = $detail["questionid"];
                }
                
            }
            $arr = array();
            $ret = array();
            
            if(empty($title_question)){
                $arr["id"] = "title_question";
                $arr["msg"] = "Thông báo.\nVui lòng nhập câu hỏi.";
                $ret[] = $arr;
            }
            if($id==0){
                if(empty($title)){
                    $arr["id"] = "title";
                    $arr["msg"] = "Thông báo.\nVui lòng nhậu câu trả lời.";
                    $ret[] = $arr;
                }

            }
            
            if(count($ret) >0){
                echo json_encode($ret);
            }else{
                $this->load->model('training_question_model');
                $user = (array)$this->user;
                $userid = $user["ID"];
                $username = $user["UserName"];
                $now = date('Y-m-d H:i:s');
                $title_question = $this->input->post("title_question");
                $qdata["title"] = $title_question;
                if($id ==0){
                    // thêm câu hỏi
                    
                    
                    
                    $qdata["author"] = $userid;
                    $qdata["enabled"] = 1;
                    $qdata["datetime"] = date('Y-m-d H:i:s');
                    $questionid = $this->training_question_model->insert($qdata);
                    // end thêm câu hỏi
                    


                    for ($i=0; $i < count($title); $i++) { 
                        if($title[$i] !=null){
                            $_query[] = "(NULL, '$title[$i]', $questionid, $_true[$i], '1', '$now', NULL, '$username', NULL)";
                        }
                        
                    }
                    $query = count($_query)>0 ? "INSERT INTO ttp_training_answer (id, title, questionid, `true`, enabled
                        , `datetime`, end_datetime, creator, end_creator) VALUES ".implode(",",$_query) : '';
                    if($query!='')
                    $this->training_answer_model->excute($query);
                }else{
                    $this->training_question_model->update($id,$qdata);
                    if(!empty($ids)){
                        foreach ($ids as $key => $value) {
                            $title_answer = $this->input->post("title_answer".$value);
                            $true = $this->input->post('trues'.$value);
                            // $__query[] = "UPDATE `ttp_training_answer` SET `title` = '$title_answer', `true` = '$true' WHERE `id` = $value"; 
                            $__query = "UPDATE `ttp_training_answer` SET `title` = '$title_answer', `true` = '$true' WHERE `id` = $value"; 
                            $this->training_answer_model->excute($__query);
                        }
                        
                        // $sql = implode($__query, ";");
                        // // var_dump($sql);exit();
                        // $this->training_answer_model->excute($sql);
                    }
                    
                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }


        }
        public function get_all_question(){
            $this->load->model('training_question_model');
            $list = $this->training_question_model->getList();
            echo json_encode($list);

        }
        public function save_question(){
            $title = $this->input->post("title_question");
            $user = (array)$this->user;
            $username = $user["UserName"];
            $userid = $user["ID"];
            $arr = array();
            $ret = array();
            if(empty($title)){
                $arr["ids"] = "title_question";
                $arr["msg"] = "Thông báo\nVui lòng nhập tiêu đề câu hỏi";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $data["title"] = $title;
                $data["author"] = $userid;
                $data["enabled"] = 1;
                $data["datetime"] = date('Y-m-d H:i:s');
                $this->load->model('training_question_model');
                $this->training_question_model->insert($data);
                $arr["ids"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }

        }


        public function add_compile($id=0){

            $this->load->model('training_question_model');
            $this->load->model('training_lesson_model');
            $question = $this->training_question_model->getList();
            $lesson = $this->training_lesson_model->getList($type=3);

            $this->_data["question"] = $question;
            $this->_data["lesson"] = $lesson;
            if($id>0){

                $this->load->model('training_lesson_question_model');
                $data = $this->training_lesson_question_model->getDetail($id);
                $this->_data["data"] = $data;
                $this->template->write_view('content','admin/edit_compile',  $this->_data);
            }else{
               $this->template->write_view('content','admin/add_compile',  $this->_data); 
            }
            


            $this->template->render();
        }
        public function save_compile(){
            $id = $this->input->post("id");
            $lessonid = (int)$this->input->post("lessonid");
            $questionid = (int)$this->input->post("questionid");
            $ret = array();
            $arr = array();
            if($lessonid ==0){
                $arr["id"] = "lessonid";
                $arr["msg"] = "Thông báo.\nVui lòng chọn bài.";
                $ret[] = $arr;
            }
            if($questionid ==0){
                $arr["id"] = "questionid";
                $arr["msg"] = "Thông báo.\nVui lòng chọn câu hỏi.";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $user = (array)$this->user;
                $username = $user["UserName"];
                $userid = $user["ID"];

                $this->load->model('training_lesson_question_model');
                $data["lessonid"] = $lessonid;
                $data["questionid"] = $questionid;
                if($id==0){
                    $data["creator"] = $username;
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $this->training_lesson_question_model->insert($data);
                }else{
                    $data["end_creator"] = $username;
                    $data["end_datetime"] = date('Y-m-d H:i:s');
                    $this->training_lesson_question_model->update($id,$data);
                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }

        public function add_course_user($id=0){
            $this->load->model('user_model');
            $kq = $this->input->post('kq');
            $user_type = $this->input->post('user_type');
            $kq = str_replace("'", "", $kq);
            $this->_data["kq"] = $kq;
            $this->_data["user_type"] = $user_type;

            $ltype_user = $this->user_model->get_user_type();
            $this->_data["ltype_user"] = $ltype_user;


            $luser = $this->user_model->getList2($kq,$user_type);
            $this->_data["luser"] = $luser;


            $this->load->model('training_model');
            $lcourse = $this->training_model->getList();
            $this->_data["lcourse"] = $lcourse;


            if($id>0){
                $this->load->model('training_user_model');
                $data = $this->training_user_model->getDetail($id);
                $this->_data["data"] = $data;
                $this->template->write_view('content','admin/edit_course_user',  $this->_data);
            }else{
               $this->template->write_view('content','admin/add_course_user',  $this->_data); 
            }

            $this->template->render();
        }
        public function save_course_user(){
            $this->load->model('training_user_model');
            $id = (int)$this->input->post("id");
            $userid = $this->input->post("userid");
            $courseid = (int)$this->input->post("courseid");
            $deathline = $this->input->post("deathline");
            if($id>0){
                $detail = $this->training_user_model->getDetail($id);
                $userid = $detail["userid"];
            }
            // var_dump($id,$deathline);exit();
            $ret = array();
            $arr = array();
            
            
            // for($i=0;$i<count($courseid);$i++){
                if($courseid ==0){
                    $arr["id"] = "courseid";
                    $arr["msg"] = "Thông báo.\nVui lòng chọn khóa học.";
                    $ret[] = $arr;
                }
                if($id==0){
                    if($deathline ==null){
                        $arr["id"] = "deathline";
                        $arr["msg"] = "Thông báo.\nVui lòng nhập ngày yêu cầu hoàn tất khóa học.";
                        $ret[] = $arr;
                    }else{
                        if(strtotime($deathline) <strtotime("now")){
                            $arr["id"] = "deathline";
                            $arr["msg"] = "Thông báo.\nNgày yêu cầu hoàn tất khóa học phải lớn hơn ngày hiện tại.";
                            $ret[] = $arr;
                        }
                    }

                    for($i=0;$i<count($userid);$i++){
                        if($courseid >0){
                            $chekc = $this->training_user_model->getDetailByCourse($userid[$i],$courseid);
                            if($chekc !=null){
                                $arr["id"] = "courseid";
                                $arr["msg"] = "Thông báo.\nKhóa học này đã được thêm vào user này.";
                                $ret[] = $arr;
                            }
                        }
                    }
                    
                }
                if($userid ==null){
                    $arr["id"] = "userid";
                    $arr["msg"] = "Thông báo.\nVui lòng chọn user.";
                    $ret[] = $arr;
                }

            // }
            
            
            $now = date('Y-m-d H:i:s');
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $user = (array)$this->user;
                $username = $user["UserName"];
                $creatorid = $user["ID"];
                if($id==0){
                    for($i=0;$i<count($userid);$i++){
                        $deathline = date('Y-m-d',strtotime($deathline))." 23:59:59";
                        $_query[] = "(NULL, $userid[$i], $courseid, '$deathline', $creatorid, '$username', NULL, NULL, '0', '1', '$now')";
                    }
                    $query = count($_query)>0 ? "INSERT INTO ttp_training_user (`id`, `userid`, `courseid`, `deathline`, `creatorid`, `creator`, `startdate`, `enddate`, `status`, `enabled`, `datetime`) VALUES ".implode(",",$_query) : '';
                    if($query!='')
                    $this->training_user_model->excute($query);

                }else{
                    $data["userid"] = $userid;
                    $data["courseid"] = $courseid;
                    $data["deathline"] = date('Y-m-d',strtotime($deathline))." 23:59:59";
                    $this->training_user_model->update($id,$data);
                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }
        public function save_group(){
            $title = $this->input->post("title_group");
            $arr = array();
            $ret = array();
            if($title == null){
                $arr["id"] ="title_group";
                $arr["msg"] = "Thông báo.\nVui lòng nhập tiêu đề";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $data["title"] = $title;
                $this->load->model('training_group_model');
                $this->training_group_model->insert($data);
                $arr["id"] = "ok";
                $arr["msg"] ="ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }
        public function save_level(){
            $title = $this->input->post("title_level");
            $arr = array();
            $ret = array();
            if($title == null){
                $arr["id"] ="title_level";
                $arr["msg"] = "Thông báo.\nVui lòng nhập tiêu đề";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $data["title"] = $title;
                $this->load->model('training_level_model');
                $this->training_level_model->insert($data);
                $arr["id"] = "ok";
                $arr["msg"] ="ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }
        
        
        public function get_group(){
            $this->load->model('training_group_model');
            $list = $this->training_group_model->getList();
            echo json_encode($list);
        }
        public function get_level(){
            $this->load->model('training_level_model');
            $list = $this->training_level_model->getList();
            echo json_encode($list);
        }
        public function get_type($id=0){
            $ret = array(
                "1"=>"Lý thuyết",
                "2"=>"Video",
                "3"=>"Bài kiểm tra"
                );
            if($id>0){
                return $ret[$id];
            }else{
                return $ret;
            }
        }


}
?>