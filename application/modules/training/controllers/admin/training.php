<?php 
class Training extends Admin_Controller { 
 
 	public $user;
 	public $classname="training";
    private $_data;
    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/training_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_title('Training Tools');
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
	public function index(){
        $this->template->add_title('Training Tools');
		$this->template->write_view('content','admin/training_content');
		$this->template->render();
	}
    public function add_course($id=0,$tab=0){
        $class0 = '';
        $class1 = '';
        if($id==0){
            $tab=0;
        }
        
        $none = 'none';
        $this->load->model('training_model');

        $this->load->model('training_part_model');
        $lpart = $this->training_part_model->getList();
        $part_name = array();

        foreach ($lpart as $key => $value) {
            $part_name[$value["id"]] = $value["title"];
        }

        $this->_data["part_name"] = $part_name;


        
        $this->load->model('training_group_model');
        $group = $this->training_group_model->getList();
        $this->_data["group"] = $group;

        $this->load->model('training_level_model');
        $level = $this->training_level_model->getList();
        $this->_data["level"] = $level;

        if($id>0){
            $none='';
            $this->load->model('training_model');
            $detail = $this->training_model->getDetail($id);
            $this->_data["data"] = $detail;
            if(!empty($detail)){
                $this->load->model('training_lesson_model');
                $list_lesson = $this->training_lesson_model->getListByCourseId($detail["id"]);
                $this->_data["list_lesson"] = $list_lesson;
            }else{
                $tab=0;
            }
        }
        $this->_data["none"] = $none;
        $this->load->model('training_chapter_model');
        $list_chapter = $this->training_chapter_model->getList();
        
        $chapter_name = array();
        foreach ($list_chapter as $key => $value) {
            $chapter_name[$value["id"]] = $value["title"];
        }

        $this->_data["chapter_name"] = $chapter_name;

        $type = $this->get_type();
        $this->_data["type"] = $type;
        if($tab ==0){
            $class0 = 'active';
        }
        if($tab ==1){
            $class1 = 'active';
        }
        $this->_data["class0"] = $class0;
        $this->_data["class1"] = $class1;
        
        $this->template->write_view('content','admin/add_course',  $this->_data);
        $this->template->render();
    }
        public function save_course(){
            $this->load->model('training_model');
            $id = (int)$this->input->post("id");
            $title = $this->input->post("title");
            $groupid = $this->input->post("groupid");
            $level = $this->input->post("level");
            $des_full = $this->input->post("full");
            $des_soft = $this->input->post("soft");
            $study = $this->input->post("sstudy");
            $request = $this->input->post("srequest");
            $video = $this->input->post("video");
            $fast_test = (int)$this->input->post("fast_test");
            $point_test = (int)$this->input->post("point_test");
			$user = (array)$this->user;
            $userid = $user["ID"];
			
            $ret = array();
            $arr = array();
            if($title ==null){
                $arr["id"] = "title";
                $arr["msg"] = "- Nhập tiêu đề.";
                $ret[] = $arr;
            }
            if($groupid ==0){
                $arr["id"] = "groupid";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn nhóm khóa học" : "- Chọn nhóm khóa học";
                $ret[] = $arr;
            }
            if($level ==0){
                $arr["id"] = "level";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn cấp độ khóa học" : "- Chọn cấp độ khóa học";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                if(isset($_FILES['Image_upload'])){
					if($_FILES['Image_upload']['error']==0){
						$this->upload_to = 'images_training';
						$img = $this->upload_image_single();
						
						$data["img"] = isset($img) ? $img :'';
					}
				}
                if($id==0){
                    $data["title"] = $title;
					$data["author"] = $userid;
                    $data["groupid"] = $groupid;
                    $data["level"] = $level;
                    $data["des_full"] = $des_full;
                    $data["des_soft"] = $des_soft;
                    $data["study"] = $study;
                    $data["request"] = $request;
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $data["video"] = $video;
                    $data["fast_test"] = $fast_test;
                    $data["point_test"] = $point_test;
                    $this->training_model->insert($data);
                    $id = $this->db->insert_id();
                }else{
                    $data["title"] = $title;
					$data["author"] = $userid;
                    $data["groupid"] = $groupid;
                    $data["level"] = $level;
                    $data["des_full"] = $des_full;
                    $data["des_soft"] = $des_soft;
                    $data["study"] = $study;
                    $data["request"] = $request;
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $data["video"] = $video;
                    $data["fast_test"] = $fast_test;
                    $data["point_test"] = $point_test;
                    $this->training_model->update($id,$data);

                }
                $arr["id"] = $id;
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }

        public function get_course_by_id($courseid){
            $this->load->model('training_part_model');
            $list = $this->training_part_model->getListByScienceId($courseid);
            echo json_encode($list);
        }
        public function get_part_by_id($partid){
            $this->load->model('training_chapter_model');
            $list = $this->training_chapter_model->getListByPartId($partid);
            echo json_encode($list);
        }
        public function get_part(){
            $this->load->model('training_part_model');
            $list = $this->training_part_model->getList();
            echo json_encode($list);
        }
        public function get_chapter(){
            $this->load->model('training_chapter_model');
            $list = $this->training_chapter_model->getList();
            echo json_encode($list);
        }
        public function get_chapter_by_course_by_part($courseid = 0,$partid = 0){
            $this->load->model('training_chapter_model');
            $list = $this->training_chapter_model->getListByCourseByPart($courseid,$partid);
            echo json_encode($list);
        }

        public function add_part($id=0){
            $this->load->model('training_part_model');
            $this->load->model('training_model');
            $list = $this->training_model->getList();
            $this->_data["list"] = $list;
            if($id>0){
                $detail = $this->training_part_model->getDetail($id);
                $this->_data["detail"] = $detail;
            }

            $lpart = $this->training_part_model->getList();
            $this->_data["data"] = $lpart;

            $name_course = array();

            foreach ($list as $key => $value) {
                $name_course[$value["id"]] = $value["title"];
            }
            $this->_data["name_course"] = $name_course;


            $this->template->write_view('content','admin/add_part',  $this->_data);
            $this->template->render();
        }
        public function save_part(){
            $id = (int)$this->input->post("id_part");
            $title = $this->input->post("title_part_new");
            $courseid = $this->input->post("courseid");
            $arr = array();
            $ret = array();
            if(empty($title)){
                $arr["id"] = "title_part_new";
                $arr["msg"] = "Thông báo\nVui lòng nhập tiêu đề phần khóa học.";
                $ret[] = $arr;
            }
            if(empty($courseid)){
                $arr["id"] = "courseid";
                $arr["msg"] = "Thông báo\nVui lòng chọn khóa học.";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return ;
            }else{
                $this->load->model('training_part_model');
                $data["title"] = $title;
                $data["scienceid"] =$courseid;
                if($id==0){
                    // insert data
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $this->training_part_model->insert($data);
                }else{
                    // update data
                    $this->training_part_model->update($id,$data);
                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }

        }
        public function add_chapter($id=0){
            $this->load->model('training_model');
            $course = $this->training_model->getList();
            $this->_data["course"] = $course;
            $this->load->model('training_chapter_model');

            $this->load->model('training_part_model');
            $list = $this->training_part_model->getList();
            $this->_data["list"] = $list;

            if($id>0){
                $detail = $this->training_chapter_model->getDetail($id);
                $this->_data["detail"] = $detail;
                $partid = $detail["partid"];
            }
            $name_course = array();
            $name_part = array();

            foreach ($course as $key => $value) {
                $name_course[$value["id"]] = $value["title"];
            }
            $this->_data["name_course"] = $name_course;

            foreach ($list as $key => $value) {
                $name_part[$value["id"]] = $value["title"];
            }
            $this->_data["name_part"] = $name_part;
            

            $lchapter = $this->training_chapter_model->getList();
            $this->_data["data"] = $lchapter;

            $this->template->write_view('content','admin/add_chapter',  $this->_data);
            $this->template->render();
        }
        public function save_chapter(){
            $id = (int)$this->input->post("id_chapter");
            $title = $this->input->post("title_chapter_new");
            $courseid = $this->input->post("id_course");
            $partid = $this->input->post("id_part");
            $arr = array();
            $ret = array();
            if($title==''){
                $arr["id"] = "title_chapter_new";
                $arr["msg"] = "- Nhập tiêu đề chương khóa học.";
                $ret[] = $arr;
            }
            if(empty($courseid)){
                $arr["id"] = "courseid";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn khóa học." : "- Chọn khóa học." ;
                $ret[] = $arr;
            }
            if(empty($partid)){
                $arr["id"] = "partid";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn phần cho chương." : "- Chọn phần cho chương.";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return ;
            }else{
                $this->load->model('training_chapter_model');
                $data["title"] = $title;
                $data["partid"] =$partid;
                $data["courseid"] =$courseid;
                if($id==0){
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $this->training_chapter_model->insert($data);
                    $id = $this->db->insert_id();
                }else{
                    $this->training_chapter_model->update($id,$data);
                }
                $arr["id"] = $id;
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }

        public function add_lesson($id=0,$courseid=0,$tip=1){
            if(!is_numeric($id)) return;
            $this->load->model('training_model');
            $course = $this->training_model->getList();
            $this->_data["course"] = $course;
            
            $type = $this->get_type();
            $this->_data["type"] = $type;
            $this->load->model('training_question_model');
            $this->_data["list_question"] = $this->db->query("select a.* from ttp_training_question a,ttp_training_lesson_question b where a.id=b.questionid and b.lessonid=$id")->result_array();
            if($id >0){
                $this->load->model('training_lesson_question_model');
                $lesson_question = $this->training_lesson_question_model->getListByLessonId($id);
                if(!empty($lesson_question)){
                    foreach ($lesson_question as $key => $value) {
                        $questionid_active[] = $value["questionid"];
                    }
                    $this->_data["questionid_active"] = $questionid_active;
                }
                
                $this->load->model('training_lesson_model');
                $data = $this->training_lesson_model->getDetail($id);
                $courseid = $data["courseid"];
                $this->_data["data"] = $data;
				
				if($data["hour"]>=60){
                    $hours = $data["hour"]/60;
                    $minutes = $data["hour"]%60;
                }else{
                    $hours = 0;
                    $minutes = 0;
                    if($data["hour"]>0){
                        $minutes = $data["hour"];
                    }
                }
                $this->_data["hours"] = (int) $hours;
                $this->_data["minutes"] = (int) $minutes;
                $display1 = 'none';
                $display2 = 'none';
                $display3 = 'none';
                if($data["type"] ==1){
                    $display1 = '';
                }
                if($data["type"] ==2){
                    $display2 = '';
                }
                if($data["type"] ==3){
                    $display3 = '';
                }
				$history = $this->training_lesson_model->getListHistory($id);
                $this->_data["history"] = $history;
				
                $this->load->model('training_chapter_model');
                $chapter = $this->training_chapter_model->getListByPartId($data["partid"]);
                $this->_data["chapter"] = $chapter;
                $tip = $data["type"];
            }else{
                $display1 = 'none';
                $display2 = 'none';
                $display3 = 'none';
                if($tip == 1){
                    $display1 = '';
                }
                if($tip == 2){
                    $display2 = '';
                }
                if($tip == 3){
                    $display3 = '';
                } 
            }
            
            $this->_data["tip"] = $tip;
            $this->_data["display1"] = $display1;
            $this->_data["display2"] = $display2;
            $this->_data["display3"] = $display3;


            $this->_data["cid"] = $courseid;
            $this->load->model('training_part_model');
            $part = $this->training_part_model->getListByScienceId($courseid);
            $this->_data["part"] = $part;
            
            if($id>0){
                $this->template->write_view('content','admin/edit_lesson',  $this->_data);
            }else{
                $this->template->write_view('content','admin/add_lesson',  $this->_data);
            }
            $this->template->render();
        }
        public function save_lesson(){
            $checks = $this->input->post("checks");
            $id = (int)$this->input->post("id");
            $title = $this->input->post("title");
            $type = $this->input->post("type");
            $video = $this->input->post("video");
            $hour = (int)$this->input->post("hour");
            $minute = (int)$this->input->post("minute");
            $courseid = $this->input->post("courseid");
            $partid = $this->input->post("partid");
            $chapterid = $this->input->post("chapterid");
            $des_soft = $this->input->post("des_soft");
            $des_full = $this->input->post("full");
            $ret =array();
            $arr = array();
            if(empty($title)){
                $arr["id"] = "title";
                $arr["msg"] = "- Nhập tên bài học";
                $ret[] = $arr;
            }
            if(empty($type)){
                $arr["id"] = "type";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn loại bài học" : "- Chọn loại bài học" ;
                $ret[] = $arr;
            }
            
            if(empty($courseid)){
                $arr["id"] = "courseid";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn khóa học" : "- Chọn khóa học" ;
                $ret[] = $arr;
            }
            if(empty($partid)){
                $arr["id"] = "partid";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn phần học" : "- Chọn phần học" ;
                $ret[] = $arr;
            }
            if(empty($chapterid)){
                $arr["id"] = "chapterid";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn chương cho bài học" : "- Chọn chương cho bài học";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $this->load->model('training_lesson_model');
                $this->load->model('training_lesson_question_model');
                $data["title"] = $title;
                $data["type"] = $type;
                $data["video"] = $video;
                $data["enabled"] = 1;
                $data["hour"] = $hour*60+$minute;
                $data["courseid"] = $courseid;
                $data["partid"] = $partid;
                $data["chapterid"] = $chapterid;
                $data["des_soft"] = $des_soft;
                $data["des_full"] = $des_full;
                $now = date('Y-m-d H:i:s');
                $user = (array)$this->user;
                $username = $user["UserName"];
                if($id==0){
					$data["datetime"] = date('Y-m-d H:i:s');
                    $data["creator"] = $username;
                    $lessonid = $this->training_lesson_model->insert($data);
                }else{
					$data["end_datetime"] = date('Y-m-d H:i:s');
                    $data["end_creator"] = $username;
					$history = $this->training_lesson_model->getDetail($id);
                    if($history["title"] !=$data["title"] || $data["type"] != $history["type"] || $data["hour"] != $history["hour"] || $data["des_soft"] != $history["des_soft"] || $data["des_full"] != $history["des_full"]){
                        $this->training_lesson_model->insert_history($history);
                    }
                    $lessonid = $id;
                    $this->training_lesson_model->update($id,$data);
                }

                $arr["id"] = $lessonid;
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }


        }

    public function get_introtext_lesson_by_id($id=0){
        if(is_numeric($id) && $id>0){
            $lesson = $this->db->query("select des_full from ttp_training_lesson where id=$id")->row();
            $data = array(
                'data'=>$lesson
            );
            $this->load->view('admin/preview_lesson',$data);
        }
    }

    public function add_answer($id=0,$cid=0){
        $this->_data["cid"] = $cid;
        if($id>0){
            $this->load->model('training_question_model');
            $data = $this->training_question_model->getDetail($id);
            $this->_data["data"] = $data;

            $this->load->model('training_answer_model');
            $list_answer = $this->training_answer_model->getListByQuestionId($id);

            $this->_data["list_answer"] = $list_answer;
            $this->template->write_view('content','admin/edit_answer',  $this->_data);


        }else{
            $this->template->write_view('content','admin/add_answer',  $this->_data);
        }
        
        $this->template->render();
    }

    public function add_chapter_view($id=0,$ChapterID=0){
        $data = array(
            'id'        =>$id,
            'Part'  =>$this->db->query("select * from ttp_training_part where scienceid=$id")->result(),
            'Chapter'    =>$this->db->query("select * from ttp_training_chapter where id=$ChapterID")->row()
        );
        $this->load->view('admin/add_chapter_view',$data);
    }

    public function add_answer_view($id=0,$QuestionID=0){
        $data = array(
            'id'        =>$id,
            'Lesson'    =>$this->db->query("select * from ttp_training_lesson where id=$id")->row(),
            'Question'  =>$this->db->query("select * from ttp_training_question where id=$QuestionID")->row(),
            'Answer'    =>$this->db->query("select * from ttp_training_answer where questionid=$QuestionID")->result()
        );
        $this->load->view('admin/add_answer',$data);
    }

    public function save_answer(){
        $QuestionID = isset($_POST['QuestionID']) ? $_POST['QuestionID'] : 0 ;
        $id = (int)$this->input->post("id");
        $title = isset($_POST['title']) ? $_POST['title'] : array();
        if(count($title)==0){ echo "False";return false;}
        $true = isset($_POST['IsTrue']) ? $_POST['IsTrue'] : 0 ;
        $title_question = isset($_POST['title_question']) ? $_POST['title_question'] : '' ;
        if($title_question=='') {echo "False";return;}
        $suggestid  = isset($_POST['suggestid']) ? $_POST['suggestid'] : 0 ;
        $arr = array();
        $now = date('Y-m-d H:i:s');
        $lesson  = $this->db->query("select id from ttp_training_lesson where id=$id")->row();
        if($lesson){
            $_query = array();
            if($QuestionID==0){
                $data_question = array(
                    'title'     => $title_question,
                    'suggestid' => $suggestid,
                    'author'    => $this->user->ID,
                    'enabled'   => 1,
                    'datetime'  => $now 
                );
                if(isset($_FILES['Image_upload'])){
                    if($_FILES['Image_upload']['error']==0){
                        $this->upload_to = 'images_training';
                        $img = $this->upload_image_single();
                        $data_question["Image"] = isset($img) ? $img :'';
                    }
                }
                $this->db->insert('ttp_training_question',$data_question);
                $IDQuestion = $this->db->insert_id();
                $i=1;
                foreach($title as $row){
                    $is_true = $true==$i ? 1 : 0 ;
                    $_query[] = "(NULL, '$row', $IDQuestion, $is_true , '1', '$now', NULL, '".$this->user->UserName."', NULL)";
                    $i++;
                }
                $query = count($_query)>0 ? "INSERT INTO ttp_training_answer (id, title, questionid, `true`, enabled
                    , `datetime`, end_datetime, creator, end_creator) VALUES ".implode(",",$_query) : '';
                if($query!='')
                $this->db->query($query);

                $data_relation_ship = array(
                    'lessonid'  =>$id,
                    'questionid'=>$IDQuestion,
                    'enabled'    =>1,
                    'datetime'  =>$now,
                    'creator'   =>$this->user->UserName
                );
                $this->db->insert('ttp_training_lesson_question',$data_relation_ship);
            }else{
                $data_question = array(
                    'title'     => $title_question,
                    'suggestid' => $suggestid
                );
                if(isset($_FILES['Image_upload'])){
                    if($_FILES['Image_upload']['error']==0){
                        $this->upload_to = 'images_training';
                        $img = $this->upload_image_single();
                        $data_question["Image"] = isset($img) ? $img :'';
                    }
                }
                $this->db->where('id',$QuestionID);
                $this->db->update('ttp_training_question',$data_question);
                $i=1;
                foreach($title as $row){
                    $is_true = $true==$i ? 1 : 0 ;
                    $_query[] = "(NULL, '$row', $QuestionID, $is_true , '1', '$now', NULL, '".$this->user->UserName."', NULL)";
                    $i++;
                }
                $this->db->query("delete from ttp_training_answer where questionid=$QuestionID");
                $query = count($_query)>0 ? "INSERT INTO ttp_training_answer (id, title, questionid, `true`, enabled
                    , `datetime`, end_datetime, creator, end_creator) VALUES ".implode(",",$_query) : '';
                if($query!=''){
                    $this->db->query($query);
                }
            }
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
        public function get_all_question(){
            $this->load->model('training_question_model');
            $list = $this->training_question_model->getList();
            echo json_encode($list);

        }
        public function save_question(){
            $title = $this->input->post("title_question");
            $user = (array)$this->user;
            $username = $user["UserName"];
            $userid = $user["ID"];
            $arr = array();
            $ret = array();
            if(empty($title)){
                $arr["ids"] = "title_question";
                $arr["msg"] = "Thông báo\nVui lòng nhập tiêu đề câu hỏi";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $data["title"] = $title;
                $data["author"] = $userid;
                $data["enabled"] = 1;
                $data["datetime"] = date('Y-m-d H:i:s');
                $this->load->model('training_question_model');
                $this->training_question_model->insert($data);
                $arr["ids"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }

        }


        public function add_compile($id=0){

            $this->load->model('training_question_model');
            $this->load->model('training_lesson_model');
            $question = $this->training_question_model->getList();
            $lesson = $this->training_lesson_model->getList($type=3);

            $this->_data["question"] = $question;
            $this->_data["lesson"] = $lesson;
            if($id>0){

                $this->load->model('training_lesson_question_model');
                $data = $this->training_lesson_question_model->getDetail($id);
                $this->_data["data"] = $data;
                $this->template->write_view('content','admin/edit_compile',  $this->_data);
            }else{
               $this->template->write_view('content','admin/add_compile',  $this->_data); 
            }
            


            $this->template->render();
        }
        public function save_compile(){
            $id = $this->input->post("id");
            $lessonid = (int)$this->input->post("lessonid");
            $questionid = (int)$this->input->post("questionid");
            $ret = array();
            $arr = array();
            if($lessonid ==0){
                $arr["id"] = "lessonid";
                $arr["msg"] = "Thông báo.\nVui lòng chọn bài.";
                $ret[] = $arr;
            }
            if($questionid ==0){
                $arr["id"] = "questionid";
                $arr["msg"] = "Thông báo.\nVui lòng chọn câu hỏi.";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $user = (array)$this->user;
                $username = $user["UserName"];
                $userid = $user["ID"];

                $this->load->model('training_lesson_question_model');
                $data["lessonid"] = $lessonid;
                $data["questionid"] = $questionid;
                if($id==0){
                    $data["creator"] = $username;
                    $data["datetime"] = date('Y-m-d H:i:s');
                    $this->training_lesson_question_model->insert($data);
                }else{
                    $data["end_creator"] = $username;
                    $data["end_datetime"] = date('Y-m-d H:i:s');
                    $this->training_lesson_question_model->update($id,$data);
                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }

        public function add_course_user($id=0){
            $this->load->model('user_model');
            $kq = $this->input->post('search_text');
            $kq = str_replace("'", "", $kq);
            $this->_data["kq"] = $kq;

            $this->load->model('training_model');
            $lcourse = $this->training_model->getList();
            $this->_data["lcourse"] = $lcourse;
            $this->_data["luser"] = $this->db->query("select a.*,b.Title from ttp_user a,ttp_department b where a.DepartmentID=b.ID and a.UserName like '%$kq%'")->result();

            if($id>0){
                $this->load->model('training_user_model');
                $data = $this->training_user_model->getDetail($id);
                $this->_data["data"] = $data;
                $this->template->write_view('content','admin/edit_course_user',  $this->_data);
            }else{
               $this->template->write_view('content','admin/add_course_user',  $this->_data); 
            }

            $this->template->render();
        }
        public function save_course_user(){
            $this->load->model('training_user_model');
            $id = (int)$this->input->post("id");
            $userid = isset($_POST['userid']) ? $_POST['userid'] : array() ;
            $courseid = (int)$this->input->post("courseid");
            $deathline = $this->input->post("deathline");
            if($id>0){
                $detail = $this->training_user_model->getDetail($id);
                $userid = $detail["userid"];
            }
            $ret = array();
            $arr = array();
            if($courseid ==0){
                $arr["id"] = "courseid";
                $arr["msg"] = "- Chọn khóa học.".implode(',', $userid);
                $ret[] = $arr;
            }
            if($deathline ==null){
                $arr["id"] = "deathline";
                $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Nhập ngày yêu cầu hoàn tất khóa học." : "- Nhập ngày yêu cầu hoàn tất khóa học." ;
                $ret[] = $arr;
            }else{
                if(strtotime($deathline) <strtotime("now")){
                    $arr["id"] = "deathline";
                    $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Ngày yêu cầu hoàn tất khóa học phải lớn hơn ngày hiện tại." : "- Ngày yêu cầu hoàn tất khóa học phải lớn hơn ngày hiện tại.";
                    $ret[] = $arr;
                }
            }
            if($id==0){
                if($userid ==false){
                    $arr["id"] = "userid";
                    $arr["msg"] = isset($arr["msg"]) ? $arr["msg"]."<br>- Chọn tài khoản để phân bổ khóa học." : "- Chọn tài khoản để phân bổ khóa học.";
                    $ret[] = $arr;
                }else{
                    for($i=0;$i<count($userid);$i++){
                        if($courseid >0){
                            $chekc = $this->training_user_model->getDetailByCourse($userid[$i],$courseid);
                            if($chekc !=null){
                                $arr["id"] = "courseid";
                                $arr["msg"] = "- Chọn các tài khoản chưa được phân bổ cho khóa học này.";
                                $ret[] = $arr;
                            }
                        }
                    }
                }
            }
            $now = date('Y-m-d H:i:s');
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $user = (array)$this->user;
                $username = $user["UserName"];
                $creatorid = $user["ID"];
                if($id==0){
                    for($i=0;$i<count($userid);$i++){
                        $deathline = date('Y-m-d',strtotime($deathline))." 23:59:59";
                        $_query[] = "(NULL, $userid[$i], $courseid, '$deathline', $creatorid, '$username', NULL, NULL, '0', '1', '$now')";
                    }
                    $query = count($_query)>0 ? "INSERT INTO ttp_training_user (`id`, `userid`, `courseid`, `deathline`, `creatorid`, `creator`, `startdate`, `enddate`, `status`, `enabled`, `datetime`) VALUES ".implode(",",$_query) : '';
                    if($query!='')
                    $this->training_user_model->excute($query);

                }else{
                    $data["userid"] = $userid;
                    $data["courseid"] = $courseid;
                    $data["deathline"] = date('Y-m-d',strtotime($deathline))." 23:59:59";
                    $this->training_user_model->update($id,$data);
                }
                $arr["id"] = "ok";
                $arr["msg"] = "ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }
        public function save_group(){
            $title = $this->input->post("title_group");
            $arr = array();
            $ret = array();
            if($title == null){
                $arr["id"] ="title_group";
                $arr["msg"] = "Thông báo.\nVui lòng nhập tiêu đề";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $data["title"] = $title;
				$data["datetime"] = date('Y-m-d H:i:s');
                $this->load->model('training_group_model');
                $this->training_group_model->insert($data);
                $arr["id"] = "ok";
                $arr["msg"] ="ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }
        public function save_level(){
            $title = $this->input->post("title_level");
            $arr = array();
            $ret = array();
            if($title == null){
                $arr["id"] ="title_level";
                $arr["msg"] = "Thông báo.\nVui lòng nhập tiêu đề";
                $ret[] = $arr;
            }
            if(count($ret)>0){
                echo json_encode($ret);
                return;
            }else{
                $data["title"] = $title;
				$data["datetime"] = date('Y-m-d H:i:s');
                $this->load->model('training_level_model');
                $this->training_level_model->insert($data);
                $arr["id"] = "ok";
                $arr["msg"] ="ok";
                $ret[] = $arr;
                echo json_encode($ret);
            }
        }
        public function get_group(){
            $this->load->model('training_group_model');
            $list = $this->training_group_model->getList();
            echo json_encode($list);
        }
        public function get_level(){
            $this->load->model('training_level_model');
            $list = $this->training_level_model->getList();
            echo json_encode($list);
        }
        public function get_type($id=0){
            $ret = array(
                "1"=>"Lý thuyết",
                "2"=>"Video",
                "3"=>"Bài kiểm tra"
                );
            if($id>0){
                return $ret[$id];
            }else{
                return $ret;
            }
        }
        public function save_order_lesson(){
            $this->load->model('training_lesson_model');
            $orderby = $this->input->post('orderby');
            $courseid = $this->input->post('id');
            $list = $this->training_lesson_model->getListByCourseId($courseid);
            for($i=0;$i<count($list);$i++){
                $ids = $list[$i]["id"];
                $data["orderby"] = (int)$orderby[$i];
                $this->training_lesson_model->update($ids,$data);
            }
            $arr["id"] = "ok";
            $arr["msg"] ="ok";
            $ret[] = $arr;
            echo json_encode($ret);
        }

    
    public function add_new_chapter(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : 'a' ;
        $CoursesID = isset($_POST['CoursesID']) ? $_POST['CoursesID'] : 1 ;
        $PartID = isset($_POST['PartID']) ? $_POST['PartID'] : 1 ;
        if($Title!='' && $CoursesID>0 && $PartID>0){
            $data = array(
                'courseid' =>$CoursesID,
                'partid'    =>$PartID,
                'title'     =>$Title,
                'datetime'  =>date('Y-m-d H:i:s'),
                'enabled'   =>1
            );
            $this->db->insert('ttp_training_chapter',$data);
            $ID = $this->db->insert_id();
            echo $ID;
        }else{
            echo "False";
        }
    }

    public function update_chapter(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $CoursesID = isset($_POST['CoursesID']) ? $_POST['CoursesID'] : 0 ;
        $PartID = isset($_POST['PartID']) ? $_POST['PartID'] : 0 ;
        if($Title!='' && $CoursesID>0 && $PartID>0){
            $data = array(
                'title'     =>$Title,
                'courseid' =>$CoursesID,
                'partid'    =>$PartID
            );
            $this->db->where('ID',$ID);
            $this->db->update('ttp_training_chapter',$data);
            echo $Title;
        }else{
            echo "False";
        }
    }

    public function add_new_part(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $CoursesID = isset($_POST['CoursesID']) ? $_POST['CoursesID'] : 0 ;
        if($Title!='' && $CoursesID>0){
            $data = array(
                'scienceid' =>$CoursesID,
                'title'     =>$Title,
                'datetime'  =>date('Y-m-d H:i:s'),
                'enabled'   =>1
            );
            $this->db->insert('ttp_training_part',$data);
            $ID = $this->db->insert_id();
            echo $ID;
        }else{
            echo "False";
        }
    }

    public function update_part(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $CoursesID = isset($_POST['CoursesID']) ? $_POST['CoursesID'] : 0 ;
        if($Title!=''){
            $data = array(
                'scienceid' =>$CoursesID,
                'title'     =>$Title
            );
            $this->db->where('ID',$ID);
            $this->db->update('ttp_training_part',$data);
            echo $Title;
        }else{
            echo "False";
        }
    }

    public function add_new_group(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        if($Title!=''){
            $data = array(
                'title'=>$Title,
                'datetime'=>date('Y-m-d H:i:s'),
                'enabled'=>1
            );
            $this->db->insert('ttp_training_group',$data);
            $ID = $this->db->insert_id();
            echo $ID;
        }else{
            echo "False";
        }
    }

    public function update_group(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        if($Title!='' && $ID>0){
            $data = array(
                'title'=>$Title,
            );
            $this->db->where('ID',$ID);
            $this->db->update('ttp_training_group',$data);
            echo $Title;
        }else{
            echo "False";
        }
    }

    public function add_new_level(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        if($Title!=''){
            $data = array(
                'title'=>$Title,
                'datetime'=>date('Y-m-d H:i:s'),
                'enabled'=>1
            );
            $this->db->insert('ttp_training_level',$data);
            $ID = $this->db->insert_id();
            echo $ID;
        }else{
            echo "False";
        }
    }

    public function update_level(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        if($Title!='' && $ID>0){
            $data = array(
                'title'=>$Title,
            );
            $this->db->where('ID',$ID);
            $this->db->update('ttp_training_level',$data);
            echo $Title;
        }else{
            echo "False";
        }
    }

    public function remove_question($id=0){
        $this->db->query("delete from ttp_training_question where id=$id");
        $this->db->query("delete from ttp_training_answer where questionid=$id");
        $this->db->query("delete from ttp_training_lesson_question where questionid=$id");
    }

    public function del_part(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        if($ID){
            $this->db->query("delete from ttp_training_part where id=$ID");
            $this->db->query("delete from ttp_training_chapter where partid=$ID");
            echo "OK";
        }else{
            echo "False";
        }
    }

    public function del_chapter($ID=0){
        if($ID>0){
            $this->db->query("delete from ttp_training_chapter where id=$ID");
            echo "OK";
        }else{
            echo "False";
        }
    }

}
?>