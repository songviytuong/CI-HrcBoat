<?php 
$url = base_url().ADMINPATH.'/training';
$arr_usertype = array(
    0   =>"Website User",
    1   =>"Nhân viên CC",
    2   =>"Nhân viên kho",
    3   =>"Nhân viên kế toán",
    4   =>"Nhân viên điều phối",
    6   =>"Nhân viên CM",
    5   =>"Manager CC",
    7   =>"Manager kế toán",
    8   =>"Manager kho",
    9   =>"CEO",
    10  =>"Sale MT - GT"
);
?>
<style type="text/css" src></style>
<div class="containner">
    <!-- warning message -->
    <div class="alert alert_error hidden"><h5></h5><p></p></div>  
    <div class="manager">
        <form id="add_course_user" action="" method="POST">
            <div class="import_select_progress">
                <div class="block1">
                    <h1>Phân bổ khóa học</h1>
                </div>
                <div class="block2">
                    <button type="button" onclick="saves(this);return false;" align="right" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Phân bổ khóa học</button>
                    <a onclick="goBack();return false;" align="right" class="btn btn-danger"><i class="fa fa-times"></i> Đóng</a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="col-xs-3 control-label ">Khóa học:</label>
                        <div class="col-xs-9">
                            <select class="form-control" name="courseid" id="courseid">
                                <option value="0">-- Chọn khóa học --</option>
                                    <?php foreach ($lcourse as $key => $value) {?>
                                    <option value="<?php echo $value["id"] ?>"><?php echo $value["title"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-3 control-label ">Ngày yêu cầu:</label>
                        <div class='input-group col-xs-9'>
                            <div class="input-group-addon ">
                                <i class="fa fa-calendar"></i>
                              </div>
                            <input name="deathline" id="deathline" type='text' placeholder='mm/dd/YY' class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
            <h3>DANH SÁCH TÀI KHOẢN</h3>
            <div class="row">
                <div class="col-xs-3 pull-left">
                    <select class="form-control" id="department" onchange="show_rowtable(this)">
                        <option value='a'>-- Chọn phòng ban --</option>
                        <?php 
                        $department = $this->db->query("select * from ttp_department")->result();
                        if(count($department)>0){
                            foreach($department as $row){
                                echo "<option value='$row->ID'>$row->Title</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-3 pull-left">
                    <select class="form-control" id="usertype" onchange="show_rowtable(this)">
                        <option value='a'>-- Chọn loại tài khoản --</option>
                        <?php 
                        foreach($arr_usertype as $key=>$row){
                            echo "<option value='$key'>$row</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-3 pull-right">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" id="search_text_temp" class="form-control" placeholder="Tìm kiếm tài khoản ..." />
                            <span class="input-group-btn"> <a type="submit" class="btn btn-default"><i class="fa fa-search"></i></a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table_data">
                <table id="table_data">
                    <tr>
                        <th class="th_stt"><input type="checkbox" id="checkAll"></input></th>
                        <th class="th_stt">STT</th>
                        <th>Tên tài khoản</th>
                        <th class="th_action">Loại tài khoản</th>
                        <th class="th_action">Phòng ban</th>
                    </tr>
                    <?php
                    $stt =0;
                     foreach ($luser as $row) {
                        ?>
                        <tr class="data_row data_department_<?php echo $row->DepartmentID ?> data_usertype_<?php echo $row->UserType ?>">
                        <td> <input type="checkbox" value="<?php echo $row->ID ?>" name="userid[]" ></input></td>
                        <td> <?php echo ++$stt; ?></td>
                        <td><?php echo $row->UserName ?></td>
                        <td><?php echo isset($arr_usertype[$row->UserType]) ? $arr_usertype[$row->UserType] : '--' ; ?></td>
                        <td><?php echo $row->Title ?></td>
                        </tr>
                    <?php } 
                        echo "<tr><td colspan='5'>Tổng số lượng tài khoản : <b>".number_format(count($luser))."</b> tài khoản</td></tr>";
                    ?>
                </table>
            </div>
            <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <input type="hidden" name="id" id="id" value="<?php echo isset($data["id"]) ? $data["id"] : 0; ?>"></input>
        </form>
        <form action="" method="post" id="search_form">
            <input type="hidden" name="search_text" id="search_text" class="form-control" placeholder="Tìm kiếm tài khoản ..." />
        </form>
    </div>
</div>
<style>.daterangepicker{width:auto;}</style>
<script type="text/javascript">
    $( document ).ready(function() {
        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });

        $('#search_text_temp').bind("enterKey",function(e){
            var data = $(this).val();
            $("#search_text").val(data);
            $("#search_form").submit();
        });
        $('#search_text_temp').keyup(function(e){
            if(e.keyCode == 13)
            {
              $(this).trigger("enterKey");
            }
        });
    });
    
    function goBack() {
        window.history.back();
    }
    
    function saves(ob){
        $(ob).addClass('saving');
        $(ob).find('i').hide();
        var link = $('#link').val();
        $.post(link+"/training/save_course_user/", $("#add_course_user").serialize(),function(resp){
            $.each(resp, function (i, obj) {
                var id = obj.id;
                var msg = obj.msg;
                if(msg != "ok"){
                    $(".alert_error").removeClass('alert-success').addClass("alert-danger").removeClass('hidden');
                    $(".alert_error h5").html("<i class='fa fa-exclamation-circle'></i> Bạn cần bổ sung các thông tin dưới đây : ").show();
                    $(".alert_error p").html(msg);
                    $('#' + id).focus();
                    $(ob).removeClass('saving');
                    $(ob).find('i').show();
                }else{
                    $(".alert_error").removeClass('alert-danger').addClass("alert-success").removeClass('hidden');
                    $(".alert_error h5").hide();
                    $(".alert_error p").html("<i class='fa fa-check-circle'></i> Phân bổ khóa học thành công !");
                    $(ob).removeClass('saving');
                    $(ob).find('i').show();
                    $("form").get(0).reset();
                }
            });

        }, 'json');
    }


    $(document).ready(function(){
        $('#deathline').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

    /*
    var data = $("#table_data tr").length-2;
    $(".alert_error p").html("Tổng số lượng tài khoản nhân viên <?php echo $kq!='' ? "phù hợp kết quả '<b>$kq</b>'" : '' ?> : <b>"+data+"</b> tài khoản");
    */
    function show_rowtable(ob){
        
        var department = $("#department").val();
        var usertype = $("#usertype").val();
        var data_fillter = 0;
        $(".data_row").hide();
        if(department=='a' && usertype=='a'){
            $(".data_row").show();
            data_fillter = $(".data_row").length;
        }else{
            if(usertype!='a' && department!='a'){
                $(".data_department_"+department+".data_usertype_"+usertype).show();
                data_fillter = data_fillter+$(".data_department_"+department+".data_usertype_"+usertype).length;
            }else{
                if(usertype!='a'){
                    $(".data_usertype_"+usertype).show();
                    data_fillter = $(".data_usertype_"+usertype).length;
                }
                if(department!='a'){
                    $(".data_department_"+department).show();
                    data_fillter = data_fillter+$(".data_department_"+department).length;
                }
            }
        }
        $(".alert_error p").html("Kết quả tìm kiếm : <b>"+data_fillter+"</b> / <b>"+data+"</b> tài khoản ");
    }

    
</script>