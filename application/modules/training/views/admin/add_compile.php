<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">
<div class="col-md-6">
        <form action="" method="POST">
            <div class="fillter_bar">
                <div class="block1">
                    <h1>Biên soạn bài kiểm tra</h1>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3"  class="col-sm-3 control-label ">Bài:</label>
                <div class="input-group">
                  <select class="form-control" name="lessonid" id="lessonid">
                        <option value="0">Lựa chọn bài kiểm tra</option>
                            <?php foreach ($lesson as $key => $value) {?>
                            <option value="<?php echo $value["id"] ?>"><?php echo $value["title"] ?></option>
                        <?php } ?>
                        </select>
                        <div class="input-group-addon add_question">
                            <!-- <i class="fa fa-plus"></i> -->
                          </div>
                </div>
            </div>

            <div class="clear"></div>
            <div class="form-group">
                <label for="inputEmail3"  class="col-sm-3 control-label ">Câu hỏi:</label>
                <div class="input-group">
                  <select class="form-control" name="questionid" id="questionid">
                        <option value="0">Lựa chọn câu hỏi</option>
                            <?php foreach ($question as $key => $value) {?>
                            <option value="<?php echo $value["id"] ?>"><?php echo $value["title"] ?></option>
                        <?php } ?>
                        </select>
                        <div class="input-group-addon add_question">
                            <!-- <i class="fa fa-plus"></i> -->
                          </div>
                </div>
            </div>
            <div class="box-footer">
                <button id="save" type="button" onclick="saves();return false;" class="btn btn-primary">Save</button>
            </div>
            <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <input type="hidden" name="id" id="id" value="<?php echo isset($data["id"]) ? $data["id"] : 0; ?>"></input>
        </form>
</div>




</div>
</form>
<script type="text/javascript">
    function saves(){
        var link = $('#link').val();
        $.post(link+"/training/save_compile/", $("form").serialize(),function(resp){
            $.each(resp, function (i, obj) {
                var id = obj.id;
                var msg = obj.msg;
                if(id != "ok"){
                    alert(msg);
                    $('#' + id).focus();
                    return false;
                }else{
                    alert('Lưu thành công');
                    location.reload(true);
                }

            });

        }, 'json');
        
    }
</script>