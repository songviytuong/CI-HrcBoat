<?php 
$url = base_url().ADMINPATH.'/training';
?>
<div class="containner">
	<div class="training-box">
		<div class="row1"> 
			<div class="col-xs-12">
				<h1 class='title_row'><?php echo $detail["title"]; ?></h1>
				<div class='description'><?php echo $detail["des_soft"] ?></div>
			</div>
		</div>
		<div class="row2 col-xs-12">
				<p>Người soạn : <?php echo isset($detail_us["LastName"]) ? $detail_us["LastName"] : ''; ?></p>
				<p><i class="fa fa-book"></i> <?php echo isset($name_group) ? $name_group : '' ?></p>
		</div>
		<div class="row3 col-xs-12">
			<img src="<?php echo $detail["img"]; ?>">
			<div class="description_box">
				<p>Ngày hết hạn: <?php echo isset($detail["deathline"]) ? date('d/m/Y',strtotime($detail["deathline"])) : ''; ?></p>
				<div class='date_title'>
					<a href="<?php echo $url ?>/learn/study/<?php echo $detail["id"] ?>" class="btn btn-primary btn-lg"><i class="fa fa-arrow-right"></i> Tham gia ngay </a>
					<?php if($detail["fast_test"]==1){ ?>
					<a href="<?php echo $url ?>/learn/study/<?php echo $detail["id"] ?>/<?php echo $detail["point_test"] ?>" class="btn btn-danger btn-lg"><i class="fa fa-pencil"></i> Kiểm tra nhanh </a>
					<?php }?>
				</div>
				<div class="description_title">
				<?php
					$status = '';
					if($detail["startdate"] !=null && $detail["enddate"] !=null ){
						$status = 'Hoàn thành';
					}
					if($detail["startdate"] !=null && $detail["enddate"] == null){
						$status = 'Đang học';
					}
					if($detail["startdate"] == null && $detail["enddate"] == null ){
						$status = 'Chưa bắt đầu';
					}

					$hour = isset($total_time) ? round($total_time/60,0) : 0;
	    			$minus = isset($total_time) ? $total_time%60 : 0;
	    			$minus = $minus<10 ? '0'.$minus : $minus ;
				?>
					<p><b>Tình trạng :</b> <?php echo $status; ?>.</p>
					<p><b>Lý thuyết :</b> <?php echo isset($counts[1]) ? $counts[1] : ''; ?></p>
					<p> <b>Video :</b><?php echo isset($counts[2]) ? $counts[2] : ''; ?></p>
					<p> <b>Kiểm tra :</b> <?php echo isset($counts[3]) ? $counts[3] : ''; ?> bài</p>
					<p> <b>Trình độ :</b> <?php echo isset($name_level) ? $name_level :'' ?></p>
					<p> <b>Tổng thời lượng :</b> <?php echo $hour.' giờ '.$minus.' phút' ?></p>
				</div>
			</div>
		</div>
		<div class="row4 col-xs-12">
			<h4 class='title_row'><i class="fa fa-star"></i> Mô tả khóa học</h4>
			<p><?php echo $detail["des_full"]; ?></p>
		</div>
		<div class="row5 col-xs-12">
			<h4 class='title_row'><i class="fa fa-star"></i> Bạn sẽ học được gì?</h4>
			<?php echo $detail["study"]; ?>
		</div>
		<div class="row6 col-xs-12">
			<h4 class='title_row'><i class="fa fa-star"></i> Yêu cầu tham gia</h4>
			<?php echo $detail["request"]; ?>
		</div>
		<div class="row7 col-xs-12">
			<h4 class='title_row'><i class="fa fa-star"></i> Nội dung</h4>
			<div class="row">
			<?php foreach ($list as $key => $value) { ?>
					<div class='col-xs-3'>
						<div class='list'>
						<h3><?php echo $value["title"]; ?></h3>
							    <?php if($value["arr_chapter"] !=null) {
							    echo "<div class='scroll-content'>";
							    foreach ($value["arr_chapter"] as $key2 => $value2) {
						    		echo "<p class='p_primary'>".$value2["title"]."</p>";
						    		$k = isset($value["lesson"][$value2["id"]][$value["id"]]) ? $value["lesson"][$value2["id"]][$value["id"]] : '';
					    			if(!empty($k)){
						    			foreach ($k as $key3 => $value3) {
						    				$bh = $key3+1;
						    				echo "<p class='p_sub'>Bài ".$bh." : ".$value3."</p>";
							    		}
						    		}
							    }
							    echo "</div>";
						    }?>
						</div>
					</div>
			<?php } ?>
			</div>
		</div>
	</div>
</div>
<script>
    function welcome(){
        $(".welcome_title").addClass("animated");
        $(".welcome_title").addClass("fadeInDown");
    }

    setTimeout(welcome,500);
</script>
<style>
	/*.body_content .containner{min-height: 569px !important;}*/
</style>