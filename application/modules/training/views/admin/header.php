<?php 
$current = $this->uri->segment(3);
$current = explode('_',$current);
$current = isset($current[0]) ? $current[0] : '' ;
$current_training = $this->uri->segment(2);
?>
<div class="header">
    <div class="block1">
        <a href="<?php echo base_url().ADMINPATH."/report" ?>">
        <img src='public/admin/images/logo.png' />
        <p class='p1'>DIVASHOP</p>
        <p class='p2'>Report Tool</p>
        </a>
    </div>
    <div class="block2">
        <ul>
            <li><a href='<?php echo base_url().ADMINPATH."/training" ?>' <?php echo $current_training=='training' ? "class='current_group'" : "" ; ?>>Cá nhân</a></li>
            <li><a href='<?php echo base_url().ADMINPATH."/report/report" ?>' <?php echo $current=='report' ? "class='current_group'" : "" ; ?>>Báo cáo</a></li>
            <li><a href='<?php echo base_url().ADMINPATH."/report/manager" ?>' <?php echo $current=='manager' ? "class='current_group'" : "" ; ?>>Quản trị</a></li>
            <li><a href='<?php echo base_url().ADMINPATH."/report/import" ?>' <?php echo $current=='import' || $current=='request' ? "class='current_group'" : "" ; ?>>Bán hàng</a></li>
            <li><a href='<?php echo base_url().ADMINPATH."/report/warehouse" ?>' <?php echo $current=='warehouse' ? "class='current_group'" : "" ; ?>>Kho hàng</a></li>
        </ul>
    </div>
    <div class="info_user">
        <ul>
        <?php 
        $checkleader = $this->db->query("select ID from ttp_report_team where UserID=".$this->user->ID)->row();
        $checkleader = $checkleader ? "TEAM LEADER" : "" ;
        $checkleader = $this->user->IsAdmin==1 ? "Developer" : $checkleader ;
        $checkleader = $this->user->UserType==1 ? "NV Telesales" : $checkleader ;
        $checkleader = $this->user->UserType==2 ? "NV KHO" : $checkleader ;
        $checkleader = $this->user->UserType==3 ? "NV KẾ TOÁN" : $checkleader ;
        $checkleader = $this->user->UserType==4 ? "NV ĐIỀU PHỐI" : $checkleader ;
        $checkleader = $this->user->UserType==5 ? "CRM MANAGER" : $checkleader ;
        $checkleader = $this->user->UserType==6 ? "NV CM" : $checkleader ;
        $checkleader = $this->user->UserType==7 ? "KẾ TOÁN MANAGER" : $checkleader ;
        $checkleader = $this->user->UserType==8 ? "KHO MANAGER" : $checkleader ;
        $checkleader = $this->user->UserType==9 ? "CEO TTP GROUP" : $checkleader ;
        echo file_exists($this->user->Thumb) ? "<a href='".base_url().ADMINPATH."/home/profile"."'><li class='li1'><img src='".$this->user->Thumb."' /></li></a>" : "<a href='".base_url().ADMINPATH."/home/profile"."'><li class='li1'><img src='public/admin/images/user.png' /></li></a>";
        echo "<li class='li2'><a href='".base_url().ADMINPATH."/home/profile"."'>".$this->user->UserName." <br><span>$checkleader</span></a></li>";
        echo "<li class='li3'><a href='".base_url().ADMINPATH."/logout"."'>Logout <i class='fa fa-sign-out pull-right'></i></a></li>";
        ?>
        </ul>
    </div>
</div>
