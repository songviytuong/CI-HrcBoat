<style type="text/css" src></style>
<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">
    <!-- warning message -->
    <div class="alert alert_error hidden"><h5></h5><p></p></div>  
    <div class="manager">
        <form action="" method="POST">
            <div class="import_select_progress">
                <div class="block1">
                    <h1>Phân bổ khóa học</h1>
                </div>
                <div class="block2">
                    <button type="button" onclick="saves(this);return false;" align="right" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Lưu thay đổi</button>
                    <a onclick="goBack();return false;" align="right" class="btn btn-danger"><i class="fa fa-times"></i> Đóng</a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="col-xs-3 control-label ">Tài khoản:</label>
                        <div class="col-xs-9">
                        <select class="form-control" disabled="true" name="userid" id="userid">
                            <option value="0">Lựa chọn user</option>
                                <?php foreach ($luser as $row) {
                                    $sl='';
                                    if($data["userid"] == $row->ID){
                                        $sl = 'selected="selected"';
                                    }
                                    ?>
                                <option <?php echo $sl; ?> value="<?php echo $row->ID ?>"><?php echo $row->UserName ?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="col-xs-3 control-label ">Khóa học:</label>
                        <div class="col-xs-9">
                            <select class="form-control" name="courseid" id="courseid">
                                <option value="0">Lựa chọn khóa học</option>
                                    <?php foreach ($lcourse as $key => $value) {
                                        $sl='';
                                        if($data["courseid"] == $value["id"]){
                                            $sl = 'selected="selected"';
                                        }

                                        ?>
                                    <option <?php echo $sl; ?> value="<?php echo $value["id"] ?>"><?php echo $value["title"] ?></option>
                                <?php } ?>
                            </select>    
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="col-xs-3 control-label ">Ngày yêu cầu:</label>
                        <div class="col-xs-9">
                        <div class='input-group'>
                            <div class="input-group-addon ">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                            <input value="<?php echo isset($data["deathline"])? date('Y-m-d',strtotime($data["deathline"])):'' ?>" name="deathline" id="deathline" type='text' class="form-control" />
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <input type="hidden" name="id" id="id" value="<?php echo isset($data["id"]) ? $data["id"] : 0; ?>"></input>
        </form>
    </div>
</div>
<style>.daterangepicker{width:auto;}</style>
<script type="text/javascript">
    function goBack() {
        window.history.back();
    }
    function saves(ob){
        $(ob).addClass('saving');
        $(ob).find('i').hide();
        var link = $('#link').val();
        $.post(link+"/training/save_course_user/", $("form").serialize(),function(resp){
            $.each(resp, function (i, obj) {
                var id = obj.id;
                var msg = obj.msg;
                if(msg != "ok"){
                    $(".alert_error").removeClass('alert-success').addClass("alert-danger").removeClass('hidden');
                    $(".alert_error h5").html("<i class='fa fa-exclamation-circle'></i> Bạn cần bổ sung các thông tin dưới đây : ").show();
                    $(".alert_error p").html(msg);
                    $('#' + id).focus();
                    $(ob).removeClass('saving');
                    $(ob).find('i').show();
                }else{
                    $(".alert_error").removeClass('alert-danger').addClass("alert-success").removeClass('hidden');
                    $(".alert_error h5").hide();
                    $(".alert_error p").html("<i class='fa fa-check-circle'></i> Phân bổ khóa học thành công !");
                    $(ob).removeClass('saving');
                    $(ob).find('i').show();
                    $("form").get(0).reset();
                }
            });

        }, 'json');
        
    }


    $(document).ready(function(){
        $('#deathline').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });


        $(".select2").select2();
        // var num =0;
        // var c =1;
        // $('.add_answer').click(function (e) {
        //     num++;
        //     c++;
        //     e.preventDefault();
        //     var html = "<div class='blck"+num+"'><div class='clear'></div><div class=form-group'><span for='inputEmail3'  class='col-sm-3 control-label '>Câu trả lời "+c+":</span><div class='input-group  col-xs-9'><input type='text' placeholder='Nhập câu trả lời...' class='form-control input-sm' name='title[]' id='title' ></input><div class='input-group-addon' onclick='removes("+num+");return false;'><i class='fa fa-times'>Xóa</i></div></div></div><div class='clear'></div><div class='form-group'><span for='inputEmail3'  class='col-sm-3 control-label '>Đáp án "+c+":</span><div class=col-xs-9'><span><input checked type='radio' name='trues"+num+"' id='trues' value='0'></input> Sai</span><span><input type='radio' name='trues"+num+"' id='trues' value='1'></input> Đúng</span></div></div></div>";
        //     $('.addanswer').append(html);

        // });




        // $(".add_question").click(function(){
        //     $('.question').show();
        // });


        // $(".save_newquestion").click(function(){
        //     var link = $('#link').val();
        //     $.post(link+"/training/save_question/", $("#newquestion").serialize(),function(resp){
        //     $.each(resp, function (i, obj) {
        //         var ids = obj.ids;
        //         var msg = obj.msg;
        //         if(ids != "ok"){
        //             alert(msg);
        //             $('#' + ids).focus();
        //             return false;
        //         }else{
        //             var url = link+"/training/get_all_question";
        //             $('#questionid').html('<option value="0">Lựa chọn câu hỏi</option>');
        //             $.ajax({
        //                   dataType: "json",
        //                   type: "GET",
        //                   url: url,
        //                   data: "",
        //                   success: function(resp){
        //                     $.each(resp,function(i,obj){
        //                         var html = "<option value='"+resp[i].id+"'> "+resp[i].title+" </option>";
        //                         $('#questionid').append(html);
        //                     });
                            

        //                   }
        //               });


        //             alert('Lưu thành công');
        //         }

        //     });

        // }, 'json');

        // });
        
    });
</script>