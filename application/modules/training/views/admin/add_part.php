<?php 
$url = base_url().ADMINPATH.'/training';
?>
<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">

        <form action="" method="POST">
        <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <div class="box-header">
                <div class="col-md-8">
                    <b class="sz20">THÔNG TIN PHẦN HỌC</b>
                </div>
                <div align="right" class="col-md-4">
                    <button type="button" onclick="saves();return false;" align="right" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"> LƯU THAY ĐỔI</i></button>
                    <a target="_blank" onclick="goBack();" align="right" class="btn btn-sm btn-danger"><i class="fa fa-times"> ĐÓNG</i></a>
                </div>
            </div>

            <div class="clear"></div>
            <div class="clear"></div>
            <div class="col-md-9">
            <div class="form-group">
                <span for="inputEmail3"  class="col-sm-3 control-label ">Tên phần:</span>
                <div class="col-md-9 input-group">
                <div class="input-group-addon "><i class="fa fa-pencil-square-o"></i></div>
                    <input type='text' placeholder="Ví dụ : Phần I" class="form-control input-sm" value="<?php echo isset($detail["title"]) ? $detail["title"] : ''; ?>" id="title_part_new" name='title_part_new' />
                </div>
            </div>
            <div class="clear"></div>
            <div class="form-group">
                <span for="inputEmail3"  class="col-sm-3 control-label ">Tên khóa học:</span>
                <div class="col-md-9 input-group">
                <div class="input-group-addon "><i class="fa fa-pencil-square-o"></i></div>
                    <select class="form-control input-sm" name="courseid" id="courseid">
                        <option value="0">Chọn Khóa học</option>
                        <?php foreach ($list as $key => $value) {
                            $sl ='';
                            if($detail["scienceid"] == $value["id"]){
                                $sl = 'selected="selected"';
                            }
                            ?>
                            <option <?php echo $sl; ?> value="<?php echo $value["id"]; ?>"><?php echo $value["title"]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="clear"></div>
            </div>
            <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <input type="hidden" name="id_part" id="id_part" value="<?php echo isset($detail["id"]) ? $detail["id"] : 0; ?>"></input>

            <div class="table_data">
                <table>
                    <tr>
                        <th>STT</th>
                        <th>Tên phần</th>
                        <th>Khóa học</th>
                        <th>Thao tác</th>
                    </tr>
                    <?php
                        $stt=0;
                        foreach ($data as $key => $value) {?>
                            <tr>
                            <td><?php echo ++$stt; ?></td>
                            <td><?php echo $value["title"]; ?></td>
                            <td><?php echo $name_course[$value["scienceid"]]; ?></td>
                            <td><a href="<?php echo $url;?>/training/add_part/<?php echo $value["id"];?>"><i class="fa fa-pencil-square-o"></i></a><i class="fa fa-trash-o" onclick="del(<?php echo $value["id"];?>);return false;"> </i></td>
                            </tr>

                        <?php }?>
                </table>

            </div>
        </form>
</div>

<script type="text/javascript">
function del(id){
    var result = confirm('Bạn có chắc chắn muốn xóa không');
    if(result){
        var link = $('#link').val();
        $.post(link+"/course/delete_part/"+id, $("form").serialize(),function(resp){
            alert('Xóa thành công');
            window.location = link+'/training/add_part';
            // location.reload();
        });
    }
}

function goBack() {
        window.history.back();
    }
    function saves(){
        var link = $('#link').val();
        $.post(link+"/training/save_part/", $("form").serialize(),function(resp){
            $.each(resp, function (i, obj) {
                var id = obj.id;
                var msg = obj.msg;
                if(id != "ok"){
                    alert(msg);
                    $('#' + id).focus();
                    return false;
                }else{
                    alert('Lưu thành công');
                    location.reload(true);
                }

            });

        }, 'json');
        
    }

    $(document).ready(function(){
        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview");
            if(file.type.match(imageType)){
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:100px;max-width: 100px");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            }else{
                console.log("Not an Image");
            }
        });
    });
</script>