<?php 
$url = base_url().ADMINPATH.'/training';
?>
<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">

        <form action="" method="POST">
        <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <div class="box-header">
                <div class="col-md-8">
                    <b class="sz20">THÊM CÂU HỎI</b>
                </div>
                <div align="right" class="col-md-4">
                    <button type="button" onclick="saves();return false;" align="right" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"> LƯU THAY ĐỔI</i></button>
                    <a target="_blank" onclick="goBack();" align="right" class="btn btn-sm btn-danger"><i class="fa fa-times"> ĐÓNG</i></a>
                </div>
            </div>
            <div class="clear"></div>
            <div class="row">
            <div class="form-group">
                <span for="inputEmail3" align=right"  class="col-sm-2 control-label">Câu hỏi:</span>
                <div class="col-md-10 input-group">
                    <div class="input-group-addon "><i class="fa fa-pencil-square-o"></i></div>
                    <input  type="text" class="form-control input-sm" id="title_question" name="title_question" value="<?php echo $data["title"]; ?>" placeholder="Nhập câu hỏi...."></input>
                </div>
            </div>
            <div class="clear"></div>
            <div class="table_data">
                <table>
                    <tr>
                        <th>STT</th>
                        <th>Câu trả lời</th>
                        <th>Đáp án</th>
                        <th>Thao tác</th>
                    </tr>
                    <?php
                    $stt=0;
                     foreach ($list_answer as $key => $value) { ?>
                            <tr>
                                <td><?php echo ++$stt; ?><input type="hidden" name="ids[]" value="<?php echo $value["id"]; ?>"></input></td>
                                <td>
                                <input type="text" class="form-control input-sm" id="title_answer<?php echo $value["title"]; ?>" value="<?php echo $value["title"]; ?>" name="title_answer<?php echo $value["id"]; ?>"></input>
                                </td>
                                <td><label><input type="radio" <?php if($value["true"]==0) echo 'checked'; ?> name="trues<?php echo $value["id"]; ?>" id="trues0" value="0"></input> Sai</label>
                                    <label><input type="radio" <?php if($value["true"]==1) echo 'checked'; ?> name="trues<?php echo $value["id"]; ?>" id="trues0" value="1"></input> Đúng</label>
                                </td>
                                <td>
                                    <i class="fa fa-trash-o" onclick="del_answer(<?php echo $value["id"];?>);return false;"> </i>
                                </td>
                            </tr>
                    <?php } ?>
                </table>

            </div>
            <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training"; ?>" />
            <input type="hidden" name="id" id="id" value="<?php echo isset($data["id"]) ? $data["id"] : 0; ?>"></input>
            </div>
        </form>
</div>

<script type="text/javascript">
function del_answer(id){
    var result = confirm('Bạn có chắc chắn muốn xóa không');
    if(result){
        var link = $('#link').val();
        $.post(link+"/course/delete_answer/"+id, $("form").serialize(),function(resp){
            alert('Xóa thành công');
            location.reload();
        });
    }
}
function goBack() {
var link = $('#link').val();
window.location = link+'/training/add_lesson';
        <!-- window.history.back(); -->
    }
    function saves(){
        var link = $('#link').val();
        $.post(link+"/training/save_answer/", $("form").serialize(),function(resp){
            $.each(resp, function (i, obj) {
                var id = obj.id;
                var msg = obj.msg;
                if(id != "ok"){
                    alert(msg);
                    $('#' + id).focus();
                    return false;
                }else{
                    alert('Lưu thành công');
                    location.reload(true);
                }

            });

        }, 'json');
        
    }

</script>