    <form id="form_answer" action="<?php echo base_url().ADMINPATH."/training/training/save_answer/" ?>" enctype="multipart/form-data" method="POST" accept-charset="utf-8">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="col-xs-4 control-label">Câu hỏi: </label>
                        <div class="col-xs-8">
                           <input type="text" class="form-control" id="title_question" name="title_question" value="<?php echo $Question ? $Question->title : '' ; ?>" placeholder="Nhập câu hỏi...."></input>
                        </div>
                     </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="col-xs-4 control-label">Hình minh họa: </label>
                        <div class="col-xs-8">
                            <input type="file" name="Image_upload" />
                                <div class="dvPreview">
                                <?php 
                                if($Question){
                                    $thumb = $this->lib->get_thumb($Question->Image);
                                    if(file_exists($thumb)){
                                        echo "<img src='$thumb' class='img-thumbnail' />";
                                    }
                                }
                                ?>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="col-xs-4 control-label">Bài học gợi ý: </label>
                        <div class="col-xs-8">
                           <select name="suggestid" class="form-control">
                                <option value=''>-- Chọn bài gợi ý đáp án --</option>
                                <?php 
                                $suggest = $Question ? $Question->suggestid :  0;
                                if($Lesson){
                                    $lesson_list = $this->db->query("select * from ttp_training_lesson where courseid=$Lesson->courseid and type in(1,2)")->result();
                                    if(count($lesson_list)>0){
                                        foreach($lesson_list as $row){
                                            $selected = $row->id==$suggest ? 'selected="selected"' : '' ;
                                            echo "<option value='$row->id' $selected>$row->title</option>";
                                        }
                                    }
                                }
                                ?>
                           </select>
                        </div>
                     </div>
                </div>
                
                <div class="col-xs-6">
                    
                </div>
            </div>
            <div class="addanswer table_data">
                <table id="addanswer">
                    <tr>
                        <th>STT</th>
                        <th class="th_answer">Đáp án đúng</th>
                        <th>Câu trả lời</th>
                        <th class="th_action_min">Thao tác</th>
                    </tr>
                    <?php 
                    if(count($Answer)>0){
                        $i=1;
                        foreach($Answer as $row){
                            $checked = $row->true==1 ? "checked='checked'" : '' ;
                            echo "<tr>
                            <td>$i</td>
                            <td><input type='radio' name='IsTrue' $checked value='$i' /></td>
                            <td><input type='text' placeholder='Nhập câu trả lời...' class='form-control' name='title[]' id='title' value='$row->title' ></input></td>
                            <td><a onclick='removes_tr(this)'><i class='fa fa-trash-o'> </i> Xóa</a></td>
                            </tr>";
                            $i++;
                        }
                    }else{
                    ?>
                    <tr>
                        <td>1</td>
                        <td>
                            <input type="radio" name="IsTrue" value="1" checked="checked" />
                        </td>
                        <td>
                            <input type="text" placeholder="Nhập câu trả lời..." class="form-control" name="title[]" id="title" ></input>
                        </td>
                        <td>
                            <a onclick="removes_tr(this)"><i class="fa fa-trash-o"> </i> Xóa</a>
                        </td>
                    </tr>
                    <?php 
                    }
                    ?>
                </table>
            </div>
            <div class="fixedtools">
                <a class="btn btn-primary btn-box-inner pull-right" onclick="saves_answer(this)"><i class="fa fa-floppy-o"></i> Lưu thay đổi</a>
                <a class="btn btn-danger btn-box-inner" onclick="adds()"><i class="fa fa-plus"></i> Thêm câu trả lời</a>
            </div>
            <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <input type="hidden" name="id" id="id" value="<?php echo $id ?>"></input>
            <input type="hidden" name="QuestionID" value="<?php echo $Question ? $Question->id : '' ; ?>"></input>
    </form>
</div>
<script type="text/javascript">
    function adds(){
        var rowCount = $('table#addanswer tr:last').index()+1;
        var stt = $('table#addanswer tr:last').index()+1;
        var table = document.getElementById("addanswer");
        var row = table.insertRow(rowCount);
        row.insertCell(0).innerHTML = stt;
        row.insertCell(1).innerHTML = '<input type="radio" name="IsTrue" value="'+stt+'" />';
        row.insertCell(2).innerHTML = '<input type="text" placeholder="Nhập câu trả lời..." class="form-control" name="title[]" id="title" ></input>';
        row.insertCell(3).innerHTML = '<a onclick="removes_tr(this)"><i class="fa fa-trash-o"></i> Xóa</a>';
    }

    function saves_answer(ob){
        $(ob).addClass("saving");
        $(ob).find('i').hide();
        $("#form_answer").submit();
    }

    function removes_tr(ob){
        $(ob).parent('td').parent('tr').remove();
    }
</script>