<?php 
$url = base_url().ADMINPATH.'/training';
?>
<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">

        <form action="" method="POST">
        <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <div class="box-header">
                <div class="col-md-8">
                    <b class="sz20">THÔNG TIN CHƯƠNG HỌC</b>
                </div>
                <div align="right" class="col-md-4">
                    <button type="button" onclick="saves();return false;" align="right" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"> LƯU THAY ĐỔI</i></button>
                    <a target="_blank" onclick="goBack();" align="right" class="btn btn-sm btn-danger"><i class="fa fa-times"> ĐÓNG</i></a>
                </div>
            </div>

            <div class="clear"></div>
            <div class="clear"></div>
            <div class="col-md-9">
            <div class="form-group">
                <span for="inputEmail3"  class="col-sm-3 control-label ">Tên chương:</span>
                <div class="col-md-9 input-group">
                <div class="input-group-addon "><i class="fa fa-pencil-square-o"></i></div>
                    <input type='text' placeholder="Ví dụ : Chương I...." class="form-control input-sm" value="<?php echo isset($detail["title"]) ? $detail["title"] : ''; ?>" id="title_chapter_new" name='title_chapter_new' />
                </div>
            </div>
            <div class="clear"></div>
            <div class="form-group">
                <span for="inputEmail3"  class="col-sm-3 control-label ">Tên khóa học:</span>
                <div class="col-md-9 input-group">
                <div class="input-group-addon "><i class="fa fa-pencil-square-o"></i></div>
                    <select class="form-control input-sm" name="courseid" id="courseid">
                        <option value="0">Chọn Khóa học</option>
                        <?php foreach ($course as $key => $value) {
                            $sl = '';
                            if($detail["courseid"] == $value["id"]){
                                $sl = 'selected="selected"';
                            }
                            ?>
                            <option <?php echo $sl; ?> value="<?php echo $value["id"]; ?>"><?php echo $value["title"]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="clear"></div>
            <div class="form-group">
                <span for="inputEmail3"  class="col-sm-3 control-label ">Tên phần:</span>
                <div class="col-md-9 input-group">
                <div class="input-group-addon "><i class="fa fa-pencil-square-o"></i></div>
                    <select class="form-control input-sm" name="partid" id="partid">
                        <option value="0">Chọn Phần</option>
                        <?php if(isset($list)){?>
                            <?php foreach ($list as $key => $value) {
                                $sl2='';
                                if($detail["partid"] == $value["id"]){
                                    $sl2 = 'selected="selected"';
                                }
                                ?>
                                <option <?php echo $sl2; ?> value="<?php echo isset($value["id"]) ? $value["id"] : ''; ?>"><?php echo isset($value["title"]) ? $value["title"] : ''; ?></option>
                            <?php } ?>



                            <?php } ?>

                            
                    </select>
                </div>
            </div>
            </div>
            <div class="clear"></div>
            <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <input type="hidden" name="id_chapter" id="id_chapter" value="<?php echo isset($detail["id"]) ? $detail["id"] : 0; ?>"></input>

            <div class="table_data">
                <table>
                    <tr>
                        <th>STT</th>
                        <th>Tên chương</th>
                        <th>Phần</th>
                        <th>Khóa học</th>
                        <th>Thao tác</th>
                    </tr>
                    <?php
                        $stt=0;
                        foreach ($data as $key => $value) {?>
                            <tr>
                            <td><?php echo ++$stt; ?></td>
                            <td><?php echo $value["title"]; ?></td>
                            <td><?php echo $name_part[$value["partid"]]; ?></td>
                            <td><?php echo $name_course[$value["courseid"]]; ?></td>
                            <td><a href="<?php echo $url;?>/training/add_chapter/<?php echo $value["id"];?>"><i class="fa fa-pencil-square-o"></i></a><i class="fa fa-trash-o" onclick="del(<?php echo $value["id"];?>);return false;"> </i></td>
                            </tr>

                        <?php }?>
                </table>

            </div>
        </form>
</div>

<script type="text/javascript">
function del(id){
    var result = confirm('Bạn có chắc chắn muốn xóa không');
    if(result){
        var link = $('#link').val();
        $.post(link+"/course/delete_chapter/"+id, $("form").serialize(),function(resp){
            alert('Xóa thành công');
            window.location = link+'/training/add_chapter';
            // location.reload();
        });
    }
}
function goBack() {
        window.history.back();
    }
    function saves(){
        var link = $('#link').val();
        $.post(link+"/training/save_chapter/", $("form").serialize(),function(resp){
            $.each(resp, function (i, obj) {
                var id = obj.id;
                var msg = obj.msg;
                if(id != "ok"){
                    alert(msg);
                    $('#' + id).focus();
                    return false;
                }else{
                    alert('Lưu thành công');
                    location.reload(true);
                }

            });

        }, 'json');
        
    }

    $(document).ready(function(){
        var link = $('#link').val();
        $('#courseid').change(function(){
            $('#partid').html('<option value="0">Chọn Phần</option>');
            var courseid = $('#courseid').val();
            var url = link+"/training/get_course_by_id/"+courseid;
            $.ajax({
              dataType: "json",
              type: "GET",
              url: url,
              data: "",
              success: function(resp){
                $.each(resp,function(i,obj){
                    var html = "<option value='"+resp[i].id+"'> "+resp[i].title+" </option>";
                    $('#partid').append(html);
                });
                

              }
          });
        });







        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview");
            if(file.type.match(imageType)){
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:100px;max-width: 100px");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            }else{
                console.log("Not an Image");
            }
        });
    });
</script>
