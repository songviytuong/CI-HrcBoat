<div class="containner">
	<?php 
		if($data && $status_pass==0){
	?>
	<div class="certificate">
		<h2 class="title"><b>Chứng chỉ khóa học</b><span><?php echo $data->title ?></span></h2>	
		<h3 class="name"><b>Học viên:</b><span><?php echo $this->user->FirstName.' '.$this->user->LastName ?></span></h3>
		<p>Chúc mừng bạn đã hoàn thành tốt khóa học. Hy vọng với những kiến thức
được trang bị giúp ích bạn thành công hơn trong công việc.</p>
		<h6 class="date"><span><?php echo date('d/m/Y',strtotime($data->enddate)) ?></span><b>Ngày hoàn thành</b></h6>
		<h6 class="ttp-logo"><a href="" title="CÔNG TY TNHH THƯƠNG MẠI DỊCH VỤ TRẦN TOÀN PHÁT"><img src="public/admin/images/ttp-logo.png" alt="CÔNG TY TNHH THƯƠNG MẠI DỊCH VỤ TRẦN TOÀN PHÁT"></a></h6>
	</div>		
	<?php 
	}else{
	?>
		<div class="alert alert-danger alert-dismissable">
			<h4>Thông báo từ hệ thống!</h4>
			<p>Tài khoản hiện tại của bạn không có chứng chỉ này. Bạn phải <a class="alert-link">hoàn thành toàn bộ khóa học</a>, để được cấp chứng chỉ cho khóa học này .</p>
			<p><a href="<?php echo base_url().ADMINPATH."/training/learn/study/$coursesid" ?>" class="btn btn-danger"><i class='fa fa-graduation-cap'></i> Tham gia khóa học</a> <a href="<?php echo base_url().ADMINPATH."/training/learn/slist" ?>" class="btn btn-link">hoặc quay về trang chính</a></p>
		</div>
	<?php
	}	
	?>
</div>
