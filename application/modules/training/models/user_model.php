<?php
class User_model extends CI_Model
{
    private $_tableName = "ttp_user";
    public function getList($groupid="")
    {
        $sql = "select * from $this->_tableName where UserType !=0 ";
        if($groupid !=null){
            $sql .=" and groupid = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }
    public function getList2($kq="",$groupid="")
    {
        $sql = "select * from $this->_tableName where UserType !=0 ";
        if($kq !=null){
            $sql .=" and UserName like '%$kq%' ";
        }
        if($groupid !=null){
            $sql .=" and UserType = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_user_type($id=0){
        $result = array(
                    "1"=>"Nhân viên Telesale",
                    "2"=>"Nhân viên kho",
                    "3"=>"Nhân viên kế toán",
                    "4"=>"Nhân viên điều phối",
                    "5"=>"Telesale Manager",
                    "6"=>"CM User",
                    "7"=>"Kế toán trưởng",
                    "8"=>"Kho Manager",
                    "9"=>"CEO",
                    "10"=>"Sale MT-GT",
                    );
        if($id!=0){
            return $result[$id]; 
        }else{
            return $result;
        }

    }

}?>