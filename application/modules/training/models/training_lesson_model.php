<?php
class Training_lesson_model extends CI_Model
{
    private $_tableName = "ttp_training_lesson";
    public function getList($type="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($type !=null){
            $sql .=" and type = $type";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getListByChapterId($chapterid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($chapterid !=null){
            $sql .=" and chapterid IN ($chapterid)";
        }
        $sql.="ORDER BY orderby";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where enabled = 1 and  id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }
    public function insert($data){
        $this->db->insert($this->_tableName,$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
	public function insert_history($data){
        $this->db->insert('ttp_training_lesson_history',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
	public function getListHistory($id)
    {
        $sql = "select * from ttp_training_lesson_history where enabled = 1 and  id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result) ? $result :'';
    }
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update($this->_tableName, $data);
    }
    public function excute($query){
        return $this->db->query($query);
    }

    public function getCountLessonByCourse($courseid)
    {
        $sql = "select type,count(*) as total from $this->_tableName where enabled = 1 and  courseid = $courseid group by type ";
        $result = $this->db->query($sql)->result_array();
        return isset($result) ? $result :'';
    }
    public function getListByCourseId($courseid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($courseid !=null){
            $sql .=" and courseid IN ($courseid)";
        }
        $sql.="ORDER BY partid,chapterid,orderby";
        $result = $this->db->query($sql)->result_array();
        return isset($result) ? $result :'';
    }
    
}?>