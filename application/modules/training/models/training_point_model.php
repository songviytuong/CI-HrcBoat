<?php
class Training_point_model extends CI_Model
{
    private $_tableName = "ttp_training_point";
    public function getList($groupid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($groupid !=null){
            $sql .=" and groupid = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
     public function insert($data){
        $this->db->insert($this->_tableName,$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update($this->_tableName, $data);
    }
    public function updateStatus($userid,$lessonid){
        $sql = "update $this->_tableName set mark_true =0,mark_false =0,mark_abs =0 where userid = $userid and lessonid = $lessonid ";
        return $this->db->query($sql);
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where enabled = 1 and  id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : '';
    }
    public function getDetailByLessonUserId($userid,$lessonid)
    {
        $sql = "select * from $this->_tableName where userid = $userid and lessonid = $lessonid ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : '';
    }
    public function getCountPoint($userid,$lessonid)
    {
        $sql = "SELECT count(*) as total FROM ttp_training_point a,ttp_training_point_detail b WHERE a.lessonid = $lessonid and a.userid = $userid and a.id = b.pointid";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : 0;
    }

    public function getCountLessonByCourse($courseid)
    {   
        $sql = "SELECT count(*) as total FROM ttp_training_lesson a WHERE a.courseid = $courseid  and a.type = 3";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : 0;
    }
    public function getCountCompleteLessonByCourse($userid,$courseid)
    {
        $sql = "SELECT count(*) as total FROM ttp_training_point WHERE courseid = $courseid and userid = $userid and status = 1;";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : 0;
    }


}?>