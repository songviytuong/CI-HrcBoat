<?php
class Training_question_model extends CI_Model
{
    private $_tableName = "ttp_training_question";
    public function getList($groupid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($groupid !=null){
            $sql .=" and groupid = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getListByChapterId($chapterid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($chapterid !=null){
            $sql .=" and chapterid IN ($chapterid)";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where enabled = 1 and  id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }

    public function getListById($id)
    {
        $sql = "SELECT *,a.title as cauhoi FROM $this->_tableName a,ttp_training_answer b WHERE a.lessionid = $id and a.id = b.questionid";
        // var_dump($sql);exit();
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function insert($data){
        $this->db->insert($this->_tableName,$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update($this->_tableName, $data);
    }
    
}?>