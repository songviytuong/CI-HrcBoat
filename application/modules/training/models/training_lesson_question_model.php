<?php
class Training_lesson_question_model extends CI_Model
{
    private $_tableName = "ttp_training_lesson_question";
    public function getList($groupid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($groupid !=null){
            $sql .=" and groupid = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getListByChapterId($chapterid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($chapterid !=null){
            $sql .=" and chapterid IN ($chapterid)";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getDetailByLessonId($lessonid,$limit=0)
    {
        $sql = "select * from $this->_tableName a,ttp_training_question b where  a.lessonid = $lessonid and a.questionid =  b.id limit $limit,1";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }

    public function getListById($id)
    {
        $sql = "SELECT *,a.title as cauhoi FROM $this->_tableName a,ttp_training_answer b WHERE a.lessionid = $id and a.id = b.questionid";
        // var_dump($sql);exit();
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getCountQuestionByLesson($lessonid)
    {
        $sql = "SELECT count(*) as total from $this->_tableName where enabled =1 and lessonid = $lessonid";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where enabled = 1 and  id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }
    public function insert($data){
        return $this->db->insert($this->_tableName,$data);
    }
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update($this->_tableName, $data);
    }
    public function getListByLessonId($lessonid)
    {
        $sql = "select * from $this->_tableName  where  lessonid = $lessonid and enabled = 1";
        $result = $this->db->query($sql)->result_array();
        return isset($result) ? $result :'';
    }
    public function getDetailByLessonQuestion($lessonid,$questionid)
    {
        $sql = "select * from $this->_tableName  where  lessonid = $lessonid and questionid =  $questionid and enabled = 1";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }

}?>