<?php 
class Report_sales extends Admin_Controller { 
 
 	public $user;
 	public $classname="report_sales";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$this->template->add_title('Sales | Report Tools');
		$this->template->write_view('content','admin/sales_overview');
		$this->template->render();
	}

    public function compare_targets_doanhso(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_compare_targets');
        $this->template->render();
    }

    public function report_sanphamban(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_sanphamban');
        $this->template->render();
    }

    public function compare_targets_sanphamban(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_compare_targets_sanpham');
        $this->template->render();
    }

    public function report_order(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_order');
        $this->template->render();
    }

    public function compare_targets_order(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_compare_targets_order');
        $this->template->render();
    }

    public function report_customer(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_customer');
        $this->template->render();
    }

    public function report_staff(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Staff sales | Report Tools');
        $this->template->write_view('content','admin/sales_staff');
        $this->template->render();
    }

    public function report_source(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report by source | Report Tools');
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $result_source = $this->db->query("select b.ID,b.Title,sum(a.SoluongSP) as SoluongSP,sum(a.Total) as Total,count(a.ID) as SLDH from ttp_report_order a,ttp_report_source b where a.SourceID=b.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' group by b.ID")->result();
        $result_channel = $this->db->query("select b.ID,b.Title,sum(a.SoluongSP) as SoluongSP,sum(a.Total) as Total,count(a.ID) as SLDH from ttp_report_order a,ttp_report_saleschannel b where a.KenhbanhangID=b.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' group by b.ID")->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result_source,
            'channel'   => $result_channel,
            'base_link' => base_url().ADMINPATH.'/report/import_order/'
        );
        $this->template->add_title('Report by source | Report Tools');
        $this->template->write_view('content','admin/sales_source',$data);
        $this->template->render();
    }

    public function compare_targets_customer(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_compare_targets_customer');
        $this->template->render();
    }

    public function set_session_compare(){
        $KPI = "";
        $targets = "";
        if(isset($_POST['KPI'])){
            $this->session->set_userdata('KPI',(int)$_POST['KPI']);
            $KPI = $_POST['KPI'];
        }
        if(isset($_POST['Target'])){
            $this->session->set_userdata('targets',(int)$_POST['Target']);
            $targets = $_POST['Target'];
        }
        if(isset($_POST['Orderstatus'])){
            $this->session->set_userdata('Orderstatus',(int)$_POST['Orderstatus']);
        }
        if($KPI==1){
            if($targets==''){
                echo base_url().ADMINPATH."/report/report_sales";
            }else{
                echo base_url().ADMINPATH."/report/report_sales/compare_targets_doanhso";
            }
        }
        if($KPI==2){
            if($targets==''){
                echo base_url().ADMINPATH."/report/report_sales/report_sanphamban";
            }else{
                echo base_url().ADMINPATH."/report/report_sales/compare_targets_sanphamban";
            }
        }
        if($KPI==3){
            if($targets==''){
                echo base_url().ADMINPATH."/report/report_sales/report_order";
            }else{
                echo base_url().ADMINPATH."/report/report_sales/compare_targets_order";
            }
        }
        if($KPI==4){
            if($targets==''){
                echo base_url().ADMINPATH."/report/report_sales/report_customer";
            }else{
                echo base_url().ADMINPATH."/report/report_sales/compare_targets_customer";
            }
        }
    }

    public function remove_session_conpare(){

    }

}
?>
