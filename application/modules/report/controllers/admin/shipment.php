<?php 
class Shipment extends Admin_Controller { 
 
 	public $user;
 	public $classname="shipment";

    public function __construct() { 
        parent::__construct();   
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
    public function index(){
       
        
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_warehouse_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_warehouse_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d where a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and a.TypeExport=0")->row();
        $result = $this->db->query("select a.*,b.MaDH,c.UserName,d.MaKho as MaKho,b.ID as IDDH from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d where a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and a.TypeExport=0 order by a.ID DESC $limit_str")->result();
        $nav = $nav ? $nav->nav : 0;
        
        $this->cache->delete_all();
        $this->cache->write($result, 'shiping_list', 120);
        $result = $this->cache->get('shiping_list');
//        var_dump($resulta); exit();
        
        $data = array(
            'data'      =>  $result,
            'base_link' =>  base_url().ADMINPATH.'/report/shipment/',
            'start'     =>  $start,
            'startday'  =>  $startday,
            'stopday'   =>  $stopday,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/shipment/list',5,$nav,$this->limit)
        );
        $this->template->add_title('Shipping | Shipment Report Tools');
        $this->template->write_view('content','admin/shipment_content_list',$data);
        $this->template->render();
    }
}
?>