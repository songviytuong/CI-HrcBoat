<?php 
class Promotion_campaign extends Admin_Controller { 
 
 	public $user;
 	public $classname="promotion_campaign";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }
	
    public function index(){
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/promotion_campaign/'
        ); 
        $view = "admin/promotion_campaign";
        $this->template->add_title('Promotion Comapaign');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    public function set_day(){
        $active = isset($_POST['active']) ? $_POST['active'] : 0 ;
        if($active==1){
            $this->session->set_userdata("active_vs",1);
        }else{
            $this->session->set_userdata("active_vs",0);
        }
        $group1 = isset($_POST['group1']) ? $_POST['group1'] : "" ;
        if($group1!=''){
            $group1 = explode("-",$group1);
            $startday = isset($group1[0]) ? trim($group1[0]) : '';
            $stopday = isset($group1[1]) ? trim($group1[1]) : '';
            if($startday!='' && $stopday!=''){
                $startday = explode('/',$startday);
                $stopday = explode('/',$stopday);
                if(count($startday)==3 && count($stopday)==3){
                    $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
                    $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
                    $timestart = strtotime($startday);
                    $timestop = strtotime($stopday);
                    if($timestart>$timestop){
                        echo "fasle";
                        return;
                    }
                    $this->session->set_userdata("campaign_startday",$startday);
                    $this->session->set_userdata("campaign_stopday",$stopday);
                }
            }
        }
        echo "OK";
    }
    public function AutoCompleteCustomer(){
        $key = $_POST["keyword"];
        if($key != null){
            $CustomerArr = $this->db->query("SELECT ID,Name,Phone1,Phone2 FROM ttp_report_customer WHERE Name LIKE '%$key%' or Phone1 LIKE '%$key%' or Phone2 LIKE '%$key%' LIMIT 0,100")->result();
            $res = "<ul id='customer-list' class='list-group'>";
            $phone = "";
            foreach($CustomerArr as $row){
                $phone1 = substr_replace($row->Phone1, str_repeat("x", 4), 0, 4);
                $phone2 = substr_replace($row->Phone2, str_repeat("x", 4), 0, 4);
                if($row->Phone2 != null){
                    $phone = $phone1." - ".$phone2;
                }else{
                    $phone = $phone1;
                }
                $res .= "<li class='list-group-item item-".$row->ID."' onClick='selectCustomer(".$row->ID.")'>".$row->Name." <span class='badge'>".$phone."</span></li>";
            }
            $res .= "</ul>";
            echo $res;
        }
        
    }
    
    public function AutoCompleteData(){
        $cid = $_POST["customer_id"];
        $Customer = $this->db->query("SELECT * FROM ttp_report_customer WHERE ID = $cid")->row();
        $phone1 = substr_replace($Customer->Phone1, str_repeat("x", 4), 0, 4);
        $phone2 = substr_replace($Customer->Phone2, str_repeat("x", 4), 0, 4);
        if($Customer->Phone2 != null){
            $phone = $phone1." - ".$phone2;
        }else{
            $phone = $phone1;
        }
        $html = "<tr class='row_".$Customer->ID."'>
                    <td class='text-center'>".$Customer->ID."<input name='customers[]' value=".$Customer->ID." type='hidden'></td>
                    <td>".$Customer->Name."</td>
                    <td class='text-center'>".$phone."</td>
                    <td class='text-center' style='cursor:pointer' onClick='del($Customer->ID)'><i class='fa fa-times-circle' rel='".$Customer->ID."' title='Xóa'></i></td>
                </tr>";
        echo $html;
    }
    public function all(){
        $i = $this->uri->segment(5);
        
        $startday = $this->session->userdata("campaign_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("campaign_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $where = "";
        if(!empty($i)){
            if($i == 1){
                $where = "and promo_type = 1";
            }elseif($i == 2){
                $where = "and promo_type = 0";
            }elseif($i == 3){
                $where = "and promo_type in(0,1)";
            }elseif($i == 4){
                $where = "and promo_type = 2";
            }
        }
        
        //$where = " where promo_owner = ".$this->user->ID;
        $result = $this->db->query("select * from ttp_promotion where del_flg = 0 and promo_status = 0 $where and date(promo_created)>='$startday' and date(promo_created)<='$stopday'")->result();
        $data = array(
            'data' => $result,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'base_link' =>  base_url().ADMINPATH.'/report/promotion_campaign/'
        );  
        
        $view = "admin/promotion_campaign_all";
        $this->template->write_view('sitebar','admin/promotion_sitebar',array('user'=>$this->user));
        $this->template->add_title('All | Promotion Comapaign');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    public function getListByCate(){
        $Cate_ID = isset($_POST["cate_id"]) ? $_POST["cate_id"] : array();
        $Cate_ID = explode(",", $Cate_ID);
        $i = 0;
        $like = "";
        foreach($Cate_ID as $row){
            if ($i == 0) {
                $like .= "'".$row."/%' ";
            } else {
                $like .= "or Path LIKE '$row/%' ";
            }
            $i++;
        }
        
        $id = $_POST["promo_id"];
        $result_detail = $this->db->query("select * from ttp_promotion_details where promo_id = $id")->row();
        $list_active = json_decode($result_detail->list);
        $result_product = $this->db->query("select ID,Title from ttp_report_categories where TypeCat = 0 and Path LIKE $like")->result();
        $html = array();
        foreach($result_product as $key=>$item){
            $html[] = "<option value='".$item->ID."'";
            if($list_active[$key] == $item->ID){
                $html[] = " selected";
            }
            $html[] = ">".$item->Title."</option>\n";
        }
        echo implode($html);
    }
    
    public function get_child_categories(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $promo_id = isset($_POST['promo_id']) ? $_POST['promo_id'] : '' ;
        $category_active = "";
        if($promo_id != ""){
            $result_detail = $this->db->query("select * from ttp_promotion_details where promo_id = $promo_id")->row();
            $category_active = json_decode($result_detail->cate);
        }
        $result = $this->db->query("select * from ttp_report_categories where TypeCat=0 and ParentID=$ID")->result();
        if(count($result)>0){
            echo "<ul>";
            foreach($result as $row){
                echo "<li><span onclick='show_child_next(this)' data='$row->ID'>+</span><input type='checkbox'";
//                if($category_active != ""){
//                    if(in_array($row->ID,$category_active)){
//                        echo " checked ";
//                    }
//                }
                echo "value='$row->ID' onchange='check_full(this)' name='CategoriesID[]' /> $row->Title</li>";
            }
            echo "</ul>";
        }else{
            echo "False";
        }
    }
    
    public function viewVoucher()
    {
        $promo_id = $_GET["promo_id"];
        $result = $this->db->query("SELECT * FROM ttp_promotion_code WHERE promo_id = $promo_id")->result();
        
        $data = array(
            'data' => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/promotion_campaign/'
        );  
        
        $view = "admin/promotion_voucher_view";
        $this->load->view($view,$data);
    }
    
    public function createVoucher()
    {
        $promo_id = $_GET["promo_id"];
        $data = array(
            'promo_id'  => $promo_id,
            'base_link' =>  base_url().ADMINPATH.'/report/promotion_campaign/'
        );  
        $view = "admin/promotion_voucher_create";
        $this->load->view($view,$data);
    }
    
    public function createVoucherData()
    {
        $promo_id = $_GET["promo_id"];
        $number = $_GET["number"];
        $opt = $_GET["opt"];
        
        for($i=0;$i<$number;$i++){
            $rand = $this->generateRandomWord($opt);
            //echo "Insert ID: ".$i."\n";
            $insertArray = array(
                "code"      => $rand,
                "active"    => 0,
                "promo_id"  => $promo_id
            );
            $this->db->insert("ttp_promotion_code",$insertArray);
        }
        echo "OK";
    }
    
    public function getProductByList(){
        $List_ID = isset($_POST["list_id"]) ? $_POST["list_id"] : array();
        $where = $List_ID;
        $id = $_POST["promo_id"];
        $result_detail = $this->db->query("select * from ttp_promotion_details where promo_id = $id")->row();
        $product_active = json_decode($result_detail->promo_code);
        $result_product = $this->db->query("select * from ttp_report_products_categories Where CategoriesID in ($where)")->result();
        if(count($result_product) > 0){
            $html = array();
            foreach($result_product as $key=>$item){
                $row = $this->db->query("select * from ttp_report_products Where ID = $item->ID")->row();
                $html[] = "<option value='".$item->ID."'";
                if($product_active[$key] == $item->ID){
                    $html[] = " selected";
                }
                $html[] = ">".$row->Title."</option>\n";
            }
            echo implode($html);
        }
    }
    
    public function getGift(){
        $where = "61";
        $result_product = $this->db->query("select * from ttp_report_products_categories Where CategoriesID in ($where)")->result();
        if(count($result_product) > 0){
            $html = array();
            foreach($result_product as $item){
                $row = $this->db->query("select * from ttp_report_products Where ID = $item->ProductsID")->row();
                $html[] = "<option value='".$item->ID."'>".$row->Title."</option>\n";
            }
            echo implode($html);
        }
    }
    
    public function getCityByArea(){
        $Area_ID = isset($_POST["area_id"]) ? $_POST["area_id"] : '';
        $result_city = $this->db->query("select ID,Title from ttp_report_city where AreaID in($Area_ID)")->result();
        
        $id = $_POST["promo_id"];
        $result_detail = $this->db->query("select * from ttp_promotion_details where promo_id = $id")->row();
        $city_active = json_decode($result_detail->city);
        
        $html = array();
        foreach($result_city as $key=>$item){
            $html[] = "<option value='".$item->ID."'";
            if($city_active[$key] == $item->ID){
                $html[] = " selected";
            }
            $html[] = ">".$item->Title."</option>\n";
        }
        echo implode($html);
    }
    public function create(){
        
        $result_category = $this->db->query("select ID,Title from ttp_report_categories where ParentID = 0 and TypeCat = 0")->result();
        $category = $result_category;
        
        $result_area = $this->db->query("select ID,Title from ttp_report_area")->result();
        $area = $result_area;
        
        $result_customer = $this->db->query("select * from ttp_report_customer limit 0,10")->result();
        $customer = $result_customer;
        
        $result_allow = $this->db->query("select * from ttp_define where promotion_active in (1,2)")->result();
        $allow = $result_allow;
        
        $result_payment = $this->conf_define->get_order_status('payment','promotion','position','asc');
        $data = array(
            'category'  => $category,
            'area'      => $area,
            'customer'      => $customer,
            'result_payment'      => $result_payment,
            'allow'      => $allow,
            'base_link' =>  base_url().ADMINPATH.'/report/promotion_campaign/'
        );
        
        $view = "admin/promotion_campaign_create";
        $this->template->write_view('sitebar','admin/promotion_sitebar',array('user'=>$this->user));
        $this->template->add_title('Create | Promotion Comapaign');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    public function edit(){
        
        $id = $this->uri->segment(5);
        $result_promotion = $this->db->query("select * from ttp_promotion where promo_id = $id")->row();
        $result = ($result_promotion) ? $result_promotion : array();
        
        $result_category = $this->db->query("select ID,Title from ttp_report_categories where ParentID = 0 and TypeCat = 0")->result();
        $category = $result_category;
        
        $result_detail = $this->db->query("select * from ttp_promotion_details where promo_id = $id")->row();
        $category_active = json_decode($result_detail->cate);
        $area_active = json_decode($result_detail->area);
        $allow_active = ($result_detail->allow) ? json_decode($result_detail->allow) : null;
        $min_quantity = $result_detail->min_quantity;
        $min_value = ($result_detail->min_value) ? $result_detail->min_value : "";
        $discount = ($result_detail->discount) ? $result_detail->discount : "";
        $discount_type = $result_detail->discount_type;
        $city = ($result_detail->city) ? json_decode($result_detail->city) : "";
        $customer = ($result_detail->customer) ? json_decode($result_detail->customer) : "";
        
        $result_area = $this->db->query("select ID,Title from ttp_report_area")->result();
        $area = $result_area;
        
        $result_allow = $this->db->query("select * from ttp_define where promotion_active in (1,2)")->result();
        $allow = $result_allow;
        
        $result_payment = $this->conf_define->get_order_status('payment','promotion','position','asc');
        $data = array(
            'data' => $result,
            'promo_id' => $id,
            'promo_type' => $result->promo_type,
            'category'  => $category,
            'category_active'  => $category_active,
            'allow_active'  => $allow_active,
            'min_quantity'  => $min_quantity,
            'min_value'  => $min_value,
            'discount'  => $discount,
            'result_payment'  => $result_payment,
            'discount_type'  => $discount_type,
            'area'      => $area,
            'area_active'      => $area_active,
            'city'      => $city,
            'customer'      => $customer,
            'allow'      => $allow,
            'base_link' =>  base_url().ADMINPATH.'/report/promotion_campaign/'
        );
        
        $view = "admin/promotion_campaign_edit";
        $this->template->write_view('sitebar','admin/promotion_sitebar',array('user'=>$this->user));
        $this->template->add_title('Edit | Promotion Comapaign');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    public function listProduct(){
        $promo_id = $_GET["promo_id"];
        $result_detail = $this->db->query("select * from ttp_promotion_details where promo_id = $promo_id")->row();
        $product_code = $result_detail->promo_code;
        $arr = array();
        foreach (json_decode($product_code) as $item){
            $arr[] = $item;
        }
        $in = implode(",", $arr);
        if($in != null){
            $result_product = $this->db->query("select * from ttp_report_products Where ID in($in)")->result();
            if(count($result_product) > 0){
                $html = array();
                foreach($result_product as $row){
                    $html[] = "<option value='".$row->ID."' selected>".$row->Title."</option>\n";
                }
            }
            echo implode($html);
        }
    }
    public function update(){
        
    }
    public function insert(){
        //Tạo mã 6 số random
        $rand = $this->generateRandomWord();
//        var_dump($_REQUEST); exit();  
        $promo_type = $_REQUEST["promo_type"] ? $_REQUEST["promo_type"] : 0;
        $promo_name = $_REQUEST["promo_name"];
        $promo_desc = $_REQUEST["promo_desc"];
        $promo_start = $_REQUEST["promo_start"];
        $promo_end = $_REQUEST["promo_end"];
        
        $insertData = array(
            "promo_code" => $rand,
            "promo_type" => $promo_type,
            "promo_name" => $promo_name,
            "promo_description" => $promo_desc,
            "promo_start" => $promo_start,
            "promo_end" => $promo_end,
            "promo_created" => date('Y-m-d H:i:s',time()),
            "promo_modified" => date('Y-m-d H:i:s',time()),
            
        );
        if($this->db->insert("ttp_promotion",$insertData)) {
            //return $this->db->insert_id();
            $promo_id           = $this->db->insert_id();
            $min_quantity       = isset($_REQUEST["min_quantity"]) ? $_REQUEST["min_quantity"] : 0;
            $min_value          = isset($_REQUEST["min_value"]) ? $_REQUEST["min_value"] : 0;
            $discount_type      = $_REQUEST["option_type"];

            if($discount_type == "3"){//Gift
                $discount = json_encode($_REQUEST["Gift"]);
            }else{
                $discount = $_REQUEST["discount"];
            }

            $cate = isset($_REQUEST["CategoriesID"]) ? json_encode($_REQUEST["CategoriesID"]) : "[]";
            $area = isset($_REQUEST["Area"]) ? json_encode($_REQUEST["Area"]) : "[]";
            $city = isset($_REQUEST["City"]) ? json_encode($_REQUEST["City"]) : "[]";
            $promo_code = isset($_REQUEST["Product"]) ? json_encode($_REQUEST["Product"]) : "[]";
            $allow = isset($_REQUEST["allow"]) ? json_encode($_REQUEST["allow"]) : "[]";
            $customers = isset($_REQUEST["customers"]) ? json_encode($_REQUEST["customers"]) : "[]";
            
            $insertDetail = array(
                "promo_id"          => $promo_id,
                "min_quantity"      => $min_quantity,
                "min_value"         => $min_value,
                "discount_type"     => $discount_type,
                "discount"          => $discount,
                "area"          => $area,
                "cate"          => $cate,
                "city"          => $city,
                "promo_code"          => $promo_code,
                "allow"          => $allow,
                "customer"          => $customers,
                "created" => date('Y-m-d H:i:s',time()),
                "modified" => date('Y-m-d H:i:s',time()),
                "status" => 0,
            );
            $this->db->insert("ttp_promotion_details",$insertDetail);
            //Nếu khác khách hàng ĐB thì tự động tạo 1 Voucher Code
            if($promo_type == 2){
                $insertArray = array(
                    "promo_id"  => $promo_id,
                    "code"      => $rand,
                    "active"    => 0
                );
                $this->db->insert("ttp_promotion_code",$insertArray);
            }
            echo "OK";
        }
        

        
    }
    
    public function delete_compaign(){
        $Promo_ID = isset($_GET["promo_id"]) ? $_GET["promo_id"] : '';
        if($Promo_ID){
            $updateData = array(
                "del_flg" => 1
            );
            $this->db->where("promo_id",$Promo_ID);
            $this->db->update("ttp_promotion",$updateData);
            echo "OK";
        }
        
    }
    
    static function generateRandomWord($length = 6) {
        $list = 'ABCDEFGHIJKLMNPQRST123456789';
        $rndWord = "";
        for ($i = 0; $i < $length; $i++) {
            $index = rand(0, strlen($list) - 1);
            $rndWord .= $list{$index};
        }
        return $rndWord;
    }
}
?>