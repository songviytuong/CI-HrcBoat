<?php 
class Report extends Admin_Controller { 
 
 	public $user;
 	public $classname="report";

    public function __construct() { 
        parent::__construct();   
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
	public function index(){


        $this->template->add_title('Report Tools');
		$this->template->write_view('content','admin/content');
		$this->template->render();
	}

    public function set_day(){
        $active = isset($_POST['active']) ? $_POST['active'] : 0 ;
        if($active==1){
            $this->session->set_userdata("active_vs",1);
        }else{
            $this->session->set_userdata("active_vs",0);
        }
        $group1 = isset($_POST['group1']) ? $_POST['group1'] : "" ;
        if($group1!=''){
            $group1 = explode("-",$group1);
            $startday = isset($group1[0]) ? trim($group1[0]) : '';
            $stopday = isset($group1[1]) ? trim($group1[1]) : '';
            if($startday!='' && $stopday!=''){
                $startday = explode('/',$startday);
                $stopday = explode('/',$stopday);
                if(count($startday)==3 && count($stopday)==3){
                    $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
                    $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
                    $timestart = strtotime($startday);
                    $timestop = strtotime($stopday);
                    if($timestart>$timestop){
                        echo "fasle";
                        return;
                    }
                    $this->session->set_userdata("startday",$startday);
                    $this->session->set_userdata("stopday",$stopday);
                }
            }
        }
        if($active==1){
            $group2 = isset($_POST['group2']) ? $_POST['group2'] : "" ;
            if($group2!=''){
                $group2 = explode("-",$group2);
                $startday = isset($group2[0]) ? trim($group2[0]) : '';
                $stopday = isset($group2[1]) ? trim($group2[1]) : '';
                if($startday!='' && $stopday!=''){
                    $startday = explode('/',$startday);
                    $stopday = explode('/',$stopday);
                    if(count($startday)==3 && count($stopday)==3){
                        $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
                        $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
                        $timestart = strtotime($startday);
                        $timestop = strtotime($stopday);
                        if($timestart>$timestop){
                            echo "fasle";
                            return;
                        }
                        $this->session->set_userdata("startday_vs",$startday);
                        $this->session->set_userdata("stopday_vs",$stopday);
                    }
                }
            }
        }
        echo 'OK';
    }
}
?>