<?php 
class Warehouse extends Admin_Controller { 
 	public $user;
 	public $classname="warehouse";

    public function __construct() { 
        parent::__construct(); 
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
    public function index(){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
            $this->template->add_title('Warehouse | Report Tools');
            $this->template->write_view('content','admin/warehouse_content');
            $this->template->render();
    }
    
    public function inventory(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Inventory | Report Tools');
        if($this->user->UserType==2 || $this->user->UserType==8){
            $result = $this->db->query("select a.*,b.MaSP,b.Title,c.ShipmentCode,d.MaKho from ttp_report_inventory a,ttp_report_products b,ttp_report_shipment c,ttp_report_warehouse d where d.Manager like '%\"".$this->user->ID."\"%' and a.ProductsID=b.ID and a.ShipmentID=c.ID and a.WarehouseID=d.ID and a.LastEdited=1 order by a.ProductsID ASC")->result();
        }else{
            $result = $this->db->query("select a.*,b.MaSP,b.Title,c.ShipmentCode,d.MaKho from ttp_report_inventory a,ttp_report_products b,ttp_report_shipment c,ttp_report_warehouse d where a.ProductsID=b.ID and a.ShipmentID=c.ID and a.WarehouseID=d.ID and a.LastEdited=1 order by a.ProductsID ASC")->result();
        }
        $data = array(
            'Data'  => $result
        );
        $this->template->write_view('content','admin/warehouse_inventory_products_home',$data);
        $this->template->render();
    }

    public function report_inventory(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Inventory | Report Tools');
        $fromdate = isset($_GET['FromDate']) ? str_replace("'","",$_GET['FromDate']) : date('Y-m-d') ;
        $todate = isset($_GET['ToDate']) ? str_replace("'","",$_GET['ToDate']) : date('Y-m-d') ;
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 1 ;
        $CategoriesID = isset($_GET['CategoriesID']) ? (int)$_GET['CategoriesID'] : 5 ;
        $Type = isset($_GET['Type']) ? (int)$_GET['Type'] : 0 ;
        $data = array(
            'FromDate'      => $fromdate,
            'ToDate'        => $todate,
            'WarehouseID'   => $WarehouseID,
            'CategoriesID'  => $CategoriesID,
            'Type'          => $Type
        );
        $this->template->write_view('content','admin/warehouse_inventory_report_inventory',$data);
        $this->template->render();
    }

    public function report_inventory_import(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report Warehouse Import | Report Tools');
        $fromdate = $this->session->userdata("import_startday");
        $fromdate = $fromdate!='' ? $fromdate : date('Y-m-01',time()) ;
        $todate = $this->session->userdata("import_stopday");
        $todate = $todate!='' ? $todate : date('Y-m-d',time()) ;
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 1 ;
        $Type = isset($_GET['Type']) ? (int)$_GET['Type'] : 0 ;
        if($Type==2){
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.TotalImport) as TotalAmount from ttp_report_transferorder a,ttp_report_transferorder_details b,ttp_report_warehouse c,ttp_report_products d where b.ProductsID=d.ID and a.ID=b.OrderID and a.WarehouseReciver=c.ID and date(a.ImportDate)>='$fromdate' and date(a.ImportDate)<='$todate' and a.Status =4 and a.WarehouseReciver=$WarehouseID group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }else{
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.Amount) as TotalAmount,sum(b.TotalVND) as TotalPrice from ttp_report_inventory_import a,ttp_report_inventory_import_details b,ttp_report_warehouse c,ttp_report_products d where b.ProductsID=d.ID and a.ID=b.ImportID and a.KhoID=c.ID and date(a.LastEdited)>='$fromdate' and date(a.LastEdited)<='$todate' and a.Status in(2,3,4) and a.KhoID=$WarehouseID and a.Type=$Type group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }
        $result = $this->db->query($sql)->result();
        $data = array(
            'FromDate'      => $fromdate,
            'ToDate'        => $todate,
            'WarehouseID'   => $WarehouseID,
            'Type'          => $Type,
            'data'          => $result
        );
        $this->template->write_view('content','admin/warehouse_inventory_report_inventory_import',$data);
        $this->template->render();
    }

    public function report_inventory_export(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report Warehouse Export | Report Tools');
        $fromdate = $this->session->userdata("import_startday");
        $fromdate = $fromdate!='' ? $fromdate : date('Y-m-01',time()) ;
        $todate = $this->session->userdata("import_stopday");
        $todate = $todate!='' ? $todate : date('Y-m-d',time()) ;
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 1 ;
        $Type = isset($_GET['Type']) ? (int)$_GET['Type'] : 0 ;
        if($Type==2){
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.TotalImport) as TotalAmount from ttp_report_transferorder a,ttp_report_transferorder_details b,ttp_report_warehouse c,ttp_report_products d where b.ProductsID=d.ID and a.ID=b.OrderID and a.WarehouseSender=c.ID and date(a.ExportDate)>='$fromdate' and date(a.ExportDate)<='$todate' and a.Status in(3,4) and a.WarehouseSender=$WarehouseID group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }elseif($Type==1){
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.Amount) as TotalAmount from ttp_report_order a,ttp_report_orderdetails b,ttp_report_warehouse c,ttp_report_products d,ttp_report_export_warehouse e where a.ID=e.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.KhoID=c.ID and date(e.Ngayxuatkho)>='$fromdate' and date(e.Ngayxuatkho)<='$todate' and a.Status=0 and a.KhoID=$WarehouseID and a.CustomerID=9996 group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }else{
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.Amount) as TotalAmount from ttp_report_order a,ttp_report_orderdetails b,ttp_report_warehouse c,ttp_report_products d,ttp_report_export_warehouse e where a.ID=e.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.KhoID=c.ID and date(e.Ngayxuatkho)>='$fromdate' and date(e.Ngayxuatkho)<='$todate' and a.Status in(7,9,0) and a.KhoID=$WarehouseID and a.CustomerID!=9996 group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }
        $result = $this->db->query($sql)->result();
        $data = array(
            'FromDate'      => $fromdate,
            'ToDate'        => $todate,
            'WarehouseID'   => $WarehouseID,
            'Type'          => $Type,
            'data'          => $result
        );
        $this->template->write_view('content','admin/warehouse_inventory_report_inventory_export',$data);
        $this->template->render();
    }

    public function load_details_report_export(){
        $fromdate = $this->session->userdata("import_startday");
        $fromdate = $fromdate!='' ? $fromdate : date('Y-m-01',time()) ;
        $todate = $this->session->userdata("import_stopday");
        $todate = $todate!='' ? $todate : date('Y-m-d',time()) ;
        $WarehouseID = isset($_POST['WarehouseID']) ? (int)$_POST['WarehouseID'] : 1 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 1 ;
        $result = $this->db->query("select Title from ttp_report_products where ID=$ProductsID")->row();
        if($result){
            $sql1 = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,d.Donvi,f.Code,f.TransferDate from ttp_report_transferorder a,ttp_report_transferorder_details b,ttp_report_warehouse c,ttp_report_products d,ttp_report_transferorder_mobilization f where a.ID=f.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.WarehouseSender=c.ID and date(f.TransferDate)>='$fromdate' and date(f.TransferDate)<='$todate' and a.Status in(3,4) and a.WarehouseSender=$WarehouseID and b.ProductsID=$ProductsID order by b.ProductsID";
            $data1 = $this->db->query($sql1)->result();
            $sql2 = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,d.Donvi,e.MaXK,e.Ngayxuatkho from ttp_report_order a,ttp_report_orderdetails b,ttp_report_warehouse c,ttp_report_products d,ttp_report_export_warehouse e where a.ID=e.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.KhoID=c.ID and date(e.Ngayxuatkho)>='$fromdate' and date(e.Ngayxuatkho)<='$todate' and a.Status=0 and a.KhoID=$WarehouseID and a.CustomerID=9996 and b.ProductsID=$ProductsID order by e.Ngayxuatkho ASC";
            $data2 = $this->db->query($sql2)->result();
            $sql3 = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,d.Donvi,e.MaXK,e.Ngayxuatkho from ttp_report_order a,ttp_report_orderdetails b,ttp_report_warehouse c,ttp_report_products d,ttp_report_export_warehouse e where a.ID=e.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.KhoID=c.ID and date(e.Ngayxuatkho)>='$fromdate' and date(e.Ngayxuatkho)<='$todate' and a.Status in(7,9) and a.KhoID=$WarehouseID and a.CustomerID!=9996 and b.ProductsID=$ProductsID order by e.Ngayxuatkho ASC";
            $data3 = $this->db->query($sql3)->result();
            $data = array(
                'Products'  =>$result,
                'data1'     =>$data1,
                'data2'     =>$data2,
                'data3'     =>$data3
            );
            $this->load->view('warehouse_report_inventory_export_details',$data);
        }
    }

    public function scan_warehouse(){
        $result = $this->db->query("select b.Title,b.MaSP,a.ProductsID,a.ShipmentCode,a.ID as ShipmentID,c.MaKho,c.ID as WarehouseID from ttp_report_shipment a,ttp_report_products b,ttp_report_warehouse c where a.ProductsID=b.ID")->result();
        $data = array(
            'data'=>$result
        );
        $this->template->add_title('Scan Warehouse | Report Tools');
        $this->template->write_view('content','admin/warehouse_scan',$data);
        $this->template->render();
    }

    public function sync_data(){
        $date = isset($_POST['date']) ? $_POST['date'] : date('Y-m-d') ;
        $ProductsID = isset($_POST['products']) ? $_POST['products'] : 0 ;
        $ShipmentID = isset($_POST['shipment']) ? $_POST['shipment'] : 0 ;
        $WarehouseID = isset($_POST['warehouse']) ? $_POST['warehouse'] : 0 ;
        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and DateInventory='$date'")->row();
        if(!$check_exists){
            $last = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and DateInventory<'$date' order by DateInventory DESC limit 0,1")->row();
            if($last){
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$last->OnHand,$last->Available,$WarehouseID,$ShipmentID,'$date',0)");
            }else{
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,0,0,$WarehouseID,$ShipmentID,'$date',0)");
            }
        }
        echo "OK";
    }
}
?>
