<?php 
class Api_website extends Admin_Controller { 
 
 	public $user;
 	public $classname="api_website";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/api_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_system_website")->row();
        $nav = $nav ? $nav->nav : 0;
        $result = $this->db->query("select * from ttp_system_website $limit_str")->result();
        $data = array(
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/api_website/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/api_website/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Website system | Report Tools');
        $this->template->write_view('content','admin/api_website_home',$data);
        $this->template->render();
    }
    
    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Add website system | Report Tools');
        $this->template->write_view('content','admin/api_website_add',array('base_link'=>base_url().ADMINPATH.'/report/api_website/'));
        $this->template->render();
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Edit website system | Report Tools');
        $result = $this->db->query("select * from ttp_system_website where ID=$id")->row();
        if($result){
            $this->template->write_view('content','admin/api_website_edit',
                array(
                    'base_link' =>base_url().ADMINPATH.'/report/api_website/',
                    'data'      =>$result
                )
            );
            $this->template->render();
        }
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_system_website where ID=$id");
            $this->db->query("delete from ttp_system_client where WebsiteID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Published = isset($_POST['Published']) ? (int)$_POST['Published'] : 0 ;
        $Domain = isset($_POST['Domain']) ? trim(mysql_real_escape_string($_POST['Domain'])) : '' ;
        $IP = isset($_POST['IP']) ? trim(mysql_real_escape_string($_POST['IP'])) : '' ;
        $ServicesPath = isset($_POST['ServicesPath']) ? trim(mysql_real_escape_string($_POST['ServicesPath'])) : '' ;
        if($Domain!='' && $ServicesPath!='' && $IP!=''){
            $check = $this->db->query("select * from ttp_system_website where Domain='$Domain'")->row();
            if($check){
                redirect($_SERVER['HTTP_REFERER']);
            }
            $activekey = sha1($Domain.time().$IP);
            $data = array(
                'Domain'=>$Domain,
                'Published'=>$Published,
                'IP'    =>$IP,
                'ServicesPath'=>$ServicesPath,
                'ActiveKey'=>$activekey,
                'Created'=>date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_system_website',$data);
            $id = $this->db->insert_id();
            redirect(base_url().ADMINPATH.'/report/api_website/edit/'.$id);
        }
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Published = isset($_POST['Published']) ? (int)$_POST['Published'] : 0 ;
        $Domain = isset($_POST['Domain']) ? trim(mysql_real_escape_string($_POST['Domain'])) : '' ;
        $ActiveKey = isset($_POST['ActiveKey']) ? trim(mysql_real_escape_string($_POST['ActiveKey'])) : '' ;
        $IP = isset($_POST['IP']) ? trim(mysql_real_escape_string($_POST['IP'])) : '' ;
        $ID = isset($_POST['ID']) ? trim(mysql_real_escape_string($_POST['ID'])) : 0 ;
        $ServicesPath = isset($_POST['ServicesPath']) ? trim(mysql_real_escape_string($_POST['ServicesPath'])) : '' ;
        if($Domain!='' && $ServicesPath!='' && $IP!=''){
            $result = $this->db->query("select * from ttp_system_website where ID=$ID")->row();
            if($result){
                $check = $this->db->query("select * from ttp_system_website where Domain='$Domain' and ID!=$ID")->row();
                if($check){
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $activekey = $ActiveKey!='' ? $ActiveKey : sha1($Domain.time().$IP);
                $data = array(
                    'Domain'=>$Domain,
                    'Published'=>$Published,
                    'IP'    =>$IP,
                    'ServicesPath'=>$ServicesPath,
                    'ActiveKey'=>$activekey
                );
                $this->db->where("ID",$ID);
                $this->db->update('ttp_system_website',$data);
                redirect(base_url().ADMINPATH.'/report/api_website/edit/'.$ID);
            }
        }
    }
}
?>