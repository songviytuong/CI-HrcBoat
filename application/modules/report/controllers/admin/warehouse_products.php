<?php 
class Warehouse_products extends Admin_Controller { 
 
    public $limit = 20;
 	public $user;
 	public $classname="warehouse_products";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_products a,ttp_report_trademark b where a.TrademarkID=b.ID and a.ParentID=0 $fillter")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,b.Title as Trademark from ttp_report_products a,ttp_report_trademark b where a.TrademarkID=b.ID and a.ParentID=0 $fillter order by a.ID DESC $limit_str")->result();
        $data = array(
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_products/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_products/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Products | Warehouse Report Tools');
		$this->template->write_view('content','admin/warehouse_products_home',$data);
		$this->template->render();
	}

    public function get_data_fillter(){
        $type = isset($_POST['Type']) ? $_POST['Type'] : 0 ;
        if($type==1){
            echo "<a onclick='set_fill_tr(this,2)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select b.Title,b.ID from ttp_report_trademark b order by b.Title ASC")->result();
            if(count($object)>0){
                foreach ($object as $row) {
                    echo "<a onclick='set_fill_tr(this,2)' value='$row->ID'>$row->Title</a>";
                }
            }
        }elseif($type==2){
            echo "<a onclick='set_fill_tr(this,0)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select a.ID,a.MaSP from ttp_report_products a where a.ParentID=0 order by a.MaSP ASC")->result();
            if(count($object)>0){
                foreach ($object as $row) {
                    $row->MaSP = str_replace("'",'',$row->MaSP);
                    echo "<a onclick='set_fill_tr(this,0)' value='$row->MaSP' title='$row->MaSP'>$row->MaSP</a>";
                }
            }
        }elseif($type==3){
            echo "<a onclick='set_fill_tr(this,1)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select a.ID,a.Title from ttp_report_products a where a.ParentID=0 order by a.Title ASC")->result();
            if(count($object)>0){
                foreach ($object as $row) {
                    $row->Title = str_replace("'",'',$row->Title);
                    echo "<a onclick='set_fill_tr(this,1)' value='$row->Title' title='$row->Title'>$row->Title</a>";
                }
            }
        }elseif($type==4){
            echo "<a onclick='set_fill_tr(this,6)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select DISTINCT a.CategoriesID from ttp_report_products a where a.ParentID=0 and CategoriesID!='[]'")->result();
            $arr = array();
            if(count($object)>0){
                foreach ($object as $row) {
                    $temp = json_decode($row->CategoriesID,true);
                    if(is_array($temp) && count($temp)>0){
                        $arr = array_merge($arr,$temp);
                    }
                }
            }
            $arr = array_unique($arr);
            if(count($arr)>0){
                $arr = implode(',',$arr);
                $categories = $this->db->query("select Path from ttp_report_categories where ID in($arr)")->result();
                $arr_parent = array();
                if(count($categories)>0){
                    foreach($categories as $row){
                        $temp_path = explode('/',$row->Path);
                        $temp_path = isset($temp_path[0]) ? $temp_path[0] : 0 ;
                        if($temp_path>0){
                            $arr_parent[] = $temp_path;
                        }
                    }
                }
                if(count($arr_parent)>0){
                    $arr_parent = implode(',', $arr_parent);
                    $categories = $this->db->query("select Title,ID from ttp_report_categories where ID in($arr_parent)")->result();
                    if(count($categories)>0){
                        foreach ($categories as $row) {
                            echo "<a onclick='set_fill_tr(this,6)' value='$row->ID' title='$row->Title'>$row->Title</a>";
                        }
                    }
                }
            }
        }elseif($type==5){
            echo "<a onclick='set_fill_tr(this,5)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select DISTINCT a.CategoriesID from ttp_report_products a where a.ParentID=0 and CategoriesID!='[]'")->result();
            $arr = array();
            if(count($object)>0){
                foreach ($object as $row) {
                    $temp = json_decode($row->CategoriesID,true);
                    if(is_array($temp) && count($temp)>0){
                        $arr = array_merge($arr,$temp);
                    }
                }
            }
            $arr = array_unique($arr);
            if(count($arr)>0){
                $arr = implode(',',$arr);
                $categories = $this->db->query("select ID,Title from ttp_report_categories where ID in($arr)")->result();
                if(count($categories)>0){
                    foreach($categories as $row){
                        echo "<a onclick='set_fill_tr(this,5)' value='$row->ID' title='$row->Title'>$row->Title</a>";
                    }
                }
            }
        }elseif($type==6){
            echo "<a onclick='set_fill_tr(this,3)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select DISTINCT a.Price from ttp_report_products a where a.ParentID=0 order by a.Price ASC")->result();
            if(count($object)>0){
                foreach ($object as $row) {
                    echo "<a onclick='set_fill_tr(this,3)' value='$row->Price' title='$row->Price'>".number_format($row->Price)."</a>";
                }
            }
        }elseif($type==7){
            echo "<a onclick='set_fill_tr(this,4)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select DISTINCT a.CurrentAmount from ttp_report_products a where a.ParentID=0 order by a.CurrentAmount ASC")->result();
            if(count($object)>0){
                foreach ($object as $row) {
                    echo "<a onclick='set_fill_tr(this,4)' value='$row->CurrentAmount' title='$row->CurrentAmount'>".number_format($row->CurrentAmount)."</a>";
                }
            }
        }
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $result = $this->db->query("select * from ttp_report_products where ID=$id")->row();
        if($result){
            $this->template->add_title('Products Edit | Warehouse Report Tools');
            $this->template->write_view('content','admin/warehouse_products_edit',array('data'=>$result));
            $this->template->render();
        }
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Products Add | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_products_add');
        $this->template->render();
    }

    public function addnew(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $MaSP = isset($_POST['MaSP']) ? trim($_POST['MaSP']) : '' ;
        $TypeProducts = isset($_POST['TypeProducts']) ? (int)$_POST['TypeProducts'] : 0 ;
        $TypeProducts = $TypeProducts>9 ? 0 : $TypeProducts ;
        $AcceptGift = isset($_POST['AcceptGift']) ? $_POST['AcceptGift'] : 0 ;
        $AcceptGift = $AcceptGift==="on" ? 1 : 0 ;
        $Featured = isset($_POST['Featured']) ? (int)$_POST['Featured'] : 0 ;
        $SaleGift = isset($_POST['SaleGift']) ? trim($_POST['SaleGift']) : '' ;
        $CategoriesID = isset($_POST['CategoriesID']) ? $_POST['CategoriesID'] : array() ;
        $CategoriesID = array_unique($CategoriesID);
        $PropertiesID = isset($_POST['PropertiesID']) ? $_POST['PropertiesID'] : array() ;
        $PropertiesPublished = isset($_POST['PropertiesPublished']) ? $_POST['PropertiesPublished'] : array() ;
        $VariantValue = array('Properties'=>$PropertiesID,'Published'=>$PropertiesPublished);
        $VariantType = isset($_POST['VariantType']) ? trim($_POST['VariantType']) : 0 ;
        $Title = isset($_POST['Title']) ? trim($_POST['Title']) : '' ;
        $Description = isset($_POST['Description']) ? trim($_POST['Description']) : '' ;
        $MetaTitle = isset($_POST['MetaTitle']) ? trim($_POST['MetaTitle']) : '' ;
        $MetaKeywords = isset($_POST['MetaKeywords']) ? trim($_POST['MetaKeywords']) : '' ;
        $MetaDescription = isset($_POST['MetaDescription']) ? trim($_POST['MetaDescription']) : '' ;
        $Weight = isset($_POST['Weight']) ? trim($_POST['Weight']) : 0 ;
        $Length = isset($_POST['Length']) ? trim($_POST['Length']) : 0 ;
        $Width = isset($_POST['Width']) ? trim($_POST['Width']) : 0 ;
        $Height = isset($_POST['Height']) ? trim($_POST['Height']) : 0 ;
        $Startday = isset($_POST['Startday']) ? trim($_POST['Startday']) : '' ;
        $Stopday = isset($_POST['Stopday']) ? trim($_POST['Stopday']) : '' ;
        $Published = isset($_POST['Published']) ? trim($_POST['Published']) : 0 ;
        $Show = isset($_POST['Show']) ? trim($_POST['Show']) : 0 ;
        $CountryID = isset($_POST['CountryID']) ? trim($_POST['CountryID']) : 0 ;
        $TrademarkID = isset($_POST['TrademarkID']) ? trim($_POST['TrademarkID']) : 0 ;
        $Donvi = isset($_POST['Donvi']) ? trim($_POST['Donvi']) : '' ;
        $Price = isset($_POST['Price']) ? trim($_POST['Price']) : 0 ;
        $RootPrice = isset($_POST['RootPrice']) ? trim($_POST['RootPrice']) : 0 ;
        $SpecialPrice = isset($_POST['SpecialPrice']) ? trim($_POST['SpecialPrice']) : '' ;
        $Special_startday = isset($_POST['Special_startday']) ? trim($_POST['Special_startday']) : '' ;
        $Special_stopday = isset($_POST['Special_stopday']) ? trim($_POST['Special_stopday']) : '' ;
        $VAT = isset($_POST['VAT']) ? trim($_POST['VAT']) : 0 ;
        $SLCurrent = isset($_POST['SLCurrent']) ? trim($_POST['SLCurrent']) : 0 ;
        $SLOutOfStock = isset($_POST['SLOutOfStock']) ? trim($_POST['SLOutOfStock']) : 0 ;
        $SLMinInCart = isset($_POST['SLMinInCart']) ? trim($_POST['SLMinInCart']) : 0 ;
        $SLMaxInCart = isset($_POST['SLMaxInCart']) ? trim($_POST['SLMaxInCart']) : 0 ;
        $AddcartNegativeNumber = isset($_POST['AddcartNegativeNumber']) ? trim($_POST['AddcartNegativeNumber']) : 0 ;
        $Available = isset($_POST['Available']) ? trim($_POST['Available']) : 0 ;
        $PriceGroup_Title = isset($_POST['PriceGroup_Title']) ? $_POST['PriceGroup_Title'] : array() ;
        $PriceGroup_Price = isset($_POST['PriceGroup_Price']) ? $_POST['PriceGroup_Price'] : array() ;
        $PriceRange_Title = isset($_POST['PriceRange_Title']) ? $_POST['PriceRange_Title'] : array() ;
        $PriceRange_Amount = isset($_POST['PriceRange_Amount']) ? $_POST['PriceRange_Amount'] : array() ;
        $PriceRange_Price = isset($_POST['PriceRange_Price']) ? $_POST['PriceRange_Price'] : array() ;
        $Title = $Title=="" ? "Sản phẩm mới" : $Title ;
        $MaSP = $MaSP=="" ? time() : $MaSP ;
        $Content = isset($_POST['Content']) ? $_POST['Content'] : '' ;
        $Instruction = isset($_POST['Instruction']) ? $_POST['Instruction'] : '' ;
        if($MaSP!='' && $Title!=''){
            $check = $this->db->query("select MaSP from ttp_report_products where MaSP='$MaSP'")->row();
            if($check){
                echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                echo "<script>alert('Mã sản phẩm này đã có trong hệ thống , vui lòng chọn mã khác !');window.location='".$_SERVER['HTTP_REFERER']."'</script>";
                return false;
            }
            $max = $this->db->query("select Max(ID) as ID from ttp_report_products")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $data = array(
                'ID'            => $max,
                'Path'          => $max,
                'MaSP'          => $MaSP,
                'Title'         => $Title,
                'Content'       => $Content,
                'Instruction'   => $Instruction,
                'Description'   => $Description,
                'Weight'        => $Weight,
                'Length'        => $Length,
                'Width'         => $Width,
                'Height'        => $Height,
                'NewStartDay'   => $Startday,
                'NewStopDay'    => $Stopday,
                'SpecialStartday'=> $Special_startday,
                'SpecialStopday'=> $Special_stopday,
                'Status'        => $Show,
                'Published'     => $Published,
                'CountryID'     => $CountryID,
                'TrademarkID'   => $TrademarkID,
                'VAT'           => $VAT,
                'CurrentAmount' => $SLCurrent,
                'WarningAmount' => $SLOutOfStock,
                'MinCartAmount' => $SLMinInCart,
                'MaxCartAmount' => $SLMaxInCart,
                'PutcartNegative'=> $AddcartNegativeNumber,
                'InventoryStatus'=> $Available,
                'Price'         => $Price,
                'RootPrice'     => $RootPrice,
                'SpecialPrice'  => $SpecialPrice,
                'Donvi'         => $Donvi,
                'CategoriesID'  => json_encode($CategoriesID),
                'VariantValue'  => json_encode($VariantValue),
                'VariantType'   => $VariantType,
                'MetaTitle'     => $MetaTitle,
                'MetaKeywords'  => $MetaKeywords,
                'MetaDescription'=> $MetaDescription,
                'AcceptGift'    => $AcceptGift,
                'SaleGift'      => $SaleGift,
                'Created'       => date('Y-m-d H:i:s'),
                'LastEdited'    => date('Y-m-d H:i:s'),
                'TypeProducts'  => $TypeProducts,
                'Featured'      => $Featured
            );
            $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title ;
            $data['Alias'] = $this->lib->alias($Alias);
            $check = $this->db->query("select ID from ttp_report_products where Alias='$Alias'")->row();
            if($check){
                $data['Alias'] .= $max;
            }
            $IDstr = str_pad($max, 8, '0', STR_PAD_LEFT);
            $data['Barcode'] = "603".$TypeProducts.$IDstr.'1';
            $this->db->insert('ttp_report_products',$data);
            $SaveAndExit = isset($_POST['SaveAndExit']) ? $_POST['SaveAndExit'] : 0 ;
            if($SaveAndExit==1){
                redirect(ADMINPATH.'/report/warehouse_products/');
            }
            $currenttab = isset($_POST['currenttab']) ? $_POST['currenttab'] : '' ;
            $currenttab = $currenttab!='' ? '?tab='.$currenttab : '' ;
            $this->write_log("Add products from ttp_report_products where ID=".$max."\n JSON = ".json_encode($data));
            redirect(ADMINPATH.'/report/warehouse_products/edit/'.$max.$currenttab);
        }
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? trim($_POST['ID']) : 0 ;
        $MaSP = isset($_POST['MaSP']) ? trim($_POST['MaSP']) : '' ;
        $TypeProducts = isset($_POST['TypeProducts']) ? (int)$_POST['TypeProducts'] : 0 ;
        $TypeProducts = $TypeProducts>9 ? 0 : $TypeProducts ;
        $AcceptGift = isset($_POST['AcceptGift']) ? $_POST['AcceptGift'] : 0 ;
        $AcceptGift = $AcceptGift==="on" ? 1 : 0 ;
        $Featured = isset($_POST['Featured']) ? (int)$_POST['Featured'] : 0 ;
        $SaleGift = isset($_POST['SaleGift']) ? trim($_POST['SaleGift']) : '' ;
        $CategoriesID = isset($_POST['CategoriesID']) ? $_POST['CategoriesID'] : array() ;
        $CategoriesID = array_unique($CategoriesID);
        $PropertiesID = isset($_POST['PropertiesID']) ? $_POST['PropertiesID'] : array() ;
        $PropertiesPublished = isset($_POST['PropertiesPublished']) ? $_POST['PropertiesPublished'] : array() ;
        $VariantValue = array('Properties'=>$PropertiesID,'Published'=>$PropertiesPublished);
        $VariantType = isset($_POST['VariantType']) ? trim($_POST['VariantType']) : 0 ;
        $Title = isset($_POST['Title']) ? trim($_POST['Title']) : '' ;
        $YoutubeCode  = isset($_POST['YoutubeCode']) ? mysql_real_escape_string(trim($_POST['YoutubeCode'])) : '' ;
        $Description = isset($_POST['Description']) ? trim($_POST['Description']) : '' ;
        $MetaTitle = isset($_POST['MetaTitle']) ? trim($_POST['MetaTitle']) : '' ;
        $MetaKeywords = isset($_POST['MetaKeywords']) ? trim($_POST['MetaKeywords']) : '' ;
        $MetaDescription = isset($_POST['MetaDescription']) ? trim($_POST['MetaDescription']) : '' ;
        $Weight = isset($_POST['Weight']) ? trim($_POST['Weight']) : 0 ;
        $Length = isset($_POST['Length']) ? trim($_POST['Length']) : 0 ;
        $Width = isset($_POST['Width']) ? trim($_POST['Width']) : 0 ;
        $Height = isset($_POST['Height']) ? trim($_POST['Height']) : 0 ;
        $Startday = isset($_POST['Startday']) ? trim($_POST['Startday']) : '' ;
        $Stopday = isset($_POST['Stopday']) ? trim($_POST['Stopday']) : '' ;
        $Published = isset($_POST['Published']) ? trim($_POST['Published']) : 0 ;
        $Show = isset($_POST['Show']) ? trim($_POST['Show']) : 0 ;
        $CountryID = isset($_POST['CountryID']) ? trim($_POST['CountryID']) : 0 ;
        $TrademarkID = isset($_POST['TrademarkID']) ? trim($_POST['TrademarkID']) : 0 ;
        $Donvi = isset($_POST['Donvi']) ? trim($_POST['Donvi']) : '' ;
        $Price = isset($_POST['Price']) ? trim($_POST['Price']) : 0 ;
        $RootPrice = isset($_POST['RootPrice']) ? trim($_POST['RootPrice']) : 0 ;
        $SpecialPrice = isset($_POST['SpecialPrice']) ? trim($_POST['SpecialPrice']) : '' ;
        $Special_startday = isset($_POST['Special_startday']) ? trim($_POST['Special_startday']) : '' ;
        $Special_stopday = isset($_POST['Special_stopday']) ? trim($_POST['Special_stopday']) : '' ;
        $VAT = isset($_POST['VAT']) ? trim($_POST['VAT']) : 0 ;
        $SLCurrent = isset($_POST['SLCurrent']) ? trim($_POST['SLCurrent']) : 0 ;
        $SLOutOfStock = isset($_POST['SLOutOfStock']) ? trim($_POST['SLOutOfStock']) : 0 ;
        $SLMinInCart = isset($_POST['SLMinInCart']) ? trim($_POST['SLMinInCart']) : 0 ;
        $SLMaxInCart = isset($_POST['SLMaxInCart']) ? trim($_POST['SLMaxInCart']) : 0 ;
        $AddcartNegativeNumber = isset($_POST['AddcartNegativeNumber']) ? trim($_POST['AddcartNegativeNumber']) : 0 ;
        $Available = isset($_POST['Available']) ? trim($_POST['Available']) : 0 ;
        $PriceGroup_Title = isset($_POST['PriceGroup_Title']) ? $_POST['PriceGroup_Title'] : array() ;
        $PriceGroup_Price = isset($_POST['PriceGroup_Price']) ? $_POST['PriceGroup_Price'] : array() ;
        $PriceRange_Title = isset($_POST['PriceRange_Title']) ? $_POST['PriceRange_Title'] : array() ;
        $PriceRange_Amount = isset($_POST['PriceRange_Amount']) ? $_POST['PriceRange_Amount'] : array() ;
        $PriceRange_Price = isset($_POST['PriceRange_Price']) ? $_POST['PriceRange_Price'] : array() ;
        $Title = $Title=="" ? "Sản phẩm mới" : $Title ;
        $Content = isset($_POST['Content']) ? $_POST['Content'] : '' ;
        $Instruction = isset($_POST['Instruction']) ? $_POST['Instruction'] : '' ;
        $MaSP = $MaSP=="" ? time() : $MaSP ;
        if($MaSP!='' && $Title!=''){
            $object_products = $this->db->query("select * from ttp_report_products where ID=$ID")->row();
            if($object_products){
                $data = array(
                    'MaSP'          => $MaSP,
                    'Title'         => $Title,
                    'Content'       => $Content,
                    'Instruction'   => $Instruction,
                    'Description'   => $Description,
                    'Weight'        => $Weight,
                    'Length'        => $Length,
                    'Width'         => $Width,
                    'Height'        => $Height,
                    'NewStartDay'   => $Startday,
                    'NewStopDay'    => $Stopday,
                    'SpecialStartday'=> $Special_startday,
                    'SpecialStopday'=> $Special_stopday,
                    'Status'        => $Show,
                    'Published'     => $Published,
                    'CountryID'     => $CountryID,
                    'TrademarkID'   => $TrademarkID,
                    'VAT'           => $VAT,
                    'CurrentAmount' => $SLCurrent,
                    'WarningAmount' => $SLOutOfStock,
                    'MinCartAmount' => $SLMinInCart,
                    'MaxCartAmount' => $SLMaxInCart,
                    'PutcartNegative'=> $AddcartNegativeNumber,
                    'InventoryStatus'=> $Available,
                    'Price'         => $Price,
                    'RootPrice'     => $RootPrice,
                    'SpecialPrice'  => $SpecialPrice,
                    'Donvi'         => $Donvi,
                    'CategoriesID'  => json_encode($CategoriesID),
                    'VariantValue'  => json_encode($VariantValue),
                    'VariantType'   => $VariantType,
                    'MetaTitle'     => $MetaTitle,
                    'MetaKeywords'  => $MetaKeywords,
                    'MetaDescription'=> $MetaDescription,
                    'AcceptGift'    => $AcceptGift,
                    'SaleGift'      => $SaleGift,
                    'LastEdited'    => date('Y-m-d H:i:s'),
                    'TypeProducts'  => $TypeProducts,
                    'Featured'      => $Featured
                );
                $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title ;
                $data['Alias'] = $this->lib->alias($Alias);
                $check = $this->db->query("select ID from ttp_report_products where Alias='$Alias' and ID!=$ID")->row();
                if($check){
                    $data['Alias'] .= $ID;
                }
                $IDstr = str_pad($ID, 8, '0', STR_PAD_LEFT);
                $data['Barcode'] = "603".$TypeProducts.$IDstr.'1';
                $video = json_decode($object_products->Video);
                $video = is_object($video) ? $video : (object)array() ;
                $videoarr = array('YoutubeCode'=>$YoutubeCode,'VideoSource'=>$video->VideoSource,'ImageVideo'=>$video->ImageVideo);
                if(isset($_FILES['VideoSource'])){
                    if($_FILES['VideoSource']['error']==0){
                        $videoarr['YoutubeCode'] = '';
                        $videoarr['VideoSource'] = $this->upload_image_single('VideoSource','video');
                    }
                }
                if(isset($_FILES['ImageVideo'])){
                    if($_FILES['ImageVideo']['error']==0){
                        $videoarr['ImageVideo'] = $this->upload_image_single('ImageVideo');
                    }
                }
                $data['Video'] = json_encode($videoarr);
                $this->db->where('ID',$ID);
                $this->db->update('ttp_report_products',$data);
                if($VariantType==0){
                    $this->db->query("delete from ttp_report_images_products where ProductsID in(select ID from ttp_report_products where ParentID=$ID)");
                    $this->db->query("delete from ttp_report_products where ParentID=$ID");
                }
                $SaveAndExit = isset($_POST['SaveAndExit']) ? $_POST['SaveAndExit'] : 0 ;
                if($SaveAndExit==1){
                    redirect(ADMINPATH.'/report/warehouse_products/');
                }
                $currenttab = isset($_POST['currenttab']) ? $_POST['currenttab'] : '' ;
                $currenttab = $currenttab=="tab8" && $VariantType==0 ? "tab1" : $currenttab ;
                $currenttab = $currenttab!='' ? '?tab='.$currenttab : '' ;
                $this->write_log("Update products from ttp_report_products where ID=".$ID."\n JSON = ".json_encode($data));
                redirect(ADMINPATH.'/report/warehouse_products/edit/'.$ID.$currenttab);
            }
        }
    }

    public function dupplicate($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $object = $this->db->query("select * from ttp_report_products where ID=$id")->row();
        if($object){
            $max = $this->db->query("select Max(ID) as ID from ttp_report_products")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $data = array(
                'ID'            => $max,
                'Path'          => $max,
                'TypeProducts'  => $object->TypeProducts,
                'MaSP'          => $object->MaSP,
                'Title'         => $object->Title.' (Sao chép)',
                'Description'   => $object->Description,
                'Weight'        => $object->Weight,
                'Length'        => $object->Length,
                'Width'         => $object->Width,
                'Height'        => $object->Height,
                'NewStartDay'   => $object->NewStartDay,
                'NewStopDay'    => $object->NewStopDay,
                'SpecialStartday'=> $object->SpecialStartday,
                'SpecialStopday'=> $object->SpecialStopday,
                'Status'        => $object->Status,
                'Published'     => $object->Published,
                'CountryID'     => $object->CountryID,
                'TrademarkID'   => $object->TrademarkID,
                'VAT'           => $object->VAT,
                'CurrentAmount' => $object->CurrentAmount,
                'WarningAmount' => $object->WarningAmount,
                'MinCartAmount' => $object->MinCartAmount,
                'MaxCartAmount' => $object->MaxCartAmount,
                'PutcartNegative'=> $object->PutcartNegative,
                'InventoryStatus'=> $object->InventoryStatus,
                'Price'         => $object->Price,
                'RootPrice'     => $object->RootPrice,
                'SpecialPrice'  => $object->SpecialPrice,
                'Donvi'         => $object->Donvi,
                'CategoriesID'  => $object->CategoriesID,
                'VariantValue'  => $object->VariantValue,
                'VariantType'   => $object->VariantType,
                'MetaTitle'     => $object->MetaTitle,
                'MetaKeywords'  => $object->MetaKeywords,
                'MetaDescription'=> $object->MetaDescription,
                'AcceptGift'    => $object->AcceptGift,
                'SaleGift'      => $object->SaleGift,
                'Created'       => date('Y-m-d H:i:s'),
                'LastEdited'    => date('Y-m-d H:i:s')
            );
            $IDstr = str_pad($object->ID, 8, '0', STR_PAD_LEFT);
            $data['Barcode'] = "603".$object->TypeProducts.$IDstr.'1';
            $this->db->insert('ttp_report_products',$data);
            $Alias = $this->lib->alias($object->Title.' (Sao chép)');
            $check = $this->db->query("select ID from diva_links where Url='$Alias'")->row();
            if($check){
                $Alias .= $max;
            }
            $data_links = array(
                'Url'   =>$Alias,
                'Type'  =>1,
                'CategoriesID'=>0,
                'Published'=>1,
                'ProductsID'=>$max
            );
            $this->db->insert('diva_links',$data_links);
            $image = $this->db->query("select * from ttp_report_images_products where ProductsID=$object->ID")->result();
            if(count($image)>0){
                foreach($image as $row){
                    $data = array(
                        'Name'      => $row->Name,
                        'Url'       => $row->Url,
                        'Created'   => date('Y-m-d H:i:s'),
                        'STT'       => $row->STT,
                        'Label'     => $row->Label,
                        'PrimaryImage' => $row->PrimaryImage,
                        'MiniImage' => $row->MiniImage,
                        'ThumbImage'=> $row->ThumbImage,
                        'ProductsID'=> $max
                    );
                    $this->db->insert('ttp_report_images_products',$data);
                }
            }
            $this->write_log("Duplicate products from ttp_report_products where ID=".$max."\n JSON = ".json_encode($data));
            redirect(ADMINPATH.'/report/warehouse_products/edit/'.$max);
        }
    }

    public function variant_add(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $result = $this->db->query("select * from ttp_report_products where ID=$ID")->row();
        if($result){
            $max = $this->db->query("select Max(ID) as ID from ttp_report_products")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $data = array(
                'ID'            => $max,
                'Path'          => $result->Path.'/'.$max,
                'MaSP'          => $result->MaSP."_".$max,
                'TypeProducts'  => $result->TypeProducts,
                'Title'         => $result->Title,
                'Description'   => $result->Description,
                'Weight'        => $result->Weight,
                'Length'        => $result->Length,
                'Width'         => $result->Width,
                'Height'        => $result->Height,
                'NewStartDay'   => $result->NewStartDay,
                'NewStopDay'    => $result->NewStopDay,
                'SpecialStartday'=> $result->SpecialStartday,
                'SpecialStopday'=> $result->SpecialStopday,
                'Status'        => $result->Status,
                'Published'     => $result->Published,
                'CountryID'     => $result->CountryID,
                'TrademarkID'   => $result->TrademarkID,
                'VAT'           => $result->VAT,
                'CurrentAmount' => 0,
                'WarningAmount' => $result->WarningAmount,
                'MinCartAmount' => $result->MinCartAmount,
                'MaxCartAmount' => $result->MaxCartAmount,
                'PutcartNegative'=> $result->PutcartNegative,
                'InventoryStatus'=> $result->InventoryStatus,
                'Price'         => $result->Price,
                'RootPrice'     => $result->RootPrice,
                'SpecialPrice'  => $result->SpecialPrice,
                'Donvi'         => $result->Donvi,
                'CategoriesID'  => $result->CategoriesID,
                'VariantValue'  => "",
                'VariantType'   => 1,
                'ParentID'      => $result->ID,
                'AcceptGift'    => $result->AcceptGift,
                'SaleGift'      => $result->SaleGift,
                'Created'       => date('Y-m-d H:i:s'),
                'LastEdited'    => date('Y-m-d H:i:s')
            );
            $IDstr = str_pad($result->ID, 8, '0', STR_PAD_LEFT);
            $data['Barcode'] = "603".$result->TypeProducts.$IDstr.'1';
            $this->db->insert('ttp_report_products',$data);
            $Alias = $this->lib->alias($result->Title);
            $check = $this->db->query("select ID from diva_links where Url='$Alias'")->row();
            if($check){
                $Alias .= $max;
            }
            $data_links = array(
                'Url'   =>$Alias,
                'Type'  =>1,
                'CategoriesID'=>0,
                'Published'=>1,
                'ProductsID'=>$max
            );
            $this->db->insert('diva_links',$data_links);
            $result1 = $this->db->query("select * from ttp_report_products where ID=$max")->row();
            $this->load->view('warehouse_products_variant_add',array('data'=>$result1,'parentdata'=>$result));
            $this->write_log("add variant from ttp_report_products where ID=".$max."\n JSON = ".json_encode($data));
        }
    }

    public function update_variant(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? trim($_POST['ID']) : 0 ;
        $MaSP = isset($_POST['SKU']) ? trim($_POST['SKU']) : '' ;
        $VariantValue = isset($_POST['Properties']) ? trim($_POST['Properties']) : '' ;
        $VariantValue = explode("|",$VariantValue);
        if(isset($VariantValue[0])){
            unset($VariantValue[0]);
        }
        $Description = isset($_POST['Description']) ? trim($_POST['Description']) : '' ;
        $Weight = isset($_POST['Weight']) ? trim($_POST['Weight']) : 0 ;
        $Length = isset($_POST['Length']) ? trim($_POST['Length']) : 0 ;
        $Width = isset($_POST['Width']) ? trim($_POST['Width']) : 0 ;
        $Height = isset($_POST['Height']) ? trim($_POST['Height']) : 0 ;
        $Published = isset($_POST['Published']) ? trim($_POST['Published']) : 0 ;
        $Available = isset($_POST['InventoryStatus']) ? trim($_POST['InventoryStatus']) : 0 ;
        $Price = isset($_POST['Price']) ? trim($_POST['Price']) : 0 ;
        $SpecialPrice = isset($_POST['SpecialPrice']) ? trim($_POST['SpecialPrice']) : '' ;
        $Special_startday = isset($_POST['StartSpecialPrice']) ? trim($_POST['StartSpecialPrice']) : '' ;
        $Special_stopday = isset($_POST['StopSpecialPrice']) ? trim($_POST['StopSpecialPrice']) : '' ;
        $Namebonus = isset($_POST['Namebonus']) ? trim($_POST['Namebonus']) : '' ;
        $data = array(
            'Title'         => $Namebonus,
            'MaSP'          => $MaSP,
            'Description'   => $Description,
            'Weight'        => $Weight,
            'Length'        => $Length,
            'Width'         => $Width,
            'Height'        => $Height,
            'Published'     => $Published,
            'InventoryStatus'=> $Available,
            'Price'         => $Price,
            'SpecialPrice'  => $SpecialPrice,
            'VariantValue'  => json_encode($VariantValue),
            'SpecialStartday' => $Special_startday,
            'SpecialStopday'=> $Special_stopday,
            'LastEdited'    => date('Y-m-d H:i:s')
        );
        $this->db->where('ID',$ID);
        $this->db->update('ttp_report_products',$data);
        $links = $this->db->query("select * from diva_links where ProductsID=$ID")->row();
        $Alias = $this->lib->alias($Namebonus);
        $check = $this->db->query("select ID from diva_links where Url='$Alias' and ProductsID!=$ID")->row();
        if($check){
            $Alias .= $ID;
        }
        if($links){
            $data_links = array(
                'Url'   =>$Alias,
                'Type'  =>1,
                'CategoriesID'=>0,
                'Published'=>1,
                'ProductsID'=>$ID
            );
            $this->db->where('ID',$links->ID);
            $this->db->update('diva_links',$data_links);
        }else{
             $data_links = array(
                'Url'   =>$Alias,
                'Type'  =>1,
                'CategoriesID'=>0,
                'Published'=>1,
                'ProductsID'=>$ID
            );
            $this->db->insert('diva_links',$data_links);
        }
        $this->write_log("update variant from ttp_report_products where ID=".$ID."\n JSON = ".json_encode($data));
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $this->db->query("delete from ttp_report_products where ID=$id");
        $this->db->query("delete from ttp_report_products where ParentID=$id");
        $this->db->query("delete from ttp_report_images_products where ProductsID=$id");
        $this->db->query("delete from ttp_report_images_products where ProductsID in(select ID from ttp_report_products where ParentID=$id)");
        $this->db->query("delete from diva_links where ProductsID=$id");
        $this->write_log("delete products from ttp_report_products where ID=".$id);
        redirect(ADMINPATH.'/report/warehouse_products/');
    }

    public function delete_variant(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $this->db->query("delete from ttp_report_products where ID=$ID");
        $this->db->query("delete from ttp_report_products where ParentID=$ID");
        $this->db->query("delete from ttp_report_images_products where ProductsID=$ID");
        $this->db->query("delete from diva_links where ProductsID=$ID");
        $this->write_log("delete variant from ttp_report_products where ID=".$ID);
    }

    public function changeimagerow(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        if(isset($_FILES['file'])){
            if($_FILES['file']['error']==0){
                $this->upload_to = "products/$ID";
                $data = $this->upload_image_single_report("ttp_report_images_products",array(),false);
                if($data!=''){
                    $this->write_log("change image to ttp_report_images_products where ProductsID=".$ID);
                    $imagerow = $this->db->query("select ProductsID,Url from ttp_report_images_products where ID=$ID")->row();
                    if($imagerow){
                        $this->db->query("update ttp_report_images_products set Url='$data' where ID=$ID");
                        $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$imagerow->ProductsID order by STT ASC")->result();
                        if(count($data_image)>0){
                            $datachange = array();
                            $primary = "";
                            foreach($data_image as $row){
                                $primary = $row->PrimaryImage==1 ? $row->Url : $primary ;
                                $datachange[] = $row->Url;
                            }
                            $primary = $primary=='' ? $row->Url : $primary ;
                            $datachange = json_encode($datachange);
                            $this->db->query("update ttp_report_products set AlbumImage='$datachange',PrimaryImage='$primary' where ID=$imagerow->ProductsID");
                        }
                        echo file_exists($this->lib->get_thumb($data)) ? $this->lib->get_thumb($data) : '' ;
                        return;
                    }
                }
            }
        }
        echo "False";
    }

    public function upload_image(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        if(isset($_FILES['file'])){
            if($_FILES['file']['error']==0){
                $this->upload_to = "products/$ID";
                $this->upload_image_single_report("ttp_report_images_products",array('ProductsID'=>$ID,'Primary'=>0));
                $this->write_log("upload image to ttp_report_images_products where ProductsID=".$ID);
                $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$ID order by STT ASC")->result();
                if(count($data_image)>0){
                    $data = array();
                    $primary = "";
                    foreach($data_image as $row){
                        $primary = $row->PrimaryImage==1 ? $row->Url : $primary ;
                        $data[] = $row->Url;
                    }
                    $primary = $primary=='' ? $row->Url : $primary ;
                    $data = json_encode($data);
                    $this->db->query("update ttp_report_products set AlbumImage='$data',PrimaryImage='$primary' where ID=$ID");
                }
                echo "OK";
            }else{
                echo "False";
            }
        }else{
            echo "False";
        }
    }

    public function delete_images(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        if($ID!=''){
            $image = $this->db->query("select ProductsID from ttp_report_images_products where ID=$ID")->row();
            if($image){
                $this->db->query("delete from ttp_report_images_products where ID=".$ID);
                $this->write_log("delete from ttp_report_images_products where ID=".$ID);
                $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$image->ProductsID order by STT ASC")->result();
                if(count($data_image)>0){
                    $data = array();
                    $primary = "";
                    foreach($data_image as $row){
                        $primary = $row->PrimaryImage==1 ? $row->Url : $primary ;
                        $data[] = $row->Url;
                    }
                    $primary = $primary=='' ? $row->Url : $primary ;
                    $data = json_encode($data);
                    $this->db->query("update ttp_report_products set AlbumImage='$data',PrimaryImage='$primary' where ID=$image->ProductsID");
                }
            }
        }
    }

    public function removefileondisc($Url){
        @unlink($Url);
        $thumbfile = $this->get_thumb($Url);
        if(file_exists($thumbfile)){
            @unlink($thumbfile);
        }
        $cropimage = explode(",",IMAGECROP);
        if(count($cropimage)>0){
            $image = explode("/", $Url);
            $filename = count($image)>0 ? $image[count($image)-1] : $image[0] ;
            foreach($cropimage as $key){
                $size = explode("x", $key);
                $width = isset($size[0]) ? (int)$size[0] : 0 ;
                $height = isset($size[1]) ? (int)$size[1] : 0 ;
                $crop = $width."x".$height."_".$filename;
                $file = str_replace($filename,$crop,$Url);
                if(file_exists($file)){
                    @unlink($file);
                }
            }
        }
    }

    public function delete_images_variant(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $listID = isset($_POST['listID']) ? $_POST['listID'] : array() ;
        if(count($listID)>0){
            $listID = json_decode($listID,true);
            $listID = implode(',',$listID);
            if($listID!=''){
                $this->write_log("delete from ttp_report_images_products where ProductsID=".$ID." and ID in($listID)");
                $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$ID order by STT ASC")->result();
                if(count($data_image)>0){
                    $data = array();
                    $primary = "";
                    foreach($data_image as $row){
                        $primary = $row->PrimaryImage==1 ? $row->Url : $primary ;
                        $data[] = $row->Url;
                    }
                    $primary = $primary=='' ? $row->Url : $primary ;
                    $data = json_encode($data);
                    $this->db->query("update ttp_report_products set AlbumImage='$data',PrimaryImage='$primary' where ID=$ID");
                }
            }
        }
    }

    public function setprimary_images_variant(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $ImageID = isset($_POST['ImageID']) ? (int)$_POST['ImageID'] : 0;
        $this->db->query("update ttp_report_images_products set PrimaryImage=0,MiniImage=0,ThumbImage=0 where ProductsID=$ID");
        $this->db->query("update ttp_report_images_products set PrimaryImage=1,MiniImage=1,ThumbImage=1 where ID=$ImageID");
        $image = $this->db->query("select Url from ttp_report_images_products where ID=$ImageID")->row();
        if($image ){
            $this->db->query("update ttp_report_products set Url='$image->Url' where ID=$ID");
        }
        $this->write_log("update ttp_report_images_products set PrimaryImage=1,MiniImage=1,ThumbImage=1 where ID=$ImageID");
    }

    public function get_child_categories(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $result = $this->db->query("select * from ttp_report_categories where ParentID=$ID")->result();
        if(count($result)>0){
            echo "<ul>";
            foreach($result as $row){
                echo "<li><span onclick='show_child_next(this)' data='$row->ID'>+</span><input type='checkbox' value='$row->ID' onchange='check_full(this)' name='CategoriesID[]' /> $row->Title</li>";
            }
            echo "</ul>";
        }else{
            echo "False";
        }
    }

    public function add_properties(){
        $this->load->view("warehouse_add_properties");
    }

    public function add_value_properties(){
        $data = isset($_POST['Data']) ? $_POST['Data'] : 0 ;
        if($data!='' && $data>0){
            $this->load->view("warehouse_add_properties",array('data'=>$data));
        }
    }

    public function save_properties(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $ParentID = isset($_POST['ParentID']) ? $_POST['ParentID'] : 0 ;
        if($Title!='' && $ParentID!=''){
            $path = $this->db->query("select Path from ttp_report_properties where ID=$ParentID")->row();
            $path = $path ? $path->Path : '' ;
            $max = $this->db->query("select max(ID) as ID from ttp_report_properties")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $data = array(
                'ID'        =>$max,
                'Title'     =>$Title,
                'ParentID'  =>$ParentID,
                'Published' =>1,
                'Path'      => $ParentID>0 ? $path.'/'.$max : $max
            );
            $data['Level'] = count(explode('/',$data['Path']));
            $this->db->insert("ttp_report_properties",$data);
            $ID = $this->db->insert_id();
            if($ParentID==0){
                echo "OK|$ParentID|<option value='$ID'> $Title</option>";
            }else{
                echo "OK|$ParentID|<div class='col-xs-3'><input type='checkbox' value='$ID' /> $Title</div>";
            }
        }else{
            echo "False|False|False";
        }
    }

    public function add_newrow_properties(){
        $ID = isset($_POST['Data']) ? $_POST['Data'] : '' ;
        if($ID!=''){
            $data = $this->db->query("select * from ttp_report_properties where ID=$ID")->row();
            if($data){
                $this->load->view("admin/warehouse_properties_addnewrow",array('data'=>$data));
                return;
            }
        }
        echo "False";
    }

    public function row_image_update(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : '' ;
        $Label = isset($_POST['Label']) ? $_POST['Label'] : '' ;
        $STT = isset($_POST['STT']) ? $_POST['STT'] : '' ;
        $Primary = isset($_POST['Primary']) ? $_POST['Primary'] : 0 ;
        $Small = isset($_POST['Small']) ? $_POST['Small'] : 0 ;
        $Thumb = isset($_POST['Thumb']) ? $_POST['Thumb'] : 0 ;
        if($ID!=''){
            $data = array(
                'Label'         => $Label,
                'STT'           => $STT,
                'PrimaryImage'  => $Primary,
                'MiniImage'     => $Small,
                'ThumbImage'    => $Thumb,
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_images_products",$data);
            $image = $this->db->query("select ProductsID from ttp_report_images_products where ID=$ID")->row();
            if($image){
                $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$image->ProductsID order by STT ASC")->result();
                if(count($data_image)>0){
                    $data = array();
                    $primary = "";
                    foreach($data_image as $row){
                        $primary = $row->PrimaryImage==1 ? $row->Url : $primary ;
                        $data[] = $row->Url;
                    }
                    $primary = $primary=='' ? $row->Url : $primary ;
                    $data = json_encode($data);
                    $this->db->query("update ttp_report_products set AlbumImage='$data',PrimaryImage='$primary' where ID=$image->ProductsID");
                }
            }
            echo "OK";
        }else{
            echo "False";
        }
    }

    public function load_fillter_by_type_and_field(){
        $field = isset($_POST['FieldName']) ? $_POST['FieldName'] : '' ;
        if($field=='trademark'){
            $result = $this->db->query("select * from ttp_report_trademark order by Title ASC")->result();
            if(count($result)>0){
                echo "<select name='FieldText[]'>";
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->Title</option>";
                }
                echo "</select>";
            }
        }else{
            echo '<input type="text" name="FieldText[]" id="textsearch" />';
        }

    }

    public function setfillter(){
        $arr_fieldname = array(0=>"a.MaSP",1=>"a.Title",2=>"b.ID",3=>"a.Price",4=>"a.CurrentAmount",5=>"a.CategoriesID",6=>"a.CategoriesID");
        $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
        $FieldName = isset($_POST['FieldName']) ? $_POST['FieldName'] : array() ;
        $FieldOparation = isset($_POST['FieldOparation']) ? $_POST['FieldOparation'] : array() ;
        $FieldText = isset($_POST['FieldText']) ? $_POST['FieldText'] : array() ;
        $str = array();
        if(count($FieldName)>0){
            $i=0;
            $arrtemp = array();
            foreach ($FieldName as $key => $value) {
                if(!in_array($value,$arrtemp)){
                    if(isset($arr_fieldname[$value]) && isset($FieldOparation[$i])){
                        if(isset($arr_oparation[$FieldOparation[$i]]) && isset($FieldText[$i])){
                            if($FieldText[$i]!=''){
                                if($value==5){
                                    $str[] = $arr_fieldname[$value]." like '%\"".$FieldText[$i]."\"%'";
                                }else{
                                    if($arr_oparation[$FieldOparation[$i]]=='like'){
                                        $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '%".mysql_real_escape_string($FieldText[$i])."%'";
                                    }else{
                                        $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '".mysql_real_escape_string($FieldText[$i])."'";
                                    }
                                }
                            }
                            $i++;
                        }
                    }
                    $arrtemp[] = $value;
                }
            }
            if(count($str)>0){
                $sql = implode(' and ',$str);
                $this->session->set_userdata("fillter",$sql);
            }else{
                $this->session->set_userdata("fillter","");    
            }
        }else{
            $this->session->set_userdata("fillter","");
        }
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
        redirect($referer);
    }

    public function set_barcode($code)
    {
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
    }

    public function export_products(){
        if($this->user->IsAdmin==1 || $this->user->UserType==6){
            $TypeExport = isset($_POST['TypeExport']) ? $_POST['TypeExport'] : 0 ;
            $day_export = isset($_POST['Export_date']) ? $_POST['Export_date'] : date('Y-m-d',time());
            $daystop_export = isset($_POST['ExportStop_date']) ? $_POST['ExportStop_date'] : date('Y-m-d',time());
            if($day_export!=''){
                if($TypeExport==1){
                    $this->export_barcode($day_export,$daystop_export);
                }
            }
        }
    }

    public function export_barcode($day_export,$daystop_export){
        $result = $this->db->query("select Barcode,Title from ttp_report_products where date(Created)>='$day_export' and date(Created)<='$daystop_export'")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'ean13')
                        ->setCellValue('B1', 'name')
                        ->setCellValue('C1', 'Name ko dau')
                        ->setCellValue('D1', 'qty_available');
            $i=2;
            foreach($result as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->Barcode)
                            ->setCellValue('B'.$i, $row->Title)
                            ->setCellValue('C'.$i, $this->lib->asciiCharacter($row->Title))
                            ->setCellValue('D'.$i, 1);
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BC_BARCODE');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BC_BARCODE.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function get_products_list(){
        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '' ;
        $result = $this->db->query("select ID,Title,Price,MaSP from ttp_report_products where (VariantType=0) or (VariantType=1 and ParentID>0) order by ID DESC limit 0,100")->result();
        if(count($result)>0){
            $str = "<div class='tools_search_products'>
                        <span>Tìm kiếm sản phẩm: </span>
                        <span><input type='text' placeholder='Nhập mã SKU hoặc tên sản phẩm' onchange='input_search_products(this)' /></span>
                    </div>
                    <table><tr><th>STT</th><th>Mã Sản phẩm</th><th>Sản phẩm</th></tr>";
            $i=1;
            foreach($result as $row){
                $str.="<tr>";
                $str.="<td>$i</td>";
                $str.="<td style='width: 130px;'><a onclick='addproductstobundle(this,$row->ID)'>".$row->MaSP."</a></td>";
                $str.="<td><a onclick='addproductstobundle(this,$row->ID)'>".$row->Title."</a></td>";
                $str.="</tr>";
                $i++;
            }
            echo $str."</table>";
        }else{
            echo "FALSE";
        }
    }

    public function addproductstobundle(){
        $BundleID = isset($_POST['BundleID']) ? (int)$_POST['BundleID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        $checkorder  = $this->db->query("select count(ID) as SL from ttp_report_orderdetails where ProductsID=$BundleID")->row();
        if($checkorder->SL==0){
            $check = $this->db->query("select * from ttp_report_products_bundle where Des_ProductsID=$ProductsID and Src_ProductsID=$BundleID")->row();
            if(!$check){
                $data = array(
                    'Des_ProductsID'    => $ProductsID,
                    'Src_ProductsID'    => $BundleID,
                    'Quantity'          => 1
                );
                $this->db->insert('ttp_report_products_bundle',$data);
                $this->write_log("insert ttp_report_products_bundle \n   'Des_ProductsID'    => $ProductsID \n   'Src_ProductsID'    => $BundleID \n   'Quantity'          => 1");
                echo "OK";
            }else{
                echo "FALSE";
            }
        }else{
            echo "FALSE1";
        }
    }

    public function removeoutbundle(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $object  = $this->db->query("select * from ttp_report_products_bundle where ID=$ID")->row();
        if($object){
            $checkorder  = $this->db->query("select count(ID) as SL from ttp_report_orderdetails where ProductsID=$object->Src_ProductsID")->row();
            if($checkorder->SL==0){
                $this->db->query("delete from ttp_report_products_bundle where ID=$ID");
                $this->write_log("delete bundle products where ID=$ID (ttp_report_products_bundle)");
                echo "OK";
            }else{
                echo "FALSE1";
            }
        }else{
            echo "FALSE";
        }
    }

    public function updatequantitybundle(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $Quantity = isset($_POST['Quantity']) ? (int)$_POST['Quantity'] : 1 ;
        $object  = $this->db->query("select * from ttp_report_products_bundle where ID=$ID")->row();
        if($object){
            $checkorder  = $this->db->query("select count(ID) as SL from ttp_report_orderdetails where ProductsID=$object->Src_ProductsID")->row();
            if($checkorder->SL==0){
                $this->db->query("update ttp_report_products_bundle set Quantity=$Quantity where ID=$ID");
                $this->write_log("update bundle products set Quantity=$Quantity where ID=$ID (ttp_report_products_bundle)");
                echo "OK";
            }else{
                echo "FALSE1";
            }
        }else{
            echo "FALSE";
        }
    }

    public function get_products_bundle_list($ID=0){
        $ID = (int)$ID;
        $result = $this->db->query("select b.*,a.Quantity,a.ID as BundleID from ttp_report_products_bundle a,ttp_report_products b where Src_ProductsID=$ID and a.Des_ProductsID=b.ID")->result();
        if(count($result)>0){
            foreach($result as $row){
                echo '<div class="col-xs-4">
                        <div class="form-group panel panel-default" style="overflow:hidden">
                            <div class="col-xs-3"><img class="img-responsive" src="'.$row->PrimaryImage.'" /></div>
                            <div class="col-xs-9">
                                <p>'.$row->Title.'</p>
                                <p class="text-danger">'.number_format($row->Price).'đ</p>
                            </div>
                            <div class="col-xs-3" style="clear:both"><input class="form-control" type="number" value="'.$row->Quantity.'" onchange="updatequantitybundle(this,'.$row->BundleID.')" /></div>
                            <div class="col-xs-9"><a class="pull-right btn btn-default" title="Loại bỏ sản phẩm này ra khỏi bộ bundle" onclick="removeoutbundle(this,'.$row->BundleID.')"><i class="fa fa-times"></i></a></div>
                        </div>
                    </div>';
            }
        }
    }
}
?>
