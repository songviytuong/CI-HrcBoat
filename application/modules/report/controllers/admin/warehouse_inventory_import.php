<?php 
class Warehouse_inventory_import extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="warehouse_inventory_import";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        if(isset($_GET['warehouse'])){
            if(is_numeric($_GET['warehouse'])){
                $fillter = $fillter!='' ? $fillter.' and a.KhoID='.$_GET['warehouse'] : ' and a.KhoID='.$_GET['warehouse'] ;
            }
        }
        $TypeImport = $this->session->userdata('TypeImport');
        $TypeImport = $TypeImport=='' ? 0 : $TypeImport ;
        if($TypeImport!='' && is_numeric($TypeImport)){
            $fillter = $fillter!='' ? $fillter.' and a.Type='.$TypeImport : ' and a.Type='.$TypeImport;
        }
        if($this->user->UserType==2 || $this->user->UserType==8){
            $fillter = $fillter!='' ? $fillter." and e.Manager like '%\"".$this->user->ID."\"%'" : " and e.Manager like '%\"".$this->user->ID."\"%'" ;
        }
        if($this->user->IsAdmin==0){
            if($this->user->Channel==2){
                $fillter = $fillter!='' ? $fillter." and c.Channel=2" : $fillter ;
            }else{
                $fillter = $fillter!='' ? $fillter." and c.Channel in(0,1)" : $fillter ;
            }
        }
        if($TypeImport=='' || $TypeImport==0 || $TypeImport==2){
            $nav = $this->db->query("select count(1) as nav from ttp_report_inventory_import a,ttp_report_production b,ttp_user c,ttp_report_perchaseorder d,ttp_report_warehouse e where a.KhoID=e.ID and a.POID=d.ID and a.ProductionID=b.ID and a.UserID=c.ID $fillter")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.*,c.UserName,b.Title,d.POCode from ttp_report_inventory_import a,ttp_report_production b,ttp_user c,ttp_report_perchaseorder d,ttp_report_warehouse e where a.KhoID=e.ID and a.POID=d.ID and a.ProductionID=b.ID and a.UserID=c.ID $fillter order by a.ID DESC $limit_str")->result();
        }else{
            $nav = $this->db->query("select count(1) as nav from ttp_report_inventory_import a,ttp_user c,ttp_report_warehouse e where a.KhoID=e.ID and a.UserID=c.ID $fillter")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.*,c.UserName from ttp_report_inventory_import a,ttp_user c,ttp_report_warehouse e where a.KhoID=e.ID and a.UserID=c.ID $fillter order by a.ID DESC $limit_str")->result();
        }
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_import/',
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'type'      => $TypeImport,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_inventory_import/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Production | Warehouse Report Tools');
        $view = 'warehouse_inventory_import_home';
        $view = $this->user->UserType == 6 ? 'warehouse_inventory_import_home_cm' : $view ;
        $this->template->write_view('content','admin/'.$view,$data);
		$this->template->render();
	}

    public function pnk(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        if(isset($_GET['warehouse'])){
            if(is_numeric($_GET['warehouse'])){
                $fillter = $fillter!='' ? $fillter.' and a.KhoID='.$_GET['warehouse'] : ' and a.KhoID='.$_GET['warehouse'] ;
            }
        }
        $TypeImport = $this->session->userdata('TypeImport');
        $TypeImport = $TypeImport=='' ? 0 : $TypeImport ;
        if($TypeImport!='' && is_numeric($TypeImport)){
            $fillter = $fillter!='' ? $fillter.' and a.Type='.$TypeImport : ' and a.Type='.$TypeImport;
        }
        if($this->user->UserType==2 || $this->user->UserType==8){
            $fillter = $fillter!='' ? $fillter." and e.Manager like '%\"".$this->user->ID."\"%'" : " and e.Manager like '%\"".$this->user->ID."\"%'" ;
        }
        if($this->user->IsAdmin==0){
            if($this->user->Channel==2){
                $fillter = $fillter!='' ? $fillter." and c.Channel=2" : $fillter ;
            }else{
                $fillter = $fillter!='' ? $fillter." and c.Channel in(0,1)" : $fillter ;
            }
        }
        if($TypeImport=='' || $TypeImport==0 || $TypeImport==2){
            $nav = $this->db->query("select count(1) as nav from ttp_report_inventory_import a,ttp_report_production b,ttp_user c,ttp_report_perchaseorder d,ttp_report_warehouse e,ttp_report_import_warehouse f where a.ID=f.ImportID and a.KhoID=e.ID and a.POID=d.ID and a.ProductionID=b.ID and a.UserID=c.ID $fillter")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.*,f.MaNK,f.Ngaynhapkho,c.UserName,b.Title,d.POCode from ttp_report_inventory_import a,ttp_report_production b,ttp_user c,ttp_report_perchaseorder d,ttp_report_warehouse e,ttp_report_import_warehouse f where a.ID=f.ImportID and a.KhoID=e.ID and a.POID=d.ID and a.ProductionID=b.ID and a.UserID=c.ID $fillter order by a.ID DESC $limit_str")->result();
        }else{
            $nav = $this->db->query("select count(1) as nav from ttp_report_inventory_import a,ttp_user c,ttp_report_warehouse e,ttp_report_import_warehouse f where a.ID=f.ImportID and a.KhoID=e.ID and a.UserID=c.ID $fillter")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.*,f.MaNK,f.Ngaynhapkho,c.UserName from ttp_report_inventory_import a,ttp_user c,ttp_report_warehouse e,ttp_report_import_warehouse f where a.ID=f.ImportID and a.KhoID=e.ID and a.UserID=c.ID $fillter order by a.ID DESC $limit_str")->result();
        }
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_import/',
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'type'      => $TypeImport,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_inventory_import/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Production | Warehouse Report Tools');
        $view = 'warehouse_inventory_import_pnk';
        $this->template->write_view('content','admin/'.$view,$data);
        $this->template->render();
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if($this->user->UserType==2 || $this->user->UserType==8){
            $result = $this->db->query("select a.* from ttp_report_inventory_import a,ttp_report_warehouse b where a.KhoID=b.ID and b.Manager like '%\"".$this->user->ID."\"%' and a.ID=$id")->row();
        }else{
            $result = $this->db->query("select * from ttp_report_inventory_import where ID=$id")->row();
        }
        if($result){
            switch ($result->Type) {
                case 0:
                    $view = 'warehouse_inventory_import_production_edit';
                    break;
                case 1:
                    $view = 'warehouse_inventory_import_rejectorder_edit';
                    break;
                case 2:
                    $view = 'warehouse_inventory_import_internal_edit';
                    break;
                default:
                    $view = 'warehouse_inventory_import_production_edit';
                    break;
            }
            if($this->user->UserType==6){
                $view = $view."_cm";
            }
            $this->template->add_title('Warehouse Edit | Warehouse Report Tools');
            $this->template->write_view('content','admin/'.$view,array('data'=>$result,'base_link'=>base_url().ADMINPATH.'/report/warehouse_inventory_import/'));
            $this->template->render();
        }
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $KhoID = isset($_POST['KhoID']) ? $_POST['KhoID'] : '' ;
        $NgayNK = isset($_POST['NgayNK']) ? $_POST['NgayNK'] : date('Y-m-d') ;
        $Type = isset($_POST['Type']) ? $_POST['Type'] : '' ;
        $ProductionID = isset($_POST['ProductionID']) ? $_POST['ProductionID'] : 0 ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $Status = isset($_POST['Status']) ? $_POST['Status'] : '' ;
        $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
        $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
        $ExportID = isset($_POST['ExportID']) ? $_POST['ExportID'] : 0 ;
        $POID = isset($_POST['POID']) ? $_POST['POID'] : 0 ;
        if(in_array(0,$ShipmentID)){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            echo "<script>alert('Vui lòng chọn lô hàng cho sản phẩm . Tất cả các sản phẩm buộc phải có lô !');window.location='".$_SERVER['HTTP_REFERER']."'</script>";
            return;
        }
        if($NgayNK!='' && $Type!=''){
            $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
            $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
            $Currency = isset($_POST['Currency']) ? $_POST['Currency'] : array() ;
            $ValueCurrency = isset($_POST['ValueCurrency']) ? $_POST['ValueCurrency'] : array() ;
            $PriceCurrency = isset($_POST['PriceCurrency']) ? $_POST['PriceCurrency'] : array() ;
            $TotalCurrency = isset($_POST['TotalCurrency']) ? $_POST['TotalCurrency'] : array() ;
            $TotalVND = isset($_POST['TotalVND']) ? $_POST['TotalVND'] : array() ;
            $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
            if(count($ProductsID)>0){
                $arr = array();
                $arrnext = array();
                $total = 0;
                $slsp = 0;
                foreach($ProductsID as $key=>$row){
                    $ShipmentID_row = isset($ShipmentID[$key]) ? (int)$ShipmentID[$key] : 0 ;
                    $Currency_row = isset($Currency[$key]) ? $Currency[$key] : 0 ;
                    $Amount_row = isset($Amount[$key]) ? $Amount[$key] : 0 ;
                    $ValueCurrency_row = isset($ValueCurrency[$key]) ? $ValueCurrency[$key] : 0 ;
                    $PriceCurrency_row = isset($PriceCurrency[$key]) ? $PriceCurrency[$key] : 0 ;
                    $TotalCurrency_row = $Amount_row*$PriceCurrency_row;
                    $TotalVND_row = ($Amount_row*$PriceCurrency_row)*$ValueCurrency_row;
                    if($TotalCurrency_row>=0 && $TotalVND_row>=0){
                        $slsp = $slsp+$Amount_row;
                        $total +=$TotalVND_row;
                        $arr[] = "(ImportIDDV,$row,$ShipmentID_row,'$Currency_row',$ValueCurrency_row,$PriceCurrency_row,$Amount_row,$TotalCurrency_row,$TotalVND_row)";
                    }
                }
                if(count($arr)>0){
                    $thismonth = date('m',time());
                    $thisyear = date('Y',time());
                    $max = $this->db->query("select count(1) as max from ttp_report_inventory_import where MONTH(Created)=$thismonth and YEAR(Created)=$thisyear and Type=$Type")->row();
                    $max = $max ? $max->max + 1 : 1 ;
                    $thisyear = date('y',time());
                    $max = "YCNK".$Type.'_'.$KhoID.'_'.$thisyear.$thismonth.'_'.str_pad($max, 7, '0', STR_PAD_LEFT);

                    $data = array(
                        'MaNK'      => $max,
                        'POID'      => $POID,
                        'ExportID'  => $ExportID,
                        'KhoID'     => $KhoID,
                        'UserID'    => $this->user->ID,
                        'NgayNK'    => $NgayNK,
                        'Type'      => $Type,
                        'ProductionID'=> $ProductionID,
                        'Note'      => $Note,
                        'Status'    => $Status,
                        'Ghichu'    => $Ghichu,
                        'TotalAmount'=> $slsp,
                        'TotalPrice'=> $total,
                        'Created'   => date('Y-m-d H:i:s'),
                        'LastEdited'=> date('Y-m-d H:i:s')
                    );
                    $this->db->insert('ttp_report_inventory_import',$data);
                    $ID = $this->db->insert_id();
                    if($Status==0){
                        $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Request,TotalCurrency,TotalVND) values".implode(',',$arr);
                    }else{
                        $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Amount,TotalCurrency,TotalVND) values".implode(',',$arr);
                    }
                    $arr = str_replace('ImportIDDV',$ID,$arr);
                    $this->db->query($arr);
                    $data_his = array(
                        'ImportID'  => $ID,
                        'Status'    => $Status,
                        'Note'      => $Ghichu,
                        'UserID'    => $this->user->ID,
                        'Created'   => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('ttp_report_inventory_import_history',$data_his);
                }
            }
        }
        redirect(ADMINPATH.'/report/warehouse_inventory_import');
    } 

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $IsChangeOrder = isset($_POST["IsChangeOrder"]) ? $_POST["IsChangeOrder"] : 0;
        $Type = isset($_POST['Type']) ? $_POST['Type'] : '' ;
        $KhoID = isset($_POST['KhoID']) ? $_POST['KhoID'] : '' ;
        $ProductionID = isset($_POST['ProductionID']) ? $_POST['ProductionID'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $Status = isset($_POST['Status']) ? $_POST['Status'] : '' ;
        $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
        $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
        $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
        $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
        $Currency = isset($_POST['Currency']) ? $_POST['Currency'] : array() ;
        $ValueCurrency = isset($_POST['ValueCurrency']) ? $_POST['ValueCurrency'] : array() ;
        $PriceCurrency = isset($_POST['PriceCurrency']) ? $_POST['PriceCurrency'] : array() ;
        $TotalCurrency = isset($_POST['TotalCurrency']) ? $_POST['TotalCurrency'] : array() ;
        $TotalVND = isset($_POST['TotalVND']) ? $_POST['TotalVND'] : array() ;
        $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
        $ExportID = isset($_POST['ExportID']) ? $_POST['ExportID'] : 0 ;
        $POID = isset($_POST['POID']) ? $_POST['POID'] : 0 ;
        $result = $this->db->query("select ID,Status,KhoID,ExportID,NgayNK as DateInventory from ttp_report_inventory_import where ID=$ID")->row();
        if($result){
            if($result->Status==4 || $result->Status==5){
                redirect(ADMINPATH.'/report/warehouse_inventory_import');
            }
            if($this->user->UserType==6 && ($result->Status!=1 && $result->Status!=4)){
                if(count($ProductsID)>0){
                    $arr = array();
                    $total = 0;
                    $slsp = 0;
                    foreach($ProductsID as $key=>$row){
                        $ShipmentID_row = isset($ShipmentID[$key]) ? (int)$ShipmentID[$key] : 0 ;
                        $Currency_row = isset($Currency[$key]) ? $Currency[$key] : 0 ;
                        $Amount_row = isset($Amount[$key]) ? $Amount[$key] : 0 ;
                        $ValueCurrency_row = isset($ValueCurrency[$key]) ? $ValueCurrency[$key] : 0 ;
                        $PriceCurrency_row = isset($PriceCurrency[$key]) ? $PriceCurrency[$key] : 0 ;
                        $TotalCurrency_row = $Amount_row*$PriceCurrency_row;
                        $TotalVND_row = ($Amount_row*$PriceCurrency_row)*$ValueCurrency_row;
                        if($TotalCurrency_row>=0 && $TotalVND_row>=0){
                            $slsp = $slsp+$Amount_row;
                            $total +=$TotalVND_row;
                            $arr[] = "(ImportIDDV,$row,$ShipmentID_row,'$Currency_row',$ValueCurrency_row,$PriceCurrency_row,$Amount_row,$TotalCurrency_row,$TotalVND_row)";
                        }
                    }

                    if(count($arr)>0){
                        $data = array(
                            'KhoID'     => $KhoID,
                            'UserID'    => $this->user->ID,
                            'ExportID'  => $ExportID,
                            'ProductionID'=> $ProductionID,
                            'Note'      => $Note,
                            'Status'    => $Status,
                            'Ghichu'    => $Ghichu,
                            'TotalAmount'=> $slsp,
                            'TotalPrice'=> $total,
                            'LastEdited'=> date('Y-m-d H:i:s')
                        );
                        $this->db->where("ID",$ID);
                        $this->db->update('ttp_report_inventory_import',$data);
                        if($IsChangeOrder==1){
                            $this->db->query("delete from ttp_report_inventory_import_details where ImportID=$ID");
                            $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Amount,TotalCurrency,TotalVND) values".implode(',',$arr);
                            $arr = str_replace('ImportIDDV',$ID,$arr);
                            $this->db->query($arr);
                        }
                        if($result->Status!=$Status){
                            $data_his = array(
                                'ImportID'  => $ID,
                                'Status'    => $Status,
                                'Note'      => $Ghichu,
                                'UserID'    => $this->user->ID,
                                'Created'   => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('ttp_report_inventory_import_history',$data_his);
                        }
                    }
                }
            }else{
                $arrnext = array();
                if($Status==2 && $result->Status==0){
                    $details = $this->db->query("select * from ttp_report_inventory_import_details where ImportID=$ID")->result();
                    if(count($details)>0){
                        $arr_history = array();
                        $details_export = $this->db->query("select c.* from ttp_report_order a,ttp_report_export_warehouse b,ttp_report_orderdetails c where a.ID=b.OrderID and a.ID=c.OrderID and b.ID=$result->ExportID")->result();
                        if(count($details_export)>0){
                            foreach($details_export as $row){
                                $arr_history[$row->ProductsID.'_'.$row->ShipmentID] = $row->ID;
                            }
                        }
                        foreach($details as $row){
                            $TotalCurrency_row = $row->TotalCurrency;
                            $TotalVND_row = $row->TotalVND;
                            if($TotalCurrency_row>=0 && $TotalVND_row>=0){
                                $IDDetails = isset($arr_history[$row->ProductsID.'_'.$row->ShipmentID]) ? $arr_history[$row->ProductsID.'_'.$row->ShipmentID] : 0;
                                $bundle = $this->db->query("select * from ttp_report_orderdetails_bundle where DetailsID=$IDDetails")->result();
                                if(count($bundle)>0){
                                    foreach($bundle as $item){
                                        $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1 order by DateInventory DESC")->row();
                                        if($check){
                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1");
                                            if($check->DateInventory==date('Y-m-d')){
                                                $this->db->query("update ttp_report_inventory set Available=Available+$item->Amount,OnHand=OnHand+$item->Amount,LastEdited=1 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$result->KhoID");
                                            }else{
                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$check->OnHand,$check->Available+$item->Amount,$result->KhoID,$item->ShipmentID,'".date('Y-m-d')."',1)");
                                            }
                                        }else{
                                            $Available = $item->Amount ;
                                            $OnHand = $item->Amount ;
                                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$OnHand,$Available,$result->KhoID,$item->ShipmentID,'".date('Y-m-d')."',1)");
                                        }
                                    }
                                }else{
                                    $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$result->KhoID and LastEdited=1 order by DateInventory DESC")->row();
                                    if($check_exists){
                                        $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$result->KhoID and LastEdited=1");
                                        if($check_exists->DateInventory==date('Y-m-d')){
                                            $this->db->query("update ttp_report_inventory set Available=Available+$row->Amount,OnHand=OnHand+$row->Amount,LastEdited=1 where ID=$check_exists->ID");
                                        }else{
                                            $Available = $check_exists ? $check_exists->Available+$row->Amount : $row->Amount ;
                                            $OnHand = $check_exists ? $check_exists->OnHand+$row->Amount : $row->Amount ;
                                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$result->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                                        }
                                        
                                    }else{
                                        $Available = $row->Amount ;
                                        $OnHand = $row->Amount ;
                                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$result->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                                    }
                                }
                            }
                        }
                    }
                }
                if($Type==1 && $Status==5){
                    $export = $this->db->query("select OrderID from ttp_report_export_warehouse where ID=$ExportID")->row();
                    if($export){
                        $this->db->query("update ttp_report_order set Status=0 where ID=$export->OrderID");
                        $datahis = array(
                            'OrderID'   =>$export->OrderID,
                            'Thoigian'  =>date('Y-m-d H:i:s'),
                            'Status'    =>0,
                            "Ghichu"    =>'Hủy phiếu chờ nhập kho => '.$Ghichu,
                            "UserID"    =>$this->user->ID
                        );
                        $this->db->insert('ttp_report_orderhistory',$datahis);
                    }
                }
                $data = array(
                    'Status'    => $Status,
                    'Ghichu'    => $Ghichu,
                    'LastEdited'=> date('Y-m-d H:i:s')
                );
                $this->db->where("ID",$ID);
                $this->db->update('ttp_report_inventory_import',$data);
                $data_his = array(
                    'ImportID'  => $ID,
                    'Status'    => $Status,
                    'Note'      => $Ghichu,
                    'UserID'    => $this->user->ID,
                    'Created'   => date('Y-m-d H:i:s')
                );
                $this->db->insert('ttp_report_inventory_import_history',$data_his);
            }
        }
        redirect(ADMINPATH.'/report/warehouse_inventory_import');
    }

    public function update_from_cm(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $Type = isset($_POST['Type']) ? $_POST['Type'] : '' ;
        $KhoID = isset($_POST['KhoID']) ? $_POST['KhoID'] : '' ;
        $ProductionID = isset($_POST['ProductionID']) ? $_POST['ProductionID'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $Status = isset($_POST['Status']) ? $_POST['Status'] : '' ;
        $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
        $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
        $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
        $POID = isset($_POST['POID']) ? $_POST['POID'] : 0 ;
        $result = $this->db->query("select ID,Status,KhoID,NgayNK as DateInventory from ttp_report_inventory_import where ID=$ID")->row();
        if($result){
            if($this->user->UserType==6 && $result->Status==1){
                if(count($ProductsID)>0){
                    $arr = array();
                    $slsp = 0;
                    foreach($ProductsID as $key=>$row){
                        $Amount_row = isset($Amount[$key]) ? $Amount[$key] : 0 ;
                        $slsp = $slsp+$Amount_row;
                        $arr[] = "(ImportIDDV,$row,0,'',0,0,$Amount_row,0,0)";
                    }

                    if(count($arr)>0){
                        $data = array(
                            'KhoID'     => $KhoID,
                            'POID'      => $POID,
                            'UserID'    => $this->user->ID,
                            'ProductionID'=> $ProductionID,
                            'Note'      => $Note,
                            'Status'    => $Status,
                            'Ghichu'    => $Ghichu,
                            'TotalAmount'=> $slsp,
                            'LastEdited'=> date('Y-m-d H:i:s')
                        );
                        $this->db->where("ID",$ID);
                        $this->db->update('ttp_report_inventory_import',$data);
                        $this->db->query("delete from ttp_report_inventory_import_details where ImportID=$ID");
                        $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Request,TotalCurrency,TotalVND) values".implode(',',$arr);
                        $arr = str_replace('ImportIDDV',$ID,$arr);
                        $this->db->query($arr);
                        if($result->Status!=$Status){
                            $data_his = array(
                                'ImportID'  => $ID,
                                'Status'    => $Status,
                                'Note'      => $Ghichu,
                                'UserID'    => $this->user->ID,
                                'Created'   => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('ttp_report_inventory_import_history',$data_his);
                        }
                    }
                }
            }
        }
        redirect(ADMINPATH.'/report/warehouse_inventory_import');
    }

    public function update_from_warehouseuser(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if($this->user->UserType==2 || $this->user->UserType==8 || $this->user->IsAdmin==1){
            $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
            $KhoID = isset($_POST['KhoID']) ? $_POST['KhoID'] : '' ;
            $Status = isset($_POST['Status']) ? $_POST['Status'] : '' ;
            $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
            $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
            $ShipmentOld = isset($_POST['ShipmentOld']) ? $_POST['ShipmentOld'] : array() ;
            $Request = isset($_POST['Request']) ? $_POST['Request'] : array() ;
            if(in_array(0,$ShipmentID)){
                $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH."/report/warehouse_inventory_import" ;
                echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                echo "<script>alert('Vui lòng chọn lô hàng cho từng sản phẩm !');</script>";
                echo "<script>window.location='$refer';</script>";
                return;
            }
            $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
            $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
            if($this->user->UserType==2 || $this->user->UserType==8){
                $result = $this->db->query("select a.* from ttp_report_inventory_import a,ttp_report_warehouse b where a.KhoID=b.ID and b.Manager like '%\"".$this->user->ID."\"%' and a.ID=$ID")->row();
            }else{
                $result = $this->db->query("select * from ttp_report_inventory_import where ID=$ID")->row();
            }
            if($result){
                if($Status==1){
                    $data = array(
                        'Status'        => $Status,
                        'LastEdited'    => date('Y-m-d H:i:s')
                    );
                    $this->db->where("ID",$ID);
                    $this->db->update('ttp_report_inventory_import',$data);
                    if($result->Status!=$Status){
                        $data_his = array(
                            'ImportID'  => $ID,
                            'Status'    => $Status,
                            'Note'      => $Ghichu,
                            'UserID'    => $this->user->ID,
                            'Created'   => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('ttp_report_inventory_import_history',$data_his);
                    }
                    redirect(ADMINPATH."/report/warehouse_inventory_import");
                }
                if(count($ProductsID)>0){
                    $total = 0;
                    $slsp = 0;
                    $details = $this->db->query("select * from ttp_report_inventory_import_details where ImportID=$ID")->result();
                    $productsstt = 1;
                    $sp_bonus = 0;
                    $arr_sql = array();
                    foreach($ProductsID as $key=>$row){
                        $ShipmentID_row = isset($ShipmentID[$key]) ? (int)$ShipmentID[$key] : 0 ;
                        $Amount_row = isset($Amount[$key]) ? $Amount[$key] : 0 ;
                        $Request_row = isset($Request[$key]) ? $Request[$key] : 0 ;
                        $ShipmentOld_row = isset($ShipmentOld[$key]) ? (int)$ShipmentOld[$key] : 0 ;
                        if($Amount_row>=0){
                            $slsp = $slsp+$Amount_row;
                            if($productsstt>count($details)){
                                $sp_bonus= $sp_bonus+$Amount_row;
                                $this->db->query("insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,Amount) value($ID,$row,$ShipmentID_row,'VND',1,$Amount_row)");
                            }
                            $arr_sql[] = "($ID,$row,$ShipmentID_row,'VND',1,$Amount_row,$Request_row)";
                        }
                        $productsstt++;
                    }
                    if(count($arr_sql>0)){
                        $arr_sql = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,Amount,Request) values".implode(',',$arr_sql);
                        $this->db->query("delete from ttp_report_inventory_import_details where ImportID=$ID");
                        $this->db->query($arr_sql);
                    }
                    $details = $this->db->query("select * from ttp_report_inventory_import_details where ImportID=$ID")->result();
                    if(count($details)>0){
                        foreach($details as $row){
                            $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$result->KhoID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$result->KhoID and LastEdited=1");
                            if($check_exists){
                                if($check_exists->DateInventory==date('Y-m-d')){
                                    $this->db->query("update ttp_report_inventory set Available=Available+$row->Amount,OnHand=OnHand+$row->Amount,LastEdited=1 where ID=$check_exists->ID");
                                }else{
                                    $Available = $check_exists->Available+$row->Amount;
                                    $OnHand = $check_exists->OnHand+$row->Amount;
                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$result->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                                }
                            }else{
                                $Available = $row->Amount;
                                $OnHand = $row->Amount;
                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$result->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                            }
                        }
                    }
                    $data = array(
                        'Status'        => $Status,
                        'TotalRequest'  => $result->TotalAmount,
                        'TotalAmount'   => $slsp,
                        'WaitingOnRack'  =>$slsp,
                        'LastEdited'    => date('Y-m-d H:i:s')
                    );
                    $this->db->where("ID",$ID);
                    $this->db->update('ttp_report_inventory_import',$data);
                    if($result->Status!=$Status){
                        $data_his = array(
                            'ImportID'  => $ID,
                            'Status'    => $Status,
                            'Note'      => $Ghichu,
                            'UserID'    => $this->user->ID,
                            'Created'   => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('ttp_report_inventory_import_history',$data_his);
                    }
                    if($result->POID!=0 && ($result->Type==0 || $result->Type==2)){
                        if($sp_bonus>0){
                            $this->db->query("update ttp_report_perchaseorder set Bonus=$sp_bonus where ID=$result->POID");
                        }
                        $check = $this->db->query("select a.ID,a.POCode,a.TotalAmount, sum(b.TotalAmount) as RemainTotal from ttp_report_perchaseorder a,ttp_report_inventory_import b where a.ID=b.POID and a.ID=$result->POID group by a.ID")->row();
                        if($check){
                            if($check->TotalAmount>=$check->RemainTotal){
                                $this->db->query("update ttp_report_perchaseorder set Status=5 where ID=$result->POID");
                            }
                        }
                    }
                    $this->create_import_warehouse($ID);
                }
            }
        }
        redirect(ADMINPATH."/report/warehouse_inventory_import");
    }

    public function update_from_kt(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if($this->user->UserType==3){
            $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
            $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
            $result = $this->db->query("select * from ttp_report_inventory_import where ID=$ID")->row();
            if($result){
                $data = array(
                    'Status'        => 4,
                    'LastEdited'    => date('Y-m-d H:i:s')
                );
                $this->db->where("ID",$ID);
                $this->db->update('ttp_report_inventory_import',$data);
                if($result->Status!=$Status){
                    $data_his = array(
                        'ImportID'  => $ID,
                        'Status'    => 4,
                        'Note'      => $Ghichu,
                        'UserID'    => $this->user->ID,
                        'Created'   => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('ttp_report_inventory_import_history',$data_his);
                }
            }
        }
        redirect(ADMINPATH."/report/warehouse_inventory_import");
    }

    public function print_import_bill($id){
        if($this->user->IsAdmin==1 || $this->user->UserType==3 || $this->user->UserType==2){
            if($id>0){
                $result = $this->db->query("select a.*,b.FirstName,b.LastName,d.MaKho as KhoTitle,d.Address from ttp_report_inventory_import a,ttp_user b,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.UserID=b.ID")->row();
                if($result){
                    $this->template->add_title('Created Bill | Inventory Tools');
                    $data = array(
                        'base_link' => base_url().ADMINPATH.'/report/warehouse_inventory_import/',
                        'data'      => $result
                    );
                    $view = 'admin/warehouse_inventory_import_create_bill' ;
                    $this->template->write_view('content',$view,$data);
                    $this->template->render();
                    return;
                }
            }
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_import' ;
            redirect($referer);
        }else{
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_import' ;
            redirect($referer);
        }
    }

    public function print_import_warehouse(){
        if($this->user->IsAdmin==1 || $this->user->UserType==3 || $this->user->UserType==2){
            $orderid = isset($_POST['ImportID']) ? $_POST['ImportID'] : 0 ;
            if($orderid>0){
                $result = $this->db->query("select * from ttp_report_inventory_import where ID=$orderid")->row();
                if($result){
                    $TKNO = isset($_POST['TKNO']) ? $_POST['TKNO'] : '' ;
                    $TKCO = isset($_POST['TKCO']) ? $_POST['TKCO'] : '' ;
                    $KPP = isset($_POST['KPP']) ? $_POST['KPP'] : '' ;
                    $Lydo = isset($_POST['Lydo']) ? $_POST['Lydo'] : '' ;
                    $KhoID = isset($_POST['KhoID']) ? $_POST['KhoID'] : '' ;
                    $hinhthucnhapkho = isset($_POST['hinhthucnhapkho']) ? $_POST['hinhthucnhapkho'] : '' ;
                    $data = array(
                        'Lydonhapkho'=>$Lydo,
                        'TKNO'       =>$TKNO,
                        'TKCO'       =>$TKCO,
                        'KPP'        =>$KPP,
                        'KhoID'      =>$KhoID,
                        'Hinhthucnhapkho'=>$hinhthucnhapkho
                    );
                    $order = $this->db->query("select * from ttp_report_import_warehouse where ImportID=$orderid")->row();
                    if($order){
                        $this->db->where("ID",$order->ID);
                        $this->db->update("ttp_report_import_warehouse",$data);
                        echo "OK";
                        return;
                    }else{
                        $result = $this->db->query("select * from ttp_report_inventory_import where ID=$orderid")->row();
                        if($result){
                            $thismonth = date('m',time());
                            $thisyear = date('Y',time());
                            $max = $this->db->query("select count(1) as max from ttp_report_import_warehouse where MONTH(Ngaynhapkho)=$thismonth and YEAR(Ngaynhapkho)=$thisyear and DATE(Ngaynhapkho)>='2016-04-21' and DepartmentID=$result->DepartmentID")->row();
                            $max = $max ? $max->max + 1 : 1 ;
                            $thisyear = date('Y',time());
                            $Department = $this->db->query("select Code from ttp_department where ID=$result->DepartmentID")->row();
                            $hinhthucnhapkho = $Department ? $Department->Code : 'LO' ;
                            $max = "NK".$hinhthucnhapkho.$thisyear.$thismonth.'.'.str_pad($max, 5, '0', STR_PAD_LEFT);
                            $data['ImportID']    = $orderid;
                            $data['MaNK']       = $max;
                            $data['Ngaynhapkho']= date("Y-m-d H:i:s",time());
                            $data['UserID']     = $this->user->ID;
                            $this->db->insert("ttp_report_import_warehouse",$data);
                            echo "OK";
                            return;
                        }
                    }
                }
            }
        }
        echo "false";
    }

    public function create_import_warehouse($orderid=0){
        if($orderid>0){
            $TKNO = isset($_POST['TKNO']) ? $_POST['TKNO'] : '' ;
            $TKCO = isset($_POST['TKCO']) ? $_POST['TKCO'] : '' ;
            $KPP = isset($_POST['KPP']) ? $_POST['KPP'] : '' ;
            $Lydo = isset($_POST['Lydo']) ? $_POST['Lydo'] : 'Nhập kho từ nhà cung cấp' ;
            $KhoID = isset($_POST['KhoID']) ? $_POST['KhoID'] : '' ;
            $hinhthucnhapkho = 0 ;
            $data = array(
                'Lydonhapkho'=>$Lydo,
                'TKNO'       =>$TKNO,
                'TKCO'       =>$TKCO,
                'KPP'        =>$KPP,
                'KhoID'      =>$KhoID,
                'Hinhthucnhapkho'=>$hinhthucnhapkho
            );
            $order = $this->db->query("select * from ttp_report_import_warehouse where ImportID=$orderid")->row();
            if($order){
                $this->db->where("ID",$order->ID);
                $this->db->update("ttp_report_import_warehouse",$data);
                return;
            }else{
                $result = $this->db->query("select * from ttp_report_inventory_import where ID=$orderid")->row();
                if($result){
                    $thismonth = date('m',time());
                    $thisyear = date('Y',time());
                    $max = $this->db->query("select count(1) as max from ttp_report_import_warehouse where MONTH(Ngaynhapkho)=$thismonth and YEAR(Ngaynhapkho)=$thisyear and DATE(Ngaynhapkho)>='2016-04-21' and DepartmentID=$result->DepartmentID")->row();
                    $max = $max ? $max->max + 1 : 1 ;
                    $thisyear = date('Y',time());
                    $Department = $this->db->query("select Code from ttp_department where ID=$result->DepartmentID")->row();
                    $hinhthucnhapkho = $Department ? $Department->Code : 'LO' ;
                    $max = "NK".$hinhthucnhapkho.$thisyear.$thismonth.'.'.str_pad($max, 5, '0', STR_PAD_LEFT);
                    $data['ImportID']    = $orderid;
                    $data['MaNK']       = $max;
                    $data['Ngaynhapkho']= date("Y-m-d H:i:s",time());
                    $data['UserID']     = $this->user->ID;
                    $this->db->insert("ttp_report_import_warehouse",$data);
                    return;
                }
            }
        }
    }

    public function import_from_production(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Import from production | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_import_production_add',array('base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_import/'));
        $this->template->render();
    }

    public function import_from_rejectorder(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        if($this->user->UserType==6){
            redirect(base_url().ADMINPATH.'/report/warehouse_inventory_import/import_from_production');
        }
        $this->template->add_title('Import from reject order | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_import_rejectorder_add',array('base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_import/'));
        $this->template->render();
    }

    public function import_from_internal(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Import from reject order | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_import_internal_add',array('base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_import/'));
        $this->template->render();
    }

    public function add_production(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        if($Title!=''){
            $data = array(
                'Title' => $Title,
                'Published' => 1
            );
            $this->db->insert('ttp_report_production',$data);
            $ID = $this->db->insert_id();
            echo "<option value='$ID'>$Title</option>";
        }
    }

    public function box_add_shipment($id=0){
        $result = $this->db->query("select ID,Title from ttp_report_products where ID=$id")->row();
        if($result){
            $this->load->view("warehouse_inventory_shipment_add",array('data'=>$result));
        }else{
            echo "<p>Dữ liệu gửi lên không chính xác !.</p>";
        }
    }

    public function save_shipment(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : '' ;
        $ShipmentCode = isset($_POST['ShipmentCode']) ? $_POST['ShipmentCode'] : '' ;
        $DateProduction = isset($_POST['DateProduction']) ? $_POST['DateProduction'] : '' ;
        $DateExpiration = isset($_POST['DateExpiration']) ? $_POST['DateExpiration'] : '' ;
        if($ShipmentCode!='' && $DateProduction!='' && $DateExpiration!=''){
            $data = array(
                'UserID'        => $this->user->ID,
                'ShipmentCode'  => $ShipmentCode,
                'ProductsID'    => $ID,
                'DateProduction'=> $DateProduction,
                'DateExpiration'=> $DateExpiration,
                'Created'       => date('Y-m-d H:i:s')
            );
            $this->db->insert("ttp_report_shipment",$data);
        }
    }

    public function get_products($status=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $POID = isset($_POST['POID']) ? $_POST['POID'] : 0 ;
        $bonus = array();
        $Quantity_po = array();
        if($POID>0){
            $detailspo = $this->db->query("select ProductsID,Amount from ttp_report_perchaseorder_details where POID=$POID")->result();
            if(count($detailspo)>0){
                foreach($detailspo as $row){
                    $bonus[] = $row->ProductsID;
                    $Quantity_po[$row->ProductsID] = $row->Amount;
                }
            }
        }
        $temp_bonus = $bonus;
        $bonus = count($bonus)>0 ? ' and ID in('.implode(',',$bonus).')' : '' ;
        $Quantity_products = array();
        if($bonus!='' && $POID>0){
            $bonus_quantity = count($bonus)>0 ? ' and a.ProductsID in('.implode(',',$temp_bonus).')' : '' ;
            $products = $this->db->query("select a.ProductsID,sum(a.Amount) as Amount from ttp_report_inventory_import_details a,ttp_report_inventory_import b where b.ID=a.ImportID and b.POID=$POID $bonus_quantity group by a.ProductsID")->result();
            if(count($products)>0){
                foreach($products as $row){
                    $Quantity_products[$row->ProductsID] = $row->Amount;
                }
            }
        }
        $CategoriesID = $this->user->UserType==20 ? " and CategoriesID like '%\"62\"%'" : '' ;
        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '' ;
        if($Title!='' && $Title!='*'){ 
            $result = $this->db->query("select ID,Title,MaSP,CategoriesID,Price,Donvi from ttp_report_products where (Title like '%$Title%' or MaSP like '$Title') $CategoriesID $bonus")->result();
        }else{
            $result = $this->db->query("select * from ttp_report_products where 1=1 $CategoriesID $bonus")->result();
        }
        if(count($result)>0){
            $bonus_th = $status==0 ? '' : '<th style="width: 128px;">Thêm số lượng dòng</th>' ;
            $str = "<div class='tools_search_products'>
                        <span>Tìm kiếm sản phẩm: </span>
                        <span><input type='text' placeholder='Nhập mã SKU hoặc tên sản phẩm' id='input_search_products' onchange='input_search_products(this)' /></span>
                    </div>
                    <table><tr><th></th><th>Mã Sản phẩm</th><th>Sản phẩm</th>
                    <th>
                        <select class='select_products' onchange='fillter_categories(this)'><option value='0'>-- Tất cả ngành hàng --</option>__</select>
                    </th>
                    $bonus_th
                    </tr>";
            $categories = $this->db->query("select * from ttp_report_categories")->result();
            $arr_categories = array();
            if(count($categories)){
                foreach($categories as $row){
                    $arr_categories[$row->ID] = $row->Title;
                }
            }
            $option = array();
            $bonus_td = $status==0 ? '' : "<td><input type='number' class='numbertd' value='1' style='width: 58px;' /></td>";
            foreach($result as $row){
                $row->CategoriesID = json_decode($row->CategoriesID,true);
                $row->CategoriesID = is_array($row->CategoriesID) ? $row->CategoriesID : array() ;
                $class='';
                $current_categories = array();
                if(count($row->CategoriesID)){
                    foreach($row->CategoriesID as $item){
                        if(isset($arr_categories[$item])){
                            $current_categories[] = $arr_categories[$item];
                            if(!isset($option[$item]))
                            $option[$item] = "<option value='".$item."'>".$arr_categories[$item]."</option>";
                        }
                        $class.=" categories_$item";
                    }
                }
                $str.="<tr class='trcategories $class'>";
                $temp_po = isset($Quantity_po[$row->ID]) ? $Quantity_po[$row->ID] : 0 ;
                $temp_products = isset($Quantity_products[$row->ID]) ? $Quantity_products[$row->ID] : 0 ;
                $str.="<td style='width:30px'><input type='checkbox' class='selected_products' data-id='$row->ID' data-code='$row->MaSP' data-name='$row->Title' data-price='$row->Price' data-donvi='$row->Donvi' data-po='$temp_po' data-products='$temp_products' /></td>";
                $str.="<td style='width:150px'>".$row->MaSP."</td>";
                $str.="<td>".$row->Title."</td>";
                $str.="<td style='width:200px'>".implode(',',$current_categories)."</td>";
                $str.="$bonus_td</tr>";
            }
            $option = implode('',$option);
            echo str_replace("__",$option,$str);
            echo "</table>";
            echo "<div class='fixedtools'><a class='btn btn-box-inner' onclick='add_products()'>Đưa vào đơn hàng</a></div>";
            return;
        }
        echo "false";
    }

    public function find_PXK(){
        $MaPXK = isset($_POST['MaPXK']) ? trim($_POST['MaPXK']) : '' ;
        $Getdata = isset($_POST['Getdata']) ? $_POST['Getdata'] : 0 ;
        $result = $this->db->query("select * from ttp_report_export_warehouse where MaXK = '$MaPXK'")->row();
        if($result){
            $data = $this->db->query("select b.Name from ttp_report_order a,ttp_report_customer b where a.CustomerID = b.ID and a.ID=".$result->OrderID)->row();
            if($data){
                $result->Ngayxuatkho = date('d/m/Y',strtotime($result->Ngayxuatkho));
                echo "0||$result->MaXK||$data->Name||$result->Ngayxuatkho||$result->ID||";
            }else{
                echo "1||Không tìm thấy mã phiếu xuất kho . Vui lòng thử lại ! || || || ||";
            }
            $this->load->view("admin/warehouse_inventory_import_export_warehouse",array('data'=>$result));
        }else{
            echo "1||Không tìm thấy mã phiếu xuất kho . Vui lòng thử lại !";
        }
    }

    public function load_shipment_by_products($id=0){
        if($id>0){
            $shipment = $this->uri->segment(6);
            echo "<option value='0'>-- Chọn lô --</option>";
            $result = $this->db->query("select ID,ShipmentCode from ttp_report_shipment where ProductsID=$id")->result();
            if(count($result)>0){
                foreach($result as $row){
                    $selected= $row->ID==$shipment ? "selected='selected'" : '' ;
                    echo "<option value='$row->ID' $selected>Lô $row->ShipmentCode</option>";
                }
            }
            echo "<option value='add'>-- Tạo mới --</option>";
        }else{
            echo "False";
        }
    }

    public function load_fillter_by_type_and_field(){
        $field = isset($_POST['FieldName']) ? $_POST['FieldName'] : '' ;
        if($field=='makho'){
            $result = $this->db->query("select MaKho,ID from ttp_report_warehouse order by Title ASC")->result();
            if(count($result)>0){
                echo "<select name='FieldText[]' class='form-control'>";
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->MaKho</option>";
                }
                echo "</select>";
            }
        }elseif($field=='nguoitao'){
            $result = $this->db->query("select UserName,ID from ttp_user order by UserName ASC")->result();
            if(count($result)>0){
                echo "<select name='FieldText[]' class='form-control'>";
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->UserName</option>";
                }
                echo "</select>";
            }
        }elseif($field=='hinhthucnhap'){
            echo "<select name='FieldText[]' class='form-control'>";
            echo "<option value='0'>Mua từ nhà cung cấp</option>" ;
            echo "<option value='1'>Hàng bán bị trả lại</option>" ;
            echo "<option value='2'>Thành phẩm, trả kho nội bộ</option>" ;
            echo "</select>";
        }else{
            echo '<input type="text" name="FieldText[]" id="textsearch" class="form-control" />';
        }

    }

    public function setfillter(){
        $arr_fieldname = array(0=>"a.KhoID",1=>"a.Type",2=>"a.UserID",3=>"a.MaNK",4=>"d.POCode");
        $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
        $FieldName = isset($_POST['FieldName']) ? $_POST['FieldName'] : array() ;
        $FieldOparation = isset($_POST['FieldOparation']) ? $_POST['FieldOparation'] : array() ;
        $FieldText = isset($_POST['FieldText']) ? $_POST['FieldText'] : array() ;
        $str = array();
        if(count($FieldName)>0){
            $i=0;
            $arrtemp = array();
            foreach ($FieldName as $key => $value) {
                if(!in_array($value,$arrtemp)){
                    if(isset($arr_fieldname[$value]) && isset($FieldOparation[$i])){
                        if(isset($arr_oparation[$FieldOparation[$i]]) && isset($FieldText[$i])){
                            if($arr_oparation[$FieldOparation[$i]]=='like'){
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '%".mysql_real_escape_string($FieldText[$i])."%'";
                            }else{
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '".mysql_real_escape_string($FieldText[$i])."'";
                            }
                            $i++;
                        }
                    }
                    $arrtemp[] = $value;
                }
            }
            if(count($str)>0){
                $sql = implode(' and ',$str);
                $this->session->set_userdata("fillter",$sql);
            }else{
                $this->session->set_userdata("fillter","");    
            }
        }else{
            $this->session->set_userdata("fillter","");
        }
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
        redirect($referer);
    }

    public function set_import_type(){
        $Type = isset($_GET['Type']) ? $_GET['Type'] : 0 ;
        if(is_numeric($Type)){
            $this->session->set_userdata('TypeImport',$Type);
        }
        redirect(ADMINPATH.'/report/warehouse_inventory_import');
    }

    public function get_production_from_purchaseorder(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $result = $this->db->query("select ProductionID from ttp_report_perchaseorder where ID=$ID")->row();
        if($result){
            echo $result->ProductionID;
        }else{
            echo "0";
        }
    }
}