<?php 
class Import_customer extends Admin_Controller { 

    public $limit = 30;
    public $user;
    public $classname="import_customer";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/import_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->UserType!=10 || $this->user->IsAdmin==1){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $bonus = " where Type=0";
            $search = $this->session->userdata("report_filter_customer_Search");
            $bonus = $search!='' ? " where (Phone1 like '$search' or Name like '$search')" : '' ;
                if($this->user->IsAdmin==0){
                    $bonus = $bonus!='' ? $bonus." and UserID=".$this->user->ID : " where UserID=".$this->user->ID ;
                }
            if($search!=''){
                $nav = $this->db->query("select count(1) as nav from ttp_report_customer $bonus")->row();
                $nav = $nav ? $nav->nav : 0;
                $object = $this->db->query("select * from ttp_report_customer $bonus order by ID DESC $limit_str")->result();
            }else{
                $nav=0;
                $object = array();
            }
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'keywords'  => $search,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/index',5,$nav,$this->limit)
            );
            $this->template->add_title('Customer | Manager Report Tools');
            $this->template->write_view('content','admin/import_customer_home',$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer/sales_customers");
        }
    }

    public function sales_customers(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $bonus = " where 1=1";
            $search = $this->session->userdata("report_filter_customer_Search");
            $bonus = $search!='' ? $bonus." and Name like '%$search%'" : $bonus ;
            $TypeChanel = $this->session->userdata("report_filter_customer_TypeChanel");
            $bonus = $TypeChanel=='' || $TypeChanel==0 ? $bonus." and Type=1" : $bonus ;
            $System = $this->session->userdata("report_filter_customer_SystemID");
            $bonus = $System=='' || $System==0 ? $bonus : $bonus." and SystemID=$System" ;

            $bonus = $TypeChanel==1 ? $bonus." and Type=2" : $bonus ;
            $bonus = $TypeChanel==4 ? $bonus." and Type=4" : $bonus ;
            $bonus = $TypeChanel==5 ? $bonus." and Type=5" : $bonus ;
            if($this->user->IsAdmin==0){
                $bonus = $bonus!='' ? $bonus." and UserID=".$this->user->ID : " where UserID=".$this->user->ID ;
            }
            $nav = $this->db->query("select count(1) as nav from ttp_report_customer $bonus")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select * from ttp_report_customer $bonus order by ID DESC $limit_str")->result();
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'keywords'  => $search,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/sales_customers',5,$nav,$this->limit)
            );
            $view = $TypeChanel=='' || $TypeChanel==0 ? 'import_customer_home_sales' : '' ;
            $view = $TypeChanel==1 ? 'import_customer_home_salesmt' : $view ;
            $view = $TypeChanel==4 ? 'import_customer_home_salestd' : $view ;
            $view = $TypeChanel==5 ? 'import_customer_home_salesnb' : $view ;
            $this->template->add_title('Customer | Manager Report Tools');
            $this->template->write_view('content','admin/'.$view,$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer");
        }
    }

    public function setsessionsearch(){
        if(isset($_POST['Search'])){
            $Search = mysql_real_escape_string($_POST['Search']);
            $this->session->set_userdata("report_filter_customer_Search",$Search);
        }
        if(isset($_GET['TypeChanel'])){
            $this->session->set_userdata("report_filter_customer_TypeChanel",(int)$_GET['TypeChanel']);
        }
        if(isset($_GET['SystemID'])){
            $this->session->set_userdata("report_filter_customer_SystemID",(int)$_GET['SystemID']);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function clearfilter(){
        $this->session->unset_userdata("report_filter_customer_Search");
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function add(){
        if($this->user->UserType!=10 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add',$data);
            $this->template->render();
        }
    }

    public function add_sales(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add_gt',$data);
            $this->template->render();
        }
    }

    public function add_sales_mt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add_mt',$data);
            $this->template->render();
        }
    }    

    public function add_sales_td(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add_td',$data);
            $this->template->render();
        }
    }

    public function add_sales_nb(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add_nb',$data);
            $this->template->render();
        }
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        $Type = isset($_POST['Type']) ? $_POST['Type'] : 0 ;
        $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
        $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
        $Phone2 = isset($_POST['Phone2']) ? $_POST['Phone2'] : '' ;
        $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
        $Age = isset($_POST['Age']) ? $_POST['Age'] : '' ;
        $Birthday = isset($_POST['NTNS']) ? $_POST['NTNS'] : '' ;
        $Sex = isset($_POST['Sex']) ? $_POST['Sex'] : '' ;
        $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
        $Job = isset($_POST['Job']) ? $_POST['Job'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $Testimonial = isset($_POST['Testimonial']) ? 1 : 0 ;
        $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
        $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
        $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
        $BeforeAd = isset($_POST['BeforeAd']) ? $_POST['BeforeAd'] : '' ;
        $AfterAd = isset($_POST['AfterAd']) ? $_POST['AfterAd'] : '' ;
        $Solution = isset($_POST['Solution']) ? $_POST['Solution'] : '' ;
        if($Name!='' && $Phone1!='' && $Address!=''){
            $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
            if(!$check){
                $max = $this->db->query("select max(ID) as ID from ttp_report_customer")->row();
                $Code = "KH".str_pad($max->ID+1, 8, '0', STR_PAD_LEFT);
                $data = array(
                    'ID'            => $max->ID+1,
                    'Type'          => $Type,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Email'         => $Email,
                    'Phone1'        => $Phone1,
                    'Phone2'        => $Phone2,
                    'Address'       => $Address,
                    'Age'           => $Age,
                    'Birthday'      => $Birthday,
                    'Sex'           => $Sex,
                    'Job'           => $Job,
                    'Note'          => $Note,
                    'Testimonial'   => $Testimonial,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'UserID'        => $this->user->ID
                );
                $this->db->insert("ttp_report_customer",$data);
                $data = array(
                    'CustomerID'=> $max->ID+1,
                    'BeforeAd'  => $BeforeAd,
                    'AfterAd'   => $AfterAd,
                    'Solution'  => $Solution,
                    'UserID'    => $this->user->ID,
                    'Created'   => date('Y-m-d H:i:s')
                );
                $this->db->insert("ttp_report_advisory_history",$data);
            }else{
                echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                return;    
            }
        }
        redirect(ADMINPATH.'/report/import_customer/');
    }

    public function add_new_gt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = isset($_POST['Type']) ? $_POST['Type'] : 1 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
            $AreaNote = isset($_POST['AreaNote']) ? $_POST['AreaNote'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $Surrogate = isset($_POST['Surrogate']) ? $_POST['Surrogate'] : '' ;
            $SystemID = isset($_POST['SystemID']) ? $_POST['SystemID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
                if(!$check){
                    $data = array(
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone1'        => $Phone1,
                        'Address'       => $Address,
                        'Note'          => $Note,
                        'AreaNote'      => $AreaNote,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'Surrogate'     => $Surrogate,
                        'SystemID'      => $SystemID,
                        'AddressOrder' => $AddressOrder,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->insert("ttp_report_customer",$data);
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;    
                }
            }
            redirect(ADMINPATH.'/report/import_customer/sales_customers');
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function add_new_mt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 2 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Surrogate = isset($_POST['Surrogate']) ? $_POST['Surrogate'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $SystemID = isset($_POST['SystemID']) ? $_POST['SystemID'] : 0 ;
            $Dept = isset($_POST['Dept']) ? (int)$_POST['Dept'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            $Company = isset($_POST['Company']) ? $_POST['Company'] : '' ;
            if($Code!='' && $Name!='' && $Address!=''){
                $data = array(
                    'Type'          => $Type,
                    'Email'         => $Email,
                    'Surrogate'     => $Surrogate,
                    'Phone1'        => $Phone1,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Address'       => $Address,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'SystemID'      => $SystemID,
                    'Dept'          => $Dept,
                    'AddressOrder' => $AddressOrder,
                    'UserID'        => $this->user->ID,
                    'Company'       => $Company
                );
                $this->db->insert("ttp_report_customer",$data);
            }
            redirect(ADMINPATH.'/report/import_customer/sales_customers');
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function add_new_td(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = isset($_POST['Type']) ? $_POST['Type'] : 4 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
                if(!$check){
                    $data = array(
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone1'        => $Phone1,
                        'Address'       => $Address,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'AddressOrder' => $AddressOrder,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->insert("ttp_report_customer",$data);
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;    
                }
            }
            redirect(ADMINPATH.'/report/import_customer/sales_customers');
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function add_new_nb(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = isset($_POST['Type']) ? $_POST['Type'] : 5 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
                if(!$check){
                    $data = array(
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone1'        => $Phone1,
                        'Address'       => $Address,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'AddressOrder' => $AddressOrder,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->insert("ttp_report_customer",$data);
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;    
                }
            }
            redirect(ADMINPATH.'/report/import_customer/sales_customers');
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function edit($id=0){
        if($this->user->UserType!=10 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            if(is_numeric($id) && $id>0){
                $bonus='';
                if($this->user->IsAdmin==0){
                    $bonus = " and UserID=".$this->user->ID ;
                }
                $result = $this->db->query("select * from ttp_report_customer where ID=$id $bonus")->row();
                if(!$result) return;
                $this->template->add_title('Edit Customer | Manager report Tools');
                $data = array(
                    'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                    'data'      =>  $result
                );
                $this->template->write_view('content','admin/import_customer_edit',$data);
                $this->template->render();
            }
        }
    }

    public function edit_sales($id=0){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            if(is_numeric($id) && $id>0){
                $bonus='';
                if($this->user->IsAdmin==0){
                    $bonus = " and UserID=".$this->user->ID ;
                }
                $result = $this->db->query("select * from ttp_report_customer where ID=$id $bonus")->row();
                if(!$result) return;
                $this->template->add_title('Edit Customer | Manager report Tools');
                $data = array(
                    'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                    'data'      =>  $result
                );
                $view = $result->Type==1 ? "import_customer_edit_gt" : "" ;
                $view = $result->Type==2 ? "import_customer_edit_mt" : $view ;
                $view = $result->Type==4 ? "import_customer_edit_td" : $view ;
                $view = $result->Type==5 ? "import_customer_edit_nb" : $view ;
                $this->template->write_view('content','admin/'.$view,$data);
                $this->template->render();
            }
        }
    }

    public function update(){
        if($this->user->UserType!=10 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Type = isset($_POST['Type']) ? $_POST['Type'] : 0 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Phone2 = isset($_POST['Phone2']) ? $_POST['Phone2'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Age = isset($_POST['Age']) ? $_POST['Age'] : '' ;
            $Birthday = isset($_POST['NTNS']) ? $_POST['NTNS'] : '' ;
            $Sex = isset($_POST['Sex']) ? $_POST['Sex'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Job = isset($_POST['Job']) ? $_POST['Job'] : '' ;
            $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
            $Testimonial = isset($_POST['Testimonial']) ? 1 : 0 ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $BeforeAd = isset($_POST['BeforeAd']) ? $_POST['BeforeAd'] : '' ;
            $AfterAd = isset($_POST['AfterAd']) ? $_POST['AfterAd'] : '' ;
            $Solution = isset($_POST['Solution']) ? $_POST['Solution'] : '' ;
            if($Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1' and ID!=$ID")->row();
                if(!$check){
                    $Code = "KH".str_pad($ID, 8, '0', STR_PAD_LEFT);
                    $data = array(
                        'ID'            => $ID,
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone2'        => $Phone2,
                        'Address'       => $Address,
                        'Age'           => $Age,
                        'Birthday'      => $Birthday,
                        'Sex'           => $Sex,
                        'Job'           => $Job,
                        'Note'          => $Note,
                        'Testimonial'   => $Testimonial,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->where("ID",$ID);
                    $this->db->update("ttp_report_customer",$data);
                    if($BeforeAd!='' || $AfterAd !='' || $Solution !=''){
                        $data = array(
                            'CustomerID'=> $ID,
                            'BeforeAd'  => $BeforeAd,
                            'AfterAd'   => $AfterAd,
                            'Solution'  => $Solution,
                            'UserID'    => $this->user->ID,
                            'Created'   => date('Y-m-d H:i:s')
                        );
                        $this->db->insert("ttp_report_advisory_history",$data);
                    }
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;    
                }
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function update_gt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 1 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
            $AreaNote = isset($_POST['AreaNote']) ? $_POST['AreaNote'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $Surrogate = isset($_POST['Surrogate']) ? $_POST['Surrogate'] : '' ;
            $SystemID = isset($_POST['SystemID']) ? $_POST['SystemID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1' and ID!=$ID")->row();
                if(!$check){
                    $data = array(
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone1'        => $Phone1,
                        'Address'       => $Address,
                        'Note'          => $Note,
                        'AreaNote'      => $AreaNote,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'Surrogate'     => $Surrogate,
                        'AddressOrder'  => $AddressOrder,
                        'SystemID'      => $SystemID,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->where("ID",$ID);
                    $this->db->update("ttp_report_customer",$data);
                    redirect(ADMINPATH.'/report/import_customer/sales_customers');
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;    
                }
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function update_mt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 2 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Surrogate = isset($_POST['Surrogate']) ? $_POST['Surrogate'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $SystemID = isset($_POST['SystemID']) ? $_POST['SystemID'] : 0 ;
            $Dept = isset($_POST['Dept']) ? (int)$_POST['Dept'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            $Company = isset($_POST['Company']) ? $_POST['Company'] : '' ;
            if($Code!='' && $Name!='' && $Address!=''){
                $data = array(
                    'Type'          => $Type,
                    'Email'         => $Email,
                    'Surrogate'     => $Surrogate,
                    'Phone1'        => $Phone1,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Address'       => $Address,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'SystemID'      => $SystemID,
                    'Dept'          => $Dept,
                    'AddressOrder'  => $AddressOrder,
                    'UserID'        => $this->user->ID,
                    'Company'       => $Company
                );
                $this->db->where("ID",$ID);
                $this->db->update("ttp_report_customer",$data);
                redirect(ADMINPATH.'/report/import_customer/sales_customers');
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function update_td(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 4 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Address!=''){
                $data = array(
                    'Type'          => $Type,
                    'Email'         => $Email,
                    'Phone1'        => $Phone1,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Address'       => $Address,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'AddressOrder'  => $AddressOrder,
                    'UserID'        => $this->user->ID
                );
                $this->db->where("ID",$ID);
                $this->db->update("ttp_report_customer",$data);
                redirect(ADMINPATH.'/report/import_customer/sales_customers');
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function update_nb(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 5 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Address!=''){
                $data = array(
                    'Type'          => $Type,
                    'Email'         => $Email,
                    'Phone1'        => $Phone1,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Address'       => $Address,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'AddressOrder'  => $AddressOrder,
                    'UserID'        => $this->user->ID
                );
                $this->db->where("ID",$ID);
                $this->db->update("ttp_report_customer",$data);
                redirect(ADMINPATH.'/report/import_customer/sales_customers');
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function get_history(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $result = $this->db->query("select a.MaDH,a.Ngaydathang,a.Status,a.Total,c.UserName from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID =b.ID and b.ID=$ID")->result();
        echo "<table><tr><th style='width:80px'>STT</th><th>Ngày mua</th><th>Mã ĐH</th><th>Giá trị ĐH</th><th>Nhân viên bán hàng</th><th>Tình trạng ĐH</th></tr>";
        if(count($result)>0){
            $array_status = array(
                9=>'Chờ nhân viên điều phối',
                8=>'Đơn hàng bị trả về',
                7=>'Chuyển sang bộ phận giao hàng',
                6=>'Đơn hàng bị trả về từ kế toán',
                5=>'Đơn hàng chờ kế toán duyệt',
                4=>'Đơn hàng bị trả về từ kho',
                3=>'Đơn hàng mới chờ kho duyệt',
                2=>'Đơn hàng nháp',
                0=>'Đơn hàng thành công',
                1=>'Đơn hàng hủy'
            );
            $i=1;
            foreach($result as $row){
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>".date('d/m/Y',strtotime($row->Ngaydathang))."</td>";
                echo "<td>$row->MaDH</td>";
                echo "<td>".number_format($row->Total)."</td>";
                echo "<td>$row->UserName</td>";
                echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>";
                echo "</tr>";
                $i++;
            }
        }else{
            echo '<tr><td colspan="6">Chưa có dữ liệu lịch sử giao dịch</td></tr>';
        }
        echo "</table>";
    }

    public function get_history_advisory(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $result = $this->db->query("select a.*,b.UserName from ttp_report_advisory_history a,ttp_user b where a.UserID=b.ID and a.CustomerID=$ID")->result();
        echo "<table><tr><th style='width:80px'>STT</th><th>Vấn đề trước khi sử dụng</th><th>Giải pháp của nv tư vấn</th><th>Hiệu quả khi sử dụng</th><th>Người tạo</th><th>Ngày cập nhật</th></tr>";
        if(count($result)>0){
            $i=1;
            foreach($result as $row){
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>".$row->BeforeAd."</td>";
                echo "<td>".$row->Solution."</td>";
                echo "<td>".$row->AfterAd."</td>";
                echo "<td>$row->UserName</td>";
                echo "<td>".date('d/m/Y',strtotime($row->Created))."</td>";
                echo "</tr>";
                $i++;
            }
        }else{
            echo '<tr><td colspan="5">Chưa có dữ liệu lịch sử tư vấn</td></tr>';
        }
        echo "</table>";
    }

    public function checkphone(){
        $data = isset($_POST['data']) ? $_POST['data'] : '' ;
        if($data!=''){
            $check = $this->db->query("select ID from ttp_report_customer where Phone1 ='$data'")->row();
            echo $check ? "false" : "ok";
        }
    }

    public function get_city_by_area(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        echo "<option value=''>-- Chọn tỉnh thành --</option>";
        if(is_numeric($ID)){
            $result = $this->db->query("select * from ttp_report_city where AreaID=$ID order by Title ASC")->result();
            if(count($result)>0){
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->Title</option>";
                }
            }
        }
    }

    public function get_district_by_city(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $data = array('DistrictHtml'=>"<option value='0'>-- Chọn quận huyện --</option>",'WarehouseHtml'=>'<option value="">Không có kho hàng</option>');
        if(is_numeric($ID)){
            $result = $this->db->query("select * from ttp_report_district where CityID=$ID order by Title ASC")->result();
            if(count($result)>0){
                foreach($result as $row){
                    $data['DistrictHtml'].= "<option value='$row->ID'>$row->Title</option>";
                }
            }
            $warehouse = $this->db->query("select b.MaKho,b.ID from ttp_report_city a,ttp_report_warehouse b where a.DefaultWarehouse=b.ID and a.ID=$ID")->row();
            if($warehouse){
                $data['WarehouseHtml'] = "<option value='$warehouse->ID'>$warehouse->MaKho</option>";
            }
        }
        echo json_encode($data);
    }
}
?>
