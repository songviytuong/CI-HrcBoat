<?php 
class Warehouse_properties extends Admin_Controller { 
 
    public $limit = 15;
 	public $user;
 	public $classname="warehouse_properties";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_properties")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_properties order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_properties/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_properties/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Properties | Warehouse Report Tools');
		$this->template->write_view('content','admin/warehouse_properties_home',$data);
		$this->template->render();
	}

    public function search($link='search'){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $PropertiesID = $this->session->userdata("report_filter_parent_PropertiesID");
        $str_nav = "select count(1) as nav from ttp_report_properties";
        $str = "select * from ttp_report_properties";
        if($PropertiesID>0){
            $str.=" where Path like '$PropertiesID/%'";
            $str_nav.=" where Path like '$PropertiesID/%'";
        }
        $nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
        $this->template->add_title('Tìm kiếm dữ liệu');
        $data=array(
            'data'  => $this->db->query($str." order by ID DESC $limit_str")->result(),
            'nav'   => $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_properties/'.$link,5,$nav,$this->limit),
            'start' => $start,
            'find'      =>  $nav,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_properties/',
        );
        $this->template->write_view('content','admin/warehouse_properties_home',$data);
        $this->template->render();
    }

    public function setsessionsearch(){
        if(isset($_POST['PropertiesID'])){
            $PropertiesID = mysql_real_escape_string($_POST['PropertiesID']);
            $this->session->set_userdata("report_filter_parent_PropertiesID",$PropertiesID);
        }
        $this->search('setsessionsearch');
    }

    public function clearfilter(){
        $this->session->unset_userdata("report_filter_parent_PropertiesID");
        $this->search('setsessionsearch');
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Properties add | Manager report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_properties/'
        );
        $this->template->write_view('content','admin/warehouse_properties_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $PropertiesID = isset($_POST['PropertiesID']) ? $this->lib->fill_data($_POST['PropertiesID']) : 0 ;
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
        if(!is_numeric($PropertiesID)) return;
        if($Title!='' && $PropertiesID!=''){
            $path = $this->db->query("select Path from ttp_report_properties where ID=$PropertiesID")->row();
            $path = $path ? $path->Path : '' ;
            $max = $this->db->query("select max(ID) as ID from ttp_report_properties")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $data = array(
                'ID'            => $max,
                'ParentID'      => $PropertiesID,
                'Title'         => $Title,
                'Published'     => $Published,
                'Path'          => $PropertiesID>0 ? $path.'/'.$max : $max
            );
            $data['Level'] = count(explode('/',$data['Path']));
            $this->db->insert("ttp_report_properties",$data);
        }
        redirect(ADMINPATH.'/report/warehouse_properties/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_properties where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_properties where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit Properties | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/warehouse_properties/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/warehouse_properties_edit',$data);
            $this->template->render();
        }
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $PropertiesID = isset($_POST['PropertiesID']) ? $this->lib->fill_data($_POST['PropertiesID']) : 0 ;
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0 ;
        if($Title!='' && $PropertiesID!=''){
            $path = $this->db->query("select Path from ttp_report_properties where ID=$PropertiesID")->row();
            $path = $path ? $path->Path : '' ;
            $data = array(
                'ParentID'  => $PropertiesID,
                'Published'     => $Published,
                'Title'     => $Title,
                'Path'      => $PropertiesID>0 ? $path.'/'.$ID : $ID 
            );
            $data['Level'] = count(explode('/',$data['Path']));
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_properties",$data);
        
        }
        redirect(ADMINPATH.'/report/warehouse_properties/');
    }
}
?>
