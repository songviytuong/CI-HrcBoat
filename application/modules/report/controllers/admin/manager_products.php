<?php 
class Manager_products extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="manager_products";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_products")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_products order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_products/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_products/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Products | Manager Report Tools');
		$this->template->write_view('content','admin/manager_products_home',$data);
		$this->template->render();
	}

    public function search($link='search'){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $CategoriesID = $this->session->userdata("report_filter_products_CategoriesID");
        $str_nav = "select count(1) as nav from ttp_report_products";
        $str = "select * from ttp_report_products";
        if($CategoriesID>0){
            $str.=" where CategoriesID=$CategoriesID";
            $str_nav.=" where CategoriesID=$CategoriesID";
        }
        $nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
        $this->template->add_title('Tìm kiếm dữ liệu');
        $data=array(
            'data'  => $this->db->query($str." order by ID DESC $limit_str")->result(),
            'nav'   => $this->lib->nav(base_url().ADMINPATH.'/report/manager_products/'.$link,5,$nav,$this->limit),
            'start' => $start,
            'find'      =>  $nav,
            'base_link' =>  base_url().ADMINPATH.'/report/manager_products/',
        );
        $this->template->write_view('content','admin/manager_products_home',$data);
        $this->template->render();
    }

    public function setsessionsearch(){
        if(isset($_POST['CategoriesID'])){
            $CategoriesID = mysql_real_escape_string($_POST['CategoriesID']);
            $this->session->set_userdata("report_filter_products_CategoriesID",$CategoriesID);
        }
        $this->search('setsessionsearch');
    }

    public function clearfilter(){
        $this->session->unset_userdata("report_filter_products_CategoriesID");
        $this->search('setsessionsearch');
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Products add | Manager report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_products/'
        );
        $this->template->write_view('content','admin/manager_products_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Price = isset($_POST['Price']) ? $_POST['Price'] : '' ;
        $MaSP = isset($_POST['MaSP']) ? $_POST['MaSP'] : '' ;
        $Donvi = isset($_POST['Donvi']) ? $_POST['Donvi'] : '' ;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0 ;
        if(!is_numeric($Price)) return;
        if($Title!='' && $Price>=0 && $MaSP!=''){
            $data = array(
                'Donvi'         => $Donvi,
                'MaSP'          => $MaSP,
                'Price'         => $Price,
                'CategoriesID'  => $CategoriesID,
                'Title'         => $Title
            );
            $this->db->insert("ttp_report_products",$data);
        }
        redirect(ADMINPATH.'/report/manager_products/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_products where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_products where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit Products | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/manager_products/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/manager_products_edit',$data);
            $this->template->render();
        }
    }
    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $MaSP = isset($_POST['MaSP']) ? $_POST['MaSP'] : '' ;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0 ;
        $Price = isset($_POST['Price']) ? $_POST['Price'] : '' ;
        $Donvi = isset($_POST['Donvi']) ? $_POST['Donvi'] : '' ;
        if(!is_numeric($Price)) return;
        if($Title!='' && $Price>=0 && $MaSP!=''){
            $data = array(
                'Donvi'         => $Donvi,
                'MaSP'          => $MaSP,
                'CategoriesID'  => $CategoriesID,
                'Title'         => $Title,
                'Price'         => $Price
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_products",$data);
        
        }
        redirect(ADMINPATH.'/report/manager_products/');
    }
}
?>
