<?php 
class Warehouse_inventory_transferproducts extends Admin_Controller { 

    public $user;
    public $classname="warehouse_inventory_transferproducts";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
    
    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        
        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $WarehouseSender = isset($_GET['WarehouseSender']) ? (int)$_GET['WarehouseSender'] : 0 ;
        if(is_numeric($WarehouseSender) && $WarehouseSender>0){
            $fillter .= " and a.WarehouseSender = $WarehouseSender";
        }
        $WarehouseReciver = isset($_GET['WarehouseReciver']) ? (int)$_GET['WarehouseReciver'] : 0 ;
        if(is_numeric($WarehouseReciver) && $WarehouseReciver>0){
            $fillter .= " and a.WarehouseReciver = $WarehouseReciver";
        }
        if($this->user->UserType==8){
            $fillter .= " and a.Status=0 and b.Manager like '%\"".$this->user->ID."\"%'";
        }elseif($this->user->UserType==2){
            $fillter.= " and (b.Manager like '%\"".$this->user->ID."\"%' or (c.Manager like '%\"".$this->user->ID."\"%' and a.Status=3))";
        }elseif($this->user->UserType==3){
            $fillter.= " and a.Status=4";
        }
        $nav = $this->db->query("select count(1) as nav from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c where a.WarehouseSender=b.ID and a.WarehouseReciver=c.ID $fillter")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,b.Makho as Sender,c.Makho as Reciver from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c where a.WarehouseSender=b.ID and a.WarehouseReciver=c.ID $fillter order by a.ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_transferproducts/',
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_inventory_transferproducts/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Warehouse Transfer | Warehouse Report Tools');
        $view = 'warehouse_inventory_transferproducts_home';
        $this->template->write_view('content','admin/'.$view,$data);
        $this->template->render();
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Warehouse Transfer | Warehouse Report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_transferproducts/'
        );
        $view = 'admin/warehouse_inventory_transferproducts_add' ;
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Note = isset($_POST["Note"]) ? $_POST["Note"] : '';
        $Ghichu = isset($_POST["Ghichu"]) ? $_POST["Ghichu"] : '';
        $NgayXK = isset($_POST["NgayXK"]) ? $_POST["NgayXK"] : date('Y-m-d');
        $KhoSender = isset($_POST["KhoSender"]) ? $_POST["KhoSender"] : 0;
        $KhoReciver = isset($_POST["KhoReciver"]) ? $_POST["KhoReciver"] : 0;
        $Status = isset($_POST["Status"]) ? $_POST["Status"] : 0;
        if($KhoSender==$KhoReciver){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            echo "<script>alert('Vui lòng chọn kho xuất và kho nhập khác nhau !');</script>";
            echo "<script>window.location='".$_SERVER['HTTP_REFERER']."'</script>";
            return;
        }
        $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
        $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
        $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
        $ProductsNote = isset($_POST['ProductsNote']) ? $_POST['ProductsNote'] : array() ;
        if(is_array($ProductsID) && count($ProductsID)>0){
            $str_products = implode(',', $ProductsID);
            $available = $this->db->query("select a.Available,a.OnHand,a.ShipmentID,a.ProductsID,a.DateInventory from ttp_report_inventory a where a.ProductsID in($str_products) and a.WarehouseID=$KhoSender and a.LastEdited=1 and a.Available>0")->result();
            if(count($available)>0){
                $arr = array();
                $total_amount = 0;
                foreach($ProductsID as $key=>$value){
                    $Shipment_row = isset($ShipmentID[$key]) ? $ShipmentID[$key] : 0 ;
                    if($Shipment_row==0){
                        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                        echo "<script>alert('Vui lòng chọn kho cho tất cả sản phẩm !');</script>";
                        echo "<script>window.location='".$_SERVER['HTTP_REFERER']."'</script>";
                        return;
                    }
                    $Amount_row = isset($Amount[$key]) ? $Amount[$key] : 0 ;
                    $Note_row = isset($ProductsNote[$key]) ? $ProductsNote[$key] : 0 ;
                    $total_amount+=$Amount_row;
                    $arr[] = "(OrderIDV,$value,$Shipment_row,$KhoSender,$Amount_row,'$Note_row')";
                }
                $max = $this->db->query("select max(ID) as ID from ttp_report_transferorder")->row();
                $max = $max ? $max->ID+1 : 1 ;
                $monthuser = date("m",time());
                $yearuser = date("Y",time());
                $userorder = $this->db->query("select count(ID) as SL from ttp_report_transferorder where MONTH(DateCreated)=$monthuser and YEAR(DateCreated)=$yearuser")->row();
                $userorder = $userorder ? $userorder->SL+1 : 1 ;
                $monyear = date('Ym',strtotime($NgayXK));
                $idbyUser = str_pad($userorder, 5, '0', STR_PAD_LEFT);
                $MaDH = 'DH'.$monyear."_".$idbyUser;
                $data = array(
                    'ID'                =>$max,
                    'WarehouseSender'   =>$KhoSender,
                    'WarehouseReciver'  =>$KhoReciver,
                    'OrderCode'         =>$MaDH,
                    'TotalProducts'     =>count($arr),
                    'TotalRequest'      =>$total_amount,
                    'DateCreated'       =>$NgayXK,
                    'Status'            =>$Status,
                    'Note'              =>$Note,
                    'UserID'            =>$this->user->ID,
                    'Created'           =>date('Y-m-d H:i:s'),
                    'LastEdited'        =>date('Y-m-d H:i:s')
                );
                $this->db->insert("ttp_report_transferorder",$data);
                $OrderID = $this->db->insert_id();
                $arr = "insert into ttp_report_transferorder_details(OrderID,ProductsID,ShipmentID,WarehouseID,Request,Note) values".implode(',',$arr);
                $arr = str_replace('OrderIDV',$OrderID,$arr);
                $this->db->query($arr);
                $datahis = array(
                    'OrderID'   => $OrderID,
                    'Thoigian'  => date('Y-m-d H:i:s',time()),
                    'Status'    => $Status,
                    "Ghichu"    => $Ghichu,
                    "UserID"    => $this->user->ID
                );
                $this->db->insert('ttp_report_transferorder_history',$datahis);
            }
        }
        redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
    }

    public function update_from_warehouseadmin(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if($this->user->UserType!=8 && $this->user->IsAdmin!=1) return;
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $Status = isset($_POST['Status']) ? $_POST['Status'] : 0 ;
        if($Status!=1 && $Status!=2) return;
        $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
        $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
        $NumberTransfer = isset($_POST['NumberTransfer']) ? $_POST['NumberTransfer'] : '' ;
        if($NumberTransfer=='') return;
        $order = $this->db->query("select * from ttp_report_transferorder where ID=$ID")->row();
        if($order){
            $details = $this->db->query("select * from ttp_report_transferorder_details where OrderID=$ID")->result();
            if(count($details)>0){
                $total_amount = 0;
                if($Status==2){
                    $i=0;
                    foreach($details as $row){
                        $Amount_row = isset($Amount[$i]) ? $Amount[$i] : 0 ;
                        $check = $this->db->query("select * from ttp_report_inventory where WarehouseID=$row->WarehouseID and ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and LastEdited=1 order by ID DESC")->row();
                        if($check){
                            $Request=$check->Available<$Amount_row ? $check->Available : $Amount_row ;
                            $total_amount+=$Request;
                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and WarehouseID=$row->WarehouseID and LastEdited=1");
                            if($check->DateInventory!=date('Y-m-d')){
                                $check->Available = $check->Available-$Request;
                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$check->OnHand,$check->Available,$row->WarehouseID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                                $this->db->query("update ttp_report_transferorder_details set TotalExport=$Request where ID=$row->ID");
                            }else{
                                $check->Available = $check->Available-$Request;
                                $this->db->query("update ttp_report_inventory set Available=$check->Available,LastEdited=1 where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$row->WarehouseID");
                                $this->db->query("update ttp_report_transferorder_details set TotalExport=$Request where ID=$row->ID");
                            }
                        }
                        $i++;
                    }
                }
                $data = array(
                    'TotalExport'       =>$total_amount,
                    'Status'            =>$Status,
                    'LastEdited'        =>date('Y-m-d H:i:s')
                );
                $this->db->where("ID",$ID);
                $this->db->update("ttp_report_transferorder",$data);
                $datahis = array(
                    'OrderID'   => $ID,
                    'Thoigian'  => date('Y-m-d H:i:s',time()),
                    'Status'    => $Status,
                    "Ghichu"    => $Ghichu,
                    "UserID"    => $this->user->ID
                );
                $this->db->insert('ttp_report_transferorder_history',$datahis);
                $data = array(
                    'OrderID'   => $ID,
                    'Code'      => $NumberTransfer,
                    'Note'      => $order->Note,
                    'TransferDate'=> date('Y-m-d'),
                    'Created'   => date('Y-m-d H:i:s'),
                    'UserID'    => $this->user->ID
                );
                $this->db->insert('ttp_report_transferorder_mobilization',$data);
            }
        }
        redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
    }

    public function accept_export_warehouse($ID=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $order = $this->db->query("select a.* from ttp_report_transferorder a,ttp_report_warehouse b where b.Manager like '%\"".$this->user->ID."\"%' and a.WarehouseSender=b.ID and a.ID=$ID and a.UserID=".$this->user->ID)->row();
        $Status=3;
        if($order){
            if($order->Status==2){
                $details = $this->db->query("select * from ttp_report_transferorder_details where OrderID=$ID")->result();
                if(count($details)>0){
                    foreach($details as $row){
                        $check = $this->db->query("select * from ttp_report_inventory where WarehouseID=$row->WarehouseID and ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and LastEdited=1 order by ID DESC")->row();
                        if($check){
                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and WarehouseID=$row->WarehouseID and LastEdited=1");
                            if($check->DateInventory!=date('Y-m-d')){
                                $Onhand = $check->OnHand - $row->TotalExport;
                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$Onhand,$check->Available,$row->WarehouseID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                            }else{
                                $Onhand = $check->OnHand - $row->TotalExport;
                                $this->db->query("update ttp_report_inventory set OnHand=$Onhand,LastEdited=1,Available=$check->Available where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$row->WarehouseID");
                            }
                        }
                    }
                }
                $data = array(
                    'Status'            =>$Status,
                    'ExportDate'        =>date('Y-m-d H:i:s'),
                    'LastEdited'        =>date('Y-m-d H:i:s')
                );
                $this->db->where("ID",$ID);
                $this->db->update("ttp_report_transferorder",$data);
                $datahis = array(
                    'OrderID'   => $ID,
                    'Thoigian'  => date('Y-m-d H:i:s',time()),
                    'Status'    => $Status,
                    "Ghichu"    => "Xác nhận chính thức xuất kho",
                    "UserID"    => $this->user->ID
                );
                $this->db->insert('ttp_report_transferorder_history',$datahis);
            }
        }
        redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
    }

    public function update_from_warehouse(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if($this->user->UserType!=2 && $this->user->IsAdmin!=1) redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");;
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $Status = isset($_POST['Status']) ? $_POST['Status'] : 0 ;
        if($Status!=4) redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
        $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
        $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
        if(count($ShipmentID)==0){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            echo "<script>alert('Dữ liệu cần thiết không đủ để thực hiện tính năng !');</script>";
            echo "<script>window.location='".$_SERVER['HTTP_REFERER']."'</script>";
            return;
        }
        $order = $this->db->query("select a.* from ttp_report_transferorder a,ttp_report_warehouse b where a.ID=$ID and a.WarehouseReciver=b.ID and b.Manager like '%\"".$this->user->ID."\"%'")->row();
        if($order){
            $details = $this->db->query("select * from ttp_report_transferorder_details where OrderID=$ID")->result();
            if(in_array(0,$ShipmentID) || count($details)!=count($ShipmentID)){
                echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                echo "<script>alert('Vui lòng chọn lô hàng cho sản phẩm cần nhập kho !');</script>";
                echo "<script>window.location='".$_SERVER['HTTP_REFERER']."'</script>";
                return;
            }
            if(count($details)>0){
                $i=0;
                foreach($details as $row){
                    $Shipment_row = isset($ShipmentID[$i]) ? $ShipmentID[$i] : 0 ;
                    $check = $this->db->query("select * from ttp_report_inventory where WarehouseID=$order->WarehouseReciver and ShipmentID=$Shipment_row and ProductsID=$row->ProductsID and LastEdited=1 order by ID DESC")->row();
                    if($check){
                        $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row->ProductsID and ShipmentID=$Shipment_row and WarehouseID=$order->WarehouseReciver and LastEdited=1");
                        if($check->DateInventory==date('Y-m-d')){
                            $this->db->query("update ttp_report_inventory set OnHand=OnHand+$row->TotalExport,LastEdited=1,Available=Available+$row->TotalExport where ProductsID=$row->ProductsID and ShipmentID=$Shipment_row and DateInventory='$check->DateInventory' and WarehouseID=$order->WarehouseReciver");
                        }else{
                            $check->OnHand = $row->TotalExport+$check->OnHand;
                            $check->Available = $row->TotalExport+$check->Available;
                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$check->OnHand,$check->Available,$order->WarehouseReciver,$Shipment_row,'".date('Y-m-d')."',1)");
                        }
                    }else{
                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$row->TotalExport,$row->TotalExport,$order->WarehouseReciver,$Shipment_row,'".date('Y-m-d')."',1)");
                    }
                    $this->db->query("update ttp_report_transferorder_details set TotalImport=$row->TotalExport,ShipmentIDImport=$Shipment_row where ID=$row->ID");
                    $i++;
                }
            }
            $data = array(
                'TotalImport'       =>$order->TotalExport,
                'Status'            =>$Status,
                'ImportDate'        =>date('Y-m-d H:i:s'),
                'LastEdited'        =>date('Y-m-d H:i:s')
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_transferorder",$data);
            $datahis = array(
                'OrderID'   => $ID,
                'Thoigian'  => date('Y-m-d H:i:s',time()),
                'Status'    => $Status,
                "Ghichu"    => $Ghichu,
                "UserID"    => $this->user->ID
            );
            $this->db->insert('ttp_report_transferorder_history',$datahis);
        }
        redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
    }

    public function preview_bill($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Preview Warehouse Transfer | Warehouse Report Tools');
        $result = $this->db->query("select a.*,b.Makho as SenderTitle,b.Address as SenderAddress,c.Makho as ReciverTitle,c.Address as ReciverAddress from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c where a.WarehouseSender=b.ID and a.WarehouseReciver=c.ID and a.ID=$id")->row();
        if($result){
            $data = array(
                'data'=>$result,
                'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_transferproducts/'
            );
            $view = 'admin/warehouse_inventory_transferproducts_createbill' ;
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
        }
    }

    public function mobilization($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Preview Mobilization Order | Warehouse Report Tools');
        $result = $this->db->query("select a.*,b.Makho as SenderTitle,b.Address as SenderAddress,c.Makho as ReciverTitle,c.Address as ReciverAddress from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c where a.WarehouseSender=b.ID and a.WarehouseReciver=c.ID and a.ID=$id")->row();
        if($result){
            $data = array(
                'data'=>$result,
                'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_transferproducts/'
            );
            $view = 'admin/warehouse_inventory_transferproducts_mobilization' ;
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
        }
    }

    public function print_bill(){
        if($this->user->IsAdmin==1 || $this->user->UserType==3){
            $orderid = isset($_POST['OrderID']) ? $_POST['OrderID'] : 0 ;
            if($orderid>0){
                $TKNO = isset($_POST['TKNO']) ? $_POST['TKNO'] : '' ;
                $TKCO = isset($_POST['TKCO']) ? $_POST['TKCO'] : '' ;
                $KPP = isset($_POST['KPP']) ? $_POST['KPP'] : '' ;
                $Lydo = isset($_POST['Lydo']) ? $_POST['Lydo'] : '' ;
                $data = array(
                    'Note'      =>$Lydo,
                    'TKNO'      =>$TKNO,
                    'TKCO'      =>$TKCO,
                    'KPP'       =>$KPP
                );
                $order = $this->db->query("select * from ttp_report_transferorder_bill where OrderID=$orderid")->row();
                if($order){
                    $this->db->where("ID",$order->ID);
                    $this->db->update("ttp_report_transferorder_bill",$data);
                    echo "OK";
                    return;
                }else{
                    $thismonth = date('m',time());
                    $thisyear = date('Y',time());
                    $max = $this->db->query("select count(1) as max from ttp_report_transferorder_bill where MONTH(DateCreated)=$thismonth and YEAR(DateCreated)=$thisyear")->row();
                    $max = $max ? $max->max + 1 : 1 ;
                    $thisyear = date('y',time());
                    $max = "LCNB".$thisyear.$thismonth.'.'.str_pad($max, 5, '0', STR_PAD_LEFT);
                    $data['OrderID']    = $orderid;
                    $data['Code']       = $max;
                    $data['DateCreated']= date("Y-m-d H:i:s",time());
                    $data['UserID']     = $this->user->ID;
                    $this->db->insert("ttp_report_transferorder_bill",$data);
                    echo "OK";
                    return;
                }
            }
        }
        echo "false";
    }

    public function get_products(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '' ;
        $KhoID = isset($_POST['KhoID']) ? mysql_real_escape_string($_POST['KhoID']) : 0 ;
        $CategoriesID = $this->user->UserType==2 ? " and a.CategoriesID like '%\"62\"%'" : '' ;
        $CategoriesID = $this->user->UserType==1 ? " and a.CategoriesID not like '%\"62\"%'" : $CategoriesID ;
        $CategoriesID = $this->user->UserType==2 && isset($_POST['getall']) ? "" : $CategoriesID ;
        if($Title!='' && $Title!='*'){ 
            if($KhoID>0){
                $result = $this->db->query("select a.*,sum(b.Available) as Available from ttp_report_products a,ttp_report_inventory b where b.Available>=0 and b.LastEdited=1 and b.WarehouseID=$KhoID and a.ID=b.ProductsID and (a.Title like '%$Title%' or a.MaSP like '$Title') $CategoriesID group by a.ID")->result();
            }else{
                $result = $this->db->query("select a.* from ttp_report_products a where (a.Title like '%$Title%' or a.MaSP like '$Title') $CategoriesID")->result();
            }
        }else{
            if($KhoID>0){
                $result = $this->db->query("select a.*,sum(b.Available) as Available from ttp_report_products a,ttp_report_inventory b where b.Available>=0 and b.LastEdited=1 and b.WarehouseID=$KhoID and a.ID=b.ProductsID $CategoriesID group by a.ID")->result();
            }else{
                $result = $this->db->query("select a.* from ttp_report_products a where 1=1 $CategoriesID")->result();
            }
        }
        if(count($result)>0){
            $colsavilable = $KhoID>0 ? "<th>Số lượng còn</th>" : '' ;
            $str = "<div class='tools_search_products'>
                        <span>Tìm kiếm sản phẩm: </span>
                        <span><input type='text' placeholder='Nhập mã SKU hoặc tên sản phẩm' onchange='input_search_products(this)' /></span>
                    </div>
                    <table><tr><th></th><th>Mã Sản phẩm</th><th>Sản phẩm</th>$colsavilable
                    <th>
                        <select class='select_products' onchange='fillter_categories(this)'><option value='0'>-- Tất cả ngành hàng --</option>__</select>
                    </th></tr>";
            $categories = $this->db->query("select * from ttp_report_categories")->result();
            $arr_categories = array();
            if(count($categories)){
                foreach($categories as $row){
                    $arr_categories[$row->ID] = $row->Title;
                }
            }
            $option = array();
            foreach($result as $row){
                $row->CategoriesID = json_decode($row->CategoriesID,true);
                $row->CategoriesID = is_array($row->CategoriesID) ? $row->CategoriesID : array() ;
                $class='';
                $current_categories = array();
                if(count($row->CategoriesID)){
                    foreach($row->CategoriesID as $item){
                        if(isset($arr_categories[$item])){
                            $current_categories[] = $arr_categories[$item];
                            if(!isset($option[$item]))
                            $option[$item] = "<option value='".$item."'>".$arr_categories[$item]."</option>";
                        }
                        $class.=" categories_$item";
                    }
                }
                $str.="<tr class='trcategories $class'>";
                $str.="<td style='width:30px'><input type='checkbox' class='selected_products' data-id='$row->ID' data-donvi='$row->Donvi' data-code='$row->MaSP' data-name='$row->Title' /></td>";
                $str.="<td style='width: 130px;'>".$row->MaSP."</td>";
                $str.="<td>".$row->Title."</td>";
                if($KhoID>0){
                    $str.=$row->Available>0 ? "<td style='width: 100px;'>".number_format($row->Available)."</td>" : "<td>## Hết Hàng ##</td>" ;
                }
                $str.="<td style='width:170px'>".implode(',',$current_categories)."</td>";
                $str.="</tr>";
            }
            $option = implode('',$option);
            echo str_replace("__",$option,$str);
            echo "</table>";
            echo "<div class='fixedtools'><a class='btn btn-danger btn-box-inner' onclick='add_products()'><i class='fa fa-reply-all'></i> Đưa vào đơn hàng</a></div>";
            return;
        }
        echo "false";
    }

    public function get_shipment_by_productsID($id=0,$warehouse=0){
        $result = $this->db->query("select a.Available,b.ShipmentCode,b.ID from ttp_report_inventory a,ttp_report_shipment b where a.ProductsID=$id and a.WarehouseID=$warehouse and a.LastEdited=1 and a.Available>0 and a.ShipmentID=b.ID")->result();
        if(count($result)>0){
            echo "<option value='0'>Chọn lô xuất hàng</option>";
            foreach($result as $row){
                echo "<option value='$row->ID'>$row->ShipmentCode</option>";
            }
        }else{
            echo "<option value='0'>## Hết hàng ##</option>";
        }
    }

    public function load_shipment_by_products($id=0){
        if($id>0){
            $shipment = $this->uri->segment(6);
            echo "<option value='0'>-- Chọn lô --</option>";
            $result = $this->db->query("select ID,ShipmentCode from ttp_report_shipment where ProductsID=$id")->result();
            if(count($result)>0){
                foreach($result as $row){
                    $selected= $row->ID==$shipment ? "selected='selected'" : '' ;
                    echo "<option value='$row->ID' $selected>Lô $row->ShipmentCode</option>";
                }
            }
            echo "<option value='add'>-- Tạo mới --</option>";
        }else{
            echo "False";
        }
    }

    public function get_available_by_shipment($id=0,$warehouse=0,$shipment=0){
        $result = $this->db->query("select a.Available from ttp_report_inventory a where a.ProductsID=$id and a.WarehouseID=$warehouse and a.ShipmentID=$shipment and a.LastEdited=1 and a.Available>0")->row();
        echo $result ? number_format($result->Available) : "0" ;
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $result = $this->db->query("select a.*,b.Manager as SenderManager,c.Manager as ReciverManager from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c where a.WarehouseReciver=c.ID and a.WarehouseSender=b.ID and a.ID=$id")->row();
        if($result){
            $arr_sender = json_decode($result->SenderManager,true);
            $arr_reciver = json_decode($result->ReciverManager,true);
            if($this->user->IsAdmin==0){
                if($result->Status==0 || $result->Status==1){
                    if(!in_array($this->user->ID,$arr_sender)){redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");}
                }elseif($result->Status==2){
                    if($this->user->UserType==8){
                        if(!in_array($this->user->ID,$arr_sender)){redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");}
                    }
                }
            }
            $this->template->add_title('Warehouse Transfer Edit | Warehouse Report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_transferproducts/',
                'data'     => $result
            );
            $view = 'admin/warehouse_inventory_transferproducts_edit' ;
            if($result->Status==2){
                if($result->UserID!=$this->user->ID){
                    redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
                }
                $view = 'admin/warehouse_inventory_transferproducts_edit_step2' ;
            }
            if($result->Status==3 || $result->Status==4){
                $view = 'admin/warehouse_inventory_transferproducts_edit_step1' ;
            }
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/warehouse_inventory_transferproducts");
        }
    }

    public function box_add_shipment($id=0){
        $result = $this->db->query("select ID,Title from ttp_report_products where ID=$id")->row();
        if($result){
            $this->load->view("warehouse_inventory_shipment_add",array('data'=>$result));
        }else{
            echo "<p>Dữ liệu gửi lên không chính xác !.</p>";
        }
    }

    public function save_shipment(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : '' ;
        $ShipmentCode = isset($_POST['ShipmentCode']) ? $_POST['ShipmentCode'] : '' ;
        $DateProduction = isset($_POST['DateProduction']) ? $_POST['DateProduction'] : '' ;
        $DateExpiration = isset($_POST['DateExpiration']) ? $_POST['DateExpiration'] : '' ;
        if($ShipmentCode!='' && $DateProduction!='' && $DateExpiration!=''){
            $data = array(
                'UserID'        => $this->user->ID,
                'ShipmentCode'  => $ShipmentCode,
                'ProductsID'    => $ID,
                'DateProduction'=> $DateProduction,
                'DateExpiration'=> $DateExpiration,
                'Created'       => date('Y-m-d H:i:s')
            );
            $this->db->insert("ttp_report_shipment",$data);
        }
    }

    public function setfillter(){
        $arr_fieldname = array(0=>"a.WarehouseSender",1=>"a.WarehouseReciver",2=>"a.OrderCode");
        $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
        $FieldName = isset($_POST['FieldName']) ? $_POST['FieldName'] : array() ;
        $FieldOparation = isset($_POST['FieldOparation']) ? $_POST['FieldOparation'] : array() ;
        $FieldText = isset($_POST['FieldText']) ? $_POST['FieldText'] : array() ;
        $str = array();
        if(count($FieldName)>0){
            $i=0;
            $arrtemp = array();
            foreach ($FieldName as $key => $value) {
                if(!in_array($value,$arrtemp)){
                    if(isset($arr_fieldname[$value]) && isset($FieldOparation[$i])){
                        if(isset($arr_oparation[$FieldOparation[$i]]) && isset($FieldText[$i])){
                            if($arr_oparation[$FieldOparation[$i]]=='like'){
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '%".mysql_real_escape_string($FieldText[$i])."%'";
                            }else{
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '".mysql_real_escape_string($FieldText[$i])."'";
                            }
                            $i++;
                        }
                    }
                    $arrtemp[] = $value;
                }
            }
            if(count($str)>0){
                $sql = implode(' and ',$str);
                $this->session->set_userdata("fillter",$sql);
            }else{
                $this->session->set_userdata("fillter","");    
            }
        }else{
            $this->session->set_userdata("fillter","");
        }
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
        redirect($referer);
    }

}
?>