<?php 
class Warehouse_warehouse extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="warehouse_warehouse";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter_warehouse');
        $fillter = $fillter=='' ? "" : " where ".$fillter;
        $nav = $this->db->query("select count(1) as nav from ttp_report_warehouse $fillter")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_warehouse $fillter order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_warehouse/',
            'fill_data' => $this->session->userdata('fillter_warehouse'),
            'fillter'   => $fillter,
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_warehouse/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Production | Manager Report Tools');
		$this->template->write_view('content','admin/warehouse_warehouse_home',$data);
		$this->template->render();
	}

    public function transaction($WarehouseID=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Warehouse Transaction | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_warehouse_transaction',array('base_link' =>  base_url().ADMINPATH.'/report/warehouse_warehouse/transaction/','WarehouseID'=>$WarehouseID));
        $this->template->render();
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $result = $this->db->query("select * from ttp_report_warehouse where ID=$id")->row();
        if($result){
            $this->template->add_title('Warehouse Edit | Warehouse Report Tools');
            $this->template->write_view('content','admin/warehouse_warehouse_edit',array('data'=>$result,'base_link'=>base_url().ADMINPATH.'/report/warehouse_warehouse/'));
            $this->template->render();
        }
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Warehouse Add | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_warehouse_add',array('base_link' =>  base_url().ADMINPATH.'/report/warehouse_warehouse/'));
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $MaKho = isset($_POST['MaKho']) ? $_POST['MaKho'] : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Color = isset($_POST['Color']) ? $_POST['Color'] : '' ;
        $Manager = isset($_POST['Manager']) ? $_POST['Manager'] : array() ;
        $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
        $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : '' ;
        $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : '' ;
        $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : '' ;
        $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
        $Phone2 = isset($_POST['Phone2']) ? $_POST['Phone2'] : '' ;
        $data = array(
            'MaKho'         => $MaKho,
            'Title'         => $Title,
            'Manager'       => json_encode($Manager),
            'Address'       => $Address,
            'AreaID'        => $AreaID,
            'CityID'        => $CityID,
            'DistrictID'    => $DistrictID,
            'Phone1'        => $Phone1,
            'Phone2'        => $Phone2,
            'Color'         => $Color
        );
        $this->db->insert('ttp_report_warehouse',$data);
        $WarehouseID = $this->db->insert_id();
        $Position = isset($_POST['Position']) ? $_POST['Position'] : array() ;
        $Rack = isset($_POST['Rack']) ? $_POST['Rack'] : array() ;
        $Colum = isset($_POST['Colum']) ? $_POST['Colum'] : array() ;
        $Row = isset($_POST['Row']) ? $_POST['Row'] : array() ;
        if(count($Position)>0 && is_array($Position)){
            $arr_sql = array();
            foreach($Position as $key=>$value){
                $row_Rack = isset($Rack[$key]) ? $Rack[$key] : '' ;
                $row_Colum = isset($Colum[$key]) ? $Colum[$key] : '' ;
                $row_Row = isset($Row[$key]) ? $Row[$key] : '' ;
                $arr_sql[] = "('$value','$row_Rack','$row_Colum','$row_Row',$WarehouseID)";
            }
            $arr_sql = "insert into ttp_report_warehouse_position(Position,Rack,Col,Row,WarehouseID) values".implode(',', $arr_sql);
            $this->db->query($arr_sql);
        }
        redirect(base_url().ADMINPATH.'/report/warehouse_warehouse/');
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $MaKho = isset($_POST['MaKho']) ? $_POST['MaKho'] : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Color = isset($_POST['Color']) ? $_POST['Color'] : '' ;
        $Manager = isset($_POST['Manager']) ? $_POST['Manager'] : array() ;
        $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
        $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : '' ;
        $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : '' ;
        $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : '' ;
        $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
        $Phone2 = isset($_POST['Phone2']) ? $_POST['Phone2'] : '' ;
        $data = array(
            'MaKho'         => $MaKho,
            'Title'         => $Title,
            'Manager'       => json_encode($Manager),
            'Address'       => $Address,
            'AreaID'        => $AreaID,
            'CityID'        => $CityID,
            'DistrictID'    => $DistrictID,
            'Phone1'        => $Phone1,
            'Phone2'        => $Phone2,
            'Color'         => $Color
        );
        $this->db->where("ID",$ID);
        $this->db->update('ttp_report_warehouse',$data);
        if($ID>0){
            $Position = isset($_POST['Position']) ? $_POST['Position'] : array() ;
            $Rack = isset($_POST['Rack']) ? $_POST['Rack'] : array() ;
            $Colum = isset($_POST['Colum']) ? $_POST['Colum'] : array() ;
            $Row = isset($_POST['Row']) ? $_POST['Row'] : array() ;
            if(count($Position)>0 && is_array($Position)){
                $arr_sql = array();
                foreach($Position as $key=>$value){
                    if($value!=''){
                        $row_Rack = isset($Rack[$key]) ? $Rack[$key] : '' ;
                        $row_Colum = isset($Colum[$key]) ? $Colum[$key] : '' ;
                        $row_Row = isset($Row[$key]) ? $Row[$key] : '' ;
                        $arr_sql[] = "('$value','$row_Rack','$row_Colum','$row_Row',$ID)";
                    }
                }
                if(count($arr_sql)>0){
                    $arr_sql = "insert into ttp_report_warehouse_position(Position,Rack,Col,Row,WarehouseID) values".implode(',', $arr_sql);
                    $this->db->query($arr_sql);
                }
            }
        }
        redirect(base_url().ADMINPATH.'/report/warehouse_warehouse/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $this->db->query("delete from ttp_report_warehouse where ID=$id");
        $this->db->query("delete from ttp_report_warehouse_position where WarehouseID=$id");
        redirect(base_url().ADMINPATH.'/report/warehouse_warehouse/');
    }

    public function delete_position(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $list = isset($_POST['list']) ? $_POST['list'] : '' ;
        $list = explode('|', $list);
        if(count($list)>0){
            foreach($list as $key=>$value){
                if(!is_numeric($value)){
                    $list[$key] = 0;
                }
            }
            $list = implode(',',$list);
            $this->db->query("delete from ttp_report_warehouse_position where ID in($list)");
            echo "OK";
        }else{
            echo "False";
        }
    }

    public function edit_position($id=0){
        $result = $this->db->query("select * from ttp_report_warehouse_position where ID=$id")->row();
        $this->load->view("warehouse_position_edit",array('data'=>$result));
    }

    public function update_position(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $check = $this->db->query("select WarehouseID from ttp_report_warehouse_position where ID=$ID")->row();
        if($check){
            $Position = isset($_POST['Position']) ? $_POST['Position'] : 0 ;
            $Rack = isset($_POST['Rack']) ? $_POST['Rack'] : 0 ;
            $Colum = isset($_POST['Colum']) ? $_POST['Colum'] : 0 ;
            $Row = isset($_POST['Row']) ? $_POST['Row'] : 0 ;
            $data = array(
                'Position' => $Position,
                'Rack'  => $Rack,
                'Col'   => $Colum,
                'Row'   => $Row
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_warehouse_position",$data);
            redirect(ADMINPATH.'/report/warehouse_warehouse/edit/'.$check->WarehouseID);
        }
    }

    public function load_fillter_by_type_and_field(){
        $field = isset($_POST['FieldName']) ? $_POST['FieldName'] : '' ;
        if($field=='khuvuc'){
            $result = $this->db->query("select * from ttp_report_area order by Title ASC")->result();
            if(count($result)>0){
                echo "<select name='FieldText[]'>";
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->Title</option>";
                }
                echo "</select>";
            }
        }elseif($field=='thanhpho'){
            $result = $this->db->query("select * from ttp_report_city order by Title ASC")->result();
            if(count($result)>0){
                echo "<select name='FieldText[]'>";
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->Title</option>";
                }
                echo "</select>";
            }
        }else{
            echo '<input type="text" name="FieldText[]" id="textsearch" />';
        }
    }

    public function setfillter(){
        $arr_fieldname = array(0=>"AreaID",1=>"CityID",2=>"MaKho",3=>"Manager",4=>"Phone1");
        $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
        $FieldName = isset($_POST['FieldName']) ? $_POST['FieldName'] : array() ;
        $FieldOparation = isset($_POST['FieldOparation']) ? $_POST['FieldOparation'] : array() ;
        $FieldText = isset($_POST['FieldText']) ? $_POST['FieldText'] : array() ;
        $str = array();
        if(count($FieldName)>0){
            $i=0;
            $arrtemp = array();
            foreach ($FieldName as $key => $value) {
                if(!in_array($value,$arrtemp)){
                    if(isset($arr_fieldname[$value]) && isset($FieldOparation[$i])){
                        if(isset($arr_oparation[$FieldOparation[$i]]) && isset($FieldText[$i])){
                            if($arr_oparation[$FieldOparation[$i]]=='like'){
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '%".mysql_real_escape_string($FieldText[$i])."%'";
                            }else{
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '".mysql_real_escape_string($FieldText[$i])."'";
                            }
                            $i++;
                        }
                    }
                    $arrtemp[] = $value;
                }
            }
            if(count($str)>0){
                $sql = implode(' and ',$str);
                $this->session->set_userdata("fillter_warehouse",$sql);
            }else{
                $this->session->set_userdata("fillter_warehouse","");    
            }
        }else{
            $this->session->set_userdata("fillter_warehouse","");
        }
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
        redirect($referer);
    }
}