<?php 
class Import extends Admin_Controller { 
 
    public $user;
    public $classname="import";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/import_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;
    
    public function index(){
       
//        $this->load->helper('Bussiness\Order\order_helper');
//        $res = OrderStatus();
//        foreach($res as $item){
//            echo $item."<br>";
//        }
//        exit();
        
//        $this->load->model('conf_define', 'cfg');
//        $status = $this->cfg->get_order_status('status','order');
//        foreach ($status as $item){
//            echo $item->code."-".$item->name."<br>";
//        }
//        //exit();
//        
//        $type = $this->cfg->get_order_status('type','order');
//        foreach ($type as $item){
//            echo $item->code."-".$item->name."<br>";
//        }
//        exit();
//        
        $this->template->add_title("Report Tools");
        $this->template->write_view('content','admin/import_content');
        $this->template->render();
    }

    public function set_day(){
        $group1 = isset($_POST['group1']) ? $_POST['group1'] : "" ;
        if($group1!=''){
            $group1 = explode("-",$group1);
            $startday = isset($group1[0]) ? trim($group1[0]) : '';
            $stopday = isset($group1[1]) ? trim($group1[1]) : '';
            if($startday!='' && $stopday!=''){
                $startday = explode('/',$startday);
                $stopday = explode('/',$stopday);
                if(count($startday)==3 && count($stopday)==3){
                    $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
                    $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
                    $timestart = strtotime($startday);
                    $timestop = strtotime($stopday);
                    if($timestart>$timestop){
                        echo "fasle";
                        return;
                    }
                    $this->session->set_userdata("import_startday",$startday);
                    $this->session->set_userdata("import_stopday",$stopday);
                }
            }
        }
        echo 'OK';
    }

    public function set_day_warehouse(){
        $group1 = isset($_POST['group1']) ? $_POST['group1'] : "" ;
        if($group1!=''){
            $group1 = explode("-",$group1);
            $startday = isset($group1[0]) ? trim($group1[0]) : '';
            $stopday = isset($group1[1]) ? trim($group1[1]) : '';
            if($startday!='' && $stopday!=''){
                $startday = explode('/',$startday);
                $stopday = explode('/',$stopday);
                if(count($startday)==3 && count($stopday)==3){
                    $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
                    $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
                    $timestart = strtotime($startday);
                    $timestop = strtotime($stopday);
                    if($timestart>$timestop){
                        echo "fasle";
                        return;
                    }
                    $this->session->set_userdata("import_warehouse_startday",$startday);
                    $this->session->set_userdata("import_warehouse_stopday",$stopday);
                }
            }
        }
        echo 'OK';
    }

    public function get_news_order(){
        $result = $this->db->query("select a.MaDH,b.UserName,a.Ngaydathang from ttp_report_order a,ttp_user b where a.UserID=b.ID order by a.ID DESC limit 0,1")->row();
        if($result){
            $remain = $this->lib->get_times_remains($result->Ngaydathang);
            echo "<label class='control-label'>$result->MaDH nhân viên <b>$result->UserName</b> vừa tạo lúc $remain</label>";
        }
    }

    public function get_current_order_department(){
        $department = isset($_POST['department']) ? $_POST['department'] : 0 ;
        $maxid = isset($_POST['max']) ? $_POST['max'] : 0 ;
        $maxid1 = isset($_POST['max1']) ? $_POST['max1'] : 0 ;
        switch ($department) {
            case 2:
                $department=3;
                break;
            case 3:
                $department=5;
                break;
            case 4:
                $department=9;
                break;
        }
        $curdate = date('Y-m-d',time());
        if($department==1){
            $max = $this->db->query("select count(1) as SL from ttp_report_order where Status = 4 and UserID=".$this->user->ID)->row();
            $slmax = $max ? $max->SL : 0 ;
            $max1 = $this->db->query("select count(1) as SL from ttp_report_order where Status = 6 and UserID=".$this->user->ID)->row();
            $slmax1 = $max1 ? $max1->SL : 0 ;
            if($slmax==$maxid && $slmax1==$maxid1){
                echo "false";
            }else{
                $noti = 0;
                $noti = $slmax-$maxid + $slmax1-$maxid1;
                if($noti<0){
                    echo "false";
                    return;
                }
                $data = array('noti'=>$noti,'content'=>array());
                $limit = $noti>3 ? 3 : $noti ;
                $result = $this->db->query("select a.ID,a.MaDH,b.UserName,b.Thumb,a.Status from ttp_report_order a,ttp_user b where Status in(4,6) and a.UserID=b.ID and a.UserID=".$this->user->ID." and date(Ngaydathang)='$curdate' order by a.HistoryEdited DESC limit 0,$limit")->result();
                if(count($result)){
                    $arr = array(4=>'vừa bị trả về từ kho .',6=>'vừa bị trả về từ kế toán');
                    foreach($result as $row){
                        $data['content'][] = array(
                            'title'=>"Đơn hàng  $row->MaDH ".$arr[$row->Status],
                            'user' =>$row->UserName,
                            'image'=>$row->Thumb!='' ? $row->Thumb : base_url().'public/admin/images/user.png'
                        );
                    }
                }
                echo json_encode($data);
            }
        }else{
            $bonus='';
            if($this->user->UserType==3){
                $bonus = $this->user->Channel==0 && $this->user->UserType==3 ? " and OrderType=0" : "" ;
                $bonus = $this->user->Channel==1 && $this->user->UserType==3 ? " and OrderType in(1,2,4,5)" : $bonus ;
                $bonus = $this->user->Channel==2 && $this->user->UserType==3 ? " and OrderType=3" : $bonus ;
            }
            if($this->user->UserType==4){
                $bonus = ($this->user->Channel==0 || $this->user->Channel==2) && $this->user->UserType==4 ? " and OrderType in(0,3)" : "" ;
                $bonus = $this->user->Channel==1 && $this->user->UserType==4 ? " and OrderType in(1,2,4,5)" : $bonus ;
            }

            $max = $this->db->query("select count(1) as SL from ttp_report_order where Status = $department and date(Ngaydathang)='$curdate' $bonus")->row();
            $slmax = $max ? $max->SL : 0 ;
            if($maxid==$slmax){
                echo "false";
            }else{
                $data = array('noti'=>$slmax,'content'=>array());
                $slmax = ($slmax-$maxid)>3 ? 3 : $slmax-$maxid ;
                $result = $this->db->query("select a.ID,a.MaDH,b.UserName,b.Thumb from ttp_report_order a,ttp_user b where Status=$department and a.UserID=b.ID and date(Ngaydathang)='$curdate' $bonus order by a.HistoryEdited DESC limit 0,$slmax")->result();
                if(count($result)){
                    foreach($result as $row){
                        $data['content'][] = array(
                            'title'=>"Đơn hàng $row->MaDH vừa được chuyển sang bộ phận của bạn để xử lý",
                            'user' =>$row->UserName,
                            'image'=>$row->Thumb!='' ? $row->Thumb : base_url().'public/admin/images/user.png'
                        );
                    }
                }
                echo json_encode($data);
            }
        }
    }

    public function lapphieuxuatkho($id=0){
        if($this->user->IsAdmin==1 || $this->user->UserType==3 || $this->user->UserType==7){
            if($id>0){
                $onoff = @file_get_contents('log/status/onoff.txt');
                if($onoff=='' || $onoff==0){
                    $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,d.MaKho as KhoTitle from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID")->row();
                }else{
                    $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,d.MaKho as KhoTitle from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID and Accept=2")->row();
                }
                if($result){
                    $this->template->add_title('Created Bill | Import Tools');
                    $data = array(
                        'base_link' => base_url().ADMINPATH.'/report/import/',
                        'data'      => $result
                    );
                    $view = $result->OrderType==1 || $result->OrderType==2 ? 'admin/import_order_created_bill_sale' : 'admin/import_order_created_bill' ;
                    $this->template->write_view('content',$view,$data);
                    $this->template->render();
                    return;
                }
            }
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/import_order' ;
            redirect($referer);
        }else{
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/import_order' ;
            redirect($referer);
        }
    }

    public function checked_fromstaff($id=0){
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("update ttp_report_order set Accept=1 where ID=$id");
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function lapphieuxuatkho_percent($id=0){
        if($this->user->IsAdmin==1 || $this->user->UserType==3 || $this->user->UserType==7){
            if($id>0){
                $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,d.MaKho as KhoTitle from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID")->row();
                if($result){
                    $this->template->add_title('Created Bill | Import Tools');
                    $data = array(
                        'base_link' => base_url().ADMINPATH.'/report/import/',
                        'data'      => $result
                    );
                    $view = $result->OrderType==1 || $result->OrderType==2 ? 'admin/import_order_created_bill_percent_sale' : 'admin/import_order_created_bill_percent' ;
                    $this->template->write_view('content',$view,$data);
                    $this->template->render();
                    return;
                }
            }
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/import_order' ;
            redirect($referer);
        }else{
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/import_order' ;
            redirect($referer);
        }
    }

    public function printorder($id=0){
        if($this->user->IsAdmin==1 || $this->user->UserType==3 || $this->user->UserType==1 || $this->user->UserType==5 || $this->user->UserType==7){
            if($id>0){
                $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,d.MaKho as KhoTitle,e.Title as Source,f.Title as Kenh,g.Title as Thanhpho,h.Title as Quanhuyen,i.Title as Area from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d,ttp_report_source e,ttp_report_saleschannel f,ttp_report_city g,ttp_report_district h,ttp_report_area i where g.AreaID=i.ID and a.DistrictID=h.ID and a.CityID=g.ID and a.KenhbanhangID=f.ID and a.SourceID=e.ID and a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID")->row();
                if($result){
                    $this->template->add_title('Created Bill | Import Tools');
                    $data = array(
                        'base_link' => base_url().ADMINPATH.'/report/import/',
                        'data'      => $result
                    );
                    $view = 'admin/import_order_printorder' ;
                    $this->template->write_view('content',$view,$data);
                    $this->template->render();
                    return;
                }
            }
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/import_order' ;
            redirect($referer);
        }else{
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/import_order' ;
            redirect($referer);
        }
    }    

    public function get_next_warehouse(){
        $OrderID = isset($_POST['OrderID']) ? $_POST['OrderID'] : 0 ;
        $hinhthucxuatkho = isset($_POST['Hinhthucxuatkho']) ? $_POST['Hinhthucxuatkho'] : '' ;
        if($OrderID>0){
            $order = $this->db->query("select * from ttp_report_order where ID=$OrderID")->row();
            if($order){
                $export = $this->db->query("select * from ttp_report_export_warehouse where OrderID=$OrderID")->row();
                $thismonth = date('m',time());
                $thisyear = date('Y',time());
                $type = $order->OrderType==0 ? ' and TypeExport=0' : '' ;
                $type = $order->OrderType==1 ? ' and TypeExport=1' : $type ;
                $type = $order->OrderType==2 ? ' and TypeExport=2' : $type ;
                $type = $order->OrderType==3 ? ' and TypeExport=3' : $type ;
                $type = $order->OrderType==4 ? ' and TypeExport=4' : $type ;
                $type = $order->OrderType==5 ? ' and TypeExport=5' : $type ;
                $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=$hinhthucxuatkho $type")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
                if($order->OrderType==0){
                    if($hinhthucxuatkho==2){
                        $thisyear = date('Y',time());
                        $hinhthucxuatkho = $hinhthucxuatkho==2 ? "TD" : $hinhthucxuatkho ;
                        if($thisyear=='2016' && $thismonth=='03'){
                            $max = $max+46;
                        }
                        $max = "XKLO".$thisyear.$thismonth.".".str_pad($max, 5, '0', STR_PAD_LEFT);
                    }else{
                        $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                        $max = "BHOL".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                    }
                }
                if($order->OrderType==1 || $order->OrderType==2){
                    $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                    if($order->OrderType==1){
                        $max = "BHGT".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                    }
                    if($order->OrderType==2){
                        $max = "BHMT".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                    }
                }
                if($order->OrderType==3){
                    $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                    $max = "BHGS".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                }
                
                if($order->OrderType==4){
                    $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                    $max = "BHTD".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                }

                if($order->OrderType==5){
                    $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                    $max = "BHNB".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                }
               echo $export ? $export->MaXK : $max ;
           }
        }
    }

    public function print_export_warehouse(){
        if($this->user->IsAdmin==1 || $this->user->UserType==3 || $this->user->UserType==2){
            $orderid = isset($_POST['OrderID']) ? $_POST['OrderID'] : 0 ;
            if($orderid>0){
                $TKNO = isset($_POST['TKNO']) ? $_POST['TKNO'] : '' ;
                $TKCO = isset($_POST['TKCO']) ? $_POST['TKCO'] : '' ;
                $KPP = isset($_POST['KPP']) ? $_POST['KPP'] : '' ;
                $Lydo = isset($_POST['Lydo']) ? $_POST['Lydo'] : '' ;
                $KhoID = isset($_POST['KhoID']) ? $_POST['KhoID'] : '' ;
                $hinhthucxuatkho = isset($_POST['hinhthucxuatkho']) ? $_POST['hinhthucxuatkho'] : '' ;
                $data = array(
                    'Lydoxuatkho'=>$Lydo,
                    'TKNO'       =>$TKNO,
                    'TKCO'       =>$TKCO,
                    'KPP'        =>$KPP,
                    'KhoID'      =>$KhoID,
                    'Hinhthucxuatkho'=>$hinhthucxuatkho
                );
                $order = $this->db->query("select * from ttp_report_export_warehouse where OrderID=$orderid")->row();
                if($order){
                    $this->db->where("ID",$order->ID);
                    $this->db->update("ttp_report_export_warehouse",$data);
                    echo "OK";
                    return;
                }else{
                    $order = $this->db->query("select * from ttp_report_order where ID=$orderid")->row();
                    $thismonth = date('m',time());
                    $thisyear = date('Y',time());
                    $type = $order->OrderType==0 ? ' and TypeExport=0' : '' ;
                    $type = $order->OrderType==1 ? ' and TypeExport=1' : $type ;
                    $type = $order->OrderType==2 ? ' and TypeExport=2' : $type ;
                    $type = $order->OrderType==3 ? ' and TypeExport=3' : $type ;
                    $type = $order->OrderType==4 ? ' and TypeExport=4' : $type ;
                    $type = $order->OrderType==5 ? ' and TypeExport=5' : $type ;
                    $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=$hinhthucxuatkho $type")->row();
                    $max = $max ? $max->max + 1 : 1 ;
                    $thisyear = date('y',time());
                    if($order->OrderType==0){
                        if($hinhthucxuatkho==2){
                            $thisyear = date('Y',time());
                            $hinhthucxuatkho = $hinhthucxuatkho==2 ? "TD" : $hinhthucxuatkho ;
                            if($thisyear=='2016' && $thismonth=='03'){
                                $max = $max+46;
                            }
                            $max = "XKLO".$thisyear.$thismonth.".".str_pad($max, 5, '0', STR_PAD_LEFT);
                        }else{
                            $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                            $max = "BHOL".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                        }
                        $data['TypeExport']=0;
                    }
                    if($order->OrderType==1 || $order->OrderType==2){
                        $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                        if($order->OrderType==1){
                            $max = "BHGT".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                            $data['TypeExport']=1;
                        }
                        if($order->OrderType==2){
                            $max = "BHMT".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                            $data['TypeExport']=2;
                        }
                    }
                    if($order->OrderType==3){
                        $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                        $max = "BHGS".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                        $data['TypeExport']=3;
                    }
                    if($order->OrderType==4){
                        $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                        $max = "BHTD".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                        $data['TypeExport']=4;
                    }
                    if($order->OrderType==5){
                        $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                        $max = "BHNB".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                        $data['TypeExport']=5;
                    }
                    $data['OrderID']    = $orderid;
                    $data['MaXK']       = $max;
                    $data['Ngayxuatkho']= date("Y-m-d H:i:s",time());
                    $data['UserID']     = $this->user->ID;
                    $this->db->insert("ttp_report_export_warehouse",$data);
                    echo "OK";
                    return;
                }
            }
        }
        echo "false";
    }

    public function data_export(){
        if(isset($_GET['transport'])){
            $transport = $_GET['transport'];
            $startday = $this->session->userdata("import_startday");
            $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
            $stopday = $this->session->userdata("import_stopday");
            $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
            $fillter = $this->session->userdata('fillter');
            $fillter = $fillter=='' ? "" : " and ".$fillter;
            $statusbonus = "";
            $statusbonus = " and a.Status=7";
            $statusbonus = $this->user->Channel==1 ? $statusbonus." and a.OrderType in(1,2,4,5)" : $statusbonus." and a.OrderType in(0,3)";
            $orderby = "a.Ngaydathang ASC";
            if($transport=="ghn"){
                $result = $this->db->query("select DISTINCT a.ID,a.Payment,a.AddressOrder,a.Total,a.Chiphi,a.Chietkhau,a.Note,f.FirstName,f.LastName,c.Name,c.Phone1,d.GHN as Thanhpho,b.GHN as Quanhuyen,k.Title as Tensanpham,h.Amount,l.MaXK from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_user f,ttp_report_orderdetails h,ttp_report_products k,ttp_report_export_warehouse l where a.ID=l.OrderID and h.ProductsID=k.ID and h.OrderID=a.ID and a.UserID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' $fillter $statusbonus and l.TransportStatus=0 order by $orderby")->result();
                if(count($result)>0){
                    $this->export_template_transport_ghn($result);
                }else{
                    echo "Can't export data because this data is empty !.";
                }
            }
            if($transport=="gold"){
                $result = $this->db->query("select DISTINCT a.ID,a.Payment,a.Total as TotalOrder,a.Chietkhau as ChietkhauOrder,a.AddressOrder,h.Total,a.Chiphi,h.Price,c.Name,c.Phone1,d.GoldTimes as Thanhpho,b.GoldTimes as Quanhuyen,k.Title as Tensanpham,k.Donvi,h.Amount,f.MaXK from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_export_warehouse f,ttp_report_orderdetails h,ttp_report_products k where a.ID=f.OrderID and h.ProductsID=k.ID and h.OrderID=a.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' $fillter $statusbonus and f.TransportStatus=0 order by $orderby")->result();
                if(count($result)>0){
                    $this->export_template_transport_gold($result);
                }else{
                    echo "Can't export data because this data is empty !. ";
                }
            }
        }
    }

    public function viewpage_transport_ghn($data){
        if(count($data)>0){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            echo "<table>";
            echo "<tr>
                <td>Nguoi_Gui</td>
                <td>Dien_Thoai_Nguoi_Gui</td>
                <td>Dia_Chi_Lay_Hang</td>
                <td>Tinh_Thanh_Gui</td>
                <td>Quan_Gui</td>
                <td>Nguoi_Nhan</td>
                <td>Dien_Thoai</td>
                <td>Dia_Chi</td>
                <td>Tinh_Thanh</td>
                <td>Quan</td>
                <td>Dich_Vu</td>
                <td>Dich_Vu_Khai_Gia</td>
                <td>Hinh_Thuc_Thanh_Toan</td>
                <td>Ghi_Chu</td>
                <td>COD</td>
                <td>Tem_Niem_Phong</td>
                <td>Ma_Shop</td>
                <td>Khoi_Luong_(gr)</td>
                <td>Dai_(cm)</td>
                <td>Rong_(cm)</td>
                <td>Cao_(cm)</td>
                <td>KL_quy_doi_(gr)</td>
                <td>Noi_Dung_Hang</td>
                </tr>";
            $arr_order = array();
            foreach($data as $row){
                if(!isset($arr_order[$row->ID])){
                    $tongtien = $row->Total+$row->Chiphi-$row->Chietkhau;
                    $dichvukhaigia = $this->dichvukhaigia($tongtien);
                    $hinhthucthanhtoan = $this->paymenttype($row->Payment);
                    $arr_order[$row->ID] = array(
                        'A'=>$row->FirstName." ".$row->LastName,
                        'B'=>'1900555552',
                        'C'=>'246/9 Bình Quới, Phường 28, Quận Bình Thạnh, Thành phố Hồ Chí Minh',
                        'D'=>'Hồ_Chí_Minh_8',
                        'E'=>'0216 - Quận Bình Thạnh',
                        'F'=>$row->Name,
                        'G'=>$row->Phone1,
                        'H'=>$row->AddressOrder,
                        'I'=>$row->Thanhpho,
                        'J'=>$row->Quanhuyen,
                        'K'=>'',
                        'L'=>$dichvukhaigia,
                        'M'=>'2 - Người nhận trả tiền',
                        'N'=>'',
                        'O'=>$tongtien,
                        'P'=>'',
                        'Q'=>'',
                        'R'=>'',
                        'S'=>'',
                        'T'=>'',
                        'U'=>'',
                        'V'=>'',
                        'W'=>"$row->Amount $row->Tensanpham",
                    );
                    $arr[] = "($row->ID)";
                }else{
                    $arr_order[$row->ID]['W'] .= "<br>$row->Amount $row->Tensanpham";
                }
            }
            if(count($arr_order)>0){
                foreach($arr_order as $row){
                    echo "<td>".implode("</td><td>",$row)."</td>";
                }
            }
            echo "<table>";
            $this->save_transport_export($arr);
        }
    }

    public function viewpage_transport_gold($data){
        if(count($data)>0){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $arr_chiphi = array();
            $arr = array();
            echo "<table>";
            echo "<tr><td></td><td colspan='7'>Thông tin sản phẩm</td><td conspan='2'>Thông tin dịch vụ</td><td>Thông tin nhận hàng</td></tr>";
            $i=1;
            foreach($data as $row){
                if(!isset($arr[$row->ID])){
                    $arr[$row->ID] = "($row->ID)";
                }
                if($row->Chiphi>0){
                    if(!isset($arr_chiphi[$row->ID])){
                        $arr_chiphi[$row->ID] = array(
                            'Chiphi'    =>$row->Chiphi,
                            'Name'      =>$row->Name,
                            'Phone'     =>$row->Phone1,
                            'Address'   =>$row->AddressOrder,
                            'City'      =>$row->Thanhpho,
                            'Quanhuyen' =>$row->Quanhuyen,
                            'MaXK'      =>$row->MaXK
                        );
                    }
                }
                $hinhthucthanhtoan = $this->paymentgold($row->Payment);
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>$row->Tensanpham</td>";
                echo "<td></td>";
                echo "<td>$row->Price</td>";
                echo "<td>$row->Amount</td>";
                echo "<td>$row->Amount $row->Donvi $row->Tensanpham</td>";
                echo "<td>$row->MaXK</td>";
                echo "<td>$row->Total</td>";
                echo "<td>CODN_Chuyển phát nhanh</td>";
                echo "<td>$hinhthucthanhtoan</td>";
                echo "<td></td>";
                echo "<td>$row->Name</td>";
                echo "<td>$row->Phone1</td>";
                echo "<td>$row->AddressOrder</td>";
                echo "<td>$row->Thanhpho</td>";
                echo "<td>$row->Quanhuyen</td>";
                echo "</tr>";
                $i++;
            }
            if(count($arr_chiphi)>0){
                foreach($arr_chiphi as $row){
                    echo "<tr>";
                    echo "<td>$i</td>";
                    echo "<td>Dịch vụ vận chuyển hàng</td>";
                    echo "<td></td>";
                    echo "<td>".$row['Chiphi']."</td>";
                    echo "<td>1</td>";
                    echo "<td>Dịch vụ vận chuyển hàng</td>";
                    echo "<td>".$row['MaXK']."</td>";
                    echo "<td>".$row['Chiphi']."</td>";
                    echo "<td>CODN_Chuyển phát nhanh</td>";
                    echo "<td>2 - Người nhận trả tiền</td>";
                    echo "<td></td>";
                    echo "<td>".$row['Name']."</td>";
                    echo "<td>".$row['Phone']."</td>";
                    echo "<td>".$row['Address']."</td>";
                    echo "<td>".$row['Thanhpho']."</td>";
                    echo "<td>".$row['Quanhuyen']."</td>";
                    echo "</tr>";
                    $i++;
                }
            }
            echo "<table>";
            $this->save_transport_export($arr);
        }
    }

    public function export_template_transport_gold($data){
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');
        require_once './public/plugin/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', '')
                    ->setCellValue('B1', 'Thông tin sản phẩm')
                    ->setCellValue('I1', 'Thông tin dịch vụ')
                    ->setCellValue('L1', 'Thông tin nhận hàng');
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2', 'STT')
                    ->setCellValue('B2', 'Tên sản phẩm')
                    ->setCellValue('C2', 'Trọng lượng (Gram)')
                    ->setCellValue('D2', 'Giá trị sản phẩm')
                    ->setCellValue('E2', 'Số lượng')
                    ->setCellValue('F2', 'Mô tả sản phẩm')
                    ->setCellValue('G2', 'Mã đơn hàng từ đối tác')
                    ->setCellValue('H2', 'Số tiền thu hộ')
                    ->setCellValue('I2', 'Dịch vụ')
                    ->setCellValue('J2', 'Hình thức thanh toán')
                    ->setCellValue('K2', 'Dịch vụ cộng thêm')
                    ->setCellValue('L2', 'Người nhận')
                    ->setCellValue('M2', 'Điện thoại người nhận')
                    ->setCellValue('N2', 'Địa chỉ nhận hàng')
                    ->setCellValue('O2', 'Tỉnh / Thành phố nhận hàng')
                    ->setCellValue('P2', 'Quận / Huyện nhận hàng');
        $objPHPExcel->getActiveSheet()->getStyle("A1:P1")->getFill()->applyFromArray(array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'rgb' => "FEE4C5"
            )
        ));

        $objPHPExcel->getActiveSheet()->getStyle("A2:P2")->getFill()->applyFromArray(array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'rgb' => "FEE4C5"
            )
        ));
        $BStyle = array(
              'borders' => array(
                'outline' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
                )
              )
        );
        $objPHPExcel->getActiveSheet()->getStyle('B1:F1')->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->getStyle('I1:K1')->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->getStyle('L1:P1')->applyFromArray($BStyle);
        $BStyle = array(
              'borders' => array(
                'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
                )
              )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A2:P2')->applyFromArray($BStyle);
        $arr_chiphi = array();
        $i=3;
        $temp = 1;
        $arr = array();
        foreach($data as $row){
            if(!isset($arr[$row->ID])){
                $arr[$row->ID] = "$row->ID";
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$i, $row->TotalOrder-$row->ChietkhauOrder);
            }
            if($row->Chiphi>0){
                if(!isset($arr_chiphi[$row->ID])){
                    $arr_chiphi[$row->ID] = array(
                        'Chiphi'    =>$row->Chiphi,
                        'Name'      =>$row->Name,
                        'Phone'     =>$row->Phone1,
                        'Address'   =>$row->AddressOrder,
                        'City'      =>$row->Thanhpho,
                        'Quanhuyen' =>$row->Quanhuyen,
                        'MaXK'      =>$row->MaXK
                    );
                }
            }

            $hinhthucthanhtoan = $this->paymentgold($row->Payment);
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $temp)
                    ->setCellValue('B'.$i, $row->Tensanpham)
                    ->setCellValue('C'.$i, '')
                    ->setCellValue('D'.$i, $row->Price)
                    ->setCellValue('E'.$i, $row->Amount)
                    ->setCellValue('F'.$i, "$row->Amount $row->Donvi $row->Tensanpham")
                    ->setCellValue('G'.$i, $row->MaXK)
                    ->setCellValue('H'.$i, $row->Total)
                    ->setCellValue('I'.$i, 'CODN_Chuyển phát nhanh')
                    ->setCellValue('J'.$i, $hinhthucthanhtoan)
                    ->setCellValue('K'.$i, '')
                    ->setCellValue('L'.$i, $row->Name)
                    ->setCellValue('M'.$i, $row->Phone1)
                    ->setCellValue('N'.$i, $row->AddressOrder)
                    ->setCellValue('O'.$i, $row->Thanhpho)
                    ->setCellValue('P'.$i, $row->Quanhuyen);
            $i++;
            $temp++;
        }

        if(count($arr_chiphi)>0){
            foreach($arr_chiphi as $row){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $temp)
                    ->setCellValue('B'.$i, "Dịch vụ vận chuyển hàng")
                    ->setCellValue('C'.$i, '')
                    ->setCellValue('D'.$i, $row['Chiphi'])
                    ->setCellValue('E'.$i, 1)
                    ->setCellValue('F'.$i, "Dịch vụ vận chuyển hàng")
                    ->setCellValue('G'.$i, $row['MaXK'])
                    ->setCellValue('H'.$i, $row['Chiphi'])
                    ->setCellValue('I'.$i, 'CODN_Chuyển phát nhanh')
                    ->setCellValue('J'.$i, '2 - Người nhận trả tiền')
                    ->setCellValue('K'.$i, '')
                    ->setCellValue('L'.$i, $row['Name'])
                    ->setCellValue('M'.$i, $row['Phone'])
                    ->setCellValue('N'.$i, $row['Address'])
                    ->setCellValue('O'.$i, $row['Thanhpho'])
                    ->setCellValue('P'.$i, $row['Quanhuyen']);
                $i++;
                $temp++;
            }
        }
        $this->save_transport_export($arr);
        $objPHPExcel->getActiveSheet()->setTitle('GHN');
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="GoldTimes.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    public function export_template_transport_ghn($data){
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');
        require_once 'public/plugin/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Nguoi_Gui')
                    ->setCellValue('B1', 'Dien_Thoai_Nguoi_Gui')
                    ->setCellValue('C1', 'Dia_Chi_Lay_Hang')
                    ->setCellValue('D1', 'Tinh_Thanh_Gui')
                    ->setCellValue('E1', 'Quan_Gui')
                    ->setCellValue('F1', 'Nguoi_Nhan')
                    ->setCellValue('G1', 'Dien_Thoai')
                    ->setCellValue('H1', 'Dia_Chi')
                    ->setCellValue('I1', 'Tinh_Thanh')
                    ->setCellValue('J1', 'Quan')
                    ->setCellValue('K1', 'Dich_Vu')
                    ->setCellValue('L1', 'Dich_Vu_Khai_Gia')
                    ->setCellValue('M1', 'Hinh_Thuc_Thanh_Toan')
                    ->setCellValue('N1', 'Ghi_Chu')
                    ->setCellValue('O1', 'COD')
                    ->setCellValue('P1', 'Tem_Niem_Phong')
                    ->setCellValue('Q1', 'Ma_Shop')
                    ->setCellValue('R1', 'Khoi_Luong_(gr)')
                    ->setCellValue('S1', 'Dai_(cm)')
                    ->setCellValue('T1', 'Rong_(cm)')
                    ->setCellValue('U1', 'Cao_(cm)')
                    ->setCellValue('V1', 'KL_quy_doi_(gr)')
                    ->setCellValue('W1', 'Noi_Dung_Hang');
        $objPHPExcel->getActiveSheet()->getStyle("A1:W1")->getFill()->applyFromArray(array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'rgb' => "126D10"
            ),
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '0000FF')
            ),
            'borders' => array(
                'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
                )
              )
        ));
        $arr_order = array();
        $i=2;
        $arr = array();
        foreach($data as $row){
            if(!isset($arr_order[$row->ID])){
                $tongtien = $row->Total+$row->Chiphi-$row->Chietkhau;
                $dichvukhaigia = $this->dichvukhaigia($tongtien);
                $hinhthucthanhtoan = $this->paymenttype($row->Payment);
                $objPHPExcel->setActiveSheetIndex(0)
                         ->setCellValue('A'.$i, $row->FirstName." ".$row->LastName)
                        ->setCellValue('B'.$i, '1900555552')
                        ->setCellValue('C'.$i, '246/9 Bình Quới, Phường 28, Quận Bình Thạnh, Thành phố Hồ Chí Minh')
                        ->setCellValue('D'.$i, 'Hồ_Chí_Minh_8')
                        ->setCellValue('E'.$i, '0216 - Quận Bình Thạnh')
                        ->setCellValue('F'.$i, $row->Name)
                        ->setCellValue('G'.$i, $row->Phone1)
                        ->setCellValue('H'.$i, $row->AddressOrder)
                        ->setCellValue('I'.$i, $row->Thanhpho)
                        ->setCellValue('J'.$i, $row->Quanhuyen)
                        ->setCellValue('K'.$i, '')
                        ->setCellValue('L'.$i, $dichvukhaigia)
                        ->setCellValue('M'.$i, '2 - Người nhận trả tiền')
                        ->setCellValue('N'.$i, $row->Note)
                        ->setCellValue('O'.$i, $tongtien)
                        ->setCellValue('P'.$i, '')
                        ->setCellValue('Q'.$i, $row->MaXK)
                        ->setCellValue('R'.$i, '')
                        ->setCellValue('S'.$i, '')
                        ->setCellValue('T'.$i, '')
                        ->setCellValue('U'.$i, '')
                        ->setCellValue('V'.$i, '')
                        ->setCellValue('W'.$i, "$row->Amount $row->Tensanpham");
                $i++;
                $arr_order[$row->ID] = "$row->Amount $row->Tensanpham";
                $arr[] = "$row->ID";
            }else{
                $j = $i-1;
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$j, $arr_order[$row->ID]."\n$row->Amount $row->Tensanpham");
                $arr_order[$row->ID] .= "\n$row->Amount $row->Tensanpham";
            }
        }

        $this->save_transport_export($arr);
        $objPHPExcel->getActiveSheet()->setTitle('GHN');
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="GHN.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    public function export_by_resultdata(){
        $TypeExport = isset($_GET['TypeExport']) ? $_GET['TypeExport'] : 0 ;
        $Type = isset($_GET['Type']) ? (int)$_GET['Type'] : 0 ;
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        ini_set('memory_limit', '3500M');
        if($TypeExport==1){
            $this->export_data_orderpage($startday,$stopday);
        }
        if($TypeExport==2 && ($this->user->UserType==2 || $this->user->IsAdmin==1)){
            $this->export_report_import($startday,$stopday,2,$Type);
        }
        if($TypeExport==3 && ($this->user->UserType==2 || $this->user->IsAdmin==1)){
            $this->export_report_import($startday,$stopday,3,$Type);
        }
    }

    public function export_report_import($startday='',$stopday='',$type_export=2,$type=0){
        $sql = $this->session->userdata("sql_export");
        if($sql!=''){
            $result = $this->db->query($sql)->result();
            if(count($result)<5000){
                error_reporting(E_ALL);
                ini_set('display_errors', TRUE);
                ini_set('display_startup_errors', TRUE);
                if (PHP_SAPI == 'cli')
                    die('This example should only be run from a Web Browser');
                require_once 'public/plugin/PHPExcel.php';
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                             ->setLastModifiedBy("Maarten Balliauw")
                                             ->setTitle("Office 2007 XLSX Test Document")
                                             ->setSubject("Office 2007 XLSX Test Document")
                                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                             ->setKeywords("office 2007 openxml php")
                                             ->setCategory("Test result file");
                if($type_export==2){
                    switch ($type) {
                        case 0:
                            $type = "(TỪ NHÀ CUNG CẤP)";
                            break;
                        case 1:
                            $type = "(HÀNG BÁN TRẢ LẠI)";
                            break;
                        case 2:
                            $type = "(LƯU CHUYỂN NỘI BỘ)";;
                            break;
                        default:
                            $type="";
                            break;
                    }
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'BÁO CÁO NHẬP KHO HÀNG HÓA '.$type.' TỪ '.$startday.' ĐẾN '.$stopday);
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A2', 'SKU')
                            ->setCellValue('B2', 'Tên sản phẩm')
                            ->setCellValue('C2', 'Số lượng nhập')
                            ->setCellValue('D2', 'Nhập tại kho');
                }else{
                    switch ($type) {
                        case 0:
                            $type = "(BÁN HÀNG)";
                            break;
                        case 1:
                            $type = "(CHO / TẶNG / HỦY)";
                            break;
                        case 2:
                            $type = "(LƯU CHUYỂN NỘI BỘ)";;
                            break;
                        default:
                            $type="";
                            break;
                    }
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'BÁO CÁO XUẤT KHO HÀNG HÓA '.$type.' TỪ '.$startday.' ĐẾN '.$stopday);
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A2', 'SKU')
                            ->setCellValue('B2', 'Tên sản phẩm')
                            ->setCellValue('C2', 'Số lượng xuất')
                            ->setCellValue('D2', 'Xuất tại kho');
                }
                $i=3;
                foreach($result as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->MaSP)
                            ->setCellValue('B'.$i, $row->Tensanpham)
                            ->setCellValue('C'.$i, $row->TotalAmount)
                            ->setCellValue('D'.$i, $row->MaKho);
                    $i++;
                }
                if($type_export==2){
                    $objPHPExcel->getActiveSheet()->setTitle('BC_NHAPKHO');
                    header('Content-Disposition: attachment;filename="BC_NHAPKHO.xls"');
                }else{
                    $objPHPExcel->getActiveSheet()->setTitle('BC_XUATKHO');
                    header('Content-Disposition: attachment;filename="BC_XUATKHO.xls"');
                }
                $objPHPExcel->setActiveSheetIndex(0);
                header('Content-Type: application/vnd.ms-excel');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header ('Cache-Control: cache, must-revalidate');
                header ('Pragma: public');

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                ob_end_clean();
                $objWriter->save('php://output');
                exit;
            }else{
                echo "WARNING : Data too large !!!!";
            }
        }
    }

    public function export_data_orderpage($startday,$stopday){
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $statusbonus = "";
        $orderby = "a.Ngaydathang DESC";
        switch ($this->user->UserType) {
            case 1:
                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;
            case 2:
                $view = 'admin/import_order_home_kho';
                $statusbonus = $this->user->Channel==1 ? " and a.Status=3 and a.OrderType in(1,2,4,5)" : "";
                $statusbonus = $this->user->Channel==2 ? " and a.Status=3 and a.OrderType=3" : $statusbonus;
                $statusbonus = $this->user->Channel==0 ? " and a.Status=3 and a.OrderType=0": $statusbonus;
                $orderby = "a.Ngaydathang ASC";
                break;
            case 3:
                $view = 'admin/import_order_home_ketoan';
                $statusbonus = $this->user->Channel==1 ? " and a.Status=5 and a.OrderType in(1,2,4,5)" : '';
                $statusbonus = $this->user->Channel==2 ? " and a.Status=5 and a.OrderType=3" : $statusbonus;
                $statusbonus = $this->user->Channel==0 ?" and a.Status=5 and a.OrderType=0" : $statusbonus;
                $orderby = "a.Ngaydathang ASC";
                break;
            case 4:
                $view = 'admin/import_order_home_dieuphoi';
                $statusbonus = " and a.Status in(7,8,9)";
                $statusbonus = $this->user->Channel==1 ? $statusbonus." and a.OrderType in(1,2,4,5)" : $statusbonus." and a.OrderType in(0,3)";
                $orderby = "a.Ngaydathang ASC";
                break;
            case 5:
                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;
            case 6:
                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;
            case 7:
                $view = 'admin/import_order_home_ketoan_manager';
                $statusbonus = " and a.Status = 5 and a.Accept=1";
                break;
            case 8:
                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;
            case 10:
                $view = 'admin/import_order_home_user';
                $statusbonus = " and a.OrderType in(1,2,4,5)";
                break;
            default:
                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;
        }
        if($this->user->UserType==4){
            $result = $this->db->query("select DISTINCT a.ID,a.AddressOrder,a.MaDH,f.MaXK,a.Status,a.OrderType,a.Chiphi,a.Chietkhau,a.SoluongSP,g.MaKho,a.Total,a.Ngaydathang,a.TransportID,a.Reduce,c.Name,c.SystemID from ttp_report_order a,ttp_report_customer c,ttp_report_export_warehouse f,ttp_report_warehouse g where a.KhoID=g.ID and a.ID=f.OrderID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus order by $orderby")->result();
            $this->export_data_orderpage_logistics($result);
        }else{
            $result = $this->db->query("select DISTINCT a.ID,a.MaDH,a.UserID,a.Status,a.OrderType,a.Accept,a.PaymentStatus,a.Payment,a.Chiphi,a.Ghichu,a.Chietkhau,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Ngaydathang,a.Reduce,c.Name,c.Phone1,d.Title as Thanhpho,g.MaKho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_warehouse g where a.KhoID=g.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus order by $orderby")->result();
            $this->export_data_orderpage_sale($result);
        }
    }

    public function export_data_orderpage_sale(){
        
    }

    public function export_data_orderpage_logistics($result){
        if(count($result)<3000){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Kênh bán hàng')
                        ->setCellValue('B1', 'Ngày đặt hàng')
                        ->setCellValue('C1', 'Mã đơn hàng')
                        ->setCellValue('D1', 'Mã xuất kho')
                        ->setCellValue('E1', 'Kho xuất hàng')
                        ->setCellValue('F1', 'Đối tác vận chuyển')
                        ->setCellValue('G1', 'Tên của hàng / khách hàng')
                        ->setCellValue('H1', 'Hệ thống kênh')
                        ->setCellValue('I1', 'Địa chỉ giao hàng')
                        ->setCellValue('J1', 'Số lượng SP bán')
                        ->setCellValue('K1', 'Tổng giá trị đơn hàng');
            $array_status = array(10=>'Chờ xác nhận hủy',9=>'Điều phối đang xử lý',8=>'Đơn hàng bị trả về',7=>'Đang vận chuyển',6=>'Kế toán trả về',5=>'Kế toán xử lý',4=>'Kho trả về',3=>'Kho đang xử lý',2=>'Đơn hàng nháp',0=>'Đơn hàng thành công',1=>'Đơn hàng hủy');
            $arr_type = array(0=>"Online",2=>"MT",1=>"GT",3=>"Gốm sứ");
            $arr_payment = array(0=>"COD",1=>"Chuyển khoản");
            $i=2;
            $system = $this->db->query("select * from ttp_report_system")->result();
            $system_array = array();
            if(count($system)>0){
                foreach($system as $row){
                    $system_array[$row->ID] = $row->Title;
                }
            }
            $transport = $this->db->query("select * from ttp_report_transport")->result();
            $transport_array = array();
            if(count($transport)>0){
                foreach($transport as $row){
                    $transport_array[$row->ID] = $row->Title;
                }
            }
            foreach($result as $row){
                $transport_title = isset($transport_array[$row->TransportID]) ? $transport_array[$row->TransportID] : '--' ;
                $system_title = isset($system_array[$row->SystemID]) ? $system_array[$row->SystemID] : '--' ;
                $status = isset($array_status[$row->Status]) ? $array_status[$row->Status] : "--" ;
                $tienhang = $row->Total-$row->Chietkhau;
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $arr_type[$row->OrderType])
                        ->setCellValue('B'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                        ->setCellValue('C'.$i, $row->MaDH)
                        ->setCellValue('D'.$i, $row->MaXK)
                        ->setCellValue('E'.$i, $row->MaKho)
                        ->setCellValue('F'.$i, $transport_title)
                        ->setCellValue('G'.$i, $row->Name)
                        ->setCellValue('H'.$i, $system_title)
                        ->setCellValue('I'.$i, $row->AddressOrder)
                        ->setCellValue('J'.$i, $row->SoluongSP)
                        ->setCellValue('K'.$i, $tienhang);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('RESULT_DATA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="DATA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "WARNING : Data too large !!!!";
        }
    }

    public function export_ketoan(){
        if($this->user->IsAdmin==1 || $this->user->UserType==3 || $this->user->UserType==5 || $this->user->UserType==7 || $this->user->UserType==2){
            $TypeExport = isset($_POST['TypeExport']) ? $_POST['TypeExport'] : 0 ;
            $day_export = isset($_POST['Export_date']) ? $_POST['Export_date'] : date('Y-m-d',time());
            $daystop_export = isset($_POST['ExportStop_date']) ? $_POST['ExportStop_date'] : date('Y-m-d',time());
            if($day_export!=''){
                ini_set('memory_limit', '3500M');
                if($TypeExport==1 && ($this->user->UserType==3 || $this->user->IsAdmin==1 || $this->user->UserType==7)){
                    $this->export_bcbh($day_export);
                }
                if($TypeExport==2 && ($this->user->UserType==3 || $this->user->IsAdmin==1 || $this->user->IsAdmin==7)){
                    $this->export_bcol($day_export);
                }
                if($TypeExport==3 && ($this->user->UserType==3 || $this->user->IsAdmin==1 || $this->user->IsAdmin==7)){
                    $this->export_bcmisa($day_export);
                }
                if($TypeExport==4 && ($this->user->UserType==5 || $this->user->IsAdmin==1)){
                    $this->export_bctls($day_export);
                }
                if($TypeExport==5 && ($this->user->UserType==3 || $this->user->IsAdmin==1 || $this->user->IsAdmin==7)){
                    $this->export_bcmisabh($day_export);
                }
                if($TypeExport==6 && ($this->user->UserType==5 || $this->user->UserType==3 || $this->user->IsAdmin==1 || $this->user->IsAdmin==7)){
                    $this->export_bcpxk($day_export,$daystop_export);
                }
                if($TypeExport==7 && ($this->user->UserType==2 || $this->user->IsAdmin==1)){
                    $this->export_bcnvkho($day_export);
                }
                if($TypeExport==8 && ($this->user->UserType==5 || $this->user->IsAdmin==1)){
                    $this->export_bcdatakh($day_export);
                }
                if($TypeExport==9 && ($this->user->UserType==3 || $this->user->IsAdmin==1)){
                    $this->export_bcbhck($day_export);
                }
                if($TypeExport==10 && ($this->user->UserType==5 || $this->user->IsAdmin==1)){
                    $this->export_bctlsrange($day_export,$daystop_export);
                }
                if($TypeExport==11 && ($this->user->UserType==7 || $this->user->UserType==3 || $this->user->IsAdmin==1)){
                    $this->export_bcbhmtgt($day_export);
                }
                if($TypeExport==12 && ($this->user->UserType==5 || $this->user->IsAdmin==1)){
                    $this->export_bcdoanhso($day_export,$daystop_export);
                }
                if($TypeExport==13 && ($this->user->UserType==3 || $this->user->IsAdmin==1)){
                    $this->export_onlinedetails($day_export,$daystop_export);
                }
            }
        }
    }

    public function export_bcdatakh($day_export){
        $month = date('m',strtotime($day_export));
        $year = date('Y',strtotime($day_export));
        $result = $this->db->query("select DISTINCT a.ID,a.Ngaydathang,a.HistoryEdited,a.Total,a.Chietkhau,a.SoluongSP,a.Status,c.Name,c.Phone1,c.Phone2,b.UserName,e.Title as Khuvuc from ttp_report_order a,ttp_report_customer c,ttp_user b,ttp_report_city d,ttp_report_area e where a.CityID=d.ID and d.AreaID=e.ID and a.UserID=b.ID and a.CustomerID=c.ID and MONTH(a.Ngaydathang)='$month' and YEAR(a.Ngaydathang)=$year order by a.ID ASC")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày lên đơn hàng')
                        ->setCellValue('B1', 'Tên khách hàng')
                        ->setCellValue('C1', 'Số điện thoại 1')
                        ->setCellValue('D1', 'Số điện thoại 2')
                        ->setCellValue('E1', 'Khu vực')
                        ->setCellValue('F1', 'Số lượng hộp')
                        ->setCellValue('G1', 'Nhân viên khởi tạo')
                        ->setCellValue('H1', 'Trạng thái đơn hàng')
                        ->setCellValue('I1', 'Nguồn')
                        ->setCellValue('J1', 'Kênh')
                        ->setCellValue('K1', 'Thời gian cập nhật cuối')
                        ->setCellValue('L1', 'Giá trị ĐH');
            $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "DCF31D"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '0000FF')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $array_status = array(
                    9=>'Chờ nhân viên điều phối',
                    8=>'Đơn hàng bị trả về',
                    7=>'Chuyển sang bộ phận giao hàng',
                    6=>'Đơn hàng bị trả về từ kế toán',
                    5=>'Đơn hàng chờ kế toán duyệt',
                    4=>'Đơn hàng bị trả về từ kho',
                    3=>'Đơn hàng mới chờ kho duyệt',
                    2=>'Đơn hàng nháp',
                    0=>'Đơn hàng thành công',
                    1=>'Đơn hàng hủy'
                );
            $i=2;
            foreach($result as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('B'.$i, $row->Name)
                            ->setCellValue('C'.$i, $row->Phone1)
                            ->setCellValue('D'.$i, $row->Phone2)
                            ->setCellValue('E'.$i, $row->Khuvuc)
                            ->setCellValue('F'.$i, $row->SoluongSP)
                            ->setCellValue('G'.$i, $row->UserName)
                            ->setCellValue('H'.$i, $array_status[$row->Status])
                            ->setCellValue('I'.$i, $row->Source)
                            ->setCellValue('J'.$i, $row->Kenhbanhang)
                            ->setCellValue('K'.$i, date('d/m/Y H:i',strtotime($row->HistoryEdited)))
                            ->setCellValue('L'.$i, $row->Total-$row->Chietkhau);
                $i++;
            }
            
            $objPHPExcel->getActiveSheet()->setTitle('DHKHO');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="DHKHO.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }    

    public function export_bcnvkho($day_export){
        $result = $this->db->query("select DISTINCT a.ID,a.MaDH,a.Ngaydathang,a.AddressOrder,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,c.Name,k.Title as Tensanpham,h.Amount,b.MaKho from ttp_report_order a,ttp_report_customer c,ttp_report_orderdetails h,ttp_report_products k,ttp_report_warehouse b,ttp_report_orderhistory e where a.ID=e.OrderID and a.KhoID=b.ID and h.ProductsID=k.ID and h.OrderID=a.ID and a.CustomerID=c.ID and date(e.Thoigian)='$day_export' and e.UserID=".$this->user->ID." order by a.ID ASC")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày xử lý')
                        ->setCellValue('B1', 'Mã đơn hàng')
                        ->setCellValue('C1', 'Ngày đặt hàng')
                        ->setCellValue('D1', 'Mã Kho')
                        ->setCellValue('E1', 'Tên khách hàng')
                        ->setCellValue('F1', 'Địa chỉ giao hàng')
                        ->setCellValue('G1', 'Số lượng SP(Không bao gồm thùng và quà tặng)')
                        ->setCellValue('H1', 'Nội dung hàng')
                        ->setCellValue('I1', 'Tổng giá trị đơn hàng');
            $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "F9D212"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '0000FF')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $arr_order = array();
            $i=2;
            foreach($result as $row){
                if(!isset($arr_order[$row->ID])){
                    $tongtien = $row->Total+$row->Chiphi-$row->Chietkhau;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, date('d/m/Y',strtotime($day_export)))
                            ->setCellValue('B'.$i, $row->MaDH)
                            ->setCellValue('C'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('D'.$i, $row->MaKho)
                            ->setCellValue('E'.$i, $row->Name)
                            ->setCellValue('F'.$i, $row->AddressOrder)
                            ->setCellValue('G'.$i, $row->SoluongSP)
                            ->setCellValue('H'.$i, "$row->Amount $row->Tensanpham")
                            ->setCellValue('I'.$i, $tongtien);
                    $i++;
                    $arr_order[$row->ID] = "$row->Amount $row->Tensanpham";
                    $arr[] = "$row->ID";
                }else{
                    $j = $i-1;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $arr_order[$row->ID]."\n$row->Amount $row->Tensanpham");
                    $arr_order[$row->ID] .= "\n$row->Amount $row->Tensanpham";
                }
            }

            $objPHPExcel->getActiveSheet()->setTitle('DHKHO');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="DHKHO.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bcpxk($day_export='',$daystop_export=''){
        $type = $this->user->Channel==0 ? " and a.OrderType=0" : "" ;
        $type = $this->user->Channel==1 ? " and a.OrderType in(1,2,4,5)" : $type ;
        $type = $this->user->Channel==2 ? " and a.OrderType=3" : $type ;
        $type = $this->user->IsAdmin==1 || $this->user->UserType==7 ? "" : $type ;
        if($daystop_export==''){
            $result = $this->db->query("select a.MaDH,a.Ngaydathang,a.Total,a.Chietkhau,a.Chiphi,b.Name,b.Phone1,d.Lydoxuatkho,d.Ngayxuatkho,g.MaKho,d.MaXK from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse d,ttp_report_warehouse g where d.KhoID=g.ID and a.ID=d.OrderID and b.ID=a.CustomerID and date(d.Ngayxuatkho)='$day_export' $type order by d.MaXK")->result();
        }else{
            $result = $this->db->query("select a.MaDH,a.Ngaydathang,a.Total,a.Chietkhau,a.Chiphi,b.Name,b.Phone1,d.Lydoxuatkho,d.Ngayxuatkho,g.MaKho,d.MaXK from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse d,ttp_report_warehouse g where d.KhoID=g.ID and a.ID=d.OrderID and b.ID=a.CustomerID and date(d.Ngayxuatkho)>='$day_export' and date(d.Ngayxuatkho)<='$daystop_export' $type order by d.MaXK")->result();
        }
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'STT')
                        ->setCellValue('B1', 'Ngày in phiếu')
                        ->setCellValue('C1', 'Ngày đơn hàng')
                        ->setCellValue('D1', 'Mã phiếu xuất kho')
                        ->setCellValue('E1', 'Mã kho')
                        ->setCellValue('F1', 'Mã ĐH')
                        ->setCellValue('G1', 'Tổng giá trị ĐH')
                        ->setCellValue('H1', 'Tên khách hàng');
            $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "E4FF00"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '000000')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $i=2;
            foreach($result as $row){
                    $row->Total = $row->Total-$row->Chietkhau+$row->Chiphi;
                    $temp = $i-1;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $temp)
                            ->setCellValue('B'.$i, date('d/m/Y',strtotime($row->Ngayxuatkho)))
                            ->setCellValue('C'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('D'.$i, $row->MaXK)
                            ->setCellValue('E'.$i, $row->MaKho)
                            ->setCellValue('F'.$i, $row->MaDH)
                            ->setCellValue('G'.$i, $row->Total)
                            ->setCellValue('H'.$i, $row->Name);
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BCPXK');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BCPXK.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bcdoanhso($day_export='',$daystop_export=''){
        $daystop_export = $daystop_export=='' ? $day_export : $daystop_export ;
        $result = $this->db->query("select a.UserID,a.Total,a.Chietkhau,a.Chiphi,a.Reduce,a.SoluongSP,a.MaDH,a.HistoryEdited,a.Status,a.Ghichu,a.Note,a.Payment,a.CustomerType,b.Name,b.Phone1,c.UserName,d.Title as Thanhpho,g.Title as Area,h.Title as Source,i.Title as Quanhuyen,j.Title as Kenh,k.Title as Transport from ttp_report_order a,ttp_report_customer b,ttp_user c,ttp_report_city d,ttp_report_area g,ttp_report_source h,ttp_report_district i,ttp_report_saleschannel j,ttp_report_transport k where a.TransportID=k.ID and a.KenhbanhangID=j.ID and a.DistrictID=i.ID and a.SourceID=h.ID and d.AreaID=g.ID and a.CityID=d.ID and c.ID=a.UserID and b.ID=a.CustomerID and date(a.HistoryEdited)>='$day_export' and date(a.HistoryEdited)<='$daystop_export'")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $teamleader = $this->db->query("select b.ID,b.UserName,a.Data from ttp_report_team a,ttp_user b where a.UserID=b.ID")->result();
            $arr_user = array();
            if(count($teamleader)>0){
                foreach($teamleader as $row){
                    $temp = json_decode($row->Data,true);
                    if(is_array($temp) && count($temp)>0){
                        foreach($temp as $value){
                            $arr_user[$value] = $row->UserName;
                        }
                    }
                }
            }
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày cập nhật')
                        ->setCellValue('B1', 'Số phiếu')
                        ->setCellValue('C1', 'Người phụ trách')
                        ->setCellValue('D1', 'Quận Huyện')
                        ->setCellValue('E1', 'Tỉnh/TP')
                        ->setCellValue('F1', 'Vùng')
                        ->setCellValue('G1', 'Tên KH')
                        ->setCellValue('H1', 'Ghi chú đặt hàng')
                        ->setCellValue('I1', 'Nguồn')
                        ->setCellValue('J1', 'Kênh bán hàng')
                        ->setCellValue('K1', 'HT thanh toán')
                        ->setCellValue('L1', 'Tình trạng ĐH')
                        ->setCellValue('M1', 'Team Leader')
                        ->setCellValue('N1', 'Loại khách hàng')
                        ->setCellValue('O1', 'Giá trị đơn hàng')
                        ->setCellValue('P1', 'Đối tác giao hàng');
            $objPHPExcel->getActiveSheet()->getStyle("A1:P1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "FBFF00"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '0000FF')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $i=2;
            $arrpayment = array(0=>"COD",1=>"Chuyển khoản");
            $array_status = array(
                    10=>'Chờ xác nhận hủy',
                    9=>'Chờ nhân viên điều phối',
                    8=>'Đơn hàng bị trả về',
                    7=>'Chuyển sang bộ phận giao hàng',
                    6=>'Đơn hàng bị trả về từ kế toán',
                    5=>'Đơn hàng chờ kế toán duyệt',
                    4=>'Đơn hàng bị trả về từ kho',
                    3=>'Đơn hàng mới chờ kho duyệt',
                    2=>'Đơn hàng nháp',
                    0=>'Đơn hàng thành công',
                    1=>'Đơn hàng hủy'
                );
            foreach($result as $row){
                    $customer = $row->CustomerType==1 ? 'Cũ' : 'Mới' ;
                    $userteam = isset($arr_user[$row->UserID]) ? $arr_user[$row->UserID] : '--' ;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, date('m/d/Y',strtotime($row->HistoryEdited)))
                            ->setCellValue('B'.$i, $row->MaDH)
                            ->setCellValue('C'.$i, $row->UserName)
                            ->setCellValue('D'.$i, $row->Quanhuyen)
                            ->setCellValue('E'.$i, $row->Thanhpho)
                            ->setCellValue('F'.$i, $row->Area)
                            ->setCellValue('G'.$i, trim($row->Name))
                            ->setCellValue('H'.$i, trim($row->Note))
                            ->setCellValue('I'.$i, $row->Source)
                            ->setCellValue('J'.$i, $row->Kenh)
                            ->setCellValue('K'.$i, $arrpayment[$row->Payment])
                            ->setCellValue('L'.$i, $array_status[$row->Status])
                            ->setCellValue('M'.$i, $userteam)
                            ->setCellValue('N'.$i, $customer)
                            ->setCellValue('O'.$i, $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi)
                            ->setCellValue('P'.$i, $row->Transport);
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BCDOANHSO');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BC_DOANHSO.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bcmisabh($day_export){
        $result = $this->db->query("select DISTINCT a.MaDH,a.Ngaydathang,a.Ghichu,a.Note,a.Payment,a.AddressOrder,a.UserID,b.Name,b.Phone1,f.Title as Tensanpham,f.MaSP,f.Donvi,e.Amount,e.Total,e.Price,e.PriceDown,d.Lydoxuatkho,d.MaXK,d.Ngayxuatkho,g.MaKho from ttp_report_order a,ttp_report_customer b,ttp_report_orderdetails e,ttp_report_products f,ttp_report_export_warehouse d,ttp_report_warehouse g where d.KhoID=g.ID and a.ID=d.OrderID and e.ProductsID=f.ID and e.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export'")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Hiển thị trên sổ')
                        ->setCellValue('B1', 'Hình thức bán hàng')
                        ->setCellValue('C1', 'Phương thức thanh toán')
                        ->setCellValue('D1', 'Kiêm phiếu xuất kho')
                        ->setCellValue('E1', 'Lập kèm hóa đơn')
                        ->setCellValue('F1', 'Đã lập hóa đơn')
                        ->setCellValue('G1', 'Ngày hạch toán(*)')
                        ->setCellValue('H1', 'Ngày chứng từ')
                        ->setCellValue('I1', 'Số chứng từ')
                        ->setCellValue('J1', 'Số phiếu xuất')
                        ->setCellValue('K1', 'Lý do xuất')
                        ->setCellValue('L1', 'Số hóa đơn')
                        ->setCellValue('M1', 'Ngày hóa đơn')
                        ->setCellValue('N1', 'Mã khách hàng')
                        ->setCellValue('O1', 'Tên khách hàng')
                        ->setCellValue('P1', 'Địa chỉ')
                        ->setCellValue('Q1', 'Mã số thuế')
                        ->setCellValue('R1', 'Diễn giải')
                        ->setCellValue('S1', 'Nộp vào tài khoản')
                        ->setCellValue('T1', 'NV bán hàng')
                        ->setCellValue('U1', 'Loại tiền')
                        ->setCellValue('V1', 'Tỷ giá')
                        ->setCellValue('W1', 'Mã hàng(*)')
                        ->setCellValue('X1', 'Tên hàng')
                        ->setCellValue('Y1', 'Hàng khuyến mãi')
                        ->setCellValue('Z1', 'TK Tiền/Chi phí/Nợ')
                        ->setCellValue('AA1', 'TK doanh thu/Có')
                        ->setCellValue('AB1', 'ĐVT')
                        ->setCellValue('AC1', 'Số lượng')
                        ->setCellValue('AD1', 'Đơn giá sau thuế')
                        ->setCellValue('AE1', 'Đơn giá')
                        ->setCellValue('AF1', 'Thành tiền')
                        ->setCellValue('AG1', 'Thành tiền quy đổi')
                        ->setCellValue('AH1', 'Tỷ lệ CK(%)')
                        ->setCellValue('AI1', 'Tiền chiết khấu')
                        ->setCellValue('AJ1', 'Tiền chiết khấu quy đổi')
                        ->setCellValue('AK1', 'TK chiết khấu')
                        ->setCellValue('AL1', 'Giá tính thuế XK')
                        ->setCellValue('AM1', '% thuế XK')
                        ->setCellValue('AN1', 'Tiền thuế XK')
                        ->setCellValue('AO1', 'TK thuế XK')
                        ->setCellValue('AP1', '% thuế GTGT')
                        ->setCellValue('AQ1', 'Tiền thuế GTGT')
                        ->setCellValue('AR1', 'Tiền thuế GTGT quy đổi')
                        ->setCellValue('AS1', 'TK thuế GTGT')
                        ->setCellValue('AT1', 'Kho')
                        ->setCellValue('AU1', 'TK giá vốn')
                        ->setCellValue('AV1', 'TK kho')
                        ->setCellValue('AW1', 'Đơn giá vốn')
                        ->setCellValue('AX1', 'Tiền vốn');
            $objPHPExcel->getActiveSheet()->getStyle("A1:AX1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "E4FF00"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '000000')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $i=2;
            foreach($result as $row){
                    $giasauthue = round($row->Price/1.1);
                    $thanhtien = $giasauthue*$row->Amount;
                    $thue = ($thanhtien/100)*10;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, '')
                            ->setCellValue('B'.$i, '')
                            ->setCellValue('C'.$i, '')
                            ->setCellValue('D'.$i, '')
                            ->setCellValue('E'.$i, '')
                            ->setCellValue('F'.$i, '')
                            ->setCellValue('G'.$i, date('d/m/Y',strtotime($row->Ngayxuatkho)))
                            ->setCellValue('H'.$i, date('d/m/Y',strtotime($row->Ngayxuatkho)))
                            ->setCellValue('I'.$i, $row->MaXK)
                            ->setCellValue('J'.$i, str_replace("BH","PX",$row->MaXK))
                            ->setCellValue('K'.$i, $row->Lydoxuatkho)
                            ->setCellValue('L'.$i, "")
                            ->setCellValue('M'.$i, "")
                            ->setCellValue('N'.$i, $row->Phone1)
                            ->setCellValue('O'.$i, $row->Name)
                            ->setCellValue('P'.$i, $row->AddressOrder)
                            ->setCellValue('Q'.$i, "")
                            ->setCellValue('R'.$i, $row->Lydoxuatkho)
                            ->setCellValue('S'.$i, '')
                            ->setCellValue('T'.$i, $row->UserID)
                            ->setCellValue('U'.$i, '')
                            ->setCellValue('V'.$i, '')
                            ->setCellValue('W'.$i, $row->MaSP)
                            ->setCellValue('X'.$i, $row->Tensanpham)
                            ->setCellValue('Y'.$i, "")
                            ->setCellValue('Z'.$i, "")
                            ->setCellValue('AA'.$i, "")
                            ->setCellValue('AB'.$i, $row->Donvi)
                            ->setCellValue('AC'.$i, $row->Amount)
                            ->setCellValue('AD'.$i, $row->Price)
                            ->setCellValue('AE'.$i, $giasauthue)
                            ->setCellValue('AF'.$i, $thanhtien)
                            ->setCellValue('AG'.$i, '')
                            ->setCellValue('AH'.$i, '')
                            ->setCellValue('AI'.$i, '')
                            ->setCellValue('AJ'.$i, '')
                            ->setCellValue('AK'.$i, '')
                            ->setCellValue('AL'.$i, '')
                            ->setCellValue('AM'.$i, '')
                            ->setCellValue('AN'.$i, '')
                            ->setCellValue('AO'.$i, '')
                            ->setCellValue('AP'.$i, '10')
                            ->setCellValue('AQ'.$i, $thue)
                            ->setCellValue('AR'.$i, '')
                            ->setCellValue('AS'.$i, '')
                            ->setCellValue('AT'.$i, $row->MaKho)
                            ->setCellValue('AU'.$i, '')
                            ->setCellValue('AV'.$i, '')
                            ->setCellValue('AW'.$i, '')
                            ->setCellValue('AX'.$i, '');
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BCMISA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BCMISA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bcmisa($day_export){
        $result = $this->db->query("select a.MaDH,a.Ngaydathang,a.Ghichu,a.Note,a.Payment,a.AddressOrder,b.Name,b.Phone1,c.UserName,f.Title as Tensanpham,f.MaSP,f.Donvi,e.Amount,e.Total,e.Price,e.PriceDown,d.Lydoxuatkho,g.MaKho from ttp_report_order a,ttp_report_customer b,ttp_user c,ttp_report_orderdetails e,ttp_report_products f,ttp_report_export_warehouse d,ttp_report_warehouse g where d.KhoID=g.ID and a.ID=d.OrderID and c.ID=a.UserID and e.ProductsID=f.ID and f.CategoriesID!=62 and e.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export'")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';

            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày đơn hàng(*)')
                        ->setCellValue('B1', 'Số đơn hàng')
                        ->setCellValue('C1', 'Tình trạng')
                        ->setCellValue('D1', 'Ngày giao hàng')
                        ->setCellValue('E1', 'Địa điểm giao hàng')
                        ->setCellValue('F1', 'Tính giá thành')
                        ->setCellValue('G1', 'Mã khách hàng')
                        ->setCellValue('H1', 'Tên khách hàng')
                        ->setCellValue('I1', 'Diễn giải')
                        ->setCellValue('J1', 'NV bán hàng')
                        ->setCellValue('K1', 'Loại tiền')
                        ->setCellValue('L1', 'Tỷ giá')
                        ->setCellValue('M1', 'Mã hàng')
                        ->setCellValue('N1', 'Tên hàng')
                        ->setCellValue('O1', 'Hàng khuyến mãi')
                        ->setCellValue('P1', 'Mã kho')
                        ->setCellValue('Q1', 'Số lô')
                        ->setCellValue('R1', 'Hạn sử dụng')
                        ->setCellValue('S1', 'ĐVT')
                        ->setCellValue('T1', 'Số lượng')
                        ->setCellValue('U1', 'Đơn giá sau thuế')
                        ->setCellValue('V1', 'Đơn giá')
                        ->setCellValue('W1', 'Thành tiền')
                        ->setCellValue('X1', 'Thành tiền quy đổi')
                        ->setCellValue('Y1', 'Tỷ lệ chiết khấu')
                        ->setCellValue('Z1', 'Số tiền chiết khấu')
                        ->setCellValue('AA1', 'Tiền chiết khấu quy đổi')
                        ->setCellValue('AB1', '% thuế GTGT')
                        ->setCellValue('AC1', 'Tiền thuế GTGT')
                        ->setCellValue('AD1', 'Tiền thuế GTGT quy đổi');
            $objPHPExcel->getActiveSheet()->getStyle("A1:AD1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "126D10"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '000000')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $i=2;
            foreach($result as $row){
                    $giasauthue = round($row->Price/1.1);
                    $thanhtien = $giasauthue*$row->Amount;
                    $thue = ($thanhtien/100)*10;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('B'.$i, $row->MaDH)
                            ->setCellValue('C'.$i, '')
                            ->setCellValue('D'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('E'.$i, $row->AddressOrder)
                            ->setCellValue('F'.$i, '')
                            ->setCellValue('G'.$i, $row->Phone1)
                            ->setCellValue('H'.$i, $row->Name)
                            ->setCellValue('I'.$i, $row->Lydoxuatkho)
                            ->setCellValue('J'.$i, $row->UserName)
                            ->setCellValue('K'.$i, "VND")
                            ->setCellValue('L'.$i, "")
                            ->setCellValue('M'.$i, $row->MaSP)
                            ->setCellValue('N'.$i, $row->Tensanpham)
                            ->setCellValue('O'.$i, '')
                            ->setCellValue('P'.$i, $row->MaKho)
                            ->setCellValue('Q'.$i, "")
                            ->setCellValue('R'.$i, "")
                            ->setCellValue('S'.$i, $row->Donvi)
                            ->setCellValue('T'.$i, $row->Amount)
                            ->setCellValue('U'.$i, $row->Price)
                            ->setCellValue('V'.$i, $giasauthue)
                            ->setCellValue('W'.$i, $thanhtien)
                            ->setCellValue('X'.$i, "")
                            ->setCellValue('Y'.$i, "")
                            ->setCellValue('Z'.$i, "")
                            ->setCellValue('AA'.$i, "")
                            ->setCellValue('AB'.$i, 10)
                            ->setCellValue('AC'.$i, $thue)
                            ->setCellValue('AD'.$i, "");
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BCMISA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BCMISA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bctls($day_export){
        if(strtotime($day_export)<strtotime(date('2015-12-01'))){
            $this->export_bctls_temp($day_export);
            return;
        }
        $result = $this->db->query("select DISTINCT a.UserID,a.MaDH,a.Ngaydathang,a.Status,a.Ghichu,a.Note,a.Payment,a.CustomerType,b.Name,b.Phone1,c.UserName,d.Title as Thanhpho,g.Title as Area,f.Title as Tensanpham,f.MaSP,e.Amount,e.Total,e.Price,e.PriceDown,h.Title as Source,i.Title as Quanhuyen,j.Title as Kenh from ttp_report_order a,ttp_report_customer b,ttp_user c,ttp_report_city d,ttp_report_orderdetails e,ttp_report_products f,ttp_report_area g,ttp_report_source h,ttp_report_district i,ttp_report_saleschannel j where a.KenhbanhangID=j.ID and a.DistrictID=i.ID and a.SourceID=h.ID and d.AreaID=g.ID and a.CityID=d.ID and c.ID=a.UserID and e.ProductsID=f.ID and f.CategoriesID!=62 and e.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export'")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $teamleader = $this->db->query("select b.ID,b.UserName,a.Data from ttp_report_team a,ttp_user b where a.UserID=b.ID")->result();
            $arr_user = array();
            if(count($teamleader)>0){
                foreach($teamleader as $row){
                    $temp = json_decode($row->Data,true);
                    if(is_array($temp) && count($temp)>0){
                        foreach($temp as $value){
                            $arr_user[$value] = $row->UserName;
                        }
                    }
                }
            }
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày ĐH')
                        ->setCellValue('B1', 'Giờ ĐH')
                        ->setCellValue('C1', 'Số phiếu')
                        ->setCellValue('D1', 'Người phụ trách')
                        ->setCellValue('E1', 'Quận Huyện')
                        ->setCellValue('F1', 'Tỉnh/TP')
                        ->setCellValue('G1', 'Vùng')
                        ->setCellValue('H1', 'Tên KH')
                        ->setCellValue('I1', 'Ghi chú đặt hàng')
                        ->setCellValue('J1', 'Nguồn')
                        ->setCellValue('K1', 'Kênh bán hàng')
                        ->setCellValue('L1', 'HT thanh toán')
                        ->setCellValue('M1', 'Ngày đặt hàng')
                        ->setCellValue('N1', 'Tên sản phẩm')
                        ->setCellValue('O1', 'Giá bán')
                        ->setCellValue('P1', 'Số lượng')
                        ->setCellValue('Q1', 'Tổng tiền phải thu')
                        ->setCellValue('R1', 'Tình trạng ĐH')
                        ->setCellValue('S1', 'Lý do')
                        ->setCellValue('T1', 'Số ĐH')
                        ->setCellValue('U1', 'Phone')
                        ->setCellValue('V1', 'Team Leader')
                        ->setCellValue('W1', 'Loại khách hàng');
            $objPHPExcel->getActiveSheet()->getStyle("A1:W1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "FBFF00"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '0000FF')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $i=2;
            $arrpayment = array(0=>"COD",1=>"Chuyển khoản");
            $array_status = array(
                    9=>'Chờ nhân viên điều phối',
                    8=>'Đơn hàng bị trả về',
                    7=>'Chuyển sang bộ phận giao hàng',
                    6=>'Đơn hàng bị trả về từ kế toán',
                    5=>'Đơn hàng chờ kế toán duyệt',
                    4=>'Đơn hàng bị trả về từ kho',
                    3=>'Đơn hàng mới chờ kho duyệt',
                    2=>'Đơn hàng nháp',
                    0=>'Đơn hàng thành công',
                    1=>'Đơn hàng hủy'
                );
            $arr_order = array();
            foreach($result as $row){
                    $customer = $row->CustomerType==1 ? 'Cũ' : 'Mới' ;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, date('m/d/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('B'.$i, date('H:i:s',strtotime($row->Ngaydathang)))
                            ->setCellValue('C'.$i, $row->MaDH)
                            ->setCellValue('D'.$i, $row->UserName)
                            ->setCellValue('E'.$i, $row->Quanhuyen)
                            ->setCellValue('F'.$i, $row->Thanhpho)
                            ->setCellValue('G'.$i, $row->Area)
                            ->setCellValue('H'.$i, trim($row->Name))
                            ->setCellValue('I'.$i, $row->Note)
                            ->setCellValue('J'.$i, $row->Source)
                            ->setCellValue('K'.$i, $row->Kenh)
                            ->setCellValue('L'.$i, $arrpayment[$row->Payment])
                            ->setCellValue('M'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('N'.$i, $row->Tensanpham)
                            ->setCellValue('O'.$i, $row->Price)
                            ->setCellValue('P'.$i, $row->Amount)
                            ->setCellValue('Q'.$i, $row->Total)
                            ->setCellValue('R'.$i, $array_status[$row->Status])
                            ->setCellValue('S'.$i, $row->Ghichu);
                    if(!in_array($row->MaDH,$arr_order)){
                        $arr_order[] = $row->MaDH;
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, 1);
                    }else{
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, 0);
                    }
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$i,$row->Phone1);
                    $userteam = isset($arr_user[$row->UserID]) ? $arr_user[$row->UserID] : '--' ;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i,$userteam);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$i,$customer);
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BCTLS');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BCTLS.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bctls_temp($day_export){
        
        $result = $this->db->query("select DISTINCT a.UserID,a.MaDH,a.Ngaydathang,a.Status,a.Ghichu,a.Note,a.Payment,a.CustomerType,b.Name,b.Phone1,c.UserName,d.Title as Thanhpho,g.Title as Area,f.Title as Tensanpham,f.MaSP,e.Amount,e.Total,e.Price,e.PriceDown,h.Title as Source,i.Title as Quanhuyen from ttp_report_order a,ttp_report_customer b,ttp_user c,ttp_report_city d,ttp_report_orderdetails e,ttp_report_products f,ttp_report_area g,ttp_report_source h,ttp_report_district i where a.DistrictID=i.ID and a.SourceID=h.ID and d.AreaID=g.ID and a.CityID=d.ID and c.ID=a.UserID and e.ProductsID=f.ID and f.CategoriesID!=62 and e.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export'")->result();
        if(count($result)>0){

            error_reporting(E_ALL);

            ini_set('display_errors', TRUE);

            ini_set('display_startup_errors', TRUE);

            if (PHP_SAPI == 'cli')

                die('This example should only be run from a Web Browser');

            require_once 'public/plugin/PHPExcel.php';

            $teamleader = $this->db->query("select b.ID,b.UserName,a.Data from ttp_report_team a,ttp_user b where a.UserID=b.ID")->result();

            $arr_user = array();

            if(count($teamleader)>0){

                foreach($teamleader as $row){

                    $temp = json_decode($row->Data,true);

                    if(is_array($temp) && count($temp)>0){

                        foreach($temp as $value){

                            $arr_user[$value] = $row->UserName;

                        }

                    }

                }

            }

            $arr = array();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")

                                         ->setLastModifiedBy("Maarten Balliauw")

                                         ->setTitle("Office 2007 XLSX Test Document")

                                         ->setSubject("Office 2007 XLSX Test Document")

                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")

                                         ->setKeywords("office 2007 openxml php")

                                         ->setCategory("Test result file");

            $objPHPExcel->setActiveSheetIndex(0)

                        ->setCellValue('A1', 'Ngày ĐH')

                        ->setCellValue('B1', 'Giờ ĐH')

                        ->setCellValue('C1', 'Số phiếu')

                        ->setCellValue('D1', 'Người phụ trách')

                        ->setCellValue('E1', 'Quận Huyện')

                        ->setCellValue('F1', 'Tỉnh/TP')

                        ->setCellValue('G1', 'Vùng')

                        ->setCellValue('H1', 'Tên KH')

                        ->setCellValue('I1', 'Ghi chú đặt hàng')

                        ->setCellValue('J1', 'Nguồn')

                        ->setCellValue('K1', 'Kênh bán hàng')

                        ->setCellValue('L1', 'HT thanh toán')

                        ->setCellValue('M1', 'Ngày đặt hàng')

                        ->setCellValue('N1', 'Tên sản phẩm')

                        ->setCellValue('O1', 'Giá bán')

                        ->setCellValue('P1', 'Số lượng')

                        ->setCellValue('Q1', 'Tổng tiền phải thu')

                        ->setCellValue('R1', 'Tình trạng ĐH')

                        ->setCellValue('S1', 'Lý do')

                        ->setCellValue('T1', 'Số ĐH')

                        ->setCellValue('U1', 'Phone')

                        ->setCellValue('V1', 'Team Leader')

                        ->setCellValue('W1', 'Loại khách hàng');

            $objPHPExcel->getActiveSheet()->getStyle("A1:W1")->getFill()->applyFromArray(array(

                'type' => PHPExcel_Style_Fill::FILL_SOLID,

                'startcolor' => array(

                    'rgb' => "FBFF00"

                ),

                'font'  => array(

                    'bold'  => true,

                    'color' => array('rgb' => '0000FF')

                ),

                'borders' => array(

                    'allborders' => array(

                      'style' => PHPExcel_Style_Border::BORDER_THIN

                    )

                  )

            ));

            $i=2;

            $arrpayment = array(0=>"COD",1=>"Chuyển khoản");

            $array_status = array(

                    9=>'Chờ nhân viên điều phối',

                    8=>'Đơn hàng bị trả về',

                    7=>'Chuyển sang bộ phận giao hàng',

                    6=>'Đơn hàng bị trả về từ kế toán',

                    5=>'Đơn hàng chờ kế toán duyệt',

                    4=>'Đơn hàng bị trả về từ kho',

                    3=>'Đơn hàng mới chờ kho duyệt',

                    2=>'Đơn hàng nháp',

                    0=>'Đơn hàng thành công',

                    1=>'Đơn hàng hủy'

                );

            $arr_order = array();

            foreach($result as $row){

                    $customer = $row->CustomerType==1 ? 'Cũ' : 'Mới' ;

                    $objPHPExcel->setActiveSheetIndex(0)

                            ->setCellValue('A'.$i, date('m/d/Y',strtotime($row->Ngaydathang)))

                            ->setCellValue('B'.$i, date('H:i:s',strtotime($row->Ngaydathang)))

                            ->setCellValue('C'.$i, $row->MaDH)

                            ->setCellValue('D'.$i, $row->UserName)

                            ->setCellValue('E'.$i, $row->Quanhuyen)

                            ->setCellValue('F'.$i, $row->Thanhpho)

                            ->setCellValue('G'.$i, $row->Area)

                            ->setCellValue('H'.$i, trim($row->Name))

                            ->setCellValue('I'.$i, $row->Note)

                            ->setCellValue('J'.$i, $row->Source)

                            ->setCellValue('K'.$i, '')

                            ->setCellValue('L'.$i, $arrpayment[$row->Payment])

                            ->setCellValue('M'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))

                            ->setCellValue('N'.$i, $row->Tensanpham)

                            ->setCellValue('O'.$i, $row->Price)

                            ->setCellValue('P'.$i, $row->Amount)

                            ->setCellValue('Q'.$i, $row->Total)

                            ->setCellValue('R'.$i, $array_status[$row->Status])

                            ->setCellValue('S'.$i, $row->Ghichu);

                    if(!in_array($row->MaDH,$arr_order)){

                        $arr_order[] = $row->MaDH;

                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, 1);

                    }else{

                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, 0);

                    }

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$i,$row->Phone1);

                    $userteam = isset($arr_user[$row->UserID]) ? $arr_user[$row->UserID] : '--' ;

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i,$userteam);

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$i,$customer);

                    $i++;

            }

            $objPHPExcel->getActiveSheet()->setTitle('BCTLS');

            $objPHPExcel->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.ms-excel');

            header('Content-Disposition: attachment;filename="BCTLS.xls"');

            header('Cache-Control: max-age=0');

            header('Cache-Control: max-age=1');

            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

            header ('Cache-Control: cache, must-revalidate');

            header ('Pragma: public');

            

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            ob_end_clean();

            $objWriter->save('php://output');

            exit;

        }else{

            echo "Data is empty !. Can't export data .";

        }

    }

    public function export_bctlsrange($day_export,$daystop_export){
        $result = $this->db->query("select a.UserID,a.Total,a.MaDH,a.SoluongSP,a.Ngaydathang,a.Status,a.Ghichu,a.Note,a.Payment,a.CustomerType,b.Name,b.Phone1,c.UserName,d.Title as Thanhpho,g.Title as Area,h.Title as Source,i.Title as Quanhuyen,j.Title as Kenh from ttp_report_order a,ttp_report_customer b,ttp_user c,ttp_report_city d,ttp_report_area g,ttp_report_source h,ttp_report_district i,ttp_report_saleschannel j where a.KenhbanhangID=j.ID and a.DistrictID=i.ID and a.SourceID=h.ID and d.AreaID=g.ID and a.CityID=d.ID and c.ID=a.UserID and b.ID=a.CustomerID and date(a.Ngaydathang)>='$day_export' and date(a.Ngaydathang)<='$daystop_export'")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $teamleader = $this->db->query("select b.ID,b.UserName,a.Data from ttp_report_team a,ttp_user b where a.UserID=b.ID")->result();
            $arr_user = array();
            if(count($teamleader)>0){
                foreach($teamleader as $row){
                    $temp = json_decode($row->Data,true);
                    if(is_array($temp) && count($temp)>0){
                        foreach($temp as $value){
                            $arr_user[$value] = $row->UserName;
                        }
                    }
                }
            }
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày ĐH')
                        ->setCellValue('B1', 'Giờ ĐH')
                        ->setCellValue('C1', 'Số phiếu')
                        ->setCellValue('D1', 'Người phụ trách')
                        ->setCellValue('E1', 'Quận Huyện')
                        ->setCellValue('F1', 'Tỉnh/TP')
                        ->setCellValue('G1', 'Vùng')
                        ->setCellValue('H1', 'Tên KH')
                        ->setCellValue('I1', 'Ghi chú đặt hàng')
                        ->setCellValue('J1', 'Nguồn')
                        ->setCellValue('K1', 'Kênh bán hàng')
                        ->setCellValue('L1', 'HT thanh toán')
                        ->setCellValue('M1', 'Tình trạng ĐH')
                        ->setCellValue('N1', 'Lý do')
                        ->setCellValue('O1', 'Phone')
                        ->setCellValue('P1', 'Team Leader')
                        ->setCellValue('Q1', 'Loại khách hàng')
                        ->setCellValue('R1', 'Giá trị đơn hàng')
                        ->setCellValue('S1', 'SL sản phẩm bán');
            $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "FBFF00"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '0000FF')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $i=2;
            $arrpayment = array(0=>"COD",1=>"Chuyển khoản");
            $array_status = array(
                    9=>'Chờ nhân viên điều phối',
                    8=>'Đơn hàng bị trả về',
                    7=>'Chuyển sang bộ phận giao hàng',
                    6=>'Đơn hàng bị trả về từ kế toán',
                    5=>'Đơn hàng chờ kế toán duyệt',
                    4=>'Đơn hàng bị trả về từ kho',
                    3=>'Đơn hàng mới chờ kho duyệt',
                    2=>'Đơn hàng nháp',
                    0=>'Đơn hàng thành công',
                    1=>'Đơn hàng hủy'
                );
            foreach($result as $row){
                    $customer = $row->CustomerType==1 ? 'Cũ' : 'Mới' ;
                    $userteam = isset($arr_user[$row->UserID]) ? $arr_user[$row->UserID] : '--' ;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, date('m/d/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('B'.$i, date('H:i:s',strtotime($row->Ngaydathang)))
                            ->setCellValue('C'.$i, $row->MaDH)
                            ->setCellValue('D'.$i, $row->UserName)
                            ->setCellValue('E'.$i, $row->Quanhuyen)
                            ->setCellValue('F'.$i, $row->Thanhpho)
                            ->setCellValue('G'.$i, $row->Area)
                            ->setCellValue('H'.$i, trim($row->Name))
                            ->setCellValue('I'.$i, $row->Note)
                            ->setCellValue('J'.$i, $row->Source)
                            ->setCellValue('K'.$i, $row->Kenh)
                            ->setCellValue('L'.$i, $arrpayment[$row->Payment])
                            ->setCellValue('M'.$i, $array_status[$row->Status])
                            ->setCellValue('N'.$i, $row->Ghichu)
                            ->setCellValue('O'.$i, $row->Phone1)
                            ->setCellValue('P'.$i, $userteam)
                            ->setCellValue('Q'.$i, $customer)
                            ->setCellValue('R'.$i, $row->Total)
                            ->setCellValue('S'.$i, $row->SoluongSP);
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BCTLS');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BCTLS.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bcbhmtgt($day_export='',$daystop_export=''){
        $type = $this->user->Channel==0 ? " and a.OrderType=0" : "" ;
        $type = $this->user->Channel==1 ? " and a.OrderType in(1,2,4,5)" : $type ;
        $type = $this->user->Channel==2 ? " and a.OrderType=3" : $type ;
        $type = $this->user->IsAdmin==1 || $this->user->UserType==7 ? "" : $type ;
        if($daystop_export==''){
            $result = $this->db->query("select DISTINCT a.*,f.MaSP,b.Name,b.Code,c.MaXK,c.Ngayxuatkho,c.KPP,d.MaKho,e.ProductsID,e.Amount from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse c,ttp_report_warehouse d,ttp_report_orderdetails e,ttp_report_products f where e.ProductsID=f.ID and e.OrderID=a.ID and d.ID=a.KhoID and c.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export' $type")->result();
        }else{
            $result = $this->db->query("select DISTINCT a.*,f.MaSP,b.Name,b.Code,c.MaXK,c.Ngayxuatkho,c.KPP,d.MaKho,e.ProductsID,e.Amount from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse c,ttp_report_warehouse d,ttp_report_orderdetails e,ttp_report_products f where e.ProductsID=f.ID and e.OrderID=a.ID and d.ID=a.KhoID and c.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)>='$day_export' and date(a.Ngaydathang)<='$daystop_export' $type")->result();
        }
            if(count($result)>0){
                error_reporting(E_ALL);
                ini_set('display_errors', TRUE);
                ini_set('display_startup_errors', TRUE);
                if (PHP_SAPI == 'cli')
                    die('This example should only be run from a Web Browser');
                require_once 'public/plugin/PHPExcel.php';
                $arr = array();
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                             ->setLastModifiedBy("Maarten Balliauw")
                                             ->setTitle("Office 2007 XLSX Test Document")
                                             ->setSubject("Office 2007 XLSX Test Document")
                                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                             ->setKeywords("office 2007 openxml php")
                                             ->setCategory("Test result file");
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A1', 'STT')
                            ->setCellValue('B1', 'Mã KH')
                            ->setCellValue('C1', 'Khách hàng')
                            ->setCellValue('D1', 'Địa chỉ')
                            ->setCellValue('E1', 'Ngày PXK')
                            ->setCellValue('F1', 'Số chứng từ')
                            ->setCellValue('G1', 'Đơn hàng CRM')
                            ->setCellValue('H1', 'Xuất kho')
                            ->setCellValue('I1', 'SL bán 7 Lọ')
                            ->setCellValue('J1', 'SL bán 14 Lọ')
                            ->setCellValue('K1', '04.000014')
                            ->setCellValue('L1', '04.000016')
                            ->setCellValue('M1', '04.000019')
                            ->setCellValue('N1', 'KM-2318')
                            ->setCellValue('O1', 'KM-2307')
                            ->setCellValue('P1', 'KM-2322')
                            ->setCellValue('Q1', 'Tiền chiết khấu')
                            ->setCellValue('R1', 'Các khoản thưởng')
                            ->setCellValue('S1', 'Tổng giảm')
                            ->setCellValue('T1', 'DT bán hàng pack7')
                            ->setCellValue('U1', 'DT bán hàng pack14')
                            ->setCellValue('V1', 'Tổng doanh thu')
                            ->setCellValue('W1', 'Thành tiền trước VAT')
                            ->setCellValue('X1', 'Tiền thuế')
                            ->setCellValue('Y1', 'Tổng')
                            ->setCellValue('Z1', 'Số tiền phải thanh toán')
                            ->setCellValue('AA1', 'Kênh')
                            ->setCellValue('AB1', 'Ngày đặt hàng');
                $arr_col = array(0=>'K',1=>'L',2=>'M',3=>'N',4=>'O',5=>'P');
                $arr_order = array();
                $arr_products = array();
                $arr_products_total = array();
                $i=2;
                $tong_adiva=0;
                foreach($result as $row){
                    if(!isset($arr_order[$row->ID])){
                        $temp = $i-1;
                        $tongtien = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
                        $dt_truocthue = $tongtien/1.1;
                        $thue = $dt_truocthue*0.1;
                        $row->Ngayxuatkho = date('d/m/Y',strtotime($row->Ngayxuatkho));
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$i, $temp)
                                ->setCellValue('B'.$i, $row->Code)
                                ->setCellValue('C'.$i, $row->Name)
                                ->setCellValue('D'.$i, $row->AddressOrder)
                                ->setCellValue('E'.$i, $row->Ngayxuatkho)
                                ->setCellValue('F'.$i, $row->MaXK)
                                ->setCellValue('G'.$i, $row->MaDH)
                                ->setCellValue('H'.$i, $row->MaKho)
                                ->setCellValue('Q'.$i, $row->Chietkhau)
                                ->setCellValue('R'.$i, $row->Reduce)
                                ->setCellValue('S'.$i, $row->Reduce+$row->Chietkhau)
                                ->setCellValue('X'.$i, $thue)
                                ->setCellValue('Y'.$i, $tongtien)
                                ->setCellValue('AA'.$i, $row->KPP)
                                ->setCellValue('AB'.$i, date('d/m/Y',strtotime($row->Ngaydathang)));
                        $arr_order[$row->ID] = $i;
                        $arr_products_total[$row->ProductsID] = isset($arr_products_total[$row->ProductsID]) ? $arr_products_total[$row->ProductsID]+$row->Total/1.1 : $row->Total/1.1;
                        $arr_products[$row->ProductsID] = isset($arr_products[$row->ProductsID]) ? $arr_products[$row->ProductsID]+$row->Amount : $row->Amount;
                        if($row->ProductsID==1){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$i,$arr_products[$row->ProductsID]);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i,$arr_products_total[$row->ProductsID]);
                        }elseif($row->ProductsID==2){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i,$arr_products[$row->ProductsID]);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$i,$arr_products_total[$row->ProductsID]);
                        }else{
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($arr_col[count($arr_products).'1'], $row->MaSP);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($arr_col[count($arr_products).$i]
                                ,$arr_products[$row->ProductsID]);
                        }
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i,$row->Total+$row->Chiphi);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$i,$row->Total+$row->Chiphi-$row->Reduce-$row->Chietkhau);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i,($row->Total+$row->Chiphi-$row->Reduce-$row->Chietkhau)*0.1);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$i,(($row->Total+$row->Chiphi-$row->Reduce-$row->Chietkhau)*0.1)+$row->Total+$row->Chiphi-$row->Reduce-$row->Chietkhau);
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i,(($row->Total+$row->Chiphi-$row->Reduce-$row->Chietkhau)*0.1)+$row->Total+$row->Chiphi-$row->Reduce-$row->Chietkhau);
                        $i++;
                    }else{
                        $cell = $arr_order[$row->ID];
                        $arr_products[$row->ProductsID] = isset($arr_products[$row->ProductsID]) ? $arr_products[$row->ProductsID]+$row->Amount : $row->Amount;
                        $arr_products_total[$row->ProductsID] = isset($arr_products_total[$row->ProductsID]) ? $arr_products_total[$row->ProductsID]+$row->Total/1.1 : $row->Total/1.1;
                        if($row->ProductsID==1){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cell,$arr_products[$row->ProductsID]);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$cell,$arr_products_total[$row->ProductsID]);
                        }elseif($row->ProductsID==2){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$cell,$arr_products[$row->ProductsID]);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$cell,$arr_products_total[$row->ProductsID]);
                        }else{
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($arr_col[count($arr_products).'1'], $row->MaSP);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($arr_col[count($arr_products).$i],$arr_products[$row->ProductsID]);
                        }
                    }
                }
                $objPHPExcel->getActiveSheet()->setTitle('BAOCAOBANHANG');
                $objPHPExcel->setActiveSheetIndex(0);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="BCBHMTGT.xls"');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header ('Cache-Control: cache, must-revalidate');
                header ('Pragma: public');
                
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                ob_end_clean();
                $objWriter->save('php://output');
                exit;
            }else{
                echo "Data is empty !. Can't export data .";
            }
    }

    public function export_bcbh($day_export='',$daystop_export=''){
        $type = $this->user->Channel==0 ? " and a.OrderType=0" : "" ;
        $type = $this->user->Channel==1 ? " and a.OrderType in(1,2,4,5)" : $type ;
        $type = $this->user->Channel==2 ? " and a.OrderType=3" : $type ;
        $type = $this->user->IsAdmin==1 || $this->user->UserType==7 ? "" : $type ;
        if($daystop_export==''){
            $result = $this->db->query("select DISTINCT a.*,b.Name,b.Phone1,c.MaXK,c.Ngayxuatkho,c.KPP,d.MaKho,e.ProductsID,e.Amount from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse c,ttp_report_warehouse d,ttp_report_orderdetails e where e.OrderID=a.ID and d.ID=a.KhoID and c.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export' $type")->result();
        }else{
            $result = $this->db->query("select DISTINCT a.*,b.Name,b.Phone1,c.MaXK,c.Ngayxuatkho,c.KPP,d.MaKho,e.ProductsID,e.Amount from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse c,ttp_report_warehouse d,ttp_report_orderdetails e where e.OrderID=a.ID and d.ID=a.KhoID and c.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)>='$day_export' and date(a.Ngaydathang)<='$daystop_export' $type")->result();
        }
            if(count($result)>0){
                error_reporting(E_ALL);
                ini_set('display_errors', TRUE);
                ini_set('display_startup_errors', TRUE);
                if (PHP_SAPI == 'cli')
                    die('This example should only be run from a Web Browser');
                require_once 'public/plugin/PHPExcel.php';
                $arr = array();
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                             ->setLastModifiedBy("Maarten Balliauw")
                                             ->setTitle("Office 2007 XLSX Test Document")
                                             ->setSubject("Office 2007 XLSX Test Document")
                                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                             ->setKeywords("office 2007 openxml php")
                                             ->setCategory("Test result file");
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A1', 'STT')
                            ->setCellValue('B1', 'Mã KH')
                            ->setCellValue('C1', 'Khách hàng')
                            ->setCellValue('D1', 'Địa chỉ')
                            ->setCellValue('E1', 'Ngày PXK')
                            ->setCellValue('F1', 'Số chứng từ')
                            ->setCellValue('G1', 'Đơn hàng CRM')
                            ->setCellValue('H1', 'Xuất kho')
                            ->setCellValue('I1', 'SL bán 7 Lọ')
                            ->setCellValue('J1', 'SL bán 14 Lọ')
                            ->setCellValue('K1', '04.000014')
                            ->setCellValue('L1', '04.000016')
                            ->setCellValue('M1', '04.000019')
                            ->setCellValue('N1', 'KM-2318')
                            ->setCellValue('O1', 'KM-2307')
                            ->setCellValue('P1', 'KM-2322')
                            ->setCellValue('Q1', 'KM-2308')
                            ->setCellValue('R1', 'KM-2298')
                            ->setCellValue('S1', 'KM-2319')
                            ->setCellValue('T1', 'DT bán hàng trước thuế')
                            ->setCellValue('U1', 'Tiền thuế')
                            ->setCellValue('V1', 'Tổng')
                            ->setCellValue('Z1', 'Số tiền phải thanh toán')
                            ->setCellValue('AA1', 'Chênh lệch')
                            ->setCellValue('AB1', 'Kênh')
                            ->setCellValue('AC1', 'Ngày đặt hàng');
                $arr_order = array();
                $i=2;
                foreach($result as $row){
                    if(!isset($arr_order[$row->ID])){
                        $temp = $i-1;
                        $tongtien = $row->Total+$row->Chiphi-$row->Chietkhau;
                        $dt_truocthue = $tongtien/1.1;
                        $thue = $dt_truocthue*0.1;
                        $row->Ngayxuatkho = date('d/m/Y',strtotime($row->Ngayxuatkho));
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$i, $temp)
                                ->setCellValue('B'.$i, $row->Phone1)
                                ->setCellValue('C'.$i, $row->Name)
                                ->setCellValue('D'.$i, $row->AddressOrder)
                                ->setCellValue('E'.$i, $row->Ngayxuatkho)
                                ->setCellValue('F'.$i, $row->MaXK)
                                ->setCellValue('G'.$i, $row->MaDH)
                                ->setCellValue('H'.$i, $row->MaKho)
                                ->setCellValue('T'.$i, $dt_truocthue)
                                ->setCellValue('U'.$i, $thue)
                                ->setCellValue('V'.$i, $tongtien)
                                ->setCellValue('Z'.$i, $tongtien)
                                ->setCellValue('AA'.$i, '-'.$tongtien)
                                ->setCellValue('AB'.$i, 'OL')
                                ->setCellValue('AC'.$i, date('d/m/Y',strtotime($row->Ngaydathang)));
                        $arr_order[$row->ID] = $i;
                        if($row->ProductsID==1){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==2){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==36){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==33){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==34){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==22){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==31){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==25){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==19){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==14){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i,$row->Amount);    
                        }
                        if($row->ProductsID==21){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$i,$row->Amount);    
                        }
                        $i++;
                    }else{
                        $cell = $arr_order[$row->ID];
                        if($row->ProductsID==1){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==2){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==36){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==33){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==34){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==22){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==31){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==25){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==19){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==14){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$cell,$row->Amount);    
                        }
                        if($row->ProductsID==21){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$cell,$row->Amount);
                        }
                    }
                }
                $objPHPExcel->getActiveSheet()->setTitle('BAOCAOBANHANG');
                $objPHPExcel->setActiveSheetIndex(0);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="BCBH.xls"');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header ('Cache-Control: cache, must-revalidate');
                header ('Pragma: public');
                
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                ob_end_clean();
                $objWriter->save('php://output');
                exit;
            }else{
                echo "Data is empty !. Can't export data .";
            }
    }

    public function export_onlinedetails($day_export='',$daystop_export=''){
        $type = $this->user->Channel==0 ? " and a.OrderType=0" : "" ;
        $type = $this->user->Channel==1 ? " and a.OrderType in(1,2,4,5)" : $type ;
        $type = $this->user->Channel==2 ? " and a.OrderType=3" : $type ;
        $type = $this->user->IsAdmin==1 || $this->user->UserType==7 ? "" : $type ;
        if($daystop_export!=''){
            $result = $this->db->query("select DISTINCT a.MaDH,a.Payment,a.ID,a.Chiphi,a.Total as TotalPrice,a.Chietkhau as TotalChietkhau,a.Reduce,a.AddressOrder,a.Ngaydathang,b.Name,b.Code,c.MaXK,c.Ngayxuatkho,d.MaKho,f.Title as Tensanpham,f.MaSP,e.Amount,e.Total,e.Price,e.PriceDown as Chietkhau,g.MisaCode from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse c,ttp_report_warehouse d,ttp_report_orderdetails e,ttp_report_products f,ttp_report_city g where a.CityID=g.ID and e.ProductsID=f.ID and e.OrderID=a.ID and d.ID=a.KhoID and a.ID=c.OrderID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export' and date(a.Ngaydathang)<='$daystop_export' $type")->result();
        }else{ 
            $result = $this->db->query("select DISTINCT a.MaDH,a.Payment,a.ID,a.Chiphi,a.Total as TotalPrice,a.Chietkhau as TotalChietkhau,a.Reduce,a.AddressOrder,a.Ngaydathang,b.Name,b.Code,c.MaXK,c.Ngayxuatkho,d.MaKho,f.Title as Tensanpham,f.MaSP,e.Amount,e.Total,e.Price,e.PriceDown as Chietkhau,g.MisaCode from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse c,ttp_report_warehouse d,ttp_report_orderdetails e,ttp_report_products f,ttp_report_city g where a.CityID=g.ID and e.ProductsID=f.ID and e.OrderID=a.ID and d.ID=a.KhoID and a.ID=c.OrderID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export' $type")->result();
        }

        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Mã KH')
                        ->setCellValue('B1', 'Khách hàng')
                        ->setCellValue('C1', 'Địa chỉ giao hàng')
                        ->setCellValue('D1', 'Ngày đơn hàng')
                        ->setCellValue('E1', 'Số chứng từ')
                        ->setCellValue('F1', 'Nhân viên bán hàng')
                        ->setCellValue('G1', 'Xuất kho')
                        ->setCellValue('H1', 'Mã SP')
                        ->setCellValue('I1', 'Tên sản phẩm')
                        ->setCellValue('J1', 'Giá bán')
                        ->setCellValue('K1', 'Số lượng')
                        ->setCellValue('L1', 'Thành tiền')
                        ->setCellValue('M1', 'Chiết khấu / sản phẩm')
                        ->setCellValue('N1', 'Doanh thu sau chiết khấu')
                        ->setCellValue('O1', 'Chi phí vận chuyển')
                        ->setCellValue('P1', 'Chiết khấu / đơn hàng')
                        ->setCellValue('Q1', 'Tổng thanh toán')
                        ->setCellValue('R1', 'Phương thức thanh toán')
                        ->setCellValue('S1', 'Ngày PXK')
                        ->setCellValue('T1', 'Tỉnh thành (Misa)');

            $objPHPExcel->getActiveSheet()->getStyle("A1:R1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "E4FF00"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '000000')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            ));
            $i=2;
            $payment = array(0=>"COD",1=>"Chuyển khoản",2=>"Tiền mặt");
            $arr_order = array();
            foreach($result as $row){
                    $tongtien = $row->Total;
                    $dt_truocthue = $tongtien/1.1;
                    $thue = $dt_truocthue*0.1;
                    $row->Ngayxuatkho = date('d/m/Y',strtotime($row->Ngayxuatkho));
                    $row->Ngaydathang = date('d/m/Y H:i',strtotime($row->Ngaydathang));
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->Code)
                            ->setCellValue('B'.$i, $row->Name)
                            ->setCellValue('C'.$i, $row->AddressOrder)
                            ->setCellValue('D'.$i, $row->Ngaydathang)
                            ->setCellValue('E'.$i, $row->MaXK)
                            ->setCellValue('F'.$i, $row->MaDH)
                            ->setCellValue('G'.$i, $row->MaKho)
                            ->setCellValue('H'.$i, $row->MaSP)
                            ->setCellValue('I'.$i, $row->Tensanpham)
                            ->setCellValue('J'.$i, $row->Price)
                            ->setCellValue('K'.$i, $row->Amount)
                            ->setCellValue('L'.$i, $row->Total)
                            ->setCellValue('M'.$i, $row->Chietkhau)
                            ->setCellValue('N'.$i, $row->Total-$row->Chietkhau);
                    if(!isset($arr_order[$row->ID])){
                        $arr_order[$row->ID]='';
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('P'.$i, $row->TotalChietkhau)
                            ->setCellValue('O'.$i, $row->Chiphi)
                            ->setCellValue('Q'.$i, $row->TotalPrice+$row->Chiphi-$row->TotalChietkhau)
                            ->setCellValue('R'.$i, $payment[$row->Payment])
                            ->setCellValue('S'.$i, $row->Ngayxuatkho)
                            ->setCellValue('T'.$i, $row->MisaCode);
                    }
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BCOL');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BCOL_DETAILS.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bcol($day_export){
        $result = $this->db->query("select DISTINCT a.MaDH,a.AddressOrder,a.Ngaydathang,b.Name,b.Phone1,c.MaXK,c.Ngayxuatkho,d.MaKho,f.Title as Tensanpham,f.MaSP,e.Amount,e.Total,e.Price,e.PriceDown,g.MisaCode from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse c,ttp_report_warehouse d,ttp_report_orderdetails e,ttp_report_products f,ttp_report_city g where a.CityID=g.ID and e.ProductsID=f.ID and e.OrderID=a.ID and d.ID=a.KhoID and a.ID=c.OrderID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export'")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Mã KH')
                        ->setCellValue('B1', 'Khách hàng')
                        ->setCellValue('C1', 'Địa chỉ giao hàng')
                        ->setCellValue('D1', 'Ngày đơn hàng')
                        ->setCellValue('E1', 'Số chứng từ')
                        ->setCellValue('F1', 'Đơn hàng CRM')
                        ->setCellValue('G1', 'Xuất kho')
                        ->setCellValue('H1', 'Mã SP')
                        ->setCellValue('I1', 'Tên sản phẩm')
                        ->setCellValue('J1', 'Giá bán')
                        ->setCellValue('K1', 'Số lượng')
                        ->setCellValue('L1', 'Tổng cộng')
                        ->setCellValue('M1', 'DT bán hàng trước thuế')
                        ->setCellValue('N1', 'Tiền thuế')
                        ->setCellValue('O1', 'Số tiền phải thanh toán')
                        ->setCellValue('P1', 'Ngày PXK')
                        ->setCellValue('Q1', 'Tỉnh thành (Misa)');

            $objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "E4FF00"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '000000')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            ));
            $i=2;
            foreach($result as $row){
                    $tongtien = $row->Total;
                    $dt_truocthue = $tongtien/1.1;
                    $thue = $dt_truocthue*0.1;
                    $row->Ngayxuatkho = date('d/m/Y',strtotime($row->Ngayxuatkho));
                    $row->Ngaydathang = date('d/m/Y H:i',strtotime($row->Ngaydathang));
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->Phone1)
                            ->setCellValue('B'.$i, $row->Name)
                            ->setCellValue('C'.$i, $row->AddressOrder)
                            ->setCellValue('D'.$i, $row->Ngaydathang)
                            ->setCellValue('E'.$i, $row->MaXK)
                            ->setCellValue('F'.$i, $row->MaDH)
                            ->setCellValue('G'.$i, $row->MaKho)
                            ->setCellValue('H'.$i, $row->MaSP)
                            ->setCellValue('I'.$i, $row->Tensanpham)
                            ->setCellValue('J'.$i, $row->Price)
                            ->setCellValue('K'.$i, $row->Amount)
                            ->setCellValue('L'.$i, $row->Total)
                            ->setCellValue('M'.$i, $dt_truocthue)
                            ->setCellValue('N'.$i, $thue)
                            ->setCellValue('O'.$i, $row->Total)
                            ->setCellValue('P'.$i, $row->Ngayxuatkho)
                            ->setCellValue('Q'.$i, $row->MisaCode);
                    $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BCOL');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BCOL.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function export_bcbhck($day_export){
        $result = $this->db->query("select DISTINCT a.*,b.Name,b.Phone1,c.MaXK,c.Ngayxuatkho,d.MaKho,e.UserName from ttp_report_order a,ttp_report_customer b,ttp_report_export_warehouse c,ttp_report_warehouse d,ttp_user e where a.UserID=e.ID and d.ID=a.KhoID and c.OrderID=a.ID and b.ID=a.CustomerID and date(a.Ngaydathang)='$day_export'")->result();
            if(count($result)>0){
                error_reporting(E_ALL);
                ini_set('display_errors', TRUE);
                ini_set('display_startup_errors', TRUE);
                if (PHP_SAPI == 'cli')
                    die('This example should only be run from a Web Browser');
                require_once 'public/plugin/PHPExcel.php';
                $arr = array();
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                             ->setLastModifiedBy("Maarten Balliauw")
                                             ->setTitle("Office 2007 XLSX Test Document")
                                             ->setSubject("Office 2007 XLSX Test Document")
                                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                             ->setKeywords("office 2007 openxml php")
                                             ->setCategory("Test result file");
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A1', 'STT')
                            ->setCellValue('B1', 'Mã KH')
                            ->setCellValue('C1', 'Khách hàng')
                            ->setCellValue('D1', 'Địa chỉ')
                            ->setCellValue('E1', 'Ngày ĐH')
                            ->setCellValue('F1', 'Số chứng từ')
                            ->setCellValue('G1', 'Đơn hàng CRM')
                            ->setCellValue('H1', 'Xuất kho')
                            ->setCellValue('I1', 'Giá trị chiết khấu')
                            ->setCellValue('J1', 'Giá trị sau chiết khấu')
                            ->setCellValue('K1', 'DT bán hàng trước thuế')
                            ->setCellValue('L1', 'Tiền thuế')
                            ->setCellValue('M1', 'Số tiền phải thanh toán')
                            ->setCellValue('N1', 'Ngày PXK')
                            ->setCellValue('O1', 'Tỉnh thành (Misa)');
                $arr_order = array();
                $i=2;
                foreach($result as $row){
                        $tongtien = $row->Total+$row->Chiphi-$row->Chietkhau;
                        $dt_truocthue = $tongtien/1.1;
                        $thue = $dt_truocthue*0.1;
                        $row->Ngayxuatkho = date('d/m/Y',strtotime($row->Ngayxuatkho));
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$i, $i-1)
                                ->setCellValue('B'.$i, $row->Phone1)
                                ->setCellValue('C'.$i, $row->Name)
                                ->setCellValue('D'.$i, $row->AddressOrder)
                                ->setCellValue('E'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                                ->setCellValue('F'.$i, $row->MaXK)
                                ->setCellValue('G'.$i, $row->UserName)
                                ->setCellValue('H'.$i, $row->MaKho)
                                ->setCellValue('I'.$i, $row->Chietkhau)
                                ->setCellValue('J'.$i, $row->Total-$row->Chietkhau)
                                ->setCellValue('K'.$i, $dt_truocthue)
                                ->setCellValue('L'.$i, $thue)
                                ->setCellValue('M'.$i, $tongtien)
                                ->setCellValue('N'.$i, $row->Ngayxuatkho)
                                ->setCellValue('O'.$i, '');
                    $i++;
                }
                $objPHPExcel->getActiveSheet()->setTitle('BAOCAOBANHANG');
                $objPHPExcel->setActiveSheetIndex(0);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="BCBH.xls"');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header ('Cache-Control: cache, must-revalidate');
                header ('Pragma: public');
                
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                ob_end_clean();
                $objWriter->save('php://output');
                exit;
            }else{
                echo "Data is empty !. Can't export data .";
            }
    }

    public function save_transport_export($arr){
        if(is_array($arr)){
            $arr = implode(",",$arr);
            $arr = "update ttp_report_export_warehouse set TransportStatus=1 where OrderID in ($arr)";
            $this->db->query($arr);
        }
    }

    public function dichvukhaigia($price=0){
        $arr = array(
            1=>'1 - 0 - 1,000,000 VNĐ',
            2=>'2 - 1,000,001 - 2,000,000 VNĐ',
            3=>'3 - 2,000,001 - 3,500,000 VNĐ',
            4=>'4 - 3,500,001 - 5,000,000 VNĐ',
            5=>'5 - 5,000,001 - 10,000,000 VNĐ',
            6=>'6 - 10,000,001 - 15,000,000 VNĐ',
            7=>'7 - 15,000,001 - 20,000,000 VNĐ',
            8=>'8 - Từ 20,000,001 VNĐ trở lên'
        );
        if($price>=0 && $price<=1000000) return $arr[1];
        if($price>1000000 && $price<=2000000) return $arr[2];
        if($price>2000000 && $price<=3500000) return $arr[3];
        if($price>3500000 && $price<=5000000) return $arr[4];
        if($price>5000000 && $price<=10000000) return $arr[5];
        if($price>10000000 && $price<=15000000) return $arr[6];
        if($price>15000000 && $price<=20000000) return $arr[7];
        if($price>20000000) return $arr[8];
    }

    public function paymenttype($payment){
        $arr = array(
            0=>'2 - Người nhận trả tiền',
            1=>'1 - Người gửi trả tiền'
        );
        if(array_key_exists($payment,$arr)){
            return $arr[$payment];
        }
        return '';
    }

    public function paymentgold($payment){
        $arr = array(
            0=>'1_Thu cả tiền phí vận chuyển và tiền hàng',
            1=>'4_Không thu'
        );
        if(array_key_exists($payment,$arr)){
            return $arr[$payment];
        }
        return '';
    }

    /***** Update status order base on file Excel ******/
    public function update_status(){
        $file = $this->Upload_xls();
        if(file_exists($file)){
            $result = array();
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow         = $worksheet->getHighestRow();
                $highestColumn      = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for ($row = 2; $row <= $highestRow; $row ++) {
                    $MaDH = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                    $Status = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $Ghichu = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $result[] = array(
                        'MaDH'  => $MaDH,
                        'Status'  => $Status,
                        'Ghichu'  => $Ghichu
                    );
                }
            }
            if(count($result)>0){
                $this->template->add_title("Cập nhật trạng thái cho đơn hàng");
                $this->template->write_view('content','admin/import_preview_xls_logigics',array('data'=>$result));
                $this->template->render();
                return;
            }
        }
        echo "This file is invalid !";
    }

    public function sync_status(){
        $MaDH = isset($_POST['MaDH']) ? $_POST['MaDH'] : '' ;
        $Status = isset($_POST['Status']) ? $_POST['Status'] : '' ;
        $Status = $Status=='' ? 0 : (int)$Status ;
        $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
        if($MaDH!='' && is_numeric($Status)){
            $order = $this->db->query("select a.* from ttp_report_export_warehouse a,ttp_report_order b where a.OrderID=b.ID and a.MaXK='$MaDH'")->row();
            if($order){
                if($Status==1){
                    /*$checkreason = $this->db->query("select * from ttp_report_order_reason_details where OrderID=$order->OrderID")->row();
                    if($checkreason){
                        $this->db->query("update ttp_report_order_reason_details set PLReason = 0,Another3PL='$Ghichu' where OrderID=$order->OrderID");
                    }else{
                        $this->db->query("insert into ttp_report_order_reason_details(OrderID,PLReason,Source,Another3PL) value($order->OrderID,0,1,'$Ghichu')");
                    }
                    $details = $this->db->query("select Amount,ProductsID,ShipmentID,ImportPrice from ttp_report_orderdetails where OrderID=$order->OrderID")->result();
                    if(count($details)>0){
                        $arr  = array();
                        $slsp = 0;
                        $total= 0;
                        foreach($details as $row){
                            $slsp = $slsp+$row->Amount;
                            $total +=$row->ImportPrice*$row->Amount;
                            $totaltemp = $row->ImportPrice*$row->Amount;
                            $arr[] = "(ImportIDDV,$row->ProductsID,$row->ShipmentID,'VND',1,$row->ImportPrice,$row->Amount,$totaltemp,$totaltemp)";
                        }
                        if(count($arr)>0){
                            $thismonth = date('m',time());
                            $thisyear = date('Y',time());
                            $max = $this->db->query("select count(1) as max from ttp_report_inventory_import where MONTH(Created)=$thismonth and YEAR(Created)=$thisyear and Type=1")->row();
                            $max = $max ? $max->max + 1 : 1 ;
                            $thisyear = date('y',time());
                            $max = "YCNK1".'_'.$order->KhoID.'_'.$thisyear.$thismonth.'_'.str_pad($max, 7, '0', STR_PAD_LEFT);

                            $data = array(
                                'MaNK'      => $max,
                                'ExportID'  => $order->ID,
                                'KhoID'     => $order->KhoID,
                                'UserID'    => $this->user->ID,
                                'NgayNK'    => date('Y-m-d'),
                                'Type'      => 1,
                                'Note'      => $Ghichu,
                                'Status'    => 0,
                                'TotalAmount'=> $slsp,
                                'TotalPrice'=> $total,
                                'Created'   => date('Y-m-d H:i:s'),
                                'LastEdited'=> date('Y-m-d H:i:s')
                            );
                            $this->db->insert('ttp_report_inventory_import',$data);
                            $ID = $this->db->insert_id();
                            $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Amount,TotalCurrency,TotalVND) values".implode(',',$arr);
                            $arr = str_replace('ImportIDDV',$ID,$arr);
                            $this->db->query($arr);
                            $data_his = array(
                                'ImportID'  => $ID,
                                'Status'    => 0,
                                'Note'      => $Ghichu,
                                'UserID'    => $this->user->ID,
                                'Created'   => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('ttp_report_inventory_import_history',$data_his);
                        }
                        $this->db->query("update ttp_report_order set Status=$Status,Ghichu='$Ghichu',HistoryEdited='".date('Y-m-d H:i:s')."' where ID=$order->OrderID");
                    }*/
                    echo "False";
                    return;
                }else{
                    $this->db->query("update ttp_report_order set Status=$Status,Ghichu='$Ghichu',HistoryEdited='".date('Y-m-d H:i:s')."' where ID=$order->OrderID");
                }
                $datahis = array(
                    'OrderID'=>$order->ID,
                    'Thoigian'=>date('Y-m-d H:i:s',time()),
                    'Status'=>$Status,
                    "Ghichu"=>$Ghichu,
                    "UserID"=>$this->user->ID
                );
                $this->db->insert('ttp_report_orderhistory',$datahis);
            }
            echo "OK";
        }else{
            echo "False";
        }
    }

    public function Upload_xls(){
        $this->load->library("upload");
        $this->upload->initialize(array(
            "upload_path"   => "./assets/logitics",
            'allowed_types' => 'xls|xlsx',
            'max_size'      => '3000',
            'encrypt_name' => TRUE
        ));
        if($this->upload->do_upload("Image_upload")){
            $image_data = $this->upload->data();
            return "assets/logitics/".$image_data['file_name'];
        }else{
            $error = $this->upload->display_errors();
            echo $error;
            return '';
        }
        return '';
    }

    public function check_notify(){
        $max = isset($_POST['Max']) ? $_POST['Max'] : 0 ;
        $bonus = "";
        $bonus = $this->user->UserType==5 ? " and b.UserType=1" : $bonus ;
        $bonus = $this->user->UserType==7 ? " and b.UserType=3" : $bonus ;
        $bonus = $this->user->UserType==8 ? " and b.UserType=2" : $bonus ;
        $cancel = $this->db->query("select count(1) as Soluong from ttp_report_request_cancelorder a,ttp_user b where a.Status=0 and a.UserID=b.ID $bonus")->row();
        if($cancel->Soluong==$max){
            echo "false";
            return;
        }else{
            echo $cancel->Soluong;
            $result = $this->db->query("select b.UserName,b.Thumb,c.MaDH,a.UserAccept from ttp_report_request_cancelorder a,ttp_user b,ttp_report_order c where a.OrderID=c.ID and a.Status=0 and a.UserID=b.ID $bonus order by a.LastEdited DESC")->row();
            if($result){
                echo $result->UserAccept==0 ? "|0" : "|1" ;
                echo "|".$result->UserName;
                echo "|".$result->MaDH;
                echo $result->Thumb!='' ? "|".base_url().$result->Thumb : "|".base_url()."public/site/images/icon24.png";
                if($result->UserAccept>0){
                    $user = $this->db->query("select UserName,Thumb from ttp_user where ID=$result->UserAccept")->row();
                    if($user){
                        echo "|".$user->UserName;
                        echo $user->Thumb!='' ? "|".base_url().$user->Thumb : "|".base_url()."public/site/images/icon24.png";;
                    }
                }
            }
        }
    }

    public function changestatus_onoff(){
        if(isset($_POST['Status'])){
            $toggle = $_POST['Status']==0 ? 0 : 1 ;
            file_put_contents('log/status/onoff.txt',$toggle);
        }
    }

    /** Test Sync Databse Ceramic **/

    public function sync_categories(){
        //$file = 'export_ceramic/category-data.xlsx';
        //$file = 'export_ceramic/brand-data.xlsx';
        //$file = 'export_ceramic/product-data.xlsx';
        //$file = 'customer_MTGT/GT.xlsx';
        ini_set('memory_limit', '3500M');
        $file = 'customer_MTGT/gomsu1.xlsx';
        if(file_exists($file)){
            $result = array();
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow         = $worksheet->getHighestRow();
                $highestColumn      = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for ($row = 1; $row <= $highestRow; $row ++) {
                    $Code = trim($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                    if(isset($result[$Code])){
                        $result[$Code] = $result[$Code]+1;
                    }else{
                        $result[$Code]=1;
                    }
                }
            }
            echo "<table><tr><td>Ma san pham</td><td>So luong</td></tr>";
            foreach($result as $key=>$value){
                echo "<tr><td>$key</td><td>$value</td></tr>";
            }
            echo "<table>";
            //echo "insert into ceramicdata_categories(ID,Title,ParentID,DestinationID,DestinationParentID) values".implode(',',$result);
            //echo "insert into ttp_report_trademark(ID,Title) values".implode(',',$result);
            //echo "insert into ttp_report_products(Title,MaSP,Price,CategoriesID,TrademarkID,Length,Width,Height) values".implode(',',$result);
            //echo "insert into ttp_report_customer(AreaID,SystemID,Type,AreaNote,Code,Name,Address,Taxcode,AddressOrder,Surrogate,Phone1,Birthday,CreatedCustomer,Email) values".implode(',',$result);
            //echo "insert into ttp_report_customer(AreaID,SystemID,Type,Code,Name,Address,AddressOrder,Phone) values".implode(',',$result);
        }
    }
}
?>