<?php 
class Manager extends Admin_Controller { 
 	public $user;
 	public $classname="manager";

    public function __construct() { 
        parent::__construct(); 
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
	$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
	public function index(){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
            $this->template->add_title('Manager | Report Tools');
            $this->template->write_view('content','admin/manager_content');
            $this->template->render();
	}
}
?>
