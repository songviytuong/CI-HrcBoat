<?php 
class Warehouse_inventory_position extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="warehouse_inventory_position";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $result = $this->db->query("select KhoID,sum(WaitingOnRack) as WaitingOnRack from ttp_report_inventory_import group by KhoID")->row();
        $import = $this->db->query("select * from ttp_report_inventory_import where WaitingOnRack>0")->result();
        $data = array(
            'base_link' => base_url().ADMINPATH.'/report/warehouse_inventory_position/',
            'result'    => $result,
            'data'      => $import
        );
        $this->template->add_title('Inventory Position | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_position_home',$data);
        $this->template->render();
    }
}