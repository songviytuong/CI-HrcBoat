<?php 
class Warehouse_inventory_perchaseorder extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="warehouse_inventory_perchaseorder";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $bonus='';
        if($this->user->IsAdmin==0 && $this->user->UserType!=7){
            $bonus = " and c.ID=".$this->user->ID;
        }
        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $nav = $this->db->query("select count(1) as nav from ttp_report_perchaseorder a,ttp_report_production b,ttp_user c where a.ProductionID=b.ID and a.UserID=c.ID $fillter $bonus")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,c.UserName,b.Title from ttp_report_perchaseorder a,ttp_report_production b,ttp_user c where a.ProductionID=b.ID and a.UserID=c.ID $fillter $bonus order by a.ID DESC $limit_str")->result();
        $data = array(
            'base_link' => base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/',
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'data'      => $object,
            'start'     => $start,
            'find'      => $nav,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'nav'       => $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Purchase Order | Warehouse Report Tools');
        $view = 'warehouse_inventory_perchaseorder_home';
        $this->template->write_view('content','admin/'.$view,$data);
		$this->template->render();
	}

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Create Purchase Order | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_perchaseorder_add',array('base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/'));
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $DatePO = isset($_POST['DatePO']) ? $_POST['DatePO'] : date('Y-m-d') ;
        $DateExpected = isset($_POST['DateExpected']) ? $_POST['DateExpected'] : date('Y-m-d') ;
        $ProductionID = isset($_POST['ProductionID']) ? $_POST['ProductionID'] : 0 ;
        $WarehouseID = isset($_POST['WarehouseID']) ? $_POST['WarehouseID'] : 0 ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $HardCode = isset($_POST['HardCode']) ? $_POST['HardCode'] : '' ;
        $Status = isset($_POST['Status']) ? $_POST['Status'] : '' ;
        $Address = $this->db->query("select Address from ttp_report_production where ID=$ProductionID")->row();
        $Address = $Address ? $Address->Address : '' ;
        $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
        if(in_array(0,$ShipmentID)){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            echo "<script>alert('Vui lòng chọn lô hàng cho sản phẩm . Tất cả các sản phẩm buộc phải có lô !');window.location='".$_SERVER['HTTP_REFERER']."'</script>";
            return;
        }
        if($DatePO!='' && $WarehouseID>0){
            $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
            $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
            $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
            $Currency = isset($_POST['Currency']) ? $_POST['Currency'] : array() ;
            $ValueCurrency = isset($_POST['ValueCurrency']) ? $_POST['ValueCurrency'] : array() ;
            $PriceCurrency = isset($_POST['PriceCurrency']) ? $_POST['PriceCurrency'] : array() ;
            $TotalCurrency = isset($_POST['TotalCurrency']) ? $_POST['TotalCurrency'] : array() ;
            $TotalVND = isset($_POST['TotalVND']) ? $_POST['TotalVND'] : array() ;
            if(count($ProductsID)>0){
                $arr = array();
                $arr_request = array();
                $arrnext = array();
                $total = 0;
                $slsp = 0;
                foreach($ProductsID as $key=>$row){
                    $Currency_row = isset($Currency[$key]) ? $Currency[$key] : 0 ;
                    $Amount_row = isset($Amount[$key]) ? $Amount[$key] : 0 ;
                    $ValueCurrency_row = isset($ValueCurrency[$key]) ? $ValueCurrency[$key] : 0 ;
                    $PriceCurrency_row = isset($PriceCurrency[$key]) ? $PriceCurrency[$key] : 0 ;
                    $TotalCurrency_row = $Amount_row*$PriceCurrency_row;
                    $TotalVND_row = ($Amount_row*$PriceCurrency_row)*$ValueCurrency_row;
                    $Shipment_row = isset($ShipmentID[$key]) ? $ShipmentID[$key] : 0 ;
                    if($TotalCurrency_row>=0 && $TotalVND_row>=0){
                        $slsp = $slsp+$Amount_row;
                        $total +=$TotalVND_row;
                        $arr[] = "(POIDDV,$row,'$Currency_row',$ValueCurrency_row,$PriceCurrency_row,$Amount_row,$TotalCurrency_row,$TotalVND_row,$Shipment_row)";
                        if($Status==2 || $Status==3 || $Status==4 || $Status==5){
                            $arr_request[] = "(ImportIDDV,$row,$Shipment_row,'$Currency_row',$ValueCurrency_row,$PriceCurrency_row,$Amount_row,$TotalCurrency_row,$TotalVND_row)";
                        }
                    }
                }
                if(count($arr)>0){
                    $thismonth = date('m',time());
                    $thisyear = date('Y',time());
                    $max = $this->db->query("select count(1) as max from ttp_report_perchaseorder where MONTH(Created)=$thismonth and YEAR(Created)=$thisyear and DATE(Created)>='".date('Y-m-d')."' and Type=".$this->user->DepartmentID)->row();
                    $max = $max ? $max->max + 1 : 1 ;
                    $thisyear = date('y',time());
                    $department = $this->db->query("select * from ttp_department where ID=".$this->user->DepartmentID)->row();
                    $code = $department ? $department->Code : '' ;
                    $max = "PO".$code.$thisyear.$thismonth.'_'.str_pad($max, 5, '0', STR_PAD_LEFT);

                    $data = array(
                        'POCode'    => $max,
                        'HardCode'  => $HardCode,
                        'UserID'    => $this->user->ID,
                        'DatePO'    => $DatePO,
                        'ProductionID'=> $ProductionID,
                        'Note'      => $Note,
                        'Address'   => $Address,
                        'Status'    => $Status,
                        'TotalAmount'=> $slsp,
                        'TotalPrice'=> $total,
                        'DateExpected'=>$DateExpected,
                        'WarehouseID'=>$WarehouseID,
                        'Created'   => date('Y-m-d H:i:s'),
                        'Type'      => $this->user->DepartmentID
                    );
                    $data_files = $this->Upload_attachment();
                    $data['Files'] = json_encode($data_files);
                    $this->db->insert('ttp_report_perchaseorder',$data);
                    $ID = $this->db->insert_id();
                    $arr = "insert into ttp_report_perchaseorder_details(POID,ProductsID,Currency,ValueCurrency,PriceCurrency,Amount,TotalCurrency,TotalVND,ShipmentID) values".implode(',',$arr);
                    $arr = str_replace('POIDDV',$ID,$arr);
                    $this->db->query($arr);
                    $data_his = array(
                        'POID'      => $ID,
                        'Status'    => $Status,
                        'Note'      => $Note,
                        'UserID'    => $this->user->ID,
                        'Created'   => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('ttp_report_perchaseorder_history',$data_his);
                    if($Status==2 || $Status==3 || $Status==4 || $Status==5){
                        $data['ID'] = $ID;
                        $this->create_request_import($data,$arr_request);
                    }
                }
            }
        }
        redirect(ADMINPATH.'/report/warehouse_inventory_perchaseorder');
    }

    public function Upload_attachment(){
        $this->load->library("upload");
        $this->upload->initialize(array(
            "upload_path"   => "./assets/purchaseorder",
            'allowed_types' => 'doc|docx|pdf|xls|xlsx',
            'max_size'      => '3000',
            'encrypt_name' => true
        ));
        if($this->upload->do_multi_upload("Images_upload")){
            $url_thumb = array();
            $data_file = $this->upload->get_multi_upload_data();
            for($i=0; $i<count($_FILES['Images_upload']["name"]); $i++){
                $url_thumb[] = "assets/purchaseorder/".$data_file[$i]['file_name'];
            }
            return $url_thumb;
        }
        return array();
    }

    public function remove_file($id=0,$file=0){
        if($file>0 && is_numeric($file)){
            $result = $this->db->query("select ID,Files from ttp_report_perchaseorder where ID=$id")->row();
            if($result){
                $files = json_decode($result->Files,true);
                if(is_array($files) && count($files)>0){
                    unset($files[$file-1]);
                    $files = json_encode($files);
                    $this->db->query("update ttp_report_perchaseorder set Files = '$files' where ID=$id");
                }
            }
        }
        redirect(ADMINPATH.'/report/warehouse_inventory_perchaseorder/edit/'.$id);
    }

    public function create_request_import($data=array(),$arr = array()){
        $KhoID = isset($data['WarehouseID']) ? $_POST['WarehouseID'] : 0 ;
        $POID = isset($data['ID']) ? $data['ID'] : 0 ;
        $ProductionID = isset($data['ProductionID']) ? $data['ProductionID'] : 0 ;
        $DateExpected = isset($data['DateExpected']) ? $data['DateExpected'] : date('Y-m-d') ;
        $POCode = isset($data['POCode']) ? $data['POCode'] : $POID ;
        $TotalAmount = isset($data['TotalAmount']) ? $data['TotalAmount'] : 0 ;
        $TotalPrice = isset($data['TotalPrice']) ? $data['TotalPrice'] : 0 ;
        
        $Type=0;
        if($KhoID>0 && $POID>0){
            if(count($arr)>0){
                $thismonth = date('m',time());
                $thisyear = date('Y',time());
                $max = $this->db->query("select count(1) as max from ttp_report_inventory_import where MONTH(Created)=$thismonth and YEAR(Created)=$thisyear and Type=0")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
                $max = "YCNK".$Type.'_'.$KhoID.'_'.$thisyear.$thismonth.'_'.str_pad($max, 7, '0', STR_PAD_LEFT);

                $data = array(
                    'MaNK'      => $max,
                    'POID'      => $POID,
                    'KhoID'     => $KhoID,
                    'UserID'    => $this->user->ID,
                    'NgayNK'    => $DateExpected,
                    'Type'      => $Type,
                    'ProductionID'=> $ProductionID,
                    'Note'      => "Yêu cầu nhập kho từ PO số $POCode",
                    'Status'    => 0,
                    'Ghichu'    => "Yêu cầu nhập kho tự động từ PO số $POCode",
                    'TotalAmount'=> $TotalAmount,
                    'TotalPrice'=> $TotalPrice,
                    'Created'   => date('Y-m-d H:i:s'),
                    'LastEdited'=> date('Y-m-d H:i:s'),
                    'DepartmentID'=>$this->user->DepartmentID
                );
                $this->db->insert('ttp_report_inventory_import',$data);
                $ID = $this->db->insert_id();
                $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Request,TotalCurrency,TotalVND) values".implode(',',$arr);
                $arr = str_replace('ImportIDDV',$ID,$arr);
                $this->db->query($arr);
                $data_his = array(
                    'ImportID'  => $ID,
                    'Status'    => 0,
                    'Note'      => "Yêu cầu nhập kho tự động từ PO số $POCode",
                    'UserID'    => $this->user->ID,
                    'Created'   => date('Y-m-d H:i:s')
                );
                $this->db->insert('ttp_report_inventory_import_history',$data_his);
            }
        }
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $result = $this->db->query("select a.*,b.FirstName,b.LastName from ttp_report_perchaseorder a,ttp_user b where a.ID=$id and a.UserID=b.ID")->row();
        if($result){
            $view = 'warehouse_inventory_perchaseorder_edit';
            $this->template->add_title('Purchase Order Edit | Warehouse Report Tools');
            $this->template->write_view('content','admin/'.$view,
                array(
                    'data'=>$result,
                    'base_link'=>base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/'
                )
            );
            $this->template->render();
        }
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $DatePO = isset($_POST['DatePO']) ? $_POST['DatePO'] : date('Y-m-d') ;
        $DateExpected = isset($_POST['DateExpected']) ? $_POST['DateExpected'] : date('Y-m-d') ;
        $WarehouseID = isset($_POST['WarehouseID']) ? $_POST['WarehouseID'] : 0 ;
        $ProductionID = isset($_POST['ProductionID']) ? $_POST['ProductionID'] : 0 ;
        $HardCode = isset($_POST['HardCode']) ? $_POST['HardCode'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $Status = isset($_POST['Status']) ? $_POST['Status'] : '' ;
        $Ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '' ;
        $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
        $IsChangeOrder  = isset($_POST["IsChangeOrder"]) ? $_POST["IsChangeOrder"] : 0;
        if($DatePO!=''){
            $result = $this->db->query("select * from ttp_report_perchaseorder where ID=$ID")->row();
            if($result){
                if($result->Status!=1 && $result->Status!=0){
                    $data = array(
                        'Status'=> $Status
                    );
                    $data_files = $this->Upload_attachment();
                    $files = json_decode($result->Files,true);
                    $files = array_merge($data_files,$files);
                    $files = array_unique($files);
                    $data['Files'] = json_encode($files);
                    $this->db->where("ID",$ID);
                    $this->db->update('ttp_report_perchaseorder',$data);
                    if($result->Status!=$Status){
                        $data_his = array(
                            'POID'      => $ID,
                            'Status'    => $Status,
                            'Note'      => $Ghichu,
                            'UserID'    => $this->user->ID,
                            'Created'   => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('ttp_report_perchaseorder_history',$data_his);
                    }
                    redirect(ADMINPATH.'/report/warehouse_inventory_perchaseorder');
                }
                $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
                $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
                $Currency = isset($_POST['Currency']) ? $_POST['Currency'] : array() ;
                $ValueCurrency = isset($_POST['ValueCurrency']) ? $_POST['ValueCurrency'] : array() ;
                $PriceCurrency = isset($_POST['PriceCurrency']) ? $_POST['PriceCurrency'] : array() ;
                $TotalCurrency = isset($_POST['TotalCurrency']) ? $_POST['TotalCurrency'] : array() ;
                $TotalVND = isset($_POST['TotalVND']) ? $_POST['TotalVND'] : array() ;
                $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
                if(count($ProductsID)>0){
                    $arr = array();
                    $arr_request = array();
                    $total = 0;
                    $slsp = 0;
                    foreach($ProductsID as $key=>$row){
                        $Currency_row = isset($Currency[$key]) ? $Currency[$key] : 0 ;
                        $Amount_row = isset($Amount[$key]) ? $Amount[$key] : 0 ;
                        $Shipment_row = isset($ShipmentID[$key]) ? $ShipmentID[$key] : 0 ;
                        $ValueCurrency_row = isset($ValueCurrency[$key]) ? $ValueCurrency[$key] : 0 ;
                        $PriceCurrency_row = isset($PriceCurrency[$key]) ? $PriceCurrency[$key] : 0 ;
                        $TotalCurrency_row = $Amount_row*$PriceCurrency_row;
                        $TotalVND_row = ($Amount_row*$PriceCurrency_row)*$ValueCurrency_row;
                        if($TotalCurrency_row>=0 && $TotalVND_row>=0){
                            $slsp = $slsp+$Amount_row;
                            $total +=$TotalVND_row;
                            $arr[] = "(POIDDV,$row,'$Currency_row',$ValueCurrency_row,$PriceCurrency_row,$Amount_row,$TotalCurrency_row,$TotalVND_row,$Shipment_row)";
                            if($Status==2 || $Status==3 || $Status==4 || $Status==5){
                                $arr_request[] = "(ImportIDDV,$row,$Shipment_row,'$Currency_row',$ValueCurrency_row,$PriceCurrency_row,$Amount_row,$TotalCurrency_row,$TotalVND_row)";
                            }
                        }
                    }
                    if(count($arr)>0){
                        $data = array(
                            'UserID'        => $this->user->ID,
                            'HardCode'  => $HardCode,
                            'DatePO'        => $DatePO,
                            'DateExpected'  => $DateExpected,
                            'ProductionID'  => $ProductionID,
                            'Note'          => $Note,
                            'Address'       => $Address,
                            'Status'        => $Status,
                            'TotalAmount'   => $slsp,
                            'TotalPrice'    => $total
                        );
                        $data_files = $this->Upload_attachment();
                        $files = json_decode($result->Files,true);
                        $files = array_merge($data_files,$files);
                        $files = array_unique($files);
                        $data['Files'] = json_encode($files);
                        $this->db->where("ID",$ID);
                        $this->db->update('ttp_report_perchaseorder',$data);
                        if($IsChangeOrder==1){
                            $this->db->query("delete from ttp_report_perchaseorder_details where POID=$ID");
                            $arr = "insert into ttp_report_perchaseorder_details(POID,ProductsID,Currency,ValueCurrency,PriceCurrency,Amount,TotalCurrency,TotalVND,ShipmentID) values".implode(',',$arr);
                            $arr = str_replace('POIDDV',$ID,$arr);
                            $this->db->query($arr);
                            
                        }
                        if($result->Status!=$Status){
                            $data_his = array(
                                'POID'      => $ID,
                                'Status'    => $Status,
                                'Note'      => $Ghichu,
                                'UserID'    => $this->user->ID,
                                'Created'   => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('ttp_report_perchaseorder_history',$data_his);
                        }
                        if($Status==2 || $Status==3 || $Status==4 || $Status==5){
                            $check = $this->db->query("select ID from ttp_report_inventory_import where POID=$ID")->row();
                            if(!$check){
                                $data['ID'] = $ID;
                                $data['POCode'] = $result->POCode;
                                $data['WarehouseID'] = $result->WarehouseID;
                                $this->create_request_import($data,$arr_request);
                            }
                        }
                    }
                }else{
                    echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                    echo "<script>alert('Cảnh báo hệ thống không được cập nhật ! Số lượng sản phẩm trong PO không được rỗng !');window.location='".$_SERVER['HTTP_REFERER']."'</script>";
                    return;
                }
            }
        }
        redirect(ADMINPATH.'/report/warehouse_inventory_perchaseorder');
    }

    public function preview($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Title as Production,c.Address as ProAddress from ttp_report_perchaseorder a,ttp_user b,ttp_report_production c where a.ID=$id and a.UserID=b.ID and a.ProductionID=c.ID")->row();
        if($result){
            $view = 'warehouse_inventory_perchaseorder_preview';
            $this->template->add_title('Preview Purchase Order | Warehouse Report Tools');
            $this->template->write_view('content','admin/'.$view,
                array(
                    'data'=>$result,
                    'base_link'=>base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/'
                )
            );
            $this->template->render();
        }
    }

    public function get_products($status=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $CategoriesID = $this->user->UserType==2 ? " and CategoriesID like '%\"62\"%'" : '' ;
        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '' ;
        if($Title!='' && $Title!='*'){ 
            $result = $this->db->query("select ID,Title,MaSP,CategoriesID,RootPrice as Price,Donvi,VAT from ttp_report_products where (Title like '%$Title%' or MaSP like '%$Title%') $CategoriesID")->result();
        }else{
            $result = $this->db->query("select ID,Title,MaSP,CategoriesID,RootPrice as Price,Donvi,VAT from ttp_report_products where 1=1 $CategoriesID")->result();
        }
        if(count($result)>0){
            $bonus_th = $status==0 ? '' : '<th style="width: 128px;">Thêm số lượng dòng</th>' ;
            $str = "<div class='tools_search_products'>
                        <span>Tìm kiếm sản phẩm: </span>
                        <span><input type='text' placeholder='Nhập mã SKU hoặc tên sản phẩm' onchange='input_search_products(this)' id='input_search_products' /></span>
                    </div>
                    <table><tr><th></th><th>Mã Sản phẩm</th><th>Sản phẩm</th>
                    <th>
                        <select class='select_products' onchange='fillter_categories(this)'><option value='0'>-- Tất cả ngành hàng --</option>__</select>
                    </th>
                    $bonus_th
                    </tr>";
            $categories = $this->db->query("select * from ttp_report_categories")->result();
            $arr_categories = array();
            if(count($categories)){
                foreach($categories as $row){
                    $arr_categories[$row->ID] = $row->Title;
                }
            }
            $option = array();
            $bonus_td = $status==0 ? '' : "<td><input type='number' class='numbertd' value='1' style='width: 58px;' /></td>";
            foreach($result as $row){
                $row->CategoriesID = json_decode($row->CategoriesID,true);
                $row->CategoriesID = is_array($row->CategoriesID) ? $row->CategoriesID : array() ;
                $class='';
                $current_categories = array();
                if(count($row->CategoriesID)){
                    foreach($row->CategoriesID as $item){
                        if(isset($arr_categories[$item])){
                            $current_categories[] = $arr_categories[$item];
                            if(!isset($option[$item]))
                            $option[$item] = "<option value='".$item."'>".$arr_categories[$item]."</option>";
                        }
                        $class.=" categories_$item";
                    }
                }
                $str.="<tr class='trcategories $class'>";
                $str.="<td style='width:30px'><input type='checkbox' class='selected_products' data-id='$row->ID' data-code='$row->MaSP' data-name='$row->Title' data-price='$row->Price' data-donvi='$row->Donvi' data-vat='$row->VAT' /></td>";
                $str.="<td style='width:150px'>".$row->MaSP."</td>";
                $str.="<td>".$row->Title."</td>";
                $str.="<td style='width:200px'>".implode(',',$current_categories)."</td>";
                $str.="$bonus_td</tr>";
            }
            $option = implode('',$option);
            echo str_replace("__",$option,$str);
            echo "</table>";
            echo "<div class='fixedtools'><a class='btn btn-box-inner' onclick='add_products()'>Đưa vào đơn hàng</a></div>";
            return;
        }
        echo "false";
    }

    public function add_production(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Code = isset($_POST['code']) ? $_POST['code'] : '' ;
        $Address = isset($_POST['address']) ? $_POST['address'] : '' ;
        $Phone = isset($_POST['phone']) ? $_POST['phone'] : '' ;
        $Fax = isset($_POST['fax']) ? $_POST['fax'] : '' ;
        if($Title!='' && $Code!=''){
            $data = array(
                'Title' => $Title,
                'ProductionCode'=>$Code,
                'Address'=>$Address,
                'Phone'=>$Phone,
                'Fax'=>$Fax,
                'Published' => 1
            );
            $this->db->insert('ttp_report_production',$data);
            $ID = $this->db->insert_id();
            echo "<option value='$ID'>$Title</option>";
        }
    }

    public function load_fillter_by_type_and_field(){
        $field = isset($_POST['FieldName']) ? $_POST['FieldName'] : '' ;
        if($field=='nhacungcap'){
            $result = $this->db->query("select Title,ID from ttp_report_production order by Title ASC")->result();
            if(count($result)>0){
                echo "<select name='FieldText[]' class='form-control'>";
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->Title</option>";
                }
                echo "</select>";
            }
        }elseif($field=='nguoitao'){
            $result = $this->db->query("select UserName,ID from ttp_user where UserType=6 or IsAdmin=1 order by UserName ASC")->result();
            if(count($result)>0){
                echo "<select name='FieldText[]' class='form-control'>";
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->UserName</option>";
                }
                echo "</select>";
            }
        }else{
            echo '<input type="text" name="FieldText[]" class="form-control" id="textsearch" />';
        }
    }

    public function setfillter(){
        $arr_fieldname = array(0=>"a.POCode",1=>"b.ID",2=>"a.UserID");
        $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
        $FieldName = isset($_POST['FieldName']) ? $_POST['FieldName'] : array() ;
        $FieldOparation = isset($_POST['FieldOparation']) ? $_POST['FieldOparation'] : array() ;
        $FieldText = isset($_POST['FieldText']) ? $_POST['FieldText'] : array() ;
        $str = array();
        if(count($FieldName)>0){
            $i=0;
            $arrtemp = array();
            foreach ($FieldName as $key => $value) {
                if(!in_array($value,$arrtemp)){
                    if(isset($arr_fieldname[$value]) && isset($FieldOparation[$i])){
                        if(isset($arr_oparation[$FieldOparation[$i]]) && isset($FieldText[$i])){
                            if($arr_oparation[$FieldOparation[$i]]=='like'){
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '%".mysql_real_escape_string($FieldText[$i])."%'";
                            }else{
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '".mysql_real_escape_string($FieldText[$i])."'";
                            }
                            $i++;
                        }
                    }
                    $arrtemp[] = $value;
                }
            }
            if(count($str)>0){
                $sql = implode(' and ',$str);
                $this->session->set_userdata("fillter",$sql);
            }else{
                $this->session->set_userdata("fillter","");    
            }
        }else{
            $this->session->set_userdata("fillter","");
        }
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
        redirect($referer);
    }

    public function get_address_from_production(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $result = $this->db->query("select Address from ttp_report_production where ID=$ID")->row();
        if($result){
            echo $result->Address;
        }else{
            echo "fasle";
        }
    }

    public function get_shipment_by_productsID($id=0,$warehouse=0){
        if($this->user->UserType==6 && $this->user->Channel==2){
            $result = $this->db->query("select b.ShipmentCode,b.ID from ttp_report_shipment b where ID=$id")->result();
        }else{
            $result = $this->db->query("select b.ShipmentCode,b.ID from ttp_report_shipment b where b.ProductsID=$id")->result();
        }
        echo "<option value='0'>-- Chọn lô --</option>";
        if(count($result)>0){
            foreach($result as $row){
                echo "<option value='$row->ID'>$row->ShipmentCode</option>";
            }
        }
        echo "<option value='add'>-- Tạo mới --</option>";
    }

    public function load_shipment_by_products($id=0){
        if($id>0){
            $shipment = $this->uri->segment(6);
            echo "<option value='0'>-- Chọn lô --</option>";
            $result = $this->db->query("select ID,ShipmentCode from ttp_report_shipment where ProductsID=$id")->result();
            if(count($result)>0){
                foreach($result as $row){
                    $selected= $row->ID==$shipment ? "selected='selected'" : '' ;
                    echo "<option value='$row->ID' $selected>Lô $row->ShipmentCode</option>";
                }
            }
            echo "<option value='add'>-- Tạo mới --</option>";
        }else{
            echo "False";
        }
    }

    public function box_add_shipment($id=0){
        $result = $this->db->query("select ID,Title from ttp_report_products where ID=$id")->row();
        if($result){
            $this->load->view("warehouse_inventory_shipment_add",array('data'=>$result));
        }else{
            echo "<p>Dữ liệu gửi lên không chính xác !.</p>";
        }
    }

    public function save_shipment(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : '' ;
        $ShipmentCode = isset($_POST['ShipmentCode']) ? $_POST['ShipmentCode'] : '' ;
        $DateProduction = isset($_POST['DateProduction']) ? $_POST['DateProduction'] : '' ;
        $DateExpiration = isset($_POST['DateExpiration']) ? $_POST['DateExpiration'] : '' ;
        if($ShipmentCode!='' && $DateProduction!='' && $DateExpiration!=''){
            $data = array(
                'UserID'        => $this->user->ID,
                'ShipmentCode'  => $ShipmentCode,
                'ProductsID'    => $ID,
                'DateProduction'=> $DateProduction,
                'DateExpiration'=> $DateExpiration,
                'Created'       => date('Y-m-d H:i:s')
            );
            $this->db->insert("ttp_report_shipment",$data);
        }
    }
}