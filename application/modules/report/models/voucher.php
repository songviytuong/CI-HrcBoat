<?php
class Voucher extends CI_Model {

    const _tablename    = 'ttp_promotion_code';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function verifyCode($code)
    {
        $this->db->select('*');
        $this->db->where('code', $code);
        $result = $this->db->get(self::_tablename)->row();
        if($result == null){
            
        }else{
            return $result;
        }
    }
    
    public function verifyExpired($code)
    {
        $this->db->select('*');
        $this->db->from('ttp_promotion_code');
        $this->db->join('ttp_promotion', 'ttp_promotion.promo_id = ttp_promotion_code.promo_id');
        $this->db->join('ttp_promotion_details', 'ttp_promotion_details.promo_id = ttp_promotion_code.promo_id');
        $this->db->where('ttp_promotion_code.code', $code);
        $result = $this->db->get()->row();
        if($result == null){
            
        }else{
            return $result;
        }
    }
    
    public function getPromotionDetail($id){
        $this->db->select('*');
        $this->db->from('ttp_promotion_details');
        $this->db->where('ttp_promotion_details.id', $id);
        $result = $this->db->get()->row();
        if($result == null){
            
        }else{
            return $result;
        }
    }

}