<?php 
if($this->user->UserType!=2 && $this->user->IsAdmin!=1 && $this->user->UserType!=3){
	echo '<div class="warning_message" style="display:block"><span>Yêu cầu này đang trong trạng thái khóa . Mọi thay đổi của bạn hiện tại sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>';
}elseif($data->Status==4 && $this->user->UserType==2){
	echo '<div class="warning_message" style="display:block"><span>Yêu cầu này đang trong trạng thái khóa . Mọi thay đổi của bạn hiện tại sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>';
}
?>
<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update_from_warehouse' ?>" method="POST">
			<input type='hidden' name='ID' value='<?php echo $data->ID ?>' />
			<div class="import_select_progress">
				<div class="block1">
					<h1>PHIẾU YÊU CẦU LƯU CHUYỂN NỘI BỘ (NHẬP KHO)</h1>
				</div>
				<div class="block2">
					<div class="btn_group">
						<?php
						if($data->Status==4){
							if($this->user->UserType==3 || $this->user->IsAdmin==1){
		    					echo "<a class='btn btn-primary btn_default' href='".$base_link."preview_bill/$data->ID'><i class='fa fa-print'></i> In Phiếu</a>";
		    				}
		    				echo "<a href='".$base_link."mobilization/$data->ID' class='btn btn-primary'><i class='fa fa-print'></i> Lệnh Điều Động</a>";
		    			}
		    			if($this->user->UserType==2 || $this->user->IsAdmin==1){
	    					echo '<button class="btn btn-danger btn_default" type="submit"><i class="fa fa-download"></i> Nhập kho</button>';
	    				}
		    			
		    			?>
	    			</div>
				</div>
			</div>
			<div class="box_content_warehouse_import">
				<div class="block1">
					<div class="row" style="margin:5px 0px">
			    		<textarea id="Ghichu" name="Ghichu" class="form-control" placeholder="Điền ghi chú cập nhật trạng thái (nếu có) ..."></textarea>
			    	</div>
					<div class="row">
		    			<div class="col-xs-4">
		    				<div class="form-group">
	    						<label class="col-xs-5 control-label">Ngày chứng từ: </label>
	    						<div class="col-xs-7">
			    					<input type='text' name='NgayXK' value='<?php echo date('Y-m-d',strtotime($data->DateCreated)) ?>' readonly="true" class="form-control" />
			    				</div>
		    				</div>
		    			</div>
						<div class="col-xs-4">
		    				<div class="form-group">
	    						<label class="col-xs-5 control-label">Kho xuất hàng: </label>
	    						<div class="col-xs-7">
			    					<select name='KhoSender' class="form-control" id="KhoSender" onchange="set_empty_table()">
										<?php 
										$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
										if(count($warehouse)>0){
											foreach($warehouse as $row){
												echo $data->WarehouseSender==$row->ID ? "<option value='$row->ID'>$row->MaKho</option>" : "";
											}
										}
										?>
									</select>
			    				</div>
		    				</div>
		    			</div>
		    			<div class="col-xs-4">
		    				<div class="form-group">
		    					<label class="col-xs-5 control-label">Kho nhận hàng: </label>
		    					<div class="col-xs-7">
			    					<select name='KhoReciver' id="KhoReciver" class="form-control">
									<?php 
									$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
									if(count($warehouse)>0){
										foreach($warehouse as $row){
											echo $data->WarehouseReciver==$row->ID ? "<option value='$row->ID'>$row->MaKho</option>" : "";
										}
									}
									?>
								</select>
			    				</div>
		    				</div>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-xs-4">
			    			<div class="form-group">
			    				<label class="col-xs-5 control-label">Lý do chuyển kho : </label>
								<label class="col-xs-7 control-label" style="white-space: nowrap;"><?php echo $data->Note=='' ? '--' : $data->Note ; ?> </label>
			    			</div>
		    			</div>
		    		</div>
				</div>
				<!-- end block1 -->
				<div class="clear"></div>
		    	<div class="table_donhang table_data">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã SP</th>
		    				<th>Tên sản phẩm</th>
		    				<th>ĐVT</th>
		    				<th style='width:155px;text-align:center'>Lô xuất hàng</th>
		    				<th style="width: 130px;">SL yêu cầu xuất</th>
		    				<th style="width: 130px;">SL thực xuất</th>
		    				<th style="width: 130px;">SL thực nhập</th>
		    				<th style="width: 130px;">Ghi chú</th>
		    				<th style="width: 130px;">Lô nhập</th>
		    			</tr>
		    			<?php 
		    			if($data->Status==4){
		    				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi,c.ShipmentCode,d.ShipmentCode as ShipmentCodeImport from ttp_report_transferorder_details a,ttp_report_products b,ttp_report_shipment c,ttp_report_shipment d where a.ShipmentIDImport=d.ID and a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
		    			}else{
		    				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi,c.ShipmentCode from ttp_report_transferorder_details a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
		    			}
		    			$total = 0;
		    			if(count($details)>0){
		    				foreach($details as $row){
		    					echo '<tr>
									<td><input type="checkbox" class="selected_products" data-id="'.$row->ProductsID.'"><input type="hidden" name="ProductsID[]" value="'.$row->ProductsID.'"></td>
		    							<td>'.$row->MaSP.'</td>
		    							<td>'.$row->Title.'</td>
		    							<td>'.$row->Donvi.'</td>
		    							<td style="width:155px;text-align:left">'.$row->ShipmentCode.'</td>
		    							<td><span>'.$row->Request.'</span></td>
		    							<td><span class="Availabletd">'.number_format($row->TotalExport).'</span></td>';
		    					echo '<td style="text-align:right">'.number_format($row->TotalExport).'</td>';
		    					echo "<td>$row->Note</td>";
		    					if($data->Status!=4){
			    					echo "<td><select name='ShipmentID[]' class='select_shipment' data='$row->ProductsID' onchange='add_shipment(this,$row->ProductsID)'><option value='0'>-- Chọn lô --</option>";
			    					$shipment = $this->db->query("select ShipmentCode,ID from ttp_report_shipment where ProductsID=$row->ProductsID")->result();
			    					if(count($shipment)>0){
			    						foreach($shipment as $item){
			    							$selected = $item->ID==$row->ShipmentID ? "selected='selected'" : "" ;
			    							echo "<option value='$item->ID' $selected>Lô $item->ShipmentCode</option>";
			    						}
			    					}
			    					echo "<option value='add'>-- Tạo mới --</option></select></td>";
		    					}else{
		    						echo "<td style='text-align:left'>$row->ShipmentCodeImport</td>";
		    					}
		    					echo '</tr>';
		    					$total +=$row->TotalExport;
		    				}
		    			}
		    			?>
		    			<tr>
		    				<td colspan="7">TỔNG CỘNG</td>
		    				<td><span class='tongcong'><?php echo $total ?></span></td>
		    				<td></td>
		    				<td></td>
		    			</tr>
		    		</table>
		    		<div class="history_status">
			    		<h3 style="margin-bottom:10px">Lịch sử trạng thái</h3>
			    		<?php 
			    		$history = $this->db->query("select a.*,b.UserName from ttp_report_transferorder_history a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
			    		if(count($history)>0){
			    			$arr_status = array(
			                    0=>'Yêu cầu chuyển kho',
					            1=>'Yêu cầu bị hủy',
					            2=>'Đã duyệt cho xuất kho',
					            3=>'Đã xuất kho & chờ nhập kho',
					            4=>'Đã nhập kho'
			                );
			    			echo "<table><tr><th style='width:200px'>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
			    			foreach($history as $row){
			    				echo "<tr>";
			    				echo isset($arr_status[$row->Status]) ? "<td>".$arr_status[$row->Status]."</td>" : "<td>--</td>" ;
			    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
			    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
			    				echo "<td>$row->UserName</td>";
			    				echo "</tr>";
			    			}
			    			echo "</table>";
			    		}
			    		?>
			    	</div>
			    	<input type="hidden" name="Status" value="4" />
			    	
		    	</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	function stopRKey(evt) { 
		var evt = (evt) ? evt : ((event) ? event : null); 
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
		if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
	} 

	document.onkeypress = stopRKey;

	$(document).ready(function () {
        $('#NgayNK,.DateProduction').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	var link = $("#baselink").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	function save_shipment(ob,id){
		$(ob).addClass("saving");
		var ShipmentCode = $(".ShipmentCode").val();
		var DateProduction = $(".DateProduction").val();
		var DateExpiration = $(".DateExpiration").val();
		if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
			alert("Vui lòng điền đầy đủ thông tin !");
			$(ob).removeClass("saving");
			return false;
		}
		$.ajax({
        	url: link+"save_shipment",
            dataType: "html",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
            	load_shipment(id);
            	$(".over_lay").hide();
				disablescrollsetup();
				$(ob).removeClass("saving");
            }
        });
	}

	function add_shipment(ob,id){
		var data = $(ob).val();
		if(data=="add"){
			enablescrollsetup();
			$(".over_lay .box_inner").css({"width":"500px"});
			$(".over_lay .box_inner .block1_inner h1").html("Tạo lô sản phẩm");
			$(".over_lay .box_inner .block2_inner").html("");
			$(".over_lay .box_inner .block2_inner").load(link+"box_add_shipment/"+id);
			$(".over_lay").removeClass('in');
			$(".over_lay").fadeIn('fast');
			$(".over_lay").addClass('in');
		}
	}

	function loadshipmentdefault(){
		var warehouse = $('#KhoSender').val();
		$(".ShipmentDefault").each(function(){
			var data = $(this).attr('data-id');
			$(this).load(link+"get_shipment_by_productsID/"+data+"/"+warehouse);
		});
	}

	function load_shipment(id){
		$(".select_shipment").each(function(){
			if(id==0){
				var data = $(this).attr('data');
				$(this).load(link+"load_shipment_by_products/"+data);
			}else{
				var data = $(this).attr('data');
				if(data==id){
					$(this).load(link+"load_shipment_by_products/"+data);
				}
			}
		});
	}

	function recal(){
		var tongcong = 0;
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			amount = parseInt(parent.find('input.Amount_input').val());
			max = parseInt(parent.find('.Availabletd').html().replace(/,/g, ''));
			if(amount>max){
				parent.find('input.Amount_input').val(max);
				amount = max;
				alert("Số thực xuất / thực nhập không được vượt số lượng yêu cầu .");
			}
			tongcong = tongcong+amount;
		});
		$(".tongcong").html(tongcong.format('a',3));
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

</script>
<style>
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(5){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(6){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(7){text-align:right;}
</style>