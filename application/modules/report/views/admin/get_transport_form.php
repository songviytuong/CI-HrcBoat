<style>
    .body_content .containner .black .box_inner .block2_inner {
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
</style>
<?php if ($isLogin == "Connected") {?>
<form method="get" name="frmPage" id="frmPage">
<input type="hidden" name="pickupHubID"  id="pickupHubID" />
<input type="hidden" name="SOCode" value="<?=$SOCode;?>" />
<input type="hidden" name="IDOrder" value="<?=$IDOrder;?>" />
<input type="hidden" name="IDExport" value="<?=$IDExport;?>"/>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs">
            <label for="PickHubID">Địa chỉ lấy hàng</label>
            <select name="PickHubID" id="PickHubID" class="form-control">
                <?php
                foreach ($PickHub as $data) {
                    ?>
                    <option rel="<?= $data["PickHubID"]; ?>" value="<?= $data["DistrictCode"]; ?>"><?= $data["DistrictName"]; ?> - <?= $data["Address"]; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="col-xs">
            <label for="DeliveryAddress">Địa chỉ giao hàng</label>
            <input class="form-control" name="DeliveryAddress" id="DeliveryAddress" placeholder="246/9 Bình Quới, Thanh Đa" value="<?=$Address;?>" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="col-xs">
            <label for="DeliveryDistrictCode">Mã Quận giao hàng</label>
            <select name="DeliveryDistrictCode" id="DeliveryDistrictCode" class="form-control">
                <option value="-1">Chọn quận giao hàng</option>
                <?php foreach ($District as $data) { 
                    if($data["DistrictCode"] == $DistrictID) {
                    ?>
                <option value="<?= $data["DistrictCode"]; ?>" selected="selected"><?= $data["ProvinceName"]; ?> - <?= $data["DistrictName"]; ?></option>
                <?php } else {?>
                    <option value="<?= $data["DistrictCode"]; ?>"><?= $data["ProvinceName"]; ?> - <?= $data["DistrictName"]; ?></option>
               <?php } }?>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="col-xs">
            <label for="ServiceID">Thời gian vận chuyển</label>
            <select name="ServiceID" id="ServiceID" class="form-control">
                <option value="-1">-------------</option>
                <?php foreach ($Service as $key => $value) { ?>
                    <option value="<?= $key; ?>"><?= $value; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
</div>

<div class="row">
        <div class="col-xs-2">
            <label for="Weight">Trọng lượng</label>
            <input class="form-control" type="number" name="Weight" id="Weight" value="300" />
        </div>
        <div class="col-xs-2">
            <label for="Length">Dài (cm)</label>
            <input class="form-control" type="number" name="Length" id="Length" value="20" />
        </div>
        <div class="col-xs-2">
            <label for="Width">Rộng (cm)</label>
            <input class="form-control" type="number" name="Width" id="Width" value="10" />
        </div>
        <div class="col-xs-2">
            <label for="Height">Cao (cm)</label>
            <input class="form-control" type="number" name="Height" id="Height" value="10" />
        </div>
    
        <div class="col-xs-2">
            <label for="Fee">Phí vận chuyển</label>
            <input class="form-control" id="Fee" name="Fee" placeholder="Chưa tính" value="" />
        </div>
    <div class="clearfix"></div>
        <div class="col-xs-2 hidden">
            <label for="btn-fee">&nbsp;</label>
            <button type="button" id="btn-fee" class="btn btn-primary">Phí vận chuyển</button>
        </div>
    
        <div class="col-xs-4 hidden">
            <label for="Fee">Có tính phí</label>
            <input type="radio" class="form-control" id="isChargeFee1" name="isChargeFee" value="1"/>
        </div>
        <div class="col-xs-4 hidden">
            <label for="Fee">Không tính phí</label>
            <input type="radio" class="form-control" id="isChargeFee2" name="isChargeFee" value="0"  checked="checked" />
        </div>
        <div class="clearfix" style="">&nbsp;</div>
        <div class="col-xs-4">
            
            <input class="form-control bfh-number" name="CODAmountPre" id="CODAmountPre" placeholder="12,000,000" value="<?=$TotalBill;?>" />
            <label for="CODAmountPre">Tiền thu người nhận dự kiến</label>
        </div>
        <div class="col-xs-2">
            <input class="form-control bfh-number" name="CODAmountPercent" id="CODAmountPercent" placeholder="" value="1" />
            <label for="CODAmountPercent">Trọng số</label>
        </div>
        
       
        <div class="col-xs-2">
            
            <input class="form-control bfh-number" name="CODAmount" id="CODAmount" placeholder="12,000,000" value="" />
            <label for="CODAmount">Tiền thu người nhận</label>
        </div>
        
        <div class="col-xs-4">
            <div class="btn-group">
                <button type="button" id="btn-fee-cod" id="btn-fee-cod" class="btn btn-primary">Phí thu người nhận</button> <button type="button" id="complete" class="btn btn-success" rel="11">Hoàn tất</button>
            </div>
        </div>
        <div class="col-xs hidden">
            <label for="CODAmount">Hình thức thanh toán</label>                                        
        </div>
        <div class="col-xs-4 hidden">
            <label for="paymentMethod1">Chuyển khoản</label>
            <input <?=($PayMethod == 1) ? 'checked="checked"':'' ?> type="radio" class="form-control" id="paymentMethod1" name="paymentMethod" value="1" />
        </div>
        <div class="col-xs-4 hidden">
            <label for="paymentMethod2">Giao hàng nhận tiền</label>
            <input <?=($PayMethod == 0) ? 'checked="checked"':'' ?> type="radio" class="form-control" id="paymentMethod2" name="paymentMethod" value="0" />
        </div>
        <div class="col-xs hidden">
            <label for="RecipientName">Họ tên người nhận hàng</label>
            <input class="form-control" name="RecipientName" id="RecipientName" placeholder="Nguyễn Văn A" value="<?=$Fullname;?>" />
        </div>
        <div class="col-xs hidden">
            <label for="RecipientPhone">Điện thoại người nhận hàng</label>
            <input class="form-control" id="RecipientPhone" name="RecipientPhone" placeholder="0909123456" value="<?=$Phone;?>" />
        </div>
        <div class="col-xs hidden">
            <label for="invoice">Thông tin xuất hóa đơn</label>
            <textarea class="form-control" id="invoice" name="invoice"></textarea>
        </div>
        <div class="col-xs hidden">
            <label for="ContentNote">Ghi chú</label>
            <textarea class="form-control" id="ContentNote" name="ContentNote"></textarea>
        </div>
</div>
</form>
<?php } else {?>
<div class="row">
    <div class="col-xs-12 text-center">
        <i class="fa fa-chain-broken" aria-hidden="true"></i> Không kết nối được với vận chuyển !
    </div>
</div>
<?php } ?>
<script type="text/javascript">
    function convertMoney(str) {
        return str.replace(/,/g, "");
    }
    $().ready(function(){
        
        setTimeout(function(){
          $("#CODAmountPre").keyup();
          $("#DeliveryDistrictCode").change();
        });

        $("#DeliveryDistrictCode").change(function(){
            var id = $(this).val();
            if(id == -1){
                alert('Vui lòng chọn Quận/Huyện');
            }else{
                var fromDistrict = $("#PickHubID").val();
                var toDistrict = $(this).val();
                var urlGetServiceList = "<?php echo base_url().ADMINPATH.'/report/import_order' ?>/getServiceList?FromDistrictCode="+fromDistrict+"&ToDistrictCode="+toDistrict;
                $.post(urlGetServiceList, function(resp){

                    if (resp.ErrorMessage!=null || resp.ErrorMessage!="") {
                        var htmlServiceList = "";
                        $.each(resp.Services, function(i, obj){
                            var str = "<option value='"+obj.ShippingServiceID+"'>"+obj.Name+"</option>";
                            htmlServiceList += str;
                        });
                    }
                    if (htmlServiceList!="") {
                        $("#ServiceID").html(htmlServiceList);                
                    }
                    $("#btn-fee").click();
                }, "json");
            }
            
        });
        $("#ServiceID").change(function(){
            $("#btn-fee").click();
        });
        $("#Weight,#Height,#Length,#Width").change(function(){
            $("#btn-fee").click();
        });
        $("#CODAmount").keyup(function(){
            var price = this.value;
            price = price.replace(/,/gi,"");
            price = price + ".";
            price = price.replace(/\d(?=(\d{3})+\.)/g, '$&,');
            var sprice = price.split(".");
            $(this).val(sprice[0]);
            
        });
        $("#Fee").keyup(function(){
            var price = this.value;
            price = price.replace(/,/gi,"");
            price = price + ".";
            price = price.replace(/\d(?=(\d{3})+\.)/g, '$&,');
            var sprice = price.split(".");
            $(this).val(sprice[0]);
        });
        $("#CODAmountPre").keyup(function(){
            var price = this.value;
            price = price.replace(/,/gi,"");
            price = price + ".";
            price = price.replace(/\d(?=(\d{3})+\.)/g, '$&,');
            var sprice = price.split(".");
            $(this).val(sprice[0]);
        });
        $("#btn-fee-cod").click(function(){
            var preCOD = $("#CODAmountPre").val();
            preCOD = convertMoney(preCOD);
            var percent = $("#CODAmountPercent").val();
            var finalCOD = preCOD*percent;
            $("#CODAmount").val(finalCOD).keyup();
        });
        
        $("#btn-fee").click(function(){
            var url = "<?php echo base_url().ADMINPATH.'/report/import_order' ?>/calculateFee";
            $.get(url, $("#frmPage").serialize(), function(resp){
                
                $("#Fee").val("Chưa tính");
                var err_msg = resp.ErrorMessage;
                var fee = resp.Items[0].ServiceFee;
//                console.log(resp);
//                console.log(url);
                if (err_msg == null || err_msg == "") {
                    $("input[name='Fee']").val(fee);
                    $("#Fee").keyup();
                    
                    $("#Fee").scroll();
                } else {
                    console.log(err_msg);
                }
            }, "json");
        });
        
        $("#complete").click(function(){
            $(this).text("Đang gửi");
            $(this).addClass("disabled");
            $("#btn-fee").click();
            $("#btn-fee-cod").click();
            var ghichu = '';
            var ids = $('#DeliveryDistrictCode').val();
//          console.log(ids);
            if(ids == -1){
                alert('Vui lòng chọn Quận/Huyện');
                $(this).focusin();
            }else{
                var pickupHubID = $("#PickHubID :selected").attr("rel");
                var status_delivery = $(this).attr("rel");
//              console.log(status_delivery);
                $("#pickupHubID").val(pickupHubID);
                if(status_delivery == 11){
                    var cf = confirm('BẠN CÓ CHẮC MUỐN TẠO VẬN ĐƠN NÀY ??');
                    if (cf == false) {
                        $("#complete").text("Hoàn tất");
                        $("#complete").removeClass("disabled");
                        return false;
                    }else{
                        var getValue = prompt("Điền ghi chú thay đổi (nếu có) : ", "");
                        if(getValue != null){
                            ghichu = getValue;
                        }
                        var soURL = "<?php echo base_url().ADMINPATH.'/report/import_order' ?>/createSo?Ghichu="+ghichu;
                        $.get(soURL, $("#frmPage").serialize(), function(resp){
                            var err = resp.ErrorMessage;
                            if (err!=null && err != "") {
                                alert(err);
                            } else {
                                alert("Tạo vận đơn thành công!");
                            }
                            $('#close_overlay').click();
                            setTimeout(function(){
                                location.reload();
                            }, 500);
                        },"json");
                    }
                }
            }
            
        });
    });
    
</script>