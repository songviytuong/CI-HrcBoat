<div class="containner">
	<div class="disableedit" style="height:1000px"></div>
	<div class="status_progress">
    	<?php 
    	$status1 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point1 = $status1=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point1 = $data->Status==3 || $data->Status==5 || $data->Status==7  || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point1 ;

		$status2 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point2 = $status2=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point2 =  $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point2 ;    	

    	$status3 = $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point3 = $status3=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point3 =  $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point3 ;

    	$status4 = $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point4 = $status4=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point4 =  $data->Status==7 || $data->Status==8 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point4 ;

		$status5 = $data->Status==8 || $data->Status==7 || $data->Status==0 ? 'class="active"' : "";
    	$point5 = $status5=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point5 =  $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point5 ;    	
    	
    	$width = $data->Status==2 || $data->Status==4 || $data->Status==6 ? 20 : 0 ;
    	$width = $data->Status==3 ? 40 : $width ;
    	$width = $data->Status==5 ? 60 : $width ;
    	$width = $data->Status==9 ? 80 : $width ;
    	$width = $data->Status==7 || $data->Status==0 || $data->Status==8 ? 100 : $width ;

    	if($data->Status==1 || $data->Status==8){
    		$title_drop = $data->Status==8 ? "Bị trả về" : "Bị hủy" ;
    		echo "<div class='orderisdrop'><a>$title_drop</a><span><i class='fa fa-times'></i></span></div>";
    		echo '<div class="transfer_progress" style="width:100%;background:#000"></div>';
    	}else{
    	?>
    	<ul>
            <li class="active"><a>ĐH nháp</a><span class="ok"><i class="fa fa-check-circle"></i></span></li>
            <li <?php echo $status1 ?>><a>Đã chuyển sang kho</a><?php echo $point1 ?></li>
            <li <?php echo $status2 ?>><a>Kho xác nhận</a><?php echo $point2 ?></li>
            <li <?php echo $status3 ?>><a>Kế toán xác nhận</a><?php echo $point3 ?></li>
            <li <?php echo $status4 ?>><a>Đã giao vận chuyển</a><?php echo $point4 ?></li>
            <li <?php echo $status5 ?>><a>Hoàn tất</a><?php echo $point5 ?></li>
        </ul>
        <div class="transfer_progress" style="width:<?php echo $width ?>%"></div>
        <?php 
    	}
        ?>
    </div>
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1><a href="<?php echo $base_link ?>">THÔNG TIN ĐƠN HÀNG</a> - <a style="color:#DE5252"><?php echo $data->MaDH ?></a></h1>
	    </div>
	    <div class="block2">
	    	<div class="block2">
		    	<div class="btn_group">
		    		<a class="btn btn-default btn-drop pull-right" onclick="show_cancelbox()">Hủy <i class="fa fa-caret-down"></i></a>
					<?php 
						$arr_url = array(
			    			2=>"save_from_kho",
			    			3=>"save_from_ketoan",
			    			4=>"save_from_dieuphoi",
			    			7=>"save_from_ketoan"
			    		);
						$next = array(2=>5,3=>9,4=>7,7=>0);
						$prev = array(2=>4,3=>6,4=>7,7=>6);
						if(array_key_exists($this->user->UserType,$next)){
							if($this->user->UserType==3){
				    			if($data->Payment==1){
					    			if($data->PaymentStatus==1){
						    			$check_export_warehouse = $this->db->query("select * from ttp_report_export_warehouse where OrderID='$data->ID'")->row();
						    			if($check_export_warehouse){
						    				echo $this->user->UserType==3 ? '<a class="btn btn-danger btn-accept" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\','.$next[$this->user->UserType].')"><i class="fa fa-check-square"></i> Duyệt</a>' : '';
						    			}
					    			}
				    			}else{
				    				$check_export_warehouse = $this->db->query("select * from ttp_report_export_warehouse where OrderID='$data->ID'")->row();
					    			if($check_export_warehouse){
					  		  			echo $this->user->UserType==3 ? '<a class="btn btn-danger btn-accept" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\','.$next[$this->user->UserType].')"><i class="fa fa-check-square"></i> Duyệt</a>' : '';
					    			}
				    			}
				    		}
							echo $this->user->UserType==2 || $this->user->UserType==3 || $this->user->UserType==7 ? '<a class="btn btn-danger btn-accept" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\','.$prev[$this->user->UserType].')"><i class="fa fa-reply-all"></i> Trả về</a>' : '';
							echo $this->user->UserType==2 || $this->user->UserType==3 ? '<a class="btn btn-primary btn-update disablebutton" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\','.$next[$this->user->UserType].')"><i class="fa fa-check-square"></i> Duyệt</a>' : '';
							echo $this->user->UserType==2 ? '<a class="btn btn-success btn-activeedit" data="0"><i class="fa fa-pencil"></i> Chỉnh sửa</a>' : '';
							echo $this->user->UserType==2 ? '<a class="btn btn-danger btn-cancel disablebutton" onclick="location.reload()"><i class="fa fa-undo"></i> Hủy bỏ</a>' : '';
							echo $this->user->UserType==3 ? "<a class='btn btn-primary btn-print' href='".base_url().ADMINPATH."/report/import/printorder/$data->ID'><i class='fa fa-print'></i> In đơn hàng</a>" : "" ;
							echo $this->user->UserType==7 ? "<a class='btn btn-primary btn-print' onclick='confirmwarning(this)' data='".base_url().ADMINPATH."/report/import_order/acceptsingle/$data->ID'><i class='fa fa-check-square'></i> Duyệt</a>" : "" ;
							
							$onoff = @file_get_contents('log/status/onoff.txt');
							if($data->Payment==1){
			    				if($onoff==1){
			    					if($data->PaymentStatus==1){
				    					echo $this->user->UserType==3 && $data->Accept==2 ? "<a class='btn btn-primary btn-export' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$data->ID'><i class='fa fa-external-link-square'></i> Phiếu xuất kho</a>" : "" ; 
				    					echo $this->user->UserType==3 && $data->Accept==1 ? "<span>Đang chờ kiểm tra lại</span>" : "" ; 
				    					echo $this->user->UserType==3 && $data->Accept==0 ? "<a class='btn btn-success btn-money' href='".base_url().ADMINPATH."/report/import_order/accept_level1/$data->ID'><i class='fa fa-check-square'></i> Tôi đã kiểm tra</a>" : "";
				    				}else{
				    					echo $this->user->UserType==3 ? "<a class='btn btn-success btn-money' href='".$base_link."accept_money/$data->ID'><i class='fa fa-exchange'></i> Đã nhận tiền</a>" : "";
				    				}
			    				}else{
			    					if($data->PaymentStatus==1){
				    					echo $this->user->UserType==3 ? "<a class='btn btn-primary btn-export' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$data->ID'><i class='fa fa-external-link-square'></i> Phiếu xuất kho</a>" : "" ; 
				    				}else{
				    					echo $this->user->UserType==3 ? "<a class='btn btn-success btn-money' href='".$base_link."accept_money/$data->ID'><i class='fa fa-exchange'></i> Đã nhận tiền</a>" : "";
				    				}
			    				}
			    			}else{
			    				if($onoff==1){
				    				echo $this->user->UserType==3 && $data->Accept==2 ? "<a class='btn btn-primary btn-export' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$data->ID'><i class='fa fa-external-link-square'></i> Phiếu xuất kho</a>" : "" ; 
				    				echo $this->user->UserType==3 && $data->Accept==1 ? "<span>Đang chờ kiểm tra lại</span>" : "";
				    				echo $this->user->UserType==3 && $data->Accept==0 ? "<a class='btn btn-success btn-money' href='".base_url().ADMINPATH."/report/import_order/accept_level1/$data->ID'><i class='fa fa-check-square'></i> Tôi đã kiểm tra</a>" : "";
			    				}else{
			    					echo $this->user->UserType==3 ? "<a class='btn btn-primary btn-export' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$data->ID'><i class='fa fa-external-link-square'></i> Phiếu xuất kho</a>" : "" ;
			    				}
			    			}

			    			if($this->user->UserType==4){
			    				if($data->Status==9){
			    					echo '<a class="btn btn-danger btn-accept" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\',7)"><i class="fa fa-check-square"></i> Xuất kho</a>';
			    					echo '<div class="col-xs-7"><select id="doitacvanchuyen" class="form-control"><option value="">-- Chọn đối tác vận chuyển --</option>';
				    				$transport = $this->db->query("select * from ttp_report_transport")->result();
				    				if(count($transport)>0){
				    					foreach($transport as $row){
				    						$selected = $row->ID==$data->TransportID ? "selected='selected'" : '' ;
				    						echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
					    			echo '</select></div>';
			    				}elseif($data->Status==7){
			    					echo '<a class="btn btn-primary btn-update" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\',0)"><i class="fa fa-check-square"></i> ĐH thành công</a>';
			    					$transport = $this->db->query("select * from ttp_report_transport where ID=$data->TransportID")->row();
				    				echo $transport ? "<a class='btn'>Vận chuyển : ".$transport->Title."</a>" : '' ;
			    				}
			    			}
						}
					?>
					<div class="cancel_box">
						<p>Chọn lý do hủy đơn hàng</p>
						<select id="Lydohuydh" onchange="checkshowlydokhac(this)" class="form-control">
			    			<option value=''>-- Lý do hủy đơn hàng --</option>
			    			<?php 
			    			$reason = $this->db->query("select * from ttp_report_order_reason")->result();
			    			if(count($reason)>0){
			    				foreach($reason as $row){
			    					echo "<option value='$row->ID'>$row->Title</option>";
			    				}
			    			}
			    			?>
			    			<option value='0'>LÝ DO KHÁC</option>
			    		</select>
			    		<p class='lydokhac'><span>Lý do hủy khác : </span><input type='text' id='Lydokhac' class="form-control" placeholder="Điền lý do hủy khác ..." /></p>
			    		<a class='btn btn-danger btn-acceptcancel' onclick='send_cancel_request(this)' data='<?php echo base_url().ADMINPATH."/report/import_order/cancel_request/$data->ID"; ?>'><i class='fa fa-minus-circle'></i> Xác nhận hủy</a>
					</div>
				</div>
		    </div>
	    </div>
    </div>

    <div class="import_order_info">
    		<input type='hidden' name="Tinhtrangdonhang" id="Status" value='<?php echo $data->Status ?>' />
    		<input type="hidden" name="IDOrder" value="<?php echo $data->ID ?>" />
	    	<div class="block1">
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
		    				<label class="col-xs-6 control-label">Người tạo đơn hàng </label>
		    				<label class="col-xs-6 control-label">: <?php echo $data->UserName ?></label>
		    			</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">Ngày / giờ đặt hàng</label>
		    				<label class="col-xs-6 control-label">: <?php echo date('Y-m-d H:i:s',strtotime($data->Ngaydathang)) ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">Ngày đề nghị giao hàng</label>
		    				<label class="col-xs-6 control-label">: <?php echo date('Y-m-d H:i:s',strtotime($data->Ngaydathang)) ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<?php 
	    			$tinhthanh = $this->db->query("select a.Title,a.ID,b.Title as AreaTitle,a.AreaID,a.DefaultWarehouse from ttp_report_city a,ttp_report_area b where a.AreaID=b.ID and a.ID=$data->CityID")->row();
	    			$quanhuyen = $this->db->query("select * from ttp_report_district where ID=$data->DistrictID")->row();
	    			?>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">Khu vực</label>
							<label class="col-xs-6 control-label"> : <?php echo $tinhthanh->AreaTitle ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Tỉnh / Thành</label>
							<label class="col-xs-8 control-label"> : <?php echo $tinhthanh->Title ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Quận / Huyện</label>
							<label class="col-xs-7 control-label"> : <?php echo $quanhuyen->Title ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">PT thanh toán</label>
							<label class="col-xs-6 control-label"> : <?php echo $data->Payment==0 ? "COD" : "Chuyển khoản" ; ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Số điện thoại 1</label>
							<label class="col-xs-8 control-label"> : <?php echo $data->Phone1 ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Số điện thoại 2</label>
							<label class="col-xs-7 control-label"> : <?php echo $data->Phone2 ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<?php 
	    					switch ($data->OrderType) {
	    						case 1:
	    							$type = "GT" ;
	    							break;
	    						case 2:
	    							$type = "MT" ;
	    							break;
	    						case 4:
	    							$type = "TD" ;
	    							break;
	    						case 5:
	    							$type = "NB" ;
	    							break;
	    						default:
	    							$type = "--" ;
	    							break;
	    					}
	    					?>
	    					<label class="col-xs-6 control-label">Loại khách hàng</label>
	    					<label class="col-xs-6 control-label">: <?php echo $type ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
							<?php 
				    		if($data->OrderType==2){
				    			$SystemID = $this->db->query("select SystemID from ttp_report_customer a,ttp_report_order b where a.ID=b.CustomerID and b.ID=".$data->ID)->row();
				    			$SystemID = $SystemID ? $SystemID->SystemID : 0 ;
				    			$title_system = $this->db->query("select Title from ttp_report_system where ID=".$SystemID)->row();
				    			echo '<label class="col-xs-6 control-label">Hệ thống kênh bán hàng</label>';
				    			echo $title_system ? '<label class="col-xs-6 control-label">:'.$title_system->Title."</label>" : '<label class="col-xs-8 control-label">:--</label>' ;
				    		}
				    		?>
	    				</div>
	    			</div>
	    		</div>
				<div class="row">
					<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">Mã đối tác</label>
	    					<label class="col-xs-6 control-label">: <?php echo $data->Code ?></label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Tên đối tác</label>
	    					<label class="col-xs-10 control-label">: <?php echo $data->Name ?></label>
						</div>
					</div>
	    			<input type='hidden' name="CustomerID" class="form-control" id="CustomerID" value="<?php echo $data->CustomerID ?>" required />
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Địa chỉ giao hàng</label>
	    					<label class="col-xs-10 control-label">: <?php echo $data->AddressOrder ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Ghi chú khách hàng</label>
	    					<label class="col-xs-10 control-label">: <?php echo $data->Note == '' ? '--' : $data->Note ; ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Kho lấy hàng</label>
	    					<div class="col-xs-2">
	    						<select name="KhoID" id="KhoID" class="form-control" onchange="changewarehouse(this)">
									<?php 
									if($this->user->UserType==2 || $this->user->UserType==8){
										$khoresult = $this->db->query("select * from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%'")->result();
									}else{
										$khoresult = $this->db->query("select * from ttp_report_warehouse")->result();
									}
									if(count($khoresult)>0){
										foreach($khoresult as $row){
											$selected = '';
											if($data->KhoID>0){
												$selected = $data->KhoID==$row->ID ? "selected='selected'" : '' ;
											}else{
												if($tinhthanh){
													$selected = $tinhthanh->DefaultWarehouse==$row->ID ? "selected='selected'" : '' ;
												}
											}
											echo "<option value='$row->ID' $selected>$row->MaKho</option>";
										}
									}
									?>
								</select>
	    					</div>
	    					<div class="col-xs-2" style='margin-left:10px'>
	    						<a class="btn btn-primary hidden accept_changewarehouse" onclick="accept_changewarehouse(this)"><i class="fa fa-retweet"></i> Xác nhận chuyển kho</a>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>

	    	<?php 
    		if($this->user->UserType==2){
    		?>
	    	<!-- end block1 -->
	    	<div class="block2">
				<a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Thùng hàng</a>
				<ul>
					<li><a id='show_thaotac'>Thao tác <b class="caret"></b></a>
	    				<ul>
		    				<li><a id="edit_row_table"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a></li>
		    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
	    				</ul>
					</li>
				</ul>
	    	</div>
	    	<?php }else{
	    		echo "<h2 style='font-size:20px;font-weight:bold;margin-top:20px'>CHI TIẾT ĐƠN HÀNG</h2>";
	    	} ?>
	    	<div class="table_donhang table_data">
	    		<form id="tableform">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã sản phẩm</th>
		    				<th>Tên sản phẩm</th>
		    				<th>Lô</th>
		    				<th>Giá bán</th>
		    				<th>Số lượng</th>
		    				<th>% CK</th>
		    				<th>Giá trị CK</th>
		    				<th>Giá sau CK</th>
		    				<th>Thành tiền</th>
		    			</tr>
		    			<?php 
		    			$details = $this->db->query("select a.*,b.Title,b.MaSP,b.CategoriesID,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
		    			$arrproducts = array();
		    			$temp = 0;
		    			if(count($details)>0){
		    				foreach($details as $row){
		    					$CategoriesID = json_decode($row->CategoriesID,true);
		    					$CategoriesID = is_array($CategoriesID) ? $CategoriesID : array() ;
		    					$categoriestemp = 62;
		    					echo "<tr>";
		    					if($this->user->UserType==2){
		    						echo "<td><input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='$row->ProductsID'>
	    								<input type='hidden' name='ProductsID[]' value='$row->ProductsID'></td>";
		    						if(in_array($categoriestemp, $CategoriesID)){
		    							$temp=1;
		    						}
		    					}else{
		    						echo "<td></td>";
		    					}
		    					$change_shipment = $this->user->UserType==2 ? "<a class='change_shipment' onclick='change_shipment(this,$row->ShipmentID,$row->ProductsID,$row->Amount,$row->ID)'><i class='fa fa-pencil'></i></a><ul></ul>" : "" ;
		    					echo "<td>$row->MaSP</td>";
		    					echo "<td>$row->Title</td>";
		    					echo "<td style='width:180px;text-align:left'><span class='ShipmentDefault' data-id='$row->ProductsID'>$row->ShipmentCode</span>$change_shipment</td>";
			    				$giaban = $row->Price+$row->PriceDown;
		    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
		    					echo "<td><input type='number' class='dongia' value='$giaban' min='0' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>".number_format($giaban)."</span></td>";
		    					echo "<td><input type='number' name='Amount[]' class='soluong' value='$row->Amount' min='1' onchange='recal()' readonly='true' /></td>";
		    					echo "<td><input type='number' onchange='calrowchietkhau(this)' class='percentck' value='$phantramck' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>$phantramck</span></td>";
								echo "<td><input type='number' onchange='calrowchietkhau(this)' class='giack' value='$row->PriceDown' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->PriceDown)."</span></td>";
								echo "<td><input type='number' class='giasauCK' value='$row->Price' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Price)."</span></td>";
								echo "<td><input type='number' class='thanhtien' value='$row->Total' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Total)."</span></td>";
		    					echo "</tr>";
		    					$arrproducts[] = '"data'.$row->ProductsID.'"';
		    				}
		    			}
		    			
		    			$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
		    			$tongtienhang = $data->Total - $data->Chietkhau;
		    			$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi-$data->Reduce;
		    			?>
		    			<tr class="last">
		    				<td colspan="9">Tổng cộng</td>
		    				<td><input type="number" class="tongcong" readonly="true" value="<?php echo $data->Total ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongcong"><?php echo number_format($data->Total) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">% chiết khấu</td>
		    				<td><input type='number' class="phantramchietkhau" min="0" max="100" value="<?php echo $phantramchietkhau ?>" onchange="calchietkhau(this)" /></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Giá chiết khấu</td>
		    				<td><input type='number' class="giachietkhau" min="1" value="<?php echo $data->Chietkhau ?>" readonly="true" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiachietkhau"><?php echo number_format($data->Chietkhau) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Tổng tiền hàng</td>
		    				<td><input type="number" class="tongtienhang" readonly="true" value="<?php echo $tongtienhang ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongtienhang"><?php echo number_format($tongtienhang) ?></span></td>
		    				
		    			</tr>
		    			<?php 
		    			$reduce = $this->db->query("select a.*,b.Title from ttp_report_reduce_order a,ttp_report_reduce b where a.ReduceID=b.ID and a.OrderID=$data->ID")->result();
		    			$arr_reduce = array();
		    			if(count($reduce)>0){
		    				foreach($reduce as $row){
		    					echo "<tr>";
		    					echo "<td><input type='checkbox' class='selected_reduce' onclick='changerow(this)' data-id='$row->ReduceID' /><input type='hidden' name='ReduceID[]' value='$row->ReduceID' /></td>";
		    					echo "<td colspan='8'>$row->Title <input type='text' name='Timereduce[]' style='display:inline;width:500px' value='$row->TimeReduce' /></td>";
		    					echo "<td><input type='number' name='Valuereduce[]' class='Valuereduce' value='".$row->ValueReduce."' onchange='recal()' /></td>";
		    					echo "</tr>";
		    					$arr_reduce[]='"data'.$row->ReduceID.'"';
		    				}
		    			}
		    			?>
		    			<tr class="last">
		    				<td colspan="9">Tổng giảm trừ</td>
		    				<td><input type="number" class="tonggiamtru" readonly="true" value="<?php echo $data->Reduce ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtonggiamtru"><?php echo number_format($data->Reduce) ?></span></td>
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Chi phí vận chuyển</td>
		    				<td><input type='number' class="chiphivanchuyen" value="<?php echo $data->Chiphi ?>" onchange="recal()" style="display:none" /><span id="showchiphivanchuyen" style="display:block;text-align:right;padding-right:15px;" onclick="showchiphi()"><?php echo number_format($data->Chiphi) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Tổng giá trị thanh toán</td>
		    				<td><input type='number' class="tonggiatrithanhtoan" readonly="true" value="<?php echo $tonggiatrithanhtoan ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiatrithanhtoan"><?php echo number_format($tonggiatrithanhtoan) ?></span></td>
		    				
		    			</tr>
		    		</table>
		    		<input type="hidden" name="OrderID" value="<?php echo $data->ID ?>" />
	    		</form>
	    	</div>
	    	<div class="history_status">
	    		<h2>Lịch sử trạng thái đơn hàng</h2>
	    		<?php 
	    		$history = $this->db->query("select a.*,b.UserName from ttp_report_orderhistory a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
	    		if(count($history)>0){
	    			$array_status = array(
	                    10=>'Chờ xác nhận hủy',
	                    9=>'Chờ nhân viên điều phối',
	                    8=>'Đơn hàng bị trả về',
	                    7=>'Chuyển cho giao nhận',
	                    6=>'Đơn hàng bị trả về từ kế toán',
	                    5=>'Đơn hàng chờ kế toán duyệt',
	                    4=>'Đơn hàng bị trả về từ kho',
	                    3=>'Đơn hàng mới chờ kho duyệt',
	                    2=>'Đơn hàng nháp',
	                    0=>'Đơn hàng thành công',
	                    1=>'Đơn hàng hủy'
	                );
	    			echo "<table><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
	    			foreach($history as $row){
	    				echo "<tr>";
	    				echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>" ;
	    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
	    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
	    				echo "<td>$row->UserName</td>";
	    				echo "</tr>";
	    			}
	    			echo "</table>";
	    		}
	    		?>
	    	</div>
	    	<div class="block1">
	    		<?php 
	    		if($this->user->UserType==3 || $this->user->UserType==4){
	    		?>
	    		<div class="row row13">
	    			<span class="title">Kho xuất: </span>
	    			<span>
	    			<?php 
	    				$kho = $this->db->query("select b.Title as KhoTitle,b.MaKho from ttp_report_warehouse b where b.ID=$data->KhoID")->row();
	    				echo $kho ? $kho->KhoTitle." ($kho->MaKho)" : "Chưa chọn kho xuất" ;
	    			?>
	    			 </span>
	    		</div>
	    		<?php } ?>
	    		<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
	    		<input type="hidden" id="referer" value="<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ; ?>" />
	    	</div>
    </div>
    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
	var link = $("#baselink_report").val();
	function updatestatus(ob,url,status){
		if(status==5 || status==9){
			if (!confirm('BẠN CÓ CHẮC MUỐN DUYỆT ĐƠN HÀNG NÀY ??')) {
				return false;
			}
		}
		if(status==7){
			if (!confirm('BẠN CÓ CHẮC MUỐN XUẤT KHO ??')) {
				return false;
			}
		}
		if(status==4 || status==6){
			if (!confirm('BẠN CÓ CHẮC MUỐN TRẢ ĐƠN HÀNG NÀY VỀ CRM ??')) {
				return false;
			}
		}
		$(ob).addClass("saving");
		$(ob).find("i").hide();
		var tinhtrangdonhang = status;
		var ghichu = '';
		var getValue = prompt("Điền ghi chú thay đổi (nếu có) : ", "");
		if(getValue!=null){
			ghichu = getValue;
		}
		
		var temp = <?php echo $temp ?>;
		<?php 
		if($this->user->UserType==2){
		?>
		var KhoID = $("#KhoID").val();
		if(tinhtrangdonhang==5 && KhoID==''){
			alert("Vui lòng chọn tên kho .");
			$("#KhoID").focus();
			$(ob).removeClass("saving");
			return false;
		}
		<?php 
		}
		?>
		
		<?php 
		if($this->user->UserType==4){
		?>
		var doitacvanchuyen = $("#doitacvanchuyen").val();
		if(tinhtrangdonhang==7 || tinhtrangdonhang=="7"){
			if(doitacvanchuyen==''){
				alert("Vui lòng chọn đối tác vận chuyển");
				$(ob).removeClass("saving");
				$(ob).find("i").show();
				return false;
			}
		}
		<?php 
		}
		?>

		if(tinhtrangdonhang==4 || tinhtrangdonhang==6 || tinhtrangdonhang==8){
			if(ghichu==''){
				alert("Nội dung ghi chú là bắt buộc đối với hành động trả đơn hàng về .");
				$(ob).removeClass("saving");
				$(ob).find("i").show();
				return false;
			}
		}		
		$.ajax({
        	url: link+"import_order/"+url,
            dataType: "html",
            type: "POST",
            context: this,
            data: <?php echo $this->user->UserType==4 ? '"TransportID="+doitacvanchuyen+"&"+' : '' ; ?><?php echo $this->user->UserType==2 ? '"KhoID="+KhoID+"&"+' : '' ; ?>"Tinhtrangdonhang="+tinhtrangdonhang+"&Ghichu="+ghichu+"&OrderID=<?php echo $data->ID ?>",
            success: function(result){}
        }).always(function(){
            addfromkho(status);
        });
	};

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	$("#add_products_to_order").click(function(){
		var KhoID = $('#KhoID').val();
		if(KhoID=='' || KhoID==0){
			alert('Vui lòng chọn kho lấy hàng');
			$('#KhoID').focus();
			return false;
		}
		enablescrollsetup();
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner").css({'margin-top':'20px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);        	
			    	$(".over_lay").removeClass('in');
			    	$(".over_lay").fadeIn('fast');
        			$(".over_lay").addClass('in');
                }else{
                	disablescrollsetup();
                	alert("Kho hàng hiện tại không có sản phẩm");
                }
                $(this).removeClass('saving');
            }
        });
	});

	function input_search_products(ob){
		var data = $(ob).val();
		var KhoID = $('#KhoID').val();
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID+"&Title="+data,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
	}

	var celldata = [<?php echo implode(',',$arrproducts) ?>];
	var sttrow = <?php echo count($arrproducts) ?>+1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				if(jQuery.inArray( "data"+data_id, celldata )<0){
					var table = document.getElementById("table_data");
					var row = table.insertRow(sttrow);
					row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
					row.insertCell(1).innerHTML=data_code;
					row.insertCell(2).innerHTML=data_name;
					row.insertCell(3).innerHTML="<span class='ShipmentDefault' data-id='"+data_id+"'></span>";
					data_price = parseInt(data_price);
					row.insertCell(4).innerHTML="<input type='number' class='dongia' value='"+data_price+"' min='1' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					row.insertCell(5).innerHTML="<input type='number' name='Amount[]' class='soluong' value='1' min='1' onchange='recal()' readonly='true' />";
					row.insertCell(6).innerHTML="<input type='number' onchange='calrowchietkhau(this)' class='percentck' value='0' min='0' readonly='true' style='display:none' /> <span class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
					row.insertCell(7).innerHTML="<input type='number' onchange='calrowchietkhau(this)' class='giack' value='0' min='0' readonly='true' style='display:none' /> <span class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
					row.insertCell(8).innerHTML="<input type='number' class='giasauCK' value='"+data_price+"' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					row.insertCell(9).innerHTML="<input type='number' class='thanhtien' value='"+data_price+"' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					sttrow=sttrow+1;
					celldata.push("data"+data_id);
				}
			}
		});
		loadshipmentdefault();
		$(".over_lay").hide();
		disablescrollsetup();
	}

	function checkshipment(ob){
		var amount = $(ob).val();
		var warehouse = $('#KhoID').val();
		var id = $(ob).parent('td').parent('tr').find('.ShipmentDefault').attr('data-id');
		$(ob).parent('td').parent('tr').find('.ShipmentDefault').load(link+"import_order/get_shipment_default/"+id+"/"+warehouse+"/"+amount);
		recal();
	}

	function loadshipmentdefault(){
		var warehouse = $('#KhoID').val();
		$(".ShipmentDefault").each(function(){
			var data = $(this).attr('data-id');
			var amount = $(this).parent('td').parent('tr').find('.soluong').val();
			$(this).load(link+"import_order/get_shipment_default/"+data+"/"+warehouse+"/"+amount);
		});
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				celldata.splice("data"+data_id, 1);
				sttrow = sttrow-1;
			}
		});
	});

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		if(ob.checked===true){
			parent.find('span.showsoluong').hide();
			parent.find('span.showdongia').hide();
			parent.find('span.showpercentCK').hide();
			parent.find('span.showgiaCK').hide();
			parent.find('input[type="text"]').prop('readonly', false);
			parent.find('input.soluong').prop('readonly', false);
			parent.find('input.dongia').prop('readonly', true).show();
			parent.find('input.percentck').prop('readonly', false).show();
			parent.find('input.giack').prop('readonly', false).show();
			parent.find('input[type="text"]').addClass('border');
			parent.find('input.soluong').addClass('border');
			parent.find('input.dongia').addClass('border');
			parent.find('input.percentck').addClass('border');
			parent.find('input.giack').addClass('border');
			parent.find('input.giasauck').addClass('border');
		}else{
			parent.find('span.showsoluong').show();
			parent.find('span.showdongia').show();
			parent.find('span.showpercentCK').show();
			parent.find('span.showgiaCK').show();
			parent.find('input[type="text"]').prop('readonly', true);
			parent.find('input.soluong').prop('readonly', true);
			parent.find('input.dongia').prop('readonly', true).hide();
			parent.find('input.percentck').prop('readonly', true).hide();
			parent.find('input.giack').prop('readonly', true).hide();
			parent.find('input[type="text"]').removeClass('border');
			parent.find('input.soluong').removeClass('border');
			parent.find('input.dongia').removeClass('border');
			parent.find('input.percentck').removeClass('border');
			parent.find('input.giack').removeClass('border');
			parent.find('input.giasauck').removeClass('border');
		}
	}

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').hide();
				parent.find('span.showdongia').hide();
				parent.find('span.showpercentCK').hide();
				parent.find('span.showgiaCK').hide();
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input.dongia').prop('readonly', true).show();
				parent.find('input.percentck').prop('readonly', false).show();
				parent.find('input.giack').prop('readonly', false).show();
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
				parent.find('input.dongia').addClass('border');
				parent.find('input.percentck').addClass('border');
				parent.find('input.giack').addClass('border');
				parent.find('input.giasauck').addClass('border');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').show();
				parent.find('span.showdongia').show();
				parent.find('span.showpercentCK').show();
				parent.find('span.showgiaCK').show();
				parent.find('input[type="text"]').prop('readonly', true);
				parent.find('input.soluong').prop('readonly', true);
				parent.find('input.dongia').prop('readonly', true).hide();
				parent.find('input.percentck').prop('readonly', true).hide();
				parent.find('input.giack').prop('readonly', true).hide();
				parent.find('input[type="text"]').removeClass('border');
				parent.find('input.soluong').removeClass('border');
				parent.find('input.dongia').removeClass('border');
				parent.find('input.percentck').removeClass('border');
				parent.find('input.giack').removeClass('border');
				parent.find('input.giasauck').removeClass('border');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-230;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function addfromkho(status){
		<?php 
		if($this->user->UserType==2){
		?>
		var tinhtrangdonhang = status;
		var KhoID = $("#KhoID").val();
		$("#tableform").append("<input type='hidden' name='WarehouseID' value='"+KhoID+"' />");
		if(tinhtrangdonhang==5){
			$.ajax({
	        	url: link+"import_order/add_from_kho",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: $("#tableform").serialize(),
	            success: function(result){
	            	alert(result);
	            	if(result=="OK"){
	                	var links = $("#referer").val();
	                	if(links!=''){
	                		window.location = links;
	                	}else{
	                		window.location = link+"import_order";
	                	}
	                }else{
	                	alert("Có lỗi xảy ra trong quá trình truyền tải dữ liệu !. Vui lòng kiểm tra lại đường truyền .");
	                }
	            }
	        });
        }else{
        	window.location = link+"import_order";
        }
        <?php 
    	}else{
        ?>
        window.location = link+"import_order";
        <?php 
    	}
        ?>
	}
	
	function send_cancel_request(ob){
		var note = $("#Lydohuydh").val();
		var another = $("#Lydokhac").val();
		var links_request = $(ob).attr('data');
		if(note=='' || (note==0 && another=='')){
			alert("Vui lòng chọn lý do hủy đơn hàng !");
			$("#Lydohuydh").focus();
		}else{
			$.ajax({
            	url: links_request,
	            dataType: "html",
	            type: "POST",
	            data: "ReasonID="+note+"&Note="+another,
	            success: function(result){
	            	location.reload();
	            }
	        });
		}
	}

	function checkshowlydokhac(ob){
		var data = $(ob).val();
		if(data==0){
			$(".lydokhac").show();
			$(".lydokhac").find('input').focus();
		}else{
			$(".lydokhac").hide();
		}
	}

	function show_cancelbox(){
		$(".cancel_box").toggle();
	}

	function confirmwarning(ob){
		if (!confirm('BẠN CÓ CHẮC MUỐN DUYỆT ĐƠN HÀNG NÀY VÀ CHUYỂN SANG ĐƠN HÀNG TIẾP THEO ??')) {
			return false;
		}
		$(ob).addClass("saving");
		$(ob).find("i").hide();
		var data = $(ob).attr('data');
		window.location=data;
	}

	function change_shipment(ob,shipment,products,amount,iddetails){
		var WarehouseID = $("#KhoID").val();
		if(WarehouseID!=''){
			$.ajax({
	        	url: link+"import_order/get_another_shipment",
	            dataType: "html",
	            type: "POST",
	            data: "ID="+iddetails+"&WarehouseID="+WarehouseID+"&ShipmentID="+shipment+"&ProductsID="+products+"&Amount="+amount,
	            success: function(result){
	            	$(ob).parent('td').find('ul').html(result);
	            }
	        });
        }
	}

	function replace_shipment(IDDetails,Shipment){
		if (!confirm('BẠN CÓ MUỐN ĐỔI LÔ LẤY HÀNG CHO SẢN PHẨM NÀY ??')) {
			return false;
		}else{
			$.ajax({
	        	url: link+"import_order/replace_shipment",
	            dataType: "html",
	            type: "POST",
	            data: "ID="+IDDetails+"&ShipmentID="+Shipment,
	            success: function(result){
	            	if(result=="False"){
	            		alert("Thay đổi lô hàng thất bại ! Mọi thay đổi của bạn ko được cập nhật !");
	            		location.reload();
	            	}else{
	            		alert("Thay đổi lô hàng thành công !");
	            		location.reload();
	            	}
	            }
	        });
		}
	}

	function changewarehouse(ob){
		var warehouse = $(ob).val();
		if(warehouse!=<?php echo $data->KhoID ?>){
			$(".accept_changewarehouse").removeClass('hidden');
		}else{
			$(".accept_changewarehouse").addClass('hidden');
		}
	}

	function accept_changewarehouse(ob){
		if (!confirm('BẠN CÓ CHẮC CHẮN MUỐN CHUYỂN KHO ??')) {
			return false;
		}else{
			var WarehouseID = $("#KhoID").val();
			$(ob).find('i').hide();
			$(ob).addClass('saving');
			$.ajax({
	        	url: link+"import_order/change_warehouse",
	            dataType: "html",
	            type: "POST",
	            data: "ID=<?php echo $data->ID ?>&WarehouseID="+WarehouseID,
	            success: function(result){
	            	$(ob).find('i').show();
					$(ob).removeClass('saving');
	            	if(result=="OK"){
	            		alert("Thay đổi kho thành công !");
	            		location.reload();
	            	}else{
	            		alert(result);
	            		location.reload();
	            	}
	            }
	        });
		}
	}
</script>