<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>BẢNG KIỂM TRA TÌNH TRẠNG TỒN KHO SẢN PHẨM</h1>
	    </div>
	    <div class="block2">
            <div class="row">
                <div class="col-xs-6">
                <select class="form-control" onchange="viewshipment(this)">
                    <option value="0">-- Tất cả các lô --</option>
                    <?php 
                    $warehouse = $this->db->query("select DISTINCT ShipmentCode from ttp_report_shipment")->result();
                    if(count($warehouse)>0){
                        foreach($warehouse as $row){
                            echo "<option value='$row->ShipmentCode'>-- $row->ShipmentCode --</option>";
                        }
                    }
                    ?>
                </select>
                </div>
                <div class="col-xs-6">
                <select class="form-control" onchange="viewwarehouse(this)">
                    <option value="0">-- Tất cả các kho --</option>
                    <?php 
                    $warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
                    if(count($warehouse)>0){
                        foreach($warehouse as $row){
                            echo "<option value='$row->ID'>-- $row->MaKho --</option>";
                        }
                    }
                    ?>
                </select>   
                </div>
            </div>
        </div>
    </div>
    <div class="import_orderlist">
    	<div class="block3 table_data">
    		<table>
    			<tr>
    				<th>STT</th>
    				<th>Mã Sản phẩm</th>
    				<th>Tên sản phẩm</th>
    				<th>Mã kho</th>
    				<th>Mã lô</th>
    				<th>Availabe</th>
    				<th>OnHand</th>
    			</tr>
    		<?php 
    		if(count($Data)>0){
    			$i = 1;
    			foreach($Data as $row){
    				echo "<tr class='rowtr row_$row->WarehouseID shipment_$row->ShipmentCode'>";
    				echo "<td>$i</td>";
    				echo "<td>$row->MaSP</td>";
    				echo "<td>$row->Title</td>";
    				echo "<td>$row->MaKho</td>";
    				echo "<td>$row->ShipmentCode</td>";
    				echo "<td style='text-align:right'>".number_format($row->Available)."</td>";
    				echo "<td style='text-align:right'>".number_format($row->OnHand)."</td>";
    				echo "</tr>";
    				$i++;
    			}
    		}else{
                echo "<tr><td colspan='7'>Không có dữ liệu tình trạng tồn kho của sản phẩm .</td></tr>";
            }
    		?>
    		</table>
    	</div>
	</div>
</div>
<style>
	table tr td:first-child{width:30px;text-align: center;background: #f7f7f7}
</style>
<script>
    var warehouse = 0;
    var shipment = 0;

    function viewwarehouse(ob){
        warehouse = $(ob).val();
        $("tr.rowtr").hide();
        if(warehouse==0){
            if(shipment==0){
                $("tr.rowtr").show();
            }else{
                $("tr.shipment_"+shipment).show();
            }
        }else{
            if(shipment==0){
                $("tr.row_"+warehouse).show();
            }else{
                $("tr.row_"+warehouse+".shipment_"+shipment).show();
            }
        }
    }

    function viewshipment(ob){
        shipment = $(ob).val();
        $("tr.rowtr").hide();
        if(shipment==0){
            if(warehouse==0){
                $("tr.rowtr").show();
            }else{
                $("tr.row_"+warehouse).show();
            }
        }else{
            if(warehouse==0){
                $("tr.shipment_"+shipment).show();
            }else{
                $("tr.row_"+warehouse+".shipment_"+shipment).show();
            }
        }
    }
</script>