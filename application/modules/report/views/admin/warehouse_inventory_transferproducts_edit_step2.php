<?php 
if($this->user->UserType!=2 && $this->user->IsAdmin!=1 && $this->user->UserType!=3){
	echo '<div class="warning_message" style="display:block"><span>Yêu cầu này đang trong trạng thái khóa . Mọi thay đổi của bạn hiện tại sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>';
}elseif($data->Status==4 && $this->user->UserType==2){
	echo '<div class="warning_message" style="display:block"><span>Yêu cầu này đang trong trạng thái khóa . Mọi thay đổi của bạn hiện tại sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>';
}
?>
<div class="containner">
	<div class="manager">
			<input type='hidden' name='ID' value='<?php echo $data->ID ?>' />
			<div class="import_select_progress">
				<div class="block1">
					<h1>PHIẾU YÊU CẦU LƯU CHUYỂN NỘI BỘ</h1>
				</div>
				<div class="block2">
					<div class="btn_group">
						<a href='<?php echo $base_link."mobilization/$data->ID" ?>' class="btn btn-primary"><i class="fa fa-print"></i> Lệnh Điều Động</a>
						<a href='<?php echo $base_link."accept_export_warehouse/$data->ID" ?>' class="btn btn-danger"><i class="fa fa-paper-plane"></i> Xuất kho</a>
					</div>
				</div>
			</div>
			<div class="box_content_warehouse_import">
				<div class="block1">
					<div class="row" style="margin:5px 0px">
			    		<textarea id="Ghichu" name="Ghichu" class="form-control" placeholder="Điền ghi chú cập nhật trạng thái (nếu có) ..."></textarea>
			    	</div>
					<div class="row">
		    			<div class="col-xs-4">
		    				<div class="form-group">
	    						<label class="col-xs-5 control-label">Ngày chứng từ: </label>
	    						<div class="col-xs-7">
			    					<input type='text' name='NgayXK' value='<?php echo date('Y-m-d',strtotime($data->DateCreated)) ?>' readonly="true" class="form-control" />
			    				</div>
		    				</div>
		    			</div>
						<div class="col-xs-4">
		    				<div class="form-group">
	    						<label class="col-xs-5 control-label">Kho xuất hàng: </label>
	    						<div class="col-xs-7">
			    					<select name='KhoSender' class="form-control" id="KhoSender" onchange="set_empty_table()">
										<?php 
										$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
										if(count($warehouse)>0){
											foreach($warehouse as $row){
												echo $data->WarehouseSender==$row->ID ? "<option value='$row->ID'>$row->MaKho</option>" : "";
											}
										}
										?>
									</select>
			    				</div>
		    				</div>
		    			</div>
		    			<div class="col-xs-4">
		    				<div class="form-group">
		    					<label class="col-xs-5 control-label">Kho nhận hàng: </label>
		    					<div class="col-xs-7">
			    					<select name='KhoReciver' id="KhoReciver" class="form-control">
									<?php 
									$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
									if(count($warehouse)>0){
										foreach($warehouse as $row){
											echo $data->WarehouseReciver==$row->ID ? "<option value='$row->ID'>$row->MaKho</option>" : "";
										}
									}
									?>
								</select>
			    				</div>
		    				</div>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-xs-4">
			    			<div class="form-group">
			    				<label class="col-xs-5 control-label">Lý do chuyển kho : </label>
								<label class="col-xs-7 control-label" style="white-space: nowrap;"><?php echo $data->Note=='' ? '--' : $data->Note ; ?> </label>
			    			</div>
		    			</div>
		    		</div>
				</div>
				<!-- end block1 -->
				<div class="clear"></div>
		    	<div class="table_donhang table_data">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã SP</th>
		    				<th>Tên sản phẩm</th>
		    				<th>ĐVT</th>
		    				<th style='width:155px;text-align:center'>Lô xuất hàng</th>
		    				<th style="width: 130px;">SL yêu cầu xuất</th>
		    				<th style="width: 130px;">SL thực xuất</th>
		    				<th style="width: 130px;">Ghi chú</th>
		    			</tr>
		    			<?php 
	    				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi,c.ShipmentCode from ttp_report_transferorder_details a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
		    			$total = 0;
		    			if(count($details)>0){
		    				foreach($details as $row){
		    					echo '<tr>
									<td><input type="checkbox" class="selected_products" data-id="'.$row->ProductsID.'"><input type="hidden" name="ProductsID[]" value="'.$row->ProductsID.'"></td>
		    							<td>'.$row->MaSP.'</td>
		    							<td>'.$row->Title.'</td>
		    							<td>'.$row->Donvi.'</td>
		    							<td style="width:155px;text-align:left">'.$row->ShipmentCode.'</td>
		    							<td><span>'.$row->Request.'</span></td>
		    							<td><span class="Availabletd">'.number_format($row->TotalExport).'</span></td>
		    							<td>'.$row->Note.'</td>';
		    					echo '</tr>';
		    					$total +=$row->TotalExport;
		    				}
		    			}
		    			?>
		    			<tr>
		    				<td colspan="6">TỔNG CỘNG</td>
		    				<td><span class='tongcong'><?php echo $total ?></span></td>
		    				<td></td>
		    			</tr>
		    		</table>
		    		<div class="history_status">
			    		<h3 style="margin-bottom:10px">Lịch sử trạng thái</h3>
			    		<?php 
			    		$history = $this->db->query("select a.*,b.UserName from ttp_report_transferorder_history a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
			    		if(count($history)>0){
			    			$arr_status = array(
			                    0=>'Yêu cầu chuyển kho',
					            1=>'Yêu cầu bị hủy',
					            2=>'Đã duyệt cho xuất kho',
					            3=>'Đã xuất kho & chờ nhập kho',
					            4=>'Đã nhập kho'
			                );
			    			echo "<table><tr><th style='width:200px'>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
			    			foreach($history as $row){
			    				echo "<tr>";
			    				echo isset($arr_status[$row->Status]) ? "<td>".$arr_status[$row->Status]."</td>" : "<td>--</td>" ;
			    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
			    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
			    				echo "<td>$row->UserName</td>";
			    				echo "</tr>";
			    			}
			    			echo "</table>";
			    		}
			    		?>
			    	</div>
		    	</div>
			</div>
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	function stopRKey(evt) { 
		var evt = (evt) ? evt : ((event) ? event : null); 
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
		if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
	} 

	document.onkeypress = stopRKey;

	$(document).ready(function () {
        $('#NgayNK,.DateProduction').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

</script>
<style>
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(5){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(6){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(7){text-align:right;}
</style>