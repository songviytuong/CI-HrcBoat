<div class="containner">
	<div class="disableedit" <?php echo $data->Status==1 ? 'style="height:1200px"' : '' ; ?>></div>
	<div class="status_progress">
    	<?php 
    	$status1 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 || $data->Status==2 ? 'class="active"' : "";
    	$point1 = $status1=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point1 = $data->Status==3 || $data->Status==5 || $data->Status==7  || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point1 ;

		$status2 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point2 = $status2=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point2 =  $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point2 ;    	

    	$status3 = $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point3 = $status3=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point3 =  $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point3 ;

    	$status4 = $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point4 = $status4=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point4 =  $data->Status==7 || $data->Status==8 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point4 ;

		$status5 = $data->Status==8 || $data->Status==7 || $data->Status==0 ? 'class="active"' : "";
    	$point5 = $status5=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point5 =  $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point5 ;    	
    	
    	$width = $data->Status==2 || $data->Status==4 || $data->Status==6 ? 20 : 0 ;
    	$width = $data->Status==3 ? 40 : $width ;
    	$width = $data->Status==5 ? 60 : $width ;
    	$width = $data->Status==9 ? 80 : $width ;
    	$width = $data->Status==7 || $data->Status==0 || $data->Status==8 ? 100 : $width ;

    	if($data->Status==1 || $data->Status==8){
    		$title_drop = $data->Status==8 ? "Bị trả về" : "Bị hủy" ;
    		echo "<div class='orderisdrop'><a>$title_drop</a><span><i class='fa fa-times'></i></span></div>";
    		echo '<div class="transfer_progress" style="width:100%;background:#000"></div>';
    	}else{
    	?>
    	<ul>
            <li class="active"><a>ĐH nháp</a><span class="ok"><i class="fa fa-check-circle"></i></span></li>
            <li <?php echo $status1 ?>><a>Chờ chuyển kho xử lý</a><?php echo $point1 ?></li>
            <li <?php echo $status2 ?>><a>Kho xác nhận</a><?php echo $point2 ?></li>
            <li <?php echo $status3 ?>><a>Kế toán xác nhận</a><?php echo $point3 ?></li>
            <li <?php echo $status4 ?>><a>Đã giao vận chuyển</a><?php echo $point4 ?></li>
            <li <?php echo $status5 ?>><a>Hoàn tất</a><?php echo $point5 ?></li>
        </ul>
        <div class="transfer_progress" style="width:<?php echo $width ?>%"></div>
        <?php 
    	}
        ?>
    </div>
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1><a href="<?php echo $base_link ?>">THÔNG TIN ĐƠN HÀNG</a> - <a style="color:#DE5252"><?php echo $data->MaDH ?></a></h1>
	    </div>
	    <div class="block2">
	    	<div class="btn_group">
	    		<a class="btn btn-default btn-drop pull-right" onclick="show_cancelbox()">Hủy <i class="fa fa-caret-down"></i></a>
				<?php 
				$checkleader = $this->db->query("select Data from ttp_report_team where UserID=".$this->user->ID)->row();
	            if($data->Status==2 || $data->Status==4 || $data->Status==6){
					echo '<a class="btn btn-danger btn-accept" type="submit" onclick="submitform(this,3)"><i class="fa fa-check-square"></i> Duyệt</button>';
					echo '<a class="btn btn-primary btn-update disablebutton" type="submit" onclick="submitform(this,'.$data->Status.')"><i class="fa fa-check-square"></i> Lưu</a>';
					echo '<a class="btn btn-success btn-activeedit" data="0"><i class="fa fa-pencil"></i> Chỉnh sửa</a>';
					echo '<a class="btn btn-default btn-cancel disablebutton" onclick="location.reload()"><i class="fa fa-undo"></i> Hủy bỏ</a>';
					echo $this->user->UserType==5 || $checkleader ? "<a class='btn btn-primary btn-print' href='".base_url().ADMINPATH."/report/import/printorder/$data->ID'><i class='fa fa-print'></i> In đơn hàng</a>" : "" ;
				}
				?>
				<a class="btn btn-primary btn-create" href="<?php echo $base_link.'add' ?>"><i class="fa fa-plus-square"></i> Thêm đơn hàng mới</a>
				<div class="cancel_box">
					<?php 
					$reasonchecked = $this->db->query("select * from ttp_report_order_reason_details where OrderID=$data->ID")->row();
					if($reasonchecked){
						if($reasonchecked->PLReason!=0 || $reasonchecked->Another3PL!=''){
							if($reasonchecked->Another3PL!=''){
								echo "<div class='PLReason'><b>Lý do hủy từ 3PL </b> : ".$reasonchecked->Another3PL."</div>";
							}else{
								$check_reason = $this->db->query("select * from ttp_report_order_reason where ID=$reasonchecked->PLReason")->row();
								echo $check_reason ? "<div class='PLReason'><b>Lý do hủy từ 3PL </b>: ".$check_reason->Title."</div>" : '' ;
							}
						}
					}
	    			$another = $reasonchecked ? $reasonchecked->AnotherSale : '' ;
	    			$Follow = $reasonchecked ? $reasonchecked->Follow : 0 ;
	    			$Follow = $Follow==1 ? "checked='checked'" : '' ;
	    			$reasonchecked = $reasonchecked ? $reasonchecked->SaleReasonID : 0 ; 
					?>
					<p>Chọn lý do hủy đơn hàng</p>
					<select id="Lydohuydh" onchange="checkshowlydokhac(this)" class="form-control">
		    			<option value=''>-- Lý do hủy đơn hàng --</option>
		    			<?php 
		    			$reason = $this->db->query("select * from ttp_report_order_reason")->result();
		    			if(count($reason)>0){
		    				foreach($reason as $row){
		    					$checked = $row->ID==$reasonchecked ? "selected='selected'" : '' ;
		    					echo "<option value='$row->ID' $checked>$row->Title</option>";
		    				}
		    			}

		    			?>
		    			<option value='0'>LÝ DO KHÁC</option>
		    		</select>
		    		<p class='lydokhac'><span>Lý do hủy khác : </span><input type='text' id='Lydokhac' placeholder="Điền lý do hủy khác ..." class="form-control" /></p>
		    		<p><input type='checkbox' name='Follow' id='Follow' style='float:left;margin-right:5px' <?php echo $Follow ?> /> Đánh dấu đơn hàng hủy cần theo dõi về sau .</p>
		    		<a class='btn btn-danger btn-acceptcancel' onclick='send_cancel_request(this)' data='<?php echo base_url().ADMINPATH."/report/import_order/cancel_request/$data->ID"; ?>'><i class='fa fa-minus-circle'></i> <?php echo $data->Status==1 ? 'Cập nhật lý do' : 'Xác nhận hủy' ; ?></a>
				</div>
			</div>
	    </div>
    </div>
    <div class="import_order_info">
    	<form id="submit_order" method="POST" action="<?php echo $base_link.'update_order_mtgt' ?>">
    		<input type='hidden' name="Tinhtrangdonhang" id="Status" value='<?php echo $data->Status ?>' />
    		<input type="hidden" name="IDOrder" value="<?php echo $data->ID ?>" />
	    	<div class="block1">
	    		<div class="row1 row">
	    			<input type="hidden" name="UserID" id="SalesmanID" value="<?php echo $this->user->ID ?>" />
	    			<input type="hidden" name="Ngayban" id="Ngayban" value="<?php echo date('m/d/Y',strtotime($data->Ngaydathang)); ?>" />
	    			<input type='hidden' id="loaidonhangstatus" class="form-control Age" value='<?php echo $data->OrderType ?>' />
	    		</div>
	    		<div class="row row3">
	    			<div class="col-xs-4">
	    				<div class="form-group find_customer">
    						<label class="col-xs-5 control-label">Loại khách hàng: </label>
    						<div class="col-xs-7">
		    					<input type="radio" class="Loaidonhang" name="Loaidonhang" <?php echo $data->OrderType==1 ? 'checked="checked"' : '' ; ?> onclick="changeloaidonhang(1)" value="1" />
				    			<span>GT</span>
				    			<input type="radio" class="Loaidonhang" name="Loaidonhang" <?php echo $data->OrderType==2 ? 'checked="checked"' : '' ; ?> value="2" onclick="changeloaidonhang(2)" />
				    			<span>MT</span>
				    			<input type="radio" class="Loaidonhang" name="Loaidonhang" <?php echo $data->OrderType==4 ? 'checked="checked"' : '' ; ?> onclick="changeloaidonhang(4)" value="4" />
				    			<span>TD</span>
				    			<input type="radio" class="Loaidonhang" name="Loaidonhang" <?php echo $data->OrderType==5 ? 'checked="checked"' : '' ; ?> value="5" onclick="changeloaidonhang(5)" />
				    			<span>NB</span>
		    				</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row row3">
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Tên NV bán hàng: </label>
    						<div class="col-xs-7">
		    					<input type='text' class="form-control" readonly="true" value="<?php echo $this->user->FirstName.' '.$this->user->LastName ?>" />
		    				</div>
	    				</div>
	    			</div>
					<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-5 control-label">Ngày đặt hàng: </label>
    						<div class="col-xs-7">
		    					<input type='text' name="CreatedDate" id="Created" class="form-control" value="<?php echo date('Y-m-d',strtotime($data->Ngaydathang)) ?>" />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-5 control-label">Ngày giao hàng: </label>
    						<div class="col-xs-7">
		    					<input type='text' name="DeliveryTime" id="DeliveryTime" class="form-control" value="<?php echo $data->DeliveryTime ?>" />
		    				</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row row3">
	    			<div class="col-xs-4">
	    				<div class="form-group find_customer">
    						<label class="col-xs-5 control-label">Tên đối tác / cửa hàng: </label>
    						<div class="col-xs-7">
		    					<input type='text' id="name_cus" class="form-control" />
		    				</div>
	    				</div>
	    			</div>
					<div class="col-xs-4">
	    				<div class="form-group find_customer">
	    					<a class="btn btn-default" id="find_customers" style="margin-top: 0px;"><i class="fa fa-search" aria-hidden="true"></i> Tìm đối tác khác</a>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row row7">
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Mã đối tác: </label>
    						<div class="col-xs-7">
		    					<input type='tetx' class="form-control" name="Code" id='Code' value="<?php echo $data->Code ?>" readonly="true" />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Tên đối tác: </label>
    						<div class="col-xs-7">
				    			<input type='hidden' name="CustomerID" class="form-control" id="CustomerID" value="<?php echo $data->CustomerID ?>" required />
			    				<input name="Tenkhachhang" id="Tenkhachhang" readonly="true" type="text" class="form-control" style="width: 285%;" value="<?php echo $data->Name ?>" />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-12" style="margin-top:10px">
	    				<div class="form-group">
	    					<div class='lichsugiaodich'><div class="history_cus"></div><a id='xemlichsugiaodich' data-status="down">Xem lịch sử giao dịch <b class='caret'></b></a></div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row row7">
	    			<?php 
	    			$tinhthanh = $this->db->query("select a.Title,a.ID,b.Title as AreaTitle,a.AreaID from ttp_report_city a,ttp_report_area b where a.AreaID=b.ID and a.ID=$data->CityID")->row();
	    			?>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Khu vực </label>
    						<div class="col-xs-7">
		    					<select id="Khuvuc" class="form-control">
				    				<option value="0">-- Chọn khu vực --</option>
				    				<?php 
				    				$area = $this->db->query("select * from ttp_report_area")->result();
				    				if(count($area)>0){
				    					foreach($area as $row){
				    						$selected= '';
				    						if($tinhthanh){
				    							$selected = $row->ID==$tinhthanh->AreaID ? "selected='selected'" : '' ;
				    						}
				    						echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Tỉnh / Thành </label>
    						<div class="col-xs-7">
		    					<select id="Tinhthanh" class="form-control" name="CityID">
				    				<?php 
				    					$selected= '';
			    						if($tinhthanh){
			    							$selected = $row->ID==$tinhthanh->AreaID ? "selected='selected'" : '' ;
			    							echo "<option value='$tinhthanh->ID'>$tinhthanh->Title</option>";
			    						}else{
			    							echo '<option value="">-- Chọn tỉnh thành --</option>';
			    						}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Quận / Huyện </label>
    						<div class="col-xs-7">
		    					<select id="Quanhuyen" class="form-control" name="DistrictID">
				    				<?php 
				    					$selected= '';
				    					$quanhuyen = $this->db->query("select * from ttp_report_district where CityID=$data->CityID")->result();
			    						if(count($quanhuyen)>0){
			    							foreach($quanhuyen as $row){
				    							$selected = $row->ID==$data->DistrictID ? "selected='selected'" : '' ;
				    							echo "<option value='$row->ID' $selected>$row->Title</option>";
			    							}
			    						}else{
			    							echo '<option value="">-- Chọn quận huyện --</option>';
			    						}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row row7">
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Số di động: </label>
    						<div class="col-xs-7">
		    					<input type='text' name="Phone1" id="Phone1" class="form-control" value='<?php echo $data->Phone1 ?>' required />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Thời gian: </label>
    						<div class="col-xs-7">
		    					<input type='text' name="DeliveryTime" id="DeliveryTime" class="form-control required" required />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">PT thanh toán</label>
    						<div class="col-xs-7">
		    					<select name="PTthanhtoan" class="form-control">
				    				<option value='0' <?php echo $data->Payment==0 ? "selected='selected'" : "" ; ?>>COD</option>
									<option value='1' <?php echo $data->Payment==1 ? "selected='selected'" : "" ; ?>>Chuyển khoản</option>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    		</div>
	    		<?php 
	    		if($data->OrderType==2){
	    		?>
	    		<div class="row row6">
	    			<div class="col-xs-12">
		    			<div class="form-group">
			    			<label class="col-xs-12 control-label">Tên công ty </label>
			    			<div class="col-xs-12">
			    				<input type='text' name="Company" id="Company" class="form-control" value="<?php echo $data->Company=='' ? '--' : $data->Company ; ?>" required />
			    			</div>
		    			</div>
	    			</div>
	    		</div>
	    		<?php 
	    		}
	    		?>
	    		<div class="row row6">
	    			<div class="col-xs-12">
		    			<div class="form-group">
			    			<label class="col-xs-12 control-label">Địa chỉ đăng ký </label>
			    			<div class="col-xs-12">
			    				<input type='text' id="Address_order" class="form-control" value="<?php echo $data->Address ?>" readonly="true" />
			    			</div>
		    			</div>
	    			</div>
	    		</div>
	    		<div class="row row6">
	    			<div class="col-xs-12">
		    			<div class="form-group">
			    			<label class="col-xs-12 control-label">Địa chỉ giao hàng </label>
			    			<div class="col-xs-12">
			    				<input type='text' name="Address" id="Address_order1" value="<?php echo $data->AddressOrder ?>" class="form-control" required />
			    			</div>
		    			</div>
	    			</div>
	    		</div>
	    		<div class="row row6">
	    			<div class="col-xs-12">
		    			<div class="form-group">
			    			<label class="col-xs-12 control-label">Ghi chú </label>
			    			<div class="col-xs-12">
			    				<input type='text' name="Note" class="form-control" value="<?php echo $data->Note ?>" required />
			    			</div>
		    			</div>
	    			</div>
	    		</div>
	    	</div>
			
			<!-- end block1 -->
	    	<div class="block2 row">
	    		<div class="col-xs-6">
	    			<div class="form-group">
		    			<div class="col-xs-2">
		    				<a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Sản phẩm</a>
		    			</div>
		    			<div class="col-xs-3">
		    			<ul>
							<li><a id='show_thaotac' class="btn btn-default">Thao tác <b class="caret"></b></a>
			    				<ul>
				    				<li><a id="edit_row_table"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a></li>
				    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
			    				</ul>
							</li>
						</ul>
						</div>
						<div class="col-xs-2">
						<a class="btn btn-primary btn-addreduce" id="add_reduce" style="margin-left:10px"><i class="fa fa-plus"></i> Giảm trừ</a>
						</div>
		    		</div>
		    	</div>
		    	<div class="col-xs-2"></div>
		    	<div class="col-xs-4">
		    		<div class="form-group">
		    			<label class="col-xs-4 control-label text-right">Kho lấy hàng </label>
		    			<div class="col-xs-7 pull-right">
		    				<select name="KhoID" id="KhoID" class="form-control" onchange="loadshipmentdefault()">
			    				<?php 
			    				$khoresult = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID=$data->KhoID")->row();
			    				if($khoresult){
			    					echo "<option value='$khoresult->ID'>$khoresult->MaKho</option>";
			    				}else{
			    					echo '<option value="">-- Chọn Kho --</option>';
			    				}
			    				?>
			    			</select>
		    			</div>
		    		</div>
		    	</div>
	    	</div>

	    	<div class="table_donhang table_data">
	    		<table class="table_data" id="table_data">
	    			<tr>
	    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
	    				<th>Mã sản phẩm</th>
	    				<th>Tên sản phẩm</th>
	    				<th>Lô</th>
	    				<th>Giá bán</th>
	    				<th>Số lượng</th>
	    				<th>% CK</th>
	    				<th>Giá trị CK</th>
	    				<th>Giá sau CK</th>
	    				<th>Thành tiền</th>											
	    			</tr>
	    			<?php 
	    			$details = $this->db->query("select a.*,b.Title,b.MaSP from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
	    			$arrproducts = array();
	    			if(count($details)>0){
	    				foreach($details as $row){
	    					echo "<tr>";
	    					echo "<td><input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='$row->ProductsID'>
	    							  <input type='hidden' name='ProductsID[]' value='$row->ProductsID'>
	    					</td>";
	    					echo "<td>$row->MaSP</td>";
	    					echo "<td>$row->Title</td>";
	    					echo "<td style='width:180px;text-align:left'><span class='ShipmentDefault' data-id='$row->ProductsID'>--</span></td>";
	    					$giaban = $row->Price+$row->PriceDown;
	    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
	    					echo "<td><input type='number' onchange='recal()' class='dongia' value='$giaban' min='0' onchange='calrowchietkhau(this,1)' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>".number_format($giaban,2)."</span></td>";
	    					echo "<td><input type='number' name='Amount[]' class='soluong' value='$row->Amount' min='1' onchange='recal()' readonly='true' /></td>";
	    					echo "<td><input type='number' name='PercentCK[]' onchange='calrowchietkhau(this,2)' class='percentck' value='$phantramck' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>$phantramck</span></td>";
							echo "<td><input type='number' name='GiaCK[]' onchange='calrowchietkhau(this,3)' class='giack' value='$row->PriceDown' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->PriceDown,2)."</span></td>";
							echo "<td><input type='number' name='Price[]' class='giasauCK' value='$row->Price' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Price,2)."</span></td>";
							echo "<td><input type='number' class='thanhtien' value='$row->Total' min='1' name='Thanhtien[]' readonly='true' onchange='recal_thanhtien(this)' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Total,2)."</span></td>";
	    					echo "</tr>";
	    					$arrproducts[] = '"data'.$row->ProductsID.'"';
	    				}
	    			}
	    			
	    			$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
	    			$tongtienhang = $data->Total - $data->Chietkhau;
	    			$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi-$data->Reduce;
	    			?>
	    			<tr class="last">
	    				<td colspan="9">Tổng cộng</td>
	    				<td><input type="number" class="tongcong" readonly="true" value="<?php echo $data->Total ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongcong"><?php echo number_format($data->Total,2) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="9">% chiết khấu</td>
	    				<td><input type='number' name="phantramchietkhau" class="phantramchietkhau" min="0" max="100" value="<?php echo $phantramchietkhau ?>" onchange="calchietkhau(this)" /></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="9">Giá chiết khấu</td>
	    				<td><input type='number' name="giachietkhau" class="giachietkhau" value="<?php echo $data->Chietkhau ?>" onchange="calchietkhau(this)" /></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="9">Tổng tiền hàng</td>
	    				<td><input type="number" class="tongtienhang" readonly="true" value="<?php echo $tongtienhang ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongtienhang"><?php echo number_format($tongtienhang,2) ?></span></td>
	    				
	    			</tr>
	    			<?php 
	    			$reduce = $this->db->query("select a.*,b.Title from ttp_report_reduce_order a,ttp_report_reduce b where a.ReduceID=b.ID and a.OrderID=$data->ID")->result();
	    			$arr_reduce = array();
	    			if(count($reduce)>0){
	    				foreach($reduce as $row){
	    					echo "<tr>";
	    					echo "<td><input type='checkbox' class='selected_reduce' onclick='changerow(this)' data-id='$row->ReduceID' /><input type='hidden' name='ReduceID[]' value='$row->ReduceID' /></td>";
	    					echo "<td colspan='8'>$row->Title <input type='text' name='Timereduce[]' style='display:inline;width:500px' value='$row->TimeReduce' /></td>";
	    					echo "<td><input type='number' name='Valuereduce[]' class='Valuereduce' value='".$row->ValueReduce."' onchange='recal()' /></td>";
	    					echo "</tr>";
	    					$arr_reduce[]='"data'.$row->ReduceID.'"';
	    				}
	    			}
	    			?>
	    			<tr class="last">
	    				<td colspan="9">Tổng giảm trừ</td>
	    				<td><input type="number" class="tonggiamtru" readonly="true" value="<?php echo $data->Reduce ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtonggiamtru"><?php echo number_format($data->Reduce,2) ?></span></td>
	    			</tr>
	    			<tr class="last">
	    				<td colspan="9">Chi phí vận chuyển</td>
	    				<td><input type='number' name="chiphivanchuyen" class="chiphivanchuyen" value="<?php echo $data->Chiphi ?>" onchange="recal()" style="display:none" /><span id="showchiphivanchuyen" style="display:block;text-align:right;padding-right:15px;" onclick="showchiphi()"><?php echo number_format($data->Chiphi,2) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="9">Tổng giá trị thanh toán</td>
	    				<td><input type='number' class="tonggiatrithanhtoan" readonly="true" value="<?php echo $tonggiatrithanhtoan ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiatrithanhtoan"><?php echo number_format($tonggiatrithanhtoan,2) ?></span></td>
	    				
	    			</tr>
	    		</table>
	    	</div>
	    	<div class="history_status">
	    		<h2>Lịch sử trạng thái đơn hàng</h2>
	    		<?php 
	    		$history = $this->db->query("select a.*,b.UserName from ttp_report_orderhistory a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
	    		if(count($history)>0){
	    			$array_status = array(
	    				10=>'Đơn hàng chờ hủy',
	                    9=>'Chờ nhân viên điều phối',
	                    8=>'Đơn hàng bị trả về',
	                    7=>'Chuyển sang bộ phận giao hàng',
	                    6=>'Đơn hàng bị trả về từ kế toán',
	                    5=>'Đơn hàng chờ kế toán duyệt',
	                    4=>'Đơn hàng bị trả về từ kho',
	                    3=>'Đơn hàng mới chờ kho duyệt',
	                    2=>'Đơn hàng nháp',
	                    0=>'Đơn hàng thành công',
	                    1=>'Đơn hàng hủy'
	                );
	    			echo "<table><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
	    			foreach($history as $row){
	    				echo "<tr>";
	    				echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>" ;
	    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
	    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
	    				echo "<td>$row->UserName</td>";
	    				echo "</tr>";
	    			}
	    			echo "</table>";
	    		}
	    		?>
	    	</div>
	    	<div class="block1">
	    		<input type="hidden" name="Ghichu" id="Ghichu" value="" />
	    		<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
	    	</div>
    	</form>
    </div>
    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
	.daterangepicker{width: 210px;}
	.daterangepicker .ranges{width:180px;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#DeliveryTime,#Created').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD'
        });
    });
</script>
<script type="text/javascript">
    var baselink = $("#baselink_report").val();

    $(".numbernoti").click(function(){
        $(this).parent().find('ul').toggle();
    });
    
	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	var link = $("#baselink_report").val();
	$("#find_customers").click(function(){
		var name_cus = $("#name_cus").val();
		var type = $("#loaidonhangstatus").val();
		if(name_cus!=''){
			enablescrollsetup();
			$(this).addClass('saving');
			$.ajax({
            	url: link+"import_order/getcusmtgt",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "type="+type+"&name_cus="+name_cus,
	            success: function(result){
	                if(result!='false'){
	                	$(".over_lay .box_inner").css({'margin-top':'50px'});
	                	$(".over_lay .box_inner .block1_inner h1").html("Danh sách khách hàng");
	                	$(".over_lay .box_inner .block2_inner").html(result);
	                	$(".over_lay").removeClass('in');
	                	$(".over_lay").fadeIn('fast');
	                	$(".over_lay").addClass('in');
	                }else{
	                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
	                }
	                $(this).removeClass('saving');
	            }
	        });
		}
	});

	$("#add_reduce").click(function(){
		enablescrollsetup();
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"import_order/get_reduce",
            dataType: "html",
            type: "POST",
            context: this,
            data: "",
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách các khoản cấn trừ kênh GT");
			    	$(".over_lay .box_inner .block2_inner").html(result);
			    	$(".over_lay").removeClass('in');
                	$(".over_lay").fadeIn('fast');
                	$(".over_lay").addClass('in');
                }else{
                	disablescrollsetup();
                	alert("Hệ thống hiện tại không có danh sách các khoản cấn trừ .");
                }
                $(this).removeClass('saving');
            }
        });
	});

	$("#add_products_to_order").click(function(){
		var KhoID = $('#KhoID').val();
		if(KhoID=='' || KhoID==0){
			alert('Vui lòng chọn kho lấy hàng');
			$('#KhoID').focus();
			return false;
		}
		enablescrollsetup();
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);        	
			    	$(".over_lay").removeClass('in');
                	$(".over_lay").fadeIn('fast');
                	$(".over_lay").addClass('in');
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
                $(this).removeClass('saving');
            }
        });
	});

	function input_search_products(ob){
		var data = $(ob).val();
		var KhoID = $('#KhoID').val();
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID+"&Title="+data,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
	}

	$.ajax({
    	url: link+"import_order/get_targets_user",
        dataType: "html",
        type: "POST",
        context: this,
        data: "IDOrder=<?php echo $data->ID ?>&UserID=<?php echo $data->UserID ?>",
        success: function(result){
        	if(result!='fasle')
        	$(".table_absolute").html(result);
        }
    });

	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_city_by_area",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            	$("#KhoID").html('<option value="">Không có kho hàng</option>');
	            	$("#KhoID").change();
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_district_by_city",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	                var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            	$("#KhoID").html(jsonobj.WarehouseHtml);
	            	$("#KhoID").change();
	            }
	        });
		}
	});

	var statusloadhis = 0;
	$("#xemlichsugiaodich").click(function(){
		var datastatus = $("#xemlichsugiaodich").attr("data-status");
		if(datastatus=="down"){
			$("#xemlichsugiaodich").attr("data-status","up");
			$(this).html("Ẩn lịch sử giao dịch <i class='fa fa-caret-up'></i>");
		}else{
			$("#xemlichsugiaodich").attr("data-status","down");
			$(this).html("Xem lịch sử giao dịch <b class='caret'></b>");
		}
		var salesman = $("#SalesmanID").val();
		var ID = $("#CustomerID").val();
		if(ID!='' && ID>0){
			if(statusloadhis==0){
				$.ajax({
	            	url: link+"import_order/get_history",
		            dataType: "html",
		            type: "POST",
		            context: this,
		            data: "ID="+ID+"&UserID="+salesman,
		            success: function(result){
		            	statusloadhis=1;
		                if(result!='false'){
			                $(".lichsugiaodich .history_cus").html(result);
		                }else{
		                	$(".lichsugiaodich .history_cus").html("<p>Không có lịch sử giao dịch của khách hàng này.</p>");
		                }
		            }
		        });
	        }
		}
		$(".lichsugiaodich .history_cus").toggle();
	});

	$("#edit_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
			}
		});
	});

	function calchietkhau(ob){
		tongcong = parseFloat($(".tongcong").val());
		var classname = $(ob).attr('class');
		data = parseFloat($(ob).val());
		if(classname=="phantramchietkhau"){
			giachietkhau = data*(tongcong/100);
			$(".giachietkhau").val(giachietkhau);
			$("#showgiachietkhau").html(giachietkhau.format('a',3));
		}else{
			phantramchietkhau = data/(tongcong/100);
			$(".phantramchietkhau").val(phantramchietkhau.toFixed(0));
		}
		recal_thanhtien();
	}

	function calrowchietkhau(ob,typechange){
		var parent = $(ob).parent('td').parent('tr');
		tongcong = parseFloat(parent.find('input.dongia').val());
		var classname = $(ob).attr('class');
		data = parseFloat($(ob).val());
		if(typechange==2){
			giachietkhau = data*(tongcong/100);
			var giasauchietkhau = tongcong-giachietkhau;
			if(giasauchietkhau>=0){
				parent.find('input.giack').val(giachietkhau);
				parent.find('span.showgiaCK').html(giachietkhau.format('a',3));
				parent.find('span.showpercentCK').html(data.format('a',0));
			}
		}
		if(typechange==3){
			var giasauchietkhau = tongcong-data;
			if(giasauchietkhau>=0){
				phantramchietkhau = Math.round((data/(tongcong/100))*10)/10;
				parent.find('input.percentck').val(phantramchietkhau);
				parent.find('span.showpercentCK').html(phantramchietkhau);
				parent.find('span.showgiaCK').html(data.format('a',0));
			}
		}
		if(typechange==1){
			giachietkhau = parseFloat(parent.find('input.giack').val());
			giasauchietkhau=data-giachietkhau;
		}
		if(giasauchietkhau>=0){
			parent.find('span.showgiasauCK').html(giachietkhau.format('a',3));
			parent.find('input.giasauCK').val(giachietkhau);
			recal();
		}
	}

	function recal(){
		chiphivanchuyen = parseFloat($(".chiphivanchuyen").val());
		$(".chiphivanchuyen").hide();
		$("#showchiphivanchuyen").html(chiphivanchuyen.format('a',3)).show();
		var tongcong = 0;
		var phantramchietkhau = parseFloat($(".phantramchietkhau").val());
		var chiphivanchuyen = parseFloat($(".chiphivanchuyen").val());
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			soluong = parseFloat(parent.find('input.soluong').val());
			dongia = parseFloat(parent.find('input.dongia').val());
			percentck = parseFloat(parent.find('input.percentck').val());
			giack = parseFloat(parent.find('input.giack').val());
			giasauck = dongia-giack;
			thanhtien = soluong*giasauck;
			parent.find('input.thanhtien').val(thanhtien);
			parent.find('span.showthanhtien').html(thanhtien.format('a',3));
			parent.find('input.giasauCK').val(giasauck.toFixed(2));
			parent.find('span.showgiasauCK').html(giasauck.format('a',3));
			tongcong = tongcong+thanhtien;
		});
		giachietkhau = parseFloat($(".giachietkhau").val());

		var giamtru = 0;
		$("#table_data .Valuereduce").each(function(){
			giamtru = giamtru + parseFloat($(this).val());
		});

		$(".tongcong").val(tongcong);
		$(".tongtienhang").val(tongcong-giachietkhau);
		$(".tonggiamtru").val(giamtru);
		$(".tonggiatrithanhtoan").val(tongcong-giachietkhau+chiphivanchuyen-giamtru);
		$("#showtongcong").html(tongcong.format('a',3));
		$("#showtongtienhang").html((tongcong-giachietkhau).format('a',3));
		$("#showtonggiamtru").html(giamtru.format('a',3));
		$("#showgiatrithanhtoan").html((tongcong-giachietkhau+chiphivanchuyen-giamtru).format('a',3));
	}

	function recal_thanhtien(){
		chiphivanchuyen = parseFloat($(".chiphivanchuyen").val());
		$(".chiphivanchuyen").hide();
		$("#showchiphivanchuyen").html(chiphivanchuyen.format('a',3)).show();
		var tongcong = 0;
		var phantramchietkhau = parseFloat($(".phantramchietkhau").val());
		var chiphivanchuyen = parseFloat($(".chiphivanchuyen").val());
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			soluong = parseFloat(parent.find('input.soluong').val());
			thanhtien = parseFloat(parent.find('input.thanhtien').val());
			dongia = thanhtien/soluong;
			parent.find('input.dongia').val(dongia.toFixed(2));
			giack = parseFloat(parent.find('input.giack').val());
			giasauck = dongia-giack;
			parent.find('input.giasauCK').val(giasauck.toFixed(2));
			parent.find('span.showthanhtien').html(thanhtien.format('a',3));
			tongcong = tongcong+thanhtien;

			parent.find('span.showdongia').html(dongia.format('a',3));
		});
		giachietkhau = parseFloat($(".giachietkhau").val());

		var giamtru = 0;
		$("#table_data .Valuereduce").each(function(){
			giamtru = giamtru + parseFloat($(this).val());
		});

		$(".tongcong").val(tongcong);
		$(".tongtienhang").val(tongcong-giachietkhau);
		$(".tonggiamtru").val(giamtru);
		$(".tonggiatrithanhtoan").val(tongcong-giachietkhau+chiphivanchuyen-giamtru);
		$("#showtongcong").html(tongcong.format('a',3));
		$("#showtongtienhang").html((tongcong-giachietkhau).format('a',3));
		$("#showtonggiamtru").html(giamtru.format('a',3));
		$("#showgiatrithanhtoan").html((tongcong-giachietkhau+chiphivanchuyen-giamtru).format('a',3));
	}

	function changestatus(){
		$("#IsChangeOrder").val("1");
	}

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').hide();
				parent.find('span.showdongia').hide();
				parent.find('span.showpercentCK').hide();
				parent.find('span.showgiaCK').hide();
				parent.find('span.showthanhtien').hide();
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input.dongia').prop('readonly', false).show();
				parent.find('input.percentck').prop('readonly', false).show();
				parent.find('input.giack').prop('readonly', false).show();
				parent.find('input.thanhtien').prop('readonly', false).show();
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
				parent.find('input.dongia').addClass('border');
				parent.find('input.percentck').addClass('border');
				parent.find('input.giack').addClass('border');
				parent.find('input.giasauck').addClass('border');
				parent.find('input.thanhtien').addClass('border');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').show();
				parent.find('span.showdongia').show();
				parent.find('span.showpercentCK').show();
				parent.find('span.showgiaCK').show();
				parent.find('span.showthanhtien').show();
				parent.find('input[type="text"]').prop('readonly', true);
				parent.find('input.soluong').prop('readonly', true);
				parent.find('input.dongia').prop('readonly', true).hide();
				parent.find('input.percentck').prop('readonly', true).hide();
				parent.find('input.giack').prop('readonly', true).hide();
				parent.find('input.thanhtien').prop('readonly', true).hide();
				parent.find('input[type="text"]').removeClass('border');
				parent.find('input.soluong').removeClass('border');
				parent.find('input.dongia').removeClass('border');
				parent.find('input.percentck').removeClass('border');
				parent.find('input.giack').removeClass('border');
				parent.find('input.giasauck').removeClass('border');
				parent.find('input.thanhtien').removeClass('border');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		if(ob.checked===true){
			parent.find('span.showsoluong').hide();
			parent.find('span.showdongia').hide();
			parent.find('span.showpercentCK').hide();
			parent.find('span.showgiaCK').hide();
			parent.find('span.showthanhtien').hide();
			parent.find('input[type="text"]').prop('readonly', false);
			parent.find('input.soluong').prop('readonly', false);
			parent.find('input.dongia').prop('readonly', false).show();
			parent.find('input.percentck').prop('readonly', false).show();
			parent.find('input.giack').prop('readonly', false).show();
			parent.find('input.thanhtien').prop('readonly', false).show();
			parent.find('input[type="text"]').addClass('border');
			parent.find('input.soluong').addClass('border');
			parent.find('input.dongia').addClass('border');
			parent.find('input.percentck').addClass('border');
			parent.find('input.giack').addClass('border');
			parent.find('input.thanhtien').addClass('border');
			parent.find('input.giasauck').addClass('border');
		}else{
			parent.find('span.showsoluong').show();
			parent.find('span.showdongia').show();
			parent.find('span.showpercentCK').show();
			parent.find('span.showgiaCK').show();
			parent.find('span.showthanhtien').show();
			parent.find('input[type="text"]').prop('readonly', true);
			parent.find('input.soluong').prop('readonly', true);
			parent.find('input.dongia').prop('readonly', true).hide();
			parent.find('input.percentck').prop('readonly', true).hide();
			parent.find('input.giack').prop('readonly', true).hide();
			parent.find('input.thanhtien').prop('readonly', true).hide();
			parent.find('input[type="text"]').removeClass('border');
			parent.find('input.soluong').removeClass('border');
			parent.find('input.dongia').removeClass('border');
			parent.find('input.percentck').removeClass('border');
			parent.find('input.giack').removeClass('border');
			parent.find('input.thanhtien').removeClass('border');
			parent.find('input.giasauck').removeClass('border');
		}
	}

	function select_this_custom(ob,id,phone1,code,address,name,surrogate,phone2){
		$(".over_lay").hide();
		disablescrollsetup();
		$("#CustomerID").val(id);
		$("#Tenkhachhang").val(name);
		$("#Phone1").val(phone1);
		$("#Phone2").val(phone2);
		$("#Code").val(code);
		$(".lichsugiaodich").show();
		$("#Address_order").val(address);
		$("#Address_order1").val(address);
		$("#Surrogate").val(surrogate);
		set_city(id);
		setsource(id);
	}

	function setsource(id){
		$.ajax({
        	url: link+"import_order/set_source",
            dataType: "html",
            type: "POST",
            context: this,
            data: "ID="+id,
            success: function(result){
            	$("#SourceID").val(result);
            }
        });
	}

	var celldata = [<?php echo implode(',',$arrproducts) ?>];
	var sttrow = <?php echo count($arrproducts) ?>+1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				if(jQuery.inArray( "data"+data_id, celldata )<0){
					var table = document.getElementById("table_data");
					var row = table.insertRow(sttrow);
					row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
					row.insertCell(1).innerHTML=data_code;
					row.insertCell(2).innerHTML=data_name;
					row.insertCell(3).innerHTML="<span class='ShipmentDefault' data-id='"+data_id+"'></span>";
					data_price = parseInt(data_price);
					row.insertCell(4).innerHTML = "<input type='text' class='dongia' value='"+data_price+"' min='1' onchange='calrowchietkhau(this,1)' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					row.insertCell(5).innerHTML = "<input type='number' name='Amout[]' class='soluong' value='1' min='1' onchange='checkshipment(this)' readonly='true' />";
					row.insertCell(6).innerHTML = "<input type='number' name='PercentCK[]' onchange='calrowchietkhau(this,2)' class='percentck' value='0' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
					row.insertCell(7).innerHTML = "<input type='number' name='GiaCK[]' onchange='calrowchietkhau(this,3)' class='giack' value='0' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
					row.insertCell(8).innerHTML = "<input type='number' name='Price[]' class='giasauCK' value='"+data_price+"' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					row.insertCell(9).innerHTML = "<input type='number' class='thanhtien' name='Thanhtien[]' value='"+data_price+"' min='1' onchange='recal_thanhtien(this)' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					sttrow=sttrow+1;
					celldata.push("data"+data_id);
				}
			}
		});
		loadshipmentdefault();
		$(".over_lay").hide();
		disablescrollsetup();
		recal();
	}

	var celldata_reduce = [<?php echo implode(',',$arr_reduce) ?>];
	function add_reduce(){
		$(".over_lay .box_inner .block2_inner .selected_reduce").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_id = $(this).attr('data-id');
				var rowCount = $('table#table_data tr:last').index() - 2;
				var timereduce = $(this).parent('td').parent('tr').find('.timereduce').val();
				var valuereduce = $(this).parent('td').parent('tr').find('.valuereduce').val();
				if(jQuery.inArray( "data"+data_id, celldata_reduce )<0){
					var table = document.getElementById("table_data");
					var row = table.insertRow(rowCount);
					row.insertCell(0).innerHTML = "<input type='checkbox' class='selected_reduce' onclick='changerow(this)' data-id='"+data_id+"' /><input type='hidden' name='ReduceID[]' value='"+data_id+"' />";
					row.insertCell(1).colSpan 	= 8;
					row.cells[1].innerHTML		= data_name+' <input type="text" name="Timereduce[]" style="display:inline;width:500px" value="'+timereduce+'" />';
					row.insertCell(2).innerHTML = "<input type='number' value='"+valuereduce+"' name='Valuereduce[]' class='Valuereduce' onchange='recal()' />";
					celldata_reduce.push("data"+data_id);
				}
			}
		});
		$(".over_lay").hide();
		disablescrollsetup();
		recal();
	}

	function checkshipment(ob){
		var amount = $(ob).val();
		var warehouse = $('#KhoID').val();
		var id = $(ob).parent('td').parent('tr').find('.ShipmentDefault').attr('data-id');
		$(ob).parent('td').parent('tr').find('.ShipmentDefault').load(link+"import_order/get_shipment_default/"+id+"/"+warehouse+"/"+amount);
		recal();
	}

	function loadshipmentdefault(){
		var warehouse = $('#KhoID').val();
		$(".ShipmentDefault").each(function(){
			var data = $(this).attr('data-id');
			var amount = $(this).parent('td').parent('tr').find('.soluong').val();
			$(this).load(link+"import_order/get_shipment_default/"+data+"/"+warehouse+"/"+amount);
		});
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				var index = celldata.indexOf("data"+data_id);
				celldata.splice(index, 1);
				sttrow = sttrow-1;
			}
		});
		$("#table_data .selected_reduce").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				var index = celldata_reduce.indexOf("data"+data_id);
				celldata_reduce.splice(index, 1);
			}
		});
		recal();
	});

	function submitform(ob,status){
		$("#Status").val(status);
		var nvbanhang = $("#SalesmanID").val();
		if(nvbanhang==''){
			$("#SalesmanID").focus();
			alert("Vui lòng chọn nhân viên bán hàng");
			return false;
		}
		var Tinhthanh = $("#Tinhthanh").val();
		if(Tinhthanh==''){
			alert("Vui lòng chọn tỉnh thành");
			return false;
		}
		var Quanhuyen = $("#Quanhuyen").val();
		if(Quanhuyen==''){
			alert("Vui lòng chọn quận huyện");
			return false;
		}
		if(celldata.length==0){
			alert("Đơn hàng đang rỗng .Vui lòng thêm sản phẩm vào đơn hàng .");
			return false;
		}
		if(status!=<?php echo $data->Status ?>){
			var getValue = prompt("Điền ghi chú thay đổi (nếu có) : ", "");
			if(getValue!=null){
				$("#Ghichu").val(getValue);
			}
		}
		$(ob).find("i").hide();
		$(ob).addClass('saving');
		$("#submit_order").submit();
	}

	function showchiphi(){
		$("#showchiphivanchuyen").hide();
		$(".chiphivanchuyen").show();
		$(".chiphivanchuyen").focus();
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function send_cancel_request(ob){
		if($("#Follow").prop('checked')==true){
			var checked_follow=1;
		}else{
			var checked_follow=0;
		}
		var note = $("#Lydohuydh").val();
		var another = $("#Lydokhac").val();
		var links_request = $(ob).attr('data');
		if(note=='' || (note==0 && another=='')){
			alert("Vui lòng chọn lý do hủy đơn hàng !");
			$("#Lydohuydh").focus();
		}else{
			$.ajax({
            	url: links_request,
	            dataType: "html",
	            type: "POST",
	            data: "ReasonID="+note+"&Note="+another+"&Follow="+checked_follow,
	            success: function(result){
	            	window.location=link+"import_order";
	            }
	        });
		}
	}

	function checkshowlydokhac(ob){
		var data = $(ob).val();
		if(data==0){
			$(".lydokhac").find('input').focus();
			$(".lydokhac").show();
		}else{
			$(".lydokhac").hide();
		}
	}

	function show_cancelbox(){
		$(".cancel_box").toggle();
	}

	function set_city(id){
		$.ajax({
        	url: link+"import_order/get_last_order_city/"+id,
            dataType: "text",
            success: function(result){
            	var jsonobj = JSON.parse(result);
            	$("#Khuvuc").val(jsonobj.AreaID);
            	$("#Tinhthanh").html(jsonobj.CityHtml);
            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
            	$("#KhoID").html(jsonobj.WarehouseHtml);
            }
        });
	}

	function changeloaidonhang(status){
		$("#loaidonhangstatus").val(status);
	}
</script>