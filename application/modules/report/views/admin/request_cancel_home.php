<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$month 			= array();
$dayofmonth		= array();
$dayofmonth_real= array();
?>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>DANH SÁCH YÊU CẦU HỦY ĐƠN HÀNG</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
	</div>
	<div class="import_orderlist">
	    <div class="clear"></div>
	    <div class="block3 table_data">
	    	<table id="table_data">
    			<tr>
    				<th>STT</th>
    				<th>Ngày / Giờ</th>
    				<th>Mã ĐH</th>
    				<th>Lý do hủy</th>
    				<th>Người yêu cầu</th>
                    <th>Trạng thái xử lý</th>
                    <th>Người xử lý</th>
    			</tr>
    			<?php 
    			$arr_trangthai = array(0=>"Chưa xử lý",1=>"Đã xử lý");
                $useraccept = $this->db->query("select ID,UserName from ttp_user where IsAdmin=1 or UserType in(5,7,8)")->result();
                $arr_useraccept = array();
                if(count($useraccept)>0){
                    foreach($useraccept as $row){
                        $arr_useraccept[$row->ID] = $row->UserName;
                    }
                }
    			if(count($data)>0){
    				$i = $start+1;
    				foreach($data as $row){
    					echo $row->Status==0 ? "<tr style='background:#FFDCDC'>" : "<tr>";
    					echo "<td style='width:30px;text-align:center;background:#F5F5F5'>$i</td>";
    					echo "<td style='width:128px'><a href='".$base_link."edit/$row->ID'>".date('d/m/Y H:i',strtotime($row->Created))."</a></td>";
    					echo "<td style='width:128px'><a href='".$base_link."edit/$row->ID'>".$row->MaDH."</a></td>";
    					echo "<td style='width:auto'><a href='".$base_link."edit/$row->ID'>$row->Note</a></td>";
    					echo "<td style='width:128px'><a href='".$base_link."edit/$row->ID'>".$row->UserName."</a></td>";
    					echo "<td style='width:128px'><a href='".$base_link."edit/$row->ID'>".$arr_trangthai[$row->Status]."</a></td>";
                        echo isset($arr_useraccept[$row->UserAccept]) ? "<td style='width:128px'>".$arr_useraccept[$row->UserAccept]."</td>" : "<td style='width:128px'>--</td>";
                        echo "</tr>";
    					$i++;
    				}
    			}else{
    				echo "<tr><td colspan='7'>Không tìm thấy yêu cầu hủy đơn hàng.</td></tr>";
    			}
    			?>
    		</table>
    		<?php 
                echo $nav;
            ?>
	    </div>
    </div>
    <div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<?php 
    $time_startday = strtotime($startday);
    $time_stopday = strtotime($stopday);
    $currday = strtotime(date('Y-m-d',time()));
?>
<script>
	$(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day_warehouse",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });

</script>