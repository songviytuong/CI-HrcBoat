<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo isset($data->ID) ? $data->ID : 0 ; ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa quận huyện</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tên quận / huyện</span></div>
					<div class='block2'><input type='text' class="form-control" name="Title" value="<?php echo isset($data->Title) ? $data->Title : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Thuộc tỉnh thành</span></div>
					<div class='block2'>
						<select name="CategoriesID" class="form-control">
							<option value="0">-- Chọn tỉnh thành --</option>
							<?php 
							$categories = $this->db->query("select * from ttp_report_city")->result();
							if(count($categories)>0){
								foreach($categories as $row){
									$select = $row->ID==$data->CityID ? "selected='selected'" : '' ;
									echo "<option value='$row->ID' $select>$row->Title</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Mã GHN</span></div>
					<div class='block2'><input type='text' class="form-control" name="GHN" value="<?php echo isset($data->GHN) ? $data->GHN : '' ; ?>" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Mã GoldTimes</span></div>
					<div class='block2'><input type='text' class="form-control" name="GoldTimes" value="<?php echo isset($data->GoldTimes) ? $data->GoldTimes : '' ; ?>" /></div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>