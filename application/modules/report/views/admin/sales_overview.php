<?php 
$active_vs = $this->session->userdata("active_vs");
$startday = $this->session->userdata("startday");
$startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
$stopday = $this->session->userdata("stopday");
$stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

$startday_vs = $this->session->userdata("startday_vs");
$startday_vs = $startday_vs!='' ? $startday_vs : date('Y-m-01',time()) ;
$stopday_vs = $this->session->userdata("stopday_vs");
$stopday_vs = $stopday_vs!='' ? $stopday_vs : date('Y-m-d',time()) ;

$Orderstatus = $this->session->userdata("Orderstatus");
$bonus_fillter = $Orderstatus=='' ? '' : ' and a.Status=0' ;
$Orderstatus = $Orderstatus=='' ? 0 : $Orderstatus ;

/*
*********************************
*	Define variable				*
*								*
*********************************
*/

$total_Order 		= array();
$total_success_Order= 0;
$total_false_Order 	= 0;
$total_Customer 	= array();
$total_Sales 		= 0;
$total_Products 	= 0;
$total_new_order 	= 0;
$percent_new_order 	= 0;
$percent_prod_order = 0;
$percent_success	= 0;
$percent_false		= 0;
$abs_order			= 0;
$city_order 		= array();
$categories_order 	= array();
$area_order 		= array();
$products_order 	= array();
$point_per_day		= array();
$point_per_order	= array();
$point_per_customer	= array();
$point_per_new		= array();
$point_per_products = array();
$point_products_per_order = array();
$total_day			= array();
$show_day			= array();

$total_Order_vs 		= array();
$total_success_Order_vs	= 0;
$total_false_Order_vs 	= 0;
$total_Customer_vs 		= array();
$total_Sales_vs 		= 0;
$total_Products_vs 		= 0;
$total_new_order_vs 	= 0;
$percent_new_order_vs 	= 0;
$percent_prod_order_vs 	= 0;
$percent_success_vs		= 0;
$percent_false_vs		= 0;
$abs_order_vs			= 0;
$city_order_vs 			= array();
$categories_order_vs 	= array();
$area_order_vs 			= array();
$products_order_vs 		= array();
$point_per_day_vs		= array();
$point_per_order_vs		= array();
$point_per_customer_vs	= array();
$point_per_new_vs		= array();
$point_per_products_vs 	= array();
$point_products_per_order_vs = array();
$total_day_vs			= array();
$show_day_vs			= array();


/*
*********************************************
*	Excute and set variable	in first range	*
*											*
*********************************************
*/

$total_order = $this->db->query("select DISTINCT a.ID,a.Status,a.CustomerType,a.CustomerID,a.SoluongSP,a.Total,a.Chietkhau,a.Chiphi,a.Reduce,a.CityID,b.Amount,b.ProductsID,a.Ngaydathang,c.CategoriesID from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where a.ID=b.OrderID and c.ID=b.ProductsID and date(Ngaydathang)>='$startday' and date(Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.CustomerID!=0 and a.OrderType=0 $bonus_fillter")->result();
if(count($total_order)>0){
	foreach($total_order as $row){
		if(!isset($products_order[$row->ProductsID][$row->ID])){
			$products_order[$row->ProductsID][$row->ID]='';
		}
		/* Total order */
		if(!array_key_exists($row->ID,$total_Order)){
			$total_Order[$row->ID] = "";
			if($Orderstatus==1){
				/* Total success and false order */
				if($row->Status==0){
					$total_success_Order++;
				}
				if($row->Status==1){
					$total_false_Order++;
				}
				/* Total Sales */
				$total_Sales+= $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce ;

				/* Total order group by city */
				if(isset($city_order[$row->CityID])){
					$city_order[$row->CityID]['sl'] += 1;
					$city_order[$row->CityID]['total'] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce ;
				}else{
					$city_order[$row->CityID]['sl'] = 1 ;
					$city_order[$row->CityID]['total'] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
				}

				/* Total order group by categories */
				if(isset($categories_order[$row->CategoriesID])){
					$categories_order[$row->CategoriesID]['sl'] += 1 ;
					$categories_order[$row->CategoriesID]['total'] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
				}else{
					$categories_order[$row->CategoriesID]['sl'] = 1 ;
					$categories_order[$row->CategoriesID]['total'] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
				}

				/* Total customer */
				if(!in_array($row->CustomerID,$total_Customer)){
					$total_Customer[] = $row->CustomerID;
				}

				/* Total new order */
				if($row->CustomerType==0){
					$total_new_order++;
				}

				/* Get list day */
				$time_key = date('Ymd',strtotime($row->Ngaydathang));
				if(array_key_exists($time_key,$point_per_day)){
					$point_per_products[$time_key] += 1 ;
					$point_per_new[$time_key] += 1 ;
					$point_per_customer[$time_key] += 1 ;
					$point_per_order[$time_key] += 1 ;
					$point_per_day[$time_key] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce ;
					if($row->Status==0){
						$point_products_per_order[$time_key]=array(
							'Total'=>$point_products_per_order[$time_key]['Total']+1,
							'Amount'=>$point_products_per_order[$time_key]['Amount']
						);
					}
				}else{
					$point_per_products[$time_key] = 1 ;
					$point_per_new[$time_key] = 1 ;
					$point_per_customer[$time_key] = 1 ;
					$point_per_order[$time_key] = 1 ;
					$point_per_day[$time_key] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					if($row->Status==0){
						$point_products_per_order[$time_key]=array('Total'=>1);
					}
				}
				if($row->Status==0){
					if(isset($point_products_per_order[$time_key]['Amount'])){
						$point_products_per_order[$time_key]['Amount'] += $row->SoluongSP;
					}else{
						$point_products_per_order[$time_key]['Amount'] = $row->SoluongSP;
					}
				}else{
					$point_products_per_order[$time_key]['Amount']=0;
					$point_products_per_order[$time_key]['Total']=0;
				}
			}else{
				/* Total success and false order */
				if($row->Status!=1){
					$total_success_Order++;
				}else{
					$total_false_Order++;
				}
				/* Total Sales */
				$total_Sales+= $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;

				/* Total order group by city */
				if(isset($city_order[$row->CityID])){
					$city_order[$row->CityID]['sl'] += 1;
					$city_order[$row->CityID]['total'] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
				}else{
					$city_order[$row->CityID]['sl'] = 1 ;
					$city_order[$row->CityID]['total'] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
				}

				/* Total order group by categories */
				if(isset($categories_order[$row->CategoriesID])){
					$categories_order[$row->CategoriesID]['sl'] += 1;
					$categories_order[$row->CategoriesID]['total'] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
				}else{
					$categories_order[$row->CategoriesID]['sl'] = 1;
					$categories_order[$row->CategoriesID]['total'] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
				}

				/* Total customer */
				if(!in_array($row->CustomerID,$total_Customer)){
					$total_Customer[] = $row->CustomerID;
				}

				/* Total new order */
				if($row->CustomerType==0){
					$total_new_order++;
				}

				/* Get list day */
				$time_key = date('Ymd',strtotime($row->Ngaydathang));
				if(array_key_exists($time_key,$point_per_day)){
					$point_per_products[$time_key] += 1 ;
					$point_per_new[$time_key] += 1 ;
					$point_per_customer[$time_key] += 1 ;
					$point_per_order[$time_key] += 1 ;
					$point_per_day[$time_key] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					$point_products_per_order[$time_key]=array(
							'Total'=>$point_products_per_order[$time_key]['Total']+1,
							'Amount'=>$point_products_per_order[$time_key]['Amount']
						);
				}else{
					$point_per_products[$time_key] = 1 ;
					$point_per_new[$time_key] = 1 ;
					$point_per_customer[$time_key] = 1 ;
					$point_per_order[$time_key] = 1 ;
					$point_per_day[$time_key] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					$point_products_per_order[$time_key]=array('Total'=>1);
				}
				if(isset($point_products_per_order[$time_key]['Amount'])){
					$point_products_per_order[$time_key]['Amount'] += $row->SoluongSP;
				}else{
					$point_products_per_order[$time_key]['Amount'] = $row->SoluongSP;
				}
			}
			/* Total products */
			$total_Products+= $row->SoluongSP;
		}
	}
	$total_Order = count($total_Order);
	$percent_prod_order = round($total_Products/$total_Order,1);
	$abs_order = round($total_Sales/$total_Order);
	$percent_success = round($total_success_Order/($total_Order/100));
	$percent_false = 100-$percent_success;
	$percent_new_order = round($total_new_order/(count($total_Customer)/100),2);
}

/*
*********************************************
*	Excute and set variable	in second range	*
*											*
*********************************************
*/
$arr_time = array();
if($active_vs==1){
	$total_order = $this->db->query("select DISTINCT a.ID,a.Status,a.CustomerType,a.CustomerID,a.SoluongSP,a.Total,a.Chietkhau,a.Chiphi,a.Reduce,a.CityID,b.Amount,b.ProductsID,a.Ngaydathang,c.CategoriesID from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where a.ID=b.OrderID and c.ID=b.ProductsID and date(Ngaydathang)>='$startday_vs' and date(Ngaydathang)<='$stopday_vs' and a.CustomerID!=9996 and a.OrderType=0 $bonus_fillter")->result();
	if(count($total_order)>0){
		foreach($total_order as $row){
			if(!isset($products_order_vs[$row->ProductsID][$row->ID])){
				$products_order_vs[$row->ProductsID][$row->ID]='';
			}
			/* Total order */
			if(!array_key_exists($row->ID,$total_Order_vs)){
				$total_Order_vs[$row->ID] = "";
				if($Orderstatus==1){
					/* Total success and false order */
					if($row->Status==0){
						$total_success_Order_vs++;
					}
					if($row->Status==1){
						$total_false_Order_vs++;
					}
					/* Total Sales */
					$total_Sales_vs+= $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce ;

					/* Total order group by city */
					if(isset($city_order_vs[$row->CityID])){
						$city_order_vs[$row->CityID]['sl'] += 1 ;
						$city_order_vs[$row->CityID]['total'] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce ;
					}else{
						$city_order_vs[$row->CityID]['sl'] = 1 ;
						$city_order_vs[$row->CityID]['total'] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}

					/* Total order group by categories */
					if(isset($categories_order_vs[$row->CategoriesID])){
						$categories_order_vs[$row->CategoriesID]['sl'] += 1 ;
						$categories_order_vs[$row->CategoriesID]['total'] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce ;
					}else{
						$categories_order_vs[$row->CategoriesID]['sl'] = 1 ;
						$categories_order_vs[$row->CategoriesID]['total'] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}


					/* Total customer */
					if(!in_array($row->CustomerID,$total_Customer_vs)){
						$total_Customer_vs[] = $row->CustomerID;
					}

					/* Total new order */
					if($row->CustomerType==0){
						$total_new_order_vs++;
					}

					/* Get list day */
					$time_key = date('Ymd',strtotime($row->Ngaydathang));
					if(array_key_exists($time_key,$point_per_day_vs)){
						$point_per_products_vs[$time_key] += 1 ;
						$point_per_new_vs[$time_key] += 1 ;
						$point_per_customer_vs[$time_key] += 1 ;
						$point_per_order_vs[$time_key] += 1 ;
						$point_per_day_vs[$time_key] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce ;
					}else{
						$point_per_products_vs[$time_key] = 1 ;
						$point_per_new_vs[$time_key] = 1 ;
						$point_per_customer_vs[$time_key] = 1 ;
						$point_per_order_vs[$time_key] = 1 ;
						$point_per_day_vs[$time_key] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}
				}else{
					/* Total success and false order */
					if($row->Status!=1){
						$total_success_Order_vs++;
					}else{
						$total_false_Order_vs++;
					}
					/* Total Sales */
					$total_Sales_vs+= $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;

					/* Total order group by city */
					if(isset($city_order_vs[$row->CityID])){
						$city_order_vs[$row->CityID]['sl'] += 1;
						$city_order_vs[$row->CityID]['total'] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}else{
						$city_order_vs[$row->CityID]['sl'] = 1 ;
						$city_order_vs[$row->CityID]['total'] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}

					/* Total order group by categories */
					if(isset($categories_order_vs[$row->CategoriesID])){
						$categories_order_vs[$row->CategoriesID]['sl'] += 1;
						$categories_order_vs[$row->CategoriesID]['total'] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}else{
						$categories_order_vs[$row->CategoriesID]['sl'] = 1;
						$categories_order_vs[$row->CategoriesID]['total'] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}

					/* Total customer */
					if(!in_array($row->CustomerID,$total_Customer_vs)){
						$total_Customer_vs[] = $row->CustomerID;
					}

					/* Total new order */
					if($row->CustomerType==0){
						$total_new_order_vs++;
					}

					/* Get list day */
					$time_key = date('Ymd',strtotime($row->Ngaydathang));
					if(array_key_exists($time_key,$arr_time)){
						$point_per_products_vs[$time_key] += 1 ;
						$point_per_new_vs[$time_key] += 1 ;
						$point_per_customer_vs[$time_key] += 1 ;
						$point_per_order_vs[$time_key] += 1 ;
						$point_per_day_vs[$time_key] += $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}else{
						$point_per_products_vs[$time_key] = 1 ;
						$point_per_new_vs[$time_key] = 1 ;
						$point_per_customer_vs[$time_key] = 1 ;
						$point_per_order_vs[$time_key] = 1 ;
						$point_per_day_vs[$time_key] = $row->Total+$row->Chiphi-$row->Chietkhau-$row->Reduce;
					}
				}
			}

			/* Total products */
			$total_Products_vs+= $row->SoluongSP;
		}
		$total_Order_vs = count($total_Order_vs);
		$percent_prod_order_vs = round($total_Products_vs/$total_Order_vs,1);
		$abs_order_vs = round($total_Sales_vs/$total_Order_vs);
		$percent_success_vs = round($total_success_Order_vs/($total_Order_vs/100));
		$percent_false_vs = 100-$percent_success_vs;
		$percent_new_order_vs = round($total_new_order_vs/(count($total_Customer_vs)/100),2);
	}
}

?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
	<div class="select_progress">
	    <div class="block1">
	    	<a class="btn btn-primary">Export To Excel</a>
	    </div>
	    <div class="block2">
		    <div class="dropdown">
		    	<a class='value_excute'><span><?php echo date('d/m/Y',strtotime($startday))." - ".date('d/m/Y',strtotime($stopday)) ?></span> <b class="caret"></b></a>
		    	<p class="value_excute_vs" <?php echo $active_vs==1 ? "style='display:block'" : "style='display:none'" ; ?>>So sánh với : <span><?php echo date('d/m/Y',strtotime($startday_vs))." - ".date('d/m/Y',strtotime($stopday_vs)) ?></span></p>
		    	<div class="box_dropdown">
		    		<div id="reportrange" class="list_div">
				        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
				        <span></span> <b class="caret"></b>
				    </div>
				    <div class="center_div list_div">
				    	<span><input type='checkbox' id='active_vs' <?php echo $active_vs==1 ? "checked='checked'" : "" ; ?> />So sánh với</span>
				    	<select id="vs_selectbox" <?php echo $active_vs==1 ? "style='opacity:1'" : "style='opacity:0.6'" ; ?>>
				    		<option value="tuychinh">Tùy chỉnh</option>
				    		<option value="lastmonth">Tháng trước</option>
				    		<option value="last7day">7 ngày trước</option>
				    		<option value="last30day">30 ngày trước</option>
				    	</select>
				    </div>
				    <div id="reportrange1" class="list_div" <?php echo $active_vs==1 ? "style='display:block'" : "style='display:none'" ; ?>>
				        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
				        <span></span> <b class="caret"></b>
				    </div>
				    <div class='compare list_div'>
				    	<a id="showcompare"><i class="fa fa-exchange"></i> Áp dụng</a>
				    	<a id="cancel_showcompare">Hủy</a>
				    </div>	
		    	</div>
		    </div>
	    </div>
    </div>
    <div class="title_content"><span>Báo cáo doanh số</span></div>
    <div class="report_sales_fill">
    	<div class="block1">
    		<div>
	    		<select id="value_sosanh">
	    			<option value="1">Doanh số</option>
	    			<option value="2">Sản phẩm bán</option>
	    			<option value="3">Đơn hàng</option>
	    			<option value="4">Khách hàng</option>
	    		</select>
    		</div>
    		<div><span>VS</span></div>
    		<div>
	    		<select id="vs_sosanh">
	    			<option value="">-- Chọn chỉ số khác --</option>
	    			<option value="2">Chỉ tiêu doanh số</option>
	    			<option value="3">Chỉ tiêu sản phẩm bán</option>
	    			<option value="4">Chỉ tiêu đơn hàng</option>
	    			<option value="1">Chỉ tiêu khách hàng</option>
	    		</select>
	    	</div>
	    	<div><span>|</span></div>
	    	<div>
	    		<select id="orderstatus">
	    			<option value="0" <?php echo $Orderstatus==0 ? "selected='selected'" : '' ; ?>>Tất cả đơn hàng</option>
	    			<option value="1" <?php echo $Orderstatus==1 ? "selected='selected'" : '' ; ?>>Chỉ đơn hàng thành công</option>
	    		</select>
	    	</div>
    	</div>

    	<div class="block2 select_day_week_month">
    		<ul>
    			<li><a data='plotchart_primary' class='active'>Ngày</a></li>
    			<li><a data='plotchart_primary_week'>Tuần</a></li>
    			<li><a data='plotchart_primary_month'>Tháng</a></li>
    		</ul>
    	</div>
    </div>
    <div class="chart linechart" style="height:280px;overflow:hidden">
		<div id="plotchart_primary" class="plotchart_item_primary" style="width: 100%;height: 100%;font-size: 16px;line-height: 1.2em;font-family:tahoma"></div>
		<div id="plotchart_primary_week" class="plotchart_item_primary" style="display:none;width: 100%;height: 100%;font-size: 16px;line-height: 1.2em;font-family:tahoma"></div>
		<div id="plotchart_primary_month" class="plotchart_item_primary" style="display:none;width: 100%;height: 100%;font-size: 16px;line-height: 1.2em;font-family:tahoma"></div>
	</div>
	<div class="chart piechart">
		<div class="block1">
			<div class="list_item">
				<p class='title'>Số đơn hàng</p>
				<h3><?php echo is_array($total_Order) ? 0 : number_format($total_Order,0) ?></h3>
				<i><?php echo $active_vs==1 && is_numeric($total_Order_vs) ? "So sánh với ".number_format($total_Order_vs,0) : ""; ?></i>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item1" class="itemchart"></div>
				</div>
			</div>
			<div class="list_item">
				<p class='title'>Người mua</p>
				<h3><?php echo number_format(count($total_Customer),0) ?></h3>
				<i><?php echo $active_vs==1 ? "So sánh với ".number_format(count($total_Customer_vs),0) : ""; ?></i>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item2" class="itemchart"></div>
				</div>
			</div>
			<div class="list_item">
				<p class='title'>Doanh số</p>
				<h3><?php echo number_format($total_Sales,0) ?></h3>
				<i><?php echo $active_vs==1 ? "So sánh với ".number_format($total_Sales_vs,0) : ""; ?></i>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item3"  class="itemchart"></div>
				</div>
			</div>
			<div class="list_item">
				<p class='title'>Tỷ lệ KH mới</p>
				<h3><?php echo $percent_new_order ?>%</h3>
				<i><?php echo $active_vs==1 ? "So sánh với ".$percent_new_order_vs."%" : ""; ?></i>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item4" class="itemchart"></div>
				</div>
			</div>
			<div class="list_item">
				<p class='title'>SP / Đơn hàng</p>
				<h3><?php echo $percent_prod_order ?></h3>
				<i><?php echo $active_vs==1 ? "So sánh với ".$percent_prod_order_vs : ""; ?></i>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item5" class="itemchart"></div>
				</div>
			</div>
			<div class="list_item">
				<p class='title'>Giá trị trung bình đơn hàng</p>
				<h3><?php echo number_format($abs_order,0) ?></h3>
				<i><?php echo $active_vs==1 ? "So sánh với ".number_format($abs_order_vs) : ""; ?></i>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item6" class="itemchart"></div>
				</div>
			</div>
		</div>
		<div class="block2">
			<div class="description">
				<a><span style="background:#29e"></span> Đơn hàng thành công (<?php echo $total_success_Order.' - '.$percent_success.'%' ?>)</a>
				<a><span style="background:#F00"></span> Đơn hàng hủy (<?php echo $total_false_Order.' - '.$percent_false."%" ?>)</a>
				<p><?php echo date("M d,Y",strtotime($startday))." - ".date("M d,Y",strtotime($stopday)) ?></p>
			</div>
			<div>
                <canvas id="canvas_pie"></canvas>
            </div>
		</div>
	</div>
	<div class="details">
		<div class="block1">
			<p>Theo địa lý</p>
			<ul>
				<li><a onclick="show_table_report(this,'area_table')" class="active_menu_report">Theo vùng</a></li>
				<li><a onclick="show_table_report(this,'city_table')" class="active_menu_report">Theo thành phố</a></li>
			</ul>
			<p>Theo sản phẩm</p>
			<ul>
				<li><a onclick="show_table_report(this,'categories_table')" class="active_menu_report">Nhóm ngành hàng</a></li>
				<li><a onclick="show_table_report(this,'products_table')" class="active_menu_report">Sản phẩm</a></li>
			</ul>
		</div>
		<div class="block2">
			<div class="city_table table_report">
				<table>
					<tr>
						<th></th>
						<th>Thành phố</th>
						<th>SL đ.hàng</th>
						<th>Doanh số</th>
						<th>Tỷ trọng</th>
					</tr>
					<?php 
					$city = $this->db->query("select * from ttp_report_city")->result();
					if(count($city)>0){
						$i=1;
						$city_report_order_total = 0;
						$city_report_price_total = 0;
						foreach($city as $row){
							if(isset($city_order[$row->ID])){
								if(isset($area_order[$row->AreaID])){
									$area_order[$row->AreaID]['sl'] = $area_order[$row->AreaID]['sl'] + $city_order[$row->ID]['sl'];
									$area_order[$row->AreaID]['total'] = $area_order[$row->AreaID]['total'] + $city_order[$row->ID]['total'];
								}else{
									$area_order[$row->AreaID]['sl']=$city_order[$row->ID]['sl'];
									$area_order[$row->AreaID]['total'] = $city_order[$row->ID]['total'];
								}
								$city_report_order_total += $city_order[$row->ID]['sl'];
								$city_report_price_total += $city_order[$row->ID]['total'];
								$percent_by_city = $total_Sales==0 ? 0 : round($city_order[$row->ID]['total'] / ($total_Sales/100),2);
								echo '<tr>
										<td>'.$i.'</td>
										<td>'.$row->Title.'</td>
										<td>'.$city_order[$row->ID]['sl'].'</td>
										<td>'.number_format($city_order[$row->ID]['total']).'</td>
										<td>'.$percent_by_city.'%</td>
									</tr>';
								$i++;
							}
						}
						echo '<tr>
										<td></td>
										<td>Tổng cộng</td>
										<td>'.number_format($city_report_order_total).'</td>
										<td>'.number_format($city_report_price_total).'</td>
										<td>100%</td>
									</tr>';
					}
					?>
				</table>
			</div>
			<div class="area_table table_report">
				<table>
					<tr>
						<th></th>
						<th>Vùng</th>
						<th>SL đ.hàng</th>
						<th>Doanh số</th>
						<th>Tỷ trọng</th>
					</tr>
					<?php 
					$area = $this->db->query("select * from ttp_report_area")->result();
					if(count($area)>0){
						$i=1;
						$area_report_order_total = 0;
						$area_report_price_total = 0;
						foreach($area as $row){
							if(isset($area_order[$row->ID])){
								$area_report_order_total += $area_order[$row->ID]['sl'];
								$area_report_price_total += $area_order[$row->ID]['total'];
								$percent_by_area = round($area_order[$row->ID]['total'] / ($total_Sales/100),2);
								echo '<tr>
										<td>'.$i.'</td>
										<td>'.$row->Title.'</td>
										<td>'.$area_order[$row->ID]['sl'].'</td>
										<td>'.number_format($area_order[$row->ID]['total']).'</td>
										<td>'.$percent_by_area.'%</td>
									</tr>';
								$i++;
							}
						}
						echo '<tr>
										<td></td>
										<td>Tổng cộng</td>
										<td>'.$area_report_order_total.'</td>
										<td>'.number_format($area_report_price_total).'</td>
										<td>100%</td>
									</tr>';
					}
					?>
				</table>
			</div>
			<div class="products_table table_report">
				<table>
					<tr>
						<th></th>
						<th>Sản phẩm</th>
						<th>SL đ.hàng</th>
						<th>SL sản phẩm</th>
						<th>Doanh số</th>
						<th>Tỷ trọng</th>
					</tr>
					<?php 
					$total_Sales_products = $this->db->query("select sum(b.Total) as TotalPrice from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where a.ID=b.OrderID and c.ID=b.ProductsID and date(Ngaydathang)>='$startday' and date(Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.OrderType=0 $bonus_fillter")->row();
					$total_Sales_products = $total_Sales_products ? $total_Sales_products->TotalPrice : 0 ;
					$products = $this->db->query("select b.ProductsID,c.Title,sum(b.Amount) as TotalAmount,sum(b.Total) as TotalPrice from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where a.ID=b.OrderID and c.ID=b.ProductsID and date(Ngaydathang)>='$startday' and date(Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.OrderType=0 $bonus_fillter group by b.ProductsID")->result();
					if(count($products)>0){
						$i=1;
						$products_report_amount_total = 0;
						$products_report_price_total = 0;
						foreach($products as $row){
							$products_report_amount_total += $row->TotalAmount;
							$products_report_price_total += $row->TotalPrice;
							$products_report_order_total = isset($products_order[$row->ProductsID]) ? count($products_order[$row->ProductsID]) : 0 ;
							$percent_by_products = round($row->TotalPrice / ($total_Sales_products/100),2);
							echo '<tr>
									<td>'.$i.'</td>
									<td>'.$row->Title.'</td>
									<td>'.number_format($products_report_order_total).'</td>
									<td>'.number_format($row->TotalAmount).'</td>
									<td>'.number_format($row->TotalPrice).'</td>
									<td>'.$percent_by_products.'%</td>
								</tr>';
							$i++;
						}
						echo '<tr>
									<td></td>
									<td>Tổng cộng</td>
									<td>'.number_format($area_report_order_total).'</td>
									<td>'.number_format($products_report_amount_total).'</td>
									<td>'.number_format($products_report_price_total).'</td>
									<td>100%</td>
								</tr>';
					}
					?>
				</table>
				<div style="clear:both;width:96%;margin-left:4%"><span class="text-danger">(*)</span> : Tổng số lượng đơn hàng sẽ không bằng tổng số đơn hàng trên từng sản phẩm vì trong mỗi đơn hàng có thể có nhiều sản phẩm .</div>
			</div>
			<div class="categories_table table_report">
				<table>
					<tr>
						<th></th>
						<th>Ngành hàng</th>
						<th>SL đ.hàng</th>
						<th>Doanh số</th>
						<th>Tỷ trọng</th>
					</tr>
					<?php 
					$categories = $this->db->query("select * from ttp_report_categories")->result();
					if(count($categories)>0){
						$i=1;
						foreach($categories as $row){
							if(isset($categories_order[$row->ID])){
								$percent_by_categories = round($categories_order[$row->ID]['total'] / ($total_Sales/100),2);
								echo '<tr>
										<td>'.$i.'</td>
										<td>'.$row->Title.'</td>
										<td>'.$categories_order[$row->ID]['sl'].'</td>
										<td>'.number_format($categories_order[$row->ID]['total']).'</td>
										<td>'.$percent_by_categories.'%</td>
									</tr>';
								$i++;
							}
						}
					}
					?>
				</table>
			</div>
		</div>
	</div>
	<div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<!-- datepicker -->
<?php 
	$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
	$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
	
?>
<script type="text/javascript">
    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /****  reportrange 2  ****/
        var cb1 = function (start, end, label) {
            $('#reportrange1 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        var optionSet2 = {
            startDate: moment().startOf('month'),
            endDate: moment(),
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 120
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange1 span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange1').daterangepicker(optionSet2, cb1);

        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".value_excute span").html(picker.startDate.format('DD/MM/YYYY')+" - "+picker.endDate.format('DD/MM/YYYY'));
		});

		$('#reportrange1').on('apply.daterangepicker', function(ev, picker) {
		    $(".value_excute_vs span").html(picker.startDate.format('DD/MM/YYYY')+" - "+picker.endDate.format('DD/MM/YYYY'));
		    $(".value_excute_vs").show();
		});
    });
</script>
<!-- /datepicker -->

<!-- flot -->
<?php 
$arrpoint 				= array();
$showday 				= array();
$total_day 				= array();
$total_day_week 		= array();
$total_day_month 		= array();
$arrpoint_per_order 	= array();
$arrpoint_per_customer 	= array();
$arrpoint_per_new 		= array();
$arrpoint_per_products 	= array();
$arrpoint_per_abs 		= array();

$arrpoint_week 			= array();
$showday_week 			= array();

$arrpoint_month 		= array();
$showday_month 			= array();

$arr_month_total		= array();



$reset=0;
$reset_month=0;
$temp_count = 1;
$day_cache = date('d/m/Y',strtotime($startday));

$arr_point_temp_vs = array();
for($i=strtotime($startday); $i < strtotime($stopday)+1; $i=$i+3600*24) { 
	$temp_point_second = $temp_count-1;
	$strday = str_replace('-','',date('Y-m-d',$i));
	$arrpoint[] = isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	$arrpoint_per_order[] = isset($point_per_order[$strday]) ? $point_per_order[$strday] : 0 ;
	$arrpoint_per_customer[] = isset($point_per_customer[$strday]) ? $point_per_customer[$strday] : 0 ;
	$arrpoint_per_new[] = isset($point_per_new[$strday]) ? $point_per_new[$strday] : 0 ;
	if(isset($point_products_per_order[$strday]) && isset($point_products_per_order[$strday])){
		$arrpoint_per_products[] = $point_products_per_order[$strday]['Total']==0 ? 0 : round($point_products_per_order[$strday]['Amount']/$point_products_per_order[$strday]['Total'],1);
	}else{
		$arrpoint_per_products[] = 0;
	}
	if(isset($point_per_day[$strday]) && isset($point_per_order[$strday])){
		$arrpoint_per_abs[] = $point_per_order[$strday]==0 ? 0 : round($point_per_day[$strday]/$point_per_order[$strday],1);
	}else{
		$arrpoint_per_abs[] = 0;
	}
	$temp_value = $i*1000;
	$total_day[] = $temp_value;
	$show_day[] = $temp_value.":'".date('d/m/Y',$i)."'";
	$arr_point_temp_vs[] = $temp_value;

	/* 
	********************************
	*	Value of Week on chart     *
	*							   *
	********************************
	*/

	if($temp_count%7==1){
		$reset = $reset+1;
		$total_day_week[] = $i*1000;
		$k = $i+60*60*24*6;
		if(strtotime($stopday)<$k){
			$k = strtotime($stopday);
		}
		$showday_week[] = $temp_value.":' $day_cache - ".date('d/m/Y',$k)."'";	
		$day_cache = date('d/m/Y',$k+3600*24);
	}
	if(isset($arrpoint_week[$reset])){
		$arrpoint_week[$reset] += isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	}else{
		$arrpoint_week[$reset] = isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	}

	/* 
	********************************
	*	Value of Month on chart    *
	*							   *
	********************************
	*/
	$month = date('mY',$i);
	if(!isset($arr_month_total[$month])){
		$total_day_month[] = $i*1000;
		$j = $i+60*60*24*30;
		if(strtotime($stopday)<$j){
			$j = strtotime($stopday);
		}
		$showday_month[] = $temp_value.":' Tháng ".date('m/Y',$i)."'";
		$arr_month_total[$month] = "";
		$reset_month = $reset_month+1;
	}
	
	if(isset($arrpoint_month[$reset_month])){
		$arrpoint_month[$reset_month] += isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	}else{
		$arrpoint_month[$reset_month] = isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	}

	$temp_count++;
}
/*
*	check if only 1 point
*/

if($reset_month<=1){
	$l = strtotime($stopday)*1000;
	$m = strtotime($startday)*1000;
	if($reset_month==1){
		$showday_month[0] = "$m:'".date('d/m/Y',strtotime($startday)).' - '.date('d/m/Y',strtotime($stopday))."'";
		$arrpoint_month[] = $arrpoint_month[1];
	}else{
		$total_day_month[] = $m;
		$arrpoint_month[1] = isset($arrpoint_month[0]) ? $arrpoint_month[0] : 0 ;
		$showday_month[0] = "$m:'".date('d/m/Y',strtotime($startday)).' - '.date('d/m/Y',strtotime($stopday))."'";
	}
	$reset_month=1;
}

/*
***************************
*	Second range 		  *
*						  *
***************************
*/
/***************************
*	Variable range second  *
*						   *
***************************/
$arrpoint_vs 				= array();
$show_day_vs 				= array();
$total_day_vs 				= array();
$total_day_week_vs 			= array();
$total_day_month_vs 		= array();
$arrpoint_per_order_vs 		= array();
$arrpoint_per_customer_vs 	= array();
$arrpoint_per_new_vs 		= array();
$arrpoint_per_products_vs 	= array();
$arrpoint_per_abs_vs 		= array();
$arrpoint_target_vs 		= array();

$arrpoint_week_vs 			= array();
$arrpoint_target_week_vs	= array();
$showday_week_vs 			= array();

$arrpoint_month_vs 			= array();
$arrpoint_target_month_vs	= array();
$showday_month_vs 			= array();

$arr_month_total_vs			= array();

$reset=0;
$reset_month=0;
$temp_count = 1;
$day_cache = date('d/m/Y',strtotime($startday));
$stt = 0;
for($i=strtotime($startday_vs); $i < strtotime($stopday_vs)+1; $i=$i+3600*24) { 
	$strday = str_replace('-','',date('Y-m-d',$i));
	$arrpoint_vs[] = isset($point_per_day_vs[$strday]) ? $point_per_day_vs[$strday] : 0 ;
	$arrpoint_per_order_vs[] = isset($point_per_order_vs[$strday]) ? $point_per_order_vs[$strday] : 0 ;
	$arrpoint_per_customer_vs[] = isset($point_per_customer_vs[$strday]) ? $point_per_customer_vs[$strday] : 0 ;
	$arrpoint_per_new_vs[] = isset($point_per_new_vs[$strday]) ? $point_per_new_vs[$strday] : 0 ;
	if(isset($point_per_products_vs[$strday]) && isset($point_per_order_vs[$strday])){
		$arrpoint_per_products_vs[] = round($point_per_products_vs[$strday]/$point_per_order_vs[$strday],1);
	}else{
		$arrpoint_per_products_vs[] = 0;
	}
	if(isset($point_per_day_vs[$strday]) && isset($point_per_order_vs[$strday])){
		$arrpoint_per_abs_vs[] = round($point_per_day_vs[$strday]/$point_per_order_vs[$strday],1);
	}else{
		$arrpoint_per_abs_vs[] = 0;
	}
	$temp_value = $i*1000;
	$total_day_vs[] = $temp_value;
	$show_day_vs[] = isset($arr_point_temp_vs[$stt]) ? $arr_point_temp_vs[$stt].":'".date('d/m/Y',$i)."'" : "0:'".date('d/m/Y',$i)."'" ;
	$stt++;
	/* 
	********************************
	*	Value of Week on chart     *
	*							   *
	********************************
	*/

	if($temp_count%7==1){
		$reset = $reset+1;
		$total_day_week_vs[] = $i*1000;
		$k = $i+60*60*24*6;
		if(strtotime($stopday)<$k){
			$k = strtotime($stopday);
		}
		$showday_week_vs[] = $temp_value.":' $day_cache - ".date('d/m/Y',$k)."'";	
		$day_cache = date('d/m/Y',$k+3600*24);
	}
	if(isset($arrpoint_week_vs[$reset])){
		$arrpoint_week_vs[$reset] += isset($point_per_day_vs[$strday]) ? $point_per_day_vs[$strday] : 0 ;
	}else{
		$arrpoint_week_vs[$reset] = isset($point_per_day_vs[$strday]) ? $point_per_day_vs[$strday] : 0 ;
	}

	/* 
	********************************
	*	Value of Month on chart    *
	*							   *
	********************************
	*/
	$month = date('mY',$i);
	if(!isset($arr_month_total_vs[$month])){
		$total_day_month[] = $i*1000;
		$j = $i+60*60*24*30;
		if(strtotime($stopday)<$j){
			$j = strtotime($stopday);
		}
		$showday_mont_vsh[] = $temp_value.":' Tháng ".date('m/Y',$i)."'";
		$arr_month_total_vs[$month] = "";
		$reset_month = $reset_month+1;
	}
	
	if(isset($arrpoint_month_vs[$reset_month])){
		$arrpoint_month_vs[$reset_month] += isset($point_per_day_vs[$strday]) ? $point_per_day_vs[$strday] : 0 ;
	}else{
		$arrpoint_month_vs[$reset_month] = isset($point_per_day_vs[$strday]) ? $point_per_day_vs[$strday] : 0 ;
	}

	$temp_count++;
}

/*
*	check if only 1 point
*/

if($reset_month<=1){
	$l = strtotime($stopday)*1000;
	$m = strtotime($startday)*1000;
	if($reset_month==1){
		$showday_month[0] = "$m:'".date('d/m/Y',strtotime($startday)).' - '.date('d/m/Y',strtotime($stopday))."'";
		$arrpoint_month[] = $arrpoint_month[1];
	}else{
		$total_day_month[] = $m;
		$arrpoint_month[1] = isset($arrpoint_month[0]) ? $arrpoint_month[0] : 0 ;
		$showday_month[0] = "$m:'".date('d/m/Y',strtotime($startday)).' - '.date('d/m/Y',strtotime($stopday))."'";
	}
	$reset_month=1;
}

/*
***************************
*	Tick date 			  *
*						  *
***************************
*/

$tickday = "1,'day'";
$tickweek = "1,'day'";
$tickmonth = "1,'month'";
if(count($total_day)>30 && count($total_day)<60){
	$tickday = "5,'day'";
	$tickweek = "5,'day'";
	$tickmonth = "10,'day'";
}elseif (count($total_day)>=60 && count($total_day)<120) {
	$tickday = "10,'day'";
	$tickweek = "10,'day'";
	$tickmonth = "1,'month'";
}elseif (count($total_day)>=120 && count($total_day)<240) {
	$tickday = "20,'day'";
	$tickweek = "20,'day'";
	$tickmonth = "1,'month'";
}elseif (count($total_day)>=240) {
	$tickday = "30,'day'";
	$tickweek = "30,'day'";
	$tickmonth = "1,'month'";
}elseif(count($total_day)<6){
	$tickweek = "1,'day'";
	$tickmonth = "1,'day'";
}elseif (count($total_day)>=6 && count($total_day)<=30) {
	$tickweek = "1,'day'";
	$tickmonth = "1,'day'";
}

?>
<script type="text/javascript">
    var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];
    var pointvalue = [<?php echo implode(',',$arrpoint); ?>];
    var pointday = [<?php echo implode(',',$total_day); ?>];
    var showday = {<?php echo implode(',',$show_day); ?>};
    var pointperorder = [<?php echo implode(',',$arrpoint_per_order); ?>];
    var pointpercustomer = [<?php echo implode(',',$arrpoint_per_customer); ?>];
    var pointpernew = [<?php echo implode(',',$arrpoint_per_new); ?>];
    var pointperproducts = [<?php echo implode(',',$arrpoint_per_products); ?>];
    var pointperabs = [<?php echo implode(',',$arrpoint_per_abs); ?>];

    var pointvalue_week = [<?php echo implode(',',$arrpoint_week) ?>];
    var pointday_week = [<?php echo implode(',',$total_day_week); ?>];
    var showday_week = {<?php echo implode(',',$showday_week); ?>};

    var pointvalue_month = [<?php echo implode(',',$arrpoint_month) ?>];
    var pointday_month = [<?php echo implode(',',$total_day_month); ?>];
    var showday_month = {<?php echo implode(',',$showday_month); ?>};

    var pointvalue_vs = [<?php echo implode(',',$arrpoint_vs); ?>];
    var pointday_vs = [<?php echo implode(',',$total_day_vs); ?>];
    var showday_vs = {<?php echo implode(',',$show_day_vs); ?>};
    var pointperorder_vs = [<?php echo implode(',',$arrpoint_per_order_vs); ?>];
    var pointpercustomer_vs = [<?php echo implode(',',$arrpoint_per_customer_vs); ?>];
    var pointpernew_vs = [<?php echo implode(',',$arrpoint_per_new_vs); ?>];
    var pointperproducts_vs = [<?php echo implode(',',$arrpoint_per_products_vs); ?>];
    var pointperabs_vs = [<?php echo implode(',',$arrpoint_per_abs_vs); ?>];

    var pointvalue_week_vs = [<?php echo implode(',',$arrpoint_week_vs) ?>];
    var pointday_week_vs = [<?php echo implode(',',$total_day_week_vs); ?>];
    var showday_week_vs = {<?php echo implode(',',$showday_week_vs); ?>};

    var pointvalue_month_vs = [<?php echo implode(',',$arrpoint_month_vs) ?>];
    var pointday_month_vs = [<?php echo implode(',',$total_day_month_vs); ?>];
    var showday_month_vs = {<?php echo implode(',',$showday_month_vs); ?>};
	

    $(function () {
        var d1 = [];
        var d1_vs = [];
        var d2 = [];
        var d2_vs = [];
        var d3 = [];
        var d3_vs = [];
        var d4 = [];
        var d4_vs = [];
        var d5 = [];
        var d5_vs = [];
        var d6 = [];
        var d6_vs = [];
        var d1_week = [];
        var d1_week_vs = [];
        var d1_month = [];
        var d1_month_vs = [];
        for (var i = 0; i < <?php echo count($arrpoint) ?>; i++) {
            d1.push([pointday[i], pointvalue[i]]);
            d1_vs.push([pointday[i], pointvalue_vs[i]]);
            d2.push([pointday[i], pointperorder[i]]);
            d2_vs.push([pointday[i], pointperorder_vs[i]]);
            d3.push([pointday[i], pointpercustomer[i]]);
            d3_vs.push([pointday[i], pointpercustomer_vs[i]]);
            d4.push([pointday[i], pointpernew[i]]);
            d4_vs.push([pointday[i], pointpernew_vs[i]]);
            d5.push([pointday[i], pointperproducts[i]]);
            d5_vs.push([pointday[i], pointperproducts_vs[i]]);
            d6.push([pointday[i], pointperabs[i]]);
            d6_vs.push([pointday[i], pointperabs_vs[i]]);
        }

        for (var i = 0; i < <?php echo $reset ?>; i++) {
            d1_week.push([pointday_week[i], pointvalue_week[i]]);
            d1_week_vs.push([pointday_week[i], pointvalue_week_vs[i]]);
        }

        for (var i = 0; i < <?php echo $reset_month ?>; i++) {
            d1_month.push([pointday_month[i], pointvalue_month[i]]);
            d1_month_vs.push([pointday_month[i], pointvalue_month_vs[i]]);
        }

        var chartMinDate = d1[0][0];
        var chartMaxDate = d1[<?php echo count($arrpoint) ?>-1][0];
        var chartMinDate_week = d1_week[0][0];
        var chartMaxDate_week = d1_week[<?php echo $reset ?>-1][0];
        var chartMinDate_month = d1_month[0][0];
        var chartMaxDate_month = d1_month[<?php echo $reset_month ?>-1][0];

        var tickSize = [<?php echo $tickday ?>];
        var tickSize_week = [<?php echo $tickweek ?>];
        var tickSize_month = [<?php echo $tickmonth ?>];
        var tformat = "%d/%m/%y";

        var options = {
        	grid: {
                show: true,aboveData: true,color: "#3f3f3f",labelMargin: 10,axisMargin: 0,borderWidth: 0,borderColor: null,minBorderMargin: 5,clickable: true,hoverable: true,autoHighlight: true,mouseActiveRadius: 20
            },
            series: {
                lines: {
                	show: true,fill: true,lineWidth: 2,steps: false
                },
                points: {
                    show: true,radius: 2.5,symbol: "circle",lineWidth: 2.0
                }
            },
            legend: {
                position: "ne",margin: [0, -25],noColumns: 0,labelBoxBorderColor: null,labelFormatter: function (label, series) {return label + '&nbsp;&nbsp;';},width: 40,height: 1
            },
            colors: chartColours,shadowSize: 0,tooltip: true,
            tooltipOpts: {
                content: "%s: %y.0",xDateFormat: "%d/%m",shifts: {x: -30,y: -50},defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",minTickSize: tickSize,timeformat: tformat,min: chartMinDate,max: chartMaxDate
            }
        };

        var options_week = {
        	grid: {
                show: true,aboveData: true,color: "#3f3f3f",labelMargin: 10,axisMargin: 0,borderWidth: 0,borderColor: null,minBorderMargin: 5,clickable: true,hoverable: true,autoHighlight: true,mouseActiveRadius: 20
            },
            series: {
                lines: {
                	show: true,fill: true,lineWidth: 2,steps: false
                },
                points: {
                    show: true,radius: 3.5,symbol: "circle",lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",margin: [0, -25],noColumns: 0,labelBoxBorderColor: null,labelFormatter: function (label, series) {return label + '&nbsp;&nbsp;';},width: 40,height: 1
            },
            colors: chartColours,shadowSize: 0,tooltip: true,
            tooltipOpts: {
                content: "%s: %y.0",xDateFormat: "%d/%m",shifts: {x: -30,y: -50},defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",minTickSize: tickSize_week,timeformat: tformat,min: chartMinDate_week,max: chartMaxDate_week
            }
        };

        var options_month = {
        	grid: {
                show: true,aboveData: true,color: "#3f3f3f",labelMargin: 10,axisMargin: 0,borderWidth: 0,borderColor: null,minBorderMargin: 5,clickable: true,hoverable: true,autoHighlight: true,mouseActiveRadius: 20
            },
            series: {
                lines: {
                	show: true,fill: true,lineWidth: 2,steps: false
                },
                points: {
                    show: true,radius: 3.5,symbol: "circle",lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",margin: [0, -25],noColumns: 0,labelBoxBorderColor: null,labelFormatter: function (label, series) {return label + '&nbsp;&nbsp;';},width: 40,height: 1
            },
            colors: chartColours,shadowSize: 0,tooltip: true,
            tooltipOpts: {
                content: "%s: %y.0",xDateFormat: "%d/%m",shifts: {x: -30,y: -50},defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",minTickSize: tickSize_month,timeformat: tformat,min: chartMinDate_month,max: chartMaxDate_month
            }
        };
        var plot = $.plot($("#plotchart_primary"), 
        	[
        		{label: "Doanh số",data: d1,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"},type:"showday"}
        	], options);
        var plot = $.plot($("#plotchart_primary_week"), [{label: "Doanh số",data: d1_week,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], options_week);
        var plot = $.plot($("#plotchart_primary_month"), [{label: "Doanh số",data: d1_month,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], options_month);
		
		var optionsitem = {
            grid: {
                show: false
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 1,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 1,
                    symbol: "circle",
                    lineWidth: 1
                }
            },
            legend: {
                position: "ne",
                margin: [0, -25],
                noColumns: 0,
                labelBoxBorderColor: null,
                labelFormatter: function (label, series) {
                    return label + '&nbsp;&nbsp;';
                },
                width: 40,
                height: 1
            },
            colors: chartColours,
            shadowSize: 0,
            tooltip: false,
            tooltipOpts: {
                content: "%s: %y.0",
                xDateFormat: "%d/%m",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",
                minTickSize: tickSize,
                timeformat: tformat,
                min: chartMinDate,
                max: chartMaxDate
            }
        };

        var plot1 = $.plot($("#plotchart_item1"), [{data: d2,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
        var plot2 = $.plot($("#plotchart_item2"), [{data: d3,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
        var plot3 = $.plot($("#plotchart_item3"), [{data: d1,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
        var plot4 = $.plot($("#plotchart_item4"), [{data: d4,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
        var plot5 = $.plot($("#plotchart_item5"), [{data: d5,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
        var plot6 = $.plot($("#plotchart_item6"), [{data: d6,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
    });
	
	$("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "5px 10px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");

	$("#plotchart_primary").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0],
				y = item.datapoint[1].toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			var width = window.innerWidth;
			if(width-item.pageX<250){
				item.pageX = item.pageX-250;
			}
			var type = item.series.type;
			if(type=="showday"){
				$("#tooltip").html("<b>Ngày " + showday[x] + "</b><br>"+item.series.label +" đạt được "+  y +" đ ")
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(300);
			}
			if(type=="showday_vs"){
				$("#tooltip").html("<b>Ngày " + showday_vs[x] + "</b><br>"+item.series.label +" đạt được "+  y +" đ ")
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(300);
			}
		} else {
			$("#tooltip").hide();
		}
	});

	$("#plotchart_primary_week").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0],
				y = item.datapoint[1].toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			var width = window.innerWidth;
			if(width-item.pageX<250){
				item.pageX = item.pageX-250;
			}
			$("#tooltip").html("<b>Ngày " + showday_week[x] + "</b><br>"+item.series.label +" đạt được "+  y +" đ")
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(300);
		} else {
			$("#tooltip").hide();
		}
	});

	$("#plotchart_primary_month").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0],
				y = item.datapoint[1].toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			var width = window.innerWidth;
			if(width-item.pageX<250){
				item.pageX = item.pageX-250;
			}
			$("#tooltip").html("<b>" + showday_month[x] + "</b><br>"+item.series.label +" đạt được "+  y +" đ")
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(300);
		} else {
			$("#tooltip").hide();
		}
	});

	
</script>
<!-- /flot -->
<!-- /pie chart-->
<script>
	var sharePiePolorDoughnutData = [
            {
                value: <?php echo $total_success_Order ?>,
                color: "#29e",
                highlight: "#27c",
                label: "Số lượng đơn hàng thành công"
        },
            {
                value: <?php echo $total_false_Order ?>,
                color: "#F00",
                highlight: "#F00",
                label: "Số lượng đơn hàng bị hủy"
        }
    ];

        $(document).ready(function () {
            window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(sharePiePolorDoughnutData, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)"
            });
        });
</script>
<!-- ajax compare -->
<script>
	var baselink = $("#baselink_report").val();
	$("#vs_sosanh").change(function(){
		var data = $(this).val();
		var kpi = $("#value_sosanh").val();
		var status = $("#orderstatus").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+data+"&KPI="+kpi+"&Orderstatus="+status,
            success: function(result){
                window.location=result;
            }
        });
	});

	$("#orderstatus").change(function(){
		var data = $(this).val();
		var kpi = $("#value_sosanh").val();
		var targets = $("#vs_sosanh").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+targets+"&KPI="+kpi+"&Orderstatus="+data,
            success: function(result){
                window.location=result;
            }
        });
	});

	$("#value_sosanh").change(function(){
		var data = $(this).val();
		var status = $("#orderstatus").val();
		var targets = $("#vs_sosanh").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+targets+"&KPI="+data+"&Orderstatus="+status,
            success: function(result){
                window.location=result;
            }
        });
	});
</script>