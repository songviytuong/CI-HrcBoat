<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Chỉ tiêu kinh doanh</h1>
	    </div>
    </div>
    <div class="header_targets">
    	<div class="block1">
    		<p>Chọn bộ phận:</p>
    		<select id="department">
    			<option value='0'> Tất cả phòng ban </option>
    			<?php 
    			$department = $this->db->query("select * from ttp_report_targets_department")->result();
    			if(count($department)>0){
    				foreach($department as $row){
    					$selected = $current_department == $row->ID ? "selected='selected'" : '' ;
    					echo "<option value='$row->ID' $selected>$row->Title</option>";
    				}
    			}
    			?>
    		</select>
    		<p>Năm tài chính:</p>
    		<select id="namtaichinh">
    			<?php 
    			$year = $this->db->query("select Year from ttp_report_targets_year order by Year ASC")->result();
    			if(count($year)>0){
    				foreach($year as $row){
    					$selected = $row->Year==$current_year ? "selected='selected'" : '' ;
	    				echo "<option value='$row->Year' $selected>$row->Year</option>";
    				}
    			}else{
	    			$year = date('Y',time());
	    			for ($i=2014; $i < $year+1; $i++) { 
	    				$selected = $i==$current_year ? "selected='selected'" : '' ;
	    				echo "<option value='$i' $selected>$i</option>";
	    			}
    			}
    			?>
    		</select>
    		<a class="btn btn-default" onclick="addyear(this)"><i class="fa fa-plus"></i></a>
    	</div>
    	<?php 
    	$current_tab = $this->uri->segment(4);
    	?>
    	<div class="block2">
    		<a href="<?php echo $base_link; ?>" <?php echo $current_tab=='' ? 'class="current"' : '' ; ?>>Theo năm</a>
    		<a href="<?php echo $base_link."bymonth"; ?>" <?php echo $current_tab=='bymonth' ? 'class="current"' : '' ; ?>>Theo tháng</a>
    		<a href="<?php echo $base_link."byteam"; ?>" <?php echo $current_tab=='byteam' ? 'class="current"' : '' ; ?>>Theo nhóm trực thuộc</a>
    		<a href="<?php echo $base_link."byperson"; ?>" <?php echo $current_tab=='byperson' ? 'class="current"' : '' ; ?>>Theo cá nhân từng nhóm</a>
    	</div>
    </div>
    <div class="content_targets">
    	<?php 
    	if($current_department!=0){
    	?>
    	<div class="team_content">
    		<form action="<?php echo $base_link.'save_chitieu_team' ?>" method="post">
    		<div class="block1">
    			<p>Chỉ tiêu đã phân bổ cho từng nhóm</p>
    			<select id="select_team">
    				<option value='0'>-- Chọn Nhóm --</option>
    				<?php 
    				$team = $this->db->query("select * from ttp_report_team where Department=$current_department and Published=1")->result();
    				if(count($team)>0){
    					foreach($team as $row){
    						$selected = $row->ID==$current_team ? "selected='selected'" : '' ;
    						echo "<option value='$row->ID' $selected>$row->Title</option>";
    					}
    				}
    				?>
    			</select>
    			<select id="select_targets">
    				<option value='0'>-- Chọn chỉ tiêu --</option>
    				<?php 
    				$targets = $this->db->query("select * from ttp_report_targets_type")->result();
    				if(count($targets)>0){
    					foreach($targets as $row){
    						$selected = $row->ID==$current_targets ? "selected='selected'" : '' ;
    						echo "<option value='$row->ID' $selected>$row->Title</option>";
    					}
    				}
    				?>
    			</select>
    		</div>
    		<div class="block2">
    			<a class="btn btn-primary" id="add_chitieu"><i class="fa fa-plus"></i> Nhóm phân bổ</a>
    			<button class="btn btn-primary" id="save_chiteu" type="submit">Lưu thay đổi</button>
    		</div>
    		<div class="block3">
    			<table>
    				<?php
					echo "<tr><th>Nhóm</th><th>Chỉ tiêu</th>"; 
					for ($i=1; $i < 13; $i++) { 
						echo "<th>$i/$current_year</th>";
					}
					echo "</tr>";
					if(count($team)>0){
						$result = $this->db->query("select * from ttp_report_targets_type")->result();
						if(count($result)>0){
							$arr_team = array();
							foreach($team as $row){
								$display = $current_team==$row->ID || $current_team==0 ? "" : "display:none" ;
								echo "<tr style='$display'><td colspan='14' class='colspan'>$row->Title</td></tr>";
								foreach($result as $item){
									$type_display = $current_targets==$item->ID || $current_targets==0 ? "" : "display:none" ;
									$type_display = $display!='' ? $display : $type_display ;
									echo "<tr style='$type_display'>";
									echo "<td colspan='2'>$item->Title</td>";
									for ($i=1; $i < 13; $i++) { 
										$total_item = isset($datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Team'][$row->ID]['Total']) ? $datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Team'][$row->ID]['Total'] : 0 ;
										echo "<td>
												<input type='number' min='0' name='year[type$item->ID][Department][$current_department][Month][$i][Team][$row->ID][Total]' class='department_{$item->ID}_{$i}' data-team='$item->ID' data-month='$i' onchange='recal(this)' value='$total_item' />
												<a class='show_numberofrow' onclick='nhapdulieu(this)'>".number_format($total_item)."</a>
											</td>";
									}
									echo "</tr>";
								}
								$arr_team[] = $row->ID;
							}
							$chitieuchuaphanbo = "";
							echo "<tr><td class='colspan green' colspan='14'>Tổng chỉ tiêu đã phân bổ</td></tr>";
							foreach($result as $item){
								$type_display = $current_targets==$item->ID || $current_targets==0 ? "" : "display:none" ;
								echo "<tr style='$type_display'>";
								echo "<td colspan='2'>$item->Title</td>";
								$chitieuchuaphanbo .= "<tr style='$type_display'><td colspan='2'>$item->Title</td>";
								for ($i=1; $i < 13; $i++) { 
									$total_item = isset($datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Total']) ? $datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Total'] : 0 ;
									$remain_item = isset($datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Remain']) ? $datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Remain'] : $total_item ;
		    						if(isset($datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Remain'])){
		    							$apply = $total_item - $remain_item;	
		    						}else{
		    							$apply = 0;
		    						}
									echo "<td>
											<input type='number' min='0' class='total_{$item->ID}_{$i}' onchange='recal(this)' value='$total_item' />
											<a class='show_numberofrow total_phanbo_{$item->ID}_{$i}'>".number_format($apply)."</a>
										</td>";
									$color = $remain_item<0 ? "class='red'" : '' ;
									$chitieuchuaphanbo .= "<td $color>
											<input type='hidden' min='0' name='year[type$item->ID][Department][$current_department][Month][$i][Remain]' class='remain_{$item->ID}_$i' value='$remain_item' />
											<a class='show_numberofrow  total_remain_{$item->ID}_{$i}'>".number_format($remain_item)."</a>
										</td>";
								}
								$chitieuchuaphanbo .= "</tr>";
								echo "</tr>";
							}
							echo "<tr><td class='colspan yellow' colspan='14'>Tổng chỉ tiêu chưa phân bổ</td></tr>";
							echo $chitieuchuaphanbo;
						}
					}
					?>
    			</table>
    		</div>
    	</div>
    	<?php 
    	}else{
    		echo "<p style='padding:20px 0px;text-align:left'>Vui lòng chọn phòng ban</p>";
    	}
    	?>
    </div>
    <div class="over_lay black">
    	<div class="box_inner" style="width:500px">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner">
    			<div class="row">Tên nhóm</div>
    			<div class="row"><input type="text" id="Title_chitieu" /></div>
    			<div class="row">Thuộc phòng ban</div>
    			<div class="row">
    				<select id="DepartmentID" class="form-control">
    				<?php 
					$depart = $this->db->query("select * from ttp_report_targets_department")->result();
					if(count($depart)>0){
						foreach($depart as $row){
							echo "<option value='$row->ID'>$row->Title</option>";
						}
					}
					?>
					</select>
    			</div>
    			<div class="row">Leader</div>
    			<div class="row">
    				<select id="UserID" class="form-control">
					<?php 
					$depart = $this->db->query("select ID,UserName from ttp_user where UserType=1")->result();
					if(count($depart)>0){
						foreach($depart as $row){
							echo "<option value='$row->ID'>$row->UserName</option>";
						}
					}
					?>
					</select>
    			</div>
    			<div class="row">
					<p class="title">Chọn nhân viên thuộc team này</p>
				</div>
				<div class="row" style="width:100%">
					<ul>
					<?php 
						if(count($depart)>0){
							foreach($depart as $row){
								echo "<li class='lilist'><input class='checkboxuser' type='checkbox' value='$row->ID' /> <span>$row->UserName</span></li>";
							}
						}
					?>
					</ul>
				</div>
    			<div class="row"><a class="btn btn-default" onclick="addchitieu(this)">Save</a></div>
    		</div>
    	</div>
    </div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
	var link = $("#baselink_report").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
	});

	$("#add_chitieu").click(function(){
		$(".over_lay .box_inner .block1_inner h1").html("Thêm nhóm mới");
		$(".over_lay").show();
	});

	$("#department").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?department="+data;
	});

	$("#namtaichinh").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?year="+data;
	});

	$("#select_team").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?team="+data;
	});

	$("#select_targets").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?targets="+data;
	});

	function addchitieu(ob){
		$(ob).addClass('saving');
		var title = $("#Title_chitieu").val();
		var department = $("#DepartmentID").val();
		var user = $("#UserID").val();
		var userinteam = "0";
		$(".checkboxuser").each(function(){
			if($(this).prop("checked")==true){
				userinteam = userinteam+","+$(this).val();
			}
		});
		if(title!='' && department!='' && user!=''){
			$.ajax({
            	url: link+"import_targets/add_team",
	            dataType: "html",
	            type: "POST",
	            data: "Title="+title+"&Department="+department+"&User="+user+"&Listuser="+userinteam,
	            success: function(result){
	                if(result=='true'){
	                	location.reload();
	                }else{
	                	alert("Yêu cầu không được thực hiện . Vui lòng kiểm tra lại kết nối mạng .");
	                }
	                $(ob).removeClass('saving');
	            }
	        });
		}
	}

	function addyear(ob){
		$(ob).addClass('saving');
		$.ajax({
        	url: link+"import_targets/add_year",
            dataType: "html",
            type: "POST",
            data: "Year=ok",
            success: function(result){
                alert("Đã thêm năm tài chính tiếp theo .");
                location.reload();
            }
        });
        $(ob).removeClass('saving');
	}

	function nhapdulieu(ob){
		$(ob).parent().find("input").css({"display":"block"});
		$(ob).parent().find("input").focus();
		$(ob).hide();
	}

	function recal(ob){
		money = parseInt($(ob).val());
		var datamonth = $(ob).attr("data-month");
		var datateam = $(ob).attr("data-team");
		total = parseInt($(".total_"+datateam+"_"+datamonth).val());
		remain=0;
		$(".department_"+datateam+"_"+datamonth).each(function(){
			remain = remain + parseInt($(this).val());
		});
		$(".total_phanbo_"+datateam+"_"+datamonth).html(remain.format('a',3));
		remain = total-remain;
		$(ob).parent().find("a.show_numberofrow").html(money.format('a',3));
		$(ob).parent().find("a.show_numberofrow").show();
		$(ob).hide();
		$(".total_remain_"+datateam+"_"+datamonth).parent().removeClass("red");
		$(".total_remain_"+datateam+"_"+datamonth).parent().removeClass("green");
		if(remain==0){
			$(".total_remain_"+datateam+"_"+datamonth).html(0);
		}
		if(remain<0){
			temp = remain*-1;
			alert("!!! Cảnh báo : vượt chỉ tiêu cho phép của tháng "+temp.format('a',3)+".");
			$(".total_remain_"+datateam+"_"+datamonth).parent().addClass("red");
			$(".total_remain_"+datateam+"_"+datamonth).html(remain.format('a',3));
		}
		if(remain>0){
			$(".total_remain_"+datateam+"_"+datamonth).html(remain.format('a',3));
		}
		$(".remain_"+datateam+"_"+datamonth).val(remain);
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

</script>