<?php 
$result_fillter = array();
$fill_temp_value="";
?>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>DỮ LIỆU NHẬP SỐ DƯ ĐẦU KỲ</h1>
	    </div>
	    <div class="block2">
	    </div>
    </div>
    <div class="import_orderlist">
    	<div class="block2">
    		<div class="block_2_1">
                <a class="btn btn-primary" href="<?php echo $base_link.'add' ?>"><i class="fa fa-plus"></i> Nhập liệu</a>
    			<div class='warehouse_fillter'>
                    <select id='WarehouseID' onchange="change_fillter()">
                        <option value=''>-- Tại tất cả kho --</option>
                        <?php 
                        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
                        $warehouse_id = $this->session->userdata('WarehouseID');
                        if(count($warehouse)>0){
                            foreach($warehouse as $row){
                                $selected = $row->ID==$warehouse_id ? "selected='selected'" : '' ;
                                echo "<option value='$row->ID' $selected>$row->MaKho</option>";
                            }
                        }
                        $checkedday = $this->session->userdata('CheckedDay');
                        ?>
                    </select>
                    <input type='text' id="CheckedDay" placeholder='Chọn ngày kiểm kho' onchange="change_fillter()" value="<?php echo $checkedday ?>" />
                </div>
    		</div>
    		<div class="block_2_2">
    			<button onclick="showtools()">Advanced Filter <b class="caret"></b></button>
                <a class="btn btn-primary" role="button" onclick="export_data(this)"><i class="fa fa-download"></i> Export</a>
    		</div>
    	</div>
        <div class="clear"></div>
        <div class="filltertools">
            <form action="<?php echo $base_link."setfillter" ?>" method="post">
                <?php 
                    $arr_fieldname = array(0=>"b.MaSP",1=>"c.ShipmentCode",2=>"a.WarehouseID",3=>"a.CheckedDay");
                    $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                    $arr_showfieldname = array(0=>"Mã sản phẩm",1=>"Số lô",2=>"Mã kho",3=>"Ngày kiểm kho");
                    $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                    $fill_data_arr = explode(" and ",$fill_data);
                    if(count($fill_data_arr)>0 && $fill_data!=''){
                        $temp_tools=0;
                        foreach($fill_data_arr as $row){
                            $param = explode(' ',$row,3);
                            $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                            $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                            $param_oparation=isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                            $param_value = isset($param[2]) ? $param[2] : '' ;
                            $param_value = str_replace("\'","",$param_value);
                            $param_value = str_replace("'","",$param_value);
                            $param_value = str_replace("%","",$param_value);
                ?>
                <div class="row <?php echo $temp_tools==0 ? "base_row" : "" ; ?>">
                    <div class="list_toolls"><p class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></p></div>
                    <div class="list_toolls">
                        <input type="hidden" class="FieldName" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                        <p class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></p>
                        <ul class="dropdownbox">
                            <li><a onclick="setfield(this,0,'masp')">Mã sản phẩm</a></li>
                            <li><a onclick="setfield(this,1,'solo')">Số lô</a></li>
                            <li><a onclick="setfield(this,2,'makho')">Mã kho</a></li>
                            <li><a onclick="setfield(this,3,'ngaykiemkho')">Ngày kiểm kho</a></li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation">
                        <select class="oparation" name="FieldOparation[]">
                            <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                            <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                            <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                            <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                            <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                            <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                            <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter">
                        <?php 
                        if($value_field==2){
                            $result = $this->db->query("select MaKho,ID from ttp_report_warehouse order by MaKho ASC")->result();    
                            if(count($result)>0){
                                echo "<select name='FieldText[]'>";
                                foreach($result as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->MaKho : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->MaKho</option>";
                                }
                                echo "</select>";
                            }
                        }else{
                            $fill_temp_value = $param_value ;
                            echo '<input type="text" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                        }
                        ?>
                    </div>
                    <a class='remove_row_x' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
                <?php   
                        $temp_tools++;
                        $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                        $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                        }
                    }else{
                ?>
                    <div class="row base_row">
                        <div class="list_toolls"><p class="title first-title">Chọn</p></div>
                        <div class="list_toolls">
                            <input type="hidden" class="FieldName" name="FieldName[]" />
                            <p class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></p>
                            <ul class="dropdownbox">
                                <li><a onclick="setfield(this,0,'masp')">Mã sản phẩm</a></li>
                                <li><a onclick="setfield(this,1,'solo')">Số lô</a></li>
                                <li><a onclick="setfield(this,2,'makho')">Mã kho</a></li>
                                <li><a onclick="setfield(this,3,'ngaykiemkho')">Ngày kiểm kho</a></li>
                            </ul>
                        </div>
                        <div class="list_toolls reciveroparation">
                            <select class="oparation" name="FieldOparation[]">
                                <option value="1">Bằng</option>
                                <option value="0">Có chứa</option>
                                <option value="2">Khác</option>
                                <option value="3">Lớn hơn</option>
                                <option value="4">Nhỏ hơn</option>
                                <option value="5">Lớn hơn hoặc bằng</option>
                                <option value="6">Nhỏ hơn hoặc bằng</option>
                            </select>
                        </div>
                        <div class="list_toolls reciverfillter">
                            <input type="text" name="FieldText[]" id="textsearch" />
                        </div>
                        <a class='remove_row_x' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                    </div>
                <?php
                    }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <a class="btn" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                    <button class="btn" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                </div>
            </form>
        </div>
    	<div class="block3">
            <?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            ?>
    		<table id="table_data">
    			<tr>
    				<th rowspan='2'>STT</th>
                    <th rowspan='2'>Mã SP</th>
                    <th rowspan='2'>Tên sản phẩm</th>
    				<th rowspan='2'>ĐVT</th>
    				<th rowspan='2'>Số lô</th>
    				<th rowspan='2'>SL tồn<br> đầu kỳ</th>
    				<th colspan='4'>Vị trí trong kho</th>
    			</tr>
                <tr>
                    <th>Khu vực</th>
                    <th>Tên kệ</th>
                    <th>Tên cột</th>
                    <th>Tên hàng</th>
                </tr>
    			<?php 
                $i=$start+1;
                if(count($data)>0){
    				foreach($data as $row){
                        echo "<tr>";
                        echo "<td style='text-align:center'>$i</td>";
                        echo "<td>$row->MaSP</td>";
                        echo "<td>$row->TenSP</td>";
                        echo "<td style='width:50px'>$row->Donvi</td>";
                        echo "<td style='width:100px'>$row->Shipment</td>";
                        echo "<td style='width:100px;text-align:right'>".number_format($row->Amount)."</td>";
                        echo "<td style='width:100px'>$row->Position</td>";
                        echo "<td style='width:100px'>$row->Rack</td>";
                        echo "<td style='width:100px'>$row->Col</td>";
                        echo "<td style='width:100px'>$row->Row</td>";
                        echo "</tr>";
                        $i++;
    				}
    			}else{
                    echo "<tr><td colspan='10'>Không tìm thấy đơn hàng.</td></tr>";
    			}
    			?>
    		</table>
            <?php 
                echo $nav;
            ?>
    	</div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Export_date,#CheckedDay').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });

        $('#CheckedDay').on('apply.daterangepicker', function(ev, picker) {
            change_fillter();
        });
    });

	$("#show_thaotac").click(function(event){
        event.stopPropagation();
		$(this).parent('li').find('ul').toggle();
	});

    $('html').click(function() {
        $("#mydiv_thaotac").hide();
        $(".dropdownbox").hide();
    });
	
    function checkfull(ob){
        if(ob.checked===true){
            $("#table_data .selected_order").prop('checked', true);
        }else{
            $("#table_data .selected_order").prop('checked', false);
        }
    }

    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("p.second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"/warehouse_inventory_import/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="makho"){
            return '<select class="oparation" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            return '<select class="oparation" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
        }
    }

    function export_data(ob){
        $(".export_tools_kt").toggle();
    }

    $("#falseclass").submit(function(){
        $(this).find('button').removeClass("saving");
    });

    function change_type(ob){
        var type = $(ob).val();
        var baselink = $("#baselink_report").val();
        window.location=baselink+"/warehouse_inventory_import/set_import_type?Type="+type;
    }

    function change_fillter(){
        var WarehouseID = $("#WarehouseID").val();
        var CheckedDay = $("#CheckedDay").val();
        var baselink = $("#baselink_report").val();
        window.location=baselink+"/warehouse_inventory_opening_balance/set_bonus_fillter/?WarehouseID="+WarehouseID+"&CheckedDay="+CheckedDay;
    }

</script>
<style>
    .body_content .containner .import_orderlist .export_tools_kt:before{right:177px;}
</style>
