<div class="containner">
    <div class="import_select_progress">
        <div class="block1">

        </div>
        <div class="block2">
            <!--button class="btn btn-default btn-close"><i class="fa fa-times" aria-hidden="true"></i> Đóng</button>-->
            <div class="btn-group">
                <button type="button" class="btn btn-warning" data-toggle="dropdown">
                    <i class="fa fa-save" aria-hidden="true"></i> Cập nhật</button>
            </div>
        </div>
    </div>
    <form name="frm" role="form" data-toggle="validator" id="frm" method="post">
        <div class="row">
            <div class="col-lg-6">
                <label for="promo_name">Tên Promotion:</label>
                <input type="text" name="promo_name" id="promo_name" class="form-control" value="<?=$data->promo_name?>" placeholder="Nhập tên Promotion..."/>
                <label for="promo_desc">Mô tả chiến dịch:</label>
                <input type="text" name="promo_desc" id="promo_desc" class="form-control" value="<?=$data->promo_description?>"/>
            </div>
            <div class="col-lg-6">
                <label for="promo_start">Bắt đầu chiến dịch:</label>
                <div class="form-group">
                    <div class='input-group promo_start'>
                        <input type='text' name="promo_start" placeholder="2016-01-01 00:00:00" id='promo_start' value="<?=$data->promo_start?>" class="form-control" />
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
                <label for="promo_end">Kết thúc chiến dịch:</label>
                <div class="form-group">
                    <div class='input-group promo_end'>
                        <input type='text' name="promo_end" placeholder="2016-01-02 00:00:00" id='promo_end' value="<?=$data->promo_end?>" class="form-control" />
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>    
            </div>
        </div>
        <div class="row">
            <style>
                .products_containner {padding:5px;
                    background-color: #fff;
                    background-image: none;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                }
                .content_products {margin-top:0px !important;
                
                }
                .containner .products_containner .content_products .content_tab6 ul li span {
                    color:#000 !important;
                    margin-left:5px;
                }
                .ms-parent {position:absolute; margin-top:10px; width:95%;}
                .ms-drop {margin-top:4px;}
                .open {margin-top:5px;}
            </style>
            <div class="col-lg-6 hiddenVIP">
                <div class="products_containner">
                    <div class="content_products">
                        <div class="content_tab6">
                        <?php 
                            $categories = $this->db->query("select Title,ID from ttp_report_categories where ParentID=0 and TypeCat=0")->result();
                            if(count($categories)>0){
                                    echo "<ul>";
                                    foreach($categories as $row){
                                        echo "<li><span onclick='show_child_next(this)' data='$row->ID' class='label label-default'>+</span><input type='checkbox' class='par' onchange='check_full(this)' value='$row->ID' name='CategoriesID[]' /> $row->Title</li>";
                                    }
                                    echo "</ul>";
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 hiddenVIP">
                <label for="promo_start">Chọn sản phẩm:</label>
                <select id="Product" name="Product[]" multiple="multiple" class="form-control" style="height:235px;">

                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <label for="min_quantity" class="hiddenVIP"><input type="radio" name="min" id="min_quantity" value="min_quantity"/></label>
                <input class="input-sm hiddenVIP" type="number" min="0" name="min_quantity" id="_quantity" placeholder="Vượt số lượng..." value="<?=$min_quantity;?>" style="width:149px"/>
                <label for="min_value" class="hiddenVIP"><input type="radio" name="min" id="min_value" value="min_value"/></label>
                <input class="input-sm hiddenVIP" type="number" min="0" name="min_value" id="_value" placeholder="Vượt giá trị..." value="<?=$min_value;?>"/>
                <input class="input-sm" type="number" min="0" name="discount" id="_discount" placeholder="Chiết khấu" value="<?=$discount;?>"/>
            </div>
            <div class="col-lg-6">
                <select class="input-sm option_type" name="option_type">
                    <?php
                        foreach($result_payment as $key=>$row){
                    ?>
                    <option value="<?=$row->code;?>" <?=($row->code == $discount_type) ? 'selected' : ''?>><?=$row->name;?></option>
                    <?php } ?>
                </select>
                <div id="sGift" style="display: none;">
                    <select id="Gift" name="Gift[]" multiple="multiple"></select>
                </div>
            </div>
        </div>
        
        <div class="row VIP">
            <div class="col-lg-6">
                <div class="block1 table_data">
                    <label for="promo_customer">Danh sách khách hàng:</label>
                    <div class="text-right" style="position: absolute; top:0px; right:16px;">
                        <input type="text" id="autoCustomer" name="autoCustomer" class="form-control" placeholder="Nhập tên hoặc SĐT..." style="width:200px;"/>
                        
                    </div>
                    
                    <table id="table_data">
                        <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th class="text-center col-xs-6">Họ tên</th>
                                <th class="text-center">Điện thoại</th>
                                <th class="text-center col-xs-1">Xóa</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div style="position: absolute; left:-8px;" class="close hidden"><i class="fa fa-times-circle"></i></div>
                <div id="suggesstion-box"></div>
            </div>
        </div>
        <style>
            #customer-list li:hover {
              background: #eee;
              cursor: pointer;
            }
            .badge{
                font-weight:normal  !important;
            }
        </style>
        
        <div class="row hiddenVIP">
            <div class="col-lg-6">
                <select id="Area" name="Area[]" multiple="multiple" style="height:120px;">
                    <?php
                    foreach ($area as $key=>$row) {
                        if ($row->Title == "Undefine") {
                            continue;
                        }
                        ?>
                        <option value="<?= $row->ID; ?>" <?=(isset($area_active[$key]) == $row->ID) ? "selected":''?>><?= $row->Title; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-6">
                <label for="promo_city">Chọn tỉnh thành:</label>
                <select id="City" name="City[]" multiple="multiple" class="form-control" style="height:225px;"></select>
            </div>
        </div>

        <div class="row hiddenVIP">
            <div class="col-lg-6">
                <label for="promo_customer">Chọn đối tượng khách hàng:</label>
                <?php
                foreach ($allow as $key=>$row) {
                    ?>
                    <div class="checkbox">
                        <label><input type="checkbox" name="allow[]" <?=($row->code == !empty($allow_active[$key])) ? 'checked':'';?> value="<?= $row->code ?>" <?= ($row->promotion_active == 2) ? "disabled" : ""; ?>><?= $row->description ?></label>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-6">
                
                
            </div>
        </div>
        <div class="import_select_progress hiddenVIP">
            <div class="block1">

            </div>
            <div class="block2">

                <div class="block2">
                    <button class="btn btn-default btn-close"><i class="fa fa-times" aria-hidden="true"></i> Đóng</button>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning" data-toggle="dropdown">
                            <i class="fa fa-save" aria-hidden="true"></i> Cập nhật</button>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <input type="hidden" name="promo_type" id="promo_type" value="<?=$data->promo_type?>"/>
    </form>
</div>
<style>
    .daterangepicker{width: auto;}
</style>

<script type="text/javascript">
    $('.close').click(function(){
        $(this).hide();
        $("#suggesstion-box").hide();
    });
    function selectCustomer(val) {
        //append to table
        $.ajax({
            url: "<?= $base_link; ?>AutoCompleteData",
            dataType: "html",
            type: "POST",
            data: "customer_id=" + val,
            success: function (result) {
                $('#table_data tbody').append(result);
                $('.item-'+val).removeAttr("onclick");
            }
        });
//        $("#suggesstion-box").hide();
        $("#autoCustomer").val("");
    }
    function del(id){
        $('#table_data tr').each(function(){
            $('.row_'+id).remove();
        });
    }
    
    $(document).on("keyup keypress blur change", "#autoCustomer", function(){
        $.ajax({
            type: "POST",
            url: "<?= $base_link; ?>AutoCompleteCustomer",
            data:'keyword='+$(this).val(),
            beforeSend: function(){
                $('.close').removeClass("hidden");
                $("#autoCustomer").css("background","#FFF url(public/admin/images/loading.gif) no-repeat 165px");
            },
            success: function(data){
                if(data == ""){
                    $('.close').hide();
                    $("#suggesstion-box").hide();
                    $("#autoCustomer").css("background","#FFF");
                }else{
                    $('.close').show();
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#autoCustomer").css("background","#FFF");
                }
            }
        });
    });
    
    function check_full(ob){  
        if(ob.checked===true){
            $(ob).parent('li').find("input").prop('checked', true);
        }else{
            $(ob).parent('li').find("input").prop('checked', false);
        }
        getProduct();
    }
    function getProduct(){
        var str="0";
        $(".content_tab6 input[type='checkbox']").each(function(){
            if(this.checked===true){
                str=str+","+$(this).val();
            }
        });
        $.ajax({
            url: "<?= $base_link; ?>getProductByList",
            dataType: "html",
            type: "POST",
            data: "promo_id=''&list_id=" + str,
            success: function (result) {
                $('#Product').html(result);
            }
        });
    }
    function check_full_rowtab(ob,key){
            $("input.valuekey"+key).prop('checked', true);
    }

    function uncheck_full_rowtab(ob,key){
            $("input.valuekey"+key).prop('checked', false);
    }

    function show_child(ob){
            var current = $(ob).html();
            if(current==="+"){
                    $(ob).html("-");
            }else{
                    $(ob).html("+");
            }
            $(ob).parent('li').find("ul").toggle('fast');
    }
    var isloaddata = [];
    function show_child_next(ob){
        var ID = $(ob).attr('data');
        var promo_id = <?=$data->promo_id;?>;
        var current = $(ob).html();
                if(current==="+"){
                        $(ob).html("-");
                        if(ID!=''){
                                if(jQuery.inArray( "data"+ID, isloaddata )<0){
                                        $.ajax({
                                        url: "<?php echo base_url().ADMINPATH.'/report/promotion_campaign/get_child_categories' ?>",
                                        dataType: "html",
                                        type: "POST",
                                        data: "promo_id="+promo_id+"&ID="+ID,
                                        success: function(result){
                                                if(result!='False'){
                                                        $(ob).parent('li').append(result);
                                                        isloaddata.push("data"+ID);
                                                }else{
                                                        $(ob).html("");
                                                        $(ob).css({"background":"#FFF","border":"1px solid #FFF"});
                                                }
                                        }
                                    });	
                                }
                }
                }else{
                        $(ob).html("+");
                }
        $(ob).parent('li').find("ul").toggle("fast");
    }
    function abc(){
        
        $(".content_tab6 input[type='checkbox']").each(function(){
            if(this.checked===true){
                $(this).parent('li').find('.label').click();
                
                $(this).change();
            }
        });
    }
    $(document).ready(function () {
        
        setTimeout(function () {
           // $('#_discount').val(discount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            //Nếu là đặc biệt
            var promo_type = "<?=$promo_type;?>";
            if(promo_type == 2){
                $('.VIP').removeClass("hidden");
                $('.hiddenVIP').addClass("hidden");
                
                $('#_discount').removeClass("input-sm");
                $('#_discount').addClass("form-control pull-right");
                $('#_discount').attr("style","width:200px");

                $('.option_type').removeClass("input-sm");
                $('.option_type').addClass("form-control");
                $('.option_type').attr("style","width:200px");
            }
        });
        
        $('#promo_start,#promo_end').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1",
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        setTimeout(function () {
            $('#Area').multipleSelect({
                filter: true,
                width: '95%',
                isOpen: true,
                keepOpen: true,
                placeholder: "Chọn khu vực...",
                selectAllText: '-- All --',
                allSelected: 'Tất cả',
                maxHeight: '170',
            });

            $('#Customer').multipleSelect({
                filter: true,
                width: '100%',
                isOpen: true,
                keepOpen: true,
                placeholder: "Chọn...",
                selectAllText: '-- All --',
                allSelected: 'Tất cả',
                maxHeight: '30',
            });
            
            $.ajax({
                url: "<?= $base_link; ?>listProduct",
                dataType: "html",
                type: "GET",
                data: "promo_id=<?=$data->promo_id?>",
                success: function (result) {
                    console.log(result);
                    $('#Product').html(result);
                }
            });
            
        }, 100);

        setTimeout(function () {
            $('#min_quantity').click();
            $('#promo_name').focus();
        }, 100);

        $('.btn-close').click(function () {
            if (!confirm('Bạn thực sự muốn thoát ??')) {
                return false;
            }
            window.history.back();
        });

        $('.option_type').change(function () {
            var id = $(this).val();
            var txt = $(this).text();
            
            if (id == 3) {
                $('#sGift').css("display", "block");
                $('#_discount').attr('disabled', true);
                $.ajax({
                    url: "<?= $base_link; ?>getGift",
                    dataType: "html",
                    type: "POST",
                    data: "",
                    success: function (result) {
                        $('#Gift').html(result).multipleSelect({
                            filter: true,
                            width: '95%',
                            maxHeight: '150',
                            isOpen: true,
                            placeholder: "Chọn quà tặng...",
                            selectAllText: '-- All --',
                            allSelected: 'Tất cả'
                        });
                    }
                });
                }else if (id == 2) {
                $('#_discount').attr("placeholder","% chiết khấu");
            } else {
                $('#_discount').attr('disabled', false);
                $('#sGift').css("display", "none");
                $('#_discount').attr("placeholder","Giá trị chiết khấu");
            }
            $('#_discount').focus();
            $('#_discount').select();
        });
        
        $('#min_quantity').click(function () {
            if ($(this).val() == 'min_quantity')
            {
                $('#_quantity').attr('disabled', false);
                $('#_value').attr('disabled', true);
                $('#_quantity').focus();
            }
        });
        
        $('#min_value').click(function () {
            if ($(this).val() == 'min_value')
            {
                $('#_value').attr('disabled', false);
                $('#_quantity').attr('disabled', true);
                $('#_value').focus();
            }
        });

        $("#close_overlay").click(function () {
            $(".over_lay").removeClass('in');
            $(".over_lay").addClass('out');
            setTimeout(function () {
                $(".over_lay").hide();
                disablescrollsetup();
            }, 200);
        });

        $('#Category').change(function () {
            var cate_id = $(this).val();
            var promo_id = <?=$data->promo_id?>;
            $.ajax({
                url: "<?= $base_link; ?>getListByCate",
                dataType: "html",
                type: "POST",
                data: "promo_id="+promo_id+"&cate_id=" + cate_id,
                success: function (result) {
                    console.log(result);
                    $('#ProductList').html(result).multipleSelect({
                        filter: true,
                        width: '100%',
                        isOpen: true,
                        keepOpen: true,
                        placeholder: "Chọn...",
                        selectAllText: '-- All --',
                        allSelected: 'Tất cả'
                    });
                }
            });
        }).multipleSelect({
            filter: true,
            width: '100%',
            maxHeight: '80',
            position: 'top',
            placeholder: "Chọn...",
            selectAllText: '-- All --',
            allSelected: 'Tất cả'
        });

        $('#ProductList').change(function () {
            var list_id = $(this).val();
            var promo_id = <?=$data->promo_id?>;
            $.ajax({
                url: "<?= $base_link; ?>getProductByList",
                dataType: "html",
                type: "POST",
                data: "promo_id="+promo_id+"&list_id=" + list_id,
                success: function (result) {
                    $('#Product').html(result);
                }
            });
        });

        $('#Area').change(function () {
            var area_id = $(this).val();
            var promo_id = "<?=$data->promo_id?>";
            $.ajax({
                url: "<?= $base_link; ?>getCityByArea",
                dataType: "html",
                type: "POST",
                data: "promo_id="+promo_id+"&area_id=" + area_id,
                success: function (result) {
                    $('#City').html(result);
                }
            });
        });

        $('.save').click(function () {
            var type = $(this).attr("rel");
            $('#promo_type').val(type);

            var promo_name = $("#promo_name").val();
            if (promo_name == "") {
                alert('Vui lòng nhập tên chiến dịch');
                $("#promo_name").focus();
                return false;
            }

            var promo_quantity = $("#_quantity").val();
            if ($("#_quantity").attr("disabled") != "disabled") {
                if (promo_quantity == "") {
                    alert('Vui lòng nhập số lượng');
                    $("#_quantity").focus();
                    return false;
                }
            }

            var promo_value = $("#_value").val();
            if ($("#_value").attr("disabled") != "disabled") {
                if (promo_value == "") {
                    alert('Vui lòng nhập giá trị');
                    $("#_value").focus();
                    return false;
                }
            }

            var promo_discount = $("#_discount").val();
            if ($("#_discount").attr("disabled") != "disabled") {
                if (promo_discount == "") {
                    alert('Vui lòng nhập chiết khấu');
                    $("#_discount").focus();
                    return false;
                }
            }

            if (!confirm('Bạn muốn cập nhật chiến dịch này ??')) {
                return false;
            } else {
                //Do nothing
                var form = $('#frm');
                $.ajax({
                    url: "<?= $base_link; ?>update",
                    dataType: "html",
                    type: "POST",
                    data: form.serialize(),
                    success: function (result) {
                        if (result == "OK") {
                            window.location = "<?= $base_link; ?>all";
                        } else {
                            console.log(result);
                        }
                    }
                });
            }
        });
    });
</script>
