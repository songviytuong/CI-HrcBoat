<div class="containner">
	<div class="warehouse_transaction">
		<div class="block1">
			<div class="block1_1"><h1>HOẠT ĐỘNG KHO</h1></div>
			<div class="block1_2">
				Chọn kho : 
				<select onchange="fillterwarehose(this)">
					<option value="0">-- Tất cả các kho --</option>
					<?php 
					if($this->user->UserType==2 || $this->user->UserType==8){
						$warehouse = $this->db->query("select * from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%'")->result();
					}else{
						$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
					}
					if(count($warehouse)>0){
						foreach($warehouse as $row){
							$selected = $WarehouseID==$row->ID ? "selected='selected'" : '' ;
							echo "<option value='$row->ID' $selected>$row->MaKho</option>";
						}
					}
					?>
				</select>
			</div>
		</div>
		<div class="block2">
			<?php 
			$bonus = $WarehouseID ==0 ? '' : " and ID=$WarehouseID" ;
			if($this->user->UserType==2 || $this->user->UserType==8){
				$warehouse = $this->db->query("select * from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%' $bonus")->result();
			}else{
				$warehouse = $this->db->query("select * from ttp_report_warehouse where 1=1 $bonus")->result();
			}
			if(count($warehouse)>0){
				$order = $this->db->query("select KhoID,Status,count(ID) as Total from ttp_report_order where Status in(3,5,9) and Ngaydathang>='2016-03-15' group by KhoID,Status")->result();
				$arr_status = array();
				if(count($order)>0){
					foreach($order as $row){
						$arr_status[$row->Status][$row->KhoID] = $row->Total;
					}
				}
				$transfer_sender = $this->db->query("select WarehouseSender,count(ID) as Total from ttp_report_transferorder where Status=0 or Status=2 group by WarehouseSender")->result();
				$arr_transfersender = array();
				if(count($transfer_sender)>0){
					foreach ($transfer_sender as $row) {
						$arr_transfersender[$row->WarehouseSender] = $row->Total;
					}
				}

				$transfer_reciver = $this->db->query("select WarehouseReciver,count(ID) as Total from ttp_report_transferorder where Status=3 group by WarehouseReciver")->result();
				$arr_transferreciver = array();
				if(count($transfer_reciver)>0){
					foreach ($transfer_reciver as $row) {
						$arr_transferreciver[$row->WarehouseReciver] = $row->Total;
					}
				}

				$order_internal = $this->db->query("select KhoID,count(ID) as Total from ttp_report_order where Status=2 and SourceID=11 and KenhbanhangID=7 group by KhoID")->result();
				$arr_internal = array();
				if(count($order_internal)>0){
					foreach($order_internal as $row){
						$arr_internal[$row->KhoID] = $row->Total;
					}
				}

				$import_po = $this->db->query("select KhoID,Type,count(ID) as Total from ttp_report_inventory_import where Status=0 group by KhoID,Type")->result();
				$arr_importpo = array();
				$arr_importreject = array();
				if(count($import_po)>0){
					foreach($import_po as $row){
						if($row->Type==0 || $row->Type==2){
							$arr_importpo[$row->KhoID] = $row->Total;
						}
						if($row->Type==1){
							$arr_importreject[$row->KhoID] = $row->Total;
						}
					}
				}
				$first_3 = $this->db->query("select ID from ttp_report_order where Status=3 order by ID ASC limit 0,1")->row();
				$links = base_url().ADMINPATH."/report/";
				foreach($warehouse as $row){
					$Amount_3 = isset($arr_status[3][$row->ID]) ? $arr_status[3][$row->ID] : 0 ;
					$Amount_5 = isset($arr_status[5][$row->ID]) ? $arr_status[5][$row->ID] : 0 ;
					$Amount_9 = isset($arr_status[9][$row->ID]) ? $arr_status[9][$row->ID] : 0 ;
					$sender = isset($arr_transfersender[$row->ID]) ? $arr_transfersender[$row->ID] : 0 ;
					$reciver = isset($arr_transferreciver[$row->ID]) ? $arr_transferreciver[$row->ID] : 0 ;
					$internal = isset($arr_internal[$row->ID]) ? $arr_internal[$row->ID] : 0 ;
					$request_po = isset($arr_importpo[$row->ID]) ? $arr_importpo[$row->ID] : 0 ;
					$reject = isset($arr_importreject[$row->ID]) ? $arr_importreject[$row->ID] : 0 ;
					
					$hightlight1 =  $Amount_3>0 ? "hightlight" : "" ;
					$hightlight2 =  $Amount_5>0 ? "hightlight" : "" ;
					$hightlight3 =  $Amount_9>0 ? "hightlight" : "" ;
					$hightlight4 =  $sender>0 ? "hightlight" : "" ;
					$hightlight5 =  $reciver>0 ? "hightlight" : "" ;
					$hightlight6 =  $internal>0 ? "hightlight" : "" ;
					$hightlight7 =  $request_po>0 ? "hightlight" : "" ;
					$hightlight8 =  $reject>0 ? "hightlight" : "" ;

					$links_3 = $first_3 ? $links.'import_order/preview/'.$first_3->ID : $links.'import_order' ;
					echo "<div class='warehouse_box'>";
					echo "<table>
						<tr><th colspan='2' style='background:$row->Color;border-radius: 3px 3px 0 0;color: #fff'>KHO $row->MaKho</th></tr>
						<tr><th>XUẤT KHO</th><th>NHẬP KHO</th></tr>
						<tr>
							<td>
								<a href='$links_3' class='$hightlight1'>Đơn hàng mới chờ kho duyệt <span>".number_format($Amount_3)."</span></a>
								<a href='{$links}import_order' class='$hightlight2'>Đơn hàng chờ kế toán duyệt <span>".number_format($Amount_5)."</span></a>
								<a href='{$links}import_order' class='$hightlight3'>Đơn hàng chờ giao <span>".number_format($Amount_9)."</span></a>
								<a href='{$links}warehouse_inventory_transferproducts' class='$hightlight4'>Xuất chuyển nội bộ <span>".number_format($sender)."</span></a>
								<a class='disablelinks'>Xuất trả NCC <span>".number_format(0)."</span></a>
								<a href='{$links}warehouse_inventory_export/internal' class='$hightlight6'>Xuất hủy / cho <span>".number_format($internal)."</span></a>
							</td>
							<td>
								<a href='{$links}warehouse_inventory_import/set_import_type?Type=0' class='$hightlight7'>Nhập kho mua hàng NCC <span>".number_format($request_po)."</span></a>
								<a href='{$links}warehouse_inventory_import/set_import_type?Type=1' class='$hightlight8'>Nhập kho trả hàng <span>".number_format($reject)."</span></a>
								<a href='{$links}warehouse_inventory_transferproducts' class='$hightlight5'>Nhập kho chuyển nội bộ <span>".number_format($reciver)."</span></a>
							</td>
						</tr>
					</table>";
					echo "</div>";
				}
			}else{
				echo '<h1 style="background: #F5E4D8;padding: 10px;text-align: center;border: 1px solid #E0B79B;"><i class="fa fa-exclamation-triangle" style="color: #DC6923;margin-right: 5px;font-size: 16px;"></i> Bạn không đủ quyền để xem các hoạt động kho .</h1>';
			}
			?>
		</div>
    </div>
</div>
<script>
	function fillterwarehose(ob){
		var baselink = "<?php echo $base_link ?>";
		var data = $(ob).val();
		if(data>0){
			window.location=baselink+data;
		}else{
			window.location=baselink;
		}
	}
</script>