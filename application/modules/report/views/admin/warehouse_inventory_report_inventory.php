<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>BÁO CÁO XUẤT NHẬP TỒN HÀNG HÓA</h1>
	    </div>
	    <div class="block2">
	    	<a class="btn btn-primary" href="<?php echo base_url().ADMINPATH.'/report/warehouse/scan_warehouse' ?>"><i class="fa fa-refresh"></i> Đồng bộ dữ liệu</a>
	    </div>
    </div>
    <div class="block_filter_inventory">
    	<form action="<?php base_url().ADMINPATH.'/report/warehouse/report_inventory' ?>" method="get">
			<div class="row">
				<div class="col-xs-6">
					<div class="form-group">
						<label class="col-xs-3 control-label">Chọn kho báo cáo: </label>
						<div class='col-xs-9'>
							<select name="WarehouseID" class="form-control">
								<?php 
								$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
								$WarehouseTitle = '';
								if(count($warehouse)>0){
									foreach($warehouse as $row){
										$selected = $row->ID==$WarehouseID ? "selected='selected'" : '' ;
										$WarehouseTitle = $WarehouseID==$row->ID ? $row->MaKho : $WarehouseTitle ;
										echo "<option value='$row->ID' $selected>$row->MaKho</option>";
									}
								}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group">
						<label class="col-xs-3 control-label">Theo ngành hàng: </label>
						<div class='col-xs-9'>
							<select name="CategoriesID" class="form-control sub_select">
								<?php 
								$categories = $this->db->query("select Title,ID from ttp_report_categories where IsLast=1")->result();
								$CategoriesTitle = '';
								if(count($categories)>0){
									foreach($categories as $row){
										$selected = $row->ID==$CategoriesID ? "selected='selected'" : '' ;
										$CategoriesTitle = $row->ID==$CategoriesID ? $row->Title : $CategoriesTitle ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="form-group">
						<label class="col-xs-3 control-label">Từ ngày: </label>
						<div class='col-xs-9'>
							<input type="text" placeholder="Chọn ngày bắt đầu báo cáo" id="FromDate" name="FromDate" value="<?php echo $FromDate ?>" class="form-control" />
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group">
						<label class="col-xs-3 control-label">Đến ngày: </label>
						<div class='col-xs-9'>
							<input type="text" placeholder="Chọn ngày kết thúc báo cáo" id="ToDate" name="ToDate" value="<?php echo $ToDate ?>" class="form-control" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="form-group">
						<label class="col-xs-3 control-label">Số lượng tồn: </label>
						<div class="col-xs-9">
							<div class="col-xs-3"><input type='radio' name="Type" value="0" <?php echo $Type=='' || $Type==0 ? 'checked="checked"' : '' ?> /> On-Hand</div>
							<div class="col-xs-3"><input type='radio' name="Type" value="1" <?php echo $Type==1 ? 'checked="checked"' : '' ?> /> Available</div>			
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group">
						<div class="col-xs-3"></div>
						<div class="col-xs-9">
							<button class="btn btn-danger"><i class="fa fa-search"></i> Xem báo cáo</button>
							<button class="btn btn-primary"><i class="fa fa-download"></i> Xuất Excel</button>	
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			</div>
    	</form>
    	<div class="alert alert-success alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<p>Kết quả báo cáo số lượng <b>"On-Hand"</b> : kho hàng <b>"<?php echo $WarehouseTitle ?>"</b> - ngành hàng <b>"<?php echo $CategoriesTitle ?>"</b> - từ ngày <b>"<?php echo date('d/m/Y',strtotime($FromDate)) ?>"</b> - đến ngày <b>"<?php echo date('d/m/Y',strtotime($ToDate)) ?>"</b></p>
		</div>
    </div>
    <div class="import_orderlist">
    	<div class="block3 table_data">
    		<table id="table_data">
	    		<tr>
	    			<th rowspan="2">STT</th>
	    			<th rowspan="2">Mã SKU</th>
	    			<th rowspan="2">Tên sản phẩm</th>
	    			<th rowspan="2">Lô hàng</th>
	    			<th colspan="2">Tồn đầu kỳ</th>
	    			<th colspan="2">Nhập trong kỳ</th>
	    			<th colspan="2">Xuất trong kỳ</th>
	    			<th colspan="2">Tồn cuối kỳ</th>
	    		</tr>
	    		<tr>
	    			<th>SL</th>
	    			<th>Giá trị</th>
	    			<th>SL</th>
	    			<th>Giá trị</th>
	    			<th>SL</th>
	    			<th>Giá trị</th>
	    			<th>SL</th>
	    			<th>Giá trị</th>
	    		</tr>

	    		<?php 
	    		/*  Tồn kho đầu kỳ 
				*	0 : Tính số On-Hand
				*	1 : Tính số Available
	    		*/
	    		$arr_total_inventory_head_import = array();
	    		if($Type==0){
	    			$temp = strtotime($FromDate)-3600*24;
	    			$temp = date('Y-m-d',$temp);
	    			$result = $this->db->query("select a.*,b.RootPrice from ttp_report_inventory a,ttp_report_products b where a.ProductsID=b.ID and a.DateInventory='$temp' and a.WarehouseID=$WarehouseID")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					$arr_total_inventory_head_import[$row->ProductsID][$row->ShipmentID]['Total']=$row->OnHand;
	    					$arr_total_inventory_head_import[$row->ProductsID][$row->ShipmentID]['Price']=($row->OnHand*$row->RootPrice);
	    				}
	    			}
	    		}else{
	    			$result = $this->db->query("select a.*,b.RootPrice from ttp_report_inventory a,ttp_report_products b where a.ProductsID=b.ID and a.DateInventory='$FromDate' and a.WarehouseID=$WarehouseID")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					$arr_total_inventory_head_import[$row->ProductsID][$row->ShipmentID]['Total']=$row->Available;
	    					$arr_total_inventory_head_import[$row->ProductsID][$row->ShipmentID]['Price']=($row->Available*$row->RootPrice);
	    				}
	    			}
	    		}

	    		/*  Tồn kho cuối kỳ 
				*	0 : Tính số On-Hand
				*	1 : Tính số Available
	    		*/
	    		$arr_total_inventory_last_import = array();
	    		if($Type==0){
	    			$result = $this->db->query("select a.*,b.RootPrice from ttp_report_inventory a,ttp_report_products b where a.ProductsID=b.ID and a.DateInventory='$ToDate' and a.WarehouseID=$WarehouseID")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					$arr_total_inventory_last_import[$row->ProductsID][$row->ShipmentID]['Total']=$row->OnHand;
	    					$arr_total_inventory_last_import[$row->ProductsID][$row->ShipmentID]['Price']=($row->OnHand*$row->RootPrice);
	    				}
	    			}
	    		}else{
	    			$result = $this->db->query("select a.*,b.RootPrice from ttp_report_inventory a,ttp_report_products b where a.ProductsID=b.ID and a.DateInventory='$ToDate' and a.WarehouseID=$WarehouseID")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					$arr_total_inventory_last_import[$row->ProductsID][$row->ShipmentID]['Total']=$row->OnHand;
	    					$arr_total_inventory_last_import[$row->ProductsID][$row->ShipmentID]['Price']=($row->OnHand*$row->RootPrice);
	    				}
	    			}
	    		}

	    		/*  Nhập kho trong kỳ 
				*	0 : Tính số On-Hand
				*	1 : Tính số Available
	    		*/
	    		$arr_total_inventory_inner_import = array();
	    		if($Type==0){
	    			/*  Nhập kho tất cả các loại
					*	Lấy tất cả danh sách sản phẩm từ phiếu yêu cầu nhập kho đã ở trạng thái nhập kho .
	    			*/
	    			$result = $this->db->query("select b.* from ttp_report_inventory_import a,ttp_report_inventory_import_details b where a.ID=b.ImportID and ((a.Status=4 and a.Type in(0,2)) or (a.Type=1 and a.Status in(2,3,4))) and date(a.NgayNK)>='$FromDate' and date(a.NgayNK)<='$ToDate' and a.KhoID=$WarehouseID")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					if(isset($arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Total'])){
	    						$arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Total']+=$row->Amount;
	    						$arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Price']+=$row->TotalVND;
	    					}else{
	    						$arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Total']=$row->Amount;
	    						$arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Price']=$row->TotalVND;
	    					}
	    				}
	    			}

	    			/*  Nhập sản phẩm từ kho khác
					*	Lấy tất cả danh sách sản phẩm từ phiếu lưu chuyển nội bộ
	    			*/
	    			$result = $this->db->query("select b.* from ttp_report_transferorder a,ttp_report_transferorder_details b where a.ID=b.OrderID and a.Status=4 and date(a.ImportDate)>='$FromDate' and date(a.ImportDate)<='$ToDate' and a.WarehouseReciver=$WarehouseID")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					if(isset($arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Total'])){
	    						$arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Total']+=$row->TotalImport;
	    						$arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Price']+=0;
	    					}else{
	    						$arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Total']=$row->TotalImport;
	    						$arr_total_inventory_inner_import[$row->ProductsID][$row->ShipmentID]['Price']=0;
	    					}
	    				}
	    			}
	    		}else{
	    			
	    		}

	    		/*  Xuất kho trong kỳ 
				*	0 : Tính số On-Hand
				*	1 : Tính số Available
	    		*/
	    		$arr_total_inventory_inner_export = array();
	    		if($Type==0){
	    			/*  Xuất kho bán hàng & cho / tặng / hủy
					*	Lấy tất cả danh sách sản phẩm từ đơn hàng đã ở trạng thái thành công hoặc chuyển cho vận chuyển .
	    			*/
	    			$result = $this->db->query("select b.* from ttp_report_order a,ttp_report_orderdetails b,ttp_report_export_warehouse c where a.ID=c.OrderID and a.ID=b.OrderID and a.Status in(7,0,1) and date(c.Ngayxuatkho)>='$FromDate' and date(c.Ngayxuatkho)<='$ToDate' and a.KhoID=$WarehouseID")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					if(isset($arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Total'])){
	    						$arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Total']+=$row->Amount;
	    						$arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Price']+=$row->ImportPrice;
	    					}else{
	    						$arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Total']=$row->Amount;
	    						$arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Price']=$row->ImportPrice;
	    					}
	    				}
	    			}
	    			
	    			/*  Xuất sản phẩm sang kho khác
					*	Lấy tất cả danh sách sản phẩm từ phiếu lưu chuyển nội bộ
	    			*/
	    			$result = $this->db->query("select b.* from ttp_report_transferorder a,ttp_report_transferorder_details b where a.ID=b.OrderID and a.Status in(3,4) and date(a.ExportDate)>='$FromDate' and date(a.ExportDate)<='$ToDate' and a.WarehouseSender=$WarehouseID")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					if(isset($arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Total'])){
	    						$arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Total']+=$row->TotalExport;
	    						$arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Price']+=0;
	    					}else{
	    						$arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Total']=$row->TotalExport;
	    						$arr_total_inventory_inner_export[$row->ProductsID][$row->ShipmentID]['Price']=0;
	    					}
	    				}
	    			}
				}else{
					
				}

	    		$products = $this->db->query("select a.Title,a.MaSP,a.ID,b.ShipmentCode,b.ID as ShipmentID from ttp_report_products a,ttp_report_shipment b where a.ID=b.ProductsID and a.CategoriesID like '%\"".$CategoriesID."\"%'")->result();
	    		if(count($products)>0){
	    			$i=1;
	    			$arr_products = array();
	    			foreach($products as $row){
	    				$head_products_total = isset($arr_total_inventory_head_import[$row->ID][$row->ShipmentID]['Total']) ? $arr_total_inventory_head_import[$row->ID][$row->ShipmentID]['Total'] : 0 ;
	    				$head_products_price = isset($arr_total_inventory_head_import[$row->ID][$row->ShipmentID]['Price']) ? $arr_total_inventory_head_import[$row->ID][$row->ShipmentID]['Price'] : 0 ;

	    				$last_products_total = isset($arr_total_inventory_last_import[$row->ID][$row->ShipmentID]['Total']) ? $arr_total_inventory_last_import[$row->ID][$row->ShipmentID]['Total'] : 0 ;
	    				$last_products_price = isset($arr_total_inventory_last_import[$row->ID][$row->ShipmentID]['Price']) ? $arr_total_inventory_last_import[$row->ID][$row->ShipmentID]['Price'] : 0 ;

	    				$import_inner_products_total = isset($arr_total_inventory_inner_import[$row->ID][$row->ShipmentID]['Total']) ? $arr_total_inventory_inner_import[$row->ID][$row->ShipmentID]['Total'] : 0 ;
	    				$import_inner_products_price = isset($arr_total_inventory_inner_import[$row->ID][$row->ShipmentID]['Price']) ? $arr_total_inventory_inner_import[$row->ID][$row->ShipmentID]['Price'] : 0 ;
	    				$export_inner_products_total = isset($arr_total_inventory_inner_export[$row->ID][$row->ShipmentID]['Total']) ? $arr_total_inventory_inner_export[$row->ID][$row->ShipmentID]['Total'] : 0 ;
	    				$export_inner_products_price = isset($arr_total_inventory_inner_export[$row->ID][$row->ShipmentID]['Price']) ? $arr_total_inventory_inner_export[$row->ID][$row->ShipmentID]['Price'] : 0 ;
	    				if(isset($arr_products[$row->ID]['Shipment'])){
	    					$arr_products[$row->ID]['Shipment'][] = "<tr>
							<td class='td_150'>$row->ShipmentCode</td>
							<td class='view_td'>".number_format($head_products_total)."</td>
							<td class='view_td'>".number_format($head_products_price)."</td>
							
							<td class='view_td'>".number_format($import_inner_products_total)."</td>
							<td class='view_td'>".number_format($import_inner_products_price)."</td>
							
							<td class='view_td'>".number_format($export_inner_products_total)."</td>
							<td class='view_td'>".number_format($export_inner_products_price)."</td>
							
							<td class='view_td'>".number_format($last_products_total)."</td>
							<td class='view_td'>".number_format($last_products_price)."</td>
	    					</tr>";
	    				}else{
	    					$arr_products[$row->ID]['Shipment'][] = "<tr>
	    					<td class='bg_default' rowspan='___'>$i</td><td class='bg_default' rowspan='___'>$row->MaSP</td><td class='bg_default' rowspan='___'>$row->Title</td>
							<td class='td_150'>$row->ShipmentCode</td>
							<td class='view_td'>".number_format($head_products_total)."</td>
							<td class='view_td'>".number_format($head_products_price)."</td>

							<td class='view_td'>".number_format($import_inner_products_total)."</td>
							<td class='view_td'>".number_format($import_inner_products_price)."</td>

							<td class='view_td'>".number_format($export_inner_products_total)."</td>
							<td class='view_td'>".number_format($export_inner_products_price)."</td>

							<td class='view_td'>".number_format($last_products_total)."</td>
							<td class='view_td'>".number_format($last_products_price)."</td>
	    					</tr>";
	    					$i++;
	    				}
	    			}
	    			foreach($arr_products as $row){
	    				if(isset($row['Shipment'])){
	    					$str = implode('',$row['Shipment']);
	    					echo str_replace('___',count($row['Shipment']),$str);
	    				}
	    			}
	    		}
	    		?>
    		</table>
    	</div>
	</div>
</div>
<style>
	table tr td:first-child{width:30px;text-align: center;background: #FFF !important}
	.daterangepicker{width: auto;}
</style>

<script>
	
	$(document).ready(function () {
        $('#FromDate,#ToDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	function toggle_fillter(ob){
		$(ob).parent('div').parent('div').find('form').toggle();
		var data_status = $(ob).attr('data-status');
		if(data_status=='up'){
			$(ob).html('<i class="fa fa-caret-up"></i>');
			$(ob).attr('data-status','down');
		}else{
			$(ob).html('<i class="fa fa-caret-down"></i>');
			$(ob).attr('data-status','up');
		}
	}

</script>