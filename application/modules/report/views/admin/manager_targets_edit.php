<?php 
$json = isset($data->Data) ? json_decode($data->Data,true) : array() ;
$monthyear = explode('/',$data->Month);
$month = isset($monthyear[0]) ? $monthyear[0] : '' ;
$year = isset($monthyear[1]) ? $monthyear[1] : '' ;
?>
<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type='hidden' value='<?php echo isset($data->ID) ? $data->ID : 0 ; ?>' name="ID" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa chỉ tiêu tháng</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tháng / năm</span></div>
					<div class='block2'>
						<input type='number' class="form-control" name="Month" required style="float: left;width:100px" value="<?php echo $month ; ?>" /> 
						<span style="float:left;padding:8px 10px"> / </span>
						<input type='number' class="form-control" name="Year" required style="float: left;width:100px" value="<?php echo $year ; ?>" />
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Chỉ tiêu tổng doanh số</span></div>
					<div class='block2'><input type='number' class="form-control" name="Sales" required value="<?php echo isset($json['Sales']) ? $json['Sales'] : '' ; ?>" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Chỉ tiêu tổng số lượng sản phẩm bán ra</span></div>
					<div class='block2'><input type='number' class="form-control" name="Amount" required value="<?php echo isset($json['Amount']) ? $json['Amount'] : '' ; ?>" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Chỉ tiêu tổng số lượng đơn hàng</span></div>
					<div class='block2'><input type='number' class="form-control" name="Order" required value="<?php echo isset($json['Order']) ? $json['Order'] : '' ; ?>" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Chỉ tiêu tổng số lượng khách hàng</span></div>
					<div class='block2'><input type='number' class="form-control" name="Customers" required value="<?php echo isset($json['Customers']) ? $json['Customers'] : '' ; ?>" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title"></span></div>
					<div class='block2'><a class="btn btn-default" id="test_result">Tính thử</a></div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>