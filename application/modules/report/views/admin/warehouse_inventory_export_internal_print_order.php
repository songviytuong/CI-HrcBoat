<div class="containner">
	<div class="print_order">
		<h1>THÔNG TIN ĐƠN HÀNG <?php echo $data->MaDH ?></h1>
		<a id="print_page"><i class="fa fa-print"></i> In Phiếu</a>
		<a id="back_page" href='<?php echo base_url().ADMINPATH."/report/warehouse_inventory_export/edit/$data->ID" ?>'><i class="fa fa-undo"></i> Quay lại</a>
		<div class="row row1">
			<li>Người tạo phiếu:</li>
			<li><?php echo $data->FirstName." ".$data->LastName ?></li>
			<li>Ngày bán:</li>
			<li><?php echo date("d/m/Y",strtotime($data->Ngaydathang)) ?></li>
		</div>
		<?php 
		$data_note = json_decode($data->Note,true);
		?>
		<div class="row row2">
			<li>Người nhận hàng:</li>
			<li><?php echo isset($data_note['Nguoinhanhang']) ? $data_note['Nguoinhanhang'] : '' ; ?></li>
		</div>
		<div class="row row3">
			<li>Phòng ban:</li>
			<li><?php echo isset($data_note['Phongban']) ? $data_note['Phongban'] : '' ; ?></li>
		</div>
		<div class="row row6">
			<li>Ghi chú:</li>
			<li><?php echo isset($data_note['Note']) ? $data_note['Note'] : '' ; ?></li>
		</div>
		<div class="table_donhang">
    			<table class="table_data" id="table_data">
	    			<tr>
	    				<th>Mã sản phẩm</th>
	    				<th>Tên sản phẩm</th>
	    				<th>Lô</th>
	    				<th>Giá bán</th>
	    				<th>Số lượng</th>
	    				<th>% CK</th>
	    				<th>Giá trị CK</th>
	    				<th>Giá sau CK</th>
	    				<th>Thành tiền</th>
	    			</tr>
	    			<?php 
	    			$details = $this->db->query("select a.*,b.Title,b.MaSP,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
	    			$arrproducts = array();
	    			$temp = 0;
	    			if(count($details)>0){
	    				foreach($details as $row){
	    					echo "<tr>";
	    					echo "<td>$row->MaSP</td>";
	    					echo "<td>$row->Title</td>";
	    					echo "<td>$row->ShipmentCode</td>";
		    				$giaban = $row->Price+$row->PriceDown;
	    					$phantramck = $giaban==0 ? 0 : round($row->PriceDown/($giaban/100),1);
	    					echo "<td><span>".number_format($giaban)."</span></td>";
	    					echo "<td>$row->Amount</td>";
	    					echo "<td><span>$phantramck</span></td>";
							echo "<td><span>".number_format($row->PriceDown)."</span></td>";
							echo "<td><span>".number_format($row->Price)."</span></td>";
							echo "<td><span>".number_format($row->Total)."</span></td>";
	    					echo "</tr>";
	    					$arrproducts[] = '"data'.$row->ProductsID.'"';
	    				}
	    			}
	    			
	    			$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
	    			$tongtienhang = $data->Total - $data->Chietkhau;
	    			$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi;
	    			?>
	    			<tr class="last">
	    				<td colspan="8">Tổng cộng</td>
	    				<td><span><?php echo number_format($data->Total) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="8">% chiết khấu</td>
	    				<td><?php echo $phantramchietkhau ?></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="8">Giá chiết khấu</td>
	    				<td><span><?php echo number_format($data->Chietkhau) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="8">Tổng tiền hàng</td>
	    				<td><span><?php echo number_format($tongtienhang) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="8">Chi phí vận chuyển</td>
	    				<td><span><?php echo number_format($data->Chiphi) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="8">Tổng giá trị thanh toán</td>
	    				<td><span><?php echo number_format($tonggiatrithanhtoan) ?></span></td>
	    				
	    			</tr>
	    		</table>
	    	</div>
	</div>
</div>
<script>
	$("#print_page").click(function(){
		window.print(); 
	});
</script>