<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo isset($data->ID) ? $data->ID : 0 ; ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa thông tin thương hiệu</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Trạng thái hoạt động</label>
							<div class='col-xs-8'>
								<input type='radio' name="Published" value="1" <?php echo $data->Published==1 ? 'checked="true"' : '' ; ?> /> Enable 
								<input type='radio' name="Published" value="0" <?php echo $data->Published==0 ? 'checked="true"' : '' ; ?> /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Tên thương hiệu</label>
							<div class='col-xs-8'><input type='text' class="form-control" name="Title" value="<?php echo isset($data->Title) ? $data->Title : '' ; ?>" required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label class="col-xs-2 control-label">Nội dung mô tả thương hiệu</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<textarea class="resizable_textarea form-control ckeditor" name="Content"><?php echo $data->Content ?></textarea>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>