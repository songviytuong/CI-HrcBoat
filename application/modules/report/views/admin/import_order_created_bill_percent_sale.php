<div class="containner">
	<div class="bill_print">
		<div class="block1">
			<div class="block1_1">
				<p>CÔNG TY TNHH TM DV TRẦN TOÀN PHÁT</p>
				<p>246/9 Bình Quới, P.28, Q.Bình Thạnh, TPHCM</p>
				<p>Mã số thuế: 0310717887</p>
			</div>
			<div class="block1_2">
				<a id="print_page"><i class="fa fa-print"></i> Lưu & In Phiếu</a>
				<a id="save_page"><i class="fa fa-check-circle"></i> Lưu</a>
				<a id="back_page" href='<?php echo base_url().ADMINPATH."/report/import_order/preview/$data->ID" ?>'><i class="fa fa-undo"></i> Quay lại</a>
				<?php 
				$hinhthucxuatkho = $data->OrderType==1 ? 3 : 0 ;
				$hinhthucxuatkho = $data->OrderType==2 ? 4 : $hinhthucxuatkho ;
				$export = $this->db->query("select * from ttp_report_export_warehouse where OrderID=$data->ID")->row();
				$thismonth = date('m',time());
                $thisyear = date('Y',time());
                $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=$hinhthucxuatkho")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
				$max = "BH".$thisyear.$thismonth.'.'.str_pad($max, 4, '0', STR_PAD_LEFT);
				?>
				<h1>Phiếu xuất kho</h1>
				<p>Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
				<p>Số : <span id="next_MaXK"><?php echo $export ? $export->MaXK : $max ; ?></span></p>
			</div>
			<div class="block1_3">
				<p>Mẫu số : 02-VT</p>
				<p>(Ban hành theo TT200/2014/TT-BTC ngày 22/12/2014 của bộ trưởng BTC)</p>
				<table>
					<tr>
						<td>TK Nợ:</td>
						<td><input type="text" id="TKNO" value="<?php echo $export ? $export->TKNO : "1311" ; ?>" /></td>
					</tr>
					<tr>
						<td>TK Có:</td>
						<td><input type="text" id="TKCO" value="<?php echo $export ? $export->TKCO : "5115" ; ?>" /></td>
					</tr>
					<tr>
						<td>KPP</td>
						<?php 
						$KPP = $data->OrderType==1 ? 'GT' : '' ;
						$KPP = $data->OrderType==2 ? 'MT' : $KPP ;
						?>
						<td><input type="text" id="KPP" value="<?php echo $export ? $export->KPP : $KPP ; ?>" /></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="block2">
			<div class="row">
				<li>Họ tên người nhận:</li>
				<li><?php echo $data->Name ?> </li>
			</div>
			<div class="row">
				<li>Lý do xuất kho:</li>
				<li><input type="text" id="lydoxuatkho" value="<?php echo $export ? $export->Lydoxuatkho : "Bán hàng DIVASHOP" ; ?>" placeholder="Điền lý do xuất kho" /></li>
			</div>
			<div class="row">
				<li>Xuất tại kho:</li>
				<li>
					<?php echo $data->KhoTitle ?>
					<input type="hidden" id="KhoID" value="<?php echo $data->KhoID ?>" />
				</li>
			</div>
			<div class="row">
				<li>Địa chỉ giao hàng:</li>
				<li class="special"><?php echo $data->AddressOrder ?></li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th rowspan='2'>Số TT</th>
					<th rowspan='2'>Tên, nhãn hiệu, quy cách phẩm chất vật tư, dụng cụ sản phẩm hàng hóa</th>
					<th rowspan='2'>Mã số</th>
					<th rowspan='2'>Đơn vị tính</th>
					<th colspan='2'>Số lượng</th>
					<th rowspan='2'>Đơn giá</th>
					<th rowspan='2'>& chiết khấu</th>
					<th rowspan='2'>Giá trị chiết khấu</th>
					<th rowspan='2'>Giá sau chiết khấu</th>
					<th rowspan='2'>Thành tiền (VNĐ)</th>
				</tr>
				<tr>
					<th style="width:100px">Theo chứng từ</th>
					<th>Thực xuất</th>
				</tr>
				<?php 
				$details = $this->db->query("select a.Title,a.MaSP,a.Donvi,b.* from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID=$data->ID")->result();
				$i=1;
				if(count($details)>0){
					foreach($details as $row){
						$giaban = $row->Price+$row->PriceDown;
    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td style='text-align:left'>$row->MaSP</td>";
						echo "<td style='text-align:left'>$row->Donvi</td>";
						echo "<td>$row->Amount</td>";
						echo "<td>$row->Amount</td>";
						echo "<td style='text-align:right'>".number_format($giaban)."</td>";
						echo "<td style='text-align:right'>".number_format($phantramck)."</td>";
						echo "<td style='text-align:right'>".number_format($row->PriceDown,2)."</td>";
						echo "<td style='text-align:right'>".number_format($row->Price,2)."</td>";
						echo "<td style='text-align:right'>".number_format($row->Total)."</td>";
						echo "</tr>";
						$i++;
					}
				}

				$reduce = $this->db->query("select a.*,b.Title from ttp_report_reduce_order a,ttp_report_reduce b where a.ReduceID=b.ID and a.OrderID=$data->ID")->result();
    			$arr_reduce = array();
    			if(count($reduce)>0){
    				foreach($reduce as $row){
    					echo "<tr>";
    					echo "<td>$i</td>";
						echo "<td>$row->Title $row->TimeReduce</td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td style='text-align:right'>(".number_format($row->ValueReduce).")</td>";
    					echo "</tr>";
    					$arr_reduce[]='"data'.$row->ReduceID.'"';
    				}
    			}

				$k = $i<10 ? 10-$i : 0 ;
				for ($i=0; $i < $k; $i++) { 
					echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				}

				$phantramchietkhau = round($data->Chietkhau/($data->Total/100));
				$tongtienhang = $data->Total - $data->Chietkhau;
		    	$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi-$data->Reduce;
				?>
				
				<tr>
					<td></td>
					<td>TỔNG CỘNG</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style='text-align:right'>
					<?php echo number_format($tonggiatrithanhtoan) ?>
					</td>
				</tr>
			</table>
		</div>

		<div class="block4">
			<div>
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
				<div><?php echo $this->user->FirstName." ".$this->user->LastName ?></div>
				<p><?php echo $data->FirstName." ".$data->LastName ?></p>
				<p>In từ tools.trantoanphat.com <br><?php echo date("H:i A d/m/Y",time()) ?></p>
			</div>
			<div>
				<p>Người nhận hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Người giao hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Thủ kho</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Kế toán trưởng</p>
				<p>(Ký, họ tên)</p>
				<p><img style="height:78px" src='public/admin/images/signature.jpg' /></p>
				<p style='margin-top:5px'>Đ.N. Nhật Thảo</p>
			</div>
			<div>
				<p>Giám đốc</p>
				<p>(Ký, họ tên)</p>
				<div>Trần Quốc Dũng</div>
			</div>
		</div>
	</div>
	<input type='hidden' id='hinhthucxuatkho' value='<?php echo $hinhthucxuatkho ?>' />
</div>
<style>body{background:#FFF;}</style>
<script>
	$("#print_page").click(function(){
	    window.print();
	});

	$("#save_page").click(function(){
		$(this).addClass("saving");
		var TKNO 	= $("#TKNO").val();
		var TKCO 	= $("#TKCO").val();
		var KPP 	= $("#KPP").val();
		var Lydo 	= $("#lydoxuatkho").val();
		var KhoID 	= $("#KhoID").val();
		var hinhthucxuatkho 	= $("#hinhthucxuatkho").val();
		if(TKNO=="" || TKCO=="" || KPP=="" || Lydo=='' || KhoID=="" || hinhthucxuatkho==""){
			$(this).removeClass("saving");
			alert("Vui lòng điền đầy đủ các thông tin cần thiết trước khi lưu và in phiếu xuất kho !");
			return false;
		}else{
			$.ajax({
	        	url: "<?php echo $base_link ?>print_export_warehouse",
	            dataType: "html",
	            type: "POST",
	            context:this,
	            data: "OrderID=<?php echo $data->ID ?>&TKNO="+TKNO+"&TKCO="+TKCO+"&KPP="+KPP+"&Lydo="+Lydo+"&KhoID="+KhoID+"&hinhthucxuatkho="+hinhthucxuatkho,
	            success: function(result){
	            	$(this).removeClass("saving");
	            	alert("Lưu thông tin thành công !");
	            }
	        });
		}
	});
</script>