<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÔNG TIN KHÁCH HÀNG</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save</button>
				</div>
			</div>
			<div class="row">
				<table class="customer_table">
					<tr>
						<td>Loại khách hàng</td>
						<td colspan="8">
							<input type="radio" value="0" name="Type" checked='checked' /> Cá nhân 
							<input type="radio" value="1" name="Type" style="margin-left:20px" /> MT 
							<input type="radio" value="2" name="Type" style="margin-left:20px" /> GT 
						</td>
					</tr>
					<tr>
						<?php 
						$max = $this->db->query("select max(ID) as ID from ttp_report_customer")->row();
						$Code = "KH".str_pad($max->ID+1, 8, '0', STR_PAD_LEFT);
						?>
						<td>Mã khách hàng</td>
						<td><input type='text' name='Code' readonly="true" value='<?php echo $Code ?>' /></td>
						<td style="text-align:right;padding-right:10px">Tên</td>
						<td colspan="6"><input type='text' name='Name' class='required' required /></td>
					</tr>
					<tr>
						<td>Địa chỉ đăng ký</td>
						<td colspan="8">
							<input type='text' name='Address' class="required" required />
						</td>
					</tr>
					<tr>
						<td>NTNS</td>
						<td><input type='text' name='NTNS' id="NTNS" placeholder="YYYY-MM-DD" /></td>
						<td style="text-align:right;padding-right:10px">Tuổi</td>
						<td><input type='text' name='Age' /></td>
						<td style="text-align:right;padding-right:10px">Giới tính</td>
						<td>
							<select name="Sex">
								<option value='0'>Nam</option>
								<option value='1'>Nữ</option>
								<option value='2'>Khác</option>
							</select>
						</td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td>Khu vực</td>
						<td>
							<select name="AreaID" id="Khuvuc">
								<option value='0'>-- Chọn khu vực --</option>
								<?php 
								$area = $this->db->query("select * from ttp_report_area")->result();
								if(count($area)>0){
									foreach($area as $row){
										echo "<option value='$row->ID'>$row->Title</option>";
									}
								}
								?>
							</select>
						</td>
						<td style="text-align:right;padding-right:10px">Tỉnh thành</td>
						<td>
							<select name="CityID" id="Tinhthanh">
								<option value='0'>-- Chọn tỉnh thành --</option>
							</select>
						</td>
						<td style="text-align:right;padding-right:10px">Quận huyện</td>
						<td>
							<select name="DistrictID" id="Quanhuyen">
								<option value='0'>-- Chọn quận huyện --</option>
							</select>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Nghề nghiệp</td>
						<td colspan="8">
							<select name="Job">
								<option value="1">Văn phòng</option>
								<option value="2">Kinh doanh-buôn bán</option>
								<option value="3">Nội trợ</option>
								<option value="4">Về hưu</option>
								<option value="5">Sinh viên/học sinh</option>
								<option value="6">Lao động phổ thông</option>
								<option value="0">Khác</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Ghi chú</td>
						<td colspan="8"><input type='text' name="Note" /></td>
					</tr>
					<tr>
						<td>Số di động 1</td>
						<td><input type='text' name="Phone1" class="required" onchange="checkphone(this)" required /></td>
						<td style="text-align:right;padding-right:10px">Số di động 2</td>
						<td><input type='text' name="Phone2" /></td>
						<td style="text-align:right;padding-right:10px">Email</td>
						<td><input type='text' name="Email" placeholder="example@example.com.vn" /></td>
					</tr>
				</table>
			</div>
			<div class="add_advisory">
				<h1>Nội dung tư vấn khách hàng</h1>
				<p>Vấn đề của khách hàng trước khi sử dụng</p>
				<textarea name="BeforeAd"></textarea>
				<p>Giải pháp của nhân viên tư vấn</p>
				<textarea name="Solution"></textarea>
				<p>Hiệu quả sau khi sử dụng</p>
				<textarea name="AfterAd" style="margin-bottom:10px"></textarea>
				<p><input type='checkbox' name='Testimonial' style='margin-right:5px' /> Đồng ý tham gia Testimonial case</p>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 780px !important;}
	.body_content .customer_table{width:100%;border-collapse: collapse;background:none !important;margin-top:10px;}
	.body_content .customer_table tr td{padding:7px 0px;}
	.body_content .customer_table tr td:first-child{width:120px;}
	.body_content .customer_table tr td:nth-child(2){width:200px;}
	.body_content .customer_table tr td:nth-child(3){width:100px;}
	.body_content .customer_table tr td input[type="text"]{width:100%; border:1px solid #c1c1c1;padding:4px 5px;height:32px;}
	.body_content .customer_table tr td select{height:30px;width:100%; border:1px solid #c1c1c1;padding:4px 5px;}
	.body_content .showbox_history table{width:100%;border-collapse: collapse;border:1px solid #E1e1e1;margin-top:10px;}
	.body_content .showbox_history table tr th{padding:5px;text-align:left;border-bottom:1px solid #E1e1e1;}
	.body_content .showbox_history table tr td{padding:5px;text-align:left;border-bottom:1px solid #E1e1e1;}
	.view_history{margin-top:10px;}
	.view_history a{color:#1A82C3;text-decoration: underline;border:none !important;padding:0px !important;margin-right:20px;}
	.view_history a i{font-size: 12px;margin-left:6px;}
	.add_advisory{clear:both;margin-top:10px;}
	.add_advisory h1{font-size: 22px;padding: 10px 0px;text-transform: uppercase;font-weight: bold;margin-bottom: 10px}
	.add_advisory textarea{width:100%;border:1px solid #c1c1c1;height:80px;padding:5px;margin-top:5px;}
	.daterangepicker{width: auto;}
</style>
<script>
	$(document).ready(function () {
        $('#NTNS').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_city_by_area' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_district_by_city' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            }
	        });
		}
	});

	function checkphone(ob){
		var data = $(ob).val();
		if(data!=''){
			$.ajax({
	        	url: "<?php echo base_url().ADMINPATH.'/report/import_customer/checkphone' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "data="+data,
	            success: function(result){
	            	if(result=='false'){
	            		alert("Số điện thoại bạn vừa nhập đã có người sử dụng . Vui lòng thay đổi số dt khác .");
	            		$(ob).val('').focus();
	            	}
	            }
	        });
        }else{
        	alert("Số dt không được để trống !!");
        	$(ob).focus();
        }
	}
</script>