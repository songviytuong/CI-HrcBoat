<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type='hidden' name="ID" value='<?php echo $data->ID ?>' /> 
			<div class="fillter_bar">
				<div class="block1">
					<h1>CHỈNH SỬA THÔNG TIN KHO</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Lưu dữ liệu</button>
				</div>
			</div>
			<div class="box_content_warehouse">
				<div class="block1">
					<table class="table1">
						<tr>
							<td>Mã kho:</td>
							<td><input type='text' name='MaKho' value='<?php echo $data->MaKho ?>' /></td>
							<td>Tên kho:</td>
							<td colspan='3'><input type='text' name='Title' value='<?php echo $data->Title ?>' /></td>
						</tr>
						<tr>
							<td>Phân loại kho:</td>
							<td>
								<select name="">
									<option>-- Chọn phân loại kho --</option>
								</select>
							</td>
							<td>Màu sắc kho</td>
							<td><input type='color' name="Color" value="<?php echo $data->Color ?>" /></td>
						</tr>
						<tr>
							<td style="vertical-align:top">Thủ kho:</td>
							<td colspan="5">
								<?php 
								$user = $this->db->query("select ID,UserName from ttp_user where UserType=2 or UserType=8")->result();
								if(count($user)>0){
									$user_list = array();
									echo "<select id='user_warehouse'>";
									foreach($user as $row){
										$user_list[$row->ID] = $row->UserName;
										echo "<option value='$row->ID'>$row->UserName</option>";
									}
									echo "</select>";
									echo "<a class='add_user' onclick='add_user()'><i class='fa fa-plus'></i> ADD</a><div style='clear:both;margin-top: 5px;'></div>";
									$arr_user = json_decode($data->Manager,true);
									$data_user = array();
									if(is_array($arr_user) && count($arr_user)>0){
										foreach($arr_user as $row){
											$data_user[]="'data$row'";
											$name = isset($user_list[$row]) ? $user_list[$row] : '--' ;
											echo "<li class='list_owner'>$name <a onclick='remove_user(this,$row)'>[x]</a> <input type='hidden' name='Manager[]' value='$row' /></li>";
										}
									}
								}
								?>
							</td>
						</tr>
						<tr>
							<td>Địa chỉ kho</td>
							<td colspan='5'><input type='text' name='Address' value='<?php echo $data->Address ?>' /></td>
						</tr>
						<tr>
							<td>Khu vực:</td>
							<td>
								<select name='AreaID' id="Khuvuc">
									<option value="">-- Chọn khu vực --</option>
				    				<?php 
				    				$area = $this->db->query("select ID,Title from ttp_report_area")->result();
				    				if(count($area)>0){
				    					foreach($area as $row){
				    						$selected = $row->ID==$data->AreaID ? "selected='selected'" : '' ;
				    						echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
								</select>
							</td>
							<td>Tỉnh/Thành:</td>
							<td>
								<select name='CityID' id="Tinhthanh">
									<?php 
										$city = $this->db->query("select ID,Title from ttp_report_city where ID=$data->CityID")->row();
				    					if($city){
			    							echo "<option value='$city->ID'>$city->Title</option>";
			    						}else{
			    							echo '<option value="">-- Chọn Tỉnh/Thành --</option>';
			    						}
				    				?>
								</select>
							</td>
							<td>Quận/Huyện:</td>
							<td>
								<select name='DistrictID' id="Quanhuyen">
									<?php 
										$district = $this->db->query("select ID,Title from ttp_report_district where ID=$data->DistrictID")->row();
				    					if($district){
			    							echo "<option value='$district->ID'>$district->Title</option>";
			    						}else{
			    							echo '<option value="">-- Chọn Quận/Huyện --</option>';
			    						}
				    				?>
								</select>
							</td>
						</tr>

						<tr>
							<td>Số di động 1:</td>
							<td><input type='text' name='Phone1' value='<?php echo $data->Phone1 ?>' /></td>
							<td>Số di động 2:</td>
							<td><input type='text' name='Phone2' value='<?php echo $data->Phone2 ?>' /></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
				<!-- end block1 -->
		    	<div class="block2">
					<a class="btn btn-primary" id="add_position_to_warehouse"><i class="fa fa-plus"></i> Vị trí</a>
					<ul>
						<li><a id='show_thaotac'>Thao tác <b class="caret"></b></a>
		    				<ul>
			    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa vị trí</a></li>
		    				</ul>
						</li>
					</ul>
		    	</div>
		    	<div class="clear"></div>
		    	<div class="table_donhang">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Khu vực</th>
		    				<th>Tên kệ</th>
		    				<th>Tên cột</th>
		    				<th>Tên hàng</th>
		    			</tr>
		    			<?php 
		    			$position = $this->db->query("select * from ttp_report_warehouse_position where WarehouseID=$data->ID")->result();
		    			if(count($position)>0){
		    				foreach($position as $row){
		    					echo "<tr>";
		    					echo "<td><input type='checkbox' class='selected_products' data='$row->ID' /></td>";
		    					echo "<td><a onclick='edit_row(this,$row->ID)'>$row->Position</a></td>";
		    					echo "<td><a onclick='edit_row(this,$row->ID)'>$row->Rack</a></td>";
		    					echo "<td><a onclick='edit_row(this,$row->ID)'>$row->Col</a></td>";
		    					echo "<td><a onclick='edit_row(this,$row->ID)'>$row->Row</a></td>";
		    					echo "</tr>";
		    				}
		    			}else{
		    				echo '<tr class="last_tr">
		    				<td colspan="5">Không có dữ liệu vị trí</td>
		    			</tr>';	
		    			}
		    			?>
		    		</table>
		    	</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<script type="text/javascript">
	var link = "<?php echo base_url().ADMINPATH.'/report/' ?>";

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_city_by_area",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_district_by_city",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	                var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            	$("#KhoID").html(jsonobj.WarehouseHtml);
	            }
	        });
		}
	});

	$("#add_position_to_warehouse").click(function(){
		$("#table_data .last_tr").remove();
		$("#table_data").append("<tr><td><input type='checkbox' class='selected_products' data='' /></td><td><input type='text' name='Position[]' /></td><td><input type='text' name='Rack[]' /></td><td><input type='text' name='Colum[]' /></td><td><input type='text' name='Row[]' /></td></tr>");
	});

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		var list = "";
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var ID = $(this).attr('data');
				if(ID!=''){
					list = list+"|"+ID;
				}
				$(this).parent('td').parent('tr').remove();
			}
		});
		if(list!=''){
			$.ajax({
            	url: link+"warehouse_warehouse/delete_position",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "list="+list,
	            success: function(result){}
	        });
		}
	});

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function edit_row(ob,ID){
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"500px"});
		$(".over_lay .box_inner .block1_inner h1").html("Chỉnh sửa thông tin khu vực");
		$(".over_lay .box_inner .block2_inner").load(link+'warehouse_warehouse/edit_position/'+ID);
		$(".over_lay").show();
	}

	var celldata = [<?php echo implode(',',$data_user) ?>];

	function add_user(){
		var user = $("#user_warehouse").val();
		var name = $("#user_warehouse option:selected" ).text();
		if(jQuery.inArray( "data"+user, celldata )<0){
			$("#user_warehouse").parent('td').append("<li class='list_owner'>"+name+"<a onclick='remove_user(this,"+user+")'>[x]</a> <input type='hidden' name='Manager[]' value='"+user+"' /></li>");
			celldata.push("data"+user);
		}
	}

	function remove_user(ob,user){
		$(ob).parent('li').remove();
		var index = celldata.indexOf("data"+user);
		celldata.splice(index, 1);
	}
</script>