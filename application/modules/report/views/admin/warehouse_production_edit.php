<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo isset($data->ID) ? $data->ID : 0 ; ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa thông tin nhà cung cấp</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tên nhà cung cấp</span></div>
					<div class='block2'><input type='text' class="form-control" name="Title" value="<?php echo isset($data->Title) ? $data->Title : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Mã nhà cung cấp</span></div>
					<div class='block2'><input type='text' class="form-control" name="Code" value="<?php echo isset($data->ProductionCode) ? $data->ProductionCode : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Địa chỉ</span></div>
					<div class='block2'><input type='text' class="form-control" name="Address" value="<?php echo isset($data->Address) ? $data->Address : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Điện thoại</span></div>
					<div class='block2'><input type='text' class="form-control" name="Phone" value="<?php echo isset($data->Phone) ? $data->Phone : '' ; ?>" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Fax</span></div>
					<div class='block2'><input type='text' class="form-control" name="Fax" value="<?php echo isset($data->Fax) ? $data->Fax : '' ; ?>" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Trạng thái hoạt động</span></div>
					<div class='block2'>
						<input type='radio' name="Published" value="1" <?php echo $data->Published==1 ? 'checked="true"' : '' ; ?> /> Enable 
						<input type='radio' name="Published" value="0" <?php echo $data->Published==0 ? 'checked="true"' : '' ; ?> /> Disable 
					</div>
				</div>
			</div>
		</form>
	</div>
</div>