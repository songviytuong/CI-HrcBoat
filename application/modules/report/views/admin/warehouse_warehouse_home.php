<?php 
$result_fillter = array();
$fill_temp_value="";
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>DANH MỤC KHO HÀNG</h1>
	    </div>
	    <div class="block2"></div>
    </div>
    <div class="import_orderlist">
    	<div class="block2">
    		<div class="block_2_1">
    			<a class="btn btn-danger" href="<?php echo $base_link.'add' ?>"><i class="fa fa-plus"></i> KHO MỚI</a>
    		</div>
    		<div class="block_2_2">
    			<button class="btn btn-default" onclick="showtools()">Advanced Filter <b class="caret"></b></button>
    		</div>
    	</div>
        <div class="clear"></div>
        <div class="filltertools">
            <form action="<?php echo $base_link."setfillter" ?>" method="post">
                <?php 
                    $arr_fieldname = array(0=>"AreaID",1=>"CityID",2=>"MaKho",3=>"Manager",4=>"Phone1");
                    $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                    $arr_showfieldname = array(0=>"Khu vực",1=>"Tỉnh thành",2=>"Mã Kho",3=>"Thủ Kho",4=>"Phone1");
                    $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                    $fill_data_arr = explode(" and ",$fill_data);
                    $arr_field = array();
                    if(count($fill_data_arr)>0 && $fill_data!=''){
                        $temp_tools=0;
                        foreach($fill_data_arr as $row){
                            $param = explode(' ',$row,3);
                            $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                            if(isset($param[0])){
                                $arr_field[] = "'data".array_search($param[0],$arr_fieldname)."'";
                                $k = array_search($param[0],$arr_fieldname);
                            }else{
                                $k=0;
                            }
                            $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                            $param_oparation = isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                            $param_value = isset($param[2]) ? $param[2] : '' ;
                            $param_value = str_replace("\'","",$param_value);
                            $param_value = str_replace("'","",$param_value);
                            $param_value = str_replace("%","",$param_value);
                ?>
                <div class="row <?php echo $temp_tools==0 ? "base_row row$k" : " row$k" ; ?>">
                    <div class="list_toolls col-xs-1"><label class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></label></div>
                    <div class="list_toolls col-xs-2">
                        <input type="hidden" class="FieldName form-control" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                        <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></label>
                        <ul class="dropdownbox" style='width: 215px;'>
                            <li><a onclick="setfield(this,0,'khuvuc')">Khu Vực</a></li>
                            <li><a onclick="setfield(this,1,'tinhthanh')">Tỉnh Thành</a></li>
                            <li><a onclick="setfield(this,2,'makho')">Mã Kho</a></li>
                            <li><a onclick="setfield(this,4,'phone')">Phone1</a></li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation col-xs-2">
                        <select class="oparation form-control" name="FieldOparation[]">
                            <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                            <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                            <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                            <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                            <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                            <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                            <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter col-xs-3">
                        <?php 
                        if($value_field==0){
                            $area = $this->db->query("select ID,Title from ttp_report_area")->result();
                            if(count($area)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($area as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                                echo "</select>";
                            }
                        }elseif($value_field==1){ 
                            $city = $this->db->query("select ID,Title from ttp_report_city")->result();
                            if(count($city)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($city as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                                echo "</select>";
                            }
                        }else{
                            $fill_temp_value = $param_value ;
                            echo '<input type="text" class="form-control" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                        }
                        ?>
                    </div>
                    <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
                <?php   
                        $temp_tools++;
                        $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                        $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                        }
                    }else{
                ?>
                    <div class="row base_row">
                        <div class="list_toolls col-xs-1"><label class="title first-title">Chọn</label></div>
                        <div class="list_toolls col-xs-2">
                            <input type="hidden" class="FieldName form-control" name="FieldName[]" />
                            <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                            <ul class="dropdownbox" style='width: 215px;'>
                                <li><a onclick="setfield(this,0,'khuvuc')">Khu Vực</a></li>
                                <li><a onclick="setfield(this,1,'tinhthanh')">Tỉnh Thành</a></li>
                                <li><a onclick="setfield(this,2,'makho')">Mã Kho</a></li>
                                <li><a onclick="setfield(this,4,'phone')">Phone1</a></li>
                            </ul>
                        </div>
                        <div class="list_toolls reciveroparation col-xs-2">
                            <select class="oparation form-control" name="FieldOparation[]">
                                <option value="1">Bằng</option>
                                <option value="0">Có chứa</option>
                                <option value="2">Khác</option>
                                <option value="3">Lớn hơn</option>
                                <option value="4">Nhỏ hơn</option>
                                <option value="5">Lớn hơn hoặc bằng</option>
                                <option value="6">Nhỏ hơn hoặc bằng</option>
                            </select>
                        </div>
                        <div class="list_toolls reciverfillter col-xs-3">
                            <input type="text" class="form-control" name="FieldText[]" id="textsearch" />
                        </div>
                        <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                    </div>
                <?php
                    }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-11">
                                <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                                <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    	<div class="block3 table_data">
            <?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            ?>
    		<table id="table_data">
    			<tr>
                    <th>STT</th>
                    <th>Mã kho</th>
    				<th>Tên kho</th>
    				<th>Thủ kho</th>
    				<th>Số ĐT liên lạc</th>
    				<th>Khu vực</th>
    				<th>Thành phố</th>
    			</tr>
                <?php 
                $user_warehouse = $this->db->query("select ID,UserName from ttp_user where UserType=2 or UserType=8")->result();
                $arr_userowner = array();
                if(count($user_warehouse)>0){
                    foreach($user_warehouse as $row){
                        $arr_userowner[$row->ID] = $row->UserName;
                    }
                }

                $i = $start+1;
                if(count($data)>0){
                    $area = $this->db->query("select ID,Title from ttp_report_area")->result();
                    $arr_area = array();
                    if(count($area)>0){
                        foreach($area as $row){
                            $arr_area[$row->ID] = $row->Title;
                        }
                    }
                    $city = $this->db->query("select ID,Title from ttp_report_city")->result();
                    $arr_city = array();
                    if(count($city)>0){
                        foreach($city as $row){
                            $arr_city[$row->ID] = $row->Title;
                        }
                    }
    				foreach($data as $row){
    					$title_area = isset($arr_area[$row->AreaID]) ? $arr_area[$row->AreaID] : '--' ;
                        $title_city = isset($arr_city[$row->CityID]) ? $arr_city[$row->CityID] : '--' ;
                        echo "<tr>
                                <td style='text-align:center'>$i</td>
                                <td><a href='{$base_link}edit/$row->ID'>$row->MaKho</a></td>
			    				<td><a href='{$base_link}edit/$row->ID'>$row->Title</a></td>";
                        $userowner = json_decode($row->Manager,true);
                        if(count($userowner)>0 && is_array($userowner)){
                            $str = array();
                            foreach($userowner as $item){
                                if(isset($arr_userowner[$item])){ 
                                    $str[] = $arr_userowner[$item];
                                }
                            }
                            $str = implode(' , ', $str);
                            echo "<td style='min-width:100px'><a href='{$base_link}edit/$row->ID'>$str</a></td>";
                        }else{
                            echo "<td style='min-width:100px'><a href='{$base_link}edit/$row->ID'>--</a></td>";    
                        }
                        echo "<td style='min-width:100px'><a href='{$base_link}edit/$row->ID'>".$row->Phone1."</a></td>";
                        echo "<td style='min-width:100px'><a href='{$base_link}edit/$row->ID''>$title_area</a></td>";
                        echo "<td style='min-width:100px'><a href='{$base_link}edit/$row->ID''>$title_city</a></td>";
                        echo "</tr>";
                        $i++;
    				}
                    echo "<tr><td colspan='8'>Tìm thấy tổng cộng <b>".number_format($find,0)."</b> kho </td></tr>";
    			}else{
    				echo "<tr><td colspan='8'>Không tìm thấy kho.</td></tr>";
    			}
    			?>
    		</table>
            <?php 
                echo $nav;
            ?>
    	</div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/warehouse_warehouse/" ?>" />
</div>

<script>
    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="khuvuc" || fieldname=="tinhthanh"){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
        }
    }
</script>