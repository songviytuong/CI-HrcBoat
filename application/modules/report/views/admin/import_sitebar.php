<?php 
$url = base_url().ADMINPATH.'/report';
$segment_current = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-shopping-cart fa-fw"></i> Doanh số</p>
    <ul>
        <li><a href="<?php echo $url.'/import_targets' ?>" <?php echo $segment_current=='import_targets' ? "class='active'" : '' ; ?>>Chỉ tiêu doanh số</a></li>
        <li><a href="<?php echo $url.'/import_order' ?>" <?php echo $segment_current=='import_order' ? "class='active'" : '' ; ?>>Đơn hàng</a></li>
        <li><a href="<?php echo $url.'/request_cancel' ?>" <?php echo $segment_current=='request_cancel' ? "class='active'" : '' ; ?>>Yêu cầu hủy ĐH</a>
        <?php 
        if($this->user->UserType==5 || $this->user->UserType==7 || $this->user->UserType==8){
            $bonus = "";
            $bonus = $this->user->UserType==5 ? " and b.UserType=1" : $bonus ;
            $bonus = $this->user->UserType==7 ? " and b.UserType=3" : $bonus ;
            $bonus = $this->user->UserType==8 ? " and b.UserType=2" : $bonus ;
            $cancel = $this->db->query("select count(1) as Soluong from ttp_report_request_cancelorder a,ttp_user b where a.Status=0 and a.UserID=b.ID $bonus")->row();
            echo $cancel->Soluong>0  ? "<span class='notification_num'>$cancel->Soluong</span>" : "" ;
        }
        ?>
        </li>
    </ul>
    <p class="title"><i class="fa fa-line-chart fa-fw"></i> DIVASHOP</p>
    <ul>
        <li><a href="<?php echo $url.'/import_customer' ?>" <?php echo $segment_current=='import_customer' && ($segment_current_bonus=='' || $segment_current_bonus=='add' || $segment_current_bonus=='edit') ? "class='active'" : '' ; ?>>Danh sách khách hàng</a></li>
        <li><a>Kênh TeleSales</a></li>
        <li><a>Kênh Web Order</a></li>
        <li><a>Hệ thống POS</a></li>
        <li><a>Sales Skill Development</a></li>
        <li><a>Marketing Campaign</a></li>
        <li><a href="<?php echo $url.'/promotion_campaign/all' ?>" <?php echo $segment_current=='promotion_campaign' ? "class='active'" : '' ; ?>>Promotion Campaign</a></li>
    </ul>
    <p class="title"><i class="fa fa-pie-chart fa-fw"></i> TTP SALES</p>
    <ul>
        <li><a href="<?php echo $url.'/import_customer/sales_customers' ?>" <?php echo $segment_current=='import_customer' && ($segment_current_bonus=='sales_customers' || $segment_current_bonus=='add_sales' || $segment_current_bonus=='edit_sales' || $segment_current_bonus=='add_sales_mt') ? "class='active'" : '' ; ?>>Đại lý & nhà phân phối</a></li>
        <li><a>Kênh GT</a></li>
        <li><a>Kênh MT</a></li>
        <li><a>Hệ thống BA</a></li>
    </ul>
    <p class="title"><i class="fa fa-users fa-fw"></i> Customer Services</p>
    <ul>
        <li><a>Tổng quan</a></li>
        <li><a>Theo SL Email gửi</a></li>
        <li><a>Theo SL cuộc gọi</a></li>
        <li><a>Theo SL online chat</a></li>
        <li><a>Theo thời gian xử lý</a></li>
        <li><a>Theo phân loại tình huống</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>

<script src="public/admin/js/notify.js"></script>
<?php
if($this->user->UserType==5 || $this->user->UserType==7 || $this->user->UserType==8){
?>
<script>
    var maxnoti = <?php echo isset($cancel) ? $cancel->Soluong : 0 ; ?>;
    var accept = '';
    var username = '';
    var madh = '';
    var getcurrentorder = function(){
        $.ajax({
            url: "<?php echo base_url().ADMINPATH; ?>/report/import/check_notify",
            dataType: "html",
            type: "POST",
            data: "Max="+maxnoti,
            success: function(result){
                if(result!="false"){
                    var arrnotify = result.split("|");
                    maxnoti = arrnotify[0];
                    var accept = arrnotify[1];
                    var username = arrnotify[2];
                    var madh = arrnotify[3];
                    var img = arrnotify[4];
                    if(accept==0){
                        notifyMe(img,username +' vừa gửi yêu cầu hủy đơn hàng '+madh);
                    }else{
                        var useraccept = arrnotify[5];
                        notifyMe(img,useraccept + ' vừa xử lý yêu cầu hủy đơn hàng '+ madh +' của '+username);
                    }
                }
            }
        });
    }
    setInterval(getcurrentorder,5000);

    function notifyMe(image,message){
        notify('THÔNG BÁO HỆ THỐNG', {
            body: message,
            icon: image,
            onclick: function(e) {
                window.location = "<?php echo base_url().ADMINPATH.'/report/request_cancel' ?>";
            },
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }
<?php 
}
?>
</script>