<div class="containner">
	<div class="bill_print">
		<div class="block1">
			<div class="block1_1">
				<p>CÔNG TY TNHH TM DV TRẦN TOÀN PHÁT</p>
				<p>246/9 Bình Quới, P.28, Q.Bình Thạnh, TPHCM</p>
				<p>Mã số thuế: 0310717887</p>
			</div>
			<div class="block1_2">
				<?php 
				$export = $this->db->query("select * from ttp_report_export_warehouse where OrderID=$data->ID")->row();
				if($export){
					echo '<a id="print_page"><i class="fa fa-print"></i> In Phiếu</a>';
				}
				?>
				<a id="save_page"><i class="fa fa-check-circle"></i> Lưu</a>
				<a id="back_page" href='<?php echo base_url().ADMINPATH."/report/import_order/preview/$data->ID" ?>'><i class="fa fa-undo"></i> Quay lại</a>
				<a id="print_have_ck" href='<?php echo base_url().ADMINPATH."/report/import/lapphieuxuatkho_percent/$data->ID" ?>'>In có chiết khấu</a>
				<?php 
				$thismonth = date('m',time());
                $thisyear = date('Y',time());
                $Type = $data->OrderType==0 ? 'and TypeExport=0' : '' ;
                $Type = $data->OrderType==3 ? 'and TypeExport=3' : $Type ;
                $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=1 $Type")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
                $show = $data->OrderType==0 ? 'OL' : '' ;
                $show = $data->OrderType==3 ? 'GS' : $show ;
                $max = "BH".$show.$thisyear.$thismonth.'.TA.'.str_pad($max, 5, '0', STR_PAD_LEFT);
				?>
				<h1>Phiếu xuất kho bán hàng</h1>
				<p>Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
				<p>Số : <span id="next_MaXK"><?php echo $export ? $export->MaXK : $max ; ?></span></p>
			</div>
			<div class="block1_3">
				<p>Mẫu số : 02-VT</p>
				<p>(Ban hành theo TT200/2014/TT-BTC ngày 22/12/2014 của bộ trưởng BTC)</p>
				<table>
					<tr>
						<td>TK Nợ:</td>
						<td><input type="text" id="TKNO" value="<?php echo $export ? $export->TKNO : "1311" ; ?>" /></td>
					</tr>
					<tr>
						<td>TK Có:</td>
						<td><input type="text" id="TKCO" value="<?php echo $export ? $export->TKCO : "5115" ; ?>" /></td>
					</tr>
					<tr>
						<td>KPP</td>
						<td><input type="text" id="KPP" value="<?php echo $export ? $export->KPP : "OL" ; ?>" /></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="block2">
			<div class="row">
				<li>Họ tên người nhận:</li>
				<li><?php echo $data->Name ?> </li>
				<li>Số điện thoại 1:</li>
				<li><?php echo $data->Phone1 ?></li>
				<li>Số điện thoại 2:</li>
				<li><?php echo $data->Phone2 ?></li>
			</div>
			<div class="row">
				<li>Lý do xuất kho:</li>
				<li><input type="text" id="lydoxuatkho" value="<?php echo $export ? $export->Lydoxuatkho : "Bán hàng DIVASHOP" ; ?>" placeholder="Điền lý do xuất kho" /></li>
				<li>Theo đơn hàng số:</li>
				<li><?php echo $data->MaDH ?></li>
				<li>Ngày:</li>
				<li><?php echo date("d/m/Y",strtotime($data->Ngaydathang)) ?></li>
			</div>
			<div class="row">
				<li>Xuất tại kho:</li>
				<li>
					<?php echo $data->KhoTitle ?>
					<input type="hidden" id="KhoID" value="<?php echo $data->KhoID ?>" />
				</li>
				<li>Hình thức xuất:</li>
				<li>
				<select id="hinhthucxuatkho">
					<option value='1' <?php echo $export && $export->Hinhthucxuatkho==1 ? "selected='selected'" : "" ; ?>>TA</option>
					<option value='0' <?php echo $export && $export->Hinhthucxuatkho==0 ? "selected='selected'" : "" ; ?>>NB</option>
				</select>
				</li>
			</div>
			<div class="row">
				<li>Địa chỉ giao hàng:</li>
				<li class="special"><?php echo $data->AddressOrder ?></li>
			</div>
			<div class="row">
				<li>Ghi chú khách hàng:</li>
				<li class="special"><?php echo $data->Note ?></li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th rowspan='2'>Số TT</th>
					<th rowspan='2'>Tên, nhãn hiệu, quy cách phẩm chất vật tư, dụng cụ sản phẩm hàng hóa</th>
					<th rowspan='2'>Mã số</th>
					<th rowspan='2'>Đơn vị tính</th>
					<th colspan='2'>Số lượng</th>
					<th rowspan='2'>Đơn giá</th>
					<th rowspan='2'>Thành tiền (VNĐ)</th>
					<th rowspan='2'>Ghi chú</th>
				</tr>
				<tr>
					<th style="width:100px">Theo chứng từ</th>
					<th>Thực xuất</th>
				</tr>
				<?php 
				$details = $this->db->query("select a.Title,a.MaSP,a.Donvi,b.* from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID=$data->ID")->result();
				$i=1;
				if(count($details)>0){
					foreach($details as $row){
						$giaban = $row->Price+$row->PriceDown;
    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Donvi</td>";
						echo "<td>$row->Amount</td>";
						echo "<td>$row->Amount</td>";
						echo "<td>".number_format($row->Price,2)."</td>";
						echo "<td>".number_format($row->Total)."</td>";
						echo "<td>$row->Khuyenmai</td>";
						echo "</tr>";
						$i++;
					}
				}
				$k = $i<10 ? 10-$i : 0 ;
				for ($i=0; $i < $k; $i++) {
					echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				}

				$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
				$tongtienhang = $data->Total - $data->Chietkhau;
		    	$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi;
				?>
				
				<tr>
					<td></td>
					<td>TỔNG CỘNG <br>GIÁ TRỊ CHIẾT KHẤU<br>TỔNG CỘNG SAU CHIẾT KHẤU<br>CHI PHÍ VẬN CHUYỂN</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
					<?php echo number_format($data->Total) ?><br>
					<?php echo number_format($data->Chietkhau) ?><br>
					<?php echo number_format($tongtienhang) ?><br>
					<?php echo number_format($data->Chiphi) ?><br>
					</td>
					<td></td>
				</tr>
			</table>
		</div>

		<div class="block4">
			<div>
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
				<div><?php echo $this->user->FirstName." ".$this->user->LastName ?></div>
				<p><?php echo $data->FirstName." ".$data->LastName ?></p>
				<p><?php echo date("H:i A d/m/Y",strtotime($data->Ngaydathang)) ?></p>
				<?php 
				$kho_accept = $this->db->query("select a.Thoigian from ttp_report_orderhistory a,ttp_user b where a.UserID=b.ID and b.UserType=2 and a.OrderID=".$data->ID." order by a.ID DESC")->row();
				if($kho_accept){
					echo "<p>Kho xác nhận</p><p>".date("H:i A d/m/Y",strtotime($kho_accept->Thoigian))."</p>";
				}
				?>
				<p>In từ tools.ttp.net.vn <br><?php echo date("H:i A d/m/Y",time()) ?></p>
			</div>
			<div>
				<p>Người nhận hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Người giao hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Thủ kho</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Kế toán trưởng</p>
				<p>(Ký, họ tên)</p>
				<p><img style="height:78px" src='public/admin/images/signature.jpg' /></p>
				<p style='margin-top:5px'>Đ.N. Nhật Thảo</p>
			</div>
			<div>
				<p>Giám đốc</p>
				<p>(Ký, họ tên)</p>
				<div>Trần Quốc Dũng</div>
			</div>
		</div>
	</div>
</div>
<style>body{background:#FFF;}</style>
<script>
	$("#hinhthucxuatkho").change(function(){
		var hinhthucxuatkho = $("#hinhthucxuatkho").val();
		$.ajax({
        	url: "<?php echo $base_link ?>get_next_warehouse",
            dataType: "html",
            type: "POST",
            context:this,
            data: "Hinhthucxuatkho="+hinhthucxuatkho+"&OrderID=<?php echo $data->ID ?>",
            success: function(result){
            	$("#next_MaXK").html(result);
            }
        });
	});

	$("#print_page").click(function(){
		$(this).addClass("saving");
		var TKNO 	= $("#TKNO").val();
		var TKCO 	= $("#TKCO").val();
		var KPP 	= $("#KPP").val();
		var Lydo 	= $("#lydoxuatkho").val();
		var KhoID 	= $("#KhoID").val();
		var hinhthucxuatkho 	= $("#hinhthucxuatkho").val();
		if(TKNO=="" || TKCO=="" || KPP=="" || Lydo=='' || KhoID=="" || hinhthucxuatkho==""){
			$(this).removeClass("saving");
			alert("Vui lòng điền đầy đủ các thông tin cần thiết trước khi lưu và in phiếu xuất kho !");
			return false;
		}else{
			$.ajax({
	        	url: "<?php echo $base_link ?>print_export_warehouse",
	            dataType: "html",
	            type: "POST",
	            context:this,
	            data: "OrderID=<?php echo $data->ID ?>&TKNO="+TKNO+"&TKCO="+TKCO+"&KPP="+KPP+"&Lydo="+Lydo+"&KhoID="+KhoID+"&hinhthucxuatkho="+hinhthucxuatkho,
	            success: function(result){
	            	$(this).removeClass("saving");
	            	if(result=="OK"){
	            		window.print();
	            	}else{
	            		alert("Vui lòng kiểm tra lại đường truyền !");
	            	}
	            }
	        });
		}
	});

	$("#save_page").click(function(){
		$(this).addClass("saving");
		var TKNO 	= $("#TKNO").val();
		var TKCO 	= $("#TKCO").val();
		var KPP 	= $("#KPP").val();
		var Lydo 	= $("#lydoxuatkho").val();
		var KhoID 	= $("#KhoID").val();
		var hinhthucxuatkho 	= $("#hinhthucxuatkho").val();
		if(TKNO=="" || TKCO=="" || KPP=="" || Lydo=='' || KhoID=="" || hinhthucxuatkho==""){
			$(this).removeClass("saving");
			alert("Vui lòng điền đầy đủ các thông tin cần thiết trước khi lưu và in phiếu xuất kho !");
			return false;
		}else{
			$.ajax({
	        	url: "<?php echo $base_link ?>print_export_warehouse",
	            dataType: "html",
	            type: "POST",
	            context:this,
	            data: "OrderID=<?php echo $data->ID ?>&TKNO="+TKNO+"&TKCO="+TKCO+"&KPP="+KPP+"&Lydo="+Lydo+"&KhoID="+KhoID+"&hinhthucxuatkho="+hinhthucxuatkho,
	            success: function(result){
	            	$(this).removeClass("saving");
	            	location.reload();
	            }
	        });
		}
	});

	$(document).on('keydown', function(e) {
	    if(e.ctrlKey && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
	        e.cancelBubble = true;
	        e.preventDefault();
	        e.stopImmediatePropagation();
	    }  
	});
</script>