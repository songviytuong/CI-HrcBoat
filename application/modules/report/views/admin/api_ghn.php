<div class="containner">
<table border="0" cellpadding="20" cellspacing="10" width="100%">
    <tbody><tr>
            <td style="padding: 5px;" align="center" valign="middle">
               
                <table cellpadding="0" border="0" width="100%" class="spaceTop5px">
                    <form method="get" name="frmPage" id="frmPage">
                        <input type="hidden" name="pickupHubID"  id="pickupHubID" />
                        <h2>Tạo vận đơn GIAO HÀNG NHANH</h2>
                        <table width="100%">
                            <tr>
                                <td width="48%" valign="top">
                                    <h2 class="para">Thông tin giao hàng</h2>
                                    <div class="col-xs">
                                        <label for="PickHubID">Địa chỉ lấy hàng</label>
                                        <select name="PickHubID" id="PickHubID" class="form-control">
                                            <?php 
                                            foreach($PickHub as $data){
                                            ?>
                                                <option rel="<?=$data["PickHubID"];?>" value="<?=$data["DistrictCode"];?>"><?=$data["DistrictName"];?> - <?=$data["Address"];?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    
                                </td>
                                <td width="4%">&nbsp;</td>
                                <td width="48%">
                                    <h2 class="para">Thông tin nhận hàng</h2>
                                    <div class="col-xs">
                                        <label for="DeliveryAddress">Địa chỉ giao hàng</label>
                                        <input class="form-control" name="DeliveryAddress" id="DeliveryAddress" placeholder="246/9 Bình Quới, Thanh Đa" value="123" />
                                    </div>
                                    <div class="col-xs">
                                        <label for="DeliveryDistrictCode">Mã Quận giao hàng</label>
                                        <select name="DeliveryDistrictCode" id="DeliveryDistrictCode" class="form-control">
                                            <option value="-1">Chọn quận giao hàng</option>
                                            <?php foreach($District as $data){?>
                                                <option value="<?=$data["DistrictCode"];?>"><?=$data["ProvinceName"];?> - <?=$data["DistrictName"];?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-xs">
                                        <label for="ServiceID">Thời gian vận chuyển</label>
                                        <select name="ServiceID" id="ServiceID" class="form-control">
                                            <option value="-1">-------------</option>
                                            <?php foreach($Service as $key => $value){?>
                                                <option value="<?=$key;?>"><?=$value;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="Weight">Trọng lượng</label>
                                        <input class="form-control" name="Weight" id="Weight" value="300" />
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="Length">Dài (cm)</label>
                                        <input class="form-control" name="Length" id="Length" value="20" />
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="Width">Rộng (cm)</label>
                                        <input class="form-control" name="Width" id="Width" value="10" />
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="Height">Cao (cm)</label>
                                        <input class="form-control" name="Height" id="Height" value="10" />
                                    </div>
                                    <div class="col-xs-2">
                                        <label for="btn-fee">&nbsp;</label>
                                        <button type="button" id="btn-fee" id="btn-fee" class="btn btn-primary">Tính phí vận chuyển</button>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-xs-4">
                                        <label for="Fee">Phí vận chuyển</label>
                                        <input class="form-control" id="Fee" name="Fee" placeholder="Chưa tính" value="" />
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="Fee">Có tính phí</label>
                                        <input type="radio" class="form-control" id="isChargeFee1" name="isChargeFee" value="1" checked="checked" />
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="Fee">Không tính phí</label>
                                        <input type="radio" class="form-control" id="isChargeFee2" name="isChargeFee" value="0" />
                                    </div>
                                    <div class="clearfix" style="border-top:1px solid #CCCCCC;">&nbsp;</div>
                                    <div class="col-xs-4">
                                        <label for="CODAmountPre">Tiền thu người nhận dự kiến</label>
                                        <input class="form-control bfh-number" name="CODAmountPre" id="CODAmountPre" placeholder="12,000,000" value="1000000" />
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="CODAmountPercent">Trọng số</label>
                                        <input class="form-control bfh-number" name="CODAmountPercent" id="CODAmountPercent" placeholder="" value="1" />
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="btn-fee-cod">&nbsp;</label><br />
                                        <button type="button" id="btn-fee-cod" id="btn-fee-cod" class="btn btn-primary">Tính phí thu người nhận</button>
                                    </div>

                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-xs">
                                        <label for="CODAmount">Tiền thu người nhận</label>
                                        <input class="form-control bfh-number" name="CODAmount" id="CODAmount" placeholder="12,000,000" value="1000000" />
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-xs">
                                        <label for="CODAmount">Hình thức thanh toán</label>                                        
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="paymentMethod1">Chuyển khoản</label>
                                        <input type="radio" class="form-control" id="paymentMethod1" name="paymentMethod" value="0" />
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="paymentMethod2">Giao hàng nhận tiền</label>
                                        <input checked="checked" type="radio" class="form-control" id="paymentMethod2" name="paymentMethod" value="1" />
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="col-xs">
                                        <label for="RecipientName">Họ tên người nhận hàng</label>
                                        <input class="form-control" name="RecipientName" id="RecipientName" placeholder="Nguyễn Văn A" value="Thanh Bình" />
                                    </div>
                                    <div class="col-xs">
                                        <label for="RecipientPhone">Điện thoại người nhận hàng</label>
                                        <input class="form-control" id="RecipientPhone" name="RecipientPhone" placeholder="0909123456" value="0989466466" />
                                    </div>
                                    <div class="col-xs">
                                        <label for="invoice">Thông tin xuất hóa đơn</label>
                                        <textarea class="form-control" id="invoice" name="invoice"></textarea>
                                    </div>
                                    <div class="col-xs">
                                        <label for="ContentNote">Ghi chú</label>
                                        <textarea class="form-control" id="ContentNote" name="ContentNote"></textarea>
                                    </div>
                                    <div class="col-xs">
                                        <p>&nbsp;</p>
                                        <button type="button" id="complete" class="btn btn-lg btn-success">Hoàn tất</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </table>
            </td>
        </tr>
    </tbody></table>
<div id="ddelete" style="display:none;"></div>
<div id="dmyorder" style="display:none"></div>
<script type="text/javascript">
    function convertMoney(str) {
        return str.replace(/,/g, "");
    }
    $().ready(function(){
        $("#DeliveryDistrictCode").change(function(){
            var fromDistrict = $("#PickHubID").val();
            var toDistrict = $(this).val();
            var urlGetServiceList = "<?php echo base_url().ADMINPATH.'/report/api_ghn' ?>/getServiceList?FromDistrictCode="+fromDistrict+"&ToDistrictCode="+toDistrict;
            $.post(urlGetServiceList, function(resp){
                
                if (resp.ErrorMessage!=null || resp.ErrorMessage!="") {
                    var htmlServiceList = "";
                    $.each(resp.Services, function(i, obj){
                        var str = "<option value='"+obj.ShippingServiceID+"'>"+obj.Name+"</option>";
                        htmlServiceList += str;
                    });
                }
                if (htmlServiceList!="") {
                    $("#ServiceID").html(htmlServiceList);                
                }
            }, "json");
        });
        $("#CODAmount").keyup(function(){
            var price = this.value;
            price = price.replace(/,/gi,"");
            price = price + ".";
            price = price.replace(/\d(?=(\d{3})+\.)/g, '$&,');
            var sprice = price.split(".");
            $(this).val(sprice[0]);
        });
        $("#Fee").keyup(function(){
            var price = this.value;
            price = price.replace(/,/gi,"");
            price = price + ".";
            price = price.replace(/\d(?=(\d{3})+\.)/g, '$&,');
            var sprice = price.split(".");
            $(this).val(sprice[0]);
        });
        $("#CODAmountPre").keyup(function(){
            var price = this.value;
            price = price.replace(/,/gi,"");
            price = price + ".";
            price = price.replace(/\d(?=(\d{3})+\.)/g, '$&,');
            var sprice = price.split(".");
            $(this).val(sprice[0]);
        });
        $("#btn-fee-cod").click(function(){
            var preCOD = $("#CODAmountPre").val();
            preCOD = convertMoney(preCOD);
            var percent = $("#CODAmountPercent").val();
            var finalCOD = preCOD*percent;
            $("#CODAmount").val(finalCOD).keyup();
        });
        $("#btn-fee").click(function(){
            var url = "<?php echo base_url().ADMINPATH.'/report/api_ghn' ?>/calculateFee";
            $.get(url, $("#frmPage").serialize(), function(resp){
                
                $("#Fee").val("Chưa tính");
                var err_msg = resp.ErrorMessage;
                var fee = resp.Items[0].ServiceFee;
                console.log(resp);
                console.log(url);
                if (err_msg == null || err_msg == "") {
                    $("input[name='Fee']").val(fee);
//                    $("#Fee").keyup();
//                    $("#Fee").scroll();
                } else {
                    alert(err_msg);
                }
            }, "json");
        });
        
        $("#complete").click(function(){
            var pickupHubID = $("#PickHubID :selected").attr("rel");
            $("#pickupHubID").val(pickupHubID);
            var soURL = "<?php echo base_url().ADMINPATH.'/report/api_ghn' ?>/createSo";
            $.get(soURL, $("#frmPage").serialize(), function(resp){
                var err = resp.ErrorMessage;
                if (err!=null && err != "") {
                    alert(err);
                } else {
                    alert("Tạo vận đơn thành công!");
                }
            },"json");
        });
    });

</script>
</div>