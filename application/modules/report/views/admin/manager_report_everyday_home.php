<div class="containner">
		<?php 
		$current_month = $this->session->userdata('month_syn');
		?>
		<select id='month_syn' style="padding:4px 10px">
			<option value=''>-- Chọn tháng --</option>
			<?php 
			$month_list = $this->db->query("select DISTINCT MONTH(Ngaydathang) as Month,YEAR(Ngaydathang) as Year from ttp_report_order order by Ngaydathang ASC")->result();
			if(count($month_list)>0){
				foreach($month_list as $row){
					$selected = $current_month==$row->Month.'/'.$row->Year ? "selected='selected'" : '' ;
					echo "<option value='$row->Month/$row->Year' $selected>$row->Month / $row->Year</option>";
				}
			}
			?>
		</select>
		<button class="btn btn-success" onclick="savedata()" style="color:#FFF;padding:5px 10px;float: right;margin-bottom: 15px;"><i style='margin-right:5px;color:#FFF' class="fa fa-refresh"></i> Synchronize</button>
		<div class="table_data">
			<table>
				<tr>
					<td style='padding:3px 5px;border:1px solid #E1e1e1'>Ngày đồng bộ báo cáo</td>
					<td style='padding:3px 5px;border:1px solid #E1e1e1'>Trạng thái đồng bộ</td>
				</tr>
			<?php 
			$arr_month = array();
			$where = "";
			if($current_month==''){
				$where = "MONTH(Ngaydathang)=MONTH(CURDATE()) and YEAR(Ngaydathang)=YEAR(CURDATE())";
			}else{
				$temp = explode('/',$current_month);
				$where = "MONTH(Ngaydathang)=".$temp[0]." and YEAR(Ngaydathang)=".$temp[1];
			}
			$result = $this->db->query("select DISTINCT DATE(Ngaydathang) as Ngaydathang from ttp_report_order where $where order by Ngaydathang ASC")->result();
			$i=1;
			foreach($result as $row){
				echo "<tr>
				<td style='padding:3px 5px;border:1px solid #E1e1e1'><input type='hidden' class='Ngaydathang$i' value='$row->Ngaydathang' />$row->Ngaydathang</td>
				<td style='padding:3px 5px;border:1px solid #E1e1e1'><span class='status$i'>Waiting...<span></td>
				</tr>";
				$i++;
			}
			?>
			</table>
		</div>
</div>
<style>
	table{border-collapse: collapse;width:100%;}
	table tr td input[type="text"]{width:100%; padding:3px 5px;border:none;}
</style>
<script>
	
	$("#month_syn").change(function(){
		var data = $(this).val();
		$.ajax({
        	url: "<?php echo base_url().ADMINPATH.'/report/manager_report_everyday/set_month' ?>",
            dataType: "html",
            type: "POST",
            data: "month="+data,
            success: function(result){
            	location.reload();
            }
        });
	});

	function savedata(){
		send_post(1);
	}

	function send_post(num){
        day = $(".Ngaydathang"+num).val();
        tempnum = num-1;
        $(".status"+tempnum).html('<span style="color:#090">Sync successfull !</span>');
        if(num==<?php echo count($result)+1 ?>){
            return false;
        }
        $.ajax({
            url: "<?php echo base_url().ADMINPATH.'/report/manager_report_everyday/sync' ?>",
            dataType: "html",
            type: "POST",
            data: "day="+day,
            beforeSend: function(){
                $(".status"+num).html('<i class="fa fa-refresh fa-spin"></i> Syncing...');
            },
            success: function(result){
                
            }
        }).always(function(){
            send_post(++num);
        });
    }
</script>
