<?php 
	$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi,c.ShipmentCode as Shipment from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=".$data->OrderID)->result();
	if(count($details)>0){
		$import_history = $this->db->query("select * from ttp_report_inventory_import_details where ShipmentID in(select ShipmentID from ttp_report_orderdetails where OrderID=$data->OrderID) group by ProductsID,ShipmentID")->result_array();
		$arr_import = array();
		if(count($import_history)>0){
			foreach($import_history as $row){
				$arr_import[$row['ShipmentID'].'_'.$row['ProductsID']] = $row;
			}
			
		}
		echo "<table>
		<tr>
			<th>Mã SP</th>
		 	<th>Tên SP</th>
		 	<th>Đơn vị tính</th>
		 	<th>Lô</th>
		 	<th>Số lượng</th>
		 	<th>Đơn giá</th>
		 	<th>Giá trị CK</th>
		 	<th>Giá sau CK</th>
		 	<th>Thành tiền</th>
		 	<th>Kiểm tra</th>
		 	<th style='text-align:center'><i class='fa fa-level-down'></i></th>
		</tr>";
		foreach($details as $row){
			$row->ValueCurrency = isset($arr_import[$row->ShipmentID.'_'.$row->ProductsID]) ? $arr_import[$row->ShipmentID.'_'.$row->ProductsID]['ValueCurrency'] : 1 ;
			$row->PriceCurrency = isset($arr_import[$row->ShipmentID.'_'.$row->ProductsID]) ? $arr_import[$row->ShipmentID.'_'.$row->ProductsID]['PriceCurrency'] : 1 ;
			$row->TotalCurrency = isset($arr_import[$row->ShipmentID.'_'.$row->ProductsID]) ? $arr_import[$row->ShipmentID.'_'.$row->ProductsID]['TotalCurrency'] : 1 ;
			$row->TotalVND = isset($arr_import[$row->ShipmentID.'_'.$row->ProductsID]) ? $arr_import[$row->ShipmentID.'_'.$row->ProductsID]['TotalVND'] : 1 ;
			$row->Currency = isset($arr_import[$row->ShipmentID.'_'.$row->ProductsID]) ? $arr_import[$row->ShipmentID.'_'.$row->ProductsID]['Currency'] : 'VND' ;
			$price = $row->Price+$row->PriceDown;
			echo "<tr>";
			echo "<td>$row->MaSP <input type='hidden' name='ProductsID[]' value='$row->ProductsID' /></td>";
			echo "<td>$row->Title </td>";
			echo "<td>$row->Donvi <input name='Currency[]' class='Currency_input' style='display:none' value='$row->Currency' /> <input type='hidden' name='ValueCurrency[]' value='$row->ValueCurrency' class='ValueCurrency_input' /></td>";
			echo "<td>$row->Shipment <input type='hidden' name='ShipmentID[]' value='$row->ShipmentID' /></td>";
			echo "<td style='width:80px'><input type='text' name='Amount[]' value='$row->Amount' onchange='changerow(this)' /></td>";
			echo "<td>$price</td>";
			echo "<td>$row->PriceDown</td>";
			echo "<td>$price <input type='hidden' name='PriceCurrency[]' value='$row->PriceCurrency' class='PriceCurrency_input' onchange='changerow(this)' required /></td>";
			echo "<td>$row->Total <input type='hidden' name='TotalCurrency[]' value='$row->TotalCurrency' class='TotalCurrency_input' onchange='changerow(this)' required /> <input type='text' name='TotalVND[]' value='$row->TotalVND' style='display:none' class='Total_input' /></td>";
			echo '<td><i class="fa fa-check-circle unchecked"></i></td>';
			echo "<td><a onclick='deleterow(this)' style='margin:0px;padding:0px;'><i class='fa fa-times' style='color: #555;'></i></a></td>";
			echo "</tr>";
		}
		echo "</table>";
	}
?>
