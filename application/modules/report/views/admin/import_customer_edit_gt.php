<div class="containner">
	<div class="manager">
		<form action="<?php echo $data->Type==1 ? $base_link.'update_gt' : $base_link.'update_mt' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÔNG TIN ĐỐI TÁC KÊNH GT</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save</button>
				</div>
			</div>
			<div class="row">
				<table class="customer_table">
					<tr>
						<td>Kênh khách hàng</td>
						<td colspan="8">
							<input type="radio" value="1" name="Type" checked="checked" style="margin-left:20px" /> GT 
							<input type="radio" value="2" name="Type" style="margin-left:20px" disabled="true" /> MT 
							<input type="radio" value="4" name="Type" style="margin-left:20px" disabled="true" /> TD 
							<input type="radio" value="5" name="Type" style="margin-left:20px" disabled="true" /> NB 
						</td>
					</tr>
					<tr>
						<td>Mã đối tác</td>
						<td><input type='text' name='Code' class='required' value='<?php echo $data->Code ?>' required /></td>
						<td style="text-align:right;padding-right:10px">Tên đối tác</td>
						<td colspan="6"><input type='text' name='Name' class='required' value='<?php echo $data->Name ?>' required /></td>
					</tr>
					<tr>
						<td>Mã số thuế</td>
						<td><input type='text' name='Taxcode' value='<?php echo $data->Taxcode ?>' /></td>
						<td style='text-align:right;padding-right:10px'>Loại KH</td>
						<td>
							<select name="SystemID">
								<option value="0" <?php echo $data->SystemID==0 ? "selected='selected'" : '' ; ?>>Nhà phân phối</option>
								<option value="1" <?php echo $data->SystemID==1 ? "selected='selected'" : '' ; ?>>Đại lý</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Khu vực</td>
						<td>
							<select name="AreaID" id="Khuvuc">
								<option value='0'>-- Chọn khu vực --</option>
								<?php 
								$area = $this->db->query("select * from ttp_report_area")->result();
								if(count($area)>0){
									foreach($area as $row){
										$selected = $row->ID==$data->AreaID ? "selected='selected'" : '' ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
							</select>
						</td>
						<td>Tỉnh thành</td>
						<td>
							<select name="CityID" id="Tinhthanh">
								<?php 
								$city = $this->db->query("select ID,Title from ttp_report_city where ID=$data->CityID")->row();
								echo $city ? "<option value='$data->CityID'>$city->Title</option>" : "<option value='0'>-- Chọn tỉnh thành --</option>" ;
								?>
							</select>
						</td>
						<td style="text-align:right;padding-right:10px">Quận huyện</td>
						<td>
							<select name="DistrictID" id="Quanhuyen">
								<?php 
								$district = $this->db->query("select ID,Title from ttp_report_district where ID=$data->DistrictID")->row();
								echo $city ? "<option value='$data->DistrictID'>$district->Title</option>" : "<option value='0'>-- Chọn quận huyện --</option>" ;
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Khu vực phân phối</td>
						<td colspan="8"><input type="text" name="AreaNote" value='<?php echo $data->AreaNote ?>' /></td>
					</tr>
 					<tr>
						<td>Địa chỉ đăng ký</td>
						<td colspan="8">
							<input type='text' name='Address' class="required" value='<?php echo $data->Address ?>' required />
						</td>
					</tr>
					<tr>
						<td>Địa chỉ giao hàng</td>
						<td colspan="8">
							<input type='text' name='AddressOrder' class="required" value='<?php echo $data->AddressOrder ?>' required />
						</td>
					</tr>
					<tr>
						<td>Ghi chú</td>
						<td colspan="8"><input type='text' name="Note" value='<?php echo $data->Note ?>' /></td>
					</tr>
					<tr>
						<td>Người đại diện</td>
						<td><input type='text' name="Surrogate" class="required" value='<?php echo $data->Surrogate ?>' required /></td>
						<td style="text-align:right;padding-right:10px">Số di động</td>
						<td><input type='text' name="Phone1" value='<?php echo $data->Phone1 ?>' class="required" onchange="checkphone(this)" required /></td>
						<td style="text-align:right;padding-right:10px">Email</td>
						<td><input type='text' name="Email" value='<?php echo $data->Email ?>' placeholder="example@example.com.vn" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 780px !important;}
	.body_content .customer_table{width:100%;border-collapse: collapse;background:none !important;margin-top:10px;}
	.body_content .customer_table tr td{padding:7px 0px;}
	.body_content .customer_table tr td:first-child{width:120px;}
	.body_content .customer_table tr td:nth-child(2){width:200px;}
	.body_content .customer_table tr td:nth-child(3){width:100px;}
	.body_content .customer_table tr td input[type="text"]{width:100%; border:1px solid #c1c1c1;padding:4px 5px;height:32px;}
	.body_content .customer_table tr td select{height:30px;width:100%; border:1px solid #c1c1c1;padding:4px 5px;}
	.body_content .showbox_history table{width:100%;border-collapse: collapse;border:1px solid #E1e1e1;margin-top:10px;}
	.body_content .showbox_history table tr th{padding:5px;text-align:left;border-bottom:1px solid #E1e1e1;}
	.body_content .showbox_history table tr td{padding:5px;text-align:left;border-bottom:1px solid #E1e1e1;}
	.view_history{margin-top:10px;}
	.view_history a{color:#1A82C3;text-decoration: underline;border:none !important;padding:0px !important;margin-right:20px;}
	.view_history a i{font-size: 12px;margin-left:6px;}
	.add_advisory{clear:both;margin-top:10px;}
	.add_advisory h1{font-size: 22px;padding: 10px 0px;text-transform: uppercase;font-weight: bold;margin-bottom: 10px}
	.add_advisory textarea{width:100%;border:1px solid #c1c1c1;height:80px;padding:5px;margin-top:5px;}
	.daterangepicker{width: auto;}
</style>
<script>
	function checkphone(ob){
		var data = $(ob).val();
		if(data!=''){
			$.ajax({
	        	url: "<?php echo base_url().ADMINPATH.'/report/import_customer/checkphone' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "data="+data,
	            success: function(result){
	            	if(result=='false'){
	            		alert("Số điện thoại bạn vừa nhập đã có người sử dụng . Vui lòng thay đổi số dt khác .");
	            		$(ob).val('').focus();
	            	}
	            }
	        });
        }else{
        	alert("Số dt không được để trống !!");
        	$(ob).focus();
        }
	}

	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_city_by_area' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_district_by_city' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            }
	        });
		}
	});
</script>