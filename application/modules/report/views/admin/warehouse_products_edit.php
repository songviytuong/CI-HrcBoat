<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">
	<?php 
	$currenttab = isset($_GET['tab']) ? $_GET['tab'] : 'tab1' ;
	?>
	<div class="products_containner">
		<form action="<?php echo base_url().ADMINPATH."/report/warehouse_products/update" ?>" method="POST" id='products_form' enctype='multipart/form-data'>
			<div><input type='hidden' id="IDEdit" name="ID" value='<?php echo $data->ID ?>' /></div>
			<div class="head_products">
				<div class="block1">
					<h1>THÔNG TIN SẢN PHẨM</h1>
				</div>
				<div class="block2">
					<span>Loại sản phẩm : </span>
					<?php 
					switch ($data->VariantType) {
						case 0:
							echo "<span>Sản phẩm đơn</span>";
							break;
						case 1:
							echo "<span>Sản phẩm biến thể</span>";
							break;
						case 2:
							echo "<span>Sản phẩm Bundle</span>";
							break;
						case 3:
							echo "<span>Sản phẩm nhóm</span>";
							break;
						default:
							break;
					}
					echo "<input type='hidden' name='VariantType' value='$data->VariantType' />";
					?>
					<!--<select name="VariantType" id="VariantType">
						<option value="0" <?php echo $data->VariantType==0 ? "selected='selected'" : '' ; ?>>Sản phẩm đơn</option>
						<option value="1" <?php echo $data->VariantType==1 ? "selected='selected'" : '' ; ?>>Sản phẩm biến thể</option>
						<option value="2" <?php echo $data->VariantType==2 ? "selected='selected'" : '' ; ?>>Sản phẩm Bundle</option>
						<option value="3" <?php echo $data->VariantType==3 ? "selected='selected'" : '' ; ?>>Sản phẩm nhóm</option>
					</select>-->
				</div>
				<div class="block3">
					<a class="btn btn-danger delete" href="<?php echo base_url().ADMINPATH.'/report/warehouse_products/delete/'.$data->ID ?>"><i class="fa fa-trash-o"></i> Xóa</a>
					<a class="btn btn-success" href="<?php echo base_url().ADMINPATH.'/report/warehouse_products/dupplicate/'.$data->ID ?>"><i class="fa fa-files-o"></i> Sao chép</a>
					<button class="btn btn-primary" type="submit"><i class="fa fa-check-square-o" aria-hidden="true"></i> Lưu</button>
					<a class="btn btn-info" onclick="saveall(this)"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Lưu & đóng</a>
				</div>
			</div>
			<div class="content_products">
				<div class="control_tab">
					<a data="tab1" <?php echo $currenttab=='tab1' ? 'class="current"' : '' ; ?>>Thông tin chung</a>
					<a data="tab2" <?php echo $currenttab=='tab2' ? 'class="current"' : '' ; ?>>Giá bán</a>
					<a data="tab3" <?php echo $currenttab=='tab3' ? 'class="current"' : '' ; ?>>Website</a>
					<a data="tab4" <?php echo $currenttab=='tab4' ? 'class="current"' : '' ; ?>>Hình ảnh</a>
					<a data="tab11" <?php echo $currenttab=='tab11' ? 'class="current"' : '' ; ?>>Video</a>
					<a data="tab5" <?php echo $currenttab=='tab5' ? 'class="current"' : '' ; ?>>Tồn kho</a>
					<a data="tab6" <?php echo $currenttab=='tab6' ? 'class="current"' : '' ; ?>>Ngành hàng</a>
					<a data="tab7" <?php echo $currenttab=='tab7' ? 'class="current"' : '' ; ?>>Thuộc tính</a>
					<a data="tab8" <?php echo $currenttab=='tab8' && $data->VariantType==1 ? 'class="current"' : '' ; ?> <?php echo $data->VariantType==1 ? "style='display:inline-block'" : "style='display:none'" ; ?>>Biến thể</a>
					<a data="tab9" <?php echo $currenttab=='tab9' ? 'class="current"' : '' ; ?>>Dịch vụ quà tặng</a>
					<a data="tab10" <?php echo $currenttab=='tab10' && $data->VariantType==2 ? 'class="current"' : '' ; ?> <?php echo $data->VariantType==2 ? "style='display:inline-block'" : "style='display:none'" ; ?>>Gom nhóm</a>
				</div>
				<!-- Thông tin chung -->
				<div class="content_tab content_tab1" <?php echo $currenttab=='tab1' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Mã sản phẩm
								</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="MaSP" value='<?php echo $data->MaSP ?>' />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Loại ngành hàng
								</label>
								<div class="col-xs-8">
									<select name="TypeProducts" class="form-control">
										<option value='2' <?php echo $data->TypeProducts==2 ? 'selected="selected"' : '' ; ?>>Gốm sứ</option>
										<option value='1' <?php echo $data->TypeProducts==1 ? 'selected="selected"' : '' ; ?>>Mỹ phẩm</option>
										<option value='0' <?php echo $data->TypeProducts==0 ? 'selected="selected"' : '' ; ?>>Khác</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">
									Tên sản phẩm
								</label>
								<div class="col-xs-10">
									<input type="text" class="form-control special_input" name="Title" value='<?php echo $data->Title ?>' />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">
									Mô tả ngắn
								</label>
								<div class="col-xs-10">
									<input type="text" class="form-control special_input" name="Description" value='<?php echo $data->Description ?>' />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Trọng lượng (kg)
								</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="Weight" value='<?php echo $data->Weight ?>' />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label text-center">
									Kích thước (cm)
								</label>
								<div class="col-xs-2">
									<div class="input-group">
										<input type="text" class="form-control" name="Length" placeholder="Dài" value='<?php echo $data->Length ?>' />
									</div>
								</div>
								<div class="col-xs-2">
									<div class="input-group">
										<input type="text" class="form-control" name="Width" placeholder="Rộng" value='<?php echo $data->Width ?>' />
									</div>
								</div>
								<div class="col-xs-2">
									<div class="input-group">
										<input type="text" class="form-control" name="Height" placeholder="Cao" value='<?php echo $data->Height ?>' />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Là SP mới từ ngày
								</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="Startday" id="Input_Startday" placeholder="yyyy-mm-dd" value='<?php echo $data->NewStartDay!='0000-00-00' ? $data->NewStartDay : '' ; ?>' />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Là SP mới đến ngày
								</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="Stopday" id="Input_Stopday" placeholder="yyyy-mm-dd" value='<?php echo $data->NewStopDay!='0000-00-00' ? $data->NewStopDay : '' ; ?>' />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Tình trạng
								</label>
								<div class="col-xs-8">
									<select name="Published" class="form-control">
										<option value="1" <?php echo $data->Published==1 ? "selected='selected'" : "" ; ?>>Kích hoạt</option>
										<option value="0" <?php echo $data->Published==0 ? "selected='selected'" : "" ; ?>>Ngưng kích hoạt</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Cho phép hiển thị
								</label>
								<div class="col-xs-8">
									<select name="Show" class="form-control">
										<option value="1" <?php echo $data->Status==1 ? "selected='selected'" : "" ; ?>>Yes</option>
										<option value="0" <?php echo $data->Status==0 ? "selected='selected'" : "" ; ?>>No</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Nước sản xuất
								</label>
								<div class="col-xs-8">
									<select name="CountryID" class="form-control">
										<?php 
										$country = $this->db->query("select * from ttp_report_country")->result();
										if(count($country)>0){
											foreach($country as $row){
												$selected = $row->ID==$data->CountryID ? "selected='selected'" : '' ;
												echo "<option value='$row->ID'>$row->Title</option>";
											}
										}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Thương hiệu
								</label>
								<div class="col-xs-8">
									<select name="TrademarkID" class="form-control">
									<?php 
										$trademark = $this->db->query("select * from ttp_report_trademark")->result();
										if(count($trademark)>0){
											foreach($trademark as $row){
												$selected = $row->ID==$data->TrademarkID ? "selected='selected'" : '' ;
												echo "<option value='$row->ID' $selected>$row->Title</option>";
											}
										}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Đơn vị tính
								</label>
								<div class="col-xs-8">
									<select name="Donvi" class="form-control">
										<?php 
										$unit = $this->db->query("select * from ttp_report_unit")->result();
										if(count($unit)>0){
											foreach($unit as $row){
												$selected = $data->Donvi==$row->Title ? "selected='selected'" : '' ;
												echo "<option value='$row->Title' $selected>$row->Title</option>";
											}
										}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">Barcode</label>
								<label class="col-xs-8 control-label"><?php echo $data->Barcode ?></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">Image Barcode</label>
								<label class="col-xs-8 control-label"><img src='<?php echo base_url().ADMINPATH.'/report/warehouse_products/set_barcode/'.$data->Barcode ?>' /></label>
							</div>
						</div>
					</div>
				</div>
				<!-- Giá bán -->
				<div class="content_tab content_tab2" <?php echo $currenttab=='tab2' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá bán</label>
								<div class="col-xs-8">
									<input type="text" name="Price" class="form-control" value="<?php echo $data->Price ?>" required />
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá mua</label>
								<div class="col-xs-8">
									<input type="text" name="RootPrice" class="form-control" value="<?php echo $data->RootPrice ?>" required/>
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Thuế VAT</label>
								<div class="col-xs-8">
									<select name="VAT" class="form-control">
										<option value="0" <?php echo $data->VAT==0 ? "selected='selected'" : '' ; ?>>Không có</option>
										<option value="5" <?php echo $data->VAT==5 ? "selected='selected'" : '' ; ?>>5%</option>
										<option value="10" <?php echo $data->VAT==10 ? "selected='selected'" : '' ; ?>>10%</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá đặc biệt</label>
								<div class="col-xs-8">
									<input type="text" name="SpecialPrice" class="form-control" value="<?php echo $data->SpecialPrice ?>" />
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Từ ngày</label>
								<div class="col-xs-8">
									<input type="text" name="Special_startday" class="form-control" id="Input_Special_startday" placeholder="yyyy-mm-dd" value='<?php echo $data->SpecialStartday!='0000-00-00' ? $data->SpecialStartday : '' ; ?>' />
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Đến ngày</label>
								<div class="col-xs-8">
									<input type="text" name="Special_stopday" class="form-control" id="Input_Special_stopday" placeholder="yyyy-mm-dd" value='<?php echo $data->SpecialStopday!='0000-00-00' ? $data->SpecialStopday : '' ; ?>' />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Nhóm giá : </label>
								<label class="col-xs-8 control-label">Nhóm khách hàng</label>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá bán</label>
							</div>
						</div>
					</div>
					<div class="row" id="PriceGroup">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label"></label>
								<label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceGroup_Title[]" /></label>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<label class="col-xs-4 control-label"><input type="text" class="form-control" name="PriceGroup_Price[]" /></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label"></label>
								<a class='btn btn-default' onclick="add_PriceGroup()"><i class="fa fa-plus"></i> Thêm nhóm giá</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Bước giá : </label>
								<label class="col-xs-8 control-label">Nhóm khách hàng</label>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá bán</label>
								<label class="col-xs-1 control-label"></label>
								<label class="col-xs-2 control-label">Số lượng</label>
							</div>
						</div>
					</div>
					<div class="row" id="PriceRange">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label"></label>
								<label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceRange_Title[]" /></label>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<label class="col-xs-4 control-label"><input type="text" name="PriceRange_Price[]" class="form-control" /></label>
								<label class="col-xs-1 control-label"></label>
								<label class="col-xs-1 control-label"><input type="number" min="0" class="form-control" name="PriceRange_Amount[]" value="0" /></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label"></label>
								<a class='btn btn-default' onclick="add_PriceRange()"><i class="fa fa-plus"></i> Thêm bước giá</a>
							</div>
						</div>
					</div>
					
				</div>
				<!-- Thẻ Meta -->
				<div class="content_tab content_tab3" <?php echo $currenttab=='tab3' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Sản phẩm tiêu biểu</label>
								<div class="col-xs-2">
									<select class="form-control" name="Featured">
										<option value="0" <?php echo $data->Featured==0 ? "selected='selected'" : '' ; ?>>Disable</option>
										<option value="1" <?php echo $data->Featured==1 ? "selected='selected'" : '' ; ?>>Enable</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Meta Title</label>
								<div class="col-xs-10">
									<input type="text" name="MetaTitle" class="form-control" value="<?php echo $data->MetaTitle ?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Meta Keywords</label>
								<div class="col-xs-10">
									<input type="text" name="MetaKeywords" class="form-control" value="<?php echo $data->MetaKeywords ?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Meta Description</label>
								<div class="col-xs-10">
									<input type="text" name="MetaDescription" class="form-control" value="<?php echo $data->MetaDescription ?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Hướng dẫn sử dụng sản phẩm</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<textarea class="resizable_textarea form-control ckeditor" name="Instruction"><?php echo $data->Instruction ?></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Nội dung mô tả sản phẩm</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<textarea class="resizable_textarea form-control ckeditor" name="Content"><?php echo $data->Content ?></textarea>
						</div>
					</div>
				</div>
				<!-- Hình ảnh -->
				<div class="content_tab content_tab4" <?php echo $currenttab=='tab4' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<table>
						<tr>
							<td>Hình ảnh</td>
							<td>Nhãn cho ảnh</td>
							<td>Thứ tự</td>
							<td>Ảnh chính</td>
							<td>Ảnh nhỏ</td>
							<td>Ảnh thumbnail</td>
							<td></td>
						</tr>
						<?php 
						$images = $this->db->query("select * from ttp_report_images_products where ProductsID=$data->ID")->result();
						if(count($images)>0){
							$i=0;
							foreach($images as $row){
								$checked_primary = $row->PrimaryImage==1 ? "checked='checked'" : '' ;
								$checked_small = $row->MiniImage==1 ? "checked='checked'" : '' ;
								$checked_thumb = $row->ThumbImage==1 ? "checked='checked'" : '' ;
								echo "<tr class='imagerow_tr imagerow_tr$i' data='$row->ID'>";
								echo file_exists($this->lib->get_thumb($row->Url)) ? "<td><div class='image_row'><img src='".$this->lib->get_thumb($row->Url)."' /> <i class='fa fa-chain-broken'></i> <input type='file' onchange='changeimagerow(this)' data='$row->ID' /><div class='progress'><div class='bar'></div></div></div></td>" : "<td></td>";
								echo "<td><input class='label_tr form-control' type='text' value='$row->Label' /></td>";
								echo "<td><input class='stt_tr form-control' type='number' value='$row->STT' /></td>";
								echo "<td class='text-center'><input $checked_primary class='primaryimage_tr' name='primaryimage_tr' type='radio' value='$row->ID' /></td>";
								echo "<td class='text-center'><input $checked_small class='smallimage_tr' name='smallimage_tr' type='radio' value='$row->ID' /></td>";
								echo "<td class='text-center'><input $checked_thumb class='thumbimage_tr' name='thumbimage_tr' type='radio' value='$row->ID' /></td>";
								echo "<td><a title='Xóa hình ảnh này' onclick='rm_image_products(this,$row->ID)'><i class='fa fa-times'></i></a></td>";
								echo "</tr>";
								$i++;
							}
						}else{
							echo "<tr><td colspan='7'>Không có hình ảnh cho sản phẩm này</td></tr>";
						}
						?>
						<tr>
							<td colspan="7">
							<div class="input-group-btn text-right">
								<a class='btn btn-success last-a' style="display:none"><i class="fa fa-check-circle"></i> Lưu thành công</a>
								<a class='btn btn-default save_update_image' onclick="active_save_image(this,0)"><i class="fa fa-refresh"></i> Lưu cập nhật</a>
								<a class="btn btn-default activeupload" onclick="activeupload(this,'filerow')"><i class="fa fa-upload" aria-hidden="true"></i> Đăng tải hình</a>
								<a class="btn btn-primary btn-file choosefile">
									<i class="fa fa-folder-open" aria-hidden="true"></i> Browse...
									<input type="file" name="Images_upload[]" id="choosefile" multiple />
								</a>
							</div>
							</td>
						</tr>
					</table>
					<div id="dvPreview"></div>
				</div>
				<!-- Video -->
				<?php 
				$video = json_decode($data->Video);
				?>
				<div class="content_tab content_tab11" <?php echo $currenttab=='tab11' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Mã video (Youtube)
								</label>
								<div class="col-xs-8">
									<input type="text" name="YoutubeCode" class="form-control" value="<?php echo isset($video->YoutubeCode) ? $video->YoutubeCode : '' ; ?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									hoặc Upload video
								</label>
								<div class="col-xs-8">
									<input type="file" name="VideoSource" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<label class="col-xs-4 control-label">
								Hình đại diện video
							</label>
							<div class="col-xs-8">
								<input type="file" name="ImageVideo" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<div align="center" class="embed-responsive embed-responsive-16by9">
								    <video class="embed-responsive-item">
								        <source src="<?php echo isset($video->VideoSource) ? $video->VideoSource : '' ; ?>" type="video/mp4">
								    </video>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<img class="img-responsive" src="<?php echo isset($video->ImageVideo) ? $video->ImageVideo : '' ; ?>" />
						</div>
					</div>
				</div>
				<!-- Tồn kho -->
				<div class="content_tab content_tab5" <?php echo $currenttab=='tab5' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Số lượng hiện tại
								</label>
								<div class="col-xs-8">
									<input type="number" min="0" name="SLCurrent" class="form-control" value="<?php echo $data->CurrentAmount ?>" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									SL báo hết hàng
								</label>
								<div class="col-xs-8">
									<input type="number" min="0" name="SLOutOfStock" class="form-control" value="<?php echo $data->WarningAmount ?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									SL tối thiểu trong giỏ hàng
								</label>
								<div class="col-xs-8">
									<input type="number" min="0" name="SLMinInCart" class="form-control" value="<?php echo $data->MinCartAmount ?>" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									SL tối đa trong giỏ hàng
								</label>
								<div class="col-xs-8">
									<input type="number" min="0" name="SLMaxInCart" class="form-control" value="<?php echo $data->MaxCartAmount ?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Cho phép đặt hàng âm
								</label>
								<div class="col-xs-8">
									<select name="AddcartNegativeNumber" class="form-control">
										<option value="1" <?php echo $data->PutcartNegative==1 ? "selected='selected'" : '' ; ?>>Yes</option>
										<option value="0" <?php echo $data->PutcartNegative==0 ? "selected='selected'" : '' ; ?>>No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Tình trạng tồn kho
								</label>
								<div class="col-xs-8">
									<select name="Available" class="form-control">
										<option value="1" <?php echo $data->InventoryStatus==1 ? "selected='selected'" : '' ; ?>>Còn hàng</option>
										<option value="0" <?php echo $data->InventoryStatus==0 ? "selected='selected'" : '' ; ?>>Hết hàng</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Nhóm ngành hàng -->
				<div class="content_tab content_tab6" <?php echo $currenttab=='tab6' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div>
						<?php 
						$arr_catgories = json_decode($data->CategoriesID,true);
						$arr_catgories = is_array($arr_catgories) ? implode(',',$arr_catgories) : '0';
						$arr_catgories = $arr_catgories=='' ? "(0)" : "($arr_catgories)" ;
						$current_categories = $this->db->query("select ID,Title from ttp_report_categories where ID in $arr_catgories")->result();
						if(count($current_categories)>0){
							echo "<h4>Nhóm ngành hàng đã chọn:</h4>";
							echo "<div class='current_categories'>";
							foreach ($current_categories as $row) {
								echo "<a><input type='checkbox' checked='checked' name='CategoriesID[]' value='$row->ID' /> $row->Title <span><i class='fa fa-times' onclick='remove_categories(this)'></i></span></a>";
							}
							echo "</div>";
						}
						$categories = $this->db->query("select Title,ID from ttp_report_categories where ParentID=0")->result();
						if(count($categories)>0){
							echo "<h4>Chọn thêm ngành hàng:</h4>";
							echo "<ul>";
							foreach($categories as $row){
								echo "<li><span onclick='show_child_next(this)' data='$row->ID'>+</span><input type='checkbox' value='$row->ID' name='CategoriesID[]' /> $row->Title</li>";
							}
							echo "</ul>";
						}
						?>
					</div>
				</div>
				<!-- Thuộc tính -->
				<div class="content_tab content_tab7" <?php echo $currenttab=='tab7' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div class="row">
						<div class="col-xs-12">
							<h4>Danh sách thuộc tính</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<div class="input-group">
								<a class="input-group-addon" onclick="select_properties()"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i> Chọn thuộc tính</a>
								<select id="PropertiesID" class="form-control">
									<?php 
									$properties = $this->db->query("select ID,Title from ttp_report_properties where ParentID=0")->result();
									$arr_name_parent = array();
									if(count($properties) > 0){
										foreach($properties as $row){
											echo "<option value='$row->ID'>$row->Title</option>";
											$arr_name_parent[$row->ID] = $row->Title;
										}
									}
									?>
								</select>
								<span class="input-group-btn">
									<a class="btn btn-default" onclick="senddata('add_properties','Thêm thuộc tính mới','')"><i class="fa fa-plus"></i> Tạo mới</a>
								</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4"><h4>Tên thuộc tính</h4></div>
						<div class="col-xs-8"><h4>Giá trị thuộc tính</h4></div>
					</div>
					<?php 
					$Properties = json_decode($data->VariantValue,true);
					$TemPropertiesID = isset($Properties['Properties']) ? $Properties['Properties'] : array() ;
					$PropertiesID = count($TemPropertiesID)>0 ? '('.implode(',',$TemPropertiesID).')' : '(0)' ;
					$PropertiesPublished = isset($Properties['Published']) ? $Properties['Published'] : array() ;
					$current_parent_properties = $this->db->query("select ID,Title,ParentID from ttp_report_properties where ParentID in (select DISTINCT ParentID from ttp_report_properties where ID in $PropertiesID)")->result();
					$arr_parent = array();
					$arr_parent_js = array();
					if(count($current_parent_properties)>0){
						foreach($current_parent_properties as $row){
							$selected = in_array($row->ID,$TemPropertiesID) ? 'checked="checked"' : '' ;
							if(isset($arr_parent[$row->ParentID])){
								$arr_parent[$row->ParentID] .= "<div class='col-xs-3'><input class='valuekey$row->ParentID' type='checkbox' value='$row->ID' name='PropertiesID[]' $selected /> $row->Title</div>";
							}else{
								$arr_parent_js[] = '"data'.$row->ParentID.'"';
								$arr_parent[$row->ParentID] = "<div class='col-xs-3'><input class='valuekey$row->ParentID' type='checkbox' value='$row->ID' name='PropertiesID[]' $selected /> $row->Title</div>";
							}
						}
					}
					if(count($arr_parent)>0){
						foreach($arr_parent as $key=>$value){
							$checked = in_array($key,$PropertiesPublished) ? "checked='checked'" : '' ;
							echo '<div class="row">
								<div class="col-xs-4 properties_variant"><b>'.$arr_name_parent[$key].'</b> <br> <input type="checkbox" name="PropertiesPublished[]" value="'.$key.'" '.$checked.' /> Có thể nhìn thấy trên trang sản phẩm</div>
								<div class="col-xs-8 value_properties_variant">
									<div class="sub_row valuekey value'.$key.'">
										'.$value.'
									</div>
									<div class="sub_row row input-group-btn">
										<a class="btn btn-default" onclick="check_full_rowtab(this,'.$key.')"><i class="fa fa-check-square-o"></i> Chọn tất cả</a>
										<a class="btn btn-default" onclick="uncheck_full_rowtab(this,'.$key.')"><i class="fa fa-times"></i> Bỏ chọn tất cả</a>
										<a class="btn btn-default" onclick="remove_rowtab(this)"><i class="fa fa-trash-o"></i> Bỏ chọn thuộc tính</a>
										<a class="btn btn-primary" onclick="senddata(\'add_value_properties\',\'Thêm giá trị thuộc tính mới\','.$key.')"><i class="fa fa-plus"></i> Tạo mới giá trị</a>
									</div>
								</div>
							</div>';
						}
					}else{
						echo '<div class="message_empty">Sản phẩm này chưa có thuộc tính . Vui lòng chọn thuộc tính cho sản phẩm này .</div>';
					}
					?>
				</div>
				<!-- Biến thể -->
				<div class="content_tab content_tab8" <?php echo $currenttab=='tab8' && $data->VariantType==1 ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div class="row">
						<a class="btn btn-danger" onclick="AcceptVariant(this,0)"><i class="fa fa-plus"></i> Tạo biến thể mới</a>
					</div>
					<?php 
					$variant_list = $this->db->query("select * from ttp_report_products where ParentID=$data->ID")->result();
					if(count($variant_list)>0){
						foreach($variant_list as $row){
							$this->load->view("warehouse_products_variant_add",array('parentdata'=>$data,'data'=>$row));
						}
					}
					?>
				</div>
				<!-- Dịch vụ quà tặng -->
				<div class="content_tab content_tab9" <?php echo $currenttab=='tab9' ? 'style="display:block"' : 'style="display:none"' ; ?>>
					<div>
						<div>
							<input type='checkbox' name="AcceptGift" <?php echo $data->AcceptGift==1 ? "checked='checked'" : '' ; ?> /> Cho phép gói quà 
						</div>
						<div>
							<p>Chi phí gói quà</p>
							<input type='text' name="SaleGift" value='<?php echo $data->SaleGift ?>' />
						</div>
					</div>
				</div>
				<!-- Gom nhóm sản phẩm bundel -->
				<div class="content_tab content_tab10" <?php echo $currenttab=='tab10' && $data->VariantType==2 ? 'style="display:block;min-height:800px"' : 'style="display:none"' ; ?>>
					<h4>Danh sách sản phẩm thuộc bundle <a class="btn btn-default pull-right" onclick="show_productsbox(this)"><i class="fa fa-plus"></i> Thêm sản phẩm</a></h4>
					<div class="row" id="bundle-list">
						<?php 
						$products_group = $this->db->query("select b.*,a.Quantity,a.ID as BundleID from ttp_report_products_bundle a,ttp_report_products b where Src_ProductsID=$data->ID and a.Des_ProductsID=b.ID")->result();
						if(count($products_group)>0){
							foreach($products_group as $row){
								echo '<div class="col-xs-4">
										<div class="form-group panel panel-default" style="overflow:hidden">
											<div class="col-xs-3"><img class="img-responsive" src="'.$row->PrimaryImage.'" /></div>
											<div class="col-xs-9">
												<p>'.$row->Title.'</p>
												<p class="text-danger">'.number_format($row->Price).'đ</p>
											</div>
											<div class="col-xs-3" style="clear:both"><input class="form-control" type="number" value="'.$row->Quantity.'" onchange="updatequantitybundle(this,'.$row->BundleID.')" /></div>
											<div class="col-xs-9"><a class="pull-right btn btn-default" title="Loại bỏ sản phẩm này ra khỏi bộ bundle" onclick="removeoutbundle(this,'.$row->BundleID.')"><i class="fa fa-times"></i></a></div>
										</div>
									</div>';
							}
						}
						?>
					</div>
				</div>
			</div>
			<input type='hidden' name="currenttab" id="currenttab" value="<?php echo $currenttab ; ?>" />
			<input type='hidden' id="baselink_url" value="<?php echo base_url().ADMINPATH.'/report/warehouse_products/' ; ?>" />
		</form>
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	$(document).ready(function () {
        $('#Input_Startday,#Input_Stopday,#Input_Special_startday,#Input_Special_stopday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
        $(".Variant_StartSpecialPrice,.Variant_StopSpecialPrice").daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	$(".control_tab a").click(function(){
		$(".control_tab a").removeClass("current");
		var data = $(this).attr('data');
		$(this).addClass("current");
		$(".content_tab").hide();
		$(".content_"+data).show();
		$("#currenttab").val(data);
	});

	$("#VariantType").change(function(){
		var value = $(this).val();
		if(value==1){
			$(".control_tab a:nth-child(8)").show();
		}else{
			$(".control_tab a:nth-child(8)").hide();
		}
	});

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function close_PriceGroup(ob){
		$(ob).parent('div').parent('div').remove();
	}

	function add_PriceGroup(){
		$("#PriceGroup").append('<div class="col-xs-4"><div class="form-group"><label class="col-xs-4 control-label"></label><label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceGroup_Title[]" /></label></div></div><div class="col-xs-8"><div class="form-group"><label class="col-xs-4 control-label"><input type="text" class="form-control" name="PriceGroup_Price[]" /></label></div></div></div>');
	}

	function close_PriceRange(ob){
		$(ob).parent('div').parent('div').remove();
	}

	function add_PriceRange(){
		$("#PriceRange").append('<div class="col-xs-4"><div class="form-group"><label class="col-xs-4 control-label"></label><label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceRange_Title[]" /></label></div></div><div class="col-xs-8"><div class="form-group"><label class="col-xs-4 control-label"><input type="text" name="PriceRange_Price[]" class="form-control" /></label><label class="col-xs-1 control-label"></label><label class="col-xs-1 control-label"><input type="number" min="0" class="form-control" name="PriceRange_Amount[]" value="0" /></label></div></div>');
	}

	function check_full(ob){
		if(ob.checked===true){
            $(ob).parent('li').find("input").prop('checked', true);
        }else{
            $(ob).parent('li').find("input").prop('checked', false);
        }
	}

	function check_full_rowtab(ob,key){
		$("input.valuekey"+key).prop('checked', true);
	}

	function uncheck_full_rowtab(ob,key){
		$("input.valuekey"+key).prop('checked', false);
	}

	function show_child(ob){
		var current = $(ob).html();
		if(current==="+"){
			$(ob).html("-");
		}else{
			$(ob).html("+");
		}
		$(ob).parent('li').find("ul").toggle('fast');
	}

	function remove_categories(ob){
		$(ob).parent('span').parent('a').remove();
	}

	var isloaddata = [];

	function show_child_next(ob){
		var ID = $(ob).attr('data');
		var current = $(ob).html();
			if(current==="+"){
				$(ob).html("-");
				if(ID!=''){
					if(jQuery.inArray( "data"+ID, isloaddata )<0){
						$.ajax({
					        url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/get_child_categories' ?>",
					        dataType: "html",
					        type: "POST",
					        data: "ID="+ID,
					        success: function(result){
					        	if(result!='False'){
					        		$(ob).parent('li').append(result);
					        		isloaddata.push("data"+ID);
					        	}else{
					        		$(ob).html("");
					        		$(ob).css({"background":"#FFF","border":"1px solid #FFF"});
					        	}
					        }
					    });	
					}
		        }
			}else{
				$(ob).html("+");
			}
		$(ob).parent('li').find("ul").toggle("fast");
	}

	function senddata(url,title,data){
		enablescrollsetup();
		$(".over_lay .block1_inner h1").html(title);
		$(".over_lay .block2_inner").html("");
		$.ajax({
	        url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/' ?>"+url,
	        dataType: "html",
	        type: "POST",
	        data: "Data="+data,
	        success: function(result){
	        	if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);
        			$(".over_lay").removeClass('in');
        			$(".over_lay").fadeIn('fast');
        			$(".over_lay").addClass('in');
                }else{
                	alert("Không thực hiện được chức năng theo yêu cầu.");
                }
	        }
	    });
	}

	function save_properties(ob){
		$(ob).addClass("saving");
		var form = $(ob).parent('div').parent('div').parent('div').parent('form');
		$.ajax({
	        url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/save_properties' ?>",
	        dataType: "html",
	        type: "POST",
	        data: form.serialize(),
	        success: function(result){
	        	var ar = result.split("|");
                if(ar[0]!='False'){
        			if(ar[0]!=0){
	        			$(".value"+ar[1]).append(ar[2]);
        			}
        			if(ar[0]!=0){
	        			$("#PropertiesID").append(ar[2]);
        			}
        			disablescrollsetup();
        			$(".over_lay").hide();
                }else{
                	alert("Không thực hiện được chức năng theo yêu cầu.");
                }
	        }
	    });
	}

	var PropertiesList = [<?php echo implode(',',$arr_parent_js) ?>];

	function select_properties(){
		var ID = $("#PropertiesID").val();
		if(ID!=''){
			if(jQuery.inArray( "data"+ID, PropertiesList )<0){
				$(".message_empty").remove();
				$.ajax({
			        url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/add_newrow_properties' ?>",
			        dataType: "html",
			        type: "POST",
			        data: 'Data='+ID,
			        success: function(result){
			        	$(".content_tab7").append(result);
			        	PropertiesList.push("data"+ID);
			        }
			    });
			}
	    }
	}

	function AcceptVariant(ob,type){
		var variantvalue = $("#VariantSelect").val();
		if(type==0){
			select_properties_variant(ob);
		}else{
			select_properties_variant(ob);
		}
	}

	function select_properties_variant(ob){
		$(ob).addClass("saving");
		var baselink = $("#baselink_url").val();
		var IDEdit = $("#IDEdit").val();
		$.ajax({
	        url: baselink+"variant_add",
	        dataType: "html",
	        type: "POST",
	        data: "ID="+IDEdit,
	        success: function(result){
	        	$(ob).removeClass("saving");
	        	$(".content_tab8").append(result);
	        }
	    });
	}

	function rm_variant(ob,ID){
		if (!confirm('BẠN CÓ CHẮC XÓA DỮ LIỆU NÀY KHÔNG ??')) {
			return false;
		}
		$(ob).addClass("saving");
		var baselink = $("#baselink_url").val();
		$.ajax({
	        url: baselink+"delete_variant",
	        dataType: "html",
	        type: "POST",
	        data: "ID="+ID,
	        success: function(result){
	        	$(ob).parent('div').parent('div').remove();
	        }
	    });
	}

	function showtable(ob){
		$(ob).parent('div').parent('div').find('table').toggle();
	}

	function save_variant(ob,ID){
		$(ob).addClass("saving");
		var baselink = $("#baselink_url").val();
		var Variant_Properties = "";
		var name_bonus = $("#TitleProducts").val()+" (";
		$(".variant_"+ID+" .Variant_Properties").each(function(){
			Variant_Properties = Variant_Properties+"|"+$(this).val();
			var properties = $(this).parent('li').find('b').html();
			var value_properties = $(this).find("option:selected").text();
			name_bonus = name_bonus+properties +" "+ value_properties +" | ";
		});
		name_bonus = name_bonus+")";
		var Variant_Published = $(".variant_"+ID+" .Variant_Published").is(':checked') ? 1 : 0 ;
		var Variant_InventoryStatus = $(".variant_"+ID+" .Variant_InventoryStatus").val();
		var Variant_SKU = $(".variant_"+ID+" .Variant_SKU").val();
		var Variant_Price = $(".variant_"+ID+" .Variant_Price").val();
		var Variant_SpecialPrice = $(".variant_"+ID+" .Variant_SpecialPrice").val();
		var Variant_Weight = $(".variant_"+ID+" .Variant_Weight").val();
		var Variant_Length = $(".variant_"+ID+" .Variant_Length").val();
		var Variant_Width = $(".variant_"+ID+" .Variant_Width").val();
		var Variant_Height = $(".variant_"+ID+" .Variant_Height").val();
		var Variant_Description = $(".variant_"+ID+" .Variant_Description").val();
		var Variant_StartSpecialPrice = $(".variant_"+ID+" .Variant_StartSpecialPrice").val();
		var Variant_StopSpecialPrice = $(".variant_"+ID+" .Variant_StopSpecialPrice").val();
		$.ajax({
	        url: baselink+"update_variant",
	        dataType: "html",
	        type: "POST",
	        data: "ID="+ID+"&Properties="+Variant_Properties+"&Published="+Variant_Published+"&InventoryStatus="+Variant_InventoryStatus+"&SKU="+Variant_SKU+"&Price="+Variant_Price+"&SpecialPrice="+Variant_SpecialPrice+"&Weight="+Variant_Weight+"&Width="+Variant_Width+"&Height="+Variant_Height+"&Length="+Variant_Length+"&Description="+Variant_Description+"&StartSpecialPrice="+Variant_StartSpecialPrice+"&StopSpecialPrice="+Variant_StopSpecialPrice+"&Namebonus="+name_bonus,
	        success: function(result){
	        	$(ob).removeClass("saving");
	        	$(ob).parent('div').find(".btn-success").fadeIn('slow').delay("1000").fadeOut('slow');
	        }
	    });
	}

	function rm_image_products(ob,ID){
		var baselink = $("#baselink_url").val();
		$(ob).find('i').hide();
		$(ob).addClass("saving");
		$.ajax({
	        url: baselink+"delete_images",
	        dataType: "html",
	        type: "POST",
	        data: "ID="+ID,
	        success: function(result){
	        	$(ob).removeClass("saving");
	        	$(ob).parent('td').parent("tr").fadeOut();
	        }
	    });
	}

	function show_dateinput(ob){
		$(ob).parent('p').parent('td').find(".date_div").toggle();
	}

	function changeimages(ob){
		var baselink = $("#baselink_url").val();
	    var img = $(ob).parent('div').find('img');
	    var bar = $(ob).parent('div').find('.progress').find('.bar');
  		var percent = $(ob).parent('div').find('.progress').find('.percent');
  		var progress = $(ob).parent('div').find('.progress');
  		progress.show();
  		var percentValue = "0%";
	    var file = $(ob)[0].files[0];
		var fd = new FormData();
		var IDEdit = $(ob).attr('data');
		fd.append('file', file);
		fd.append('ID',IDEdit);
	    var xhr = new XMLHttpRequest();
		xhr.open('POST', baselink+"changeimages", true);
	    xhr.upload.onprogress = function(e) {
		    if (e.lengthComputable) {
		        var percentValue = (e.loaded / e.total) * 100 + '%';
		        percent.html(percentValue);
		        bar.css({'width':percentValue});
		    }
	    };
	    xhr.onload = function() {
		    if (this.status == 200) {
		        if(xhr.responseText!="False"){
		        	img.attr("src",xhr.responseText);
		        }else{
		        	alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu . Vui lòng kiểm tra lại đường truyền !");
		        }
		    };
		    progress.hide();
	    };

	    xhr.send(fd);
	};

	function remove_rowtab(ob){
		$(ob).parent('div').parent('div').parent('div').remove();
	}

	var imageeach = [];

	function activeupload(ob,temp,IDEdit){
		var i=0;
		$("."+temp).each(function(){
			imageeach[i] = $(this).attr('data');
			i++;
		});
		if(i>0){
			$(ob).find('i').hide();;
			$(ob).addClass("saving");
			if(temp=='filerow'){
				eachupload(0,imageeach.length);
			}
			if(temp=='filemultirow'){
				eachuploadmulti(0,imageeach.length,IDEdit);
			}
		}else{
			alert("Vui lòng chọn file");
		}
	}

	function eachupload(ob,count){
		temp = ob+1;
		if(ob==count){
			var tab = $("#currenttab").val();
			window.location="<?php echo current_url() ?>?tab="+tab;
			return false;
		}
		ob = imageeach[ob];
		var data = $(".filerow"+ob).attr('data');
		var bar = $('.bar'+data);
  		var progress = $('.progress'+data);
  		progress.show();
  		var percentValue = "0%";
		var baselink = $("#baselink_url").val();
		var Fileinput = document.getElementById("choosefile");
		var file = Fileinput.files[data];
		var fd = new FormData();
		var IDEdit = $("#IDEdit").val();
		fd.append('file', file);
		fd.append('ID',IDEdit);
	    var xhr = new XMLHttpRequest();
		xhr.open('POST', baselink+"upload_image", true);
	    xhr.upload.onprogress = function(e) {
		    if (e.lengthComputable) {
		        var percentValue = (e.loaded / e.total) * 100 + '%';
		        bar.css({'width':percentValue});
		    }
	    };
	    xhr.onload = function() {
		    if (this.status == 200) {
		        if(xhr.responseText!="False"){
		        	$(".filerow"+ob).remove();
		        }else{
		        	$(this).css({'border':'1px solid #F00'});
		        }
		        eachupload(temp,imageeach.length);
		    }else{
		    	eachupload(temp,imageeach.length);
		    }
	    };
	    xhr.send(fd);
	}

	function eachuploadmulti(ob,count,IDEdit){
		temp = ob+1;
		if(ob==count){
			var tab = $("#currenttab").val();
			window.location="<?php echo current_url() ?>?tab="+tab;
			return false;
		}
		ob = imageeach[ob];
		var data = $(".filemultirow"+ob).attr('data');
		var bar = $('.bar'+data);
  		var progress = $('.progress'+data);
  		progress.show();
  		var percentValue = "0%";
		var baselink = $("#baselink_url").val();
		var Fileinput = document.getElementById("multipleimages"+IDEdit);
		var file = Fileinput.files[data];
		var fd = new FormData();
		fd.append('file', file);
		fd.append('ID',IDEdit);
	    var xhr = new XMLHttpRequest();
		xhr.open('POST', baselink+"upload_image", true);
	    xhr.upload.onprogress = function(e) {
		    if (e.lengthComputable) {
		        var percentValue = (e.loaded / e.total) * 100 + '%';
		        bar.css({'width':percentValue});
		    }
	    };
	    xhr.onload = function() {
		    if (this.status == 200) {
		        if(xhr.responseText!="False"){
		        	$(".filemultirow"+ob).remove();
		        }else{
		        	$(this).css({'border':'1px solid #F00'});
		        }
		        eachuploadmulti(temp,imageeach.length,IDEdit);
		    }else{
		    	eachuploadmulti(temp,imageeach.length,IDEdit);
		    }
	    };
	    xhr.send(fd);
	}

	$("#choosefile").change(function(){
        var Fileinput = document.getElementById("choosefile");
        var numfile = Fileinput.files.length;
        var dvPreview = $("#dvPreview");
        if(numfile>0){
            dvPreview.html("");
            dvPreview.append("<div class='row'><div class='col-md-1 col-sm-1 col-xs-12'>Preview</div><div class='col-md-1 col-sm-1 col-xs-12'></div><div class='col-md-5 col-sm-5 col-xs-12'>FileName</div><div class='col-md-2 col-sm-2 col-xs-12'>File Type</div><div class='col-md-2 col-sm-2 col-xs-12'>File Size</div><div class='col-md-1 col-sm-1 col-xs-12'>Remove</div></div>");
            var j=0;
            for (var i = 0; i < numfile; i++) {
                var reader = new FileReader();
                var file = Fileinput.files[i];
                var imageType = /image.*/;
                var head = "";
                if(file.type.match(imageType)){
                    reader.onloadend = function (e) {
                        var file = Fileinput.files[j];
                        dvPreview.append("<div class='row filerow filerow"+j+"' data='"+j+"'><div class='col-md-1 col-sm-1 col-xs-12'><img src='"+e.target.result+"' class='img-responsive' /></div><div class='col-md-1 col-sm-1 col-xs-12'></div><div class='col-md-5 col-sm-5 col-xs-12'>"+file.name+"</div><div class='col-md-2 col-sm-2 col-xs-12'>"+file.type+"</div><div class='col-md-2 col-sm-2 col-xs-12'>"+file.size+"</div><div class='col-md-1 col-sm-1 col-xs-12'><a onclick='remove_imagerow(this)' title='Gỡ bỏ file này khỏi danh sách'><i class='fa fa-times'></i></a></div><div class='progress"+j+"'><div class='bar"+j+"'></div></div></div>");
                        $(".activeupload").show();
                        j++;
                    }
                }else{
                    console.log("Not an Image");
                }
                reader.readAsDataURL(file);
            }
            $(".dvPreview").slideDown('slow');
        }
    });

	function changemultipleimages(ob){
		var id = $(ob).attr('id');
		var Fileinput = document.getElementById(id);
        var numfile = Fileinput.files.length;
        var dvPreview = $(ob).parent('a').parent('div').parent('.image_variant').find('.list-preview-images');
        if(numfile>0){
            dvPreview.html("");
            var j=0;
            for (var i = 0; i < numfile; i++) {
                var reader = new FileReader();
                var file = Fileinput.files[i];
                var imageType = /image.*/;
                var head = "";
                if(file.type.match(imageType)){
                    reader.onloadend = function (e) {
                        var file = Fileinput.files[j];
                        dvPreview.append("<div class='col-xs-3 filemultirow filemultirow"+j+"' data='"+j+"'><img src='"+e.target.result+"' class='img-responsive' /></div>");
                        $(".activeupload").show();
                        j++;
                    }
                }else{
                    console.log("Not an Image");
                }
                reader.readAsDataURL(file);
            }
            $(".dvPreview").slideDown('slow');
        }
	}

	function remove_imagerow(ob){
		$(ob).parent('div').parent('div').remove();
		var temp = 0;
		$(".filerow").each(function(){
			temp++;
		});
		if(temp==0){
			$(".activeupload").hide();
			$("#dvPreview").html("");
		}
	}

	function active_save_image(ob,i){
		if($(".imagerow_tr").length==i){
			$(ob).removeClass("saving");
	        $(ob).find('i').show();
	        $(ob).parent('div').find(".last-a").fadeIn('slow').delay("500").fadeOut('slow');
			return false;
		}
		$(ob).find('i').hide();
		$(ob).addClass("saving");
		var baselink = $("#baselink_url").val();
		var ID = $(".imagerow_tr"+i).attr('data');
		var label_tr = $(".imagerow_tr"+i+" td .label_tr").val();
		var stt_tr = $(".imagerow_tr"+i+" td .stt_tr").val();
		var primaryimage_tr = $(".imagerow_tr"+i+" td .primaryimage_tr").is(":checked") ? 1 : 0 ;
		var smallimage_tr = $(".imagerow_tr"+i+" td .smallimage_tr").is(":checked") ? 1 : 0 ;
		var thumbimage_tr = $(".imagerow_tr"+i+" td .thumbimage_tr").is(":checked") ? 1 : 0 ;
		$.ajax({
	        url: baselink+"row_image_update",
	        dataType: "html",
	        type: "POST",
	        data: "ID="+ID+"&Label="+label_tr+"&STT="+stt_tr+"&Primary="+primaryimage_tr+"&Small="+smallimage_tr+"&Thumb="+thumbimage_tr,
	        success: function(result){}
	    }).always(function(){
	    	active_save_image(ob, ++i);
        });
	}

	function changeimagerow(ob){
		var baselink = $("#baselink_url").val();
	    var img = $(ob).parent('div').find('img');
	    var bar = $(ob).parent('div').find('.progress').find('.bar');
  		var progress = $(ob).parent('div').find('.progress');
  		progress.show();
  		var percentValue = "0%";
	    var file = $(ob)[0].files[0];
		var fd = new FormData();
		var IDEdit = $(ob).attr('data');
		fd.append('file', file);
		fd.append('ID',IDEdit);
	    var xhr = new XMLHttpRequest();
		xhr.open('POST', baselink+"changeimagerow", true);
	    xhr.upload.onprogress = function(e) {
		    if (e.lengthComputable) {
		        var percentValue = (e.loaded / e.total) * 100 + '%';
		        bar.css({'width':percentValue});
		    }
	    };
	    xhr.onload = function() {
		    if (this.status == 200) {
		        if(xhr.responseText!="False"){
		        	img.attr("src",xhr.responseText);
		        }else{
		        	alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu . Vui lòng kiểm tra lại đường truyền !");
		        }
		    };
		    progress.hide();
	    };

	    xhr.send(fd);
	};

	function saveall(ob){
		$(ob).addClass("saving");
		$("#products_form").append("<input type='hidden' name='SaveAndExit' value='1' />");
		$("#products_form").submit();
	}

	function remove_images_variant(ob,ID){
		var baselink = $("#baselink_url").val();
		var listID = [];
		$(".select_image").each(function(){
			listID.push($(this).attr('data'));
		});
		if(listID.length==0){
			alert("Vui lòng chọn hình ảnh để xóa !");
			return false;
		}
		$(ob).find('i').hide();
		$(ob).addClass("saving");
		listID = JSON.stringify(listID);
		$.ajax({
	        url: baselink+"delete_images_variant",
	        dataType: "html",
	        type: "POST",
	        data: "ID="+ID+"&listID="+listID,
	        success: function(result){
	        	$(ob).removeClass("saving");
	        	$(ob).find('i').show();
	        	$(".select_image").parent('div').remove();
	        }
	    });
	}

	function setprimaryimage(ob,imageID,productsID){
		var baselink = $("#baselink_url").val();
		var src = $(ob).parent('div.col-xs-3').find('img').attr('data-src');
		$.ajax({
	        url: baselink+"setprimary_images_variant",
	        dataType: "html",
	        type: "POST",
	        data: "ID="+productsID+"&ImageID="+imageID,
	        success: function(result){
	        	$(ob).addClass('text-info');
	        	$(ob).parent('div.col-xs-3').parent('div.list-preview-images').parent('.image_variant').find('.img-primary-variant').attr('src',src);
	        }
	    });
	}

	function select_image(ob){
		var dataselected = $(ob).attr('data-selected');
		if(dataselected==0){
			$(ob).addClass("select_image");	
			$(ob).attr('data-selected',1);
		}else{
			$(ob).removeClass("select_image");
			$(ob).attr('data-selected',0);
		}
	}

	function show_productsbox(ob){
		var baselink = $("#baselink_url").val();
		$(ob).addClass('saving');
		$.ajax({
        	url: baselink+"get_products_list",
            dataType: "html",
            type: "POST",
            context: this,
            data: "",
            success: function(result){
                if(result!='FALSE'){
        			$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);
					$(".over_lay").removeClass("in");
					$(".over_lay").fadeIn('fast');
					$(".over_lay").addClass("in");
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
                $(ob).removeClass('saving');
            }
        });
	}

	function addproductstobundle(ob,ProductsID){
		var baselink = $("#baselink_url").val();
		$.ajax({
        	url: baselink+"addproductstobundle",
            dataType: "html",
            type: "POST",
            context: this,
            data: "BundleID=<?php echo $data->ID ?>&ProductsID="+ProductsID,
            success: function(result){
                if(result=='OK'){
        			reload_bundle_list();
        			if (!confirm('THỰC HIỆN THAO TÁC THÀNH CÔNG !! \nBẠN CÓ MUỐN CHỌN SẢN PHẨM TIẾP KHÔNG ??')) {
						var tab = $("#currenttab").val();
						window.location="<?php echo current_url() ?>?tab="+tab;
					}
                }
                if(result=='FALSE'){
                	alert("Sản phẩm đã được thêm vào nhóm này rồi ! Vui lòng không chọn nữa !");
                }
                if(result=='FALSE1'){
                	alert("Sản phẩm này đã bị khóa . Mọi thay đổi sẽ không được thực hiện !");
                }
            }
        });
	}

	function removeoutbundle(ob,IDBundle){
		$(ob).find('i').hide();
		$(ob).addClass("saving");
		var baselink = $("#baselink_url").val();
		$.ajax({
        	url: baselink+"removeoutbundle",
            dataType: "html",
            type: "POST",
            context: this,
            data: "ID="+IDBundle,
            success: function(result){
                if(result=='OK'){
                	reload_bundle_list();
                }
                if(result=='FALSE'){
                	$(ob).find('i').show();
					$(ob).removeClass("saving");
                	alert("Thao tác thực hiện không thành công ! Vui lòng kiểm tra lại đường truyền !");
                }
                if(result=='FALSE1'){
                	$(ob).find('i').show();
					$(ob).removeClass("saving");
                	alert("Sản phẩm này đã bị khóa . Mọi thay đổi sẽ không được thực hiện !");
                }
            }
        });
	}

	function updatequantitybundle(ob,IDBundle){
		var Quantity = $(ob).val();
		if(Quantity>0){
			var baselink = $("#baselink_url").val();
			$.ajax({
	        	url: baselink+"updatequantitybundle",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+IDBundle+"&Quantity="+Quantity,
	            success: function(result){
	            	if(result=='FALSE'){
	                	alert("Thao tác thực hiện không thành công ! Vui lòng kiểm tra lại đường truyền !");
	                }
	                if(result=='FALSE1'){
	                	alert("Sản phẩm này đã bị khóa . Mọi thay đổi sẽ không được thực hiện !");
	                }
	            }
	        });
        }
	}

	function reload_bundle_list(){
		var baselink = $("#baselink_url").val();
		$("#bundle-list").load(baselink+"get_products_bundle_list/<?php echo $data->ID ?>");
	}
</script>

