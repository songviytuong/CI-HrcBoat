<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$month 			= array();
$dayofmonth		= array();
$dayofmonth_real= array();
$result_fillter = array();
$fill_temp_value="";
?>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>DANH SÁCH PHIẾU ĐỀ NGHỊ MUA HÀNG (PO)</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
	</div>
	<div class="import_orderlist">
        <div class="block2 row">
            <div class="form-group">
                <div class="block_2_1 col-xs-8">
                    <a class="btn btn-primary" href="<?php echo $base_link.'add' ?>"><i class="fa fa-plus"></i> Tạo phiếu mới</a>
                </div>
                <div class="block_2_2 col-xs-4">
                    <button class="btn btn-default pull-right" onclick="showtools()">Advanced Filter <b class="caret"></b></button>
                    <a class="btn btn-primary" role="button"><i class="fa fa-download"></i> Export</a>
                </div>
            </div>
        </div>
	    <div class="clear"></div>
        <div class="filltertools">
            <form action="<?php echo $base_link."setfillter" ?>" method="post">
                <?php 
                    $arr_fieldname = array(0=>"a.POCode",1=>"b.ID",2=>"a.UserID");
                    $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                    $arr_showfieldname = array(0=>"Mã PO",1=>"Nhà cung cấp",2=>"Người tạo");
                    $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                    $fill_data_arr = explode(" and ",$fill_data);
                    if(count($fill_data_arr)>0 && $fill_data!=''){
                        $temp_tools=0;
                        foreach($fill_data_arr as $row){
                            $param = explode(' ',$row,3);
                            $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                            $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                            $param_oparation=isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                            $param_value = isset($param[2]) ? $param[2] : '' ;
                            $param_value = str_replace("\'","",$param_value);
                            $param_value = str_replace("'","",$param_value);
                            $param_value = str_replace("%","",$param_value);
                ?>
                <div class="row <?php echo $temp_tools==0 ? "base_row" : "" ; ?>">
                    <div class="list_toolls col-xs-1"><label class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></label></div>
                    <div class="list_toolls col-xs-2">
                        <input type="hidden" class="FieldName" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                        <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></label>
                        <ul class="dropdownbox">
                            <li><a onclick="setfield(this,0,'mapo')">Mã PO</a></li>
                            <li><a onclick="setfield(this,1,'nhacungcap')">Nhà cung cấp</a></li>
                            <li><a onclick="setfield(this,2,'nguoitao')">Người tạo</a></li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation col-xs-2">
                        <select class="oparation form-control" name="FieldOparation[]">
                            <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                            <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                            <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                            <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                            <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                            <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                            <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter col-xs-3">
                        <?php 
                        if ($value_field==2) {
                            $userlist = $this->db->query("select ID,UserName from ttp_user where UserType=6 or IsAdmin=1")->result();
                            if(count($userlist)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($userlist as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->UserName : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->UserName</option>";
                                }
                                echo "</select>";
                            }
                        }elseif ($value_field==1) {
                            $production = $this->db->query("select ID,Title from ttp_report_production")->result();
                            if(count($production)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($production as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                                echo "</select>";
                            }
                        }else{
                            $fill_temp_value = $param_value ;
                            echo '<input type="text" class="form-control" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                        }
                        ?>
                    </div>
                    <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
                <?php   
                        $temp_tools++;
                        $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                        $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                        }
                    }else{
                ?>
                    <div class="row base_row">
                        <div class="list_toolls col-xs-1"><label class="title first-title">Chọn</label></div>
                        <div class="list_toolls col-xs-2">
                            <input type="hidden" class="FieldName" name="FieldName[]" />
                            <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                            <ul class="dropdownbox">
                                <li><a onclick="setfield(this,0,'mapo')">Mã PO</a></li>
                                <li><a onclick="setfield(this,1,'nhacungcap')">Nhà cung cấp</a></li>
                                <li><a onclick="setfield(this,2,'nguoitao')">Người tạo</a></li>
                            </ul>
                        </div>
                        <div class="list_toolls reciveroparation col-xs-2">
                            <select class="oparation form-control" name="FieldOparation[]">
                                <option value="1">Bằng</option>
                                <option value="0">Có chứa</option>
                                <option value="2">Khác</option>
                                <option value="3">Lớn hơn</option>
                                <option value="4">Nhỏ hơn</option>
                                <option value="5">Lớn hơn hoặc bằng</option>
                                <option value="6">Nhỏ hơn hoặc bằng</option>
                            </select>
                        </div>
                        <div class="list_toolls reciverfillter col-xs-3">
                            <input type="text" name="FieldText[]" id="textsearch" class='form-control' />
                        </div>
                        <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                    </div>
                <?php
                    }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-11">
                                <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                                <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
	    <div class="block3 table_data">
            <?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            ?>
	    	<table id="table_data">
    			<tr>
    				<th>STT</th>
    				<th>Ngày khởi tạo</th>
                    <th>Dự kiến nhập kho</th>
    				<th>Mã PO</th>
    				<th>Nhà cung cấp</th>
    				<th>Diễn giải</th>
                    <th>Tình trạng PO</th>
    				<th>User</th>
    			</tr>
    			<?php
                $arr_status = array(
                    0=> "PO nháp",
                    1=> "PO đang chờ duyệt",
                    2=> "PO đã được duyệt",
                    3=> "PO chờ nhập",
                    4=> "Đang giao hàng",
                    5=> "PO đã đóng"
                );
                if(count($data)>0){
    				$i = $start+1;
    				foreach($data as $row){
    					echo "<tr>";
    					echo "<td style='width: 36px;text-align:center'>$i</td>";
    					echo "<td><a href='".$base_link."edit/$row->ID'>".date('d/m/Y',strtotime($row->DatePO))."</a></td>";
                        echo "<td><a href='".$base_link."edit/$row->ID'>".date('d/m/Y',strtotime($row->DateExpected))."</a></td>";
    					echo "<td><a href='".$base_link."edit/$row->ID'>".$row->POCode."</a></td>";
    					echo "<td><a href='".$base_link."edit/$row->ID'>".$row->Title."</a></td>";
    					echo "<td><a href='".$base_link."edit/$row->ID'>$row->Note</td>";
                        echo isset($arr_status[$row->Status]) ? "<td><a href='".$base_link."edit/$row->ID'>".$arr_status[$row->Status]."</a></td>" : "<td>--</td>";
    					echo "<td><a href='".$base_link."edit/$row->ID'>".$row->UserName."</a></td>";
    					echo "</tr>";
    					$i++;
    				}
    			}else{
    				echo "<tr><td colspan='8'>Không tìm thấy đề nghị mua hàng.</td></tr>";
    			}
    			?>
    		</table>
    		<?php 
                echo $nav;
            ?>
	    </div>
    </div>
    <div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<?php 
    $time_startday = strtotime($startday);
    $time_stopday = strtotime($stopday);
    $currday = strtotime(date('Y-m-d',time()));
?>
<script>
	$(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day_warehouse",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"/warehouse_inventory_perchaseorder/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="mapo" || fieldname=="nhacungcap" || fieldname=="nguoitao"){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
        }
    }

</script>