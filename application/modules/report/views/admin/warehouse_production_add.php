<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Thêm nhà cung cấp</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tên nhà cung cấp</span></div>
					<div class='block2'><input type='text' class="form-control" name="Title" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Mã nhà cung cấp</span></div>
					<div class='block2'><input type='text' class="form-control" name="Code" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Địa chỉ</span></div>
					<div class='block2'><input type='text' class="form-control" name="Address" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Điện thoại</span></div>
					<div class='block2'><input type='text' class="form-control" name="Phone" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Fax</span></div>
					<div class='block2'><input type='text' class="form-control" name="Fax" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Trạng thái hoạt động</span></div>
					<div class='block2'>
						<input type='radio' name="Published" value="1" checked="true" /> Enable 
						<input type='radio' name="Published" value="0" /> Disable 
					</div>
				</div>
			</div>
		</form>
	</div>
</div>