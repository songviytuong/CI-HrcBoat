<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa thông tin client</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Lưu thông tin</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Trạng thái hoạt động</label>
							<div class='col-xs-10'>
								<input type='radio' name="Published" value="1" <?php echo $data->Published==1 ? 'checked="true"' : '' ; ?> /> Enable 
								<input type='radio' name="Published" value="0" <?php echo $data->Published==0 ? 'checked="true"' : '' ; ?> /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Tên miền (Domain)</label>
							<div class='col-xs-10'>
								<select class="form-control" name="WebsiteID">
									<?php 
									$website = $this->db->query("select * from ttp_system_website")->result();
									if(count($website)>0){
										foreach($website as $row){
											$selected = $row->ID==$data->WebsiteID ? 'selected="selected"' : '' ;
											echo "<option value='$row->ID' $selected>$row->Domain</option>";
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Client ID</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="ClientID" value='<?php echo $data->ClientID ?>' required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Client Key</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="ClientKey" value='<?php echo $data->ClientKey ?>' required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Client Password</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="ClientPassword" value='<?php echo $data->ClientPassword ?>' required /></div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>