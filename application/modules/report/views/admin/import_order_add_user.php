<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>THÔNG TIN ĐƠN HÀNG</h1>
        </div>
        <div class="block2">
            <div class="btn_group">
                <a class="btn btn-accept btn-danger" onclick="skip(this)"><i class="fa fa-check-square"></i> Duyệt</a>
                <a type='button' value="Cập nhật" class="btn btn-success btn-update submit" id="buttonsubmit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Lưu nháp</a>
            </div>
        </div>
    </div>
    <div class="import_order_info">
        <form id="submit_order" method="POST" action="<?php echo $base_link . 'add_new_order' ?>">
            <input type='hidden' name="Tinhtrangdonhang" id="Status" value='2' />
            <div class="block1">
                <div class="table_absolute">
                    <table>
                        <tr>
                            <th>Chỉ tiêu ngày</th>
                            <th>Chỉ tiêu tháng</th>
                            <th>Thực tế ngày</th>
                            <th>Thực tế tháng</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="row1 row">
                    <input type='hidden' name="UserID" id="SalesmanID" value="<?php echo $this->user->ID ?>" />
                    <input type='hidden' name="Ngayban" class="form-control date-picker" id="Ngayban" value="<?php echo date('m/d/Y', time()); ?>" required />
                </div>
                <div class="row row5">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Nguồn đơn hàng </label>
                            <div class="col-xs-7">
                                <select name="SourceID" id="SourceID" class="form-control">	
                                    <?php
                                    $source = $this->db->query("select * from ttp_report_source")->result();
                                    if (count($source) > 0) {
                                        foreach ($source as $row) {
                                            echo "<option value='$row->ID'>$row->Title</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Kênh bán hàng </label>
                            <div class="col-xs-7">
                                <select name="KenhbanhangID" id="KenhbanhangID" class="form-control">
                                    <option value='0'>-- Chọn kênh bán hàng --</option>
                                    <?php
                                    $kenhbanhang = $this->db->query("select * from ttp_report_saleschannel")->result();
                                    if (count($kenhbanhang) > 0) {
                                        foreach ($kenhbanhang as $row) {
                                            echo "<option value='$row->ID'>$row->Title</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row1 row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Loại khách hàng</label>
                            <div class="col-xs-7">
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="Loaikhachhang" id="Loaikhachhangmoi" name="Loaikhachhang" value="0" checked='checked' />
                                        Khách hàng mới
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="Loaikhachhang" id="Loaikhachhangcu" name="Loaikhachhang" value="1" />
                                        Khách hàng cũ
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row3">
                    <div class="col-xs-4">
                        <div class="form-group find_customer hidden">
                            <label class="col-xs-5 control-label">Tìm số điện thoại: </label>
                            <div class="col-xs-7">
                                <input type='text' id="sdt_cus" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group find_customer hidden">
                            <label class="col-xs-5 control-label">Hoặc tìm tên: </label>
                            <div class="col-xs-7">
                                <input type='text' id="name_cus" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group find_customer hidden">
                            <a class="btn btn-default" id="find_customers" style="margin-top: 0px;"><i class="fa fa-search" aria-hidden="true"></i> Tìm khách hàng</a>
                        </div>
                    </div>
                </div>
                <div class="row row4">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Tên khách hàng </label>
                            <div class="col-xs-7">
                                <input name="CustomerID" id="CustomerID" value="" type="hidden" />
                                <input type='text' name="Tenkhachhang" class="form-control" id="Tenkhachhang" required />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Ngày sinh </label>
                            <div class="col-xs-7">
                                <input type='text' name="NTNS" id="NTNS" placeholder="dd/mm/yyyy" class="form-control NTNS" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Tuổi </label>
                            <div class="col-xs-7">
                                <input type='text' name="Age" class="form-control Age" id="Age" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top:10px">
                        <div class="form-group">
                            <div class='lichsugiaodich hidden'><div class="history_cus hidden"></div><a id='xemlichsugiaodich' data-status="down">Xem lịch sử giao dịch <b class='caret'></b></a></div>
                        </div>
                    </div>
                </div>
                <div class="row row7">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Số di động 1: </label>
                            <div class="col-xs-7">
                                <input type='text' name="Phone1" id="Phone1" class="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Số di động 2: </label>
                            <div class="col-xs-7">
                                <input type='text' name="Phone2" id="Phone2" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">PT thanh toán</label>
                            <div class="col-xs-7">
                                <select name="PTthanhtoan" class="form-control">
                                    <option value='0'>COD</option>
                                    <option value='1'>Chuyển khoản</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row row7">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Khu vực </label>
                            <div class="col-xs-7">
                                <select id="Khuvuc" class="form-control">
                                    <option value="0">-- Chọn khu vực --</option>
                                    <?php
                                    $area = $this->db->query("select * from ttp_report_area")->result();
                                    if (count($area) > 0) {
                                        foreach ($area as $row) {
                                            echo "<option value='$row->ID'>$row->Title</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Tỉnh / Thành </label>
                            <div class="col-xs-7">
                                <select id="Tinhthanh" class="form-control" name="CityID">
                                    <option value="0">-- Chọn tỉnh thành --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="col-xs-5 control-label">Quận / Huyện </label>
                            <div class="col-xs-7">
                                <select id="Quanhuyen" class="form-control" name="DistrictID">
                                    <option value="0">-- Chọn quận huyện --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row6">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-xs-12 control-label">Địa chỉ giao hàng </label>
                            <div class="col-xs-12">
                                <input type='text' name="Address" id="Address_order" class="form-control" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row6">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-xs-12 control-label">Ghi chú giao hàng</label>
                            <div class="col-xs-12">
                                <input type='text' name="Note" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end block1 -->
            <div class="block2 row">
                <div class="col-xs-5">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Sản phẩm</a>
                        </div>
                        <div class="col-xs-4">
                            <ul>
                                <li><a id='show_thaotac' class="btn btn-default">Thao tác <b class="caret"></b></a>
                                    <ul>
                                        <li><a id="edit_row_table"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a></li>
                                        <li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-briefcase"></i></div>
                            <input type="text" class="form-control" id="voucher" placeholder="Voucher" maxlength="8" style="text-transform: uppercase;">
                            <div class="input-group-addon"><i class="fa fa-search" id="checkA" data="0" style="cursor: pointer"></i></div>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="col-xs-7">
                    <div class="form-group">
                        <label class="col-xs-4 control-label text-right">Kho lấy hàng </label>
                        <div class="col-xs-7 pull-right">
                            <select name="KhoID" id="KhoID" class="form-control" onchange="loadshipmentdefault()">
                                <option value="">Không có kho hàng</option>
                            </select>
                        </div>
                    </div>
                </div>
                
            </div>
            <div id="ErrorMessenger"></div>
            <div class="table_donhang table_data">
                <table class="table_data" id="table_data">
                    <tr>
                        <th><input type='checkbox' class="" onclick='checkfull(this)' /></th>
                        <th>Mã sản phẩm</th>
                        <th>Tên sản phẩm</th>
                        <th>Lô</th>
                        <th>Giá bán</th>
                        <th>Số lượng</th>
                        <th>% CK</th>
                        <th>Giá trị CK</th>
                        <th>Giá sau CK</th>
                        <th>Thành tiền</th>
                    </tr>
                    <tr class="last insert">
                        <td colspan="9">Tổng cộng</td>
                        <td><input type="number" class="tongcong" readonly="true" value="0" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongcong">0</span></td>

                    </tr>
                    
                    <tr class="last">
                        <td colspan="9">% chiết khấu</td>
                        <td><input type='number' name="phantramchietkhau" class="phantramchietkhau" min="0" max="100" value="0" onchange="calchietkhau(this)" /></td>

                    </tr>
                    <tr class="last">
                        <td colspan="9">Giá chiết khấu</td>
                        <td><input type='number' name="giachietkhau" class="giachietkhau"  value="0" onchange="calchietkhau(this)" /></td>

                    </tr>
                    <tr class="last">
                        <td colspan="9">Tổng tiền hàng</td>
                        <td><input type="number" class="tongtienhang" readonly="true" value="0" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongtienhang">0</span></td>

                    </tr>
                    <tr class="last">
                        <td colspan="9">Chi phí vận chuyển</td>
                        <td><input type='number' name="chiphivanchuyen" class="chiphivanchuyen" value="0" onchange="recal()" style="display:none" /><span id="showchiphivanchuyen" style="display:block;text-align:right;padding-right:15px;" onclick="showchiphi()">0</span></td>

                    </tr>
                    <tr class="last">
                        <td colspan="9" style="position: relative;">
                            <div class="showVoucherCode hidden" style="text-align: center; position: absolute; bottom:20px; background-color: #fff; border: 2px solid green; padding:3px 10px; left:55px;">
                                <div><small style="font-size:11px; font-weight: normal">Đã sử dụng Voucher</small></div>
                                <div style="font-weight:bold;"><strong class="VCode">AJSDU2</strong></div>
                            </div>
                            Tổng giá trị thanh toán</td>
                        <td><input type='number' class="tonggiatrithanhtoan" readonly="true" value="0" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiatrithanhtoan">0</span></td>

                    </tr>
                </table>
                <a class="addGift btn btn-danger hidden" rel="0"><<>></a>
            </div>
            <br/>
            <div class="">
                <div class="form-group">
                    <div class="col-xs-4">
                <select name="Campaign" id="Campaign" class="form-control">
                    <option value="-1" selected="selected" disabled>--- Lựa chọn Campaign (<?=$promotion_count;?>) ---</option>
                    <?php
                        foreach($promotion as $row){
                    ?>
                    <option value="<?=$row->promo_id;?>"><?=$row->promo_name;?></option>
                    <?php } ?>
                </select>
                        
                    </div>&nbsp;<a class="btn btn-success btnUsed" data="0"><i class='fa fa-search'></i> Kiểm tra</a>
                </div>
                <div class="clearfix"></div>
                <small class="desc"></small>
            </div>
            <input type='hidden' id="UserCreated" name="UserCreated" value="0" />
        </form>
        
    </div>
    
    <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url() . ADMINPATH . "/report/" ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
    .alert{
        margin-bottom: -20px;
    }
</style>
<script type="text/javascript">
    
    $('.addGift').click(function(){
        var id = $(this).attr('rel');
        if(id != 0){
            $.ajax({
                url: link + "import_order/addGift",
                dataType: "JSON",
                type: "POST",
                context: this,
                data: "id="+id,
                success: function (result) {
                    $('#table_data tr.insert').before(result.ResTR);
                    $('.addGift').attr("rel","0");
                }
            });
        }
        
        
    });
    
    setTimeout(function(){
            $('.btnUsed').addClass("hidden");
        },100);
    $(document).ready(function () {
        $('#Ngayban').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        });
        
        
        $('#Campaign').change(function(){
            var id = $(this).val();
            if(id == -1){
                $('.btnUsed').addClass("hidden");
            }else{
                $('.btnUsed').removeClass("hidden");
            }
            
        });
       
        
        $('.btnUsed').click(function(){
            //$(this).attr("disabled",true);
            //$('#Campaign').attr("disabled",true);
            $('.addGift').attr("rel",$('#Campaign').val());
            var dat = $(this).attr("data");
            if(dat == "0"){
                $.ajax({
                url: link + "import_order/verifyCampaign",
                dataType: "JSON",
                type: "POST",
                context: this,
                data: "promo_id="+$('#Campaign').val(),
                success: function (result) {
                    $('.desc').html(result.desc);
                    if(result.status == "Error"){
                        $('#ErrorMessenger').html(result.messenger);
                        $('#voucher').focus();
                    }else if(result.status == "None"){
                        $('#ErrorMessenger').html(result.messenger);
                    }else if(result.status == "Used"){
                        $('#ErrorMessenger').html(result.messenger);
                    }else{
                        $('.VCode').text(result.code);
                        $('#ErrorMessenger').html(result.messenger);
                    }
                    console.log(result);
                    }
                });
            }
        });
    });
</script>
<script type="text/javascript">
    var link = $("#baselink_report").val();
    var celldata = "";
    function thisUsed(ob,id){
        $('.showVoucherCode').removeClass("hidden");
        $('.btnUsed').attr("disabled",true);
        $('#Campaign').attr("disabled",true);
        //Live
        $('.thisUsed').attr("disabled",true);
        $('#voucher').attr("disabled",true);
        $('.fa-search').attr("data","1");
        $('.btnUsed').attr("data","1");
        $('.thisUsed').html("<i class='fa fa-thumbs-o-up'></i> Đang sử dụng voucher");
        
         $.ajax({
            url: link + "import_order/getVoucher",
            dataType: "JSON",
            type: "POST",
            context: this,
            data: "id="+id,
            success: function (result) {
                if(result.discount_type == 3){
                    $('.addGift').click();
                }
                celldata = result.ProductList;
                $('tr').each(function(){
                var valuetr = $(this).children('td').find('#ProductsID').attr('value');
                if(jQuery.inArray(valuetr, celldata) > 0) {
                    //do something
                    var sol = $(this).children('td').find('.soluong');
                    console.log(sol.val());
                    var per = $(this).children('td').find('.percentck');
                    var gck = $(this).children('td').find('.giack');
                    var sel = $(this).children('td').find('.selected_products');
                    if(sol.val() >= result.min_quantity){
                            sel.click();        
                            if(result.discount_type == 1){
                                gck.attr("value",parseInt(result.discount));
                            }else if(result.discount_type == 2){
                                per.attr("value",parseInt(result.discount));
                            }else{
                                
                            }
                            sel.click();
                        }
                        else{
                            sel.click();
                            per.attr("value","0");
                            gck.attr("value","0");
                            sel.click();
                        }
                    }
                });
            }
        });
    }
    $(document).ready(function () {
        $('#checkA').click(function(){
        var dat = $(this).attr("data");
        if(dat == "0"){
            $.ajax({
            url: link + "import_order/verifyCode",
            dataType: "JSON",
            type: "POST",
            context: this,
            data: "voucher="+$('#voucher').val(),
            success: function (result) {
                if(result.status == "Error"){
                    $('#ErrorMessenger').html(result.messenger);
                    $('#voucher').focus();
                }else if(result.status == "Expired"){
                    $('#ErrorMessenger').html(result.messenger);
                }else if(result.status == "Used"){
                    $('#ErrorMessenger').html(result.messenger);
                }else{
                    $('#ErrorMessenger').html(result.messenger);
                }
                console.log(result);
                }
            });
        }
        });
    });
    
    $("#close_overlay").click(function () {
        $(".over_lay").hide();
        disablescrollsetup();
    });

    $("#show_thaotac").click(function () {
        $(this).parent('li').find('ul').toggle();
    });

    $(".Loaikhachhang").change(function () {
        var data = $(this).val();
        if (data == 1) {
            $(".find_customer").removeClass('hidden');
            $("#Tenkhachhang").prop("readonly", true);
        } else {
            $(".find_customer").addClass('hidden');
            $("#Tenkhachhang").prop("readonly", false);
        }
    });
    
    $('#voucher').keypress(function(e){
        if(e.which == 13) {
            console.log("enter");
            $('#checkA').click();
        }
    });
    
    $("#find_customers").click(function () {
        var salesman = $("#SalesmanID").val();
        var sdt_cus = $("#sdt_cus").val();
        var name_cus = $("#name_cus").val();
        if (salesman != '' && (sdt_cus != '' || name_cus != '')) {
            enablescrollsetup();
            $(this).addClass('saving');
            $.ajax({
                url: link + "import_order/getcus",
                dataType: "html",
                type: "POST",
                context: this,
                data: "CustomerID=" + salesman + '&sdt_cus=' + sdt_cus + "&name_cus=" + name_cus,
                success: function (result) {
                    if (result != 'false') {
                        $(".over_lay .box_inner").css({'margin-top': '50px'});
                        $(".over_lay .box_inner .block1_inner h1").html("Danh sách khách hàng");
                        $(".over_lay .box_inner .block2_inner").html(result);
                        $(".over_lay").removeClass('in');
                        $(".over_lay").fadeIn('fast');
                        $(".over_lay").addClass('in');
                    } else {
                        alert("Không tìm thấy dữ liệu theo yêu cầu.");
                    }
                    $(this).removeClass('saving');
                }
            });
        } else {
            if (salesman == '') {
                alert("Vui lòng tên nhân viên để thực hiện quá trình tìm kiếm");
            } else {
                alert("Vui lòng điền tên hoặc số dt của khách hàng để thực hiện quá trình tìm kiếm");
            }
        }
    });

    $("#add_products_to_order").click(function () {
        var KhoID = $('#KhoID').val();
        if (KhoID == '' || KhoID == 0) {
            alert('Vui lòng chọn kho lấy hàng');
            $('#KhoID').focus();
            return false;
        }
        enablescrollsetup();
        $(".over_lay .box_inner .block2_inner").html("");
        $.ajax({
            url: link + "import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID=" + KhoID,
            success: function (result) {
                if (result != 'false') {
                    $(".over_lay .box_inner").css({'margin-top': '50px'});
                    $(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
                    $(".over_lay .box_inner .block2_inner").html(result);
                    $(".over_lay").removeClass('in');
                    $(".over_lay").fadeIn('fast');
                    $(".over_lay").addClass('in');
                } else {
                    disablescrollsetup();
                    alert("Kho hàng hiện tại không có sản phẩm hoặc đã hết hàng .");
                }
                $(this).removeClass('saving');
            }
        });
    });

    function input_search_products(ob) {
        var data = $(ob).val();
        var KhoID = $('#KhoID').val();
        $.ajax({
            url: link + "import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID=" + KhoID + "&Title=" + data,
            success: function (result) {
                if (result != 'false') {
                    $(".over_lay .box_inner .block2_inner").html(result);
                } else {
                    alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });
    }

    $.ajax({
        url: link + "import_order/get_targets_user",
        dataType: "html",
        type: "POST",
        context: this,
        data: "UserID=<?php echo $this->user->ID ?>",
        success: function (result) {
            if (result != 'fasle')
                $(".table_absolute").html(result);
        }
    });

    $("#Khuvuc").change(function () {
        var ID = $(this).val();
        if (ID != '') {
            $.ajax({
                url: link + "import_order/get_city_by_area",
                dataType: "html",
                type: "POST",
                context: this,
                data: "ID=" + ID,
                success: function (result) {
                    $("#Tinhthanh").html(result);
                    $("#KhoID").html('<option value="">Không có kho hàng</option>');
                }
            });
        }
    });

    $("#Tinhthanh").change(function () {
        var ID = $(this).val();
        if (ID != '') {
            $.ajax({
                url: link + "import_order/get_district_by_city",
                dataType: "html",
                type: "POST",
                context: this,
                data: "ID=" + ID,
                success: function (result) {
                    var jsonobj = JSON.parse(result);
                    $("#Quanhuyen").html(jsonobj.DistrictHtml);
                    $("#KhoID").html(jsonobj.WarehouseHtml);
                }
            });
        }
    });

    var statusloadhis = 0;
    $("#xemlichsugiaodich").click(function () {
        var datastatus = $("#xemlichsugiaodich").attr("data-status");
        if (datastatus == "down") {
            $("#xemlichsugiaodich").attr("data-status", "up");
            $(".lichsugiaodich").removeClass('hidden');
            $(".history_cus").removeClass('hidden');
            $(this).html("Ẩn lịch sử giao dịch <i class='fa fa-caret-up'></i>");
        } else {
            $("#xemlichsugiaodich").attr("data-status", "down");
            $(".history_cus").addClass('hidden');
            $(this).html("Xem lịch sử giao dịch <b class='caret'></b>");
        }
        var salesman = $("#SalesmanID").val();
        var ID = $("#CustomerID").val();
        if (ID != '' && ID > 0) {
            if (statusloadhis == 0) {
                $.ajax({
                    url: link + "import_order/get_history",
                    dataType: "html",
                    type: "POST",
                    context: this,
                    data: "ID=" + ID + "&UserID=" + salesman,
                    success: function (result) {
                        statusloadhis = 1;
                        if (result != 'false') {
                            $(".lichsugiaodich .history_cus").html(result);
                        } else {
                            $(".lichsugiaodich .history_cus").html("<p>Không có lịch sử giao dịch của khách hàng này.</p>");
                        }
                    }
                });
            }
        }
    });

    $("#edit_row_table").click(function () {
        $(this).parent('li').parent('ul').toggle();
        $("#table_data .selected_products").each(function () {
            if (this.checked === true) {
                var parent = $(this).parent('td').parent('tr');
                parent.find('input[type="text"]').prop('readonly', false);
                parent.find('input.soluong').prop('readonly', false);
                parent.find('input[type="text"]').addClass('border');
                parent.find('input.soluong').addClass('border');
            }
        });
    });

    function calchietkhau(ob) {
        tongcong = parseInt($(".tongcong").val());
        var classname = $(ob).attr('class');
        data = parseInt($(ob).val());
        if (classname == "phantramchietkhau") {
            giachietkhau = data * (tongcong / 100);
            $(".giachietkhau").val(giachietkhau);
            $("#showgiachietkhau").html(giachietkhau.format('a', 3));
        } else {
            phantramchietkhau = data / (tongcong / 100);
            $(".phantramchietkhau").val(phantramchietkhau.toFixed(0));
        }
        recal();
    }

    function calrowchietkhau(ob) {
        var parent = $(ob).parent('td').parent('tr');
        tongcong = parseInt(parent.find('input.dongia').val());
        var classname = $(ob).attr('class');
        data = parseInt($(ob).val());
        if (classname == "percentck border") {
            giachietkhau = data * (tongcong / 100);
            var giasauchietkhau = tongcong - giachietkhau;
            if (giasauchietkhau >= 0) {
                parent.find('input.giack').val(giachietkhau);
                parent.find('span.showgiaCK').html(giachietkhau.format('a', 3));
                parent.find('span.showpercentCK').html(data.format('a', 0));
            }
        } else {
            var giasauchietkhau = tongcong - data;
            if (giasauchietkhau >= 0) {
                phantramchietkhau = Math.round((data / (tongcong / 100)) * 10) / 10;
                parent.find('input.percentck').val(phantramchietkhau);
                parent.find('span.showpercentCK').html(phantramchietkhau);
                parent.find('span.showgiaCK').html(data.format('a', 0));
            }
        }
        if (giasauchietkhau >= 0) {
            parent.find('span.showgiasauCK').html(giachietkhau.format('a', 3));
            parent.find('input.giasauCK').val(giachietkhau);
            recal();
        }
    }

    function recal() {

        chiphivanchuyen = parseInt($(".chiphivanchuyen").val());
        $(".chiphivanchuyen").hide();
        $("#showchiphivanchuyen").html(chiphivanchuyen.format('a', 3)).show();
        var tongcong = 0;
        var phantramchietkhau = parseInt($(".phantramchietkhau").val());
        var chiphivanchuyen = parseInt($(".chiphivanchuyen").val());
        $("#table_data .selected_products").each(function () {
            var parent = $(this).parent('td').parent('tr');
            soluong = parseInt(parent.find('input.soluong').val());
            dongia = parseInt(parent.find('input.dongia').val());
            percentck = parseInt(parent.find('input.percentck').val());
            giack = parseInt(parent.find('input.giack').val());
            giasauck = dongia - giack;
            thanhtien = soluong * giasauck;
            parent.find('input.thanhtien').val(thanhtien);
            parent.find('span.showthanhtien').html(thanhtien.format('a', 3));
            parent.find('input.giasauCK').val(giasauck);
            parent.find('span.showgiasauCK').html(giasauck.format('a', 3));
            tongcong = tongcong + thanhtien;
        });
        giachietkhau = parseInt($(".giachietkhau").val());
        $(".tongcong").val(tongcong);
        $(".tongtienhang").val(tongcong - giachietkhau);
        $(".tonggiatrithanhtoan").val(tongcong - giachietkhau + chiphivanchuyen);
        $("#showtongcong").html(tongcong.format('a', 3));
        $("#showtongtienhang").html((tongcong - giachietkhau).format('a', 3));
        $("#showgiatrithanhtoan").html((tongcong - giachietkhau + chiphivanchuyen).format('a', 3));
    }

    function checkfull(ob) {
        if (ob.checked === true) {
            $("#table_data .selected_products").each(function () {
                var parent = $(this).parent('td').parent('tr');
                parent.find('span.showsoluong').hide();
                //parent.find('span.showdongia').hide();
                parent.find('span.showpercentCK').hide();
                parent.find('span.showgiaCK').hide();
                parent.find('input[type="text"]').prop('readonly', false);
                parent.find('input.soluong').prop('readonly', false);
                parent.find('input.dongia').prop('readonly', true).show();
                parent.find('input.percentck').prop('readonly', false).show();
                parent.find('input.giack').prop('readonly', false).show();
                parent.find('input[type="text"]').addClass('border');
                parent.find('input.soluong').addClass('border');
                parent.find('input.dongia').addClass('border');
                parent.find('input.percentck').addClass('border');
                parent.find('input.giack').addClass('border');
                parent.find('input.giasauck').addClass('border');
                parent.find('input[type="checkbox"]').prop("checked", true);
            });
        } else {
            $("#table_data .selected_products").each(function () {
                var parent = $(this).parent('td').parent('tr');
                parent.find('span.showsoluong').show();
                //parent.find('span.showdongia').show();
                parent.find('span.showpercentCK').show();
                parent.find('span.showgiaCK').show();
                parent.find('input[type="text"]').prop('readonly', true);
                parent.find('input.soluong').prop('readonly', true);
                parent.find('input.dongia').prop('readonly', true).hide();
                parent.find('input.percentck').prop('readonly', true).hide();
                parent.find('input.giack').prop('readonly', true).hide();
                parent.find('input[type="text"]').removeClass('border');
                parent.find('input.soluong').removeClass('border');
                parent.find('input.dongia').removeClass('border');
                parent.find('input.percentck').removeClass('border');
                parent.find('input.giack').removeClass('border');
                parent.find('input.giasauck').removeClass('border');
                parent.find('input[type="checkbox"]').prop("checked", false);
            });
        }
    }

    function changerow(ob) {
        var parent = $(ob).parent('td').parent('tr');
        if (ob.checked === true) {
            parent.find('span.showsoluong').hide();
            //parent.find('span.showdongia').hide();
            parent.find('span.showpercentCK').hide();
            parent.find('span.showgiaCK').hide();
            parent.find('input[type="text"]').prop('readonly', false);
            parent.find('input.soluong').prop('readonly', false);
            //parent.find('input.dongia').prop('readonly', true).show();
            parent.find('input.percentck').prop('readonly', true).show();
            parent.find('input.giack').prop('readonly', true).show();
            parent.find('input[type="text"]').addClass('border');
            parent.find('input.soluong').addClass('border');
            parent.find('input.dongia').addClass('border');
            parent.find('input.percentck').addClass('border');
            parent.find('input.giack').addClass('border');
            parent.find('input.giasauck').addClass('border');
        } else {
            parent.find('span.showsoluong').show();
            //parent.find('span.showdongia').show();
            parent.find('span.showpercentCK').show();
            parent.find('span.showgiaCK').show();
            parent.find('input[type="text"]').prop('readonly', true);
            parent.find('input.soluong').prop('readonly', true);
            parent.find('input.dongia').prop('readonly', true).hide();
            parent.find('input.percentck').prop('readonly', true).hide();
            parent.find('input.giack').prop('readonly', true).hide();
            parent.find('input[type="text"]').removeClass('border');
            parent.find('input.soluong').removeClass('border');
            parent.find('input.dongia').removeClass('border');
            parent.find('input.percentck').removeClass('border');
            parent.find('input.giack').removeClass('border');
            parent.find('input.giasauck').removeClass('border');
        }
    }

    function select_this_custom(ob, id, birthday, age, phone1, phone2, address) {
        $(".over_lay").hide();
        disablescrollsetup();
        var name = $(ob).parent('td').parent('tr').find('td.name_td a').html();
        $("#CustomerID").val(id);
        $("#Tenkhachhang").val(name);
        $("#NTNS").val(birthday);
        $("#Age").val(age);
        $("#Phone1").val(phone1);
        $("#Phone2").val(phone2);
        $(".lichsugiaodich").removeClass('hidden');
        $("#Address_order").val(address);
        set_city(id);
        setsource(id);
    }

    function setsource(id) {
        $.ajax({
            url: link + "import_order/set_source",
            dataType: "html",
            type: "POST",
            context: this,
            data: "ID=" + id,
            success: function (result) {
                $("#SourceID").val(result);
            }
        });
    }

    var celldata = [];
    var sttrow = 1;
    function add_products() {
        $(".over_lay .box_inner .block2_inner .selected_products").each(function () {
            if (this.checked === true) {
                var data_name = $(this).attr('data-name');
                var data_price = $(this).attr('data-price');
                var data_code = $(this).attr('data-code');
                var data_id = $(this).attr('data-id');
                if (jQuery.inArray("data" + data_id, celldata) < 0) {
                    var table = document.getElementById("table_data");
                    var row = table.insertRow(sttrow);
                    row.insertCell(0).innerHTML = "<input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='" + data_id + "' /><input type='hidden' name='ProductsID[]' id='ProductsID' value='" + data_id + "' />";
                    row.insertCell(1).innerHTML = data_code;
                    row.insertCell(2).innerHTML = data_name;
                    row.insertCell(3).innerHTML = "<span class='ShipmentDefault' data-id='" + data_id + "'></span>";
                    data_price = parseInt(data_price);
                    row.insertCell(4).innerHTML = "<input type='number' class='dongia' value='" + data_price + "' min='1' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>" + data_price.format('a', 3) + "</span>";
                    row.insertCell(5).innerHTML = "<input type='number' name='Amout[]' class='soluong' value='1' min='1' onchange='checkshipment(this)' readonly='true' />";
                    row.insertCell(6).innerHTML = "<input type='number' name='PercentCK[]' onchange='calrowchietkhau(this)' class='percentck' value='0' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
                    row.insertCell(7).innerHTML = "<input type='number' name='GiaCK[]' onchange='calrowchietkhau(this)' class='giack' value='0' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
                    row.insertCell(8).innerHTML = "<input type='number' name='Price[]' class='giasauCK' value='" + data_price + "' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>" + data_price.format('a', 3) + "</span>";
                    row.insertCell(9).innerHTML = "<input type='number' class='thanhtien' value='" + data_price + "' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>" + data_price.format('a', 3) + "</span>";
                    sttrow = sttrow + 1;
                    celldata.push("data" + data_id);
                }
            }
        });
        loadshipmentdefault();
        $(".over_lay").hide();
        disablescrollsetup();
        recal();
    }

    function accept_oldcustomer(customer) {
        $("#CustomerID").val(customer);
        document.getElementById("Loaikhachhangcu").checked = true;
        $("#submit_order").submit();
    }

    function checkshipment(ob) {
        $('.thisUsed').click();
        var amount = $(ob).val();
        var warehouse = $('#KhoID').val();
        var id = $(ob).parent('td').parent('tr').find('.ShipmentDefault').attr('data-id');
        $(ob).parent('td').parent('tr').find('.ShipmentDefault').load(link + "import_order/get_shipment_default/" + id + "/" + warehouse + "/" + amount);
        recal();
        
    }

    function loadshipmentdefault() {
        var warehouse = $('#KhoID').val();
        $(".ShipmentDefault").each(function () {
            var data = $(this).attr('data-id');
            var amount = $(this).parent('td').parent('tr').find('.soluong').val();
            $(this).load(link + "import_order/get_shipment_default/" + data + "/" + warehouse + "/" + amount);
        });
    }

    $("#delete_row_table").click(function () {
        $(this).parent('li').parent('ul').toggle();
        $("#table_data .selected_products").each(function () {
            if (this.checked === true) {
                var data_id = $(this).attr('data-id');
                $(this).parent('td').parent('tr').remove();
                var index = celldata.indexOf("data" + data_id);
                celldata.splice(index, 1);
                sttrow = sttrow - 1;
            }
        });
        recal();
    });

    $("#buttonsubmit").click(function () {
        $("#Status").val(2);
        var nvbanhang = $("#SalesmanID").val();
        if (nvbanhang == '') {
            $("#SalesmanID").focus();
            alert("Vui lòng chọn nhân viên bán hàng");
            return false;
        }
        var chanel = $("#KenhbanhangID").val();
        if (chanel == 0) {
            alert("Vui lòng chọn kênh bán hàng");
            return false;
        }
        var Tinhthanh = $("#Tinhthanh").val();
        if (Tinhthanh == '') {
            alert("Vui lòng chọn tỉnh thành");
            return false;
        }
        var Quanhuyen = $("#Quanhuyen").val();
        if (Quanhuyen == '') {
            alert("Vui lòng chọn quận huyện");
            return false;
        }

        var Tinhtrangdonhang = $("#Tinhtrangdonhang").val();
        if (Tinhtrangdonhang == 1 || Tinhtrangdonhang == '1') {
            var ghichu = $("#ghichu").val();
            if (ghichu == '') {
                alert("Vui lòng điền ghi chú nếu đơn hàng thuộc trạng thái bị hủy");
                return false;
            }
        }
        if (celldata.length == 0) {
            alert("Đơn hàng đang rỗng .Vui lòng thêm sản phẩm vào đơn hàng .");
            return false;
        }

        loaikhachhang = 0;
        $(".Loaikhachhang").each(function () {
            if ($(this).prop("checked") == true) {
                loaikhachhang = $(this).val();
            }
        });
        if (loaikhachhang == 0) {
            $(this).addClass("saving");
            $(this).find("i").hide();
            var sdt_cus = $("#Phone1").val();
            $.ajax({
                url: link + "import_order/checkphone",
                dataType: "html",
                type: "POST",
                context: this,
                data: '&Phone1=' + sdt_cus,
                success: function (result) {
                    $(this).removeClass("saving");
                    $(this).find("i").show();
                    if (result != 'false') {
                        enablescrollsetup();
                        $(".over_lay").show();
                        $(".over_lay .box_inner .block1_inner h1").html("Cảnh báo đã tồn tại khách hàng");
                        $(".over_lay .box_inner .block2_inner").html(result);
                        $(".over_lay").removeClass('in');
                        $(".over_lay").fadeIn('fast');
                        $(".over_lay").addClass('in');
                        return false;
                    } else {
                        $("#submit_order").submit();
                    }
                }
            });
        } else {
            $("#submit_order").submit();
        }
    });

    function skip(ob) {
        $("#Status").val(3);
        var nvbanhang = $("#SalesmanID").val();
        if (nvbanhang == '') {
            $("#SalesmanID").focus();
            alert("Vui lòng chọn nhân viên bán hàng");
            return false;
        }
        var chanel = $("#KenhbanhangID").val();
        if (chanel == 0) {
            alert("Vui lòng chọn kênh bán hàng");
            return false;
        }
        var Tinhthanh = $("#Tinhthanh").val();
        if (Tinhthanh == '') {
            alert("Vui lòng chọn tỉnh thành");
            return false;
        }
        var Quanhuyen = $("#Quanhuyen").val();
        if (Quanhuyen == '') {
            alert("Vui lòng chọn quận huyện");
            return false;
        }

        var Tinhtrangdonhang = $("#Tinhtrangdonhang").val();
        if (Tinhtrangdonhang == 1 || Tinhtrangdonhang == '1') {
            var ghichu = $("#ghichu").val();
            if (ghichu == '') {
                alert("Vui lòng điền ghi chú nếu đơn hàng thuộc trạng thái bị hủy");
                return false;
            }
        }
        if (celldata.length == 0) {
            alert("Đơn hàng đang rỗng .Vui lòng thêm sản phẩm vào đơn hàng .");
            return false;
        }

        loaikhachhang = 0;
        $(".Loaikhachhang").each(function () {
            if ($(this).prop("checked") == true) {
                loaikhachhang = $(this).val();
            }
        });
        if (loaikhachhang == 0) {
            $(ob).addClass("saving");
            $(ob).find("i").hide();
            var sdt_cus = $("#Phone1").val();
            $.ajax({
                url: link + "import_order/checkphone",
                dataType: "html",
                type: "POST",
                context: ob,
                data: '&Phone1=' + sdt_cus,
                success: function (result) {
                    if (result != 'false') {
                        $(ob).removeClass("saving");
                        $(ob).find("i").show();
                        enablescrollsetup();
                        $(".over_lay").show();
                        $(".over_lay .box_inner .block1_inner h1").html("Cảnh báo đã tồn tại khách hàng");
                        $(".over_lay .box_inner .block2_inner").html(result);
                        $(".over_lay").removeClass('in');
                        $(".over_lay").fadeIn('fast');
                        $(".over_lay").addClass('in');
                        return false;
                    } else {
                        $("#submit_order").submit();
                    }
                }
            });
        } else {
            $(ob).find("i").hide();
            $(ob).addClass('saving');
            $("#submit_order").submit();
        }
    }

    $("#ortherUser").click(function () {
        $(".userteam").toggle();
        var datastatus = $(this).attr('data-status');
        if (datastatus == 'down') {
            $(this).attr('data-status', 'up');
            $(this).html("Làm ĐH cho NV khác <i class='fa fa-caret-up'></i>");
        } else {
            $(this).attr('data-status', 'down');
            $(this).html("Làm ĐH cho NV khác <b class='caret'></b>");
        }
    });

    function showchiphi() {
        $("#showchiphivanchuyen").hide();
        $(".chiphivanchuyen").show();
        $(".chiphivanchuyen").focus();
    }

    function changeUserCreatedOrder(ob, ID) {
        $(ob).parent().parent().hide();
        var username = $(ob).attr('data-user');
        $("#SalesmanID").html("<option value='" + ID + "'>" + username + "</option>");
        $("#UserCreated").val(1);
        if (ID != '') {
            $.ajax({
                url: link + "import_order/get_targets_user",
                dataType: "html",
                type: "POST",
                context: this,
                data: "UserID=" + ID,
                success: function (result) {
                    if (result != 'fasle')
                        $(".table_absolute").html(result);
                }
            });
        }
    }

    Number.prototype.format = function (n, x) {
        var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
    };

    function fillter_categories(ob) {
        var data = $(ob).val();
        if (data == 0) {
            $(ob).parent().parent().parent().find("tr.trcategories").show();
        } else {
            $(ob).parent().parent().parent().find("tr.trcategories").hide();
            $(ob).parent().parent().parent().find("tr.categories_" + data).show();
        }
    }

    function enablescrollsetup() {
        $(window).scrollTop(70);
        $("body").css({'height': '100%', 'overflow-y': 'hidden'});
        h = window.innerHeight;
        h = h - 200;
        $(".over_lay .box_inner .block2_inner").css({"max-height": h + "px"});
    }

    function disablescrollsetup() {
        $("body").css({'height': 'auto', 'overflow-y': 'scroll'});
    }

    function set_city(id) {
        $.ajax({
            url: link + "import_order/get_last_order_city/" + id,
            dataType: "text",
            success: function (result) {
                var jsonobj = JSON.parse(result);
                $("#Khuvuc").val(jsonobj.AreaID);
                $("#Tinhthanh").html(jsonobj.CityHtml);
                $("#Quanhuyen").html(jsonobj.DistrictHtml);
                $("#KhoID").html(jsonobj.WarehouseHtml);
            }
        });
    }
</script>