<div class="containner">
	<div class="disableedit" style="height:1000px"></div>
	<div class="status_progress">
    	<?php 
    	$status1 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point1 = $status1=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point1 = $data->Status==3 || $data->Status==5 || $data->Status==7  || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point1 ;

		$status2 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point2 = $status2=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point2 =  $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point2 ;    	

    	$status3 = $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point3 = $status3=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point3 =  $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point3 ;

    	$status4 = $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point4 = $status4=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point4 =  $data->Status==7 || $data->Status==8 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point4 ;

		$status5 = $data->Status==8 || $data->Status==7 || $data->Status==0 ? 'class="active"' : "";
    	$point5 = $status5=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point5 =  $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point5 ;    	
    	
    	$width = $data->Status==2 || $data->Status==4 || $data->Status==6 ? 20 : 0 ;
    	$width = $data->Status==3 ? 40 : $width ;
    	$width = $data->Status==5 ? 60 : $width ;
    	$width = $data->Status==9 ? 80 : $width ;
    	$width = $data->Status==7 || $data->Status==0 || $data->Status==8 ? 100 : $width ;

    	if($data->Status==1 || $data->Status==8){
    		$title_drop = $data->Status==8 ? "Bị trả về" : "Bị hủy" ;
    		echo "<div class='orderisdrop'><a>$title_drop</a><span><i class='fa fa-times'></i></span></div>";
    		echo '<div class="transfer_progress" style="width:100%;background:#000"></div>';
    	}else{
    	?>
    	<ul>
            <li class="active"><a>ĐH nháp</a><span class="ok"><i class="fa fa-check-circle"></i></span></li>
            <li <?php echo $status1 ?>><a>Đã chuyển sang kho</a><?php echo $point1 ?></li>
            <li <?php echo $status2 ?>><a>Kho xác nhận</a><?php echo $point2 ?></li>
            <li <?php echo $status3 ?>><a>Kế toán xác nhận</a><?php echo $point3 ?></li>
            <li <?php echo $status4 ?>><a>Đã giao vận chuyển</a><?php echo $point4 ?></li>
            <li <?php echo $status5 ?>><a>Hoàn tất</a><?php echo $point5 ?></li>
        </ul>
        <div class="transfer_progress" style="width:<?php echo $width ?>%"></div>
        <?php 
    	}
        ?>
    </div>
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1><a href="<?php echo $base_link ?>">THÔNG TIN ĐƠN HÀNG</a> - <a style="color:#DE5252"><?php echo $data->MaDH ?></a></h1>
	    </div>
	    <div class="block2"></div>
    </div>
    
    <div class="import_order_info">
    		<input type='hidden' name="Tinhtrangdonhang" id="Status" value='<?php echo $data->Status ?>' />
    		<input type="hidden" name="IDOrder" value="<?php echo $data->ID ?>" />
	    	<div class="block1">
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Tên NV bán hàng</label>
	    				<label class="col-xs-7 control-label">: <?php echo $data->UserName ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Ngày bán</label>
	    				<label class="col-xs-7 control-label">: <?php echo date('m/d/Y',strtotime($data->Ngaydathang)); ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Kho xuất hàng</label>
	    				<?php 
	    				$khoresult = $this->db->query("select MaKho from ttp_report_warehouse where ID=$data->KhoID")->row();
	    				?>
	    				<label class="col-xs-7 control-label">: <?php echo $khoresult ? $khoresult->MaKho : '--' ; ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Tên khách hàng</label>
	    				<label class="col-xs-7 control-label">: <?php echo $data->Name ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Loại khách hàng</label>
	    				<label class="col-xs-7 control-label">: <?php echo $data->CustomerType==0 ? "Khách hàng mới" : "Khách hàng cũ" ; ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Số di động 1</label>
	    				<label class="col-xs-7 control-label">: <?php echo $data->Phone1 ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Số di động 12</label>
	    				<label class="col-xs-7 control-label">: <?php echo $data->Phone2 ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
		    		<div class="col-xs-12" style="margin-top:10px">
	    				<div class="form-group">
	    					<div class='lichsugiaodich' style="position: relative;z-index: 999;"><div class="history_cus"></div><a id='xemlichsugiaodich' data-status="down">Xem lịch sử giao dịch <b class='caret'></b></a></div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Nguồn</label>
	    				<?php 
	    				$source = $this->db->query("select * from ttp_report_source where ID=$data->SourceID")->row();
	    				$Source_Title = $source ? $source->Title : '--' ;
	    				?>
	    				<label class="col-xs-7 control-label">: <?php echo $Source_Title ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Kênh bán hàng</label>
	    				<?php 
						$kenhbanhang = $this->db->query("select * from ttp_report_saleschannel where ID=$data->KenhbanhangID")->row();
						$KenhTitle = $kenhbanhang ? $kenhbanhang->Title : '--' ;
	    				?>
	    				<label class="col-xs-7 control-label">: <?php echo $KenhTitle ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">PT thanh toán</label>
	    				<?php 
	    				$payment = $data->Payment==1 ? 'Chuyển khoản' : 'COD' ;
	    				?>
	    				<label class="col-xs-7 control-label">: <?php echo $payment ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<?php 
	    			$area = $this->db->query("select a.Title,b.Title as AreaTitle,c.Title as DistrictTitle from ttp_report_city a,ttp_report_area b,ttp_report_district c where a.ID=c.CityID and a.AreaID=b.ID and a.ID=$data->CityID")->row();
	    			?>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Khu vực</label>
	    				<label class="col-xs-7 control-label">: <?php echo $area ? $area->AreaTitle : '--' ; ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Tỉnh / Thành</label>
	    				<label class="col-xs-7 control-label">: <?php echo $area ? $area->Title : '--' ; ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Quận / Huyện</label>
	    				<label class="col-xs-7 control-label">: <?php echo $area ? $area->DistrictTitle : '--' ; ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Địa chỉ giao hàng</label>
	    				<label class="col-xs-7 control-label">: <?php echo $data->AddressOrder ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class='form-group'>
	    				<label class="col-xs-5 control-label">Ghi chú giao hàng</label>
	    				<label class="col-xs-7 control-label">: <?php echo $data->Note !='' ? $data->Note : '--' ; ?></label>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
			<h2 style='font-size:20px;font-weight:bold;margin-top:20px'>CHI TIẾT ĐƠN HÀNG</h2>
	    	<div class="table_donhang table_data">
	    		<form id="tableform">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th>STT</th>
		    				<th>Mã sản phẩm</th>
		    				<th>Tên sản phẩm</th>
		    				<th>Lô</th>
		    				<th>Giá bán</th>
		    				<th>Số lượng</th>
		    				<th>% CK</th>
		    				<th>Giá trị CK</th>
		    				<th>Giá sau CK</th>
		    				<th>Thành tiền</th>
		    			</tr>
		    			<?php 
		    			$details = $this->db->query("select a.*,b.Title,b.MaSP,b.CategoriesID,b.VariantType from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
		    			$arrproducts = array();
		    			$temp = 1;
		    			if(count($details)>0){
		    				foreach($details as $row){
		    					$Shipment = $this->db->query("select * from ttp_report_shipment where ID=$row->ShipmentID")->row();
		    					$ShipmentCode = $Shipment ? $Shipment->ShipmentCode : '--' ;
		    					$ShipmentCode = $row->VariantType==2 && $this->user->UserType==2 ? "<a onclick='viewcombo(this,$row->ID)'><i class='fa fa-table' aria-hidden='true'></i> Xem combo</a>" : $ShipmentCode ;
		    					echo "<tr>";
		    					echo "<td>$temp</td>";
		    					$temp++;
		    					echo "<td>$row->MaSP</td>";
		    					echo "<td>$row->Title</td>";
		    					echo "<td style='width:180px;text-align:left'><span class='ShipmentDefault' data-id='$row->ProductsID'>$ShipmentCode</span></td>";
			    				$giaban = $row->Price+$row->PriceDown;
		    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
		    					echo "<td><input type='number' class='dongia' value='$giaban' min='0' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>".number_format($giaban)."</span></td>";
		    					echo "<td><input type='number' name='Amount[]' class='soluong' value='$row->Amount' min='1' onchange='recal()' readonly='true' /></td>";
		    					echo "<td><input type='number' onchange='calrowchietkhau(this)' class='percentck' value='$phantramck' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>$phantramck</span></td>";
								echo "<td><input type='number' onchange='calrowchietkhau(this)' class='giack' value='$row->PriceDown' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->PriceDown)."</span></td>";
								echo "<td><input type='number' class='giasauCK' value='$row->Price' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Price)."</span></td>";
								echo "<td><input type='number' class='thanhtien' value='$row->Total' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Total)."</span></td>";
		    					echo "</tr>";
		    					$arrproducts[] = '"data'.$row->ProductsID.'"';
		    				}
		    			}
		    			
		    			$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
		    			$tongtienhang = $data->Total - $data->Chietkhau;
		    			$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi;
		    			?>
		    			<tr class="last">
		    				<td colspan="9">Tổng cộng</td>
		    				<td><input type="number" class="tongcong" readonly="true" value="<?php echo $data->Total ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongcong"><?php echo number_format($data->Total) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">% chiết khấu</td>
		    				<td><input type='number' class="phantramchietkhau" min="0" max="100" value="<?php echo $phantramchietkhau ?>" onchange="calchietkhau(this)" /></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Giá chiết khấu</td>
		    				<td><input type='number' class="giachietkhau" min="1" value="<?php echo $data->Chietkhau ?>" readonly="true" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiachietkhau"><?php echo number_format($data->Chietkhau) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Tổng tiền hàng</td>
		    				<td><input type="number" class="tongtienhang" readonly="true" value="<?php echo $tongtienhang ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongtienhang"><?php echo number_format($tongtienhang) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Chi phí vận chuyển</td>
		    				<td><input type='number' class="chiphivanchuyen" value="<?php echo $data->Chiphi ?>" onchange="recal()" style="display:none" /><span id="showchiphivanchuyen" style="display:block;text-align:right;padding-right:15px;" onclick="showchiphi()"><?php echo number_format($data->Chiphi) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Tổng giá trị thanh toán</td>
		    				<td><input type='number' class="tonggiatrithanhtoan" readonly="true" value="<?php echo $tonggiatrithanhtoan ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiatrithanhtoan"><?php echo number_format($tonggiatrithanhtoan) ?></span></td>
		    				
		    			</tr>
		    		</table>
		    		<input type="hidden" name="OrderID" value="<?php echo $data->ID ?>" />
	    		</form>
	    	</div>
	    	<div class="history_status">
	    		<h2>Lịch sử trạng thái đơn hàng</h2>
	    		<?php 
	    		$history = $this->db->query("select a.*,b.UserName from ttp_report_orderhistory a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
	    		if(count($history)>0){
	    			$array_status = array(
	                    10=>'Chờ xác nhận hủy',
	                    9=>'Chờ nhân viên điều phối',
	                    8=>'Đơn hàng bị trả về',
	                    7=>'Chuyển cho giao nhận',
	                    6=>'Đơn hàng bị trả về từ kế toán',
	                    5=>'Đơn hàng chờ kế toán duyệt',
	                    4=>'Đơn hàng bị trả về từ kho',
	                    3=>'Đơn hàng mới chờ kho duyệt',
	                    2=>'Đơn hàng nháp',
	                    0=>'Đơn hàng thành công',
	                    1=>'Đơn hàng hủy'
	                );
	    			echo "<table><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
	    			foreach($history as $row){
	    				echo "<tr>";
	    				echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>" ;
	    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
	    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
	    				echo "<td>$row->UserName</td>";
	    				echo "</tr>";
	    			}
	    			echo "</table>";
	    		}
	    		?>
	    	</div>
	    	<div class="block1">
	    		<?php 
	    		if($this->user->UserType==3 || $this->user->UserType==4){
	    		?>
	    		<div class="row row13">
	    			<span class="title">Kho xuất: </span>
	    			<span>
	    			<?php 
	    				$kho = $this->db->query("select b.Title as KhoTitle,b.MaKho from ttp_report_warehouse b where b.ID=$data->KhoID")->row();
	    				echo $kho ? $kho->KhoTitle." ($kho->MaKho)" : "Chưa chọn kho xuất" ;
	    			?>
	    			 </span>
	    		</div>
	    		<?php } ?>
	    		<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
	    		<input type="hidden" id="referer" value="<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ; ?>" />
	    	</div>
    </div>
    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
	var link = $("#baselink_report").val();
	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-230;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function viewcombo(ob,ID){
		if(ID>0){
			enablescrollsetup();
			$(".over_lay .box_inner .block2_inner").html("");
			$.ajax({
	        	url: link+"import_order/get_combo_details",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "DetailsID="+ID,
	            success: function(result){
	            	if(result!="FALSE"){
	            		$(".over_lay .box_inner").css({'margin-top':'20px'});
				    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách chi tiết COMBO");
				    	$(".over_lay .box_inner .block2_inner").html(result);
				    	$(".over_lay").removeClass('in');
				    	$(".over_lay").fadeIn('fast');
				    	$(".over_lay").addClass('in');
	                }else{
	                	alert("Có lỗi xảy ra trong quá trình truyền tải dữ liệu !. Vui lòng kiểm tra lại đường truyền .");
	                }
	            }
	        });
        }
	}

	var statusloadhis = 0;
	$("#xemlichsugiaodich").click(function(){
		var datastatus = $("#xemlichsugiaodich").attr("data-status");
		if(datastatus=="down"){
			$("#xemlichsugiaodich").attr("data-status","up");
			$(this).html("Ẩn lịch sử giao dịch <i class='fa fa-caret-up'></i>");
		}else{
			$("#xemlichsugiaodich").attr("data-status","down");
			$(this).html("Xem lịch sử giao dịch <b class='caret'></b>");
		}
		var ID = <?php echo $data->CustomerID ?>;
		if(ID!='' && ID>0){
			if(statusloadhis==0){
				$.ajax({
	            	url: link+"import_order/get_history",
		            dataType: "html",
		            type: "POST",
		            context: this,
		            data: "ID="+ID+"&UserID=<?php echo $data->UserID ?>",
		            success: function(result){
		            	statusloadhis=1;
		                if(result!='false'){
			                $(".lichsugiaodich .history_cus").html(result);
		                }else{
		                	$(".lichsugiaodich .history_cus").html("<p>Không có lịch sử giao dịch của khách hàng này.</p>");
		                }
		            }
		        });
	        }
		}
	});
</script>