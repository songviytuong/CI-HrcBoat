<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$baselink = ADMINPATH.'/report/import_order/';
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1><a href='<?php echo base_url().ADMINPATH.'/report/import_order' ?>'>DANH SÁCH ĐƠN HÀNG</a> | <a style="color:#27c" href='<?php echo base_url().ADMINPATH.'/report/import_order/quick_view' ?>'>THỐNG KÊ NHANH</a></h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div <?php echo $this->user->UserType==8 ? "hidden" : '' ; ?>">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="quick_view">
        <div class="block1">
            <div style="margin-bottom:50px"></div>
            <canvas id="canvas_pie"></canvas>
        </div>
        <div class="block2">
            <h3>Theo trạng thái đơn hàng</h3>
            <?php 
            $arr = array();
            $arr_total = array('SLDH'=>0,'SLSP'=>0,'Total'=>0);
            $totalpercent = 0;
            $array_status = array(
                10=>'Đơn hàng chờ hủy',
                9=>'Đơn hàng chờ nhân viên điều phối',
                8=>'Đơn hàng bị trả về',
                7=>'Chuyển sang bộ phận giao hàng',
                6=>'Đơn hàng bị trả về từ kế toán',
                5=>'Đơn hàng chờ kế toán duyệt',
                4=>'Đơn hàng bị trả về từ kho',
                3=>'Đơn hàng mới chờ kho duyệt',
                2=>'Đơn hàng nháp',
                0=>'Đơn hàng thành công',
                1=>'Đơn hàng hủy'
            );
            $array_color = array("#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B");
            if(count($data)>0){
                foreach($data as $row){
                    $arr_total['SLDH'] = $arr_total['SLDH']+1;
                    $arr_total['SLSP'] = $arr_total['SLSP']+$row->SoluongSP;
                    $arr_total['Total'] = $arr_total['Total']+$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                    if(isset($arr[$row->Status])){
                        $arr[$row->Status]['SLDH'] = $arr[$row->Status]['SLDH']+1;
                        $arr[$row->Status]['SLSP'] = $arr[$row->Status]['SLSP']+$row->SoluongSP;
                        $arr[$row->Status]['Total'] = $arr[$row->Status]['Total']+$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                        $totalpercent += $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                    }else{
                        $arr[$row->Status] = array(
                            'SLDH'  =>1,
                            'SLSP'  =>$row->SoluongSP,
                            'Total' =>$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi
                        );
                    }
                }
            }
            $arr_chart = array();
            if(count($arr)>0){
                echo "<table>
                        <tr>
                            <th>Tình trạng</th>
                            <th>SL đơn hàng</th>
                            <th>SL sản phẩm</th>
                            <th>Tổng doanh số</th>
                            <th>Tỷ lệ %</th>
                        </tr>
                        ";
                foreach($arr as $key=>$value){
                    $percent = $totalpercent==0 ? 0 : round($value['Total']/($totalpercent/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[$key]."'></span><a href='{$baselink}setfillter_report?Status=$key'>".$array_status[$key]."</a></td>";
                    echo "<td>".number_format($value['SLDH'])."</td>";
                    echo "<td>".number_format($value['SLSP'])."</td>";
                    echo "<td>".number_format($value['Total'])."</td>";
                    echo "<td>".$percent."%</td>";
                    echo "</tr>";
                    $arr_chart[] = "{
                                        value: ".$value['SLDH'].",
                                        color: '".$array_color[$key]."',
                                        highlight: '".$array_color[$key]."',
                                        label: 'Số lượng ".$array_status[$key]."'
                                    }";
                }
                echo "<tr>
                        <th>Tổng cộng</th>
                        <th>".number_format($arr_total['SLDH'])."</th>
                        <th>".number_format($arr_total['SLSP'])."</th>
                        <th>".number_format($arr_total['Total'])."</th>
                        <th>100%</th>
                    </tr>";
                echo "</table>";
            }
            ?>
        </div>
        <div style="clear:both;width:100%"></div>
        <div class="block1 <?php echo $this->user->UserType==8 ? "hidden" : '' ; ?>">
            <div style="margin-bottom:200px"></div>
            <canvas id="canvas_Doughnut1"></canvas>
            <canvas id="canvas_Doughnut2"></canvas>
            <canvas id="canvas_Doughnut3"></canvas>
            <div class="color_view_chart">
            <?php 
                echo "<span class='current' style='background:".$array_color[6]."' onclick='showpie(this,1,\"Bán trong kỳ\")'></span>";
                echo "<span style='background:".$array_color[1]."' onclick='showpie(this,2,\"Hủy trong kỳ\")'></span>";
                echo "<span style='background:".$array_color[0]."' onclick='showpie(this,3,\"Tổng hợp giao dịch trong kỳ\")'></span>";
            ?>
            </div>
            <div class='current_view_chart'>Biểu đồ : <span>Bán trong kỳ</span></div>
        </div>
        <div class="block2 <?php echo $this->user->UserType==8 ? "hidden" : '' ; ?>">
            <h3>Theo doanh số</h3>
            <div class="row" style="margin-bottom: 10px;">
                <?php 
                $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
                ?>
                <div class="col-xs-4"><input type='checkbox' value="0" <?php echo $current_view==0 ? 'checked="checked"' : "" ; ?> onclick="change_viewtype(0)" /> Theo ngày đặt hàng</div>
                <div class="col-xs-8"><input type='checkbox' value="1" <?php echo $current_view==1 ? 'checked="checked"' : "" ; ?> onclick="change_viewtype(1)" /> Theo ngày cập nhật trạng thái</div>
            </div>
            <?php 

            $arr_chart1 = array();
            $arr_chart2 = array();
            $arr_chart3 = array();
            if(count($arr)>0){
                echo "<table>
                        <tr>
                            <th style='width:252px'>Chỉ tiêu</th>
                            <th>SL đơn hàng</th>
                            <th>SL sản phẩm</th>
                            <th>Tổng doanh số</th>
                            <th>Tỷ lệ %</th>
                        </tr>
                        ";
                $value0 = isset($arr[0]) ? $arr[0] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value1 = isset($arr[1]) ? $arr[1] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value2 = isset($arr[2]) ? $arr[2] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value3 = isset($arr[3]) ? $arr[3] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value4 = isset($arr[4]) ? $arr[4] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value5 = isset($arr[5]) ? $arr[5] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value6 = isset($arr[6]) ? $arr[6] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value7 = isset($arr[7]) ? $arr[7] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value8 = isset($arr[8]) ? $arr[8] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value9 = isset($arr[9]) ? $arr[9] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                $value10 = isset($arr[10]) ? $arr[10] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;

                $arr_chart1[] = "{
                                        value: ".$value9['SLDH'].",
                                        color: '".$array_color[6]."',
                                        highlight: '".$array_color[6]."',
                                        label: 'Số lượng ".$array_status[9]."'
                                    }";
                $arr_chart1[] = "{
                                        value: ".$value7['SLDH'].",
                                        color: '".$array_color[7]."',
                                        highlight: '".$array_color[7]."',
                                        label: 'Số lượng ".$array_status[7]."'
                                    }";
                $arr_chart1[] = "{
                                        value: ".$value0['SLDH'].",
                                        color: '".$array_color[0]."',
                                        highlight: '".$array_color[0]."',
                                        label: 'Số lượng ".$array_status[0]."'
                                    }";

                $percent = ($value9['Total']+$value7['Total']+$value0['Total'])==0 ? 0 : round($value9['Total']/(($value9['Total']+$value7['Total']+$value0['Total'])/100),1);
                echo "<tr><td colspan='5'><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[6]."'></span> Bán trong kỳ</td></tr>";
                echo "<tr>";
                echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=9'>".$array_status[9]."</a></td>";
                echo "<td>".number_format($value9['SLDH'])."</td>";
                echo "<td>".number_format($value9['SLSP'])."</td>";
                echo "<td>".number_format($value9['Total'])."</td>";
                echo "<td>$percent%</td>";
                echo "</tr>";

                $percent = ($value9['Total']+$value7['Total']+$value0['Total'])==0 ? 0 : round($value7['Total']/(($value9['Total']+$value7['Total']+$value0['Total'])/100),1);
                echo "<tr>";
                echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=7'>".$array_status[7]."</a></td>";
                echo "<td>".number_format($value7['SLDH'])."</td>";
                echo "<td>".number_format($value7['SLSP'])."</td>";
                echo "<td>".number_format($value7['Total'])."</td>";
                echo "<td>$percent%</td>";
                echo "</tr>";
                
                $percent = ($value9['Total']+$value7['Total']+$value0['Total'])==0 ? 0 : round($value0['Total']/(($value9['Total']+$value7['Total']+$value0['Total'])/100),1);
                echo "<tr>";
                echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=0'>".$array_status[0]."</a></td>";
                echo "<td>".number_format($value0['SLDH'])."</td>";
                echo "<td>".number_format($value0['SLSP'])."</td>";
                echo "<td>".number_format($value0['Total'])."</td>";
                echo "<td>$percent%</td>";
                echo "</tr>";
                
                echo "<tr>
                        <th><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span>Tổng cộng</th>
                        <th>".number_format($value9['SLDH']+$value7['SLDH']+$value0['SLDH'])."</th>
                        <th>".number_format($value9['SLSP']+$value7['SLSP']+$value0['SLSP'])."</th>
                        <th>".number_format($value9['Total']+$value7['Total']+$value0['Total'])."</th>
                        <th>100%</th>
                    </tr>";
                echo "<tr><td colspan='5'><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[1]."'></span> Hủy trong kỳ</td></tr>";
                $total_reason = array('SLDH'=>0,'SLSP'=>0,'Total'=>0);
                $i=0;
                if(count($reason)>0){
                    foreach($reason as $row){
                        $percent = $value1['Total']==0 ? 0 : round($row->Total/($value1['Total']/100),1);
                        echo "<tr>";
                        echo "<td style='padding-left:30px'>".$row->Title."</td>";
                        echo "<td>".number_format($row->SLDH)."</td>";
                        echo "<td>".number_format($row->SoluongSP)."</td>";
                        echo "<td>".number_format($row->Total)."</td>";
                        echo "<td>$percent%</td>";
                        echo "</tr>";
                        $total_reason['SLDH'] = $total_reason['SLDH']+$row->SLDH;
                        $total_reason['SLSP'] = $total_reason['SLSP']+$row->SoluongSP;
                        $total_reason['Total'] = $total_reason['Total']+$row->Total;
                        $arr_chart2[] = "{
                                        value: ".$total_reason['SLDH'].",
                                        color: '".$array_color[$i]."',
                                        highlight: '".$array_color[$i]."',
                                        label: 'Số lượng $row->Title'
                                    }";
                        $i++;
                    }
                }
                $temp_arr_chart2 = $value1['SLDH']-$total_reason['SLDH'];
                $arr_chart2[] = "{
                                        value: ".$temp_arr_chart2.",
                                        color: '".$array_color[$i]."',
                                        highlight: '".$array_color[$i]."',
                                        label: 'Số lượng không xác định'
                                    }";
                $percent =  ($value1['Total']-$total_reason['Total'])==0 ? 0 : round(($value1['Total']-$total_reason['Total'])/($value1['Total']/100),1);
                echo "<tr>";
                echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span> Không xác định</td>";
                echo "<td>".number_format($value1['SLDH']-$total_reason['SLDH'])."</td>";
                echo "<td>".number_format($value1['SLSP']-$total_reason['SLSP'])."</td>";
                echo "<td>".number_format($value1['Total']-$total_reason['Total'])."</td>";
                echo "<td>$percent%</td>";
                echo "</tr>";

                echo "<tr>
                        <th><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span>Tổng cộng</th>
                        <th>".number_format($value1['SLDH'])."</th>
                        <th>".number_format($value1['SLSP'])."</th>
                        <th>".number_format($value1['Total'])."</th>
                        <th>100%</th>
                    </tr>";

                echo "<tr><td colspan='5'><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[0]."'></span> Tổng hợp giao dịch trong kỳ</td></tr>";
                
                $arr_chart3[] = "{
                                        value: ".$value0['SLDH'].",
                                        color: '".$array_color[7]."',
                                        highlight: '".$array_color[7]."',
                                        label: 'Số lượng ".$array_status[0]."'
                                    }";
                $arr_chart3[] = "{
                                        value: ".$value1['SLDH'].",
                                        color: '".$array_color[1]."',
                                        highlight: '".$array_color[1]."',
                                        label: 'Số lượng ".$array_status[1]."'
                                    }";
                $arr_chart3_temp = $arr_total['SLDH']-$value0['SLDH']-$value1['SLDH'];
                $arr_chart3[] = "{
                                        value: ".$arr_chart3_temp.",
                                        color: '".$array_color[0]."',
                                        highlight: '".$array_color[0]."',
                                        label: 'Số lượng đơn hàng đang xử lý'
                                    }";

                $percent = $arr_total['Total']==0 ? 0 : round($value0['Total']/($arr_total['Total']/100),1);
                echo "<tr>";
                echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=0'>".$array_status[0]."</a></td>";
                echo "<td>".number_format($value0['SLDH'])."</td>";
                echo "<td>".number_format($value0['SLSP'])."</td>";
                echo "<td>".number_format($value0['Total'])."</td>";
                echo "<td>$percent%</td>";
                echo "</tr>";
                
                $percent = ($arr_total['Total']-$value0['Total']-$value1['Total'])==0 ? 0 : round(($arr_total['Total']-$value0['Total']-$value1['Total'])/($arr_total['Total']/100),1);
                echo "<tr>";
                echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span> <a href='{$baselink}setfillter_report?Status=processing'>Đơn hàng đang xử lý</a></td>";
                echo "<td>".number_format($arr_total['SLDH']-$value0['SLDH']-$value1['SLDH'])."</td>";
                echo "<td>".number_format($arr_total['SLSP']-$value0['SLSP']-$value1['SLSP'])."</td>";
                echo "<td>".number_format($arr_total['Total']-$value0['Total']-$value1['Total'])."</td>";
                echo "<td>$percent%</td>";
                echo "</tr>";

                $percent = $arr_total['Total']==0 ? 0 : round($value1['Total']/($arr_total['Total']/100),1);
                echo "<tr>";
                echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=1'>".$array_status[1]."</a></td>";
                echo "<td>".number_format($value1['SLDH'])."</td>";
                echo "<td>".number_format($value1['SLSP'])."</td>";
                echo "<td>".number_format($value1['Total'])."</td>";
                echo "<td>$percent%</td>";
                echo "</tr>";

                echo "<tr>
                        <th><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span>Tổng cộng</th>
                        <th>".number_format($arr_total['SLDH'])."</th>
                        <th>".number_format($arr_total['SLSP'])."</th>
                        <th>".number_format($arr_total['Total'])."</th>
                        <th>100%</th>
                    </tr>";
            }
            ?>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

    var sharePiePolorDoughnutData = [
        <?php 
        echo implode(',', $arr_chart);
        ?>
    ];

    var sharePiePolorDoughnutData1 = [
        <?php 
        echo implode(',', $arr_chart1);
        ?>
    ];

    var sharePiePolorDoughnutData2 = [
        <?php 
        echo implode(',', $arr_chart2);
        ?>
    ];

    var sharePiePolorDoughnutData3 = [
        <?php 
        echo implode(',', $arr_chart3);
        ?>
    ];


    $(document).ready(function () {
        window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(sharePiePolorDoughnutData, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_Doughnut1").getContext("2d")).Doughnut(sharePiePolorDoughnutData1, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_Doughnut2").getContext("2d")).Doughnut(sharePiePolorDoughnutData2, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_Doughnut3").getContext("2d")).Doughnut(sharePiePolorDoughnutData3, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });

    function showpie(ob,num,title){
        $(".color_view_chart span").removeClass("current");
        $(ob).addClass("current");
        $("#canvas_Doughnut3").hide();
        $("#canvas_Doughnut2").hide();
        $("#canvas_Doughnut1").hide();
        $("#canvas_Doughnut"+num).show();
        $(".current_view_chart span").html(title);
    }

    function change_viewtype(type){
        if(type==0){
            window.location="<?php echo current_url() ?>";
        }else{
            window.location="<?php echo current_url().'?view_type=1' ?>";
        }
    }
</script>
<style>
    .version{display:none;}
    .copyright{display:none;}
</style>