<?php 
$url = base_url().ADMINPATH.'/report';
$segment_current = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-shopping-cart fa-fw"></i> Doanh số</p>
    <ul>
        <li><a href="<?php echo $url.'/report_sales' ?>" <?php echo $segment_current=='report_sales' && $segment_current_bonus=='' ? "class='active'" : '' ; ?>>Tổng quan</a></li>
        <li><a>Theo khách hàng</a></li>
        <li><a href="<?php echo $url.'/report_sales/report_staff' ?>" <?php echo $segment_current=='report_sales' && $segment_current_bonus=='report_staff' ? "class='active'" : '' ; ?>>Theo nhân viên</a></li>
        <li><a>Theo khu vực</a></li>
        <li><a>Theo sản phẩm</a></li>
        <li><a href="<?php echo $url.'/report_sales/report_source' ?>" <?php echo $segment_current=='report_sales' && $segment_current_bonus=='report_source' ? "class='active'" : '' ; ?>>Theo nguồn đặt hàng</a></li>
    </ul>
    <p class="title"><i class="fa fa-line-chart fa-fw"></i> Traffic</p>
    <ul>
        <li><a>Tổng quan</a></li>
        <li><a>Theo khách hàng</a></li>
        <li><a>Theo nhân viên</a></li>
        <li><a>Theo khu vực</a></li>
        <li><a>Theo sản phẩm</a></li>
        <li><a>Theo nguồn đặt hàng</a></li>
    </ul>
    <p class="title"><i class="fa fa-pie-chart fa-fw"></i> Marketing</p>
    <ul>
        <li><a>Tổng quan</a></li>
        <li><a>Theo chiến dịch</a></li>
        <li><a>Theo doanh số</a></li>
        <li><a>Theo traffic</a></li>
    </ul>
    <p class="title"><i class="fa fa-users fa-fw"></i> Customer Services</p>
    <ul>
        <li><a>Tổng quan</a></li>
        <li><a>Theo SL Email gửi</a></li>
        <li><a>Theo SL cuộc gọi</a></li>
        <li><a>Theo SL online chat</a></li>
        <li><a>Theo thời gian xử lý</a></li>
        <li><a>Theo phân loại tình huống</a></li>
    </ul>
</div>