<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Chỉ tiêu kinh doanh</h1>
	    </div>
    </div>
    <div class="header_targets">
    	<div class="block1">
    		<p>Chọn bộ phận:</p>
    		<select id="department">
    			<option value='0'> Tất cả phòng ban </option>
    			<?php 
    			$department = $this->db->query("select * from ttp_report_targets_department")->result();
    			if(count($department)>0){
    				foreach($department as $row){
    					$selected = $current_department == $row->ID ? "selected='selected'" : '' ;
    					echo "<option value='$row->ID' $selected>$row->Title</option>";
    				}
    			}
    			?>
    		</select>
    		<p>Năm tài chính:</p>
    		<select id="namtaichinh">
    			<?php 
    			$year = $this->db->query("select Year from ttp_report_targets_year order by Year ASC")->result();
    			if(count($year)>0){
    				foreach($year as $row){
    					$selected = $row->Year==$current_year ? "selected='selected'" : '' ;
	    				echo "<option value='$row->Year' $selected>$row->Year</option>";
    				}
    			}else{
	    			$year = date('Y',time());
	    			for ($i=2014; $i < $year+1; $i++) { 
	    				$selected = $i==$current_year ? "selected='selected'" : '' ;
	    				echo "<option value='$i' $selected>$i</option>";
	    			}
    			}
    			?>
    		</select>
    		<a class="btn btn-default" onclick="addyear(this)"><i class="fa fa-plus"></i></a>
    	</div>
    	<?php 
    	$current_tab = $this->uri->segment(4);
    	?>
    	<div class="block2">
    		<a href="<?php echo $base_link; ?>" <?php echo $current_tab=='' ? 'class="current"' : '' ; ?>>Theo năm</a>
    		<a href="<?php echo $base_link."bymonth"; ?>" <?php echo $current_tab=='bymonth' ? 'class="current"' : '' ; ?>>Theo tháng</a>
    		<a href="<?php echo $base_link."byteam"; ?>" <?php echo $current_tab=='byteam' ? 'class="current"' : '' ; ?>>Theo nhóm trực thuộc</a>
    		<a href="<?php echo $base_link."byperson"; ?>" <?php echo $current_tab=='byperson' ? 'class="current"' : '' ; ?>>Theo cá nhân từng nhóm</a>
    	</div>
    </div>
    <div class="content_targets">
    	<?php 
    	if($current_department!=0){
    	?>
    	<div class="team_content">
    		<form action="<?php echo $base_link.'save_chitieu_person' ?>" method="post">
    		<div class="block1">
    			<p>Chỉ tiêu đã phân bổ cho từng nhóm</p>
    			<select id="select_team">
    				<option value='0'>-- Chọn Nhóm --</option>
    				<?php 
    				$team = $this->db->query("select * from ttp_report_team where Department=$current_department and Published=1")->result();
    				$userinteam = array(0);
    				if(count($team)>0){
    					foreach($team as $row){
    						$selected = $row->ID==$current_team ? "selected='selected'" : '' ;
    						if($row->ID==$current_team){
    							$userinteam = json_decode($row->Data,true);
    						}
    						echo "<option value='$row->ID' $selected>$row->Title</option>";
    					}
    				}
    				?>
    			</select>
    			<select id="select_targets">
    				<option value='0'>-- Chọn chỉ tiêu --</option>
    				<?php 
    				$targets = $this->db->query("select * from ttp_report_targets_type")->result();
    				if(count($targets)>0){
    					foreach($targets as $row){
    						$selected = $row->ID==$current_targets ? "selected='selected'" : '' ;
    						echo "<option value='$row->ID' $selected>$row->Title</option>";
    					}
    				}
    				?>
    			</select>
    		</div>
    		<div class="block2">
    			<a class="btn btn-primary" id="add_chitieu"><i class="fa fa-plus"></i> Cá nhân</a>
    			<button class="btn btn-primary" id="save_chiteu" type="submit">Lưu thay đổi</button>
    		</div>
    		<div class="block3">
    			<table>
    				<?php
					if($current_team!=0){
						echo "<tr><th>Nhóm</th><th>Chỉ tiêu</th>"; 
						for ($i=1; $i < 13; $i++) { 
							echo "<th>$i/$current_year</th>";
						}
						echo "</tr>";
					}else{
						echo "<tr><td style='text-align:center'>Vui lòng chọn nhóm muốn truy xuất dữ liệu .</td></tr>";
					}
					$userinteam = implode(',', $userinteam);
					$person = $this->db->query("select * from ttp_user where UserType=1 and ID in ($userinteam)")->result();
					if(count($person)>0){
						$result = $this->db->query("select * from ttp_report_targets_type")->result();
						if(count($result)>0){
							$arr_team = array();
							foreach($person as $row){
								echo "<tr><td colspan='14' class='colspan'>$row->FirstName $row->LastName</td></tr>";
								foreach($result as $item){
									$type_display = $current_targets==$item->ID || $current_targets==0 ? "" : "display:none" ;
									echo "<tr style='$type_display'>";
									echo "<td colspan='2'>$item->Title</td>";
									for ($i=1; $i < 13; $i++) { 
										$total_item = isset($datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Team'][$current_team]['Person'][$row->ID]['Total']) ? $datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Team'][$current_team]['Person'][$row->ID]['Total'] : 0 ;
										echo "<td>
												<input type='number' min='0' name='year[type$item->ID][Department][$current_department][Month][$i][Team][$current_team][Person][$row->ID][Total]' class='department_{$item->ID}_{$i}' data-team='$item->ID' data-month='$i' onchange='recal(this)' value='$total_item' />
												<a class='show_numberofrow' onclick='nhapdulieu(this)'>".number_format($total_item)."</a>
											</td>";
									}
									echo "</tr>";
								}
								$arr_team[] = $row->ID;
							}
							$chitieuchuaphanbo = "";
							echo "<tr><td class='colspan green' colspan='14'>Tổng chỉ tiêu đã phân bổ</td></tr>";
							foreach($result as $item){
								$type_display = $current_targets==$item->ID || $current_targets==0 ? "" : "display:none" ;
								echo "<tr style='$type_display'>";
								echo "<td colspan='2'>$item->Title</td>";
								$chitieuchuaphanbo .= "<tr style='$type_display'><td colspan='2'>$item->Title</td>";
								for ($i=1; $i < 13; $i++) { 
									$total_item = isset($datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Team'][$current_team]['Total']) ? $datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Team'][$current_team]['Total'] : 0 ;
									$remain_item = isset($datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Team'][$current_team]['Remain']) ? $datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Team'][$current_team]['Remain'] : $total_item ;
		    						if(isset($datadepartment['type'.$item->ID]['Department'][$current_department]['Month'][$i]['Remain'])){
		    							$apply = $total_item - $remain_item;	
		    						}else{
		    							$apply = 0;
		    						}
									echo "<td>
											<input type='number' min='0' class='total_{$item->ID}_{$i}' onchange='recal(this)' value='$total_item' />
											<a class='show_numberofrow total_phanbo_{$item->ID}_{$i}'>".number_format($apply)."</a>
										</td>";
									$chitieuchuaphanbo .= "<td>
											<input type='hidden' min='0' name='year[type$item->ID][Department][$current_department][Month][$i][Team][$current_team][Remain]' class='remain_{$item->ID}_$i' value='$remain_item' />
											<a class='show_numberofrow  total_remain_{$item->ID}_{$i}'>".number_format($remain_item)."</a>
										</td>";
								}
								$chitieuchuaphanbo .= "</tr>";
								echo "</tr>";
							}
							echo "<tr><td class='colspan yellow' colspan='14'>Tổng chỉ tiêu chưa phân bổ</td></tr>";
							echo $chitieuchuaphanbo;
						}
					}
					?>
    			</table>
    		</div>
    	</div>
    	<?php 
    	}else{
    		echo "<p style='padding:20px 0px;text-align:left'>Vui lòng chọn phòng ban</p>";
    	}
    	?>
    </div>
    <div class="over_lay black">
    	<div class="box_inner" style="width:500px">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner">
    			<div class="row">Thuộc nhóm</div>
    			<div class="row">
    				<select id="TeamID" class="form-control">
					<?php 
					$depart = $this->db->query("select ID,Title from ttp_report_team")->result();
					if(count($depart)>0){
						foreach($depart as $row){
							$selected = $row->ID==$current_team ? "selected='selected'" : '' ;
							echo "<option value='$row->ID' $selected>$row->Title</option>";
						}
					}
					?>
					</select>
    			</div>
    			<div class="row">First Name</div>
    			<div class="row"><input type="text" id="firstname" /></div>
    			<div class="row">Last Name</div>
    			<div class="row"><input type="text" id="lastname" /></div>
    			<div class="row">UserName</div>
    			<div class="row"><input type="text" id="username" /></div>
    			<div class="row">Password</div>
    			<div class="row"><input type="password" id="password" /></div>
    			<div class="row">Email</div>
    			<div class="row"><input type="text" id="email" /></div>
    			<div class="row"><a class="btn btn-default" onclick="addchitieu(this)">Save</a></div>

    		</div>
    	</div>
    </div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
	var link = $("#baselink_report").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
	});

	$("#add_chitieu").click(function(){
		$(".over_lay .box_inner .block1_inner h1").html("Thêm nhân viên mới");
		$(".over_lay").show();
	});

	$("#department").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?department="+data;
	});

	$("#namtaichinh").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?year="+data;
	});

	$("#select_team").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?team="+data;
	});

	$("#select_targets").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?targets="+data;
	});

	function addchitieu(ob){
		$(ob).addClass('saving');
		var firstname = $("#firstname").val();
		var lastname = $("#lastname").val();
		var username = $("#username").val();
		var password = $("#password").val();
		var email = $("#email").val();
		var teamid = $("#TeamID").val();
		if(username!='' && password!='' && firstname!='' && lastname!=''){
			$.ajax({
            	url: link+"import_targets/add_person",
	            dataType: "html",
	            type: "POST",
	            data: "firstname="+firstname+"&lastname="+lastname+"&username="+username+"&password="+password+"&email="+email+"&TeamID="+teamid,
	            success: function(result){
	                if(result=='true'){
	                	location.reload();
	                }else{
	                	if(result=='false1'){
	                		alert("Username đã có người sử dụng ! Vui lòng chọn UserName khác.");
	                	}else{
	                		alert("Yêu cầu không được thực hiện . Vui lòng kiểm tra lại kết nối mạng và dữ liệu trước khi truyền lên server .");
	                	}
	                }
	            }
	        });
		}
		$(ob).removeClass('saving');
	}

	function addyear(ob){
		$(ob).addClass('saving');
		$.ajax({
        	url: link+"import_targets/add_year",
            dataType: "html",
            type: "POST",
            data: "Year=ok",
            success: function(result){
                alert("Đã thêm năm tài chính tiếp theo .");
                location.reload();
            }
        });
        $(ob).removeClass('saving');
	}

	function nhapdulieu(ob){
		$(ob).parent().find("input").css({"display":"block"});
		$(ob).parent().find("input").focus();
		$(ob).hide();
	}

	function recal(ob){
		money = parseInt($(ob).val());
		var datamonth = $(ob).attr("data-month");
		var datateam = $(ob).attr("data-team");
		total = parseInt($(".total_"+datateam+"_"+datamonth).val());
		remain=0;
		$(".department_"+datateam+"_"+datamonth).each(function(){
			remain = remain + parseInt($(this).val());
		});
		$(".total_phanbo_"+datateam+"_"+datamonth).html(remain.format('a',3));
		remain = total-remain;
		$(ob).parent().find("a.show_numberofrow").html(money.format('a',3));
		$(ob).parent().find("a.show_numberofrow").show();
		$(ob).hide();
		$(".total_remain_"+datateam+"_"+datamonth).parent().removeClass("red");
		$(".total_remain_"+datateam+"_"+datamonth).parent().removeClass("green");
		if(remain==0){
			$(".total_remain_"+datateam+"_"+datamonth).html(0);
		}
		if(remain<0){
			temp = remain*-1;
			alert("!!! Cảnh báo : vượt chỉ tiêu cho phép của tháng "+temp.format('a',3)+".");
			$(".total_remain_"+datateam+"_"+datamonth).parent().addClass("red");
			$(".total_remain_"+datateam+"_"+datamonth).html(remain.format('a',3));
		}
		if(remain>0){
			$(".total_remain_"+datateam+"_"+datamonth).html(remain.format('a',3));
		}
		$(".remain_"+datateam+"_"+datamonth).val(remain);
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

</script>