<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<h1>Danh sách nhà cung cấp</h1>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add new</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th>Mã nhà cung cấp</th>
					<th>Tên nhà cung cấp</th>
					<th>Điện thoại</th>
					<th>Trạng thái hoạt động</th>
					<th>Action</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						$status = $row->Published==1 ? "Enable" : "Disable" ;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->ProductionCode</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->Phone</td>";
						echo "<td>$status</td>";
						echo "<td>
								<a href='{$base_link}edit/$row->ID'><i class='fa fa-pencil-square-o'></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                <a href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
						echo "</tr>";
					}
				}else{
					echo "<td colspan='6'>Không tìm thấy dữ liệu</td>";
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>