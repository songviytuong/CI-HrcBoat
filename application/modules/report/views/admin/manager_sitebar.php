<?php 
$url = base_url().ADMINPATH.'/report';
$segment_current = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-th fa-fw"></i> Dashboards</p>
    <ul>
        <li><a href="<?php echo $url.'/manager_products' ?>" <?php echo $segment_current=='manager_products' ? "class='active'" : '' ; ?>>Sản phẩm</a></li>
        <li><a href="<?php echo $url.'/manager_categories' ?>" <?php echo $segment_current=='manager_categories' ? "class='active'" : '' ; ?>>Nhóm ngành hàng</a></li>
        <li><a href="<?php echo $url.'/manager_warehouse' ?>" <?php echo $segment_current=='manager_warehouse' ? "class='active'" : '' ; ?>>Danh sách kho</a></li>
        <li><a href="<?php echo $url.'/manager_customer' ?>" <?php echo $segment_current=='manager_customer' ? "class='active'" : '' ; ?>>Danh sách khách hàng</a></li>
        <li><a href="<?php echo $url.'/manager_district' ?>" <?php echo $segment_current=='manager_district' ? "class='active'" : '' ; ?>>Quận huyện</a></li>
        <li><a href="<?php echo $url.'/manager_city' ?>" <?php echo $segment_current=='manager_city' && $segment_current_bonus=='' ? "class='active'" : '' ; ?>>Tỉnh thành</a></li>
        <li><a href="<?php echo $url.'/manager_area' ?>" <?php echo $segment_current=='manager_area' ? "class='active'" : '' ; ?>>Khu vực</a></li>
        <li><a href="<?php echo $url.'/manager_department' ?>" <?php echo $segment_current=='manager_department' ? "class='active'" : '' ; ?>>Phòng ban</a></li>
        <li><a href="<?php echo $url.'/manager_team' ?>" <?php echo $segment_current=='manager_team' ? "class='active'" : '' ; ?>>Danh sách team</a></li>
        <li><a href="<?php echo $url.'/manager_city/city_mapper' ?>" <?php echo $segment_current=='manager_city' && $segment_current_bonus=='city_mapper' ? "class='active'" : '' ; ?>>Map data tỉnh thành</a></li>
        <li><a href="<?php echo $url.'/manager_city/district_mapper' ?>" <?php echo $segment_current=='manager_city' && $segment_current_bonus=='district_mapper' ? "class='active'" : '' ; ?>>Map data quận huyện</a></li>
        <li><a href="<?php echo $url.'/manager_report_everyday' ?>" <?php echo $segment_current=='manager_report_everyday' ? "class='active'" : '' ; ?>>Đồng bộ báo cáo ngày</a></li>
    </ul>
</div>