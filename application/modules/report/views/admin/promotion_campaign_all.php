<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$month          = array();
$dayofmonth     = array();
$dayofmonth_real= array();
$result_fillter = array();
$fill_temp_value="";
?>
<div class="containner">
	<!--<div class="disableedit" style="height:1000px"></div>-->
	
	<div class="import_select_progress">
	    <div class="block1">
                <div class="btn_group">
                <button class="btn btn-default create"><i class="fa fa-calendar-o" aria-hidden="true"></i> Tạo mới</button>
                <button class="btn btn-info disabled"><i class="fa fa-calendar" aria-hidden="true"></i> Ngày</button>
                <button class="btn btn-info disabled"><i class="fa fa-calendar" aria-hidden="true"></i> Tháng</button>
                <button class="btn btn-info disabled"><i class="fa fa-calendar" aria-hidden="true"></i> Năm</button>
                <button class="btn btn-warning disabled"><i class="fa fa-copy" aria-hidden="true"></i> Copy</button>
                </div>
	    </div>
            <div class="block2">
                <div class="block2">
                    <div id="reportrange" class="list_div">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span></span> <b class="caret"></b>
                    </div>
                </div>
            </div>
        </div>
        <div class="table_data">
            <table id="table_data">
                <tr>
                    <th class="text-center"></th>
                    <th class="text-center col-xs-2"></th>
                    <th class="col-xs-3">Tên chương trình</th>
                    <th class="text-center">Thời gian bắt đầu</th>
                    <th class="text-center">Thời gian kết thúc</th>
                    <th class="text-center">Tình trạng</th>
                    <th class="text-center"></th>
                    <th class="text-center"></th>
                </tr>
                <?php
                    foreach($data as $row){
                        $promo_id = $row->promo_id;
                        $voucher = $this->db->query("SELECT count(*) as count FROM ttp_promotion_code WHERE promo_id = $promo_id")->result();
                        $TotalCount = $voucher[0]->count;
                        $voucher = $this->db->query("SELECT count(*) as used FROM ttp_promotion_code WHERE active = 1 and promo_id = $promo_id")->result();
                        $UsedCount = $voucher[0]->used;
                        
                        $arr_status = $this->conf_define->get_order_status('status','promotion');
                        $array_status = array();
                        foreach($arr_status as $key=>$ite){
                            $code = (int)$ite->code;
                            $array_status[$code] = $ite->name;
                        }
                        
                        $arr_type = $this->conf_define->get_order_status('type','promotion');
                        $array_type = array();
                        foreach($arr_type as $key=>$ite){
                            $code = (int)$ite->code;
                            $array_type[$code] = $ite->name;
                            
                        }
                        
                        if($row->promo_type == 2){
                            $class = "btn-danger";
                        }elseif($row->promo_type == 1){
                            $class = "btn-primary";
                        }else{
                            $class = "btn-success";
                        }
                ?>
                <tr>
                    <td class="text-center"><?=($TotalCount)? "<i class='fa fa-search view' style='cursor: pointer' title='Xem' rel='".$promo_id."'></i>":"<i class='fa fa-plus code' style='cursor: pointer' title='Tạo mã' rel='".$promo_id."'></i>";?></td>
                    <td class="">
                        <?php if($TotalCount) { ?>
                            Có: <?=str_pad($TotalCount,2,"0",STR_PAD_LEFT);?> voucher<br/><small style="color:#255625;"><i class="fa fa-retweet"></i> Đã sử dụng <font color="red"><?=$UsedCount;?></font>/<?=$TotalCount;?> voucher</small>
                        <?php } ?>
                    </td>
                    <td><a href="<?=$base_link;?>edit/<?=$row->promo_id?>"><?=$row->promo_name?></a><br/><small style="color:#255625;"><i class="fa fa-comments-o"></i> <?=$row->promo_description?></small></td>
                    <td class="text-center"><?=$row->promo_start?></td>
                    <td class="text-center"><?=$row->promo_end?></td>
                    <td class="text-center"><?=$array_status[$row->promo_status] ? $array_status[$row->promo_status] : '--' ?></td>
                    <td class="text-center <?=$class?>"><?=$array_type[$row->promo_type] ? $array_type[$row->promo_type] : '--'?></td>
                    <td class="text-center"><i class="fa fa-trash-o btn btnDel" rel="<?=$row->promo_id?>"></i></td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <div class="over_lay black">
            <div class="box_inner">
                <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
                <div class="block2_inner"></div>
            </div>
        </div>
        <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
  </div>
<script type="text/javascript">
$(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"promotion_campaign/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    console.log(result);
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
        
        $('.create').click(function(){
            window.location = "<?=$base_link;?>create";
        });
        
        $('.btnDel').click(function(){
            var promo_id = $(this).attr("rel");
            if (!confirm('BẠN CÓ CHẮC XÓA DỮ LIỆU NÀY KHÔNG ??')) {
                return false;
            }else{
                //Do nothing
                $.ajax({
                    url: "<?=$base_link;?>delete_compaign",
                    dataType: "html",
                    type: "GET",
                    data: "promo_id="+promo_id,
                    success: function(result){
                        if(result=="OK"){
                            location.reload();
                        }else{
//                           alert(result);
                        }
                    }
                });
            }
        });
        
        $("#close_overlay").click(function(){
            $(".over_lay").removeClass('in');
            $(".over_lay").addClass('out');
            setTimeout(function(){
                $(".over_lay").hide();
                disablescrollsetup();
            },200);
	});
        
        $('.view').click(function(){
            var promo_id = $(this).attr("rel");
            $.ajax({
                url: "<?=$base_link;?>viewVoucher",
                dataType: "html",
                type: "GET",
                data: "promo_id="+promo_id,
                success: function(result){
                    enablescrollsetup();
                    $(".over_lay .box_inner").css({'width':'600px','margin-top':'20px'});
                    $(".over_lay .box_inner .block1_inner h1").html("Danh sách Voucher");
                    $(".over_lay .box_inner .block2_inner").html(result);        	
                    $(".over_lay").removeClass('in');
                    $(".over_lay").fadeIn('fast');
                    $(".over_lay").addClass('in');
                }
            });
            
        });
        
        $('.code').click(function(){
            var promo_id = $(this).attr("rel");
            $.ajax({
                url: "<?=$base_link;?>createVoucher",
                dataType: "html",
                type: "GET",
                data: "promo_id="+promo_id,
                success: function(result){
                    enablescrollsetup();
                    $(".over_lay .box_inner").css({'width':'600px','margin-top':'20px'});
                    $(".over_lay .box_inner .block1_inner h1").html("Tạo Voucher");
                    $(".over_lay .box_inner .block2_inner").html(result);        	
                    $(".over_lay").removeClass('in');
                    $(".over_lay").fadeIn('fast');
                    $(".over_lay").addClass('in');
                }
            });
            
        });
        
        function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}
    });
</script>
 