<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<form action="<?php echo $base_link ?>setsessionsearch" method="post">
					<span>Lọc dữ liệu</span>
					<select name="PropertiesID" class="form-control">
						<option value="">-- Nhóm biến thể --</option>
						<?php 
							$CategoriesID = $this->session->userdata("report_filter_parent_PropertiesID");
							$arr = array();
							$categories = $this->db->query("select * from ttp_report_Properties where ParentID=0")->result();
							if(count($categories)>0){
								foreach($categories as $row){
									$arr[$row->ID] = $row->Title;
									$selected=$row->ID==$CategoriesID ? "selected='selected'" : '' ;
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
								}
							}
						?>
					</select>
					<input type="submit" class="btn btn-default" value="Filter" />
                    <a class="btn btn-default" href="<?php echo $base_link.'clearfilter' ?>">Clear Filter</a>
				</form>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'add'; ?>" class="btn btn-primary">Add new</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th>Thuộc tính</th>
					<th>Thuộc loại thuộc tính</th>
					<th>Action</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo isset($arr[$row->ParentID]) ? "<td>".$arr[$row->ParentID]."</td>" : "<td>--</td>";
						echo "<td>
								<a href='{$base_link}edit/$row->ID'><i class='fa fa-pencil-square-o'></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                <a href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
						echo "</tr>";
					}
				}else{
					echo "<tr><td colspan='4'>Không tìm thấy dữ liệu .</td></tr>";
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>