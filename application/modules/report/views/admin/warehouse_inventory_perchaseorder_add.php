<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST" enctype="multipart/form-data">
			<div class="progress_second">
				<ul>
					<li><span class='active'><i class="fa fa-check-circle"></i></span><a>PO nháp</a></li>
					<li><span><i class="fa fa-check-circle"></i></span><a>PO chờ duyệt</a></li>
					<li><span><i class="fa fa-check-circle"></i></span><a>PO đã được duyệt</a></li>
					<li><span><i class="fa fa-check-circle"></i></span><a>PO đang chờ nhập</a></li>
					<li><span><i class="fa fa-check-circle"></i></span><a>PO đã đóng</a></li>
				</ul>
				<input type='hidden' name="Status" id="Status" value='0' />
				<div class="btn_group text-right" style="margin:7px">
					<a class="btn btn-danger btn-accept" onclick="skip(this)"><i class="fa fa-check-square"></i> Duyệt</a>
					<button class="btn btn-success btn-update" type="submit"><i class="fa fa-check-square"></i> Lưu PO</button>
				</div>
			</div>
			<div class="fillter_bar">
				<div class="block1">
					<h1>PHIẾU ĐỀ NGHỊ MUA HÀNG (PURCHASE ORDER)</h1>
				</div>
				<div class="block2"></div>
			</div>
			<div class="row" style='margin:5px 0px'></div>
			<div class="box_content_warehouse_import">
				<div class="row">
					<div class="form-group">
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Số chứng từ</label>
							<div class="col-xs-7">
								<input type='text' name="HardCode" class="form-control" placeholder="Điền số phiếu PO (nếu có)" />
							</div>
						</div>
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Nhà cung cấp</label>
							<div class="col-xs-7">
								<div class="input-group">
									<select name="ProductionID" id="ProductionID" class="form-control">
										<?php 
										$production = $this->db->query("select ID,Title from ttp_report_production where Published=1")->result();
										if(count($production)>0){
											foreach($production as $row){
												echo "<option value='$row->ID'>$row->Title</option>";
											}
										}
										?>
									</select>
									<span class="input-group-btn">
										<a class='btn btn-default' title="Thêm nhà cung cấp mới" onclick="add_production(this)"><i class="fa fa-plus"></i></a>
									</span>
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Nhập tại kho</label>
							<div class="col-xs-6">
								<select name="WarehouseID" id="WarehouseID" class="form-control">
									<?php 
									$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
									if(count($warehouse)>0){
										foreach($warehouse as $row){
											echo "<option value='$row->ID'>$row->MaKho</option>";
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Ngày dự kiến nhập</label>
							<div class="col-xs-7">
								<input type='text' name='DateExpected' class="form-control" id='NgayDukien' value='<?php echo date('Y-m-d') ?>' readonly="true" />
							</div>
						</div>
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Diễn giải</label>
							<div class="col-xs-7">
								<input type='text' name="Note" class='required form-control' required />
							</div>
						</div>
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Đính kèm file</label>
							<div class="col-xs-7">
								<div class="input-group-btn">
									<a class="btn btn-default btn-file choosefile">
										<i class="fa fa-folder-open" aria-hidden="true"></i> Browse...
										<input type="file" name="Images_upload[]" id="choosefile" multiple="">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- end block1 -->
		    	<div class="block2">
		    		<div class="row">
					<div class="col-xs-12">
		    			<div class="form-group">
			    			<div class="col-xs-1">
			    				<a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Sản phẩm</a>
			    			</div>
			    			<div class="col-xs-2">
			    			<ul>
								<li><a id='show_thaotac' class="btn btn-default">Thao tác <b class="caret"></b></a>
				    				<ul>
					    				<li><a id="edit_row_table"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a></li>
					    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
				    				</ul>
								</li>
							</ul>
							</div>
						</div>
					</div>
					</div>
		    	</div>
		    	<div class="clear"></div>
		    	<div class="table_donhang table_data">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã SP</th>
		    				<th>Tên sản phẩm</th>
		    				<th>ĐVT</th>
		    				<th>Lô hàng</th>
		    				<th>Số lượng</th>
		    				<th>Đơn vị <br>tiền tệ</th>
		    				<th>Tỷ giá <br>nguyên tệ</th>
		    				<th>Đơn giá <br>nguyên tệ</th>
		    				<th>Thành tiền <br>nguyên tệ</th>
		    				<th>Thành tiền VNĐ</th>
		    				<th style="width:30px">VAT</th>
		    			</tr>
		    			<tr>
		    				<td colspan="10">TỔNG CỘNG</td>
		    				<td><span class='tongcong'>0</span></td>
		    				<td></td>
		    			</tr>
		    		</table>
		    	</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	function stopRKey(evt) { 
		var evt = (evt) ? evt : ((event) ? event : null); 
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
		if ((evt.keyCode == 13) && (node.type=="text") && node.id!="input_search_products")  {return false;} 
	} 

	document.onkeypress = stopRKey;

	$(document).ready(function () {
        $('#NgayNK,.DateProduction,#NgayDukien').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD'
        });
    });

	var link = $("#baselink").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function add_production(ob){
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"500px"});
		$(".over_lay .box_inner .block1_inner h1").html("Thêm nhà cung cấp mới");
		$(".over_lay .box_inner .block2_inner").html("<div class='row'><p style='margin-bottom:5px'>Tên nhà cung cấp</p><div><input type='text' class='title_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Mã nhà cung cấp</p><div><input type='text' class='code_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Địa chỉ trụ sở</p><div><input type='text' class='address_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Số điện thoại</p><div><input type='text' class='phone_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Fax</p><div><input type='text' class='fax_production' style='margin-bottom:5px' /></div></div><div class='row'><button class='btn btn-primary' style='float:left;background:#1A82C3;color:#FFF;border: 1px solid #2477CA;' onclick='save_production(this)'>Lưu dữ liệu</button></div>");
		$(".over_lay").removeClass('in');
		$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function save_production(ob){
		$(ob).addClass("saving");
		var title = $(".title_production").val();
		var code = $(".code_production").val();
		var address = $(".address_production").val();
		var phone = $(".phone_production").val();
		var fax = $(".fax_production").val();
		if(title!='' && code!=''){
			$.ajax({
            	url: link+"add_production",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "Title="+title+"&code="+code+"&address="+address+"&phone="+phone+"&fax="+fax,
	            success: function(result){
	            	$("#ProductionID").prepend(result);
	            	$(".over_lay").hide();
					disablescrollsetup();
					$(ob).removeClass("saving");
	            }
	        });
		}else{
			alert("Vui lòng điền đầy đủ thông tin !");
		}
	}

	function save_shipment(ob,id){
		$(ob).addClass("saving");
		var ShipmentCode = $(".ShipmentCode").val();
		var DateProduction = $(".DateProduction").val();
		var DateExpiration = $(".DateExpiration").val();
		if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
			alert("Vui lòng điền đầy đủ thông tin !");
			$(ob).removeClass("saving");
			return false;
		}
		$.ajax({
        	url: link+"save_shipment",
            dataType: "html",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
            	load_shipment(id);
            	$(".over_lay").hide();
				disablescrollsetup();
				$(ob).removeClass("saving");
            }
        });
	}

	function add_shipment(ob,id){
		var data = $(ob).val();
		if(data=="add"){
			enablescrollsetup();
			$(".over_lay .box_inner").css({"width":"500px"});
			$(".over_lay .box_inner .block1_inner h1").html("Tạo lô sản phẩm");
			$(".over_lay .box_inner .block2_inner").html("");
			$(".over_lay .box_inner .block2_inner").load(link+"box_add_shipment/"+id);
			$(".over_lay").removeClass('in');
	    	$(".over_lay").fadeIn('fast');
			$(".over_lay").addClass('in');
		}
	}

	$("#add_products_to_order").click(function(){
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"850px"});
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "",
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);
			    	$(".over_lay").removeClass('in');
			    	$(".over_lay").fadeIn('fast');
					$(".over_lay").addClass('in');
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
                $(this).removeClass('saving');
            }
        });
	});

	function input_search_products(ob){
		var data = $(ob).val();
		$.ajax({
        	url: link+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Title="+data,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
	}

	var celldata = [];
	var sttrow = 1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				var data_donvi = $(this).attr('data-donvi');
				var data_vat = $(this).attr('data-vat');
				var table = document.getElementById("table_data");
				var row = table.insertRow(sttrow);
				row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
				row.insertCell(1).innerHTML=data_code;
				row.insertCell(2).innerHTML=data_name;
				row.insertCell(3).innerHTML=data_donvi;
				row.insertCell(4).innerHTML="<select class='ShipmentDefault' name='ShipmentID[]' data-id='"+data_id+"' onchange='add_shipment(this,"+data_id+")'></select>";
				row.insertCell(5).innerHTML="<input type='text' name='Amount[]' class='Amount_input' value='1' onchange='changerow(this)' required />";
				row.insertCell(6).innerHTML="<select name='Currency[]' class='Currency_input' onchange='checkcurrency(this)'><option value='VND'>VNĐ</option><option value='USD'>USD</option><option value='EURO'>EURO</option><option value='GBP'>GBP</option><option value='CNY'>CNY</option><option value='JPY'>JPY</option></select>";
				row.insertCell(7).innerHTML="<input type='text' name='ValueCurrency[]' value='1' class='ValueCurrency_input' onchange='changerow(this)' required />";
				row.insertCell(8).innerHTML="<input type='text' name='PriceCurrency[]' value='"+data_price+"' class='PriceCurrency_input' onchange='changerow(this)' required />";
				row.insertCell(9).innerHTML="<input type='text' name='TotalCurrency[]' value='1' class='TotalCurrency_input' onchange='changerow(this)' required />";
				row.insertCell(10).innerHTML="<input type='text' name='TotalVND[]' style='display:none' class='Total_input' /><span class='TotalVND'>0</span>";
				row.insertCell(11).innerHTML="<span>"+data_vat+"</span>";
				sttrow=sttrow+1;
			}
		});
		loadshipmentdefault();
		recal();
		$(".over_lay").hide();
		disablescrollsetup();
	}

	function loadshipmentdefault(){
		var warehouse = $('#WarehouseID').val();
		$(".ShipmentDefault").each(function(){
			var data = $(this).attr('data-id');
			$(this).load(link+"get_shipment_by_productsID/"+data+"/"+warehouse);
		});
	}

	function load_shipment(id){
		$(".ShipmentDefault").each(function(){
			if(id==0){
				var data = $(this).attr('data-id');
				$(this).load(link+"load_shipment_by_products/"+data);
			}else{
				var data = $(this).attr('data-id');
				if(data==id){
					$(this).load(link+"load_shipment_by_products/"+data);
				}
			}
		});
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				var index = celldata.indexOf("data"+data_id);
				celldata.splice(index, 1);
				sttrow = sttrow-1;
			}
		});
		recal();
	});

	function recal(){
		var tongcong = 0;
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			amount = parseInt(parent.find('input.Amount_input').val());
			valueCurrency = parent.find('input.ValueCurrency_input').val();
			pricecurrentcy = parent.find('input.PriceCurrency_input').val();
			total = (amount*pricecurrentcy)*valueCurrency;
			tongcong = tongcong+total;
			parent.find('input.Total_input').val(total);
			parent.find('.TotalVND').html(total.format('a',3));
		});
		$(".tongcong").html(tongcong.format('a',3));
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		var cls = $(ob).attr('class');
		Amount_input = parseInt(parent.find('td input.Amount_input').val());
		PriceCurrency_input = parent.find('input.PriceCurrency_input').val();
		ValueCurrency = parent.find('input.ValueCurrency_input').val();
		value = $(ob).val();
		if(cls=="PriceCurrency_input"){
			TotalCurrency = value*Amount_input;
			parent.find('input.TotalCurrency_input').val(TotalCurrency);
		}
		if(cls=="TotalCurrency_input"){
			PriceCurrency = value/Amount_input;
			parent.find('input.PriceCurrency_input').val(PriceCurrency);
		}
		if(cls=="Amount_input"){
			TotalCurrency = value*PriceCurrency_input;
			parent.find('input.TotalCurrency_input').val(TotalCurrency);
		}
		recal();
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	}

	function change_type(ob){
		var data = parseInt($(ob).val());
		var destination = '';
		if(data==0){
			destination="import_from_production";
		}
		if(data==1){
			destination="import_from_rejectorder";
		}
		if(data==2){
			destination="import_from_internal";
		}
		window.location=link+destination;
	}

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	function checkcurrency(ob){
		var data = $(ob).val();
		if(data=="VND"){
			$(ob).parent('td').parent('tr').find(".ValueCurrency_input").val(1);
			changerow();
		}
	}

	function skip(ob){
		$("#Status").val(2);
		var status_required=0;
		$(".required").each(function(){
			var check = $(this).val();
			if(check==''){
				status_required=1;
				$(this).css({'border':'1px solid rgb(232, 111, 111)'});
			}
		});
		if(status_required==0){
			$(ob).find("i").hide();
			$(ob).addClass('saving');
			$("form").submit();
		}
	}

</script>