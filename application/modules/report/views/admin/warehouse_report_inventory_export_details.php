<div class="box_inner"  style="width: 1092px">
	<div class="block1_inner"><h1>BÁO CÁO XUẤT KHO <?php echo $Products->Title ?></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
	<div class="block2_inner">
		<div class="table_data">
			<table>
				<tr><th colspan="8"><a class="btn btn-success" onclick="showtr(this,3)">NGHIỆP VỤ XUẤT KHO BÁN HÀNG</a> <a class="btn btn-default" onclick="showtr(this,2)">NGHIỆP VỤ XUẤT KHO CHO / TẶNG / HỦY</a> <a class="btn btn-default" onclick="showtr(this,1)">NGHIỆP VỤ XUẤT KHO LƯU CHUYỂN KHO</a> </th></tr>
				<tr>
					<th style="width:115px">Ngày chứng từ</th>
					<th style="width:130px">Mã chứng từ</th>
					<th style="width:80px">SKU</th>
					<th style="width:350px">Tên sản phẩm</th>
					<th style="width:50px">ĐVT</th>
					<th style="width:90px">Số lô</th>
					<th style="width:110px">Ngày hết hạn</th>
					<th style="width:110px;">Số lượng xuất</th>
				</tr>
				<?php 
				$shipmentID = $this->db->query("select DateExpiration,ID,ShipmentCode from ttp_report_shipment")->result();
				$arr_shipment = array();
				if(count($shipmentID)>0){
					foreach($shipmentID as $row){
						$arr_shipment[$row->ID] = array('ShipmentCode'=>$row->ShipmentCode,'DateExpiration'=>$row->DateExpiration);
					}
				}
				if(count($data3)>0){
					$total = 0;
					foreach($data3 as $row){
						echo "<tr class='tr3'>";
						echo "<td>".date('d/m/Y',strtotime($row->Ngayxuatkho))."</td>";
						echo "<td>$row->MaXK</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Tensanpham</td>";
						echo "<td>$row->Donvi</td>";
						echo isset($arr_shipment[$row->ShipmentID]['ShipmentCode']) ? "<td>".$arr_shipment[$row->ShipmentID]['ShipmentCode']."</td>" : "<td>--</td>";
						echo isset($arr_shipment[$row->ShipmentID]['DateExpiration']) ? "<td>".date('d/m/Y',strtotime($arr_shipment[$row->ShipmentID]['DateExpiration']))."</td>" : "<td>--</td>";
						echo "<td>".number_format($row->Amount,2)."</td>";
						echo "</tr>";
						$total = $total+$row->Amount;
					}
					echo "<tr class='tr3'><td></td><td></td><td></td><td></td><td></td><td></td><td><b>TỔNG CỘNG</b></td><td><b>".number_format($total,2)."</b></td></tr>";
				}else{
					echo "<tr class='tr3'><td colspan='8'>Không có dữ liệu cho loại xuất kho này</td></tr>";
				}

				if(count($data2)>0){
					$total = 0;
					foreach($data2 as $row){
						echo "<tr class='tr2'>";
						echo "<td>".date('d/m/Y',strtotime($row->Ngayxuatkho))."</td>";
						echo "<td>$row->MaXK</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Tensanpham</td>";
						echo "<td>$row->Donvi</td>";
						echo isset($arr_shipment[$row->ShipmentID]['ShipmentCode']) ? "<td>".$arr_shipment[$row->ShipmentID]['ShipmentCode']."</td>" : "<td>--</td>";
						echo isset($arr_shipment[$row->ShipmentID]['DateExpiration']) ? "<td>".date('d/m/Y',strtotime($arr_shipment[$row->ShipmentID]['DateExpiration']))."</td>" : "<td>--</td>";
						echo "<td>".number_format($row->Amount,2)."</td>";
						echo "</tr>";
						$total = $total+$row->Amount;
					}
					echo "<tr class='tr2'><td></td><td></td><td></td><td></td><td></td><td></td><td><b>TỔNG CỘNG</b></td><td><b>".number_format($total,2)."</b></td></tr>";
				}else{
					echo "<tr class='tr2'><td colspan='8'>Không có dữ liệu cho loại xuất kho này</td></tr>";
				}

				if(count($data1)>0){
					$total = 0;
					foreach($data1 as $row){
						echo "<tr class='tr1'>";
						echo "<td>".date('d/m/Y',strtotime($row->TransferDate))."</td>";
						echo "<td>$row->Code</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Tensanpham</td>";
						echo "<td>$row->Donvi</td>";
						echo isset($arr_shipment[$row->ShipmentID]['ShipmentCode']) ? "<td>".$arr_shipment[$row->ShipmentID]['ShipmentCode']."</td>" : "<td>--</td>";
						echo isset($arr_shipment[$row->ShipmentID]['DateExpiration']) ? "<td>".date('d/m/Y',strtotime($arr_shipment[$row->ShipmentID]['DateExpiration']))."</td>" : "<td>--</td>";
						echo "<td>".number_format($row->TotalExport,2)."</td>";
						echo "</tr>";
						$total = $total+$row->TotalExport;
					}
					echo "<tr class='tr1'><td></td><td></td><td></td><td></td><td></td><td></td><td><b>TỔNG CỘNG</b></td><td><b>".number_format($total,2)."</b></td></tr>";
				}else{
					echo "<tr class='tr1'><td colspan='8'>Không có dữ liệu cho loại xuất kho này</td></tr>";
				}
				?>
			</table>
		</div>
	</div>
</div>
<style>
	.body_content .containner .black .box_inner .block2_inner table tr td{padding:5px 6px;}
	.body_content .containner .black .box_inner .block2_inner table tr td:last-child{text-align:right;}
	.body_content .containner .black .box_inner .block2_inner table tr.tr2,.body_content .containner .black .box_inner .block2_inner table tr.tr1{display:none;}
</style>
<script>
	
	$("#close_overlay").click(function(){
        $(".over_lay").hide();
        disablescrollsetup();
    });

    function showtr(ob,tr){
    	$(ob).parent('th').find('a').removeClass('btn-success');
    	$(ob).parent('th').find('a').addClass('btn-default');
    	$(ob).addClass('btn-success');
    	$(".block2_inner table tr.tr1").hide();
    	$(".block2_inner table tr.tr2").hide();
    	$(".block2_inner table tr.tr3").hide();
    	$(".block2_inner table tr.tr"+tr).show();
    }
</script>