<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
</style>
<div class="table_data">
    <table id="table_data">
        <tr>
            <th class="text-center"></th>
            <th class="text-center col-xs-2">Voucher</th>
            <th class="text-center col-xs-2">Tình trạng</th>
            <th class="text-center">Dùng cho</th>
            <th class="text-center">Thời gian</th>
        </tr>
        <?php
        $i = 0;
        foreach($data as $row){
            $i++;
        ?>
        <tr>
            <td class="text-center"><?=$i;?></td>
            <td class="text-center"><?=$row->code?></td>
            <td class="text-center"><?=($row->active == 0)? "<i class='fa fa-square-o'></i>" : "<i class='fa fa-square'></i>" ?></td>
            <td class="text-center"><?=($row->apply) ? $row->apply : ""?></td>
            <td class="text-center"><?=$row->apply_date?></td>
        </tr>
        <?php }?>
    </table>
</div>