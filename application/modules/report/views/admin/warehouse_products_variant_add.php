<div class="variant_row variant_<?php echo $data->ID ?>">
		<div class="minitools">
			<a title="Mở rộng/Thu gọn thông tin" onclick="showtable(this)"><i class="fa fa-caret-down"></i></a>
			<a title="Xóa biến thể này" onclick="rm_variant(this,<?php echo $data->ID ?>)"><i class="fa fa-times"></i></a>
		</div>
		<?php 
		$current_variant_properties = json_decode($data->VariantValue,true);
		$current_variant_properties = is_array($current_variant_properties) ? $current_variant_properties : array() ;
		$Properties = json_decode($parentdata->VariantValue,true);
		$TemPropertiesID = isset($Properties['Properties']) ? $Properties['Properties'] : array() ;
		$PropertiesID = count($TemPropertiesID)>0 ? implode(',',$TemPropertiesID) : '0' ;
		$PropertiesPublished = isset($Properties['Published']) ? $Properties['Published'] : array() ;
		$arr_parent_id = array();
		$current_parent_properties = $this->db->query("select ID,Title,ParentID from ttp_report_properties where ID in ($PropertiesID)")->result();
		$arr_parent = array();
		if(count($current_parent_properties)>0){
			foreach($current_parent_properties as $row){
				$selected = in_array($row->ID,$current_variant_properties) ? "selected='selected'" : '' ;
				if(isset($arr_parent[$row->ParentID])){
					$arr_parent[$row->ParentID] .= "<option value='$row->ID' $selected>$row->Title</option>";
				}else{
					$arr_parent_id[] = $row->ParentID;
					$arr_parent[$row->ParentID] = "<option value='$row->ID' $selected>$row->Title</option>";
				}
			}
		}
		if(count($arr_parent)>0){
			echo "<div class='properties_option'><li><b>#".$data->ID."</b></li>";
			$parent = $this->db->query("select ID,Title from ttp_report_properties where ID in (".implode(',',$arr_parent_id).")")->result();
			if(count($parent)>0){
				foreach($parent as $row){
					echo "<li>";
					echo '<b>'.$row->Title.'</b><select class="Variant_Properties">'.$arr_parent[$row->ID].'</select>';
					echo "</li>";
				}
				echo "</div>";
			}
		}
		?>
		<div class="table">
			<div class="row">
				<div class="col-xs-4">
					<div class="image_variant">
						<?php 
						$image = $this->db->query("select ID,Url from ttp_report_images_products where ProductsID=$data->ID and PrimaryImage=1")->row();
						$imagedefault = $image ? $image->ID : 0 ;
						echo $image ? "<img src='$image->Url' class='img-responsive img-primary-variant' />" : "<img src='public/admin/images/default_image.PNG' class='img-responsive img-primary-variant' />" ;
						?>
						<div class="list-preview-images row">
							<?php 
							$images = $this->db->query("select * from ttp_report_images_products where ProductsID=$data->ID")->result();
							if(count($images)>0){
								$i=0;
								foreach($images as $row){
									$primary = $imagedefault==$row->ID ? "text-info" : '' ;
									echo file_exists($this->lib->get_thumb($row->Url)) ? "<div class='col-xs-3'><img src='".$this->lib->get_thumb($row->Url)."' class='img-responsive' data='$row->ID' onclick='select_image(this)' data-src='$row->Url' data-selected='0' /><a class='status_primary $primary' onclick='setprimaryimage(this,$row->ID,$data->ID)' title='Đặt ảnh này làm ảnh chính của sản phẩm'><i class='fa fa-picture-o' aria-hidden='true'></i></a></div>" : "";
									$i++;
								}
							}
							?>
						</div>
						<div class="input-group-btn">
							<a class="btn btn-default" onclick="remove_images_variant(this,<?php echo $data->ID ?>)"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>
							<a class="btn btn-default" onclick="activeupload(this,'filemultirow',<?php echo $data->ID ?>)"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i> Upload</a>
							<a class="btn btn-primary btn-file">
								<i class="fa fa-folder-open" aria-hidden="true"></i> Browse...
								<input type="file" onchange="changemultipleimages(this)" id='multipleimages<?php echo $data->ID ?>' data="<?php echo $data->ID ?>" multiple />
							</a>
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="row">
						<div class="col-xs-6">
							<input type='checkbox' checked="checked" class="Variant_Published" /> Kích hoạt biến thể
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<label class="col-xs-5 control-label">Tình trạng tồn kho</label>
							<div class="col-xs-7">
								<select class="Variant_InventoryStatus form-control"><option value="1">Còn hàng</option><option value="0">Hết hàng</option></select>
							</div>
						</div>
						<div class="col-xs-6">
							<label class="col-xs-5 control-label">SKU</label>
							<div class="col-xs-7">
								<input type='text' class="Variant_SKU form-control" value="<?php echo $data->MaSP ?>" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<label class="col-xs-5 control-label">Giá bán (VNĐ)</label>
							<div class="col-xs-7">
								<input type='text' class="Variant_Price form-control" value="<?php echo $data->Price ?>" />
							</div>
						</div>
						<div class="col-xs-6">
							<label class="col-xs-5 control-label">Giá đặc biệt (VNĐ)</label>
							<div class="col-xs-7">
								<input type='text' class="Variant_SpecialPrice form-control" value="<?php echo $data->SpecialPrice ?>" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12"><div class="col-xs-12"><a onclick="show_dateinput(this)" class="pull-left text-info">Kế hoạch áp dụng giá đặc biệt <i class="fa fa-caret-down"></i></a></div></div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<label class="col-xs-5 control-label">Bắt đầu kế hoạch</label>
							<div class="col-xs-7">
								<input type='text' class="Variant_StartSpecialPrice form-control" value="<?php echo $data->SpecialStartday!="0000-00-00" ? $data->SpecialStartday : '' ; ?>" />
							</div>
						</div>
						<div class="col-xs-6">
							<label class="col-xs-5 control-label">Kết thúc kế hoạch</label>
							<div class="col-xs-7">
								<input type='text' class="Variant_StopSpecialPrice form-control" value="<?php echo $data->SpecialStopday!="0000-00-00" ? $data->SpecialStopday : '' ; ?>" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<label class="col-xs-5 control-label">Trọng lượng (kg)</label>
							<div class="col-xs-7">
								<input type='text' class="Variant_Weight form-control" value="<?php echo $data->Weight ?>" />
							</div>
						</div>
						<div class="col-xs-6">
							<label class="col-xs-5 control-label">Kích thước (cm)</label>
							<div class="col-xs-7">
								<div class="input-group col-xs-4 pull-left" style="display:inline-table">
									<input type='text' placeholder="Dài" class="Variant_Length form-control" value="<?php echo $data->Length ?>" placeholder="Dài" />
								</div>
								<div class="input-group col-xs-4 pull-left" style="display:inline-table">
									<input type='text' placeholder="Rộng" class="Variant_Width form-control" value="<?php echo $data->Width ?>" placeholder="Rộng" />
								</div>
								<div class="input-group col-xs-4 pull-left" style="display:inline-table">
									<input type='text' placeholder="Cao" class="Variant_Height form-control" value="<?php echo $data->Height ?>" placeholder="Cao" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-12">
								<textarea class="Variant_Description form-control" placeholder="Mô tả ngắn biến thể"><?php echo $data->Description ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-12">
								<a class="btn btn-primary pull-right" onclick="save_variant(this,<?php echo $data->ID ?>)"><i class="fa fa-check" aria-hidden="true"></i> Lưu biến thể</a>
								<a class="btn btn-success pull-right" style="margin-right:10px;display:none"><i class="fa fa-check-circle" style='margin-right:5px;'></i> Lưu thành công</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
