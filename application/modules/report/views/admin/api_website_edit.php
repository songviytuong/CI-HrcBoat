<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa thông tin website</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Lưu thông tin</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Trạng thái hoạt động</label>
							<div class='col-xs-10'>
								<input type='radio' name="Published" value="1" <?php echo $data->Published==1 ? 'checked="true"' : '' ; ?> /> Enable 
								<input type='radio' name="Published" value="0" <?php echo $data->Published==0 ? 'checked="true"' : '' ; ?> /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Tên miền (Domain)</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="Domain" placeholder="www.example.com.vn" value='<?php echo $data->Domain ?>' required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">IP website</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="IP" value='<?php echo $data->IP ?>' required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">URL (Link api internal)</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="ServicesPath" value='<?php echo $data->ServicesPath ?>' required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Key Active Domain</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="ActiveKey" value='<?php echo $data->ActiveKey ?>' required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label"></label>
							<div class='col-xs-10'><a><i class="fa fa-download"></i> Download this key</a></div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>