<?php 
$result_fillter = array();
$fill_temp_value="";
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>DANH MỤC SẢN PHẨM</h1>
        </div>
        <div class="block2">
            
        </div>
    </div>
    <div class="import_orderlist">
        <div class="block2">
            <div class="block_2_1">
                <a class="btn btn-danger" href="<?php echo $base_link.'add' ?>"><i class="fa fa-plus"></i> Sản phẩm</a>
            </div>
            <div class="block_2_2">
                <button class="btn btn-default" onclick="showtools()">Advanced Filter <b class="caret"></b></button>
                <a class="btn btn-primary" role="button" onclick="export_data(this)"><i class="fa fa-download"></i> Export</a>
            </div>
        </div>
        <div class="clear"></div>
        <div class="export_tools_kt">
            <form action="<?php echo base_url().ADMINPATH."/report/warehouse_products/export_products" ?>" method="post" id="falseclass">
                <span>Xuất từ ngày: </span>
                <input type='text' name="Export_date" class="form-control date-picker" id="Export_date" value="<?php echo date('Y-m-d',time()-(3600*8)); ?>" required />
                <span>Đến ngày: </span>
                <input type='text' name="ExportStop_date" class="form-control date-picker" id="ExportStop_date" value="<?php echo date('Y-m-d',time()-(3600*8)); ?>" required />
                <span>Chọn loại báo cáo: </span>
                <select name="TypeExport">
                    <option value="1">In Barcode</option>
                </select>
                <button type="submit" class="btn btn-default">Xuất dữ liệu</button>
            </form>
        </div>
        <div class="filltertools">
            <form action="<?php echo $base_link."setfillter" ?>" method="post">
                <?php 
                    $arr_fieldname = array(0=>"a.MaSP",1=>"a.Title",2=>"b.ID",3=>"a.Price",4=>"a.CurrentAmount",5=>"a.CategoriesID");
                    $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                    $arr_showfieldname = array(0=>"Mã SKU",1=>"Tên sản phẩm",2=>"Thương hiệu",3=>"Giá bán",4=>"Số lượng tồn hiện tại",5=>"Ngành hàng cấp 2");
                    $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                    $fill_data_arr = explode(" and ",$fill_data);
                    $arr_field = array();
                    if(count($fill_data_arr)>0 && $fill_data!=''){
                        $temp_tools=0;
                        foreach($fill_data_arr as $row){
                            $param = explode(' ',$row,3);
                            $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                            if(isset($param[0])){
                                $arr_field[] = "'data".array_search($param[0],$arr_fieldname)."'";
                                $k = array_search($param[0],$arr_fieldname);
                            }else{
                                $k=0;
                            }
                            $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                            $param_oparation = isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                            $param_value = isset($param[2]) ? $param[2] : '' ;
                            $param_value = str_replace("\'","",$param_value);
                            $param_value = str_replace("'","",$param_value);
                            $param_value = str_replace("%","",$param_value);
                ?>
                <div class="row <?php echo $temp_tools==0 ? "base_row row$k" : " row$k" ; ?>">
                    <div class="list_toolls col-xs-1"><label class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></label></div>
                    <div class="list_toolls col-xs-2">
                        <input type="hidden" class="FieldName form-control" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                        <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></label>
                        <ul class="dropdownbox" style='width: 215px;'>
                            <li><a onclick="setfield(this,0,'masp')">Mã SKU</a></li>
                            <li><a onclick="setfield(this,1,'title')">Tên sản phẩm</a></li>
                            <li><a onclick="setfield(this,2,'trademark')">Thương hiệu</a></li>
                            <li><a onclick="setfield(this,3,'price')">Giá bán lẻ</a></li>
                            <li><a onclick="setfield(this,4,'currentamount')">Số lượng tồn kho</a></li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation col-xs-2">
                        <select class="oparation form-control" name="FieldOparation[]">
                            <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                            <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                            <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                            <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                            <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                            <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                            <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter col-xs-3">
                        <?php 
                        if($value_field==2){
                            $trademark = $this->db->query("select ID,Title from ttp_report_trademark")->result();
                            if(count($trademark)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($trademark as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                                echo "</select>";
                            }
                        }elseif($value_field==5){ 
                            if($param_value!=''){
                                $check_categories = $this->db->query("select Title from ttp_report_categories where ID=$param_value")->row();
                                $fill_temp_value = $check_categories ? $check_categories->Title : $param_value;
                                echo '<input type="text" class="form-control" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                            }
                        }else{
                            $fill_temp_value = $param_value ;
                            echo '<input type="text" class="form-control" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                        }
                        ?>
                    </div>
                    <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
                <?php   
                        $temp_tools++;
                        $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                        $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                        }
                    }else{
                ?>
                    <div class="row base_row">
                        <div class="list_toolls col-xs-1"><label class="title first-title">Chọn</label></div>
                        <div class="list_toolls col-xs-2">
                            <input type="hidden" class="FieldName form-control" name="FieldName[]" />
                            <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                            <ul class="dropdownbox" style='width: 215px;'>
                                <li><a onclick="setfield(this,0,'masp')">Mã SKU</a></li>
                                <li><a onclick="setfield(this,1,'title')">Tên sản phẩm</a></li>
                                <li><a onclick="setfield(this,2,'trademark')">Thương hiệu</a></li>
                                <li><a onclick="setfield(this,3,'price')">Giá bán lẻ</a></li>
                                <li><a onclick="setfield(this,4,'currentamount')">Số lượng tồn kho</a></li>
                            </ul>
                        </div>
                        <div class="list_toolls reciveroparation col-xs-2">
                            <select class="oparation form-control" name="FieldOparation[]">
                                <option value="1">Bằng</option>
                                <option value="0">Có chứa</option>
                                <option value="2">Khác</option>
                                <option value="3">Lớn hơn</option>
                                <option value="4">Nhỏ hơn</option>
                                <option value="5">Lớn hơn hoặc bằng</option>
                                <option value="6">Nhỏ hơn hoặc bằng</option>
                            </select>
                        </div>
                        <div class="list_toolls reciverfillter col-xs-3">
                            <input type="text" name="FieldText[]" class="form-control" id="textsearch" />
                        </div>
                        <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                    </div>
                <?php
                    }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-11">
                                <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                                <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="block3 table_data">
            <?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            ?>
            <table id="table_data">
                <tr>
                    <th>STT</th>
                    <th>Mã SKU <br>Barcode</th>
                    <th>Tên sản phẩm</th>
                    <th>Ngành hàng cấp 1</th>
                    <th>Ngành hàng cấp 2</th>
                    <th>Thương hiệu </th>
                    <th>Giá bán lẻ</th>
                    <th>Action</th>
                </tr>
                <?php 
                $categories = $this->db->query("select ID,Title,Path from ttp_report_categories")->result();
                $arr_categories = array();
                if(count($categories)>0){
                    foreach($categories as $row){
                        $temp_path = explode('/',$row->Path);
                        $temp_path = isset($temp_path[0]) ? $temp_path[0] : $row->ID ;
                        $arr_categories[$row->ID] = array('Title'=>$row->Title,'RootParent'=>$temp_path);
                    }
                }
                $i = $start+1;
                $arr_products = array();
                if(count($data)>0){
                    foreach($data as $row){
                        $arr_products[] = $row->ID;
                        $last_categories = json_decode($row->CategoriesID,true);
                        echo "<tr>
                                <td style='text-align:center'>$i</td>
                                <td><a href='{$base_link}edit/$row->ID'>$row->MaSP</a></td>
                                <td><a href='{$base_link}edit/$row->ID'>".$row->Title."</a></td>
                                <td style='text-align:center'>";
                        if(is_array($last_categories) && count($last_categories)>0){
                            foreach($last_categories as $value){
                                echo isset($arr_categories[$value]['RootParent']) ? "<a class='categories_a' href='{$base_link}edit/$row->ID''>".$arr_categories[$arr_categories[$value]['RootParent']]['Title']."</a>" : '' ;
                            }
                        }
                        echo "</td><td style='text-align:center'>";
                        if(is_array($last_categories) && count($last_categories)>0){
                            foreach($last_categories as $value){
                                echo isset($arr_categories[$value]['Title']) ? "<a class='categories_a' href='{$base_link}edit/$row->ID''>".$arr_categories[$value]['Title']."</a>" : '' ;
                            }
                        }
                            echo "</td><td style='text-align:center'><a href='{$base_link}edit/$row->ID''>$row->Trademark</a></td>
                                <td style='text-align:center'>".number_format($row->Price)."</td>
                                <td style='width:68px'><a title='Xóa sản phẩm này' href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i></a></td>";
                        echo "</tr>";
                        $i++;
                    }
                    echo "<tr><td colspan='8'>Tìm thấy tổng cộng <b>".number_format($find,0)."</b> sản phẩm </td></tr>";
                }else{
                    echo "<tr><td colspan='8'>Không tìm thấy sản phẩm.</td></tr>";
                }
                ?>
            </table>
            <?php 
                echo $nav;
            ?>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/warehouse_products/" ?>" />
</div>
<script>
    
    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('label.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="trademark"){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            if(fieldname=="masp" || fieldname=="title"){
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="0">Có chứa</option><option value="2">Khác</option></select>';
            }else{
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
            }
        }
    }

    $(".fill_tr td a.title").click(function(){  
        var type= $(this).attr('data');
        var baselink = $("#baselink_report").val();
        var parent = $(this).parent('div').find('.dropdown_tr');
        var isload = parseInt($(this).attr('isload'));
        if(isload==0){
            $.ajax({
                url: baselink+"get_data_fillter",
                dataType: "html",
                type: "POST",
                data: "Type="+type,
                context:this,
                success: function(result){
                    parent.html(result);
                }
            });
            $(this).attr('isload',1);
        }
        parent.toggle();
    });

    var arr_field = [<?php echo implode(',',$arr_field) ?>];

    function set_fill_tr(ob,field){
        if(jQuery.inArray( "data"+field, arr_field )>=0){
            $(".filltertools form .row"+field).remove();
        }
        var data = $(ob).attr('value');
        $(".filltertools form").html("");
        $(".filltertools form").prepend("<input type='hidden' name='FieldName[]' value='"+field+"' />");
        $(".filltertools form").prepend("<input type='hidden' name='FieldOparation[]' value='1' />");
        $(".filltertools form").prepend("<input type='hidden' name='FieldText[]' value='"+data+"' />");
        $(".filltertools form").submit();
    }

    function export_data(ob){
        $(".export_tools_kt").toggle();
    }

    $("#falseclass").submit(function(){
        $(this).find('button').removeClass("saving");
    });
</script>
