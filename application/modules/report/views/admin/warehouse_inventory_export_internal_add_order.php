<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>THÔNG TIN YÊU CẦU XUẤT CHO / TẶNG / HỦY</h1>
	    </div>
	    <div class="block2">
	    	<div class="btn_group">
				<a type='button' value="Cập nhật" class="btn btn-update btn-danger submit" id="buttonsubmit"><i class="fa fa-check-square"></i> Duyệt</a>
			</div>
	    </div>
    </div>
    <div class="import_order_info">
    	<form id="submit_order" method="POST" action="<?php echo $base_link.'add_new_order' ?>">
	    	<div class="block1">
	    		<div class="row">
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Tên người yêu cầu : </label>
			    			<div class="col-xs-8">
				    			<select name="UserID" id="SalesmanID" class="form-control">
				    				<?php 
				    				echo "<option value='".$this->user->ID."'>".$this->user->UserName."</option>";
				    				?>
				    			</select>
			    			</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Người nhận hàng : </label>
			    			<div class="col-xs-8">
				    			<input type="text" name="Nguoinhanhang" class="form-control required" id="Nguoinhanhang" required />
		    					<input type='hidden' name="Ngayban" class="form-control date-picker" id="Ngayban" value="<?php echo date('m/d/Y',time()); ?>" required />
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Phòng ban / bộ phận : </label>
			    			<div class="col-xs-8">
				    			<input type="text" name="Phongban" class="form-control required" id="Phongban" required />
	    						<input type='hidden' name="Address" id="Address_order" class="form-control" value="246/9 Bình Quới, Phường 28, Quận Bình Thạnh, Thành phố Hồ Chí Minh" required />
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Lý do xuất : </label>
			    			<div class="col-xs-8">
				    			<input type='text' name="Note" class="form-control required" id='Note' required />
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Chọn kho xuất hàng : </label>
	    					<div class="col-xs-8">
	    						<select name="KhoID" id="KhoID" class="form-control" onchange="loadshipmentdefault()">
				    				<option value="">-- Chọn Kho --</option>
				    				<?php 
				    				if($this->user->UserType==2 || $this->user->UserType==8){
				    					$khoresult = $this->db->query("select * from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%'")->result();
				    				}else{
				    					$khoresult = $this->db->query("select * from ttp_report_warehouse")->result();
				    				}
				    				if(count($khoresult)>0){
				    					foreach($khoresult as $row){
				    						$selected = '';
				    						if($data->KhoID>0){
				    							$selected = $data->KhoID==$row->ID ? "selected='selected'" : '' ;
				    						}else{
				    							if($tinhthanh){
				    								$selected = $tinhthanh->DefaultWarehouse==$row->ID ? "selected='selected'" : '' ;
				    							}
				    						}
				    						echo "<option value='$row->ID' $selected>$row->MaKho</option>";
				    					}
				    				}
				    				?>
				    			</select>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Địa chỉ nhận hàng : </label>
	    					<div class="col-xs-10">
								<input type='text' name="Address" class="form-control special_input" />
							</div>
						</div>
					</div>
	    		</div>
	    	</div>

	    	<!-- end block1 -->
	    	<div class="block2">
				<a class="btn btn-primary" id="add_products_to_order"><i class="fa fa-plus"></i> Sản phẩm</a>
				<ul>
					<li><a id='show_thaotac'>Thao tác <b class="caret"></b></a>
	    				<ul>
		    				<li><a id="edit_row_table"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a></li>
		    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
	    				</ul>
					</li>
				</ul>
				
	    	</div>
	    	<div class="table_donhang table_data">
	    		<table class="table_data" id="table_data">
	    			<tr>
	    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
	    				<th>Mã sản phẩm</th>
	    				<th>Tên sản phẩm</th>
	    				<th>Lô</th>
	    				<th>Số lượng</th>
	    			</tr>
	    		</table>
	    	</div>
	    	<div class="block1">
	    		<input type='hidden' name="Tinhtrangdonhang" id="Status" value='2' />
	    	</div>
    	</form>
    </div>
    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
	.daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Ngayban').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        });
    });
</script>
<script type="text/javascript">

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	var link = $("#baselink_report").val();
	$("#add_products_to_order").click(function(){
		var KhoID = $('#KhoID').val();
		if(KhoID=='' || KhoID==0){
			alert('Vui lòng chọn kho lấy hàng');
			$('#KhoID').focus();
			return false;
		}
		var Nguoinhanhang = $('#Nguoinhanhang').val();
		if(Nguoinhanhang==''){
			alert('Vui lòng điền người nhận hàng');
			$('#Nguoinhanhang').focus();
			return false;
		}
		var Phongban = $('#Phongban').val();
		if(Phongban==''){
			alert('Vui lòng điền phòng ban');
			$('#Phongban').focus();
			return false;
		}
		var Note = $('#Note').val();
		if(Note==''){
			alert('Vui lòng điền nội dung lý do xuất hàng');
			$('#Note').focus();
			return false;
		}

		enablescrollsetup();
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"warehouse_inventory_export/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID+"&Show=1",
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);
			    	$(".over_lay").removeClass('in');
			    	$(".over_lay").fadeIn('fast');
        			$(".over_lay").addClass('in');
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
                $(this).removeClass('saving');
            }
        });
	});

	function input_search_products(ob){
		var data = $(ob).val();
		$.ajax({
        	url: link+"warehouse_inventory_export/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Title="+data+"&KhoID="+KhoID,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
	}

	$("#edit_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
			}
		});
	});

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').hide();
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').show();
				parent.find('input[type="text"]').prop('readonly', true);
				parent.find('input.soluong').prop('readonly', true);
				parent.find('input[type="text"]').removeClass('border');
				parent.find('input.soluong').removeClass('border');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		if(ob.checked===true){
			parent.find('span.showsoluong').hide();
			parent.find('input[type="text"]').prop('readonly', false);
			parent.find('input.soluong').prop('readonly', false);
			parent.find('input[type="text"]').addClass('border');
			parent.find('input.soluong').addClass('border');
		}else{
			parent.find('span.showsoluong').show();
			parent.find('input[type="text"]').prop('readonly', true);
			parent.find('input.soluong').prop('readonly', true);
			parent.find('input[type="text"]').removeClass('border');
			parent.find('input.soluong').removeClass('border');
		}
	}

	var celldata = [];
	var sttrow = 1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				var table = document.getElementById("table_data");
				var row = table.insertRow(sttrow);
				row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
				row.insertCell(1).innerHTML=data_code;
				row.insertCell(2).innerHTML=data_name;
				row.insertCell(3).innerHTML="<select class='ShipmentDefault' name='ShipmentID[]' data-id='"+data_id+"' onchange='get_available_by_shipment("+data_id+",this)'></select><span class='Availabletd'></span>";
				row.insertCell(4).innerHTML="<input type='number' name='Amout[]' class='soluong' value='1' min='1' readonly='true' />";
				sttrow=sttrow+1;
				celldata.push("data"+data_id);
			}
		});
		loadshipmentdefault();
		$(".over_lay").hide();
		disablescrollsetup();
	}

	function get_available_by_shipment(id,ob){
		var KhoSender = $('#KhoID').val();
		var Shipment = $(ob).val();
		$(ob).parent('td').find('.Availabletd').load(link+"warehouse_inventory_export/get_available_by_shipment/"+id+"/"+KhoSender+"/"+Shipment);
	}

	function checkshipment(ob){
		var amount = $(ob).val();
		var warehouse = $('#KhoID').val();
		var id = $(ob).parent('td').parent('tr').find('.ShipmentDefault').attr('data-id');
		$(ob).parent('td').parent('tr').find('.ShipmentDefault').load(link+"import_order/get_shipment_default/"+id+"/"+warehouse+"/"+amount);
	}

	function loadshipmentdefault(){
		var warehouse = $('#KhoID').val();
		$(".ShipmentDefault").each(function(){
			var data = $(this).attr('data-id');
			$(this).load(link+"warehouse_inventory_export/get_shipment_by_productsID/"+data+"/"+warehouse);
		});
	}

	function add_shipment(ob,id){
		var data = $(ob).val();
		$(ob).attr('data-id',data);
		if(data=="add"){
			enablescrollsetup();
			$(".over_lay .box_inner").css({"width":"500px"});
			$(".over_lay .box_inner .block1_inner h1").html("Tạo lô sản phẩm");
			$(".over_lay .box_inner .block2_inner").html("");
			$(".over_lay .box_inner .block2_inner").load(link+"/warehouse_inventory_import/box_add_shipment/"+id);
			$(".over_lay").removeClass('in');
	    	$(".over_lay").fadeIn('fast');
			$(".over_lay").addClass('in');
		}
	}

	function save_shipment(ob,id){
		$(ob).addClass("saving");
		var ShipmentCode = $(".ShipmentCode").val();
		var DateProduction = $(".DateProduction").val();
		var DateExpiration = $(".DateExpiration").val();
		if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
			alert("Vui lòng điền đầy đủ thông tin !");
			$(ob).removeClass("saving");
			return false;
		}
		$.ajax({
        	url: link+"/warehouse_inventory_import/save_shipment",
            dataType: "html",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
            	load_shipment(id);
            	$(".over_lay").hide();
				disablescrollsetup();
				$(ob).removeClass("saving");
            }
        });
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				celldata.splice("data"+data_id, 1);
				sttrow = sttrow-1;
			}
		});
	});

	$("#buttonsubmit").click(function(){
		$("#Status").val(2);
		var Nguoinhanhang = $("#Nguoinhanhang").val();
		if(Nguoinhanhang==''){
			alert("Vui lòng điền người nhận hàng");
			return false;
		}
		var Phongban = $("#Phongban").val();
		if(Phongban==''){
			alert("Vui lòng điền phòng ban yêu cầu xuất kho");
			return false;
		}
		var Note = $("#Note").val();
		if(Note==''){
			alert("Vui lòng điền lý do xuất kho");
			return false;
		}
		var KhoID = $("#KhoID").val();
		if(KhoID=='' || KhoID==0){
			alert("Vui lòng chọn kho xuất hàng");
			return false;
		}
		if(celldata.length==0){
			alert("Đơn hàng đang rỗng .Vui lòng thêm sản phẩm vào đơn hàng .");
			return false;
		}

		$(this).addClass("saving");
		$(this).find("i").hide();
		$("#submit_order").submit();
	});

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}	
</script>