<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Thêm sản phẩm</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tên sản phẩm</span></div>
					<div class='block2'><input type='text' class="form-control" name="Title" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Mã sản phẩm</span></div>
					<div class='block2'><input type='text' class="form-control" name="MaSP" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Giá bán sản phẩm</span></div>
					<div class='block2'><input type='number' class="form-control" name="Price" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Thuộc nhóm ngành hàng</span></div>
					<div class='block2'>
						<select name="CategoriesID" class="form-control">
							<option value="0">-- Chọn nhóm ngành hàng --</option>
							<?php 
							$categories = $this->db->query("select * from ttp_report_categories")->result();
							if(count($categories)>0){
								foreach($categories as $row){
									echo "<option value='$row->ID'>$row->Title</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Đơn vị tính</span></div>
					<div class='block2'>
						<select name="Donvi" class="form-control">
							<option value="">-- Chọn đơn vị tính --</option>
							<option value="Hộp">Hộp</option>
							<option value="Cái">Cái</option>
							<option value="Bộ">Bộ</option>
							<option value="Thùng">Thùng</option>
						</select>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>