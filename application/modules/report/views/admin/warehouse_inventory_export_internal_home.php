<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$month          = array();
$dayofmonth     = array();
$dayofmonth_real= array();
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>DANH SÁCH PHIẾU XUẤT KHO CHO / TẶNG / HỦY</h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="import_orderlist">
        <div class="block2">
            
        </div>
        <div class="clear"></div>
        <div class="block3 table_data">
            <table id="table_data">
                <tr>
                    <th>STT</th>
                    <th>Ngày</th>
                    <th>Mã XK</th>
                    <th>Mã ĐH</th>
                    <th>ID ĐH</th>
                    <th>Hình thức</th>
                    <th>Mã kho</th>
                    <th>Lý do xuất kho</th>
                    <th>User</th>
                    <th>Action</th>
                </tr>
                <?php 
                $arr_hinhthuc = array(0=>"NB",1=>"TA");
                if(count($data)>0){
                    $i = $start+1;
                    foreach($data as $row){
                        echo "<tr>";
                        echo "<td>$i</td>";
                        echo "<td>".date('d/m/Y',strtotime($row->Ngayxuatkho))."</td>";
                        echo "<td>".$row->MaXK."</td>";
                        echo "<td>".$row->MaDH."</td>";
                        echo "<td>".$row->IDDH."</td>";
                        echo isset($arr_hinhthuc[$row->Hinhthucxuatkho]) ? "<td>".$arr_hinhthuc[$row->Hinhthucxuatkho]."</td>" : "<td>--</td>";
                        echo "<td>$row->MaKho</td>";
                        echo "<td>$row->Lydoxuatkho</td>";
                        echo "<td>".$row->UserName."</td>";
                        echo "<td><a style='color:#27c;text-decoration:underline;display:inline' href='".base_url().ADMINPATH."/report/warehouse_inventory_export/lapphieuxuatkho/$row->OrderID'>Xem phiếu xuất</a> | <a href='".base_url().ADMINPATH."/report/warehouse_inventory_export/printorder/$row->OrderID' style='color:#27c;text-decoration:underline;display:inline'>Xem ĐH</a></td>";
                        echo "</tr>";
                        $i++;
                    }
                }else{
                    echo "<tr><td colspan='10'>Không tìm thấy phiếu xuất kho.</td></tr>";
                }
                ?>
            </table>
            <?php 
                echo $nav;
            ?>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<?php 
    $time_startday = strtotime($startday);
    $time_stopday = strtotime($stopday);
    $currday = strtotime(date('Y-m-d',time()));
?>
<script>
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day_warehouse",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

</script>