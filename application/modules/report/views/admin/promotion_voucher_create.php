<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
</style>
<div class="table_data">
    <table id="table_data">
        <tr>
            <th class="text-center vidu"></th>
            <th class="text-center col-xs-4"><input type="number" min="0" max="200" id="_number" name="_number" value="" placeholder="Số lượng Voucher" class="form-control"/></th>
            <th class="text-center col-xs-4"><select class="form-control opt" name="opt">
                    <option value=6>6 Ký tự</option>
                    <option value=8>8 Ký tự</option>
                </select></th>
                <th class="text-center col-xs-2"><button class="btn btn-danger start" rel="<?=$promo_id;?>">Bắt đầu</button></th>
        </tr>
    </table>
</div>
<script type="text/javascript">
$(document).ready(function () {
    setTimeout(function(){
        $('.vidu').text("DXJK12");
        $('.start').attr("disabled",true);
    },100);
    
    $('.opt').change(function(){
        var opt = $('.opt').val();
        if(opt == 6){
            $('.vidu').text("DXJK12");
        }else{
            $('.vidu').text("DXJK12SS");
        }
    });
    
    $('#_number').keyup(function(){
        if($(this).val() == "" || $(this).val() == null){
            $('.start').attr("disabled",true);
        }else{
            $('.start').attr("disabled",false);
        }
        $(this).text("Bắt đầu");
    });
    
    $('#_number').keypress(function(e){
        if(e.which == 13) {
            $('.start').click();
        }
    });
    
    $("#close_overlay").click(function () {
        $(".over_lay").hide();
    });
    
    $('.start').click(function(){
        var promo_id = $(this).attr("rel");
        $('#_number').attr("disabled",true);
        $('.opt').attr("disabled",true);
        $(this).addClass("saving");
        $(this).text("Đang tạo");
        
        
        $.ajax({
            url: "<?=$base_link;?>createVoucherData",
            dataType: "html",
            type: "GET",
            data: "opt="+$('.opt').val()+"&promo_id="+promo_id+"&number="+$('#_number').val(),
            context: $(this),
            success: function(result){
                if(result == 'OK'){
                    $(this).text("Đã xong");
                    $(this).removeClass("saving");
                    $('#_number').attr("disabled",false);
                    $('#_number').val("");
                    $('.opt').attr("disabled",false);
                    
                    setTimeout(function(){
                        $("#close_overlay").click();
                    },500);
                    setTimeout(function(){
                        location.reload();
                    },1000);
                    
                    
                }else{
                    console.log(result);
                }
            }
        });
    });
});
</script>