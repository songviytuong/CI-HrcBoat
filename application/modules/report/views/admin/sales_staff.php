<?php 
$active_vs = 1;
$startday = $this->session->userdata("startday");
$startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
$stopday = $this->session->userdata("stopday");
$stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

$numday = $this->lib->get_nume_day($startday,$stopday);

$startday_vs = $this->session->userdata("startday_vs");
$startday_vs = $startday_vs!='' ? $startday_vs : date('Y-m-d',strtotime($startday)-($numday*3600*24)) ;
$stopday_vs = $this->session->userdata("stopday_vs");
$stopday_vs = $stopday_vs!='' ? $stopday_vs : date('Y-m-d',strtotime($startday)) ;


$Orderstatus = isset($_GET['status']) ? (int)$_GET['status'] : '' ;
$bonus_fillter = $Orderstatus=='' ? '' : ' and a.Status=0' ;
$Orderstatus = $Orderstatus=='' ? 0 : $Orderstatus ;

$ordertype = isset($_GET['ordertype']) ? (int)$_GET['ordertype'] : 0 ;


/*
*********************************
*	Define variable				*
*								*
*********************************
*/
$point_target = array();
$point_sales = array();
$point_history = array();

$team = $this->db->query("select a.*,b.UserName from ttp_report_team a,ttp_user b where a.UserID=b.ID and a.ID !=4 and a.ID!=5")->result();
$arr_team = array();
$arr_team_vs = array();
$arr_user = array();

if(count($team)>0){
	foreach($team as $row){
		$temp = json_decode($row->Data,true);
		if(count($temp)>0){
			foreach($temp as $item){
				$arr_user[$item]=$row->ID;
			}
		}
	}
}

/*
*********************************************
*	Excute and set variable	in first range	*
*											*
*********************************************
*/

$total_order = $this->db->query("select a.ID,a.Status,a.CustomerType,a.CustomerID,a.SoluongSP,a.Total,a.Chietkhau,a.Chiphi,a.Reduce,a.CityID,a.Ngaydathang,a.UserID from ttp_report_order a,ttp_user b where a.UserID=b.ID and date(a.Ngaydathang)>='$startday' and date(Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.CustomerID!=0 and a.OrderType=$ordertype $bonus_fillter")->result();
if(count($total_order)>0){
	foreach($total_order as $row){
		if(isset($arr_user[$row->UserID])){
			$ofteam = $arr_user[$row->UserID];
			if(isset($arr_team[$ofteam])){
				$arr_team[$ofteam] = $arr_team[$ofteam]+($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
			}else{
				$arr_team[$ofteam] = ($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
			}
		}
	}	
}

/*
*********************************************
*	Excute and set variable	in second range	*
*											*
*********************************************
*/

$total_order_vs = $this->db->query("select a.ID,a.Status,a.CustomerType,a.CustomerID,a.SoluongSP,a.Total,a.Chietkhau,a.Chiphi,a.Reduce,a.CityID,a.Ngaydathang,a.UserID from ttp_report_order a,ttp_user b where a.UserID=b.ID and date(a.Ngaydathang)>='$startday_vs' and date(Ngaydathang)<='$stopday_vs' and a.CustomerID!=9996 and a.CustomerID!=0 and a.OrderType=$ordertype $bonus_fillter")->result();
if(count($total_order_vs)>0){
	foreach($total_order_vs as $row){
		if(isset($arr_user[$row->UserID])){
			$ofteam = $arr_user[$row->UserID];
			if(isset($arr_team_vs[$ofteam])){
				$arr_team_vs[$ofteam] = $arr_team_vs[$ofteam]+($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
			}else{
				$arr_team_vs[$ofteam] = ($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
			}
		}
	}
}

?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
	<div class="select_progress">
	    <div class="block1">
	    	<a class="btn btn-primary">Export To Excel</a>
	    </div>
	    <div class="block2">
		    <div class="dropdown">
		    	<a class='value_excute'><span><?php echo date('d/m/Y',strtotime($startday))." - ".date('d/m/Y',strtotime($stopday)) ?></span> <b class="caret"></b></a>
		    	<p class="value_excute_vs" <?php echo $active_vs==1 ? "style='display:block'" : "style='display:none'" ; ?>>So sánh với : <span><?php echo date('d/m/Y',strtotime($startday_vs))." - ".date('d/m/Y',strtotime($stopday_vs)) ?></span></p>
		    	<div class="box_dropdown">
		    		<div id="reportrange" class="list_div">
				        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
				        <span></span> <b class="caret"></b>
				    </div>
				    <div class="center_div list_div">
				    	<span><input type='checkbox' id='active_vs' <?php echo $active_vs==1 ? "checked='checked'" : "" ; ?> />So sánh với</span>
				    	<select id="vs_selectbox" <?php echo $active_vs==1 ? "style='opacity:1'" : "style='opacity:0.6'" ; ?>>
				    		<option value="tuychinh">Tùy chỉnh</option>
				    		<option value="lastmonth">Tháng trước</option>
				    		<option value="last7day">7 ngày trước</option>
				    		<option value="last30day">30 ngày trước</option>
				    	</select>
				    </div>
				    <div id="reportrange1" class="list_div" <?php echo $active_vs==1 ? "style='display:block'" : "style='display:none'" ; ?>>
				        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
				        <span></span> <b class="caret"></b>
				    </div>
				    <div class='compare list_div'>
				    	<a id="showcompare" class="btn btn-danger"><i class="fa fa-exchange"></i> Áp dụng</a>
				    	<a id="cancel_showcompare" class="btn btn-default">Hủy</a>
				    </div>	
		    	</div>
		    </div>
	    </div>
    </div>
    <div class="title_content"><span>Báo cáo theo nhân viên</span></div>
    <div class="report_sales_fill">
    	<div class="block1">
    		<div>
	    		<select id="orderstatus">
	    			<option value="0" <?php echo $Orderstatus==0 ? "selected='selected'" : '' ; ?>>Tất cả đơn hàng</option>
	    			<option value="1" <?php echo $Orderstatus==1 ? "selected='selected'" : '' ; ?>>Đơn hàng thành công</option>
	    		</select>
	    	</div>
	    	<div><span>|</span></div>
	    	<div>
	    		<select onchange="changeordertype(this)">
	    			<option value="0" <?php echo $ordertype==0 ? "selected='selected'" : '' ; ?>>Online</option>
	    			<option value="3" <?php echo $ordertype==3 ? "selected='selected'" : '' ; ?>>Gom su</option>
	    		</select>
	    	</div>
	    	<div><span>|</span></div>
    		<div>
	    		<select id="value_sosanh">
	    			<option value="2">Sản phẩm bán</option>
	    			<option value="1">Doanh số</option>
	    		</select>
    		</div>
    		<div><span>|</span></div>
    		<div>
	    		<input type='radio' value="0" checked="checked" onclick="changeradio(this)" /> Theo nhóm
	    		<input type='radio' value="1" onclick="changeradio(this)" /> Theo cá nhân
    		</div>
    	</div>

    	<div class="block2 select_day_week_month">
    		<ul>
    			<li><a data='plotchart_primary' class='active'>Ngày</a></li>
    			<li><a data='plotchart_primary_week'>Tuần</a></li>
    			<li><a data='plotchart_primary_month'>Tháng</a></li>
    		</ul>
    	</div>

    	<div class="row">
	    	<div class="col-xs-12 table_data">
	    		<?php 
	    		$target = $this->db->query("select * from ttp_report_targets_year where Year=".date('Y'))->row();
    			$target = $target ? json_decode($target->DataDepartment,true) : array() ;
    			?>
	    		<table>
	    			<tr>
	    				<th class="width150">Tên nhóm</th>
	    				<th>Chỉ tiêu</th>
	    				<th>Thực hiện</th>
	    				<th>Lịch sử</th>
	    				<th>% tăng trưởng</th>
	    				<th>% chỉ tiêu</th>
	    				<th>Chênh lệch / chỉ tiêu</th>
	    			</tr>
	    			<?php 
	    			$total_target = 0;
	    			$total_sale = 0;
	    			$total_his = 0;
	    			$total_range = 0;
	    			$total_tangtruong = 0;
	    			if(count($team)>0){
	    				foreach($team as $row){
	    					$target_team = isset($target['type2']['Department'][2]['Month'][(int)date('m')]['Team'][$row->ID]['Total']) ? $target['type2']['Department'][2]['Month'][(int)date('m')]['Team'][$row->ID]['Total'] : 0 ;	    				
	    					
	    					$sales = isset($arr_team[$row->ID]) ? $arr_team[$row->ID] : 0 ;
	    					$sales_his = isset($arr_team_vs[$row->ID]) ? $arr_team_vs[$row->ID] : 0 ;
	    					$percent_sale = round($sales/($target_team/100),1);
	    					$range_sale = $sales-$target_team;
	    					$total_sale+= $sales;
	    					$total_his+= $sales_his;
	    					$total_target+=$target_team;
	    					$total_range+=$range_sale;

	    					$point_target[$row->ID] = $target_team;
	    					$point_sales[$row->ID] = $sales;
	    					$point_history[$row->ID] = $sales_his;
	    					$tangtruong = $sales_his==0 ? 0 : ($sales/$sales_his)-1;
	    					$icon_tangtruong = $tangtruong<0 ? '<i class="fa fa-caret-down text-danger"></i>' : '<i class="fa fa-caret-up text-success"></i>' ;
	    					$text_tangtruong = $tangtruong<0 ? 'text-danger' : 'text-success' ;
	    					echo "<tr>";
	    					echo "<td>$row->Title <br>($row->UserName)</td>";
	    					echo "<td>".number_format($target_team)."</td>";
	    					echo  "<td>".number_format($sales)."</td>";
	    					echo "<td>".number_format($sales_his)."</td>";
	    					echo "<td class='$text_tangtruong'>".round($tangtruong,2)." % $icon_tangtruong</td>";
	    					echo "<td>$percent_sale %</td>";
	    					echo "<td>".number_format($range_sale)."</td>";
	    					echo "</tr>";
	    				}
	    			}
	    			$percent_total = round($total_sale/($total_target/100),1);
	    			$total_tangtruong = $total_his==0 ? 0 : ($total_sale/$total_his)-1;
	    			$icon_tangtruong = $total_tangtruong<0 ? '<i class="fa fa-caret-down text-danger"></i>' : '<i class="fa fa-caret-up text-success"></i>' ;
					$text_tangtruong = $total_tangtruong<0 ? 'text-danger' : 'text-success' ;
	    			?>
	    			<tr>
	    				<td class="width150">Tổng cộng</td>
	    				<td><?php echo number_format($total_target) ?></td>
	    				<td><?php echo number_format($total_sale) ?></td>
	    				<td><?php echo number_format($total_his) ?></td>
	    				<td class="<?php echo $text_tangtruong ?>"><?php echo round($total_tangtruong,2).' % '.$icon_tangtruong ?></td>
	    				<td><?php echo $percent_total.' %'; ?></td>
	    				<td><?php echo number_format($total_range) ?></td>
	    			</tr>
	    		</table>
	    	</div>
	    </div>
    </div>
    <div class="chart linechart" style="height:480px;overflow:hidden">
		<div  id="placeholder" class="plotchart_item_primary" style="font-size: 16px;line-height: 1.2em;font-family:tahoma"></div>
	</div>
	<div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<!-- datepicker -->
<?php 
	$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
	$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
	
?>
<script type="text/javascript">
    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /****  reportrange 2  ****/
        var cb1 = function (start, end, label) {
            $('#reportrange1 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        var optionSet2 = {
            startDate: moment().startOf('month'),
            endDate: moment(),
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 120
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange1 span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange1').daterangepicker(optionSet2, cb1);

        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".value_excute span").html(picker.startDate.format('DD/MM/YYYY')+" - "+picker.endDate.format('DD/MM/YYYY'));
		});

		$('#reportrange1').on('apply.daterangepicker', function(ev, picker) {
		    $(".value_excute_vs span").html(picker.startDate.format('DD/MM/YYYY')+" - "+picker.endDate.format('DD/MM/YYYY'));
		    $(".value_excute_vs").show();
		});
    });
</script>
<!-- /datepicker -->

<script type="text/javascript">
	<?php 
	if(count($team)>0){
		$str_d1 = array();
		$str_d2 = array();
		$str_d3 = array();
		$arr_time = array(0=>1325376000000,1=>1328054400000,2=>1330560000000);
		$j=0;
		foreach($team as $row){
			$row_point_target = isset($point_target[$row->ID]) ? $point_target[$row->ID] : 0 ;
			$str_d1[]="[".$arr_time[$j].", $row_point_target]";
			$j++;
		}
		echo 'var d1_1 = ['.implode(',', $str_d1).'];';

		$j=0;
		foreach($team as $row){
			$row_point_history = isset($point_history[$row->ID]) ? $point_history[$row->ID] : 0 ;
			$str_d2[]="[".$arr_time[$j].", $row_point_history]";
			$j++;
		}
		echo 'var d1_2 = ['.implode(',', $str_d2).'];';

		$j=0;
		foreach($team as $row){
			$row_point_sales = isset($point_sales[$row->ID]) ? $point_sales[$row->ID] : 0 ;
			$str_d3[]="[".$arr_time[$j].", $row_point_sales]";
			$j++;
		}
		echo 'var d1_3 = ['.implode(',', $str_d3).'];';

		$arr_color = array(1=>'#AA4643',2=>'#89A54E',3=>'#4572A7');
		$arr_title = array(1=>'Chỉ tiêu',2=>'Lịch sử',3=>'Đạt được');
		$i=1;
		$var = array();
		foreach($team as $row){
			$var[] = '{
			            label: "'.$arr_title[$i].'", 
			            data: d1_'.$row->ID.',
			            bars: {
			                show: true,
			                barWidth: 12*24*60*60*300,
			                fill: true,
			                lineWidth: 1,
			                order: 1,
			                fillColor:  "'.$arr_color[$i].'"
			            },
			            color: "'.$arr_color[$i].'"
			        }';
		    $i++;
		}
		echo count($var)>0 ? "var data1 = [".implode(',',$var)."]; " : "";

	}
	?>
	
    
     
    $.plot($("#placeholder"), data1, {
        xaxis: {
            min: (new Date(2011, 11, 01)).getTime(),
            max: (new Date(2012, 03, 01)).getTime(),
            mode: "time",
            timeformat: "%b",
            tickSize: [1, "month"],
            monthNames: ["Nhóm 1 : Mai", "Nhóm 2 : Khoa", "Nhóm 3 : Giang", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", ""],
            tickLength: 0,
            axisLabel: '',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 13,
            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            axisLabelPadding: 5
        },
        yaxis: {
            axisLabel: '',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 13,
            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            axisLabelPadding: 5
        },
        grid: {
            hoverable: true,
            clickable: false,
            borderWidth: 1
        },
        legend: {
            labelBoxBorderColor: "none",
            position: "right"
        },
        series: {
            shadowSize: 1
        }
    });
 
    function showTooltip(x, y, contents, z) {
        $('<div id="flot-tooltip">' + contents + '</div>').css({
            top: y - 20,
            left: x - 90,
            'border-color': z,
        }).appendTo("body").show();
    }
     
    function getMonthName(newTimestamp) {
        var d = new Date(newTimestamp);
         
        var numericMonth = d.getMonth();
        var monthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
         
        var alphaMonth = monthArray[numericMonth];
 
        return alphaMonth;
    }
 
    $("#placeholder").bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.datapoint) {
                previousPoint = item.datapoint;
                $("#flot-tooltip").remove();
 
                var originalPoint;
                 
                if (item.datapoint[0] == item.series.data[0][3]) {
                    originalPoint = item.series.data[0][0];
                } else if (item.datapoint[0] == item.series.data[1][3]){
                    originalPoint = item.series.data[1][0];
                } else if (item.datapoint[0] == item.series.data[2][3]){
                    originalPoint = item.series.data[2][0];
                } else if (item.datapoint[0] == item.series.data[3][3]){
                    originalPoint = item.series.data[3][0];
                } else if (item.datapoint[0] == item.series.data[4][3]){
                    originalPoint = item.series.data[4][0];
                }
                 
                var x = getMonthName(originalPoint);
                y = item.datapoint[1];
                z = item.series.color;
 
                showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b><br /> " + y,
                    z);
            }
        } else {
            $("#flot-tooltip").remove();
            previousPoint = null;
        }
    });

	
</script>
<!-- /flot -->

<!-- ajax compare -->
<script>
	var baselink = $("#baselink_report").val();
	$("#vs_sosanh").change(function(){
		var data = $(this).val();
		var kpi = $("#value_sosanh").val();
		var status = $("#orderstatus").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+data+"&KPI="+kpi+"&Orderstatus="+status,
            success: function(result){
                window.location=result;
            }
        });
	});

	$("#orderstatus").change(function(){
		var data = $(this).val();
		window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/report_staff?status=' ?>"+data;
	});

	$("#value_sosanh").change(function(){
		var data = $(this).val();
		var status = $("#orderstatus").val();
		var targets = $("#vs_sosanh").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+targets+"&KPI="+data+"&Orderstatus="+status,
            success: function(result){
                window.location=result;
            }
        });
	});

	function changeordertype(ob){
		var dataordertype = $(ob).val();
		window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/report_staff?ordertype=' ?>"+dataordertype;
	}
</script>