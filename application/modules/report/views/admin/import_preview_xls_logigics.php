<div class="containner">
		<button class="btn btn-success" onclick="savedata()" style="color:#FFF;padding:5px 10px;float: right;margin-bottom: 15px;"><i style='margin-right:5px;color:#FFF' class="fa fa-refresh"></i> Đồng bộ</button>
		<div class="table_data">
		<table>
			<tr>
				<td style='padding:3px 5px;border:1px solid #E1e1e1;background:#EEE'>STT</td>
				<td style='padding:3px 5px;border:1px solid #E1e1e1;background:#EEE'>Mã Đơn hàng</td>
				<td style='padding:3px 5px;border:1px solid #E1e1e1;background:#EEE'>Trạng thái đồng bộ</td>
				<td style='padding:3px 5px;border:1px solid #E1e1e1;background:#EEE'>Ghi chú trạng thái</td>
				<td style='padding:3px 5px;border:1px solid #E1e1e1;background:#EEE'>Trạng thái đồng bộ</td>
			</tr>
		<?php 
		$i=1;
		$arr_status = array(0=>"Thành công",1=>"Hủy");
		foreach($data as $row){
			$status = isset($arr_status[$row['Status']]) ? $arr_status[$row['Status']] : '<span style="color:#F00">Không xác định</span>' ;
			echo "<tr>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'>".$i."</td>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'><input type='hidden' class='MaDH$i' value='".$row['MaDH']."' />".$row['MaDH']."</td>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'><input type='hidden' class='Trangthai$i' value='".$row['Status']."' />".$status."</td>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'><input type='hidden' class='Ghichu$i' value='".$row['Ghichu']."' />".$row['Ghichu']."</td>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'><span class='status$i'>Waiting...<span></td>
			</tr>";
			$i++;
		}
		?>
		</table>
		</div>
</div>
<style>
	table{border-collapse: collapse;width:100%;}
	table tr td input[type="text"]{width:100%; padding:3px 5px;border:none;}
</style>
<script>
	
	function savedata(){
		send_post(1);
	}

	function send_post(num){
        var MaDH = $(".MaDH"+num).val();
        var Trangthai = $(".Trangthai"+num).val();
        var Ghichu = $(".Ghichu"+num).val();

        if(num==<?php echo count($data)+1 ?>){
            return false;
        }
        $.ajax({
            url: "<?php echo base_url().ADMINPATH.'/report/import/sync_status' ?>",
            dataType: "html",
            type: "POST",
            data: "MaDH="+MaDH+"&Status="+Trangthai+"&Ghichu="+Ghichu,
            beforeSend: function(){
                $(".status"+num).html('<i class="fa fa-refresh fa-spin"></i> Syncing...');
            },
            success: function(result){
                if(result=='OK'){
                	$(".status"+num).html('<span style="color:#090">Sync successfull !</span>');
                }else{
                	$(".status"+num).html('<span style="color:#F00">Sync false !'+result+'</span>');
                }
            }
        }).always(function(){
            send_post(++num);
        });
    }
</script>
