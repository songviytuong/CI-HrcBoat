<div class="containner">
		<div class="import_orderlist">
			<div class="block2">
	    		<div class="block_2_1">
	    			<a class="btn btn-primary" href="<?php echo $base_link.'add_sales'; ?>"><i class="fa fa-plus"></i> KHÁCH HÀNG</a>
	    			<form action="<?php echo $base_link ?>setsessionsearch" method="post">
						<input type="text" name="Search" placeholder="Search..." style="height: 36px;padding: 5px 10px;border: 1px solid #ccc;float:left" />
						<button type="submit" class="btn btn-default" style="height: 36px;border-radius: 0px 5px 5px 0px;padding: 5px 10px;background: #EEE;border-left: none;"><i class="fa fa-search"></i> Fillter</button>
	                    <a href="<?php echo $base_link.'clearfilter' ?>" style='margin-left: 10px;text-decoration: underline;'>Clear Filter</a>
					</form>
	    		</div>
	    		<div class="block_2_2" style="text-align:right;line-height: 35px;">
	    			<select style="height:32px;width:128px;border:1px solid #c1c1c1;margin-right:10px;margin-left:5px" onchange="setsearch(this)">
	    				<?php 
	    				$TypeChanel = $this->session->userdata("report_filter_customer_TypeChanel");
	    				?>
	    				<option value='0' <?php echo $TypeChanel==0 || $TypeChanel=='' ? "selected='selected'" : '' ; ?>>-- Loại kênh GT --</option>
	    				<option value='1' <?php echo $TypeChanel==1 ? "selected='selected'" : '' ; ?>>-- Loại kênh MT --</option>
	    				<option value='4' <?php echo $TypeChanel==4 ? "selected='selected'" : '' ; ?>>-- Loại kênh TD --</option>
	    				<option value='5' <?php echo $TypeChanel==5 ? "selected='selected'" : '' ; ?>>-- Loại kênh NB --</option>
	    			</select>
	    		</div>
	    		<div class="clear"></div>
	    	</div>
	    	<div class="block3 table_data">
				<table id="table_data">
					<tr>
						<th>STT</th>
						<th>Kênh</th>
						<th>Tên đối tác</th>
						<th>Điện thoại</th>
						<th>Địa chỉ giao hàng</th>
					</tr>
					<?php 
					$arr_chanel = array(0=>'Online',1=>'GT',2=>'MT',4=>'TD',5=>'NB');
					$arr_type = array(1=>'Đại lý',0=>'Nhà phân phối');
					if(count($data)>0){
						$i=$start;
						foreach($data as $row){
							$chanel = isset($arr_chanel[$row->Type]) ? $arr_chanel[$row->Type] : '--' ;
							$type = isset($arr_type[$row->SystemID]) ? $arr_type[$row->SystemID] : '--' ;
							$i++;
							echo "<tr>";
							echo "<td style='width:30px;text-align:center;background:#F7F7F7'><a href='{$base_link}edit_sales/$row->ID'>$i</a></td>";
							echo "<td style='width:80px'><a href='{$base_link}edit_sales/$row->ID'>$chanel</a></td>";
							echo "<td style='width:auto'><a href='{$base_link}edit_sales/$row->ID'>$row->Name</a></td>";
							echo "<td style='width:190px'><a href='{$base_link}edit_sales/$row->ID'>$row->Phone1</a></td>";
							echo "<td style='width:100px'><a href='{$base_link}edit_sales/$row->ID'>$row->AddressOrder</a></td>";
							echo "</tr>";
						}
					}else{
						$keywords = $keywords!='' ? '"<b>'.$keywords.'</b>"' : $keywords ;
						echo "<tr><td colspan='5'>Không tìm thấy dữ liệu $keywords.</td></tr>";
					}
					?>
				</table>
				<?php if(count($data)>0) echo $nav; ?>
			</div>
		</div>
</div>
<style>
	.body_content .containner table tr td{white-space: nowrap;text-overflow: ellipsis;overflow: hidden;}
	.body_content .containner table tr td:nth-child(5){max-width: 300px}
</style>
<script>
	function setsearch(ob){
		var data = $(ob).val();
		window.location = '<?php echo base_url().ADMINPATH."/report/import_customer/setsessionsearch/?TypeChanel=" ?>'+data;
	}
</script>
