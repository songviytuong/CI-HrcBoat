<?php 
	if($data->Status==5){
?>
<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>
<?php 
	}
$Export = $this->db->query("select a.*,c.Name from ttp_report_export_warehouse a,ttp_report_order b,ttp_report_customer c where a.OrderID=b.ID and b.CustomerID=c.ID and a.ID=$data->ExportID")->row();
if(!$Export){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này không tìm thấy dữ liệu chi tiết . Vui lòng kiểm tra lại !</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div><div class="containner"></div>';
	return;
}
?>
<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type='hidden' name='ID' value='<?php echo $data->ID ?>' />
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÔNG TIN PHIẾU NHẬP KHO <b style="font-size:inherit;color:#D94A38"><?php echo $data->MaNK ?></b> </h1>
				</div>
				<div class="block2">
					<?php echo $data->Status==2 || $data->Status==4 ? '<a href="'.$base_link.'print_import_bill/'.$data->ID.'" class="btn btn-primary"><i class="fa fa-print"></i> IN PHIẾU NK</a>' : ''; ?>
				</div>
			</div>
			<div class="box_content_warehouse_import">
				<div class="block1">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-4">
								<label class="control-label col-xs-5">Nhập tại kho</label>
								<div class="col-xs-7">
									<select name='KhoID' class="form-control">
										<?php 
										$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
										if(count($warehouse)>0){
											foreach($warehouse as $row){
												if($this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ){
													if($data->KhoID==$row->ID){
														echo "<option value='$row->ID' selected='selected'>$row->MaKho</option>";
													}
												}else{
													$selected = $data->KhoID==$row->ID ? "selected='selected'" : '' ;
													echo "<option value='$row->ID' $selected>$row->MaKho</option>";	
												}
												
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-xs-4">
								<label class="control-label col-xs-5">Ngày chứng từ</label>
								<div class="col-xs-7">
									<input type='text' class="form-control" name='NgayNK' <?php echo $this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ? "" : "id='NgayNK'" ; ?> value='<?php echo date('Y-m-d') ?>' value='<?php echo date('Y-m-d',strtotime($data->NgayNK)) ?>' readonly="true" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12">
								<label class="control-label col-xs-2">Hình thức nhập</label>
								<div class="col-xs-7" style="margin-left: -60px;line-height: 22px;">
									<input type='radio' name="Type" value="0" disabled="true" class='pull-left' style='margin-left:30px;margin-right:8px' />
									<span class='pull-left'>Mua hàng</span>
									<input type='radio' name="Type" value="1" checked="checked" class='pull-left' style='margin-left:30px;margin-right:8px;margin-left:20px' />
									<span class='pull-left'>Hàng trả về</span>
									<input type='radio' name="Type" value="2" disabled="true" class='pull-left' style='margin-left:30px;margin-right:8px;margin-left:20px' />
									<span class='pull-left'>Thành phẩm / Trả kho nội bộ</span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12">
								<label class="control-label col-xs-2">Diễn giải</label>
								<div class="col-xs-10" style="margin-left: -32px;line-height: 22px;">
									<input type='text' name="Note" value='<?php echo $data->Note ?>' class="form-control" required />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-4">
								<label class="control-label col-xs-5">Tìm phiếu XK</label>
								<div class="col-xs-7">
									<input type='text' id='input_pxk' class="form-control" />
									<input type='hidden' value='<?php echo $Export->ID ?>' name='ExportID' id='ExportID' />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="col-xs-5"><span style="padding:0px 5px"></span><a class='btn btn-default' onclick='find_pxk(this)'><i class="fa fa-search" style="padding:0px"></i> Tìm phiếu</a></div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-xs-4">
								<div class="form-group">
									<label class="col-xs-5 control-label">Mã phiếu XK</label>
									<div class="col-xs-7">
										<input type='text' id="MaPXK" value='<?php echo $Export->MaXK ?>' class="form-control" readonly="true" />
									</div>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="form-group">
								<label class="col-xs-5 control-label">Tên người nhận</label>
								<div class="col-xs-7">
									<input type='text' id="ReciverName" readonly="true" value='<?php echo $Export->Name ?>' class='form-control' />
								</div>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="form-group">
								<label class="col-xs-5 control-label">Ngày xuất kho</label>
								<div class="col-xs-7">
									<input type='text' id="DayExport" class="form-control" readonly="true" value='<?php echo date('d/m/Y',strtotime($Export->Ngayxuatkho)) ?>' />
								</div>
								</div>
							</div>
						
					</div>
				</div>
				<!-- end block1 -->
		    	<div class="block2"></div>
		    	<div class="clear"></div>
		    	<div class="table_donhang table_data">
		    		<div id="inner_table_pxk">
			    		<table>
			    			<tr>
								<th>Mã SP</th>
							 	<th>Tên SP</th>
							 	<th>Đơn vị tính</th>
							 	<th>Lô</th>
							 	<th>Số lượng</th>
							 	<th>Đơn giá</th>
							 	<th>Giá trị CK</th>
							 	<th>Giá sau CK</th>
							 	<th>Thành tiền</th>
							 	<th>Kiểm tra</th>
							 	<th style='text-align:center'><i class='fa fa-level-down'></i></th>
							</tr>
			    			<?php 
			    			$arr_history = array();
			    			$details_export = $this->db->query("select c.* from ttp_report_order a,ttp_report_export_warehouse b,ttp_report_orderdetails c where a.ID=b.OrderID and a.ID=c.OrderID and b.ID=$data->ExportID")->result();
			    			if(count($details_export)>0){
			    				foreach($details_export as $row){
			    					$arr_history[$row->ProductsID.'_'.$row->ShipmentID] = $row;
			    				}
			    			}
			    			$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi,b.VariantType from ttp_report_inventory_import_details a,ttp_report_products b where a.ImportID=$data->ID and a.ProductsID=b.ID")->result();
			    			if(count($details)>0){
			    				foreach($details as $row){
			    					$Shipment = $this->db->query("select * from ttp_report_shipment where ID=$row->ShipmentID")->row();
			    					$ShipmentCode = $Shipment ? $Shipment->ShipmentCode : '--' ;
			    					$IDDetails = isset($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->ID) ? $arr_history[$row->ProductsID.'_'.$row->ShipmentID]->ID : 0;
			    					$ShipmentCode = $row->VariantType==2 && $this->user->UserType==2 ? "<a onclick='viewcombo(this,$IDDetails)' style='white-space:nowrap'><i class='fa fa-table' aria-hidden='true'></i> Xem combo</a>" : $ShipmentCode ;
			    					$giachietkhau = isset($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->PriceDown) && isset($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->Price) ? $arr_history[$row->ProductsID.'_'.$row->ShipmentID]->PriceDown+$arr_history[$row->ProductsID.'_'.$row->ShipmentID]->Price : 0 ;
			    					echo '<tr>
			    							<td>'.$row->MaSP.'<input type="hidden" name="ProductsID[]" value="'.$row->ProductsID.'"></td>
			    							<td>'.$row->Title.'</td>
			    							<td>'.$row->Donvi.'</td>';
			    					echo "<td><input type='hidden' name='ShipmentID[]' value='$row->ShipmentID' /> $ShipmentCode</td>";
			    					echo '<td style="width:80px"><input type="text" name="Amount[]" readonly="true" class="Amount_input" value="'.$row->Amount.'" onchange="changerow(this)" style="border:none" required ></td>';
			    					echo "<td>".number_format($giachietkhau)."</td>";
			    					echo isset($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->PriceDown) ? "<td>".number_format($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->PriceDown)."</td>" : "<td>--</td>";
			    					echo isset($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->Price) ? "<td>".number_format($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->Price)."</td>" : "<td>--</td>";
			    					echo isset($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->Total) ? "<td>".number_format($arr_history[$row->ProductsID.'_'.$row->ShipmentID]->Total)."</td>" : "<td>--</td>";
			    					echo '<td><i class="fa fa-check-circle unchecked"></i> <input type="hidden" name="PriceCurrency[]" value="'.$row->PriceCurrency.'" class="PriceCurrency_input" onchange="changerow(this)" required=""></td>
			    							<td style="text-align:center"><a style="margin:0px;padding:0px;float:none;"><i class="fa fa-times" style="color: #555;"></i></a>
			    							<input type="hidden" name="TotalVND[]" style="display:none" class="Total_input" value="'.$row->TotalVND.'"><input name="Currency[]" class="Currency_input" style="display:none" value="'.$row->Currency.'" /> <input type="text" name="ValueCurrency[]" value="'.$row->ValueCurrency.'" class="ValueCurrency_input" onchange="changerow(this)" required="" style="display:none"> <input type="text" name="TotalCurrency[]" value="'.$row->TotalCurrency.'" class="TotalCurrency_input" onchange="changerow(this)" required="" style="display:none"> </td>
			    						</tr>';
			    				}
			    				echo "<tr>
				    					<td colspan='9'><input type='text' class='form-control BarcodeList' name='BarcodeList' placeholder='Enter barcode of products ...' /></td>
				    					<td colspan='2'><a class='btn btn-danger BarcodeButton'><i class='fa fa-refresh'></i> Check</a></td>
				    				</tr>";
			    			}
			    			?>
			    		</table>
			    	</div>
		    		<div class="history_status">
			    		<h3 style="margin-bottom:15px">Lịch sử trạng thái yêu cầu nhập kho</h3>
			    		<?php 
			    		$history = $this->db->query("select a.*,b.UserName from ttp_report_inventory_import_history a,ttp_user b where a.UserID=b.ID and a.ImportID=$data->ID")->result();
			    		if(count($history)>0){
			    			$arr_status = array(
			                    0=>'Phiếu chờ nhập kho',
			                    1=>'',
			                    2=>'Yêu cầu chờ kế toán xử lý',
			                    3=>'',
			                    4=>'Hàng đã nhập kho',
			                    5=>'Hủy phiếu chờ - đơn hàng thành công',
			                );
			    			echo "<table><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
			    			foreach($history as $row){
			    				echo "<tr>";
			    				echo isset($arr_status[$row->Status]) ? "<td class='width150'>".$arr_status[$row->Status]."</td>" : "<td>--</td>" ;
			    				echo "<td class='width150'>".date('d/m/Y H:i:s',strtotime($row->Created))."</td>";
			    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
			    				echo "<td>$row->UserName</td>";
			    				echo "</tr>";
			    			}
			    			echo "</table>";
			    		}
			    		?>
			    	</div>
			    	<div class='row' style="margin:5px 0px"></div>
		    		<div class='row'>
	    				<div class="col-xs-12">
	    					<div class="form-group">
			    				<label class="col-xs-2 control-label">Tình trạng chứng từ</label>
			    				<div class="col-xs-3">
			    					<select name="Status" class="form-control">
			    						<?php 
			    						if($this->user->UserType==6){
			    							echo $data->Status==0 ? "<option value='0' selected='selected'>Phiếu chờ nhập kho</option>" : "<option value='0'>Phiếu chờ nhập kho</option>" ;
			    						}elseif($this->user->UserType==2){
			    							echo $data->Status==2 ? "<option value='2' selected='selected'>	Chuyển sang kế toán xử lý</option>" : "<option value='2'> Chuyển sang kế toán xử lý</option>" ;
			    							echo $data->Status==5 ? "<option value='5' selected='selected'> Hủy phiếu chờ -> Đơn hàng thành công</option>" : "<option value='5'> Hủy phiếu chờ -> Đơn hàng thành công</option>" ;
			    						}elseif($this->user->UserType==3){
			    							echo $data->Status==4 ? "<option value='4'selected='selected'>Hàng đã nhập kho thành công</option>" : "<option value='4'>Hàng đã nhập kho thành công</option>";
			    						}
			    						?>
			    					</select>
			    				</div>
		    				</div>
		    			</div>
		    			<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
		    		</div>
		    		<div class="row">
	    				<div class="col-xs-12">
	    					<div class="form-group">
			    				<label class="col-xs-2 control-label">Ghi chú tình trạng</label>
			    				<div class="col-xs-9"><textarea name="Ghichu" class="form-control"><?php echo $data->Ghichu ?></textarea></div>
		    				</div>
	    				</div>
		    		</div>
		    		<div class="row">
	    				<div class="col-xs-12">
	    					<div class="form-group">
			    				<label class="col-xs-2 control-label"></label>
			    				<div class="col-xs-9">
			    					<?php 
					    			if($this->user->UserType!=3){
					    				echo "<button class='btn btn-primary btn_default'><i class='fa fa-refresh'></i> Cập nhật</button>";
					    			}
					    			?>
					    			<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
			    				</div>
			    			</div>
		    			</div>
		    		</div>
		    	</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	$(document).ready(function () {
        $('#NgayNK,.DateProduction').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	var link = $("#baselink").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	function changestatus(){
		$("#IsChangeOrder").val("1");
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function deleterow(ob){
		$(ob).parent('td').parent('tr').remove();
		changestatus();
		recal();
	}

	function recal(){
		var tongcong = 0;
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			amount = parseInt(parent.find('input.Amount_input').val());
			valueCurrency = parent.find('input.ValueCurrency_input').val();
			pricecurrentcy = parent.find('input.PriceCurrency_input').val();
			total = (amount*pricecurrentcy)*valueCurrency;
			tongcong = tongcong+total;
			parent.find('input.Total_input').val(total);
			parent.find('.TotalVND').html(total.format('a',3));
		});
		$(".tongcong").html(tongcong.format('a',3));
		changestatus();
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		var cls = $(ob).attr('class');
		Amount_input = parseInt(parent.find('td input.Amount_input').val());
		PriceCurrency_input = parent.find('input.PriceCurrency_input').val();
		ValueCurrency = parent.find('input.ValueCurrency_input').val();
		value = $(ob).val();
		if(cls=="PriceCurrency_input"){
			TotalCurrency = value*Amount_input;
			parent.find('input.TotalCurrency_input').val(TotalCurrency);
		}
		if(cls=="TotalCurrency_input"){
			PriceCurrency = value/Amount_input;
			parent.find('input.PriceCurrency_input').val(PriceCurrency);
		}
		if(cls=="Amount_input"){
			TotalCurrency = value*PriceCurrency_input;
			parent.find('input.TotalCurrency_input').val(TotalCurrency);
		}
		recal();
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	}

	function find_pxk(ob){
		var mapxk = $("#input_pxk").val();
		if(mapxk!=''){
			$.ajax({
	            url: link+"find_PXK",
	            dataType: "html",
	            type: "POST",
	            data: "Getdata=0&MaPXK="+mapxk,
	            success: function(result){
	            	data = result.split("||",6);
	            	var status = data[0];
	            	if(status==1){
	            		alert(data[1]);
	            	}else{
	            		changestatus();
	            		$("#MaPXK").val(data[1]);
	            		$("#ReciverName").val(data[2]);
	            		$("#DayExport").val(data[3]);
	            		$("#ExportID").val(data[4]);
	            		$("#inner_table_pxk").html(data[5]);
	            	}
	            }
	        });
        }else{
        	$("#input_pxk").focus();
        }
	}

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	function viewcombo(ob,ID){
		if(ID>0){
			enablescrollsetup();
			$(".over_lay .box_inner .block2_inner").html("");
			$.ajax({
	        	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_combo_details' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "DetailsID="+ID,
	            success: function(result){
	            	if(result!="FALSE"){
	            		$(".over_lay .box_inner").css({'margin-top':'20px'});
				    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách chi tiết COMBO");
				    	$(".over_lay .box_inner .block2_inner").html(result);
				    	$(".over_lay").removeClass('in');
				    	$(".over_lay").fadeIn('fast');
				    	$(".over_lay").addClass('in');
	                }else{
	                	alert("Có lỗi xảy ra trong quá trình truyền tải dữ liệu !. Vui lòng kiểm tra lại đường truyền .");
	                }
	            }
	        });
        }
	}

</script>