<?php 
if($this->user->UserType==6 && ($data->Status!=1 && $data->Status!=3)){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>' ;
}elseif($this->user->UserType==2 && $data->Status!=0){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>' ;
}elseif($this->user->UserType==3 && $data->Status!=2){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>' ;
}elseif($data->Status==4||$data->Status==5){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>' ;
}
?>
<div class="containner">
	<div class="manager">
		<?php 
		$destinationform = "";
		$destinationform = $this->user->UserType==6 ? $base_link.'update' : $destinationform ;
		$destinationform = $this->user->UserType==2 || $this->user->UserType==8 ? $base_link.'update_from_warehouseuser' : $destinationform ;
		$destinationform = $this->user->UserType==3 || $this->user->UserType==7 ? $base_link.'update_from_kt' : $destinationform ;
		?>
		<form action="<?php echo $destinationform ?>" method="POST">
			<input type='hidden' name='ID' value='<?php echo $data->ID ?>' />
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÔNG TIN PHIẾU NHẬP KHO <b style="font-size:inherit;color:#D94A38"><?php echo $data->MaNK ?></b> </h1>
				</div>
				<div class="block2">
					<?php echo $data->Status==4 ? '<a href="'.$base_link.'print_import_bill/'.$data->ID.'" class="btn btn-primary"><i class="fa fa-print"></i> IN PHIẾU NK</a>' : ''; ?>
				</div>
			</div>
			<div class="row" style="margin:5px 0px"></div>
			<div class="box_content_warehouse_import">
				<div class="row">
					<div class="form-group">
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Nhập tại kho</label>
							<div class="col-xs-7">
								<select name='KhoID' class="form-control">
									<?php 
									$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
									if(count($warehouse)>0){
										foreach($warehouse as $row){
											if($this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ){
												if($data->KhoID==$row->ID){
													echo "<option value='$row->ID' selected='selected'>$row->MaKho</option>";
												}
											}else{
												$selected = $data->KhoID==$row->ID ? "selected='selected'" : '' ;
												echo "<option value='$row->ID' $selected>$row->MaKho</option>";	
											}
											
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Ngày chứng từ</label>
							<div class="col-xs-7">
								<input type='text' class="form-control" name='NgayNK' <?php echo $this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ? "" : "id='NgayNK'" ; ?> value='<?php echo date('Y-m-d') ?>' value='<?php echo date('Y-m-d',strtotime($data->NgayNK)) ?>' readonly="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12">
							<label class="control-label col-xs-2">Hình thức nhập</label>
							<div class="col-xs-7" style="margin-left: -60px;line-height: 22px;">
								<input type='radio' name="Type" value="0" checked="checked" class='pull-left' style='margin-left:30px;margin-right:8px' />
								<span class='pull-left'>Mua hàng</span>
								<input type='radio' name="Type" value="1" disabled="true" class='pull-left' style='margin-left:30px;margin-right:8px;margin-left:20px' />
								<span class='pull-left'>Hàng trả về</span>
								<input type='radio' name="Type" value="2" disabled="true" class='pull-left' style='margin-left:30px;margin-right:8px;margin-left:20px' />
								<span class='pull-left'>Thành phẩm / Trả kho nội bộ</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Theo PO số</label>
							<div class="col-xs-7">
								<?php 
								$PO = $this->db->query("select POCode from ttp_report_perchaseorder where ID=$data->POID")->row();
								echo $PO ? "<input type='text' class='form-control' name='POID' value='".$PO->POCode."' />" : "" ;
								?>
							</div>
						</div>
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Tên nhà cung cấp</label>
							<div class="col-xs-7 input-group">
								<select name="ProductionID" id="ProductionID" class="form-control">
									<?php 
									$production = $this->db->query("select ID,Title from ttp_report_production where Published=1")->result();
									if(count($production)>0){
										foreach($production as $row){
											if($this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ){
												if($data->ProductionID==$row->ID){
													echo "<option value='$row->ID' selected='selected'>$row->Title</option>";
												}
											}else{
												$selected = $data->ProductionID==$row->ID ? "selected='selected'" : '' ;
												echo "<option value='$row->ID' $selected>$row->Title</option>";
											}
										}
									}
									?>
								</select>
								<?php 
								if($this->user->UserType!=2 && $this->user->UserType!=8 && $this->user->UserType!=3 && $this->user->UserType!=7 ){
								?>
								<span class="input-group-btn"><a class='btn btn_default' title="Thêm nhà cung cấp mới" onclick="add_production(this)"><i class="fa fa-plus"></i></a></span>
								<?php 
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12">
							<label class="control-label col-xs-2">Diễn giải</label>
							<div class="col-xs-10">
								<input type='text' class="form-control" name="Note" value='<?php echo $data->Note ?>' style="margin-left:-30px" required <?php echo $this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ? "readonly='true'" : "" ; ?> />
							</div>
						</div>
					</div>
				</div>
				<!-- end block1 -->
		    	<!--<div class="block2">
		    		<div class="row">
					<div class="col-xs-12">
		    			<div class="form-group">
			    			<div class="col-xs-1">
			    				<a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Sản phẩm</a>
			    			</div>
			    			<div class="col-xs-2">
			    			<ul>
								<li><a id='show_thaotac' class="btn btn-default">Thao tác <b class="caret"></b></a>
				    				<ul>
					    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
				    				</ul>
								</li>
							</ul>
							</div>
						</div>
					</div>
					</div>
		    	</div>-->
		    	<div class="clear"></div>
		    	<div class="table_donhang table_data">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã SP</th>
		    				<th>Tên sản phẩm</th>
		    				<th>ĐVT</th>
		    				<th>Lô</th>
		    				<th>Số lượng PO</th>
		    				<th>Số lượng <br>phiếu yêu cầu</th>
		    				<th>Số lượng <br>thực nhập</th>
		    				<th>Số lượng còn của <br>phiếu yêu cầu</th>
		    			</tr>
		    			<?php 
		    			$Quantity_po = array();
				        if($data->POID>0){
				            $detailspo = $this->db->query("select ProductsID,Amount,ShipmentID from ttp_report_perchaseorder_details where POID=$data->POID")->result();
				            if(count($detailspo)>0){
				                foreach($detailspo as $row){
				                    $bonus[] = $row->ProductsID;
				                    $Quantity_po[$row->ProductsID][$row->ShipmentID] = $row->Amount;
				                }
				            }
				        }

		    			$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi from ttp_report_inventory_import_details a,ttp_report_products b where a.ImportID=$data->ID and a.ProductsID=b.ID")->result();
		    			$arrproducts = array();
		    			if(count($details)>0){
		    				foreach($details as $row){
		    					echo '<tr>
		    							<td><input type="checkbox" class="selected_products" data-id="'.$row->ProductsID.'"><input type="hidden" name="ProductsID[]" value="'.$row->ProductsID.'"></td>
		    							<td>'.$row->MaSP.'</td>
		    							<td>'.$row->Title.'</td>
		    							<td>'.$row->Donvi.'</td>';
	    						$Shipment = $this->db->query("select * from ttp_report_shipment where ProductsID=$row->ProductsID")->result();
		    					echo "<td><input type='hidden' name='ShipmentOld[]' value='$row->ShipmentID' />
		    					<select name='ShipmentID[]' class='select_shipment' data='$row->ProductsID' onchange='add_shipment(this,$row->ProductsID)'>";
		    					if($this->user->UserType==8 || $this->user->UserType==2 || $this->user->IsAdmin==1 || $this->user->UserType==6){
		    						echo "<option value='0'>-- Chọn lô --</option>";
		    					}
		    					if(count($Shipment)>0){
		    						foreach($Shipment as $item){
		    							if($this->user->UserType==8 || $this->user->UserType==2 || $this->user->IsAdmin==1 || $this->user->UserType==6){
		    								$selected = $item->ID==$row->ShipmentID ? "selected='selected'" : '' ;
		    								echo "<option value='$item->ID' $selected>Lô $item->ShipmentCode</option>";
		    							}else{
			    							$selected = $item->ID==$row->ShipmentID ? "selected='selected'" : '' ;
			    							echo "<option value='$item->ID' $selected>Lô $item->ShipmentCode</option>";
		    							}
		    						}
		    					}
		    					if($this->user->UserType==8 || $this->user->UserType==2 || $this->user->IsAdmin==1){
		    						echo "<option value='add'>-- Tạo mới --</option></select></td>";
		    					}
		    					echo isset($Quantity_po[$row->ProductsID][$row->ShipmentID]) ? '<td><span class="quantitypo">'.number_format($Quantity_po[$row->ProductsID][$row->ShipmentID],2).'</span></td>' : '<td><span class="quantitypo">0</span></td>';
		    					echo "<td><span class='request' data-number='$row->Request'>".number_format($row->Request,2)."</span></td>";
		    					if($data->Status==0){
			    					echo '<td><input type="text" name="Amount[]" class="Amount_input" value="'.$row->Request.'" onchange="changerow(this)" required ></td>';
			    				}else{
			    					echo '<td><input type="text" name="Amount[]" class="Amount_input" value="'.$row->Amount.'" onchange="changerow(this)" required ></td>';
			    				}
		    					echo '<td><span class="remain">0</span></td>
		    						</tr>';
		    					$arrproducts[] = '"data'.$row->ProductsID.'"';
		    				}
		    			}
		    			?>
		    			<tr>
		    				<td colspan="7">TỔNG CỘNG</td>
		    				<td><span class='tongcong'><?php echo number_format($data->TotalAmount,2) ?></span></td>
		    				<td></td>
		    			</tr>
		    		</table>
		    		<div class="history_status">
			    		<h3 style="margin-bottom:10px">Lịch sử trạng thái yêu cầu nhập kho</h3>
			    		<?php 
			    		$history = $this->db->query("select a.*,b.UserName from ttp_report_inventory_import_history a,ttp_user b where a.UserID=b.ID and a.ImportID=$data->ID")->result();
			    		if(count($history)>0){
			    			$arr_status = array(
			                    0=>'Yêu cầu nhập kho',
			                    1=>'Yêu cầu bị trả về từ kho',
			                    2=>'Yêu cầu chờ kế toán xử lý',
			                    3=>'Yêu cầu bị trả về từ kế toán',
			                    4=>'Hàng đã nhập kho thành công',
			                    5=>'Yêu cầu bị hủy'
			                );
			    			echo "<table><tr><th style='width:250px'>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
			    			foreach($history as $row){
			    				echo "<tr>";
			    				echo isset($arr_status[$row->Status]) ? "<td>".$arr_status[$row->Status]."</td>" : "<td>--</td>" ;
			    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Created))."</td>";
			    				echo isset($row->Note) ? "<td>".$row->Note."</td>" : "<td>--</td>";
			    				echo "<td>$row->UserName</td>";
			    				echo "</tr>";
			    			}
			    			echo "</table>";
			    		}
			    		?>
			    	</div>
		    	</div>
		    	<?php 
		    	if($data->Status!=4){
		    	?>
	    		<div class='row' style="margin:5px 0px"></div>
	    		<div class='row'>
	    			<div class="form-group">
		    			<div class="col-xs-6">
		    				<label class="control-label col-xs-4">Tình trạng chứng từ</label>
		    				<div class="col-xs-4">
		    					<select name="Status" class="form-control">
		    						<?php 
		    						if($this->user->UserType==6){
		    							echo $data->Status==0 ? "<option value='0' selected='selected'>Yêu cầu nhập kho</option>" : "<option value='0'>Yêu cầu nhập kho</option>" ;
		    						}elseif($this->user->UserType==2 || $this->user->UserType==8 || $this->user->UserType==3){
		    							echo $data->Status==4 ? "<option value='4' selected='selected'>	Nhập kho thành công</option>" : "<option value='4'> Nhập kho thành công</option>" ;
		    						}elseif($this->user->IsAdmin==1){
		    							echo $data->Status==0 ? "<option value='0' selected='selected'>Yêu cầu nhập kho</option>" : "<option value='0'>Yêu cầu nhập kho</option>" ;
		    							echo $data->Status==2 ? "<option value='2' selected='selected'>	Chuyển sang kế toán xử lý</option>" : "<option value='2'> Chuyển sang kế toán xử lý</option>" ;
		    							echo $data->Status==1 ? "<option value='1' selected='selected'> Trả yêu cầu về CM</option>" : "<option value='1'> Trả yêu cầu về CM</option>" ;
		    							echo $data->Status==4 ? "<option value='4'selected='selected'>Hàng đã nhập kho thành công</option>" : "<option value='4'>Hàng đã nhập kho thành công</option>";
		    						}
		    						?>
		    					</select>
		    				</div>
		    			</div>
	    			</div>
	    			<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
	    		</div>
	    		<div class="row">
	    			<div class="form-group">
	    				<div class="col-xs-12">
		    				<label class="control-label col-xs-2">Ghi chú tình trạng</label>
		    				<div class="col-xs-9"><textarea name="Ghichu" class="form-control"></textarea></div>
		    			</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="form-group">
	    				<div class="col-xs-12">
		    				<label class="control-label col-xs-2"></label>
		    				<div class="col-xs-9">
		    					<?php
				    				echo $data->Status!=0 ? "" : "<button class='btn btn-primary btn_default' type='submit'><i class='fa fa-upload'></i> Nhập kho</button>";
									echo $data->Status!=0 ? "" : "<a href='".base_url().ADMINPATH."/report/warehouse_inventory_import/drop_request_po/$data->ID' class='btn btn-danger pull-right' style='margin-top: 62px;margin-right: 120px;'><i class='fa fa-ban' aria-hidden='true'></i> Hủy phiếu yêu cầu</a>";
				    			?>
		    				</div>
		    			</div>
	    			</div>
	    		</div>
	    		<?php 
	    		}
	    		?>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	function stopRKey(evt) { 
		var evt = (evt) ? evt : ((event) ? event : null); 
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
		if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
	} 

	document.onkeypress = stopRKey;

	$(document).ready(function () {
        $('#NgayNK,.DateProduction').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	var link = $("#baselink").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	function changestatus(){
		$("#IsChangeOrder").val("1");
	}

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function add_production(ob){
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"500px"});
		$(".over_lay .box_inner .block1_inner h1").html("Thêm nhà cung cấp mới");
		$(".over_lay .box_inner .block2_inner").html("<div class='row'><p style='margin-bottom:5px'>Tên nhà cung cấp</p><div><input type='text' class='title_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Mã nhà cung cấp</p><div><input type='text' class='code_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Địa chỉ trụ sở</p><div><input type='text' class='address_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Số điện thoại</p><div><input type='text' class='phone_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Fax</p><div><input type='text' class='fax_production' style='margin-bottom:5px' /></div></div><div class='row'><button class='btn btn-primary' style='float:left;background:#1A82C3;color:#FFF;border: 1px solid #2477CA;' onclick='save_production(this)'>Lưu dữ liệu</button></div>");
		$(".over_lay").show();
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function save_production(ob){
		$(ob).addClass("saving");
		var title = $(".title_production").val();
		var code = $(".code_production").val();
		var address = $(".address_production").val();
		var phone = $(".phone_production").val();
		var fax = $(".fax_production").val();
		if(title!='' && code!=''){
			$.ajax({
            	url: link+"add_production",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "Title="+title+"&code="+code+"&address="+address+"&phone="+phone+"&fax="+fax,
	            success: function(result){
	            	$("#ProductionID").prepend(result);
	            	$(".over_lay").hide();
					disablescrollsetup();
					$(ob).removeClass("saving");
	            }
	        });
		}else{
			alert("Vui lòng điền đầy đủ thông tin !");
		}
	}

	function save_shipment(ob,id){
		$(ob).addClass("saving");
		var ShipmentCode = $(".ShipmentCode").val();
		var DateProduction = $(".DateProduction").val();
		var DateExpiration = $(".DateExpiration").val();
		if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
			alert("Vui lòng điền đầy đủ thông tin !");
			$(ob).removeClass("saving");
			return false;
		}
		$.ajax({
        	url: link+"save_shipment",
            dataType: "html",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
            	load_shipment(id);
            	$(".over_lay").hide();
				disablescrollsetup();
				$(ob).removeClass("saving");
            }
        });
	}

	$("#add_products_to_order").click(function(){
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"850px"});
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "POID=<?php echo $data->POID ?>",
            success: function(result){
                if(result!='false'){
        			$(".over_lay").show();
					$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                	disablescrollsetup();
                }
                $(this).removeClass('saving');
            }
        });
	});

	function input_search_products(ob){
		var data = $(ob).val();
		$.ajax({
        	url: link+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Title="+data,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
	}

	var celldata = [];
	var sttrow = <?php echo count($arrproducts) ?>+1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				var data_donvi = $(this).attr('data-donvi');
				if(jQuery.inArray( "data"+data_id, celldata )<0){
					var table = document.getElementById("table_data");
					var row = table.insertRow(sttrow);
					row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
					row.insertCell(1).innerHTML=data_code;
					row.insertCell(2).innerHTML=data_name;
					row.insertCell(3).innerHTML=data_donvi;
					row.insertCell(4).innerHTML="<select name='ShipmentID[]' class='select_shipment' data='"+data_id+"' onchange='add_shipment(this,"+data_id+")'></select>";
					row.insertCell(5).innerHTML="";
					row.insertCell(6).innerHTML="";
					row.insertCell(7).innerHTML="<input type='text' name='Amount[]' class='Amount_input' value='1' onchange='changerow(this)' required />";
					row.insertCell(8).innerHTML="";
					sttrow=sttrow+1;
					celldata.push("data"+data_id);
				}
				load_shipment(data_id);
			}
		});
		recal();
		$(".over_lay").hide();
		disablescrollsetup();
		changestatus();
	}

	function load_shipment(id){
		$(".select_shipment").each(function(){
			if(id==0){
				var data = $(this).attr('data');
				$(this).load(link+"load_shipment_by_products/"+data);
			}else{
				var data = $(this).attr('data');
				if(data==id){
					$(this).load(link+"load_shipment_by_products/"+data);
				}
			}
		});
	}

	function add_shipment(ob,id){
		var data = $(ob).val();
		if(data=="add"){
			enablescrollsetup();
			$(".over_lay .box_inner").css({"width":"500px"});
			$(".over_lay .box_inner .block1_inner h1").html("Tạo lô sản phẩm");
			$(".over_lay .box_inner .block2_inner").html("");
			$(".over_lay .box_inner .block2_inner").load(link+"box_add_shipment/"+id);
			$(".over_lay").show();
		}
		changestatus();
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				celldata.splice("data"+data_id, 1);
				sttrow = sttrow-1;
			}
		});
		recal();
		changestatus();
	});

	function recal(){
		var tongcong = 0;
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			amount = parseInt(parent.find('input.Amount_input').val());
			tongcong = tongcong+amount;
		});
		$(".tongcong").html(tongcong.format('a',3));
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		Quantitypo = parseInt(parent.find('span.quantitypo').html());
		Request = parseInt(parent.find('span.request').attr('data-number'));
		Amount = $(ob).val();
		if(Request<Amount){
			alert("Cảnh báo : Số bạn vừa nhập đã vượt quá số lượng yêu cầu !.");
			$(ob).val(Request);
			Amount = Request;
		}
		remain = Request-Amount;
		remain = remain.format('a',3);
		parent.find('span.remain').html(remain);
		recal();
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	$('form input').on('keypress', function(e) {
	    return e.which !== 13;
	});
</script>