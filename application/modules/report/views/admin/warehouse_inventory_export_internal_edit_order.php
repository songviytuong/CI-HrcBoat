<?php 
if(($this->user->UserType!=8 && $data->Status==2) || $data->Status==1 || $data->Status==0){
?>
<div class="warning_message" style="display:block"><span>Yêu cầu này đang trong trạng thái khóa . Mọi thay đổi của bạn hiện tại sẽ không được áp dụng .</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<?php 
}elseif($this->user->UserType==8 && $data->Status!=2){
?>
<div class="warning_message" style="display:block"><span>Yêu cầu này chỉ được phép thay đổi tình trạng đơn hàng . Mọi thay đổi khác của bạn hiện tại sẽ không được áp dụng .</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<?php
}
?>
<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>THÔNG TIN YÊU CẦU - <?php echo $data->MaDH ?></h1>
	    </div>
	    <div class="block2">
	    	<?php 
    		if(($this->user->UserType==2 || $this->user->UserType==7)){
    			if($data->Status==3 || $data->Status==0){
    				echo "<a href='".base_url().ADMINPATH."/report/warehouse_inventory_export/lapphieuxuatkho/$data->ID' class='phieuxuatkho btn btn-primary' style='margin-right:10px'><i class='fa fa-upload'></i> Phiếu xuất kho</a>";
    			}
    			if($data->Status==3){
    				echo "<a type='button' class='btn btn-update btn-danger submit' style='margin-right:10px' onclick='skip(0)'><i class='fa fa-external-link-square'></i> Xuất kho</a>";
    			}
    		}
    		if($this->user->UserType==8 && $data->Status==2){
    			echo "<a type='button' class='btn btn-update btn-primary submit' style='margin-right:10px' onclick='skip(3)'><i class='fa fa-check-square'></i> Duyệt</a>";
    			echo "<a type='button' class='btn btn-update btn-danger submit' onclick='skip(4)'><i class='fa fa-reply-all'></i> Trả về</a>";
    		}
    		if($this->user->UserType==2 && $data->Status==4){
    			echo "<a type='button' class='btn btn-update btn-primary submit' id='buttonsubmit' style='margin-right:10px' onclick='skip(2)'><i class='fa fa-check-square'></i> Lưu & Duyệt</a>";
    			echo "<a type='button' class='btn btn-update btn-danger submit' id='buttonsubmit' onclick='skip(1)'><i class='fa fa-minus-circle'></i> Hủy</a>";
    		}
    		?>
	    </div>
    </div>
    <div class="import_order_info">
    	<form id="submit_order" method="POST" action="<?php echo $base_link.'update_order' ?>">
    		<input type="hidden" name="IDOrder" value="<?php echo $data->ID ?>" />
	    	<div class="block1">
	    		<?php 
	    		$data_note = json_decode($data->Note,true);
	    		?>
	    		<div class="row">
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Tên người yêu cầu : </label>
			    			<div class="col-xs-8">
				    			<select name="UserID" id="SalesmanID" class="form-control">
				    				<?php 
				    				echo "<option value='".$this->user->ID."'>".$this->user->UserName."</option>";
				    				?>
				    			</select>
			    			</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Người nhận hàng : </label>
			    			<div class="col-xs-8">
				    			<input type="text" name="Nguoinhanhang" class="form-control required" id="Nguoinhanhang" value="<?php echo isset($data_note['Nguoinhanhang']) ? $data_note['Nguoinhanhang'] : '' ; ?>" required />
		    					<input type='hidden' name="Ngayban" class="form-control date-picker" id="Ngayban" value="<?php echo date('m/d/Y',time()); ?>" required />
	    					</div>
	    				</div>
	    			</div>
	    		</div>
				<div class="row">
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Phòng ban / bộ phận : </label>
			    			<div class="col-xs-8">
				    			<input type="text" name="Phongban" class="form-control required" id="Phongban" value="<?php echo isset($data_note['Phongban']) ? $data_note['Phongban'] : '' ; ?>" required />
	    						<input type='hidden' name="Address" id="Address_order" class="form-control" value="246/9 Bình Quới, Phường 28, Quận Bình Thạnh, Thành phố Hồ Chí Minh" required />
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Lý do xuất : </label>
			    			<div class="col-xs-8">
				    			<input type='text' name="Note" class="form-control required" id='Note' value="<?php echo isset($data_note['Note']) ? $data_note['Note'] : '' ; ?>" required />
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    		
	    		<div class="row">
	    			<div class="col-xs-6">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Chọn kho xuất hàng : </label>
	    					<div class="col-xs-8">
	    						<select name="KhoID" id="KhoID" class="form-control" onchange="loadshipmentdefault()">
				    				<?php 
				    				if($this->user->UserType==2 || $this->user->UserType==8){
				    					$khoresult = $this->db->query("select * from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%'")->result();
				    				}else{
				    					$khoresult = $this->db->query("select * from ttp_report_warehouse")->result();
				    				}
				    				if(count($khoresult)>0){
				    					foreach($khoresult as $row){
				    						$selected = $data->KhoID==$row->ID ? "selected='selected'" : '' ;
				    						echo "<option value='$row->ID' $selected>$row->MaKho</option>";
				    					}
				    				}
				    				?>
				    			</select>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Địa chỉ nhận hàng : </label>
	    					<div class="col-xs-10">
								<input type='text' name="Address" class="form-control special_input" value="<?php echo $data->AddressOrder; ?>" />
							</div>
						</div>
					</div>
	    		</div>
	    	</div>
	    	<!-- end block1 -->
	    	<?php 
    		if($this->user->UserType==2 && $data->Status==4 && $data->Status!=0 && $data->Status!=1){
    		?>
	    	<div class="block2">
				<a class="btn btn-primary" id="add_products_to_order"><i class="fa fa-plus"></i> Sản phẩm</a>
	    	</div>
	    	<?php 
	    	}
	    	?>
	    	<div class="table_donhang table_data">
	    		<table class="table_data" id="table_data">
	    			<tr>
	    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
	    				<th>Mã sản phẩm</th>
	    				<th>Tên sản phẩm</th>
	    				<th>Lô</th>
	    				<th>Số lượng</th>
	    				<th>Thao tác</th>	
	    			</tr>
	    			<?php 
	    			$details = $this->db->query("select a.*,b.Title,b.MaSP,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
	    			$arrproducts = array();
	    			if(count($details)>0){
	    				foreach($details as $row){
	    					echo "<tr>";
	    					echo "<td><input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='$row->ProductsID'>
	    							  <input type='hidden' name='ProductsID[]' value='$row->ProductsID'>
	    					</td>";
	    					echo "<td>$row->MaSP</td>";
	    					echo "<td>$row->Title</td>";
	    					$change_shipment = $this->user->UserType==2 && $data->Status==4 ? "<a class='change_shipment' onclick='change_shipment(this,$row->ShipmentID,$row->ProductsID,$row->Amount,$row->ID)'><i class='fa fa-pencil'></i></a><ul></ul>" : "" ;
	    					echo "<td><span>$row->ShipmentCode</span> $change_shipment</td>";
	    					echo "<td><input type='number' name='Amout[]' class='soluong' value='$row->Amount' min='1' onchange='changeamount(this,$row->ID)' readonly='true' /></td>";
	    					$remove_function = $this->user->UserType==2 && $data->Status==4 ? "<a onclick='remove_products(this,$row->ID)' title='Loại bỏ sản phẩm này ra khỏi đơn hàng'><i class='fa fa-times'></i></a>" : '--' ;
	    					echo "<td style='text-align:center'>$remove_function</td>";
	    					echo "</tr>";
	    				}
	    			}

	    			?>
	    		</table>
	    	</div>
	    	<div class="history_status">
	    		<h2>Lịch sử trạng thái đơn hàng</h2>
	    		<?php 
	    		$history = $this->db->query("select a.*,b.UserName from ttp_report_orderhistory a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
	    		if(count($history)>0){
	    			$array_status = array(
	                    4 => 'Yêu cầu bị trả về',
	                    3 => 'Yêu cầu đã được duyệt',
	                    2 => 'Yêu cầu chờ được duyệt',
	                    1 => 'Yêu cầu bị hủy',
	                    0 => 'Yêu cầu thành công'
	                );
	    			echo "<table><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
	    			foreach($history as $row){
	    				echo "<tr>";
	    				echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>" ;
	    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
	    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
	    				echo "<td>$row->UserName</td>";
	    				echo "</tr>";
	    			}
	    			echo "</table>";
	    		}
	    		?>
	    	</div>
	    	<div class="block1">
	    		<input type='hidden' name="Tinhtrangdonhang" id="Status" value='<?php echo $data->Status ?>' />
	    		<input type="hidden" name="Ghichu" id="ghichu" value="<?php echo $data->Ghichu ?>" />
	    		<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
	    	</div>
    	</form>
    </div>
    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
	.daterangepicker{width: auto;}
</style>
<script type="text/javascript">
	$("#buttonsubmit").click(function(){
		$(this).addClass('saving');
	});

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	var link = $("#baselink_report").val();

	$(".select_shipment").each(function(){
		var data = $(this).attr('data');
		var id = $(this).attr('data-id');
		$(this).load(link+"/warehouse_inventory_import/load_shipment_by_products/"+data+"/"+id);
	});

	$("#add_products_to_order").click(function(){
		enablescrollsetup();
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "",
            success: function(result){
                if(result!='false'){
        			$(".over_lay").show();
					$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
                $(this).removeClass('saving');
            }
        });
	});

	$("#edit_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
			}
		});
	});

	function changestatus(){
		$("#IsChangeOrder").val("1");
	}

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').hide();
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').show();
				parent.find('input[type="text"]').prop('readonly', true);
				parent.find('input.soluong').prop('readonly', true);
				parent.find('input[type="text"]').removeClass('border');
				parent.find('input.soluong').removeClass('border');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		if(ob.checked===true){
			parent.find('span.showsoluong').hide();
			parent.find('input[type="text"]').prop('readonly', false);
			parent.find('input.soluong').prop('readonly', false);
			parent.find('input[type="text"]').addClass('border');
			parent.find('input.soluong').addClass('border');
		}else{
			parent.find('span.showsoluong').show();
			parent.find('input[type="text"]').prop('readonly', true);
			parent.find('input.soluong').prop('readonly', true);
			parent.find('input[type="text"]').removeClass('border');
			parent.find('input.soluong').removeClass('border');
		}
	}

	
	var celldata = [<?php echo implode(',',$arrproducts) ?>];
	var sttrow = <?php echo count($arrproducts) ?>+1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				if(jQuery.inArray( "data"+data_id, celldata )<0){
					var table = document.getElementById("table_data");
					var row = table.insertRow(sttrow);
					row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
					row.insertCell(1).innerHTML=data_code;
					row.insertCell(2).innerHTML=data_name;
					row.insertCell(3).innerHTML="";
					row.insertCell(4).innerHTML="<input type='number' name='Amout[]' class='soluong' value='1' min='1' onchange='recal()' readonly='true' />";
					sttrow=sttrow+1;
					celldata.push("data"+data_id);
				}
			}
		});
		$(".over_lay").hide();
		disablescrollsetup();
	}

	function remove_products(ob,IDDetails){
		if(IDDetails>0){
			$(ob).addClass("...");
			$.ajax({
	        	url: link+"warehouse_inventory_export/remove_products",
	            dataType: "html",
	            type: "POST",
	            data: "ID="+IDDetails,
	            success: function(result){
	            	if(result=="OK"){
	            		location.reload();
	            	}else{
	            		alert("False");
	            	}
	            }
	        });
        }
	}

	function skip(status){
		if (!confirm('VUI LÒNG XÁC NHẬN ĐỂ THỰC HIỆN THAO TÁC ??')) {
			return false;
		}
		$("#Status").val(status);
		var Nguoinhanhang = $("#Nguoinhanhang").val();
		if(Nguoinhanhang==''){
			alert("Vui lòng điền người nhận hàng");
			return false;
		}
		var Phongban = $("#Phongban").val();
		if(Phongban==''){
			alert("Vui lòng điền phòng ban yêu cầu xuất kho");
			return false;
		}
		var Note = $("#Note").val();
		if(Note==''){
			alert("Vui lòng điền lý do xuất kho");
			return false;
		}
		var KhoID = $("#KhoID").val();
		if(KhoID=='' || KhoID==0){
			alert("Vui lòng chọn kho xuất hàng");
			return false;
		}

		$(this).addClass("saving");
		$(this).find("i").hide();
		$("#submit_order").submit();
	}

	function showchiphi(){
		$("#showchiphivanchuyen").hide();
		$(".chiphivanchuyen").show();
		$(".chiphivanchuyen").focus();
	}

	document.onkeydown = function(e) {
		if(e.keyCode === 13){
			$("form").submit();
		}
	}

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function change_shipment(ob,shipment,products,amount,iddetails){
		var WarehouseID = $("#KhoID").val();
		if(WarehouseID!=''){
			$.ajax({
	        	url: link+"warehouse_inventory_export/get_another_shipment",
	            dataType: "html",
	            type: "POST",
	            data: "ID="+iddetails+"&WarehouseID="+WarehouseID+"&ShipmentID="+shipment+"&ProductsID="+products+"&Amount="+amount,
	            success: function(result){
	            	$(ob).parent('td').find('ul').html(result);
	            }
	        });
        }
	}

	function replace_shipment(IDDetails,Shipment){
		if (!confirm('BẠN CÓ MUỐN ĐỔI LÔ LẤY HÀNG CHO SẢN PHẨM NÀY ??')) {
			return false;
		}else{
			$.ajax({
	        	url: link+"warehouse_inventory_export/replace_shipment",
	            dataType: "html",
	            type: "POST",
	            data: "ID="+IDDetails+"&ShipmentID="+Shipment,
	            success: function(result){
	            	if(result=="False"){
	            		alert("Thay đổi lô hàng thất bại ! Mọi thay đổi của bạn ko được cập nhật !");
	            		location.reload();
	            	}else{
	            		alert("Thay đổi lô hàng thành công !");
	            		location.reload();
	            	}
	            }
	        });
		}
	}

	function changeamount(ob,IDDetails){
		if (!confirm('BẠN CÓ MUỐN THAY ĐỔI SỐ LƯỢNG CHO SẢN PHẨM NÀY ??')) {
			return false;
		}else{
			var amount = $(ob).val();
			if(amount>0){
				$.ajax({
		        	url: link+"warehouse_inventory_export/replace_amount",
		            dataType: "html",
		            type: "POST",
		            data: "ID="+IDDetails+"&Amount="+amount,
		            success: function(result){
		            	if(result=="OK"){
		            		alert("Thay đổi số lượng thành công !");
		            		location.reload();
		            	}else{
		            		alert("Thay đổi số lượng thất bại ! \n"+result);
		            		location.reload();
		            	}
		            }
		        });
	        }
		}
	}
</script>