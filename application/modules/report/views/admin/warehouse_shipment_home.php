<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<h1>Danh sách lô hàng sản phẩm</h1>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'add'; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tạo mới</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th>Mã lô</th>
					<th>Tên sản phẩm</th>
					<th>Ngày sản xuất</th>
					<th>Hạn sử dụng</th>
					<th>Action</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->ShipmentCode</td>";
						echo "<td>$row->Title</td>";
						echo "<td>".date('d/m/Y',strtotime($row->DateProduction))."</td>";
						echo "<td>".date('d/m/Y',strtotime($row->DateExpiration))."</td>";
						echo "<td>
								<a href='{$base_link}edit/$row->ID'><i class='fa fa-pencil-square-o'></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                <a href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
						echo "</tr>";
					}
				}else{
					echo "<td colspan='6'>Không tìm thấy dữ liệu</td>";
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>