<style>
	table{border-collapse: collapse;width:100%;}
	table tr td input[type="text"]{width:100%; padding:3px 5px;border:none;}
</style>

<div class="containner">
		<select id="cityclass" style="padding:4px 10px;border:1px solid #E1e1e1">
			<option value="">-- Tất cả tỉnh thành --</option>
			<?php 
				$city = $this->db->query("select * from ttp_report_city")->result();
				foreach($city as $row){
					echo "<option value='$row->ID'>$row->Title</option>";
				}
			?>
		</select>
		
		<button class="btn btn-success" onclick="savedata()" style="color:#FFF;padding:5px 10px;float: right;margin-bottom: 15px;">Save All</button>
		<a href="<?php echo base_url().ADMINPATH."/report/manager_city/export" ?>" class='btn btn-primary' style="color:#FFF;padding:5px 10px;float:right;margin-right:10px">Export</a>
		<div class="table_data">
		<table>
			<tr>
				<td style='padding:3px 5px;border:1px solid #E1e1e1;width:150px'>Quận huyện</td>
				<td style='padding:3px 5px;border:1px solid #E1e1e1'>Giao hàng nhanh</td>
				<td style='padding:3px 5px;border:1px solid #E1e1e1'>Goldtimes</td>
				<td style='padding:3px 5px;border:1px solid #E1e1e1'>Status</td>
			</tr>
		<?php 
		$result = $this->db->query("select * from ttp_report_district")->result();
		foreach($result as $row){
			echo "<tr class='trclass cityclass$row->CityID'>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'><input type='hidden' class='CITY$row->CityID' value='$row->ID' />$row->Title</td>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'><input type='text' class='GHN' value='$row->GHN' /></td>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'><input type='text' class='GOLD' value='$row->GoldTimes' /></td>
			<td style='padding:3px 5px;border:1px solid #E1e1e1'><span class='status'></span></td>
			</tr>";
		}
		?>
		</table>
		</div>
</div>

<script>
	$("#cityclass").change(function(){
		var data = $(this).val();
		$("table tr.trclass").hide();
		$(".cityclass"+data).show();
	});

	function savedata(){
		var currentcity = $("#cityclass").val();
		$(".CITY"+currentcity).each(function(){
			var CITY = $(this).val();
			var GHN  = $(this).parent().parent().find('.GHN').val();
			var GOLD = $(this).parent().parent().find('.GOLD').val();
			save(this,CITY,GHN,GOLD);
		});
	}

	function save(OB,CITY,GHN,GOLD){
		$.ajax({
        	url: "<?php echo base_url().ADMINPATH.'/report/manager_city/save_district_mapper' ?>",
            dataType: "html",
            type: "POST",
            data: "CITY="+CITY+"&GHN="+GHN+"&GOLD="+GOLD,
            beforeSend:function(){
            	$(OB).parent().parent().find('.status').html('Saving ...');
            },
            success: function(result){
            	$(OB).parent().parent().find('.status').html('Saved');
            }
        });
	}

</script>