<?php 
$url = base_url().ADMINPATH.'/report';
$segment_current = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-line-chart fa-fw"></i> API</p>
    <ul>
        <li><a href="<?php echo $url.'/api_website' ?>">Website register</a></li>
        <li><a href="<?php echo $url.'/api_client' ?>">Client certificate</a></li>
        <li><a href="<?php echo $url.'/api_users' ?>">Client users</a></li>
        <li><a href="<?php echo $url.'/api_history' ?>">History connect</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>
