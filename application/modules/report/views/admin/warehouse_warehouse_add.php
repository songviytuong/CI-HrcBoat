<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÊM KHO MỚI</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_content_warehouse">
				<div class="block1">
					<table class="table1">
						<tr>
							<td>Mã kho:</td>
							<td><input type='text' name='MaKho' /></td>
							<td>Tên kho:</td>
							<td colspan='3'><input type='text' name='Title' /></td>
						</tr>
						<tr>
							<td>Phân loại kho:</td>
							<td>
								<select name="">
									<option>-- Chọn phân loại kho --</option>
								</select>
							</td>
							<td>Màu sắc kho</td>
							<td><input type='color' name="Color" /></td>
						</tr>
						<tr>
							<td style="vertical-align:top">Thủ kho:</td>
							<td colspan="5">
								<?php 
								$user = $this->db->query("select ID,UserName from ttp_user where UserType=2 or UserType=8")->result();
								if(count($user)>0){
									echo "<select id='user_warehouse'>";
									foreach($user as $row){
										echo "<option value='$row->ID'>$row->UserName</option>";
									}
									echo "</select>";
									echo "<a class='add_user' onclick='add_user()'><i class='fa fa-plus'></i> ADD</a><div style='clear:both;margin-top: 5px;'></div>";
								}
								?>
							</td>
						</tr>
						<tr>
							<td>Địa chỉ kho</td>
							<td colspan='5'><input type='text' name='Address' /></td>
						</tr>
						<tr>
							<td>Khu vực:</td>
							<td>
								<select name='AreaID' id="Khuvuc">
									<option value="">-- Chọn khu vực --</option>
				    				<?php 
				    				$area = $this->db->query("select * from ttp_report_area")->result();
				    				if(count($area)>0){
				    					foreach($area as $row){
				    						echo "<option value='$row->ID'>$row->Title</option>";
				    					}
				    				}
				    				?>
								</select>
							</td>
							<td>Tỉnh/Thành:</td>
							<td>
								<select name='CityID' id="Tinhthanh">
									<option value='0'>-- Chọn Tỉnh/Thành --</option>
								</select>
							</td>
							<td>Quận/Huyện:</td>
							<td>
								<select name='DistrictID' id="Quanhuyen">
									<option value='0'>-- Chọn Quận/Huyện --</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Số di động 1:</td>
							<td><input type='text' name='Phone1' /></td>
							<td>Số di động 2:</td>
							<td><input type='text' name='Phone2' /></td>
							<td></td>
							<td></td>
						</tr>
						
					</table>
				</div>
				<!-- end block1 -->
		    	<div class="block2">
					<a class="btn btn-primary" id="add_position_to_warehouse"><i class="fa fa-plus"></i> Vị trí</a>
					<ul>
						<li><a id='show_thaotac'>Thao tác <b class="caret"></b></a>
		    				<ul>
			    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa vị trí</a></li>
		    				</ul>
						</li>
					</ul>
		    	</div>
		    	<div class="clear"></div>
		    	<div class="table_donhang">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Khu vực</th>
		    				<th>Tên kệ</th>
		    				<th>Tên cột</th>
		    				<th>Tên hàng</th>
		    			</tr>
		    			<tr class='last_tr'>
		    				<td colspan="5">Không có dữ liệu vị trí</td>
		    			</tr>
		    		</table>
		    	</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
</div>
<script type="text/javascript">
	var link = "<?php echo base_url().ADMINPATH.'/report/' ?>";

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_city_by_area",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_district_by_city",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	                var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            	$("#KhoID").html(jsonobj.WarehouseHtml);
	            }
	        });
		}
	});

	$("#add_position_to_warehouse").click(function(){
		$("#table_data .last_tr").remove();
		$("#table_data").append("<tr><td><input type='checkbox' class='selected_products' /></td><td><input type='text' name='Position[]' /></td><td><input type='text' name='Rack[]' /></td><td><input type='text' name='Colum[]' /></td><td><input type='text' name='Row[]' /></td></tr>");
	});

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				$(this).parent('td').parent('tr').remove();
			}
		});
	});

	var celldata = [];

	function add_user(){
		var user = $("#user_warehouse").val();
		var name = $("#user_warehouse option:selected" ).text();
		if(jQuery.inArray( "data"+user, celldata )<0){
			$("#user_warehouse").parent('td').append("<li class='list_owner'>"+name+"<a onclick='remove_user(this,"+user+")'>[x]</a> <input type='hidden' name='Manager[]' value='"+user+"' /></li>");
			celldata.push("data"+user);
		}
	}

	function remove_user(ob,user){
		$(ob).parent('li').remove();
		var index = celldata.indexOf("data"+user);
		celldata.splice(index, 1);
	}
</script>