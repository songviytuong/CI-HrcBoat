<?php 
$url = base_url().ADMINPATH.'/report/promotion_campaign';
$segment_current = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
$segment_current_bonus_child = $this->uri->segment(5);

?>
<style>
    .squ {background-color: #fff; width:25px; padding:5px; text-align: center; border-radius:50%; font-size:11px; position: absolute; right:5px; top:1px;}
</style>
<div class="sitebar">
    <p class="title"><i class="fa fa-line-chart fa-fw"></i> PROMOTION</p>
    <ul>
        <li><a href="<?php echo $url.'/all' ?>" <?php echo $segment_current_bonus_child=='' ? "class='active'" : '' ; ?>>Tất cả chiến dịch</a> <div class="squ">0</div></li>
        <li><a href="<?php echo $url.'/all/1' ?>" <?php echo $segment_current_bonus_child=='1' ? "class='active'" : '' ; ?>>Chiết khấu trên SP</a> <div class="squ">0</div></li>
        <li><a href="<?php echo $url.'/all/2' ?>" <?php echo $segment_current_bonus_child=='2' ? "class='active'" : '' ; ?>>Chiết khấu trên ĐH</a> <div class="squ">0</div></li>
        <li><a href="<?php echo $url.'/all/3' ?>" <?php echo $segment_current_bonus_child=='3' ? "class='active'" : '' ; ?>>Quà tặng</a></li>
        <li><a href="<?php echo $url.'/all/4' ?>" <?php echo $segment_current_bonus_child=='4' ? "class='active'" : '' ; ?>>Khách hàng đặc biệt</a> <div class="squ">0</div></li>
        <li><a>Combo / Bundle</a></li>
        <li><a>Tích điểm thưởng</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>

<script src="public/admin/js/notify.js"></script>
<?php
if($this->user->UserType==5 || $this->user->UserType==7 || $this->user->UserType==8){
?>
<script>
    var maxnoti = <?php echo isset($cancel) ? $cancel->Soluong : 0 ; ?>;
    var accept = '';
    var username = '';
    var madh = '';
    var getcurrentorder = function(){
        $.ajax({
            url: "<?php echo base_url().ADMINPATH; ?>/report/import/check_notify",
            dataType: "html",
            type: "POST",
            data: "Max="+maxnoti,
            success: function(result){
                if(result!="false"){
                    var arrnotify = result.split("|");
                    maxnoti = arrnotify[0];
                    var accept = arrnotify[1];
                    var username = arrnotify[2];
                    var madh = arrnotify[3];
                    var img = arrnotify[4];
                    if(accept==0){
                        notifyMe(img,username +' vừa gửi yêu cầu hủy đơn hàng '+madh);
                    }else{
                        var useraccept = arrnotify[5];
                        notifyMe(img,useraccept + ' vừa xử lý yêu cầu hủy đơn hàng '+ madh +' của '+username);
                    }
                }
            }
        });
    }
    setInterval(getcurrentorder,5000);

    function notifyMe(image,message){
        notify('THÔNG BÁO HỆ THỐNG', {
            body: message,
            icon: image,
            onclick: function(e) {
                window.location = "<?php echo base_url().ADMINPATH.'/report/request_cancel' ?>";
            },
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }
<?php 
}
?>
</script>