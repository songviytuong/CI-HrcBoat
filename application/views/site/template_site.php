<?php echo $_doctype; ?>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo $_title; ?>
	<base href="<?php echo base_url(); ?>" >
	<meta name="description" content="<?php echo $MetaDescription; ?>" />
	<meta name="keywords" content="<?php echo $MetaKeywords; ?>" />
    <?php echo $MetaExtend ?>
    <meta http-equiv='Refresh' content='300; url=<?php echo current_url() ?>'>
	<meta name="robots" content="index,follow,all" />
	<meta name="revisit-after" content="1 days" />
	<link type="image/x-icon" href="public/site/images/fav.ico" rel="shortcut icon">
    <link href="public/site/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" type="text/css"><link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
    <link href="public/site/css/style.css" rel="stylesheet" type="text/css">
    <!-- <link rel="stylesheet" type="text/css" href="public/site/css/animate.css"> -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    -->
	<?php 
    $GA = $this->db->query("select Data from ttp_static_config limit 0,1")->row();
    $GA = $GA ? json_decode($GA->Data) : "" ;
    if(isset($GA->Analytics)){
        echo $GA->Analytics!='' ? "<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', '$GA->Analytics', 'auto');
          ga('send', 'pageview');

        </script>" : "";
    }
    ?>
</head>
<body>
    <?php echo $header; ?>
	<?php echo $content; ?>
	<?php echo $footer; ?>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
    <!-- Latest compiled JavaScript-->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="public/site/js/script.js"></script>
    <!-- Include wow.min.js-->
    <!-- <script src="public/site/js/wow.min.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed		-->
</body>
</html>
