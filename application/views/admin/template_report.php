<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo base_url() ?>">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="image/x-icon" href="public/site/images/fav.ico" rel="shortcut icon">
	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="public/admin/css/bootstrap.min.css" rel="stylesheet">

	    <link href="public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="public/admin/css/animate.min.css" rel="stylesheet">
        <!-- Custom styling plus plugins -->
        <link href="public/admin/css/style_report.css" rel="stylesheet">
        <link href="public/admin/css/multiple-select.css" rel="stylesheet">

        <script src="public/admin/js/jquery.min.js"></script>
        <script src="public/admin/js/bootstrap.min.js"></script>
        <!-- chart js -->
        <script src="public/admin/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="public/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="public/admin/js/moment.min2.js"></script>
        <script type="text/javascript" src="public/admin/js/datepicker/daterangepicker.js"></script>
        <!-- flot js -->
        <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.pie.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.orderBars.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.time.min.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/date.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.spline.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.stack.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/curvedLines.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.resize.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.axislabels.js"></script>
        <script type="text/javascript" src="public/admin/js/multiple-select.js"></script>
        <?php echo $_styles; ?>
        <?php echo $_title; ?>
    </head>
    <body>
        <div class="body_content">
            <?php echo $header; ?>
            <?php echo $sitebar; ?>
            <?php echo $content; ?>
        </div>
        <div class="version">Develop by Mr.Hieu (Ext : 922)
          <span>Report tools version v1.1 last update 11/2015 - &copy; Tran Toan Phat group</span>
        </div>
    <?php echo $_scripts; ?>
</body>
</html>
