<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>.:: H-RC BOAT 2020 ::.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="/public/sollyken/css/reset.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Montserrat:400,700'>
    <link rel='stylesheet prefetch' href='/public/sollyken/css/font-awesome.min.css'>
    <link rel='stylesheet prefetch' href='/public/sollyken/css/bootstrap.min.css'>
    <link rel="stylesheet" href="/public/sollyken/css/style.css?v=<?= filemtime(BASEPATH . '../public/sollyken/css/style.css'); ?>">
</head>

<body>
    <div class="container">
        <div class="info">
            <h1>H-RC BOAT</h1><span>Made with <i class="fa fa-heart"></i> by <a href="<?php echo base_url() ?>">Lee Peace</a></span>
        </div>
    </div>
    <?php echo $content; ?>
    <script src='/public/sollyken/js/jquery.min.js'></script>
    <?php
    if ($this->session->userdata("admin")) :
    ?>
        <script>
            window.sollyken = {
                is_admin: "<?= $this->session->userdata("admin"); ?>"
            };
        </script>
    <?php
    endif;
    ?>
    <script src="/public/sollyken/js/index.js?v=<?= filemtime(BASEPATH . '../public/sollyken/js/index.js'); ?>"></script>
</body>

</html>