<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Admin_Controller extends CI_Controller
{
	public $upload_to = '';
	public $folder_path_upload_image = "";
    public $folder_url_upload_image = "";
    
    
    public function __construct(){
        parent::__construct();
        $this->folder_url_upload_image = base_url()."assets/";
        $this->folder_path_upload_image = "./assets/";
		$user = $this->session->userdata('ttp_usercp');
		if($user==''){
			redirect(ADMINPATH.'/login');
		}
    }
    
    public function make_folder($str,$folder='assets/'){
    	if($str!=''){
	        if(!file_exists($folder.$str)){
		        mkdir("./$folder".$str, 0777, true);
	        }
    	}
    }

    public function upload_image_single($name="Image_upload",$type="image"){
        $this->upload_to = date('Y').'/'.date('Y-m');
        $this->make_folder($this->upload_to,'assets/');
        if($type=="image"){
            $config = array('upload_path'   => $this->folder_path_upload_image.$this->upload_to,
                        'allowed_types' => 'gif|jpg|png',
                        'max_size'      => '300',
                        'encrypt_name' => TRUE
                        );
        }else{
            $config = array('upload_path'   => $this->folder_path_upload_image.$this->upload_to,
                        'allowed_types' => 'mp4|flv|wmv|3gp|avi',
                        'max_size'      => '300000',
                        'encrypt_name' => false
                        );
        }
        $this->load->library("upload",$config);
        if(!$this->upload->do_upload($name)){
            $error = array($this->upload->display_errors());
            print_r($error);
            return false;
        }else{
            $image_data = $this->upload->data();
            $data = array(
                "Url"   =>"assets/".$this->upload_to.'/'.$image_data['file_name'],
                "Name"  =>$image_data['file_name'],
                "Created"=>date('Y-m-d H:i:s'),
                "LastEdited"=>date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_files',$data);
        }
        $this->created_thumb_single("assets/".$this->upload_to.'/'.$image_data['file_name']);
        $cropimage = explode(",",IMAGECROP);
        foreach($cropimage as $row){
            $size = explode("x", $row);
            $width = isset($size[0]) ? (int)$size[0] : 0 ;
            $height = isset($size[1]) ? (int)$size[1] : 0 ;
            if($width>0 && $height>0) $this->lib->cropimage("assets/".$this->upload_to.'/'.$image_data['file_name'],$width,$height);
        }
        return "assets/".$this->upload_to.'/'.$image_data['file_name'];
    }

    public function upload_image_single_report($table="ttp_report_images_products",$data,$save=true){
        $this->upload_to = date('Y').'/'.date('Y-m');
        $this->make_folder($this->upload_to,'assets/');
        $config = array('upload_path'   => $this->folder_path_upload_image.$this->upload_to,
                        'allowed_types' => 'gif|jpg|png',
                        'max_size'      => '3000',
                        'encrypt_name' => TRUE
                        );
        $this->load->library("upload",$config);
        if(!$this->upload->do_upload("file")){
            $error = array($this->upload->display_errors());
            return false;
        }else{
            $image_data = $this->upload->data();
        }
        $this->created_thumb_single("assets/".$this->upload_to.'/'.$image_data['file_name']);
        if($save==true){
            $max = $this->db->query("select max(ID) as ID from $table")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $sql = array(
                'ID'        =>$max,
                'Name'      =>$image_data['file_name'],
                'Url'       =>"assets/".$this->upload_to.'/'.$image_data['file_name'],
                'Created'   =>date('Y-m-d H:i:s'),
                'Label'     =>isset($data['Label']) ? $data['Label'] : $image_data['file_name'],
                'PrimaryImage'=>isset($data['Primary']) ? $data['Primary'] : 1,
                'Published' =>1,
                'STT'       =>isset($data['STT']) ? $data['STT'] : 1,
                'ProductsID'=>$data['ProductsID']
            );
            $this->db->insert($table,$sql);
            $cropimage = explode(",",IMAGECROP);
            foreach($cropimage as $row){
                $size = explode("x", $row);
                $width = isset($size[0]) ? (int)$size[0] : 0 ;
                $height = isset($size[1]) ? (int)$size[1] : 0 ;
                if($width>0 && $height>0) $this->lib->cropimage($sql['Url'],$width,$height);
            }
            return $sql;
        }else{
            $cropimage = explode(",",IMAGECROP);
            foreach($cropimage as $row){
                $size = explode("x", $row);
                $width = isset($size[0]) ? (int)$size[0] : 0 ;
                $height = isset($size[1]) ? (int)$size[1] : 0 ;
                if($width>0 && $height>0) $this->lib->cropimage("assets/".$this->upload_to.'/'.$image_data['file_name'],$width,$height);
            }
            return "assets/".$this->upload_to.'/'.$image_data['file_name'];
        }
    }

    public function created_thumb_single($path,$width=80,$height=80){
        $config['image_library'] = 'gd2';
        $config['create_thumb'] = true;
        $config['maintain_ratio'] = true;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['source_image'] = $path;
        $this->load->library('image_lib', $config);
        if (!$this->image_lib->resize()){
            echo $this->image_lib->display_errors();
        }
    }
	
	public function UploadMultiImages($table_file='ttp_images'){
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->upload_to = date('Y').'/'.date('Y-m');
        $this->make_folder($this->upload_to,'assets/',$table_file);
        $this->load->library("upload");
        $this->upload->initialize(array(
            "upload_path"   => $this->folder_path_upload_image.$this->upload_to,
            'allowed_types' => 'gif|jpg|png',
            'max_size'      => '2000',
			'encrypt_name' => TRUE
        ));
        if($this->upload->do_multi_upload("Images_upload")){
			$url_ID = array();
            $url_thumb = array();
            $data_file = $this->upload->get_multi_upload_data();
        	for($i=0; $i<count($_FILES['Images_upload']["name"]); $i++){
                $data = array(
		            'Name'=>$data_file[$i]['file_name'],
		            'Thumb'=>"assets/".$this->upload_to.'/'.$data_file[$i]['file_name'],
                    'Created'=>date('Y-m-d H:i:s',time()),
                    'LastEdited'=>date('Y-m-d H:i:s',time())
		        );
		        $this->db->insert($table_file, $data);
                $max = $this->db->query("select max(ID) as ID from ttp_images")->row();
                $url_ID[] = $max->ID;
                $url_thumb[] = "assets/".$this->upload_to.'/'.$data_file[$i]['file_name'];
        	}
            return array("ID"=>$url_ID,"Thumb"=>$url_thumb);
        }
        return array();
    }
    
    public function delete_file_by_id_record($table='',$idrecord,$table_file='files'){
    	if($table!=''){
            $list_id = $this->db->query("select * from $table where ID in($idrecord)")->result();
            if($list_id){ 
                foreach($list_id as $row){
                    $this->delete_file_by_id_file($row->ImageID,$table_file);
                }
            }
    	}
    }
    
    public function resize_image($path,$width=200,$height=300){
    	if(is_array($path)){
    		$this->load->library('image_lib');
			foreach($path as $row){
				$config['image_library'] = 'gd2';
				$config['create_thumb'] = false;
				$config['maintain_ratio'] = true;
				$config['width'] = $width;
				$config['height'] = $height;
				$config['source_image'] = $row;
				$this->image_lib->clear();
			    $this->image_lib->initialize($config);
			    if ( ! $this->image_lib->resize()){
				    echo $this->image_lib->display_errors();
				}
			}
		}else{
			$this->load->library('image_lib');
            $config['image_library'] = 'gd2';
			$config['create_thumb'] = false;
			$config['maintain_ratio'] = true;
			$config['width'] = $width;
			$config['height'] = $height;
			$config['source_image'] = $path;
			$this->image_lib->clear();
            $this->image_lib->initialize($config);
			if(!$this->image_lib->resize()){
			    echo $this->image_lib->display_errors();
			}
		}
    }
    
    public function created_thumb_image($path,$width=100,$height=80,$table_file='files'){
        if(is_array($path)){
            $this->load->library('image_lib');
            foreach($path as $row){
                $config['image_library'] = 'gd2';
                $config['create_thumb'] = true;
                $config['maintain_ratio'] = true;
                $config['width'] = $width;
                $config['height'] = $height;
                $config['source_image'] = $row;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if ( ! $this->image_lib->resize()){
                    echo $this->image_lib->display_errors();
                }else{
                    $thumb_image = $this->get_thumb($row);
                    $this->db->query("update $table_file set Thumb = '$thumb_image' where Path = '$row'");
                }

            }
        }else{
            $config['image_library'] = 'gd2';
            $config['create_thumb'] = true;
            $config['maintain_ratio'] = true;
            $config['width'] = $width;
            $config['height'] = $height;
            $config['source_image'] = $path;
            $this->load->library('image_lib', $config);
            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
            }else{
                $thumb_image = $this->get_thumb($path);
                $this->db->query("update $table_file set Thumb = '$thumb_image' where Path = '$path'");
            }           
        }
    }

    public function get_thumb($path=''){
        if($path!=''){
            $str = strlen($path);
            $name = $str-4;
            $start = $str-4;
            $str_name = substr($path,0,$name);
            $str_extend = substr($path,$start,4);
            $name_thumb = $str_name.'_thumb'.$str_extend;
            return $name_thumb;
        }
    }

    public function delete_file_by_id_file($id,$table_file='files'){
    	if($id!=''){
	        $this->load->helper("file");
	        $path = $this->db->query("select * from $table_file where ID = $id")->row_array();
	        if($path){
	        	if(file_exists($path['Path'])){
                    unlink("./".$path['Path']);    
                }
                if(file_exists($path['Thumb'])){
                    unlink("./".$path['Thumb']);    
                }
	            $this->db->query("delete from $table_file where ID=$id");
	        }
    	}
    }

    public function delete_file_path($path=''){
        if(file_exists($path)){
            @unlink("./".$path);
        }
        $thumbfile = $this->get_thumb($path);
        if(file_exists($thumbfile)){
            @unlink("./".$thumbfile);
        }
        $cropimage = explode(",",IMAGECROP);
        if(count($cropimage)>0){
            $image = explode("/", $path);
            $filename = count($image)>0 ? $image[count($image)-1] : $image[0] ;
            foreach($cropimage as $row){
                $size = explode("x", $row);
                $width = isset($size[0]) ? (int)$size[0] : 0 ;
                $height = isset($size[1]) ? (int)$size[1] : 0 ;
                $crop = $width."x".$height."_".$filename;
                $file = str_replace($filename,$crop,$path);
                if(file_exists($file)){
                    @unlink("./".$file);
                }
            }
        }
    }

    public function write_log($message=''){
        if($message!=''){
            $messagelog = date('H:i:s',time())." => User '{$this->user->UserName}' write log \n".$message;
            file_put_contents("./log/report/".date('d-m-Y',time()).".txt",$messagelog."\n\n",FILE_APPEND);
        }
    }
}
