<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Order Status
 */
if (!function_exists('OrderStatus')) {
    function OrderStatus($id = null)
    {
        $status = array(
            0   => 'Đơn hàng thành công',
            1   => 'Đơn hàng hủy',
            2   => 'Đơn hàng nháp',
            3   => 'Kho đang xử lý',
            4   => 'Kho trả về',
            5   => 'Kế toán xử lý',
            6   => 'Kế toán trả về',
            7   => 'Đang vận chuyển',
            8   => 'Đơn hàng bị trả về',
            9   => 'Điều phối đang xử lý',
            10  => 'Chờ xác nhận hủy',
        );

        if ($id === null) {
            return $status;
        }
        return $status[$id];
    }
}
/*
 * Order Type
 */
if (!function_exists('OrderType')) {
    function OrderType($id = null)
    {
        $type = array(
            0   => 'Online',
            1   => 'GT',
            2   => 'MT',
            3   => 'Gốm sứ'
        );

        if ($id === null) {
            return $type;
        }
        return $type[$id];
    }
}
/*
 * Payment Method
 */
if (!function_exists('OrderPayment')) {
    function OrderPayment($id = null)
    {
        $payment = array(
            0   => 'COD',
            1   => 'Chuyển khoản'
        );

        if ($id === null) {
            return $payment;
        }
        return $payment[$id];
    }
}
