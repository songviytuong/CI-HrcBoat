<?php

/*
* File
*/
if (!function_exists('GetRemoteLastModified')) {
    function GetRemoteLastModified($uri = null)
    {
        // default
        $unixtime = 0;

        $fp = fopen($uri, "r");
        if (!$fp) {
            return;
        }

        $MetaData = stream_get_meta_data($fp);

        foreach ($MetaData['wrapper_data'] as $response) {
            // case: redirection
            if (substr(strtolower($response), 0, 10) == 'location: ') {
                $newUri = substr($response, 10);
                fclose($fp);
                return GetRemoteLastModified($newUri);
            }
            // case: last-modified
            elseif (substr(strtolower($response), 0, 15) == 'last-modified: ') {
                $unixtime = strtotime(substr($response, 15));
                break;
            }
        }
        fclose($fp);
        return $unixtime;
    }
}
