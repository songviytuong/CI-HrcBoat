<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$route[ADMINPATH] = "authentication";
$route[ADMINPATH . '/check-login'] = "authentication/login_now";
$route[ADMINPATH . '/login'] = "authentication/login";
$route[ADMINPATH . '/login/(:any)'] = "authentication/login";
$route[ADMINPATH . '/logout'] = "authentication/logout";
$route[ADMINPATH . '/([a-zA-Z_-]+)/(:any)'] = "$1/admin/$2";
$route[ADMINPATH . '/([a-zA-Z_-]+)'] = "$1/admin/$1";
$route['([a-zA-Z_-]+)/'.ADMINPATH.'/([a-zA-Z_-]+)'] = "";
$route['accept_deny'] = 'home/accept_deny';
$route['localtest'] = "home/test";
$route['checkstatus'] = "home/checkstatus";
$route['api/'] = "api/$1";
//$subdomain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '' ;
//$subdomain = explode('.',$subdomain);
//if(count($subdomain)>= 3){
//    $subdomain = isset($subdomain[0]) ? $subdomain[0] : '' ;
//    $route['default_controller'] = "shop";
//    $route['(:any)-(:num).html'] = "shop/detail/$1/$2";
//}
//else{
//    $route['default_controller'] = "home";
//    //$route['(:any)'] = "home"; /*Ngôn ngữ*/
//}
$route['default_controller'] = "sollyken";
//$route['play'] = "sollyken/play";
//$route['screate'] = "sollyken/screate";
//$route['splay'] = "sollyken/splay";
//$route['iplayer'] = "sollyken";
$route['(:any)'] = "sollyken/$1/$2";
$route['404_override'] = '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */