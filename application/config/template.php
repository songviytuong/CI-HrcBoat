<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// section default
$template['default']['template'] = 'default/default_template';
$template['default']['regions'] = array(
    'top',
    'menu',
    'left',
    'content',
    'right',
    'bottom'
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;


$template['admin']['template'] = 'admin/template_admin';
$template['admin']['regions'] = array(
    'topnav',
    'sitebar',
    'content',
    'footer'
);
$template['admin']['parser'] = 'parser';
$template['admin']['parser_method'] = 'parse';
$template['admin']['parse_template'] = FALSE;

$template['report']['template'] = 'admin/template_report';
$template['report']['regions'] = array(
    'header',
    'sitebar',
    'content'
);
$template['report']['parser'] = 'parser';
$template['report']['parser_method'] = 'parse';
$template['report']['parse_template'] = FALSE;


$template['site']['template'] = 'site/template_site';
$template['site']['regions'] = array(
    'MetaDescription',
	'MetaKeywords',
	'MetaExtend',
	'header',
    'content',
	'footer'
);
$template['site']['parser'] = 'parser';
$template['site']['parser_method'] = 'parse';
$template['site']['parse_template'] = FALSE;

$template['shop']['template'] = 'shop/template_shop';
$template['shop']['regions'] = array(
    'MetaDescription',
    'MetaKeywords',
    'MetaExtend',
    'header',
    'content',
    'footer'
);

$template['sollyken']['template'] = 'sollyken/template_sollyken';
$template['sollyken']['regions'] = array(
    'MetaDescription',
    'MetaKeywords',
    'MetaExtend',
    'header',
    'content',
    'footer'
);